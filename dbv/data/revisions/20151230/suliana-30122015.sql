
ALTER TABLE `order`
CHANGE `paymentmethod` `paymentmethod` varchar(100) COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT 'MIGS, FPX' AFTER `amount`,
CHANGE `invoice_no` `invoice_no` varchar(50) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `invoice_id`;

ALTER TABLE `user`
CHANGE `lastlogin` `lastlogin` datetime NULL AFTER `token`;

ALTER TABLE `user`
CHANGE `token` `token` varchar(25) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `active`;

ALTER TABLE `enrol`
CHANGE `completed` `completed` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER `haspayment`;