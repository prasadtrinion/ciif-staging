ALTER TABLE `enrol`
CHANGE `schedule` `schedule` varchar(255) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `learningmode`;

ALTER TABLE `user`
ADD `from` varchar(100) COLLATE 'utf8mb4_unicode_ci' NOT NULL DEFAULT '0' COMMENT '1=corporate' AFTER `contactno`;

ALTER TABLE `enrol`
ADD `learningmode` varchar(50) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `order_id`,
ADD `schedule` int(11) unsigned NULL AFTER `learningmode`;

ALTER TABLE `order`
CHANGE `schedule` `schedule` varchar(255) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `learningmode`;