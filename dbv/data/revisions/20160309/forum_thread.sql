CREATE TABLE `forum_thread` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) unsigned NOT NULL DEFAULT '0',
  `thread_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thread_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `reply_count` int(11) DEFAULT '0',
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_safe` (`thread_subject`),
  KEY `parent_id` (`forum_id`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci