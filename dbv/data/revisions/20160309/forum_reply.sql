CREATE TABLE `forum_reply` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `thread_id` int(11) unsigned NOT NULL,
  `message` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reply_content` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_safe` (`reply_content`),
  KEY `parent_id` (`thread_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci