CREATE TABLE `forum` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` bigint(11) unsigned NOT NULL,
  `parent_id` bigint(11) unsigned NOT NULL DEFAULT '0',
  `category_id` bigint(11) unsigned NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visibility` tinyint(5) unsigned NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `display_only` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci