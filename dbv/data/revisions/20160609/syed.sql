DROP TABLE IF EXISTS `quiz_answer`;
CREATE TABLE `quiz_answer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_question` int(11) unsigned NOT NULL COMMENT 'quiz_qbank',
  `attempt_id` int(11) unsigned NOT NULL COMMENT 'quiz_attempt',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `answer` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correct_answer` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quesnum` tinyint(1) DEFAULT NULL,
  `mark` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `quiz_attempt`;
CREATE TABLE `quiz_attempt` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data_id` int(11) unsigned NOT NULL COMMENT 'content_block_data',
  `course_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `date_attempt` datetime DEFAULT NULL,
  `date_completed` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `quiz_qbank`;
CREATE TABLE `quiz_qbank` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data_id` int(11) unsigned NOT NULL COMMENT 'content_block_data',
  `course_id` int(11) unsigned NOT NULL,
  `mark` int(11) unsigned NOT NULL,
  `question` longtext COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `visibility` tinyint(5) unsigned NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `use_comments` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `answer1` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer2` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer3` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer4` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correct_answer` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `visibility` (`visibility`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;