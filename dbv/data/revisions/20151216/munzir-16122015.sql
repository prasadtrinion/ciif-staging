ALTER TABLE `order`
ADD `learningmode` varchar(50) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `item_id`;

ALTER TABLE `order`
ADD `schedule` int(11) NULL AFTER `learningmode`;