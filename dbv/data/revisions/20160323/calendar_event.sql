-- Adminer 4.2.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `calendar_event`;
CREATE TABLE `calendar_event` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) unsigned DEFAULT '0',
  `user_id` int(11) unsigned DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `time_start` time NOT NULL DEFAULT '00:00:00',
  `time_end` time NOT NULL DEFAULT '00:00:00',
  `visibility` tinyint(5) unsigned NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `owner` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0=personal [user_id],1=course [course_id]',
  PRIMARY KEY (`id`),
  KEY `visibility` (`visibility`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `calendar_event` (`id`, `course_id`, `user_id`, `title`, `description`, `date_start`, `date_end`, `time_start`, `time_end`, `visibility`, `created_by`, `created_date`, `modified_by`, `modified_date`, `owner`) VALUES
(2,	50,	0,	'This is a calendar option',	'This is a new event',	'2016-03-16',	'2016-03-19',	'12:00:00',	'12:30:00',	1,	1,	'2016-03-22 09:13:05',	NULL,	NULL,	1),
(3,	50,	0,	'Second event here',	'This is a new event',	'2016-03-10',	'2016-03-11',	'12:00:00',	'12:30:00',	1,	1,	'2016-03-22 09:13:05',	NULL,	NULL,	1),
(4,	50,	0,	'Third event here',	'This is a new event',	'2016-03-23',	'2016-03-24',	'12:00:00',	'12:30:00',	1,	1,	'2016-03-22 09:13:05',	NULL,	NULL,	1),
(7,	0,	1,	'Personal calendar option',	'This is a new event',	'2016-03-11',	'2016-03-13',	'12:00:00',	'12:30:00',	1,	1,	'2016-03-22 09:13:05',	NULL,	NULL,	0),
(8,	0,	1,	'Personal second event here',	'This is a new event',	'2016-03-30',	'2016-03-31',	'12:00:00',	'12:30:00',	1,	1,	'2016-03-22 09:13:05',	NULL,	NULL,	0),
(9,	0,	1,	'Third personal event here',	'This is a new event',	'2016-03-13',	'2016-03-14',	'12:00:00',	'12:30:00',	1,	1,	'2016-03-22 09:13:05',	NULL,	NULL,	0);

-- 2016-03-23 04:09:59
