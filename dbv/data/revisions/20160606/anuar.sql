CREATE TABLE `user_doc` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_extension` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_filesize` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;