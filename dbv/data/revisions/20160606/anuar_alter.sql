
ALTER TABLE user_doc ADD user_id INT(11) NOT NULL AFTER doc_id;
ALTER TABLE user_doc ADD doc_hashname VARCHAR(255) AFTER doc_filename;
ALTER TABLE user_doc ADD doc_url VARCHAR(255) AFTER doc_hashname;