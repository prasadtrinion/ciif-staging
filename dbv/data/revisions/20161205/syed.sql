ALTER TABLE `user`
ADD COLUMN  `gender` tinyint(1) DEFAULT NULL COMMENT ' 0 - male ; 1 - female;',
ADD COLUMN  `race` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'def : external_datatype = 7',
ADD COLUMN  `religion` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'def : external_datatype = 8',
