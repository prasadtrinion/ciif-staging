ALTER TABLE `content_block_data`
ADD `grading_method` tinyint(4) NULL DEFAULT '0' COMMENT '0=None, 1=Highest, 2=Latest';