CREATE TABLE `chat` (
  `chat_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'global' COMMENT 'global, course, personal',
  `user_id` int(11) unsigned NOT NULL COMMENT 'fk user',
  `to_user_id` int(11) unsigned DEFAULT '0' COMMENT 'fk user',
  `course_id` int(11) unsigned DEFAULT '0' COMMENT 'fk courses',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'text only no HTML',
  `date_time` datetime NOT NULL COMMENT 'timestamp',
  PRIMARY KEY (`chat_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;