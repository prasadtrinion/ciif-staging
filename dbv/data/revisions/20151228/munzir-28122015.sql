ALTER TABLE `curriculum` ADD `accessperiod` varchar(50) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `display_only`;
ALTER TABLE `courses` ADD `accessperiod` varchar(50) COLLATE 'utf8mb4_unicode_ci' NULL;
ALTER TABLE `enrol` ADD `expiry_date` date NULL;