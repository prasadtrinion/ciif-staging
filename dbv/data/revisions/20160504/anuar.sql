ALTER TABLE content_block_data
  ADD arrangement INT(11) NOT NULL DEFAULT '1'
  COMMENT 'display order'
    AFTER trackable
