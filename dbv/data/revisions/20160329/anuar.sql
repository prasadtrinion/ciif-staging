
ALTER TABLE `enrol` ADD `migrated` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0=local, 1=migrated';
ALTER TABLE `enrol` ADD `migrate_date` datetime DEFAULT NULL;
ALTER TABLE `enrol` ADD `completed_date` date NULL AFTER completed;


CREATE TABLE `bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookmark_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bookmark_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bookmark_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `data_id` int(11) unsigned DEFAULT '0' COMMENT 'fk content_block_data, or NULL if external',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `data_id` (`data_id`),
  CONSTRAINT `bookmark_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `bookmark_ibfk_2` FOREIGN KEY (`data_id`) REFERENCES `content_block_data` (`data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `user_group_mapping` (
  `map_id` int(11) NOT NULL AUTO_INCREMENT,
  `map_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '''announcements'', ''calendar'', ''forum'', ''content''',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `course_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`map_id`),
  KEY `group_id` (`group_id`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `user_group_mapping_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `user_group` (`group_id`),
  CONSTRAINT `user_group_mapping_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;