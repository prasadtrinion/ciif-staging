ALTER TABLE `tbl_graduation_guests`
CHANGE `payment_status` `payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Not Paid 1:Paid' AFTER `invoice_id`;

ALTER TABLE `tbl_graduation_guests`
CHANGE `applied_role` `applied_role` varchar(20) NULL AFTER `applied_by`;

ALTER TABLE `tbl_graduation_collection`
CHANGE `payment_status` `payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Not Paid 1:Paid' AFTER `gc_invoice_id`;