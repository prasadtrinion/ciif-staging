-- Adminer 4.2.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `data_wiki`;
CREATE TABLE `data_wiki` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data_id` int(11) unsigned NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `data_id` (`data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `data_wiki` (`id`, `data_id`, `title`, `content`) VALUES
(1,	69,	'Assignment 1',	'<p>Discuss shariah principles in islamic banking .</p>\r\n'),
(7,	236,	'dsss',	'dsdsd'),
(8,	237,	'1234test wiki',	'<p>wiki123 wiki wiki wkikii</p>\r\n'),
(9,	238,	'This is wiki content',	'<p><strong>Content of wiki</strong></p>\r\n\r\n<h2 style=\"font-style:italic\"><strong>tAAAAAAAAAAAAAAAAAAAAAAAA</strong></h2>\r\n\r\n<p><strong>DASDADASDSADAS</strong></p>\r\n\r\n<p><strong>DASDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD</strong></p>\r\n\r\n<p><strong>DASDASDASDASDASDDASD</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><img alt=\"\" src=\"https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png\" style=\"height:184px; width:544px\" /></strong></p>\r\n');

DROP TABLE IF EXISTS `forum`;
CREATE TABLE `forum` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` bigint(11) unsigned NOT NULL,
  `parent_id` bigint(11) unsigned NOT NULL DEFAULT '0',
  `category_id` bigint(11) unsigned NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visibility` tinyint(5) unsigned NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `display_only` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `forum` (`id`, `course_id`, `parent_id`, `category_id`, `name`, `visibility`, `created_by`, `created_date`, `modified_by`, `modified_date`, `display_only`) VALUES
(23,	10,	0,	0,	'This is new topic',	1,	1,	'2016-03-11 00:48:10',	NULL,	NULL,	0),
(24,	10,	0,	0,	'test',	1,	1,	'2016-03-11 01:04:13',	NULL,	NULL,	0),
(25,	10,	0,	0,	'test',	1,	1,	'2016-03-11 01:06:19',	NULL,	NULL,	0),
(26,	10,	0,	0,	'test',	1,	1,	'2016-03-11 01:06:32',	NULL,	NULL,	0),
(27,	10,	0,	0,	'test',	1,	1,	'2016-03-11 01:06:45',	NULL,	NULL,	0),
(28,	10,	0,	0,	'test',	1,	1,	'2016-03-11 01:07:30',	NULL,	NULL,	0),
(29,	10,	0,	0,	'test',	1,	1,	'2016-03-11 01:08:13',	NULL,	NULL,	0),
(30,	10,	0,	0,	'test',	1,	1,	'2016-03-11 01:08:28',	NULL,	NULL,	0),
(31,	10,	0,	0,	'test',	1,	1,	'2016-03-11 01:16:16',	NULL,	NULL,	0);

DROP TABLE IF EXISTS `forum_category`;
CREATE TABLE `forum_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_safe` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_safe` (`name_safe`),
  KEY `parent_id` (`parent_id`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `forum_category` (`id`, `parent_id`, `name`, `name_safe`, `description`, `thumbnail`, `created_by`, `created_date`, `modified_by`, `modified_date`, `active`) VALUES
(5,	0,	'Banking',	'banking',	'Banking programmes',	'',	1,	'2015-10-26 08:23:11',	NULL,	NULL,	1),
(6,	0,	'Takaful',	'takaful',	'Takaful courses',	'',	1,	'2015-10-26 08:23:31',	1,	'2015-10-26 11:04:13',	1),
(7,	0,	'Capital Market',	'capital-market',	'Capital Market courses',	'',	1,	'2015-10-26 08:23:45',	NULL,	NULL,	1),
(8,	0,	'Wealth Management',	'wealth-management',	'Wealth Management courses',	'',	1,	'2015-10-26 08:24:01',	NULL,	NULL,	1),
(10,	0,	'Core',	'core',	'Core courses',	'',	1,	'2015-10-26 15:10:04',	NULL,	NULL,	1);

DROP TABLE IF EXISTS `forum_reply`;
CREATE TABLE `forum_reply` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `thread_id` int(11) unsigned NOT NULL,
  `message` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reply_content` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `forum_reply` (`id`, `thread_id`, `message`, `reply_content`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(49,	22,	'RE : This is new topic',	'<p>REPLY</p>\r\n',	1,	'2016-03-11 00:49:24',	NULL,	NULL);

DROP TABLE IF EXISTS `forum_thread`;
CREATE TABLE `forum_thread` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) unsigned NOT NULL DEFAULT '0',
  `thread_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thread_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `reply_count` int(11) DEFAULT '0',
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`forum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `forum_thread` (`id`, `forum_id`, `thread_subject`, `thread_content`, `reply_count`, `created_by`, `created_date`, `modified_by`, `modified_date`, `active`) VALUES
(22,	23,	'This is new topic',	'<p>THIS IS</p>\r\n',	1,	1,	'2016-03-11 00:48:10',	NULL,	NULL,	1),
(23,	24,	'test',	'<p>test</p>\r\n',	0,	1,	'2016-03-11 01:04:13',	NULL,	NULL,	1),
(24,	25,	'test',	'<p>test</p>\r\n',	0,	1,	'2016-03-11 01:06:19',	NULL,	NULL,	1),
(25,	26,	'test',	'<p>test</p>\r\n',	0,	1,	'2016-03-11 01:06:32',	NULL,	NULL,	1),
(26,	27,	'test',	'<p>test</p>\r\n',	0,	1,	'2016-03-11 01:06:45',	NULL,	NULL,	1),
(27,	28,	'test',	'<p>test</p>\r\n',	0,	1,	'2016-03-11 01:07:30',	NULL,	NULL,	1),
(28,	29,	'test',	'<p>test</p>\r\n',	0,	1,	'2016-03-11 01:08:13',	NULL,	NULL,	1),
(29,	30,	'test',	'<p>test</p>\r\n',	0,	1,	'2016-03-11 01:08:28',	NULL,	NULL,	1),
(30,	31,	'test',	'<p>test</p>\r\n',	0,	1,	'2016-03-11 01:16:16',	NULL,	NULL,	1);

-- 2016-03-11 01:22:08
