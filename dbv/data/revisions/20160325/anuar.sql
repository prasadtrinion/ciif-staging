ALTER TABLE `enrol` ADD `migrated` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0=local, 1=migrated';
ALTER TABLE `enrol` ADD `migrate_date` datetime DEFAULT NULL;
