ALTER TABLE `user`
ADD `address1` varchar(255) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `contactno`,
ADD `address2` varchar(255) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `address1`,
ADD `postcode` varchar(10) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `address2`,
ADD `city` int NULL AFTER `postcode`,
ADD `state` int NULL AFTER `city`,
ADD `country` int NULL AFTER `state`;

CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `external_id` int(11) NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `state` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) NOT NULL,
  `code` varchar(20) NULL,
  `country_id` varchar(20) NOT NULL,
  `external_id` int NOT NULL,
  `external_country_id` int NOT NULL,
  `modified_date` datetime NULL
) ENGINE='InnoDB';

CREATE TABLE `city` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) NOT NULL,
  `code` varchar(20) NULL,
  `state_id` int NOT NULL,
  `external_id` int NOT NULL,
  `external_state_id` int NOT NULL,
  `modified_date` datetime NULL
) ENGINE='InnoDB';