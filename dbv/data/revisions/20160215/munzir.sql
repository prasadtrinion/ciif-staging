ALTER TABLE `data_scorm`
CHANGE `title` `title` varchar(255) COLLATE 'utf8mb4_unicode_ci' NOT NULL AFTER `data_id`,
CHANGE `url` `url` varchar(255) COLLATE 'utf8mb4_unicode_ci' NOT NULL AFTER `title`;