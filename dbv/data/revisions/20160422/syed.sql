DROP TABLE IF EXISTS `data_assignments`;
CREATE TABLE `data_assignments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) unsigned DEFAULT '0',
  `user_id` int(11) unsigned DEFAULT '0',
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `time_start` time NOT NULL DEFAULT '00:00:00',
  `time_end` time NOT NULL DEFAULT '00:00:00',
  `visibility` tinyint(5) unsigned NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `owner` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0=personal [user_id],1=course [course_id]',
  `status` tinyint(1) unsigned NOT NULL COMMENT '0=No submission 1=Submitted for grading Graded',
  `grade` int(11) unsigned NOT NULL,
  `online_text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_id` int(11) NOT NULL COMMENT 'from table datacontent',
  `file_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `visibility` (`visibility`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `content_block_data`
 ADD  `date_start` date NOT NULL,
 ADD  `date_end` date NOT NULL,
 ADD  `time_start` time NOT NULL,
 ADD  `time_end` time NOT NULL ;

