ALTER TABLE `curriculum`
ADD `excerpt` varchar(255) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `description`;