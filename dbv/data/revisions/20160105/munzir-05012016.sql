ALTER TABLE `curriculum`
ADD `allow_course_registration` tinyint(1) unsigned NULL DEFAULT '0';

ALTER TABLE `curriculum_courses`
ADD INDEX `curriculum_id` (`curriculum_id`),
ADD INDEX `course_id` (`course_id`);