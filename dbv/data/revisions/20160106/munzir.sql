ALTER TABLE `courses`
ADD `learning_online` tinyint(1) unsigned NOT NULL DEFAULT '1',
ADD `learning_ftf` tinyint(1) unsigned NOT NULL DEFAULT '1' AFTER `learning_online`;