CREATE TABLE `curriculum_courses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `curriculum_id` int(11) unsigned NOT NULL,
  `course_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci