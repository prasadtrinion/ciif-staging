CREATE TABLE `options` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `opt_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opt_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `opt_default` longtext COLLATE utf8mb4_unicode_ci COMMENT 'default value',
  `autoload` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `opt_name` (`opt_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci