CREATE TABLE `curriculum` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `visibility` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1: show; 0 hidden; ',
  `display_only` tinyint(1) DEFAULT '0',
  `external_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'external use',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci