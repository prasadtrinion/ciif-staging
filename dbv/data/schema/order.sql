CREATE TABLE `order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currencycode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'MYR',
  `paymentmethod` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'MIGS, FPX',
  `status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PENDING, ACTIVE, CANCELLED',
  `invoice_id` int(10) unsigned NOT NULL,
  `invoice_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_external` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `regtype` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `payment_id` int(11) unsigned DEFAULT NULL,
  `external_id` int(11) unsigned DEFAULT NULL COMMENT 'eg. IdStudentRegistration external',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci