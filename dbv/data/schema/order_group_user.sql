CREATE TABLE `order_group_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) unsigned NOT NULL,
  `order_id` int(11) unsigned NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `nationality` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` int(10) unsigned DEFAULT NULL,
  `company_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` int(10) unsigned DEFAULT NULL,
  `activated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `activated_date` datetime DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'userid once activated',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci