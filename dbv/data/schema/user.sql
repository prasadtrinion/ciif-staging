CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `register_date` datetime NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `token` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastlogin` datetime NOT NULL,
  `modifiedby` int(11) unsigned DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `modifiedrole` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthdate` date DEFAULT NULL COMMENT 'ibfim',
  `nationality` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ibfim',
  `company` int(10) unsigned DEFAULT NULL COMMENT 'ibfim',
  `company_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ibfim',
  `qualification` int(10) unsigned DEFAULT NULL COMMENT 'ibfim',
  `contactno` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `external_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci