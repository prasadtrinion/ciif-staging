CREATE TABLE `cron_service` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `results` mediumtext COLLATE utf8mb4_unicode_ci COMMENT 'if any results needed',
  `created_date` datetime NOT NULL,
  `executed_date` datetime DEFAULT NULL,
  `executed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `execution_time` float unsigned DEFAULT NULL,
  `inprogress` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recurring` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recurring_day` tinyint(2) unsigned DEFAULT NULL,
  `recurring_lastexecuted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci