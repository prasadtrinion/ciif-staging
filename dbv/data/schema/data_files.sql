CREATE TABLE `data_files` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(11) unsigned NOT NULL,
  `file_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` bigint(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_type` (`file_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci