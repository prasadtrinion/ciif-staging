CREATE TABLE `user_scorm` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL COMMENT 'link to data_scorm',
  `user_id` int(11) NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` smallint(5) NOT NULL COMMENT 'in seconds',
  `attempts` smallint(5) unsigned NOT NULL DEFAULT '0',
  `score` smallint(3) unsigned DEFAULT NULL,
  `lastaccessed` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci