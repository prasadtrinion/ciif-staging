<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once('common_functions.php');

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initAutoload() {
		
		$moduleLoader = new Zend_Application_Module_Autoloader(array (
				'namespace'	=> 'App_'	,
				'basePath'	=> APPLICATION_PATH));
		
		return $moduleLoader;
	}

	protected function _initViewHelpers() {

		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();

		$view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
	    $view->addHelperPath ( 'Zend/Dojo/View/Helper/', 'Zend_Dojo_View_Helper' );

		$view->doctype ('HTML5');
		$view->headMeta()->appendHttpEquiv ('Content-Type','text/html;charset=utf-8');
		$view->headMeta()->appendHttpEquiv ('X-UA-Compatible','IE=edge,chrome=1');
		$view->headMeta()->appendHttpEquiv ('viewport','width=device-width, initial-scale=1');
		$view->headMeta()->appendHttpEquiv ('Cache-control','no-cache');
		$view->headMeta()->appendHttpEquiv ('Pragma','no-cache');
		$view->headTitle()->setSeparator (' - ');
		$view->headTitle(APPLICATION_ENTERPRISE_SHORT ." - ". APPLICATION_TITLE_SHORT);

		$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewRenderer->setView($view);

        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

		return $view;
	}

	protected function setconstants($constants){
        foreach ($constants as $key=>$value){
            if(!defined($key)){
                define($key, $value);
            }
        }
	}
	
	protected function _initTranslate(){
		$registry = Zend_Registry::getInstance();	
		
		 // Create Session block and save the locale
        $session = new Zend_Session_Namespace('session');  
       
		    	
		$locale = new Zend_Locale('en_US');		
		$file = APPLICATION_PATH . DIRECTORY_SEPARATOR .'languages'. DIRECTORY_SEPARATOR . "en_US.php";
			
						
		$translate = new Zend_Translate('array',
            $file, $locale,
            array(
            'disableNotices' => true,    // This is a very good idea!
            'logUntranslated' => false,  // Change this if you debug
            )
        );
        
		        
        $registry->set('Zend_Locale', $locale);
        $registry->set('Zend_Translate', $translate);
              
        
        return $registry;
	}
	
	protected function _initPlugin(){
		$fc = Zend_Controller_Front::getInstance();
        $fc->registerPlugin(new icampus_Plugin_LangSelector());        
	}
	
	
	protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set('config', $config);
    }
    
	protected function _initLoadAclIni ()
	{
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/acl.ini');
		Zend_Registry::set('acl', $config);
	}

	protected function _initEvents()
	{
		$events = new Zend_EventManager_EventManager();
		Zend_Registry::set('Events', $events);
	}
	
	protected function _initAuth() 
	{
		/*
         * ACL
         */

		//All Module & Controller
		$acl = array();
    	$front = Zend_Controller_Front::getInstance();
		foreach ($front->getControllerDirectory() as $module => $path) {

                foreach (scandir($path) as $file) {

                        if (strstr($file, "Controller.php") !== false) {

                                include_once $path . DIRECTORY_SEPARATOR . $file;

                                foreach (get_declared_classes() as $class) {

                                        if (is_subclass_of($class, 'Zend_Controller_Action')) {

                                                $controller = strtolower(substr($class, 0, strpos($class, "Controller")));
                                                $actions = array();

                                                foreach (get_class_methods($class) as $action) {

                                                        if (strstr($action, "Action") !== false) {
                                                        		$action = str_replace("Action", "", $action);
                                                                $actions[] = $action;
                                                        }
                                                }
                                        }
                                }

                                $acl[$module][$controller] = $actions;
                        }
                }
   		}
   		
   		
   		
        $acl = new icampus_Plugin_Acl($acl);
		
		/*
		 * AUTH
		 */
		
		$auth = Zend_Auth::getInstance();
        $fc = Zend_Controller_Front::getInstance();
        
        //$fc->registerPlugin(new icampus_Plugin_Auth($auth,null));
        $fc->registerPlugin(new icampus_Plugin_Auth( $auth, $acl ));

	}
	
	protected function _initACLLayout(){
		$auth = Zend_Auth::getInstance();
		
		if ($auth->hasIdentity()) {
			$front = Zend_Controller_Front::getInstance(); 
        	$front->registerPlugin(new icampus_Plugin_Layout()); 

			//whos online
			$front->registerPlugin(new icampus_Plugin_UserOnline()); 
		}
	}

	protected function _initPlugins()
	{
		$path = implode(PATH_SEPARATOR, array(
			realpath(APPLICATION_PATH . '/../library/Plugins/App')
		));


		$dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
		$classes = array();
		foreach ( $dir as $file )
		{
			if ($file->isDir()) continue;

			include_once $file;
			$class = get_declared_classes();

			$classes[] = end($class);
		}

		//
		if ( !empty($classes) )
		{
			$front = Zend_Controller_Front::getInstance();
			foreach ( $classes as $class_name ) {
				$front->registerPlugin(new $class_name());
			}
		}
	}


	protected function _initRoute()
	{
		$router  = Zend_Controller_Front::getInstance()->getRouter();


		//dashboard
		$router->addRoute('dashboard', new Zend_Controller_Router_Route(
			'dashboard',
			array('module' => 'default', 'controller' => 'user', 'action' => 'dashboard')
		));

		//login
		$router->addRoute('login', new Zend_Controller_Router_Route(
			'login',
			array('module' => 'default', 'controller' => 'index', 'action' => 'login')
		));

		//logout
		$router->addRoute('logout', new Zend_Controller_Router_Route(
			'logout',
			array('module' => 'default', 'controller' => 'index', 'action' => 'logout')
		));

		//register
		$router->addRoute('register', new Zend_Controller_Router_Route(
			'register',
			array('module' => 'default', 'controller' => 'index', 'action' => 'register')
		));


		//course learn
		$router->addRoute('course_learn', new Zend_Controller_Router_Route(
			'courses/:course_id/learn',
			array('module' => 'default', 'controller' => 'courses', 'action' => 'learn')
		));

		//course with cur
		$router->addRoute('course_hascur', new Zend_Controller_Router_Route(
			'courses/:course_id/pid/:pid',
			array('module' => 'default', 'controller' => 'courses', 'action' => 'view')
		));

		//course overview
		$router->addRoute('course_view', new Zend_Controller_Router_Route(
			'courses/:course_id',
			array('module' => 'default', 'controller' => 'courses', 'action' => 'view')
		));



    }

	protected function _initCache()
	{
		$frontend= array(
							'lifetime' => 7200,
							'automatic_serialization' => true
		);

		$backend= array(
							'cache_dir' => DOCUMENT_PATH.'/cache/',
		);

		if ( ! Cms_Common::mkdir_p( DOCUMENT_PATH .'/cache' ) )
		{
			throw new Exception('Cannot create cache folder: '.DOCUMENT_PATH.'/cache' );
		}

		$cache = Zend_Cache::factory('core',
										'File',
										$frontend,
										$backend
		);

		Zend_Registry::set('cache',$cache);
	}

}

