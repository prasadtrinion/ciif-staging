<?php
class Studentfinance_Model_DbTable_CreditNote extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'credit_note';
	protected $_primary = "cn_id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.cn_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = cn.cn_approver', array('approver_name'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
					->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = cn.cn_cancel_by', array('canceler_name'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"));
		
		if($id!=0){
			$selectData->where("cn.cn_id = '".$id."'");
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	public function getDataByInvoice($invoice_no,$active=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->where('cn.cn_billing_no = ?', $invoice_no);

		if($active!=null){
			if($active){
				$selectData->where('cn.cn_cancel_date is null');
			}else{
				$selectData->where('cn.cn_cancel_date is not null');
			}
		}
		$row = $db->fetchRow($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;	
		}
	}
	
	public function getAllDataByInvoice($invoice_no){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->where('cn.cn_billing_no = ?', $invoice_no);

		$row = $db->fetchAll($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;	
		}
	}
	
	public function getPaginateData($approve=null, $cancel=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				->from(array('cn'=>$this->_name))
				->join(array('ap'=>'applicant_profile'),'ap.appl_id = cn.appl_id', array("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname)name"))
				->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.cn_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
				->joinLeft(array('tu2'=>'tbl_user'),'tu.iduser = cn.cn_approver', array('approver_name'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
				->joinLeft(array('tu3'=>'tbl_user'),'tu.iduser = cn.cn_cancel_by', array('canceler_name'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"));
	
		if(isset($approve)){
			if($approve==false){
				$select->where("cn.cn_approver is null");
				$select->where("cn.cn_approve_date is null");
			}else
			if($approve==true){
				$select->where("cn.cn_approver is not null");
				$select->where("cn.cn_approve_date is not null");
			}
		}
				
		if(isset($cancel)){				
			if(!$cancel){
				$select->where("cn.cn_cancel_by is null");
				$select->where("cn.cn_cancel_date is null");
			}else 
			if($cancel){
				$select->where("cn.cn_cancel_by is not null");
				$select->where("cn.cn_cancel_date is not null");
			}
		}
				
		return $select;
	}
	
	public function getPaginateDataHistory($approve=null, $cancel=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				->from(array('cn'=>$this->_name))
				->join(array('ap'=>'applicant_profile'),'ap.appl_id = cn.appl_id', array("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname)name"))
				->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.cn_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
				->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = cn.cn_approver', array('approver_name'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
				->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = cn.cn_cancel_by', array('canceler_name'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"));
	
		if(isset($approve)){
			if($approve==false){
				$select->where("cn.cn_approver = null");
				$select->where("cn.cn_approve_date = null");
			}else
			if($approve==true){
				$select->where("cn.cn_approver is not null");
				$select->where("cn.cn_approve_date is not null");
			}
		}

		$select2 = $db->select()
				->from(array('cn'=>$this->_name))
				->join(array('ap'=>'applicant_profile'),'ap.appl_id = cn.appl_id', array("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname)name"))
				->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.cn_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
				->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = cn.cn_approver', array('approver_name'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
				->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = cn.cn_cancel_by', array('canceler_name'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"));
				
		if(isset($cancel)){				
			if(!$cancel){
				$select2->where("cn.cn_cancel_by = null");
				$select2->where("cn.cn_cancel_date = null");
			}else 
			if($cancel){
				$select2->where("cn.cn_cancel_by is not null");
				$select2->where("cn.cn_cancel_date is not null");
			}
		}
		
		$selectUnion = $db
							->select()
							->union(array($select, $select2))
							->order('cn_create_date desc');

		return $selectUnion;
	}
	
}

