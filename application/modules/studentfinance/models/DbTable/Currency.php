<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */

class Studentfinance_Model_DbTable_Currency extends Zend_Db_Table_Abstract {
	
	/**
	 * The default table name
	 */
	protected $_name = 'tbl_currency';
	protected $_primary = "cur_id";
	
	public function getCurrencyFromCode($code){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('c'=>$this->_name))
						->where('c.cur_code = ?', $code);
		
		$row = $db->fetchRow($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('c'=>$this->_name))
						->where('c.cur_id = ?', $id);
		
		$row = $db->fetchRow($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	public function getDefaultCurrency(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('c'=>$this->_name))
						->where("c.cur_default = 'Y'");
		
		$row = $db->fetchRow($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
}
?>