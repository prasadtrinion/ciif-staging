<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_FeeCountry extends Zend_Db_Table_Abstract {

	/**
	 * The default table name
	 */
	protected $_name = 'fee_item_country';
	protected $_primary = "fic_id";
	
	
	public function getItemData($fee_id, $idCountry, $cur){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fsp'=>$this->_name))
					->join(array('fi'=>'fee_item'),'fi.fi_id = fsp.fic_fi_id')
					->where("fsp.fic_fi_id= ?", $fee_id)
					->where("fsp.fic_idCountry = ?", $idCountry)
					->where('fsp.fic_cur_id = ?',$cur);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getFeeByCountry($fee_id, $cur){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fic'=>$this->_name))
					->join(array('c'=>'tbl_countries'),'c.idCountry=fic.fic_idCountry',array('CountryName'))
					->where("fic.fic_fi_id= ?", $fee_id)
					->where('fic.fic_cur_id = ?',$cur)
					->order('c.CountryName ASC')
					->group('fic.fic_idCountry');

		$row = $db->fetchAll($selectData);				
		return $row;
	}
	
	public function getFeeStateByCountry($fee_id, $idCountry, $cur){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fis'=>'fee_item_state'))
					->join(array('s'=>'tbl_state'),'s.idState=fis.fic_idState',array('StateName'))
					->where("fis.fic_fi_id= ?", $fee_id)
					->where("fis.fic_idCountry = ?", $idCountry)
					->where('fis.fic_cur_id = ?',$cur);

		$row = $db->fetchAll($selectData);				
		return $row;
	}
	
}