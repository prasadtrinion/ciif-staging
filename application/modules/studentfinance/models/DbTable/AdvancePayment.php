<?php
class Studentfinance_Model_DbTable_AdvancePayment extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'advance_payment';
	protected $_primary = "advpy_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ap'=>$this->_name))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ap.advpy_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->where("ap.advpy_id = ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	

	/*
	 * Overite Insert function
	 */
	
	public function insert(array $data){
		
		$auth = Zend_Auth::getInstance();
		
		if(!isset($data['advpy_creator'])){
			$data['advpy_creator'] = 1;
		}
		
		$data['advpy_create_date'] = date('Y-m-d H:i:s');
			
		return parent::insert( $data );
	}
	
	/*
	 * Get Advance payment with balance
	 */
	public function getBalanceAvdPayment($fomulir){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ap'=>$this->_name))
					->where("ap.advpy_fomulir = ?",$fomulir)
					->where("ap.advpy_total_balance > 0");

		$row = $db->fetchAll($selectData);				
		
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	public function getApplicantBalanceAvdPayment($appl_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ap'=>$this->_name))
					->joinLeft(array('pmt'=>'payment_main'), 'pmt.id = ap.advpy_payment_id')
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ap.advpy_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->where("ap.advpy_appl_id = ?",$appl_id)
					->where("ap.advpy_total_balance !='0.00'");

		$row = $db->fetchAll($selectData);				
		
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}

	/*
	 * Get applicant advance payment
	 */
	
	public function getApplicantAvdPayment($transID,$type=0){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ap'=>$this->_name))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ap.advpy_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->where("ap.advpy_status = 'A'");
					
		if($type == 1){
			$selectData->where("ap.advpy_trans_id  = ?",$transID);
		}elseif($type == 2){
			$selectData->where("ap.advpy_idStudentRegistration  = ?",$transID);
		}
					

		$row = $db->fetchAll($selectData);
                //var_dump($type);
		//echo $selectData; exit;
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	/*
	 * Get Sum Advance payment
	*/
	public function getSumAvdPayment($fomulir){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('ap'=>$this->_name), array('total_amount' => new Zend_Db_Expr('SUM(ap.advpy_amount)')))
		->where("ap.advpy_fomulir = ?",$fomulir);
	
		$row = $db->fetchRow($selectData);
		
	
	
		if(!$row){
			return null;
		}else{
			return $row['total_amount'];
		}
	}
	
	/*
	 * Get advance payment transfered from invoice paid
	 */
	public function getAdvancePaymentFromInvoice($invoice_id){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
			->from(array('ap'=>$this->_name))
			->joinLeft(array('u'=>'tbl_user'), 'u.iduser = ap.advpy_creator', array())
			->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
			->where("ap.advpy_invoice_id = ?",$invoice_id);
	
		$row = $db->fetchRow($selectData);
	
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
}
?>