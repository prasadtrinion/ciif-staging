<?php
class Studentfinance_Model_DbTable_AdvancePaymentDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'advance_payment_detail';
	protected $_primary = "advpydet_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('ap'=>$this->_name))
					->where("ap.advpy_id ?", (int)$id);

		$row = $db->fetchRow($selectData);				
		return $row;
	}

	
	/*
	 * Overite Update function
	 */
/*	public function update($data=null){
		
		return null;
	}*/
	
	public function getDataDetail($parent_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('apd'=>$this->_name))
					->joinLeft(array('im'=>'invoice_main'), 'im.bill_number = apd.advpydet_bill_no')
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = im.creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
					->where("apd.advpydet_advpy_id = ?", (int)$parent_id);

		$row = $db->fetchAll($selectData);				
		return $row;
	}
	
	public function getAllBillPayment($billing_no){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
		->from(array('apd'=>$this->_name))
		->joinLeft(array('ap'=>'advance_payment'), 'ap.advpy_id = apd.advpydet_advpy_id')
		->joinLeft(array('im'=>'invoice_main'), 'im.id = ap.advpy_invoice_id')
		->joinLeft(array('u'=>'tbl_user'), 'u.iduser = im.creator', array())
		->joinLeft(array('ts'=>'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name'=>'FullName'))
		->where("ap.advpy_invoice_id = ?", (int)$billing_no);
		
		$row = $db->fetchAll($selectData);
		
		if(!$row){
			return null;
		}else{
			return $row;
		}
		
	}
}
?>