<?php
class Studentfinance_Model_DbTable_Scholarship extends Zend_Db_Table_Abstract 
{
	protected $_name = 'sponsor_application';
	protected $_detl = 'sponsor_application_details';
		
	public function getData($id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
                    ->from(array('pm'=>$this->_name))
                    ->join(array('b'=>$this->_detl),'b.application_id=pm.sa_id')
                    ->joinLeft(array('c'=>'tbl_definationms'), 'b.currency = c.idDefinition', array('currencyName'=>'c.DefinitionDesc'))
                    ->joinLeft(array('d'=>'tbl_qualificationmaster'), 'b.edulevel = d.IdQualification', array('d.QualificationLevel'))
                    ->joinLeft(array('e'=>'tbl_studentregistration'),'pm.sa_cust_id=e.IdStudentRegistration')
                    ->joinLeft(array('f'=>'tbl_semestermaster'), 'pm.sa_semester_id = f.IdSemesterMaster')
                    ->where("pm.sa_id = ?", (int)$id);
		
	
		$row = $db->fetchRow($selectData);				
		return $row;
	}
        
        public function updateData($data, $id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $update = $db->update('sponsor_application', $data, 'sa_id = '.$id);
            return $update;
        }

	public function getDataByStudent($student_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select()
					->from(array('a' => $this->_name))
					->where('a.sa_cust_id = ?', $student_id)
					->where('a.sa_cust_type = ?', 1)
                                        ->where('a.sa_status <> ?', 6)
                                        ->order('a.sa_id DESC');

		$row = $db->fetchRow($select);

		return $row;
	}

	public function addData($data)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$db->insert($this->_name, $data );
		
		$_id = $db->lastInsertId();

		return $_id;
	}

	public function addDetails($data)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$db->insert($this->_detl, $data );
		
		$_id = $db->lastInsertId();

		return $_id;
	}

        public function getProgramList(){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_program'), array('value'=>'*'))
                ->order('a.seq_no ASC')
                ->order('a.ProgramName ASC');
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        static function getCity($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_city'), array('CityName'))
                ->where('a.idCity = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
        
        static function getState($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_state'), array('StateName'))
                ->where('a.idState = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
        
        static function getCountry($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_countries'), array('CountryName'))
                ->where('a.idCountry = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
        
        static function getDefination($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_definationms'), array('DefinitionDesc'))
                ->where('a.idDefinition = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
        
        public function uploadfile($data){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->insert('sponsor_application_upload', $data);
            //var_dump($data); exit;
            return true;
        }
        
        public function viewUpl($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $selectData = $db->select()
                ->from(array('pm'=>'sponsor_application_upload'))
                ->where("pm.sau_sa_id = ?", (int)$id);

            $row = $db->fetchAll($selectData);				
            return $row;
	}
        
        public function deleteUpl($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $delete = $db->delete('sponsor_application_upload', 'sau_id = '.$id);
            return $delete;
        }
        
        public function getLevelEducationList($type = 607){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_qualificationmaster'), array('value'=>'*'))
                ->where('a.qualification_type_id = ?', $type)
                ->order('a.QualificationRank');
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getHighestLevelEdu($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'student_qualification'), array('value'=>'*'))
                ->join(array('b'=>'tbl_qualificationmaster'), 'a.ae_qualification = b.IdQualification')
                ->joinLeft(array('c'=>'tbl_definationms'), 'a.ae_class_degree = c.idDefinition')
                ->where('b.qualification_type_id = ?', 607)
                ->where('a.sp_id = ?', $id)
                ->order('b.QualificationRank DESC');
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getHighestLevelEdu2($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_program'), array('value'=>'*'))
                ->joinLeft(array('b'=>'tbl_definationms'), 'a.Award = b.idDefinition', array('QualificationLevel'=>'b.DefinitionDesc'))
                ->where('a.IdProgram = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getCurrency(){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
                ->where('a.idDefType = ?', 178);

            $result = $db->fetchAll($select);
            return $result;
        }

        static function getCurrencyDefination($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_definationms'), array('a.DefinitionDesc'))
                ->where('a.idDefinition = ?', $id);

            $result = $db->fetchOne($select);
            return $result;
        }
        
        public function getSemester($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
                //->where('a.IsCountable = ?', 1)
                ->where('a.IdScheme = ?', $id)
                ->order('a.SemesterMainStartDate DESC');

            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getSemesterApply($schemeid, $program, $stype = 3, $schtype = 1, $apptype = 0){
            $date = date('Y-m-d');
            
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_student'), array('value'=>'*'))
                ->join(array('b'=>'tbl_semestermaster'), 'a.semester_id=b.IdSemesterMaster')
                ->join(array('c'=>'tbl_scholarship_student_program'), 'a.id=c.setup_id')
                //->where('a.IsCountable = ?', 1)
                ->where('a.stype = ?', $stype)
                ->where('a.scholarship_type = ?', $schtype)
                ->where('a.application_type = ?', $apptype)
                ->where('c.program_id = ?', $program)
                ->where('a.start_date <= ?', $date)
                ->where('a.end_date >= ?', $date)
                ->where('b.IdScheme = ?', $schemeid)
                ->order('b.SemesterMainStartDate DESC')
                ->group('b.IdSemesterMaster');

            $result = $db->fetchAll($select);
            return $result;
        }

		public function getCourseAvailabilitySemester($schemeid, $program, $semester, $stype = 3, $schtype = 1, $apptype = 0){
            $date = date('Y-m-d');
            
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_student'), array('value'=>'*'))
                ->join(array('b'=>'tbl_semestermaster'), 'a.semester_id=b.IdSemesterMaster')
                ->join(array('c'=>'tbl_scholarship_student_program'), 'a.id=c.setup_id')
                //->where('a.IsCountable = ?', 1)
                ->where('a.semester_id = ?', $semester)
                ->where('a.stype = ?', $stype)
                ->where('a.scholarship_type = ?', $schtype)
                ->where('a.application_type = ?', $apptype)
                ->where('c.program_id = ?', $program)
                ->where('a.start_date <= ?', $date)
                ->where('a.end_date >= ?', $date)
                ->where('b.IdScheme = ?', $schemeid)
                ->order('b.SemesterMainStartDate DESC')
                ->group('b.IdSemesterMaster');
			
			$result = $db->fetchAll($select);
            return $result;
        }
        
        public function getCourseAvailability($schemeid, $program, $semester, $stype = 3, $schtype = 1, $apptype = 0){
            $date = date('Y-m-d');
            
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_student'), array('value'=>'*'))
                ->join(array('b'=>'tbl_semestermaster'), 'a.semester_id=b.IdSemesterMaster')
                ->join(array('c'=>'tbl_scholarship_student_program'), 'a.id=c.setup_id')
                //->where('a.IsCountable = ?', 1)
                ->where('a.semester_id = ?', $semester)
                ->where('a.stype = ?', $stype)
                ->where('a.scholarship_type = ?', $schtype)
                ->where('a.application_type = ?', $apptype)
                ->where('c.program_id = ?', $program)
                ->where('a.course_start_date <= ?', $date)
                ->where('a.course_end_date >= ?', $date)
                ->where('b.IdScheme = ?', $schemeid)
                ->order('b.SemesterMainStartDate DESC')
                ->group('b.IdSemesterMaster');

            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getSemesterById($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
                ->where('a.IdSemesterMaster = ?', $id);

            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getIndustryWeightageList(){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_industry'), array('value'=>'*'));
            
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getListSemesterRegistered($IdStudentRegistration){
        	
            $auth = Zend_Auth::getInstance(); 
            $db = Zend_Db_Table::getDefaultAdapter();

            $date = date('Y-m-d');

            $sql = $db->select()
                ->from(array('srs'=>'tbl_studentregsubjects'),array('IdSemesterMain'))
                ->join(array('sm'=>'tbl_semestermaster'),'sm.IdSemesterMaster=srs.IdSemesterMain',array('SemesterMainName','SemesterMainCode','SemesterFunctionType','IsCountable'))
                ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
                ->where('srs.Active IN (1,4,6)')
                ->where('sm.AcademicYear < 2015')
                ->where("srs.exam_status NOT IN ('I','M')")
                ->where('sm.SemesterMainEndDate < ?',$date)
                ->order('sm.SemesterMainStartDate')
                ->group('srs.IdSemesterMain');

            $result = $db->fetchAll($sql);

            //below code was created to cater if there are subjct registered in tbl_studentregsubject but record in tbl_studentsemesterstatus 14-2-1014 yatie
            foreach($result as $index=>$semester){

                //check if studentsemesterstatus exist
                $isExist = $this->getSemesterStatusBySemester($IdStudentRegistration,$semester['IdSemesterMain']);

                if(!$isExist){
                    //create
                    $data = array(  'IdStudentRegistration' => $IdStudentRegistration,									           
                        'idSemester' => $semester['IdSemesterMain'],
                        'IdSemesterMain' => $semester['IdSemesterMain'],								
                        'studentsemesterstatus' => 130, 	//Register idDefType = 32 (student semester status)
                        'Level'=>1,
                        'UpdDate' => date ( 'Y-m-d H:i:s'),
                        'UpdUser' => $auth->getIdentity()->iduser
                    );											
                    $idstudentsemsterstatus = $this->addData2($data);
                    $result[$index]['idstudentsemsterstatus']=$idstudentsemsterstatus;
                }else{
                    $result[$index]['idstudentsemsterstatus']=$isExist['idstudentsemsterstatus'];
                }
            }
            return $result;
        }
        
        public function getSemesterStatusBySemester($IdStudentRegistration,$IdSemesterMain){
            $db = Zend_Db_Table::getDefaultAdapter();      
            $sql = $db->select()	
                ->from(array('sss' => 'tbl_studentsemesterstatus'))
                ->where('IdStudentRegistration  = ?',$IdStudentRegistration)
                ->where('IdSemesterMain = ?',$IdSemesterMain);
            $result = $db->fetchRow($sql);
            return $result;		
	}
        
        public function addData2($data){
            $db = Zend_Db_Table::getDefaultAdapter();
            $id = $db->insert('tbl_studentsemesterstatus', $data);
            return $id;
	}
        
        public function getListCourseRegisteredBySemesterWithAttendanceStatus($registrationId,$idSemesterMain){
		
            $db = Zend_Db_Table::getDefaultAdapter();

            $sql = $db->select()
                ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubName'=>'SubjectName')) 
                ->joinLeft(array('er'=>'exam_registration'),'er_idSemester=srs.IdSemesterMain AND er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idProgram=sr.IdProgram AND er.er_idSubject=srs.IdSubject AND er.er_attendance_status !=396')
                ->where('sr.IdStudentRegistration = ?', $registrationId)
                ->where('srs.IdSemesterMain = ?',$idSemesterMain)        
                // ->where('srs.subjectlandscapetype != 2')  
                // ->where('er.er_attendance_status != ?',396)//absent with valid reason no need to display              
                ->where('srs.Active = 1 OR srs.Active =4 OR srs.Active=5 OR srs.Active=6') //Status => 1:Register 4:Repeat 5:Refer
                ->where('srs.exam_status!= ?','I');  // kalo exam_status I=incomplete jgn display subject ni

            $result = $db->fetchAll($sql);
            return $result;
	}
        
        public function getListCourseRegisteredBySemesterWithoutAttendance($registrationId,$idSemesterMain){
		
            $db = Zend_Db_Table::getDefaultAdapter();

            $sql = $db->select()
                ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubName'=>'SubjectName')) 
                //->joinLeft(array('er'=>'exam_registration'),'er_idSemester=srs.IdSemesterMain AND er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idProgram=sr.IdProgram AND er.er_idSubject=srs.IdSubject')
                ->where('sr.IdStudentRegistration = ?', $registrationId)
                ->where('srs.IdSemesterMain = ?',$idSemesterMain)
                //->where('er.er_attendance_status != ?',396)        
               // ->where('srs.subjectlandscapetype != 2')                
                ->where('srs.Active = 1 OR srs.Active =4 OR srs.Active=5 or srs.Active=6')   //Status => 1:Register 4:Repeat 5:Refer
                ->where('srs.exam_status!= ?','I'); // kalo exam_status I=incomplete jgn display subject ni

            $result = $db->fetchAll($sql);
            return $result;
	}
        
        function getStudentGrade($IdStudentRegistration,$idSemester){
		
            $db =  Zend_Db_Table::getDefaultAdapter();

            $select = $db->select()
               ->from('tbl_student_grade')
               ->where('sg_IdStudentRegistration = ?',$IdStudentRegistration)
               ->where('sg_semesterId = ?',$idSemester);

            return $rowSet = $db->fetchRow($select);
		
	}
        
        public function getHeadTemplate($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_sponsor_appform'), array('value'=>'*'))
                ->where('a.type = ?', 'student')
                ->where('a.application_type = ?', 0)
                ->where('a.stype = ?', 3)
                ->where('a.sch_type = ?', 1)
                ->where('a.program_id = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getIndustryWeightage($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_industry'), array('value'=>'*'))
                ->where('a.id = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getAgeWeightage($age){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_age_weight'), array('value'=>'*'))
                ->where('"'.$age.'" BETWEEN a.from AND a.to');
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getAcademicWeightage($id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_academic_weight'), array('value'=>'*'))
                ->where('a.id = ?', $id);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getIncomeWeightage($income, $currency){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_income_weight'), array('value'=>'*'))
                ->where('"'.$income.'" BETWEEN a.from AND a.to')
                ->where('a.currency = ?', $currency);
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function getDependentWeightage($no){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_dependent_weight'), array('value'=>'*'))
                ->where('"'.$no.'" BETWEEN a.from AND a.to');
            
            $result = $db->fetchRow($select);
            return $result;
        }
        
        public function storeSubject($bind){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->insert('sponsor_application_course', $bind);
            $id = $db->lastInsertId('sponsor_application_course', 'sac_id');
            return $id;
        }
        
        public function set_status($as_id, $status, $remarks = false) {
            $where = $this->getAdapter()->quoteInto('sa_id IN (?)', $as_id);

            $data['sa_status'] = $status;
            if ($remarks != false){
                $data['sa_remarks'] = $remarks;
            }
            $this->update($data,$where);
        return(true);
    }
    
    public function getCourseCovered($semesterid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_financial_aid_course'), array('value'=>'*'))
            ->where('a.financial_aid_id = ?', 1)
            ->where('a.semester_id = ?', $semesterid);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getApplicationCourse($id, $group = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_application_course'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.sac_subjectid = b.IdSubject')
            ->where('a.sac_said = ?', $id);

        if ($group == true){
            $select->group('a.sac_subjectid');
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSubjectItem($regsubid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects_detail'), array('value'=>'*'))
            ->where('a.regsub_id = ?', $regsubid)
            ->group('a.invoice_id');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkCourse($said, $regsubid, $subid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_application_course'), array('value'=>'*'))
            ->where('a.sac_said = ?', $said)
            ->where('a.sac_regsubid = ?', $regsubid)
            ->where('a.sac_subjectid = ?', $subid);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function checkReCourse($said, $regsubid, $subid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication_course'), array('value'=>'*'))
            ->where('a.src_sreid = ?', $said)
            ->where('a.src_regsubid = ?', $regsubid)
            ->where('a.src_subjectid = ?', $subid);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function deleteSponsorSubject($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('sponsor_application_course', 'sac_id = '.$id);
        return $delete;
    }

    public function deleteSponsorReSubject($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('sponsor_reapplication_course', 'src_id = '.$id);
        return $delete;
    }

    public function getInvoiceDetails($id, $subjectid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_detail'), array('value'=>'*'))
            ->join(array('b'=>'fee_item'), 'a.fi_id = b.fi_id')
            ->join(array('c'=>'invoice_subject'), 'a.fi_id = b.fi_id AND a.id = invoice_detail_id', array('amt'=>'c.amount'))
            ->where('a.invoice_main_id = ?', $id)
            ->where('c.subject_id = ?', $subjectid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInvoiceMain($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'), array('value'=>'*'))
            ->where('a.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function financialAidCoverage($feeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_financial_aid_coverage'), array('value'=>'*'))
            ->where('a.fee_code = ?', $feeid);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function saveSponsorInvMain($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_invoice_main', $data);
        $id = $db->lastInsertId('sponsor_invoice_main', 'sp_id');
        return $id;
    }

    public function saveSponsorInvDetail($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_invoice_detail', $data);
        $id = $db->lastInsertId('sponsor_invoice_detail', 'id');
        return $id;
    }

    public function updateApplicationCourse($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_application_course', $data, 'sac_id = '.$id);
        return $update;
    }

    public function updateReApplicationCourse($data, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_reapplication_course', $data, 'src_id = '.$id);
        return $update;
    }

    public function insertReceipt($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_invoice_receipt', $data);
        $id = $db->lastInsertId('sponsor_invoice_receipt', 'rcp_id');
        return $id;
    }

    public function insertSponsorReceipt($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_invoice_receipt_invoice', $data);
        $id = $db->lastInsertId('sponsor_invoice_receipt_invoice', 'rcp_inv_id');
        return $id;
    }

    public function approveSponsorInvoive($id){
        //receipt
        $sponsorreceiptDb = new Studentfinance_Model_DbTable_SponsorReceipt();
        $receipt = $sponsorreceiptDb->getData($id);

        $receipt = $receipt[0];

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        /*
         * knockoff invoice
         */

        if ($receipt['rcp_status'] == 'ENTRY') {
            $sponsorinvoiceMainDb = new Studentfinance_Model_DbTable_SponsorInvoice();
            $sponsorinvoiceDetailDb = new Studentfinance_Model_DbTable_SponsorInvoiceDetail();
            $invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();


            foreach ($receipt['receipt_student'] as $rcpStudent) {

                $totalSponsorAmount = $rcpStudent['rcp_inv_amount'];

                $appTrxId = $rcpStudent['IdStudentRegistration'];
                //$txnStudentDb = new Registration_Model_DbTable_Studentregistration();
                $txnProfile = $this->getStudentInfoData($appTrxId);

                //get current semester
                //$semesterDb = new Records_Model_DbTable_SemesterMaster();
                $semesterInfo = $this->getStudentCurrentSemester($txnProfile);

                $totalAmount = 0.00;
                $total_payment_amount = $receipt['rcp_amount'];
                $total_paid_receipt = 0;
                $paidReceiptInvoice = 0;
                $paidReceipt = 0;

                if ($rcpStudent['receipt_invoice']) {
                    //get receipt invoice
                    $balanceNew1 = 0;
                    $paidNew1 = 0;
                    $balanceNew = 0;
                    $paidNew = 0;
                    $balance = 0;
                    $amountDN = 0;
                    foreach ($rcpStudent['receipt_invoice'] as $inv) {
                        $invID = $inv['rcp_inv_sp_id'];//sponsor_invoice_main
                        $invDetID = $inv['rcp_inv_sponsor_dtl_id'];//sponsor_invoice_detail
                        $rcpCurrency = $receipt['rcp_cur_id'];

                        $invDetData = $sponsorinvoiceDetailDb->getData($invDetID);

                        $invCurrency = $invDetData['currency_id'];

                        $currencyDb = new Studentfinance_Model_DbTable_Currency();
                        $currency = $currencyDb->fetchRow('cur_id = ' . $invCurrency)->toArray();

                        $amountDefault = $invDetData['sp_balance_det'];

                        $curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

                        $paidReceipt = $inv['rcp_inv_amount'];

                        if ($invCurrency != 1) {
                            //$dataCur = $curRateDB->getData($inv['rcp_exchange_rate']);
                            //$paidReceipt = round($inv['rcp_inv_amount'] * $dataCur['cr_exchange_rate'], 2);
                            $paidReceipt = round($inv['rcp_inv_amount'],2);
                        }

                        $balance = $amountDefault - $paidReceipt;
                        $totalSponsorAmount -= $paidReceipt;

                        //update invoice details
                        $dataInvDetail = array(
                            'sp_paid' => $paidReceipt + $invDetData['sp_paid_det'],
                            'sp_balance' => $balance,
                        );

                        $sponsorinvoiceDetailDb->update($dataInvDetail, array('id =?' => $invDetID));

                        $totalAmount += $paidReceiptInvoice;

                        //update invoice main
                        $invMainData = $sponsorinvoiceMainDb->getData($invID);

                        $balanceMain = ($invMainData['sp_balance']) - ($paidReceipt);
                        $paidMain = ($invMainData['sp_paid']) + ($paidReceipt);

                        //update invoice main
                        $dataInvMain = array(
                            'sp_paid' => $paidMain,
                            'sp_balance' => $balanceMain,
                            'sp_approve_by' => $getUserIdentity->id,
                            'sp_approve_date' => date('Y-m-d H:i:s')
                        );

                        $sponsorinvoiceMainDb->update($dataInvMain, array('sp_id =?' => $invID));
                        $invDetMainID = $inv['idDet'];
                        $invMainId = $inv['idMain'];

                        //update invoice_main & invoice_detail
                        $invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
                        $invDet = $invoiceDetailDB->getData($invDetMainID);

                        $amountDN = $inv['rcp_inv_amount'];
                        if ($invDet['cur_id'] != 1) {
                            //$dataCur = $curRateDB->getData($inv['rcp_exchange_rate']);
                            //$amountDN = round($inv['rcp_inv_amount'] * $dataCur['cr_exchange_rate'], 2);
                            $amountDN = round($inv['rcp_inv_amount'],2);
                        }

                        $balanceNew1 = $invDet['balance'] - $amountDN;
                        $paidNew1 = $invDet['paid'] + $amountDN;
                        $dataUpdDetail = array('balance' => $balanceNew1, 'sp_id' => $invDetID, 'paid' => $paidNew1);
                        $db->update('invoice_detail', $dataUpdDetail, $db->quoteInto("id = ?", $invDetMainID));

                        //update amount sponsor at invoice_main

                        $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                        $invMain = $invoiceMainDB->getData($invMainId);

                        $balanceNew = $invMain['bill_balance'] - $amountDN;
                        $paidNew = $invMain['bill_paid'] + $amountDN;

                        $dataUpdMain = array('bill_balance' => $balanceNew, 'bill_paid' => $paidNew, 'upd_by' => $creator, 'upd_date' => date('Y-m-d H:i:s'));
                        $db->update('invoice_main', $dataUpdMain, $db->quoteInto("id = ?", $invMainId));

                    }
                }

                if ($totalSponsorAmount > 0 || $rcpStudent['rcp_adv_amount'] != 0) {

                    //generetae advance payment
                    if ($rcpStudent['rcp_adv_amount'] > 0) {
                        $total_advance_payment_amount = $rcpStudent['rcp_adv_amount'];
                    } else {
                        $total_advance_payment_amount = $totalSponsorAmount;
                    }

                    $advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
                    $advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();

                    $bill_no = $this->getBillSeq(8, date('Y'));

                    $advance_payment_data = array(
                        'advpy_fomulir' => $bill_no,
                        'advpy_appl_id' => 0,
                        'advpy_trans_id' => 0,
                        'advpy_idStudentRegistration' => $rcpStudent['IdStudentRegistration'],
                        'advpy_sem_id' => $semesterInfo['IdSemesterMaster'],
                        'advpy_rcp_id' => $receipt['rcp_id'],
                        'advpy_description' => 'Advance payment from receipt no:' . $receipt['rcp_no'],
                        'advpy_cur_id' => $receipt['rcp_cur_id'],
                        'advpy_amount' => $total_advance_payment_amount,
                        'advpy_total_paid' => 0.00,
                        'advpy_total_balance' => $total_advance_payment_amount,
                        'advpy_status' => 'A',
                        'advpy_date' => date('Y-m-d'),

                    );


                    $advPayID = $advancePaymentDb->insert($advance_payment_data);

                    $advance_payment_det_data = array(
                        'advpydet_advpy_id' => $advPayID,
                        'advpydet_total_paid' => 0.00
                    );

                    $advancePaymentDetailDb->insert($advance_payment_det_data);

                    $dataAdv = array('rcp_adv_amount' => $total_advance_payment_amount);
                    $db->update('sponsor_invoice_receipt_student', $dataAdv, $db->quoteInto("rcp_inv_id = ?", $rcpStudent['rcp_inv_id']));

                }
            }

        }

        /*knockoff end*/


        //change receipt status to APPROVE
        if ($receipt['rcp_status'] == 'ENTRY') {

            foreach ($receipt['receipt_student'] as $rcpStudent) {
                if ($rcpStudent['receipt_invoice']) {
                    //get receipt invoice
                    foreach ($rcpStudent['receipt_invoice'] as $inv) {
                        $invID = $inv['idMain'];
                        $invDetMainID = $inv['idDet'];

                        if ($invID) {
                            if ($balanceNew == 0) {
                                $paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
                                $paymentClass->updateCourseStatus($invID);

                            }
                        }
                    }
                }
            }

            $sponsorreceiptDb->update(
                array(
                    'rcp_status' => 'APPROVE',
                    'UpdDate' => date('Y-m-d H:i:s'),
                    'UpdUser' => $getUserIdentity->id
                ), 'rcp_id = ' . $receipt['rcp_id']);

            return array('type' => 'success', 'message' => 'Receipt Approved');
        } else {
            return array('type' => 'error', 'message' => 'Receipt Already Approved');
        }
    }

    private function getBillSeq($type, $year)
    {

        $seq_data = array(
            $type,
            $year,
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['proforma_invoice_no'];
    }

    public function getStudentInfoData($id, $full=0) {
        $this->lobjDbAdpt = $db = Zend_Db_Table::getDefaultAdapter();

        if ( $full == 0 )
        {
            $sql = $this->lobjDbAdpt->select()
                ->from(array('a' => 'tbl_studentregistration'))
                ->join(array('StudentProfile' => 'student_profile'), 'StudentProfile.id = a.sp_id', array('StudentProfile.*'))
                //->joinLeft(array('ApplicantTransaction' => 'applicant_transaction'), 'ApplicantTransaction.at_trans_id = a.transaction_id', array('ApplicantTransaction.*'))
                ->join(array('b' => 'tbl_program'), "a.IdProgram = b.IdProgram", array("b.ProgramName", "b.IdScheme"))
                ->join(array('c' => 'tbl_intake'), "a.IdIntake = c.IdIntake", array("c.IntakeDesc"))
                ->joinLeft(array('d' => 'tbl_definationms'), "a.profileStatus = d.idDefinition", array("d.DefinitionDesc"))
                ->where("a.IdStudentRegistration =?", $id);
        }
        else
        {
            $sql = $this->lobjDbAdpt->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.*'))
                ->join(array('p'=>'student_profile'),'p.id=sa.sp_id',array('appl_fname','appl_mname','appl_lname','appl_religion'))
                ->joinLeft(array('deftn' =>'tbl_definationms'), 'deftn.idDefinition=sa.Status', array('deftn.DefinitionCode','Status','DefinitionDesc')) //Application status
                ->joinLeft(array('defination' => 'tbl_definationms'), 'defination.idDefinition=sa.profileStatus', array('profileStatus'=>'DefinitionCode')) //Application STtsu
                ->join(array('prg' => 'tbl_program'), 'prg.IdProgram=sa.IdProgram', array('ArabicName','ProgramName', "IdScheme"))
                ->join(array('intk' => 'tbl_intake'), 'intk.IdIntake=sa.IdIntake', array('intk.IntakeDesc','IntakeDefaultLanguage'))
                ->joinLeft(array('s'=>'tbl_staffmaster'),'s.IdStaff = sa.AcademicAdvisor',array('advisor'=>'Fullname'))
                ->joinLeft(array('lk'=>'sis_setup_detl'),"lk.ssd_id=p.appl_religion",array("religion"=>"ssd_name"))
                ->joinLeft(array('defination2' => 'tbl_definationms'), 'defination2.idDefinition=p.appl_category', array('stdCtgy'=>'DefinitionDesc'))
                ->joinLeft(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=sa.IdProgramScheme')
                ->joinLeft(array('e'=>'tbl_definationms'), 'ps.mode_of_program = e.idDefinition', array('mop'=>'e.DefinitionDesc','mop_my'=>'DefinitionDesc'))
                ->joinLeft(array('f'=>'tbl_definationms'), 'ps.mode_of_study = f.idDefinition', array('mos'=>'f.DefinitionDesc','mos_my'=>'DefinitionDesc'))
                ->joinLeft(array('g'=>'tbl_definationms'), 'ps.program_type = g.idDefinition', array('pt'=>'g.DefinitionDesc','pt_my'=>'DefinitionDesc'))
                ->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sa.IdLandscape')
                ->where("sa.IdStudentRegistration = ?",$id);
        }

        $result = $this->lobjDbAdpt->fetchRow($sql);

        return $result;
    }

    public function getStudentCurrentSemester($student)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()
            ->from(array('sm' => 'tbl_semestermaster'))
            ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
            ->where('sm.IdScheme = ?',(int)$student['IdProgramScheme'])
            ->where('sm.Branch = ?',(int)$student['IdBranch'])
            ->order('sm.SemesterMainStartDate desc');

        $result = $db->fetchRow($sql);

        if(!$result){
            $sql = $db->select()
                ->from(array('sm' => 'tbl_semestermaster'))
                ->where('sm.SemesterMainEndDate >= CURDATE() AND sm.SemesterMainStartDate <= CURDATE()')
                ->where('sm.IdScheme = ?',(int)$student['IdProgramScheme'])
                ->where('sm.Branch = ?',0)
                ->order('sm.SemesterMainStartDate desc');
            $result = $db->fetchRow($sql);
        }

        return $result;

    }
}
?>