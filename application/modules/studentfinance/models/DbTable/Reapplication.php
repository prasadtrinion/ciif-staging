<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Studentfinance_Model_DbTable_Reapplication extends Zend_Db_Table_Abstract {
    
    
    public function getReApplicationList($id,$semester_id=null){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.sre_cust_id=b.IdStudentRegistration')
            ->joinLeft(array('c'=>'student_profile'), 'b.sp_id=c.id')
            ->joinLeft(array('d'=>'tbl_program'), 'b.IdProgram=d.IdProgram')
            ->joinLeft(array('e'=>'tbl_semestermaster'), 'a.sre_semester_id = e.IdSemesterMaster')
            ->where('a.sre_cust_id = ?', $id)
            ->where('a.sre_cust_type = 1');
        
		if ( $semester_id != null )
		{
			$select->where('a.sre_semester_id = ?', $semester_id);
		}

        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getReApplication($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication'), array('value'=>'*'))
            ->where('a.sre_id = ?', $id)
            ->where('a.sre_cust_type = 1');
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getCity($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_city'))
            ->where('a.idCity = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getState($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_state'))
            ->where('a.idState = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getCountry($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_countries'))
            ->where('a.idCountry = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemester($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IsCountable = ?', 1)
            ->where('a.IdScheme = ?', $id)
            ->order('a.SemesterMainStartDate DESC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getSemesterApply($schemeid, $program, $stype = 3, $schtype = 1, $apptype = 1){
            $date = date('Y-m-d');
            
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from(array('a'=>'tbl_scholarship_student'), array('value'=>'*'))
                ->join(array('b'=>'tbl_semestermaster'), 'a.semester_id=b.IdSemesterMaster')
                ->join(array('c'=>'tbl_scholarship_student_program'), 'a.id=c.setup_id')
                //->where('a.IsCountable = ?', 1)
                ->where('a.stype = ?', $stype)
                ->where('a.scholarship_type = ?', $schtype)
                ->where('a.application_type = ?', $apptype)
                ->where('c.program_id = ?', $program)
                ->where('a.start_date <= ?', $date)
                ->where('a.end_date >= ?', $date)
                ->where('b.IdScheme = ?', $schemeid)
                ->order('b.SemesterMainStartDate DESC')
                ->group('b.IdSemesterMaster');

            $result = $db->fetchAll($select);
            return $result;
        }
        
    public function getCourseAvailability($schemeid, $program, $semester, $stype = 3, $schtype = 1, $apptype = 1){
        $date = date('Y-m-d');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_scholarship_student'), array('value'=>'*'))
            ->join(array('b'=>'tbl_semestermaster'), 'a.semester_id=b.IdSemesterMaster')
            ->join(array('c'=>'tbl_scholarship_student_program'), 'a.id=c.setup_id')
            //->where('a.IsCountable = ?', 1)
            ->where('a.semester_id = ?', $semester)
            ->where('a.stype = ?', $stype)
            ->where('a.scholarship_type = ?', $schtype)
            ->where('a.application_type = ?', $apptype)
            ->where('c.program_id = ?', $program)
            ->where('a.course_start_date <= ?', $date)
            ->where('a.course_end_date >= ?', $date)
            ->where('b.IdScheme = ?', $schemeid)
            ->order('b.SemesterMainStartDate DESC')
            ->group('b.IdSemesterMaster');

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getCurSem($idScheme){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('"'.date('Y-m-d').'" BETWEEN a.SemesterMainStartDate AND a.SemesterMainEndDate');
        //echo $select;
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getSemesterById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdSemesterMaster = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getLastSem($idScheme, $curSemId, $date, $branch){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.IdScheme = ?', $idScheme)
            ->where('a.IdSemesterMaster != ?', $curSemId)
            ->where('a.IsCountable = ?', 1)
            ->where('a.Branch = '.$branch.' or a.Branch = 0')
            ->where('a.SemesterMainStartDate < ?', $date)
            ->order('a.SemesterMainStartDate DESC');
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getRegSubLastSem($studentid, $semesterid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregsubjects'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.IdSubject = b.IdSubject')
            ->joinLeft(array('c'=>'exam_registration'), 'a.IdSubject = c.er_idSubject AND a.IdStudentRegistration = c.er_idStudentRegistration AND a.IdSemesterMain = c.er_idSemester')
            ->where('a.IdStudentRegistration = ?', $studentid)
            ->where('c.er_attendance_status != ?', 396)
            ->where('a.IdSemesterMain = ?', $semesterid);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getPublishResult($programId, $semesterId, $category = 0){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_publish_mark'), array('value'=>'*'))
            ->where('a.pm_idProgram = ?', $programId)
            ->where('a.pm_idSemester = ?', $semesterId)
            ->where('a.pm_category = ?', $category);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function insertSponsorData($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_reapplication', $bind);
        $id = $db->lastInsertId('sponsor_reapplication', 'sre_id');
        return $id;
    }
    
    public function insertSponsorCourse($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('sponsor_reapplication_course', $bind);
        $id = $db->lastInsertId('sponsor_reapplication_course', 'src_id');
        return $id;
    }
    
    public function getSectionC($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sponsor_appform'), array('value'=>'*'))
            ->where('a.type = ?', 'student')
            ->where('a.stype = ?', 3)
            ->where('a.application_type = ?', 1)
            ->where('a.sch_type = ?', 1)
            ->where('a.program_id = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getAllregistered($idstud){
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()
            ->distinct()
            ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
            ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
            ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode','SubName'=>'SubjectName')) 
            ->joinLeft(array('er'=>'exam_registration'),'er_idSemester=srs.IdSemesterMain AND er.er_idStudentRegistration=sr.IdStudentRegistration AND er.er_idSubject=srs.IdSubject',array("er_attendance_status"))
            ->where('sr.IdStudentRegistration = ?', $idstud)
            ->where('er.er_attendance_status <> 396 or er.er_attendance_status is null')
            ->where('srs.Active = 1 OR srs.Active =4 OR srs.Active=5 or srs.Active=6 or srs.Active=0 or srs.Active=9');   //Status => 1:Register 4:Repeat 5:Refer
        $result = $db->fetchAll($sql);

        return $result;
    }
    
    public function getRemainingCourses($program_id,$landscape_id,$subject_id){		
        $db = Zend_Db_Table::getDefaultAdapter();

        $ids = implode("','",$subject_id);

        $select = $db->select()
            ->from(array("ls"=>"tbl_landscapesubject"))
            ->joinLeft(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject',array('BahasaIndonesia','SubCode','SubjectName'=>'SubjectName','CreditHour' => 'CreditHours'))
            ->joinLeft(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array('DefinitionDesc'))
            ->joinLeft(array("pr"=>"tbl_programrequirement"),'pr.IdProgramReq=ls.IdProgramReq')
            ->joinLeft(array("lb"=>"tbl_landscapeblock"),'ls.idlevel=lb.idblock',array("block"))
            ->where("ls.IdProgram = ?",$program_id)
            ->where("ls.IdLandscape = ?",$landscape_id)
            ->where("IDProgramMajoring = 0")
            ->where("ls.IdSubject NOT IN ('$ids')")
            ->order("ls.IdSemester")
            ->order("pr.IdProgramReq")
            ->order("ls.IdLandscapeSub");

        $row = $db->fetchAll($select);
        return $row;
    }
    
    public function getGSSubject($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication_course'), array('value'=>'*'))
            ->join(array('b'=>'tbl_subjectmaster'), 'a.src_subjectid = b.IdSubject')
            ->where('a.src_sreid = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateStatus($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('sponsor_reapplication', $bind, 'sre_id = '.$id);
        return $update;
    }
    
    public function getReApplicationCourse($id, $group = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'sponsor_reapplication_course'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_subjectmaster'), 'a.src_subjectid = b.IdSubject')
            ->where('a.src_sreid = ?', $id);

        if ($group == true){
            $select->group('a.src_subjectid');
        }

        $result = $db->fetchAll($select);
        return $result;
    }
}
?>