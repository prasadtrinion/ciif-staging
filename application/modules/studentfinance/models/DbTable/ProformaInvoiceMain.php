<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_ProformaInvoiceMain extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'proforma_invoice_main';
	protected $_primary = "id";

	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('im'=>$this->_name))
						->where("im.id = ?", (int)$id);

		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	public function getApplicantProformaIssued($appl_id, $semester_id){
		
	}
	
	/*
	 * Overite Insert function
	*/
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		if(!isset($data['creator'])){
			$data['creator'] = $auth->getIdentity()->iduser;
		}
	
		if( !isset($data['date_create'])  ){
			$data['date_create'] = date('Y-m-d H:i:s');
		}
	
		return parent::insert( $data );
	}
        
        public function getDataByTxnId($txnId,$fee_category){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
                    ->from(array('a'=>$this->_name))
                    ->joinLeft(array('b'=>'tbl_currency'), 'a.currency_id = b.cur_id', array('currencyName'=>'b.cur_code'))
                    ->where("a.trans_id = ?", (int)$txnId)
                    ->where("a.fee_category = ?", (int)$fee_category);


		$row = $db->fetchRow($selectData);
		return $row;
	}
}