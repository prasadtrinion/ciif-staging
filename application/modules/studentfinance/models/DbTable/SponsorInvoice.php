<?php
class Studentfinance_Model_DbTable_SponsorInvoice extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'sponsor_invoice_main';
	protected $_primary = "sp_id";
		
	public function getData($id=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>$this->_name));
					
		if($id!=null){
			$selectData->where("d.sp_id =?",$id);
			
			$row = $db->fetchRow($selectData);
		}else{
			$row = $db->fetchAll($selectData);
		}
			
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getPaginateData(){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
				->from(array('d'=>$this->_name))
				->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = d.dcnt_creator', array())
				->joinLeft(array('ts'=>'tbl_staffmaster'),'ts.IdStaff = tu.IdStaff', array("creator"=>new Zend_Db_Expr("CONCAT_WS(' ', fName,Mname,Lname)")));
	
		return $selectData;
	}
	
	public function getDiscountData($fomulir){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = d.dcnt_creator', array())
					->joinLeft(array('ts'=>'tbl_staffmaster'),'ts.IdStaff = tu.IdStaff', array("creator"=>'Fullname'))
					->where('d.dcnt_fomulir_id =?',$fomulir);
					
		$row = $db->fetchAll($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function insert(array $data){
		
		if( !isset($data['sp_creator']) ){
			$auth = $auth = Zend_Auth::getInstance();
			
			$data['sp_creator'] = $auth->getIdentity()->iduser; 
		}
		
		if( !isset($data['sp_create_date']) ){
			$data['sp_create_date'] = date('Y-m-d H:i:a'); 
		}
		
		return parent::insert($data);
	}
	
	public function getlistStudent($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>'sponsor_invoice_main'))
					->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = d.sp_IdStudentRegistration',array('IdStudentRegistration','registrationId'))
					->join(array('ap'=>'student_profile'),'b.sp_id=ap.id',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )")))
					->join(array('c'=>'tbl_currency'), 'd.sp_currency_id = c.cur_id',array('cur_code'))
					->join(array('p' => 'tbl_program'), 'p.IdProgram = b.IdProgram',array('ProgramCode'))
					->join(array('i' => 'tbl_intake'), 'i.IdIntake = b.IdIntake',array('IntakeDesc'))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = d.sp_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('tu1'=>'tbl_user'),'tu1.iduser = d.sp_approve_by', array('approve_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = d.sp_cancel_by', array('cancel_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->where('d.sp_batch_id =?',$id);
					
		$row = $db->fetchAll($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getlistStudentByStatus($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$arrayStatus = array('A','E','X');
		$newArray = array();
		foreach($arrayStatus as $key=>$sts){
			
			
			$selectData = $db->select()
						->from(array('d'=>'sponsor_invoice_main'),array('count(*) as total'))
						->where('d.sp_batch_id =?',$id)
						->where('d.sp_status =?',$sts);
						
			$row = $db->fetchRow($selectData);
			
			$newArray[$sts]=$row['total'];
		}

		if($newArray){
			return $newArray;
		}else{
			return null;
		}
	}
	
	public function getlistInvoice($type,$id,$batchno){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>'sponsor_invoice_main'))
					->join(array('bd' => 'sponsor_invoice_batch'), 'bd.id = d.sp_batch_id',array('batch_number','semester_id','b_amount'=>'bd.amount','b_paid'=>'bd.paid','b_balance'=>'bd.balance'))
					->join(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster = bd.semester_id',array('SemesterMainName'))
					->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = d.sp_IdStudentRegistration',array('IdStudentRegistration','registrationId'))
					->join(array('ap'=>'student_profile'),'b.sp_id=ap.id',array('name' => new Zend_Db_Expr("CONCAT_WS(' ', appl_fname,appl_mname,appl_lname )")))
					->join(array('c'=>'tbl_currency'), 'd.sp_currency_id = c.cur_id',array('cur_code'))
					->join(array('p' => 'tbl_program'), 'p.IdProgram = b.IdProgram',array('ProgramCode'))
					->join(array('i' => 'tbl_intake'), 'i.IdIntake = b.IdIntake',array('IntakeDesc'))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = d.sp_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('tu1'=>'tbl_user'),'tu1.iduser = d.sp_approve_by', array('approve_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = d.sp_cancel_by', array('cancel_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->where('d.sp_type =?',$type)
					->where('d.sp_type_id =?',$id)
					->where('d.sp_batch_id =?',$batchno)
					->where('d.sp_balance > 0')
					->where("d.sp_status = 'A'");
					
		$row = $db->fetchAll($selectData);

		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getInvoiceSponsor($idDetail){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('d'=>'sponsor_invoice_detail'))
					->where('d.sp_invoice_det_id =?',$idDetail);
					
		$row = $db->fetchRow($selectData);

		return $row;
		
	}
	
	public function autocompleteSearch($type=1, $id, $batchno, $limit=10)
	{
		$db = getDB();

		$select = $db->select()->from(array('a' => 'sponsor_invoice_batch'), array('sa.IdStudentRegistration as key','sa.registrationId as value'))     
		->join(array('df' => 'tbl_definationms'), 'df.IdDefinition = sa.profileStatus')  		
						  ->join(array('p'=>'student_profile'),'p.id=sa.sp_id',array('CONCAT(CONCAT_WS(" ",appl_fname,appl_mname,appl_lname)," - ",registrationId," - ",DefinitionDesc) as label'))
						  ;
//						  ->where('sa.ProfileStatus = 92'); //active only

		if ( $term != '' )
		{
			//1 = name
			if ( $searchby == 1 )
			{
				$select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?','%'.$term.'%');
			}
			else
			{
				$select->where('sa.registrationId LIKE ?','%'.$term.'%');
			}
		}

		$select->limit($limit);

		$results = $db->fetchAll($select);
		return $results;
	}
}

