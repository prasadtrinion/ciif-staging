<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_InvoiceSubject extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'invoice_subject';
	protected $_primary = "id";

	public function getData($invoice_id,$invoice_detail_id=0){
		$db = Zend_Db_Table::getDefaultAdapter();

		$selectData = $db->select()
					->from(array('a'=>$this->_name))
					->join(array('s'=>'tbl_subjectmaster'),'s.IdSubject=a.subject_id', array('*'))
					->where('a.invoice_main_id = ?', $invoice_id);

		if($invoice_detail_id){
			$selectData->where('a.invoice_detail_id = ?', $invoice_detail_id);		
		}
		$row = $db->fetchAll($selectData);

		return $row;
	}
	
}