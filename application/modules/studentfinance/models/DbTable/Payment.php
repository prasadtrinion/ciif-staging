<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_Payment extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'payment';
	protected $_primary = "p_id";
	protected $_locale;
	
	public function init() {
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
	}

	public function getData($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('p'=>$this->_name))
						->where("p.p_id ?", (int)$id);

		$row = $db->fetchRow($selectData);
		return $row;
	}
	
	public function getReceiptPayment($receipt_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
						->from(array('p'=>$this->_name))
						->joinLeft(array('t'=>'creditcard_terminal'), 't.id = p.p_terminal_id', array('terminal_id'))
						->where("p.p_rcp_id = ?", (int)$receipt_id);
		
		if($this->_locale == 'en_US'){
			$selectData ->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = p.p_cur_id', array('cur_code'=>'cur_code', 'cur_desc'=>'cur_desc', 'cur_symbol_prefix'=>'cur_symbol_prefix', 'cur_symbol_suffix'=>'cur_symbol_suffix'))
						->joinLeft(array('pm'=>'payment_mode'), 'pm.id = p.p_payment_mode_id', array('payment_mode'=>'payment_mode'));
		}else{
			$selectData	->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = p.p_cur_id', array('cur_code'=>'cur_code', 'cur_desc'=>'cur_desc_default_language', 'cur_symbol_prefix'=>'cur_symbol_prefix', 'cur_symbol_suffix'=>'cur_symbol_suffix'))
						->joinLeft(array('pm'=>'payment_mode'), 'pm.id = p.p_payment_mode_id', array('payment_mode'=>'description_second_language'));
		}
		
		$row = $db->fetchAll($selectData);
		
		return $row;
	}
}