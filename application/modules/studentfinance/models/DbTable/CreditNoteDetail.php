<?php
class Studentfinance_Model_DbTable_CreditNoteDetail extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'credit_note_detail';
	protected $_primary = "id";
		
	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('cn'=>$this->_name))
					->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = cn.cn_creator', array('creator_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
					->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = cn.cn_approver', array('approver_name'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
					->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = cn.cn_cancel_by', array('canceler_name'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"));
		
		if($id!=0){
			$selectData->where("cn.cn_id = '".$id."'");
			
			$row = $db->fetchRow($selectData);
		}else{
			
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;	
		}				
		
	}
	
	
	
}

