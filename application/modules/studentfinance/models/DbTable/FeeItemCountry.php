<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_FeeItemCountry extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'fee_item_country';
	protected $_primary = "fic_id";

	public function getData($id=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
		->from(array('fic'=>$this->_name));

		if($id!=0){
			$selectData->where("fic.fic_id = '".$id."'");
				
			$row = $db->fetchRow($selectData);
		}else{
				
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;
		}

	}
	
	/*
	 * Get fee item by id and country
	 */
	public function getFeeItemByCountry($fic_fi_id, $fic_country_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('fic'=>$this->_name))
					->where("fic.fic_fi_id = ?",$fic_fi_id)
					->where("fic.fic_idCountry = ?",$fic_country_id);
		
				
		$row = $db->fetchRow($selectData);
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	}
	
	/*
	 * Get country list
	 */
	public function getCountryFeeItem($row_format=false, $idCountry=null,$feeitem=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if($row_format==true){
			
			$selectData = $db->select()
							 ->from(array('c'=>'tbl_countries'), array('idCountry', 'CountryName', 'CountryIso', 'CountryISO3' ))
							 ->joinLeft(array('fic'=>$this->_name),'fic.fic_idCountry = c.idCountry');
			
			if($idCountry){
				$selectData->where('c.idCountry = ?', $idCountry);
			}
			
			$row = $db->fetchAll($selectData);
			
				
			if(!$row){
				return null;
			}else{
				return $row;
			}
			
		}else{
			
			$selectData = $db->select()
							 ->from(array('c'=>'tbl_countries'), array('idCountry', 'CountryName', 'CountryIso', 'CountryISO3' ));

			if($idCountry){
				$selectData->where('c.idCountry = ?', $idCountry);
				
				$rows = $db->fetchRow($selectData);
			}else{
				$rows = $db->fetchAll($selectData);
			}
				
			if($rows){
				
				if(isset($rows[0])){
					foreach ($rows as $index=>$country){
					
						$selectData2 = $db->select()
						->from(array('fic'=>$this->_name))
						->where('fic.fic_idCountry = ? ', $country['idCountry']);
						
						if ( $feeitem )
						{
							$selectData2->where('fic.fic_fi_id = ?',$feeitem);
						}
							
						$row = $db->fetchAll($selectData2);

		
							
						if($row){
							$rows[$index]['fees'] = $row;
					
							//fee index by currency
							foreach ($row as $cur){
								$rows[$index]['fees_by_currency'][$cur['fic_cur_id']] = $cur;
							}
						}
					}	
				}else{
					$selectData2 = $db->select()
					->from(array('fic'=>$this->_name))
					->where('fic.fic_idCountry = ? ', $rows['idCountry']);
					if ( $feeitem )
					{
						$selectData2->where('fic.fic_fi_id = ?',$feeitem);
					}
					$row = $db->fetchAll($selectData2);
						
					if($row){
						$rows['fees'] = $row;
							
						//fee index by currency
						foreach ($row as $cur){
							$rows['fees_by_currency'][$cur['fic_cur_id']] = $cur;
						}
					}
				}
				
				
			}
			
			if(!$rows){
				return null;
			}else{
				return $rows;
			}
			
		}
	}
	
	public function insert(array $data){
	
		$auth = Zend_Auth::getInstance();
	
		$data['fic_added_by'] = $auth->getIdentity()->iduser;
		$data['fic_added_date'] = date('Y-m-d H:i:s');
			
		return parent::insert($data);
	}
	
	public function update(array $data, $condition){
	
		$auth = Zend_Auth::getInstance();
	
		$data['fic_updated_by'] = $auth->getIdentity()->iduser;
		$data['fic_updated_date'] = date('Y-m-d H:i:s');
			
		return parent::update($data,$condition);
	}
}