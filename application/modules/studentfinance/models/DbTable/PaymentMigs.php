<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_Model_DbTable_PaymentMigs extends Zend_Db_Table_Abstract {

	/**
	 * The default table name
	 */
	protected $_name = 'payment_migs';
	protected $_primary = "pm_id";
	
	
	/*
	 * Overite Insert function
	*/
	
	/*public function insert(array $data){
	
		if( !isset($data['pm_date_create'])  ){
			$data['pm_date_create'] = date('Y-m-d H:i:s');
		}
	
		return parent::insert( $data );
	}*/

	public function checkDuplicatePayment($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'payment'))
			->where('a.p_migs = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}
}