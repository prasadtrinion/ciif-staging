<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 15/8/2016
 * Time: 2:24 PM
 */
class Studentfinance_Model_DbTable_EarlyBird extends Zend_Db_Table_Abstract {


    public function getEbit($id){
        $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
        $calculation = new Studentfinance_Model_DbTable_EarlyBirdCalculation();
        $studentInfo = $studentRegDB->getStudentInfo($id);

        $curEbit = array();

        if ($studentInfo){
            $checkArr = array();

            $checkSponsor = $this->getFinancialAidTagging($id);

            if ($checkSponsor){
                if (isset($checkSponsor['Sponsorship'])){
                    $checkArr['SponsorId']=$checkSponsor['Sponsorshipid'];
                    $checkArr['Type']=1;
                }else if(isset($checkSponsor['Scholarship'])){
                    $checkArr['ScholarId']=$checkSponsor['Scholarshipid'];
                    $checkArr['Type']=2;
                }else{
                    $checkArr['SelfFunding']=1;
                    $checkArr['Type']=3;
                }
            }else{
                $checkArr['SelfFunding']=1;
                $checkArr['Type']=3;
            }

            //var_dump($checkArr); exit;

            $semesterDb = new App_Model_Registration_DbTable_Semester();
            $listSemester = $semesterDb->getListCurrentSemester(array('ap_prog_scheme' => $studentInfo['IdScheme']));

            $curEbit = $this->getCurrentEarlyBird($listSemester[0]['IdSemesterMaster']);

            if ($curEbit){
                foreach ($curEbit as $ebitKey => $curEbitLoop){
                    $mainStatus = false;
                    $checkProgram = $this->checkProgramEarlyBird($curEbitLoop['eb_id'], $studentInfo['IdProgram']);

                    $financialAid = $this->getFinancialAid($id, $listSemester[0]['IdSemesterMaster']);
                    if ($curEbitLoop['eb_financial_aid'] == $financialAid){
                        $financialAidStatus = true;
                    }else{
                        $financialAidStatus = false;
                    }

                    if ($checkProgram){
                        $sponsorStatus = false;

                        if ($checkArr['Type']==1){
                            $ebitSponsor = $this->checkSponsorEarlyBird($curEbitLoop['eb_id'], $checkArr['SponsorId']);

                            if ($ebitSponsor){
                                $sponsorStatus = true;
                            }
                        }else if ($checkArr['Type']==2){
                            $ebitScholar = $this->checkScholarEarlyBird($curEbitLoop['eb_id'], $checkArr['ScholarId']);

                            if ($ebitScholar){
                                $sponsorStatus = true;
                            }
                        }else{
                            if ($curEbitLoop['eb_self_sponsor']==$checkArr['SelfFunding']){
                                $sponsorStatus = true;
                            }
                        }

                        if ($sponsorStatus == true || $financialAidStatus == true){
                            $prevSemester = $this->getPrevSemester($curEbitLoop['eb_id']);

                            if ($prevSemester){
                                foreach ($prevSemester as $prevSemesterLoop){
                                    $inv = $this->getInvoice($id, $prevSemesterLoop['ebp_prev_sem']);

                                    if ($inv){
                                        $balAmt = 0.00;
                                        foreach ($inv as $invLoop){
                                            $balance = $calculation->getBalanceMain($invLoop['id'], $prevSemesterLoop['ebp_payment_date']);
                                            $balAmt = $balAmt+$balance['bill_balance'];
                                            //var_dump($balance);
                                        }

                                        //var_dump($balAmt);
                                        if ($balAmt <= 0.00){
                                            $mainStatus = true;
                                        }else{
                                            $mainStatus = false;
                                            break;
                                        }
                                    }else{
                                        $mainStatus = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    //var_dump($mainStatus);
                    if ($mainStatus == false){
                        unset($curEbit[$ebitKey]);
                    }
                }
            }
        }

        return $curEbit;
    }

    public function checkEbitItem($dtId, $feeId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'discount_type_fee_info'))
            ->where('a.IdDiscountType = ?', $dtId)
            ->where('a.FeeCode = ?', $feeId);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function studentInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'))
            ->joinLeft(array('b'=>'student_profile'), 'a.sp_id = b.id')
            ->where('a.IdStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getCurrentEarlyBird($semId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'early_bird'))
            //->where('a.eb_semester = ?', 0)
            ->where('a.eb_semester = ?', $semId)
            ->where('a.eb_payment_date >= ?', date('Y-m-d'));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkProgramEarlyBird($ebId, $progId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'early_bird_program'))
            ->where('a.ebpr_eb_id = ?', $ebId)
            ->where('a.ebpr_prog_id = ?', $progId);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getPrevSemester($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'early_bird_prevsem'))
            ->join(array('b'=>'tbl_semestermaster'), 'a.ebp_prev_sem = b.IdSemesterMaster')
            ->where('a.ebp_eb_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getFinancialAid($studId, $semId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select1 = $db->select()
            ->from(array('a'=>'sponsor_application'))
            ->where('a.sa_cust_type = ?', 2)
            ->where('a.sa_status IN (?)', array(2, 7))
            ->where('a.sa_cust_id = ?', $studId)
            ->where('a.sa_semester_id = ?', $semId);

        $result1 = $db->fetchAll($select1);

        $select2 = $db->select()
            ->from(array('a'=>'sponsor_reapplication'))
            ->where('a.sre_cust_type = ?', 2)
            ->where('a.sre_status IN (?)', array(2, 7))
            ->where('a.sre_cust_id = ?', $studId)
            ->where('a.sre_semester_id = ?', $semId);

        $result2 = $db->fetchAll($select2);

        if ($result1 || $result2){
            return 1;
        }else{
            return 0;
        }
    }

    public function checkSponsorEarlyBird($ebId, $sponsorId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'early_bird_sponsor'))
            ->where('a.ebsp_eb_id = ?', $ebId)
            ->where('a.ebsp_sponsor_id = ?', $sponsorId);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function checkScholarEarlyBird($ebId, $sponsorId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'early_bird_scholar'))
            ->where('a.ebsc_eb_id = ?', $ebId)
            ->where('a.ebsc_scholar = ?', $sponsorId);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInvoice($id, $semId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'invoice_main'))
            ->where('a.IdStudentRegistration = ?', $id)
            ->where('a.semester = ?', $semId)
            ->where('a.status = ?', 'A');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getFinancialAidTagging($id, $date=null) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectScholar = $db->select()
            ->from(array('schtg'=>'tbl_scholarship_studenttag'),array(
                    'id'=>'sca.sch_Id',
                    'IdStudentRegistration'=>'schtg.sa_cust_id',
                    'type'=>new Zend_Db_Expr ('"Scholarship"'),
                    'fundingMethod'=>'sca.sch_name',
                    'start_date'=>'DATE(schtg.sa_start_date)',
                    'end_date'=>"IFNULL(DATE(schtg.sa_end_date),'0000-00-00')",
                )
            )
            ->joinLeft(array('sca'=>'tbl_scholarship_sch'), 'sca.sch_Id = schtg.sa_scholarship_type', array())
            ->where("schtg.sa_cust_id = ?",$id)
            ->where("schtg.sa_status != 7");

        $selectSponsor = $db->select()
            ->from(array('spt'=>'tbl_sponsor_tag'),array(
                    'id'=>'sptt.idsponsor',
                    'IdStudentRegistration'=>"spt.StudentId",
                    'type'=>new Zend_Db_Expr ('"Sponsorship"'),
                    'fundingMethod'=>"CONCAT_WS(' ',sptt.fName, sptt.lName)",
                    'start_date'=>'DATE(spt.StartDate)',
                    'end_date'=>"IFNULL(DATE(spt.EndDate),'0000-00-00')",
                )
            )
            ->joinLeft(array('sptt'=>'tbl_sponsor'), 'sptt.idsponsor = spt.Sponsor', array())
            ->where("spt.StudentId = ?",$id);

        $selectDiscount = $db->select()
            ->from(array('dcn'=>'discount_type_tag'),array(
                    'id'=>'dcnt.dt_id',
                    'IdStudentRegistration'=>"dcn.IdStudentRegistration",
                    'type'=>new Zend_Db_Expr ('"Discount"'),
                    'fundingMethod'=>"dcnt.dt_discount",
                    'start_date'=>'DATE(dcn.dtt_start_date)',
                    'end_date'=>"IFNULL(DATE(dcn.dtt_end_date),'0000-00-00')",
                )
            )
            ->joinLeft(array('dcnt'=>'discount_type'), 'dcnt.dt_id=dcn.dt_id', array())
            ->where("dcn.IdStudentRegistration = ?",$id);

        $selectFunding = $db->select()
            ->union(array($selectScholar, $selectSponsor,$selectDiscount),  Zend_Db_Select::SQL_UNION_ALL);

        $resultsFunding = $db->fetchAll($selectFunding);


        if ($date) {
            $currentDate = $date;
        } else {
            $currentDate = date('Y-m-d');
        }

        $newArray = array();
        foreach($resultsFunding as $key=>$fund){

            $newArray[$fund['type']] = strtoupper($fund['fundingMethod']);
            $newArray['invoice_date'] = $date;
            $newArray['startdate'] = $fund['start_date'];
            $newArray['enddate'] = $fund['end_date'];
            $newArray[$fund['type'].'id'] = $fund['id'];

            $disStartDate = $fund['start_date'];
            $disEndDate =  $fund['end_date'];
            if($disEndDate != '0000-00-00'){
                $disEndDate = $disEndDate;
            }else{
                $disEndDate = $currentDate;
            }

            if($currentDate >= $disStartDate){
                if($currentDate <= $disEndDate){

                }else{
                    unset($resultsFunding[$key]);
                    unset($newArray[$fund['type']]);
                }

            }else{
                unset($resultsFunding[$key]);
                unset($newArray[$fund['type']]);
            }
        }

        return $newArray;
    }

    public function getBillSeq($type, $year){

        $seq_data = array(
            $type,
            $year,
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['proforma_invoice_no'];
    }

    public function insertEbDiscount($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('discount', $data);
        $id = $db->lastInsertId('discount', 'dcnt_id');
        return $id;
    }

    public function insertEbDiscountDtl($data){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('discount_detail', $data);
        $id = $db->lastInsertId('discount_detail', 'id');
        return $id;
    }
}