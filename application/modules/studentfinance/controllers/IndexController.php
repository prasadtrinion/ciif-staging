<?php
/**
 * @author Suliana
 * @version 1.0
 */

class Studentfinance_IndexController extends Zend_Controller_Action {
	
	private $_DbObj;
	private $_sis_session;
	
	public function init() {
		
		$this->locale = Zend_Registry::get('Zend_Locale');
    	$this->view->translate = Zend_Registry::get('Zend_Translate');
    	Zend_Form::setDefaultTranslator($this->view->translate);
    	
       Zend_Layout::getMvcInstance()->assign('navActive', 'finance');

        $this->auth = Zend_Auth::getInstance();
		$this->studentInfo = $this->auth->getIdentity()->info;

		$this->studentModel = new Portal_Model_DbTable_Student();

		$studentRegDB = new App_Model_StudentRegistration();
		$student = $studentRegDB->getStudentInfo($this->auth->getIdentity()->IdStudentRegistration);

		if ( empty($student) )
		{
			throw new Exception('Invalid Student ID');
		}

		$this->studentinfo = $this->view->studentinfo = $student;
		
	}
	
	
	public function indexAction(){
		
		if( $this->getRequest()->isXmlHttpRequest() ){
			$this->_helper->layout->disableLayout();
		}
		
		$student = $this->studentinfo;
		
		$idStudentRegistration = $student['IdStudentRegistration'];
		$this->view->idStudentRegistration = $idStudentRegistration;
		
		$trans_id = $idStudentRegistration;
		
		//title
    	$this->view->title= $this->view->translate("Statement of Account");
		
    	//profile
    	$studentRegistrationDb = new CourseRegistration_Model_DbTable_Studentregistration();
    	$profile = $studentRegistrationDb->getTheStudentRegistrationDetail($idStudentRegistration);
    	
    	$this->view->profile = $profile;
    	
//    	echo "<pre>";
//    	print_r($profile);
//    	exit;

    	//account
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		$select_invoice = $db->select()
								->from(array('im'=>'invoice_main'),array(
									'id'=>'im.id',
									'record_date'=>'im.invoice_date',
									'description' => 'GROUP_CONCAT(fc.fi_name)',
									'txn_type' => new Zend_Db_Expr ('"Invoice"'),
									'debit' =>'bill_amount',
									'credit' => new Zend_Db_Expr ('"0.00"'),
									'document' => 'bill_number',
									'invoice_no' => 'bill_number',
									'receipt_no' => new Zend_Db_Expr ('"-"'),
									'subject' => 'GROUP_CONCAT(distinct s.SubCode)',
									'fc_seq'=> 'ivd.fi_id',
									'migrate_desc' => 'bill_description',
									'migrate_code' => 'MigrateCode',
									//'exchange_rate'=>'im.exchange_rate',
										'exchange_rate' => new Zend_Db_Expr ('"x"'),
								)
						)
						->join(array('ivd'=>'invoice_detail'),'ivd.invoice_main_id=im.id',array())
						->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id',array())
						->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject=ivs.subject_id',array())
						->joinLeft(array('fc'=>'fee_item'),'fc.fi_id=ivd.fi_id',array())
						->join(array('c'=>'tbl_currency'),'c.cur_id=im.currency_id', array('cur_code','cur_id','cur_desc'))
						->joinLeft(array('cn'=>'credit_note'), 'cn.cn_invoice_id = im.id AND cn.cn_status = "A"', array())
						//->where('im.currency_id= ?',$currency)						  
						->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(im.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(im.invoice_date), 0)) OR im.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=im.id) OR im.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")')
						->where("im.status NOT IN ('X', 'E','0')")//display ALL status except X,0=entry (24/27/07/2015)
						->group('im.id');
						
			$select_invoice->where('im.IdStudentRegistration = ?', $trans_id);
							
			$select_payment = $db->select()
							->from(
								array('pm'=>'receipt'),array(
										'id'=>'pm.rcp_id',
										'record_date'=>'pm.rcp_receive_date',
										'description' => "CONCAT_WS('',p.p_cheque_no,p.p_card_no)",
										'txn_type' => new Zend_Db_Expr ('"Payment"'),
										'debit' =>new Zend_Db_Expr ('"0.00"'),
										'credit' => "rcp_amount",
										'document' => 'rcp_no',
										'invoice_no' => 'rcp_no',
										'receipt_no' => 'rcp_no',
										'subject' =>  'rcp_gainloss',//gain/loss
										'fc_seq'=> 'rcp_gainloss_amt',//gain/loss - myr
										'migrate_desc' => 'pm.rcp_description',
										'migrate_code' => 'MigrateCode',		
										'exchange_rate' =>  new Zend_Db_Expr ('"x"'),															
									)
							)
							
							->join(array('p'=>'payment'),'p.p_rcp_id = pm.rcp_id', array())	
							->join(array('c'=>'tbl_currency'),'c.cur_id=pm.rcp_cur_id', array('cur_code','cur_id','cur_desc'))	
							->joinLeft(array('a'=>'tbl_receivable_adjustment'),"a.IdReceipt = pm.rcp_id and a.Status = 'APPROVE'", array())	
							
							//->where('pm.rcp_cur_id= ?',$currency)		
							->where("pm.rcp_status = 'APPROVE' OR (pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0 AND MONTH(pm.rcp_receive_date) != MONTH(a.EnterDate))");
//							->orWhere("pm.rcp_status = 'CANCEL' AND pm.adjustment_id != 0");
							
			$select_payment->where('pm.rcp_idStudentRegistration = ?', $trans_id);
							
			$select_credit = $db->select()
							->from(
								array('cn'=>'credit_note'),array(
										'id'=>'cn.cn_id',
										'record_date'=>'cn.cn_create_date',
										'description' => 'cn.cn_description',
										'txn_type' => new Zend_Db_Expr ('"Credit Note"'),
										'debit' =>new Zend_Db_Expr ('"0.00"'),
										'credit' => 'cn_amount',
										'document' => 'cn_billing_no',
										'invoice_no' => 'cn_billing_no',
										'receipt_no' => 'cn_billing_no',
										'subject' =>  new Zend_Db_Expr ('""'),
										'fc_seq'=> 'c.cur_id',
										'migrate_desc' => 'cn.cn_description',	
								 		'migrate_code' => 'MigrateCode',
										'exchange_rate' =>  "IFNULL(i.exchange_rate,'x')",																
									)
							)
							
							
							->join(array('c'=>'tbl_currency'),'c.cur_id=cn.cn_cur_id', array('cur_code','cur_id','cur_desc'))	
							->joinLeft(array('i'=>'invoice_main'),'i.id = cn.cn_invoice_id', array())	
							//->where('pm.rcp_cur_id= ?',$currency)		
							->where("cn.cn_status = 'A'")
							->where("IFNULL(i.status, 'A') NOT IN ('X', 'E','0')")
							//->where('(IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(i.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(i.invoice_date), 0)) OR i.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=i.id)');
							->where('(IFNULL(YEAR(cn.cn_approve_date), 0) != IFNULL(YEAR(i.invoice_date), 0) OR IFNULL(MONTH(cn.cn_approve_date), 0) != IFNULL(MONTH(i.invoice_date), 0)) OR i.bill_amount != (SELECT IFNULL(SUM(cnn.cn_amount), 0) FROM credit_note as cnn WHERE cnn.cn_invoice_id=i.id AND cnn.cn_status="A" AND (IFNULL(YEAR(cnn.cn_approve_date), 0) = IFNULL(YEAR(i.invoice_date), 0) AND IFNULL(MONTH(cnn.cn_approve_date), 0) = IFNULL(MONTH(i.invoice_date), 0))) OR i.id IN (SELECT rcp.rcp_inv_invoice_id FROM receipt_invoice AS rcp INNER JOIN receipt AS rc ON rcp.rcp_inv_rcp_id = rc.rcp_id WHERE rc.rcp_status = "APPROVE")');
		$select_credit->where('cn.cn_IdStudentRegistration = ?', $trans_id);
			
			$select_discount = $db->select()
							->from(
								array('a'=>'discount_detail'),array(
										'id'=>'dn.dcnt_id',
										'record_date'=>'dn.dcnt_approve_date',
										'description' => 'dn.dcnt_description',
										'txn_type' => new Zend_Db_Expr ('"Discount"'),
										'debit' =>new Zend_Db_Expr ('"0.00"'),
										'credit' => 'SUM(a.dcnt_amount)',
										'document' => 'dn.dcnt_fomulir_id',
										'invoice_no' => 'dn.dcnt_fomulir_id',
										'receipt_no' => 'dn.dcnt_fomulir_id',
										'subject' =>  new Zend_Db_Expr ('""'),
										'fc_seq'=> 'c.cur_id',
										'migrate_desc' => 'dn.dcnt_description',	
								 		'migrate_code' => new Zend_Db_Expr ('""'),
										'exchange_rate' => 'i.exchange_rate',																
									)
							)
							
							->join(array('dn'=>'discount'),'dn.dcnt_id = a.dcnt_id', array())	
							->join(array('c'=>'tbl_currency'),'c.cur_id=dn.dcnt_currency_id', array('cur_code','cur_id','cur_desc'))	
							->joinLeft(array('i'=>'invoice_main'),'i.id = a.dcnt_invoice_id', array())	
//							->joinLeft(array('adj'=>'discount_adjustment'),"adj.IdDiscount = a.id and adj.Status= 'APPROVE' AND (MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date))", array())	
							//->where('pm.rcp_cur_id= ?',$currency)		
							->where("MONTH(dn.dcnt_approve_date) != MONTH(dn.dcnt_cancel_date)")
							->group('dn.dcnt_id');
			$select_discount->where('dn.dcnt_IdStudentRegistration = ?', $trans_id)	;
							
			$select_refund = $db->select()
							->from(
								array('rn'=>'refund'),array(
										'id'=>'rfd_id',
										'record_date'=>'rfd_date',
										'description' => 'rfd_desc',
										'txn_type' => new Zend_Db_Expr ('"Refund"'),
										'debit' => 'rfd_amount',
										'credit' => new Zend_Db_Expr ('"0.00"'),
										'document' => 'rfd_fomulir',
										'invoice_no' => 'rfd_fomulir',
										'receipt_no' => 'rfd_fomulir',
										'subject' =>  new Zend_Db_Expr ('""'),
										'fc_seq'=> 'c.cur_id',
										'migrate_desc' => 'rfd_desc',	
								 		'migrate_code' => 'MigrateCode',
										'exchange_rate' =>  new Zend_Db_Expr ('"x"'),																
									)
							)
							
							->join(array('c'=>'tbl_currency'),'c.cur_id=rn.rfd_currency_id', array('cur_code','cur_id','cur_desc'))	
							->where("MONTH(rn.rfd_date) != MONTH(rn.rfd_cancel_date)")
							->where("rn.rfd_status IN  ('A','X')");
							
			$select_refund->where('rn.rfd_IdStudentRegistration = ?', $trans_id);	
						
			//advance payment from invoice deposit only
			$select_advance = $db->select()
							->from(
								array('adv'=>'advance_payment'),array(
										'id'=>'advpy_id',
										'record_date'=>'advpy_date',
										'description' => "CONCAT(advpy_description,' - ',fi.fi_name)",
										'txn_type' => new Zend_Db_Expr ('"Advance Payment"'),
										'debit' => new Zend_Db_Expr ('"0.00"'),
										'credit' => 'advpy_amount',
										'document' => 'advpy_fomulir',
										'invoice_no' => 'advpy_fomulir',
										'receipt_no' => 'advpy_fomulir',
										'subject' =>  new Zend_Db_Expr ('""'),
										'fc_seq'=> 'c.cur_id',
										'migrate_desc' => 'advpy_description',	
								 		'migrate_code' => 'MigrateCode',
										'exchange_rate' =>  "IFNULL(adv.advpy_exchange_rate,'x')",																
									)
							)
							
							->join(array('c'=>'tbl_currency'),'c.cur_id=adv.advpy_cur_id', array('cur_code','cur_id','cur_desc'))	
							->join(array('i'=>'invoice_main'),'i.id=adv.advpy_invoice_id', array())
							->join(array('ivd'=>'invoice_detail'),'ivd.invoice_main_id = i.id', array())	
							->join(array('fi'=>'fee_item'), 'fi.fi_id = ivd.fi_id',array())
							->where('fi.fi_refundable = 1')
//							->where('i.adv_transfer_id != 0 ')
							->where("i.status = 'A'")
							->where("adv.advpy_status = 'A'");
		$select_advance->where('adv.advpy_idStudentRegistration = ?', $trans_id);

		//refund cancel
			$select_refund_cancel = $db->select()
							->from(
								array('rnc'=>'refund'),array(
										'id'=>'rfd_id',
										'record_date'=>'rfd_cancel_date',
										'description' => 'rfd_desc',
										'txn_type' => new Zend_Db_Expr ('"Refund Adjustment"'),
										'debit' => new Zend_Db_Expr ('"0.00"'),
										'credit' => 'rfd_amount',
										'document' => 'rfd_fomulir',
										'invoice_no' => "CONCAT(rfd_fomulir,'-',rfd_id)",
										'receipt_no' => 'rfd_fomulir',
										'subject' =>  new Zend_Db_Expr ('""'),
										'fc_seq'=> 'c.cur_id',
										'migrate_desc' => 'rfd_desc',	
								 		'migrate_code' => 'MigrateCode',
										'exchange_rate' =>  new Zend_Db_Expr ('"x"'),																
									)
							)
							
							->join(array('c'=>'tbl_currency'),'c.cur_id=rnc.rfd_currency_id', array('cur_code','cur_id','cur_desc'))	
							->where("MONTH(rnc.rfd_date) != MONTH(rnc.rfd_cancel_date)")
							->where("rnc.rfd_status = 'X'");
		$select_refund_cancel->where('rnc.rfd_IdStudentRegistration = ?', $trans_id);
							
			
			//discount adjustment
			$select_discount_cancel = $db->select()
							->from(
								array('dsn'=>'discount_adjustment'),array(
										'id'=>'dsn.id',
										'record_date'=>'dsn.EnterDate',
										'description' => "CONCAT(dsnt.dcnt_description,' (',dsnta.dcnt_fomulir_id,')')",
										'txn_type' => new Zend_Db_Expr ('"Discount Adjustment"'),
										'debit' => 'dsn.amount',
										'credit' => new Zend_Db_Expr ('"0.00"'),
										'document' => 'AdjustmentCode',
										'invoice_no' => "AdjustmentCode",
										'receipt_no' => 'AdjustmentCode',
										'subject' =>  new Zend_Db_Expr ('""'),
										'fc_seq'=> 'c.cur_id',
										'migrate_desc' => 'dsnt.dcnt_description',	
								 		'migrate_code' => new Zend_Db_Expr ('""'),
										'exchange_rate' =>  new Zend_Db_Expr ('"x"'),																
									)
							)
							
							->join(array('c'=>'tbl_currency'),'c.cur_id=dsn.cur_id', array('cur_code','cur_id','cur_desc'))
							->join(array('dsnt'=>'discount_detail'),'dsnt.id=dsn.IdDiscount', array())	
							->join(array('dsnta'=>'discount'),'dsnta.dcnt_id=dsnt.dcnt_id', array())	
							->where("dsn.Status = 'APPROVE'")
							->where("MONTH(dsnta.dcnt_approve_date) != MONTH(dsnta.dcnt_cancel_date)")
							->where('dsn.AdjustmentType = 1'); //cancel
			$select_discount_cancel->where('dsnta.dcnt_IdStudentRegistration = ?', $trans_id); //cancel
			
			//payment adjustment - cancel approved receipt
			//checking not same month with receipt only
			
			$select_receipt_adj = $db->select()
							->from(
								array('a'=>'tbl_receivable_adjustment'),array(
										'id'=>'a.IdReceivableAdjustment',
										'record_date'=>'a.EnterDate',
										'description' => 'a.Remarks',
										'txn_type' => new Zend_Db_Expr ('"Payment Adjustment"'),
										'debit' => 'b.rcp_amount',
										'credit' => new Zend_Db_Expr ('"0.00"'),
										'document' => 'AdjustmentCode',
										'invoice_no' => 'AdjustmentCode',
										'receipt_no' => 'AdjustmentCode',
										'subject' =>  new Zend_Db_Expr ('""'),
										'fc_seq'=> 'c.cur_id',
										'migrate_desc' => 'a.Remarks',	
								 		'migrate_code' => new Zend_Db_Expr ('""'),
										'exchange_rate' => new Zend_Db_Expr ('"x"'),																
									)
							)
							
							->join(array('b'=>'receipt'),'b.rcp_id = a.IdReceipt', array())	
							->join(array('c'=>'tbl_currency'),'c.cur_id=b.rcp_cur_id', array('cur_code','cur_id','cur_desc'))	
							->where("a.Status = 'APPROVE'")
							
							->where("MONTH(b.rcp_receive_date) != MONTH(a.EnterDate)");
			$select_receipt_adj->where('b.rcp_IdStudentRegistration = ?', $trans_id)
							;
							
			$select_receipt_sponsor = $db->select()
							->from(
								array('a'=>'sponsor_invoice_receipt_student'),array(
										'id'=>'a.rcp_inv_id',
										'record_date'=>'b.rcp_receive_date',
										'description' => 'b.rcp_description',
										'txn_type' => new Zend_Db_Expr ('"Sponsor Payment"'),
										'debit' => new Zend_Db_Expr ('"0.00"'),
										'credit' => 'a.rcp_inv_amount',
										'document' => 'b.rcp_no',
										'invoice_no' => 'b.rcp_no',
										'receipt_no' => 'b.rcp_no',
										'subject' =>  new Zend_Db_Expr ('""'),
										'fc_seq'=> 'c.cur_id',
										'migrate_desc' => 'b.rcp_description',	
								 		'migrate_code' => new Zend_Db_Expr ('""'),
										'exchange_rate' => new Zend_Db_Expr ('"x"'),																
									)
							)
							
							->join(array('b'=>'sponsor_invoice_receipt'),'b.rcp_id = a.rcp_inv_rcp_id', array())	
							->join(array('c'=>'tbl_currency'),'c.cur_id=a.rcp_inv_cur_id', array('cur_code','cur_id','cur_desc'))
							->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))');
							//->where("b.rcp_status = 'APPROVE'");
			$select_receipt_sponsor->where('a.IdStudentRegistration = ?', $trans_id);

			$select_sponsor_adjustment = $db->select()
				->from(
					array('a' => 'sponsor_invoice_receipt_student'), array(
						'id' => 'a.rcp_inv_id',
						'record_date' => 'IFNULL(e.sp_cancel_date, b.cancel_date)',
						'description' => 'b.rcp_description',
						'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
						'debit' => 'e.sp_amount',
						'credit' => new Zend_Db_Expr ('"0.00"'),
						'document' => 'b.rcp_no',
						'invoice_no' => 'concat(b.rcp_no, "-", e.id)',
						'receipt_no' => 'b.rcp_no',
						'subject' => new Zend_Db_Expr ('""'),
						'fc_seq' => 'c.cur_id',
						'migrate_desc' => 'b.rcp_description',
						'migrate_code' => new Zend_Db_Expr ('""'),
						'exchange_rate' => new Zend_Db_Expr ('"x"'),
					)
				)
				->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
				->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
				->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
				->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
				//->where("e.sp_status = ?", 0);
				->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
				->where("e.sp_status = 0 OR b.rcp_status = 'CANCEL'");
				//->where("b.rcp_status = 'APPROVE'");

			$select_sponsor_adjustment->where('a.IdStudentRegistration = ?', $trans_id);

		$select_sponsor_adv_adjustment = $db->select()
			->from(
				array('a' => 'sponsor_invoice_receipt_student'), array(
					'id' => 'a.rcp_inv_id',
					'record_date' => 'f.advpy_cancel_date',
					'description' => 'b.rcp_description',
					'txn_type' => new Zend_Db_Expr ('"Sponsor Payment Adjustment"'),
					'debit' => 'f.advpy_amount',
					'credit' => new Zend_Db_Expr ('"0.00"'),
					'document' => 'b.rcp_no',
					'invoice_no' => 'concat(b.rcp_no, "-", f.advpy_id)',
					'receipt_no' => 'b.rcp_no',
					'subject' => new Zend_Db_Expr ('""'),
					'fc_seq' => 'c.cur_id',
					'migrate_desc' => 'b.rcp_description',
					'migrate_code' => new Zend_Db_Expr ('""'),
					'exchange_rate' => new Zend_Db_Expr ('"x"'),
				)
			)
			->join(array('b' => 'sponsor_invoice_receipt'), 'b.rcp_id = a.rcp_inv_rcp_id', array())
			->join(array('f'=>'advance_payment'), 'a.rcp_inv_rcp_id = f.advpy_rcp_id', array())
			->join(array('c' => 'tbl_currency'), 'c.cur_id=a.rcp_inv_cur_id', array('cur_code', 'cur_id', 'cur_desc'))
			//->join(array('d' => 'sponsor_invoice_receipt_invoice'), 'a.rcp_inv_id=d.rcp_inv_student', array())
			//->join(array('e' => 'sponsor_invoice_detail'), 'd.rcp_inv_sponsor_dtl_id=e.id', array())
			->where('(IFNULL(YEAR(b.rcp_receive_date), 0) != IFNULL(YEAR(b.cancel_date), 0) OR IFNULL(MONTH(b.rcp_receive_date), 0) != IFNULL(MONTH(b.cancel_date), 0))')
			->where("f.advpy_status = ?", 'X');

		$select_sponsor_adv_adjustment->where('a.IdStudentRegistration = ?', $trans_id);
		$select_sponsor_adv_adjustment->where('f.advpy_idStudentRegistration = ?', $trans_id);

		//currency adjustment
		$select_currency_adjustment = $db->select()
			->from(
				array('a' => 'advance_payment_detail'), array(
					'id' => 'a.advpydet_id',
					'record_date' => 'a.advpydet_upddate',
					'description' => new Zend_Db_Expr ('"Currency Adjustment"'),
					'txn_type' => new Zend_Db_Expr ('"Currency Adjustment"'),
					'debit' => 'a.gain',
					'credit' => 'a.loss',
					'document' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
					'invoice_no' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
					'receipt_no' => 'concat(b.advpy_fomulir, "-", a.advpydet_id)',
					'subject' => new Zend_Db_Expr ('""'),
					'fc_seq' => 'c.cur_id',
					'migrate_desc' => 'b.advpy_description',
					'migrate_code' => new Zend_Db_Expr ('""'),
					'exchange_rate' => new Zend_Db_Expr ('"x"'),
				)
			)
			->join(array('b' => 'advance_payment'), 'a.advpydet_advpy_id = b.advpy_id', array())
			->join(array('c' => 'tbl_currency'), 'c.cur_id=b.advpy_cur_id', array('cur_code', 'cur_id'=>new Zend_Db_Expr ('"1"'), 'cur_desc'))
			->where('a.advpydet_status = ?', 'A')
			->where('b.advpy_status = ?', 'A')
			->where('a.gain != 0.00 OR a.loss != 0.00');

		$select_currency_adjustment->where('b.advpy_idStudentRegistration = ?', $trans_id);

			//get array payment				
			$select = $db->select()
					    ->union(array($select_invoice, $select_payment,$select_credit,$select_discount,$select_refund,$select_advance,$select_refund_cancel,$select_discount_cancel,$select_receipt_adj,$select_receipt_sponsor, $select_sponsor_adjustment, $select_currency_adjustment, $select_sponsor_adv_adjustment),  Zend_Db_Select::SQL_UNION_ALL)
					    //->union(array($select_invoice, $select_payment,$select_credit,$select_discount,$select_refund,$select_advance,$select_refund_cancel,$select_discount_cancel,$select_receipt_adj,$select_receipt_sponsor, $select_sponsor_adjustment, $select_sponsor_adv_adjustment),  Zend_Db_Select::SQL_UNION_ALL)
					    ->order("record_date asc")
					    ->order("txn_type asc")
					   
					   ;

					  
		$results = $db->fetchAll($select);
		
		if(!$results){
			$results = null;
		}

		///dd($results);
		
			//process data
			$results2 = array();
			$amountCurr = array();
			$curencyArray = array();
			$balance = 0;
			
			if($results){
				foreach ( $results as $row )
				{
					$spmCurAdjStatus = false;
				$curencyArray[$row['cur_id']] = $row['cur_code'];
					
					$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();

					if ($row['txn_type'] == "Sponsor Payment" || $row['txn_type'] == "Sponsor Payment Adjustment") {
						$selectInvoiceSpm = $db->select()
							->from(array('a'=>'sponsor_invoice_receipt_invoice'), array())
							->join(array('b'=>'sponsor_invoice_detail'), 'a.rcp_inv_sp_id = b.sp_id', array('b.sp_amount'))
							->joinLeft(array('c'=>'invoice_main'), 'b.sp_invoice_id = c.id', array('c.invoice_date'))
							->join(array('d'=>'sponsor_invoice_receipt'), 'a.rcp_inv_rcp_id = d.rcp_id', array())
							->where('a.rcp_inv_student = ?', $row['id'])
							->group('b.id');

						if ($row['txn_type'] == "Sponsor Payment Adjustment") {
							$selectInvoiceSpm->where("b.sp_status = 0 OR d.rcp_status = 'CANCEL'");
						}

						$listInvoiceSpm = $db->fetchAll($selectInvoiceSpm);

						$amountDebitSponsor = 0.00;
						$amountCreditSponsor = 0.00;

						if ($listInvoiceSpm){
							foreach ($listInvoiceSpm as $invoiceSpm){
								if ($row['cur_id'] == 2) {
									$dataCur = $curRateDB->getRateByDate(2, $invoiceSpm['invoice_date']);
									//$dataCur = $curRateDB->getRateByDate(2, $row['record_date']);

									if ($row['txn_type'] == "Sponsor Payment") {
										$amountCreditSponsor = round($amountCreditSponsor + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
									}else{
										$amountDebitSponsor = round($amountDebitSponsor + round($invoiceSpm['sp_amount'] * $dataCur['cr_exchange_rate'], 2), 2);
									}
								} else {
									if ($row['txn_type'] == "Sponsor Payment") {
										$amountCreditSponsor = $amountCreditSponsor + $invoiceSpm['sp_amount'];
									}else{
										$amountDebitSponsor = $amountDebitSponsor + $invoiceSpm['sp_amount'];
									}
								}
							}
						}
					} //else {
						if ($row['exchange_rate']) {
							if ($row['exchange_rate'] == 'x') {
								$dataCur = $curRateDB->getRateByDate(2, $row['record_date']);//usd
//							$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
							} else {
								$dataCur = $curRateDB->getData($row['exchange_rate']);//usd
							}
						} else {
							$dataCur = $curRateDB->getRateByDate(2, $row['record_date']);
//						$dataCur = $curRateDB->getCurrentExchangeRate(2);//usd
						}
					//}
						
//					$amountCurr[$row['invoice_no']]['MYR']['cur_id'] = $row['cur_id'];
//					$amountCurr[$row['invoice_no']]['MYR']['credit2'] = $row['credit'];
//					$amountCurr[$row['invoice_no']]['MYR']['exchange_rate'] = $row['exchange_rate'];
					//if ($row['txn_type'] != "Sponsor Payment" && $row['txn_type'] != "Sponsor Payment Adjustment") {
						if ($row['cur_id'] == 2) {
							$amountDebit = round($row['debit'] * $dataCur['cr_exchange_rate'], 2);
							$amountCredit = round($row['credit'] * $dataCur['cr_exchange_rate'], 2);
						} else {
							$amountDebit = $row['debit'];
							$amountCredit = $row['credit'];
						}
					//}
//					$amountCurr[$row['invoice_no']]['MYR']['credit3'] = $amountCredit;
		
					if( $row['txn_type']=="Invoice" ){
						$balance = $balance + $amountDebit;
					}else
					if( $row['txn_type']=="Payment"){
			
						$balance = $balance - $amountCredit ;
						
						///hide on 05/08/2015 - do nothing for no
						//unhide 12/08/2015
						if($row['subject'] == 'gain'){
							$balance = $balance + $row['fc_seq'];
							$amountCredit = $amountCredit - $row['fc_seq'];
						}elseif($row['subject'] == 'loss'){//loss
							$balance = $balance - $row['fc_seq'];
							$amountCredit = $amountCredit + $row['fc_seq'];
						}
						
					}else
					if( $row['txn_type']=="Credit Note" ){
						$balance = $balance - $amountCredit;
					}else
					if( $row['txn_type']=="Discount" ){
						$balance = $balance - $amountCredit;
					}else
					if( $row['txn_type']=="Debit Note" ){
						$balance = $balance + $row['debit'];
					}else
					if( $row['txn_type']=="Refund" ){
						$balance = $balance + $amountDebit;
					}else
					if( $row['txn_type']=="Advance Payment" ){
			
						$balance = $balance - $amountCredit;
						
					}else
					if($row['txn_type']=="Refund Adjustment"){
			
						$balance = $balance - $amountCredit;
						
					}
					else
					if($row['txn_type']=="Discount Adjustment"){
			
						$balance = $balance + $amountDebit;
						
					}
					else
					if($row['txn_type']=="Payment Adjustment"){
			
						$balance = $balance + $amountDebit;
						
					}else
						if ($row['txn_type'] == "Sponsor Payment") {
							if (strtotime($row['record_date']) <= strtotime('2016-12-31 00:00:00')){
								$balance = $balance - $amountCredit;

								$sponsorCurAdj = ($amountCreditSponsor - $amountCredit);

								if ($sponsorCurAdj != 0.00){
									$spmCurAdjStatus = true;
								}
							}else{
								$balance = $balance - $amountCreditSponsor;
								$row['credit'] = $amountCredit = $amountCreditSponsor;
								$row['cur_code'] = 'MYR';
								$row['cur_id'] = 1;
								$row['cur_desc'] = 'Malaysia Ringgit';
							}
						}else if ($row['txn_type'] == "Sponsor Payment Adjustment"){
							if (strtotime($row['record_date']) <= strtotime('2016-12-31 00:00:00')) {
								$balance = $balance + $amountDebit;

								$sponsorCurAdj = ($amountDebitSponsor - $amountDebit);

								if ($sponsorCurAdj != 0.00) {
									$spmCurAdjStatus = true;
								}
							}else{
								$balance = $balance + $amountDebitSponsor;
								$row['debit'] = $amountDebit = $amountDebitSponsor;
								$row['cur_code'] = 'MYR';
								$row['cur_id'] = 1;
								$row['cur_desc'] = 'Malaysia Ringgit';
							}
						}else if ($row['txn_type'] == "Currency Adjustment"){
						$balance = $balance - $amountCredit;
						$balance = $balance + $amountDebit;
					}

					$results2[] = $row;
					
					$amountCurr[$row['invoice_no']]['MYR']['debit'] = $amountDebit;
					$amountCurr[$row['invoice_no']]['MYR']['credit'] = $amountCredit;
					$amountCurr[$row['invoice_no']]['MYR']['balance'] = number_format($balance, 2, '.', ',');

					if ($spmCurAdjStatus == true){
						if ($row['txn_type'] == "Sponsor Payment") {
							$balance = $balance - ($sponsorCurAdj);
						}else{
							$balance = $balance + ($sponsorCurAdj);
						}

						$results2[] = array(
							'id' => $row['id'],
							'record_date' => $row['record_date'],
							'description' => $row['description'],
							'txn_type' => 'Sponsor Payment Currency Adjustment',
							'debit'=> str_replace('-', '', number_format(($sponsorCurAdj < 0.00 ? $sponsorCurAdj:0.00), 2, '.', '')),
							'credit' => str_replace('-', '', number_format(($sponsorCurAdj > 0.00 ? $sponsorCurAdj:0.00), 2, '.', '')),
							'document' => $row['document'].'-CURADV',
							'invoice_no' => $row['invoice_no'].'-CURADV',
							'receipt_no' => $row['receipt_no'].'-CURADV',
							'subject' => $row['subject'],
							'fc_seq' => $row['fc_seq'],
							'migrate_desc' => $row['migrate_desc'].' Currency Adjustment',
							'migrate_code' => $row['migrate_code'],
							'exchange_rate' => $row['exchange_rate'],
							'cur_code' => 'MYR',
							'cur_id' => 1,
							'cur_desc' => 'Malaysia Ringgit'
						);

						if ($sponsorCurAdj > 0.00){
							$amountCurr[$row['invoice_no'].'-CURADV']['MYR']['debit'] = 0.00;
							$amountCurr[$row['invoice_no'].'-CURADV']['MYR']['credit'] = str_replace('-', '', $sponsorCurAdj);
							$amountCurr[$row['invoice_no'].'-CURADV']['MYR']['balance'] = number_format($balance, 2, '.', ',');
						}else{
							$amountCurr[$row['invoice_no'].'-CURADV']['MYR']['debit'] = str_replace('-', '', $sponsorCurAdj);
							$amountCurr[$row['invoice_no'].'-CURADV']['MYR']['credit'] = 0.00;
							$amountCurr[$row['invoice_no'].'-CURADV']['MYR']['balance'] = number_format($balance, 2, '.', ',');
						}
					}
				}
			}
		
			
//		    $curAvailable = array_unique($curencyArray);
		/*if($trans_id == 3258){
		    echo "<pre>";
			print_r($amountCurr);
		}*/
//			exit;

		$this->view->account = $results2;

		    $curAvailable = array('2'=>'USD','1'=>'MYR');
		    
			$this->view->availableCurrency= $curAvailable;
			$this->view->amountCurr = $amountCurr;
	}
	
	public function invoiceAction(){
		if( $this->getRequest()->isXmlHttpRequest() ){
			$this->_helper->layout->disableLayout();
		}
		
		$appl_id = $this->_getParam('id', null);
		$this->view->appl_id = $appl_id;
		
		$type = $this->_getParam('type', null);
		$this->view->type = $type;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
						->from(
							array('im'=>'invoice_main'),
							array(
								'id'=>'im.id',
								'billing'=>'im.bill_number',
								'payer'=>'im.id',
								'record_date'=>'im.invoice_date',
								'description' => 'im.bill_description',
								'txn_type' => new Zend_Db_Expr ('"Invoice"'),
								'debit' =>'bill_amount',
								'credit' => new Zend_Db_Expr ('"0.00"'),
								'document' => 'bill_number',
								'paid' => 'bill_paid',
								'balance' => 'bill_balance',
								'cn' =>'im.cn_amount',
								'currency'=>'c.cur_symbol_prefix'
							)
						)
						->join(array('c'=>'tbl_currency'),'c.cur_id=im.currency_id', array('cur_code','cur_id','cur_desc','cur_symbol_prefix'))
						->where("im.status NOT IN ('X','0')")//display ALL status except X,0=entry (24/27/07/2015)
						->order('im.invoice_date')
						->order('im.id');
						
		if($type == 1){
			$select->where('im.trans_id = ?', $appl_id);
		}elseif($type == 2){
			$select->where('im.IdStudentRegistration = ?', $appl_id);
		}
		

		$row = $db->fetchAll($select);
				
		//calculate invoice paid
		if($row){
			$paymentMainDb = new Studentfinance_Model_DbTable_PaymentMain();
			$creditNoteDb = new Studentfinance_Model_DbTable_CreditNote();
			$receiptInvoiceDB = new Studentfinance_Model_DbTable_ReceiptInvoice();
			$invoiceSubjectDB = new Studentfinance_Model_DbTable_InvoiceSubject();
			$invoiceDetailDB = new Studentfinance_Model_DbTable_InvoiceDetail();
			
			
			foreach ($row as $index=>$invoice){
//				$payment_record = $paymentMainDb->getAllInvoicePaymentRecord($invoice['billing']);
				$payment_record = $receiptInvoiceDB->getAllInvoicePaymentRecord($invoice['id']);
				$row[$index]['payment'] = $payment_record;
				
				//get invoice details
				$invoiceDetailData = $invoiceDetailDB->getInvoiceDetail($invoice['id']);
				$row[$index]['invoice_detail'] = $invoiceDetailData;
				
				$m=0;
				foreach($invoiceDetailData as $det){
					//get invoice subject
					$row[$index]['invoice_detail'][$m]['invoice_subject'] = $invoiceSubjectDB->getData($invoice['id'],$det['id']);
					$m++;
				}
				
				
				$creditNote = $creditNoteDb->getAllDataByInvoice($invoice['billing']);
				$row[$index]['credit_note'] = $creditNote;
				
				//calculate if payment made and paid is null or 0.00 and no credit note
				if( ($invoice['paid']==null || $invoice['paid']==0.00) && $payment_record && $creditNote==null){
					
					//initialy set balance to invoice amount
					$row[$index]['balance'] = $row[$index]['debit'];
						
					//loop all payment record
					foreach ($payment_record as $pmt_record){
						
						//check if payment having excess amount
						if($pmt_record['rcp_amount'] > $row[$index]['debit']){
							
							$amount_paid = $row[$index]['debit'];
							
							$row[$index]['paid'] = $amount_paid;
							$row[$index]['balance'] = $row[$index]['balance'] - $amount_paid;
						}else{
							
							$amount_paid = $pmt_record['rcp_amount'];
							
							$row[$index]['paid'] = $row[$index]['paid'] + $amount_paid;
							$row[$index]['balance'] = $row[$index]['balance'] - $amount_paid;
						}
					}
					
					//mark update
					$row[$index]['update'] = 1;
					$row[$index]['update_record'] = "paid";
				}else
				//do correction if paid is null and balance is null
				if( $invoice['paid']==null && $invoice['balance']==null ){
					$row[$index]['paid'] = 0;
					$row[$index]['balance'] = $row[$index]['debit'];
					
					//mark update
					$row[$index]['update'] = 1;
					$row[$index]['update_record'] = "init";
				}else{
					//mark do not update
					$row[$index]['update'] = 0;
					$row[$index]['update_record'] = "nan";
				}
				
				//check for credit note
				if( $creditNote && ( $invoice['cn']==0.00 || $invoice['cn']==null || $invoice['cn']=="" ) ){

					$sum_cn = 0;
					foreach ($creditNote as $cn){
						if( $cn['cn_cancel_date']==null && isset($cn['cn_approve_date']) ){
							$sum_cn += $cn['cn_amount'];
						}
					}
					$row[$index]['cn'] = $sum_cn;
					$row[$index]['balance'] = $row[$index]['balance'] - $sum_cn;
					
					if($row[$index]['balance']<0){
						$row[$index]['balance'] = 0.00;
					}
					
					$row[$index]['update'] = $row[$index]['update'] || 1;
					$row[$index]['update_record'] .= " CN";
					
				}else{
					if(!isset($row[$index]['cn'])){
						$row[$index]['cn'] = 0.00;
						$row[$index]['update'] = $row[$index]['update'] || 1;
						$row[$index]['update_record'] .= " CN";
					}else{
						$row[$index]['update'] = $row[$index]['update'] || 0;
						$row[$index]['update_record'] .= " XCN";
					}
					
				}
			
			}
		}
		
		
		//process update invoice's balance and paid
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		foreach ($row as $invoice){
			if($invoice['update']==1){
				$data = array(
					'bill_paid' => $invoice['paid'],
					'bill_balance' => $invoice['balance'],
					'cn_amount' => $invoice['cn']
				);
				 
				$invoiceMainDb->update($data,'id = '.$invoice['id']);
			}
		}
		
		if(!$row){
			$row = null;
		}
		
		
		//get payment, advance payment detail and cn info
		$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
		
		if($row){
			foreach ($row as $index => $invoice){
				$row[$index]['payment_record'] = $receiptInvoiceDB->getAllInvoicePaymentRecord($invoice['id']);
				$row[$index]['advance_payment_detail'] = $advancePaymentDetailDb->getAllBillPayment($invoice['id']);
				$row[$index]['cn_info'] = $creditNoteDb->getAllDataByInvoice($invoice['billing']);
				
			}
		}

		
		$this->view->invoice = $row;
	}
	
	public function paymentAction(){
		if( $this->getRequest()->isXmlHttpRequest() ){
			$this->_helper->layout->disableLayout();
		}
		
		$appl_id = $this->_getParam('id', null);
		$this->view->appl_id = $appl_id;
		$type = $this->_getParam('type', null);
		$this->view->type = $type;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
						->from(
							array('pm'=>'receipt'),array(
															'id'=>'pm.rcp_id',
															'billing_no'=>'pm.rcp_no',
															'record_date'=>'pm.rcp_date',
															'description' => 'pm.rcp_description',
															'txn_type' => new Zend_Db_Expr ('"Payment"'),
															'debit' =>new Zend_Db_Expr ('"0.00"'),
															'credit' => 'pm.rcp_amount',
															'currency'=>'c.cur_symbol_prefix'
														)
						)
						->join(array('c'=>'tbl_currency'),'c.cur_id=pm.rcp_cur_id', array('cur_code','cur_id','cur_desc','cur_symbol_prefix'))
						->where("pm.rcp_status = 'APPROVE'");
						
		if($type == 1){
			$select->where('pm.rcp_trans_id = ?', $appl_id);
		}elseif($type == 2){
			$select->where('pm.rcp_idStudentRegistration = ?', $appl_id);
		}
					
		$row = $db->fetchAll($select);
		
		if(!$row){
			$row = null;
		}
		
		$this->view->payment = $row;
	}
	
	public function printInvoiceAction()
	{
		$this->view->title = 'Invoice';
		
		$student = $this->studentinfo;
		
		$invoiceID = $this->_getParam('id', null);
		
		$invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceDetail();
		$invoice = $invoiceMainDB->getDetail($invoiceID);
		
		$address = trim($student["appl_caddress1"]);
        if($student["appl_caddress2"]){
            $address.='<br/>'.$student["appl_caddress2"].' ';
        }
        if($student["appl_cpostcode"]){
        	$address.='<br/>'.$student["appl_cpostcode"].' ';
        }
        if($student["appl_ccity"]!=99){
        	$address.= '<br />'.$student["CCityName"].' ';
        }else{
        	$address.= '<br/ >'.$student["appl_ccity_others"].' ';
        }
        if($student["appl_cstate"]!=99){
        	$address.=$student["CStateName"].' ';
        }else{
       		$address.= $student["appl_cstate_others"];
        }
        	$address.= '<br/>'.$student["CCountryName"];
                    
		
		$fieldValues = array(
     		 '[Name]'=>strtoupper( $student["appl_fname"].' '.$student["appl_lname"]) , 
			 '[Address]'=> strtoupper( $address ) ,
			 '[Date]'=> date('d F Y',strtotime($invoice[0]["invoice_date"]))  ,
			 '[ID]'=> strtoupper($student["registrationId"] ),
			 '[NRIC]'=> strtoupper( $student["appl_idnumber"] ),
			 '[Amount in Text]' => '',
			'[Ref No]' => strtoupper( $invoice[0]["bill_number"] ),
			'[Programme]' => strtoupper( $student["ProgramName"] ),
			'[Mode of Study]' => strtoupper( $student["StudyMode"] ),
			'[Semester]' => strtoupper( $invoice[0]["SemesterMainName"] ),
			'[DatePrint]'=> date('d F Y')  ,
//			 '$[LOGO]'=> APPLICATION_URL."/images/logo_text_high.jpg"
      		);
      	
      		
      	$currency = new Zend_Currency(array('currency'=>$invoice[0]['currency_id']));
		
					$totalInvoice = 0;
					$totalNet = 0;
					$totalDisc = 0;
		
					$processing_fee = "<table id='tfhover' class='tftable' border='1'>";
                    $processing_fee .= "<thead>";
                    $processing_fee .= "<tr>";
                    $processing_fee .= "<th>Item Code</th>";
                    $processing_fee .= "<th>Description</th>";
                    $processing_fee .= "<th>Type</th>";
                    $processing_fee .= "<th>Amount (".$invoice[0]['cur_code'].")</th>";
                    $processing_fee .= "<th>Discount Amount</th>";
                    $processing_fee .= "<th>Nett Amount</th>";
                    $processing_fee .= "</tr>";
                    $processing_fee .= "</thead>";
                    $processing_fee .= "<tbody>";

                    if (count($invoice) > 0){
                            $a=0;

                        foreach($invoice as $proformaInfoLoop){
                        	$desc = '';
                        	if($proformaInfoLoop['IdSubject']){
                        		$desc .= $proformaInfoLoop['SubCode'].' ';
                        		$desc .= $proformaInfoLoop['fi_name'];
//                        		$desc .= ' - '.$proformaInfoLoop['item_name'].' ';
                        		$amount = $proformaInfoLoop['amount_subject'];
                        		$discamount = $proformaInfoLoop['dcnt_amount'];
                        	}elseif($proformaInfoLoop["MigrateCode"]!=""){
                        		$desc=$proformaInfoLoop['bill_description'];
                        		$amount = $proformaInfoLoop['amount'];
                        		$discamount = $proformaInfoLoop['dcnt_amount'];
                        	}else{
                        		$desc .= $proformaInfoLoop['fi_name'];
                        		$amount = $proformaInfoLoop['amount'];
                        		$discamount = $proformaInfoLoop['dcnt_amount'];
                        	}
                        	
                        	$nettamount = $amount - $discamount;
                        	$discamount = $discamount ? $discamount : '0.00';
                        	
                            $processing_fee .= "<tr>";
                            $processing_fee .= "<td>".$student["ProgramCode"]."</td>";
                            $processing_fee .= "<td>".$student["ProgramName"]."</td>";
                            $processing_fee .= "<td>".$desc."</td>";
                            $processing_fee .= "<td align='right'>".$amount."</td>";
                            $processing_fee .= "<td align='right'>".$discamount."</td>";
                            $processing_fee .= "<td align='right'>".number_format($nettamount,2)."</td>";
                            $processing_fee .= "</tr>";
                            
                            $totalInvoice = $totalInvoice+$amount;
                            $totalNet = $totalNet+$nettamount;
                            $totalDisc = $totalDisc+$discamount;
                            
                            $a++;
                        }
                    }
                    $processing_fee .= "<tr><td colspan='3' align='right'>Total : </td>";
                    $processing_fee .= "<td align='right'>".sprintf ("%.2f", $totalInvoice)."</td>";
                    $processing_fee .= "<td align='right'>".sprintf ("%.2f", $totalDisc)."</td>";
                    $processing_fee .= "<td align='right'>".sprintf ("%.2f", $totalNet)."</td>";
                    $processing_fee .= "</tr>";
                    $processing_fee .= "</tbody>";
                    $processing_fee .= "</table>";

             $fieldValues['[Invoice]'] = $processing_fee; 
	    require_once '../library/dompdf/dompdf_config.inc.php';
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		$templateDb = new App_Model_Communication_DbTable_Template();
		$template = $templateDb->getTemplateContent(83,'en_US');
					
		$html = $template['tpl_content'];
		
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);	
		}

//		echo $html;exit;
		$option = array(
			'content' => $html,
			'save' => false,
			'file_name' => 'invoice',
			'css' => '@page { margin: 110px 50px 50px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#2b2b2b;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#d3d3d3;border-width: 1px;padding: 8px;border-style: solid;border-color: #c6c6c6;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 8px;border-style: solid;border-color: #c6c6c6;}',
			
			'header' => '<script type="text/php">
					if ( isset($pdf) ) {
				
					  $header = $pdf->open_object();
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					  
					  $color = array(0,0,0);
						
					  $img_w = 172; 
					  $img_h = 69;
                      $pdf->image("images/logo_iumw.png",  $w-($img_w+35), 20, $img_w, $img_h);
					  
					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
			
			          $pdf->close_object();
			
					  $pdf->add_object($header, "all");
					}
					</script>',
			'footer' => '<script type="text/php">
					if ( isset($pdf) ) {
				
			          $footer = $pdf->open_object();
			
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
					  $color = array(0,0,0);
					  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					 
				
					  // Draw a line along the bottom
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  //1st row footer
					  $text = "#MALAYSIA TEL:+603 2617 3000 #FAX:+603 2617 3203 #WEB: iumw.edu.my #EMAIL: enquiry@iumw.edu.my";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (3 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
					 
			          //2nd row footer
					  $text = "Note: This is a computer generated document and does not require signature";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (2 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
				
					  // Draw a second line along the bottom
					  $y = $h - (0.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  $pdf->close_object();
			
					  $pdf->add_object($footer, "all");
					 
					}
					</script>'
		);
		
		$pdf = generatePdf($option);
		
		exit;
	}

    public function printReceiptAction()
    {
        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $this->view->title = 'Receipt';

//		$student = $this->studentinfo;

        $receiptID = $this->_getParam('id', null);
        $type = $this->_getParam('type', null);

        $receiptDB = new Studentfinance_Model_DbTable_Receipt();
        $receipt = $receiptDB->getData($receiptID);

        $receipt = $receipt[0];

        if ($receipt['rcp_idStudentRegistration']) {
            $studentRegistrationDb = new App_Model_Registration_DbTable_Studentregistration();
            $student = $studentRegistrationDb->getTheStudentRegistrationDetail($receipt['rcp_idStudentRegistration']);
            $taggingFinancialAid = $studentRegistrationDb->getFinancialAidTagging($receipt['rcp_idStudentRegistration']);
        } else {
            $applicantProfileDb = new App_Model_Application_DbTable_ApplicantTransaction();
            $student = $applicantProfileDb->getApplicantRegistrationDetail($receipt['rcp_trans_id']);
        }

        if ($type == null) {
            $address = trim($student["appl_caddress1"]);
            if ($student["appl_caddress2"]) {
                $address .= '<br/>' . $student["appl_caddress2"] . ' ';
            }
            if ($student["appl_cpostcode"]) {
                $address .= '<br/>' . $student["appl_cpostcode"] . ' ';
            }
            if ($student["appl_ccity"] != 99) {
                $address .= '<br />' . $student["CCityName"] . ' ';
            } else {
                $address .= '<br/ >' . $student["appl_ccity_others"] . ' ';
            }
            if ($student["appl_cstate"] != 99) {
                $address .= $student["CStateName"] . ' ';
            } else {
                $address .= $student["appl_cstate_others"];
            }
            $address .= '<br/>' . $student["CCountryName"];
        } else {
            $sponsormodel = new Studentfinance_Model_DbTable_Sponsor();
            $sponsorInfo = $sponsormodel->getSponsorInfo($taggingFinancialAid['Sponsorshipid']);

            $address = $sponsorInfo['Add1'] != '' ? $sponsorInfo['Add1'] : '';
            $address .= $sponsorInfo['Add2'] != '' ? '<br />' . $sponsorInfo['Add2'] : '';
            $address .= $sponsorInfo['zipCode'] != '' ? '<br />' . $sponsorInfo['zipCode'] : '';
            $address .= $sponsorInfo['CityName'] != '' && $sponsorInfo['CityName'] != null ? '<br />' . $sponsorInfo['CityName'] : '';
            $address .= $sponsorInfo['StateName'] != '' && $sponsorInfo['StateName'] != null ? '<br />' . $sponsorInfo['StateName'] : '';
            $address .= $sponsorInfo['CountryName'] != '' && $sponsorInfo['CountryName'] != null ? '<br />' . $sponsorInfo['CountryName'] : '';
        }

        if ($student['sem_seq']) {
            switch ($student['sem_seq']) {
                case "JAN":
                    $semseq = 1;
                    break;
                case "JUN":
                    $semseq = 2;
                    break;
                case "SEP":
                    $semseq = 3;
                    break;
            }
        }

        //get username
        $userDb = new App_Model_General_DbTable_Admin();
        /*$user_list = $userDb->getData($creator);

        if ($user_list) {
            $login_name = $user_list['loginName'];
        }*/
        $login_name = mb_convert_case($student["appl_fname"] . ' ' . $student["appl_lname"], MB_CASE_TITLE);
        /*total reprint receipt*/

        // $this->totalReprint($receipt[rcp_idStudentRegistration]);
        $db = Zend_Db_Table::getDefaultAdapter();
        $select_reprint = $db->select()
            ->from(
                array('rp' => 'tbl_receipt_reprint'), array(
                    'receipt_id' => 'COUNT(receipt_id) as total_print'
                )
            )
            ->where("rp.stud_id = '$receipt[rcp_idStudentRegistration]'");
        $select_reprint->group('receipt_id');

        $row_reprint = $db->fetchAll($select_reprint);

        $print_tot=0;
        if ($row_reprint) {
            foreach ($row_reprint as $resultprint => $rowp) {
                $print_tot = $rowp['total_print'];
            }
        }

        if ($print_tot == 0) {
            $rprint_tot = "/Original";
        }
        else { //if ($print_tot != 1) {
            $rprint_tot = "/R-".$print_tot;
            //++$rprint_tot;
        }
        /*else {

            //$rprint_tot = "/R-";
            $rprint_tot = "/R-".$print_tot - 1;

        }*/


        /* end total reprint */

        $data = array(
            'userid' => $creator,
            'receipt_id' => $receiptID,
            'stud_id' => $receipt['rcp_idStudentRegistration'],
            'created_date' => date("Y-m-d H:i:s"),
        );

        $reprintDb = new App_Model_Finance_DbTable_ReprintReceipt();

        $reprintDb->insert($data);

        $fieldValues = array(
            '[Applicant Name]' => $type==null ? strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"]):strtoupper($student["appl_fname"] . ' ' . $student["appl_lname"].'<br />'.$sponsorInfo['fName']),
            '[Applicant Address]' => strtoupper($address),
            '[Date]' => date('d-M-y', strtotime($receipt["rcp_receive_date"])),
            '[Applicant ID]' => isset($student["registrationId"]) ? strtoupper($student["registrationId"]):strtoupper($student["at_pes_id"]),
            '[Applicant IC]' => strtoupper($student["appl_idnumber"]),
            '[Applicant Intake]' =>  strtoupper(trim($student['IntakeDesc'])).'/'.$semseq,
            '[Applicant Program]' => strtoupper($student["ProgramName"]),
            '[Recepient Name]' => '-',
            '[User ID]' => $login_name,
            '[DatePrint]' => date('d-M-y'),

//			 '$[LOGO]'=> APPLICATION_URL."/images/logo_text_high.jpg"
        );

        $currency = new Zend_Currency(array('currency' => $receipt['cur_code']));

        //invoice
        $invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
        $invoice_list = $invoiceMainDb->getInvoiceFromReceipt($receiptID);

        $inv = array();
        $total_payment = 0.00;

        if ($invoice_list) {

            foreach ($invoice_list as $invoice) {

                if ($receipt['rcp_description']!="") {
                    $description_resit = $invoice['bill_description'].": ".$receipt['rcp_description'];
                } else {
                    $description_resit = $invoice['bill_description'];
                }
                $iv = array(
                    'invoice_no' => $invoice['bill_number'],
                    'amount' => 0,
                    'currency' => $invoice['cur_symbol_prefix'],
                    'description' => $description_resit,
                    'bill_amount' => $invoice['bill_amount']
                );

                foreach ($invoice['receipt_invoice'] as $ri) {
                    $iv['amount'] += $ri['rcp_inv_amount'];
                    $total_payment += $ri['rcp_inv_amount'];

                    $feeID = $ri['fi_id'];
                    $receipt_no = $ri['rcp_inv_rcp_no'];
                    $bill_paid = $ri['rcp_inv_amount'];
                    $currencyPaid = $ri['cur_symbol_prefix'];
                }

                $inv[] = $iv;
            }
        }

        //non-invoice
        $nonInvoiceDb = new Studentfinance_Model_DbTable_NonInvoice();
        $non_invoice_list = $nonInvoiceDb->getNonInvoiceFromReceipt($receiptID);

        if ($non_invoice_list) {

            foreach ($non_invoice_list as $non_invoice) {
                $iv = array(
                    'invoice_no' => $non_invoice['fi_name'],
                    'amount' => $non_invoice['ni_amount'],
                    'currency' => $non_invoice['cur_symbol_prefix'],
                    'description' => $non_invoice['rcp_description']
                );

                $total_payment += $non_invoice['ni_amount'];

                $inv[] = $iv;
            }
        }

        //$dash = str_replace("PM",'',$receipt['rcp_no']);
        //$dash = str_replace("-",'0',$dash);
        //$fieldValues['[Receipt No]'] = $dash.''.$rprint_tot; //str_replace("PM",'',$receipt['rcp_no']);
        $fieldValues['[Receipt No]'] = $receipt['rcp_no'].$rprint_tot;


        $fieldValues['[Data Loop]'] = '<table width="100%" cellpadding="2" cellspacing="0" border="0">
                                        <tr>
                                        <th width="5%" style="border-bottom:2px solid #000000;border-top: 2px solid #000000;">No.</th>
                                        <th width="13%" style="border-bottom:2px solid #000000;border-top: 2px solid #000000;text-align: left;">Invoice No.</th>
                                        <th width="46%" style="border-bottom:2px solid #000000;border-top: 2px solid #000000;text-align: left;">Description</th>
                                        <th width="17%" style="border-bottom:2px solid #000000;border-top: 2px solid #000000;text-align: right;">Invoiced Amount (RM)&nbsp;&nbsp;</th>
                                        <th width="19%"style="border-bottom:2px solid #000000;border-top: 2px solid #000000;text-align: right;">Payment Amount (RM)&nbsp;&nbsp;</th>
                                        </tr>';
        $n = 1;
        for ($i = 0; $i < sizeof($inv); $i++) {
            $invoice = $inv[$i];

            $fieldValues['[Data Loop]'] .= '<tr>';
            $fieldValues['[Data Loop]'] .= '<td  align="center" >' . $n . '</td>';
            $fieldValues['[Data Loop]'] .= '<td  align="" >' . $invoice['invoice_no'] . '</td>';
            $fieldValues['[Data Loop]'] .= '<td  align="" >' . strtoupper($invoice['description']) . '</td>';
            $fieldValues['[Data Loop]'] .= '<td align="right" >' . $currency->toCurrency($invoice['bill_amount'], array('display' => false)) . '&nbsp;</td>';
            $fieldValues['[Data Loop]'] .= '<td align="right" >' . $currency->toCurrency($invoice['amount'], array('display' => false)) . '&nbsp;</td>';
            $fieldValues['[Data Loop]'] .= '</tr>';
            $n++;
        }

        $totalloop = 8;
        $totalreceipt = count($receipt['rcp_adv_payment_amt']) + sizeof($inv);
        $totalloop = $totalloop - $totalreceipt;

        if ($inv && $receipt['rcp_adv_payment_amt']<=0) {
            if (sizeof($inv) < $totalloop) {
                for ($ii = 0; $ii < $totalloop; $ii++) {
                    $fieldValues['[Data Loop]'] .= '<tr><td colspan="5">&nbsp;</td></tr>';
                }
            }
        }

        //Excess Payment
//        $n = 1;
        if ($receipt['rcp_adv_payment_amt'] > 0) {
            $fieldValues['[Data Loop]'] .= '<tr>';
            $fieldValues['[Data Loop]'] .= '<td  align="center" >' . $n . '</td>';
            $fieldValues['[Data Loop]'] .= '<td  align="" >' . $receipt['advpy_fomulir'] . '</td>';
            $fieldValues['[Data Loop]'] .= '<td  align="" >' . strtoupper($receipt['rcp_description']) . '</td>';
            $fieldValues['[Data Loop]'] .= '<td align="right" >' . $currency->toCurrency($receipt['rcp_adv_payment_amt'], array('display' => false)) . '&nbsp;</td>';
            $fieldValues['[Data Loop]'] .= '<td align="right" >' . $currency->toCurrency($receipt['rcp_adv_payment_amt'], array('display' => false)) . '&nbsp;</td>';
            $fieldValues['[Data Loop]'] .= '</tr>';

            $total_payment = $total_payment + $receipt['rcp_adv_payment_amt'];
            $n++;

            for ($ii=0;$ii<$totalloop;$ii++) {
                $fieldValues['[Data Loop]'] .= '<tr><td colspan="5">&nbsp;</td></tr>';
            }
        }


        //
        $fieldValues['[Data Loop]'] .= '<tr><td style="border-bottom:2px solid #000000;border-top: 2px solid #000000;" colspan="4" align="right"><b>TOTAL :</b></td><td align="right" style="border-bottom:2px solid #000000;border-top: 2px solid #000000;"><b>' . $currency->toCurrency($total_payment, array('currency' => $receipt['cur_code'])) . '</b>&nbsp;</td></tr>';
        $fieldValues['[Data Loop]'] .= '</table>';

        //number to words
        $total_amount_in_text = convert_number_to_words($total_payment);


        //invoice & non-invoice data
        //$fieldValues['[Amount in Text]'] = $receipt['cur_symbol_prefix'] . ": " . $total_amount_in_text . " Only";
        $fieldValues['[Amount in Text]'] = strtoupper($receipt['cur_desc_default_language']) . ": <br><br>" . strtoupper($total_amount_in_text) . " ONLY";
        $fieldValues['[Pay Mode]'] = strtoupper($receipt['payment_mode']);
        $fieldValues['[Pay Reference]'] = strtoupper($receipt['p_cheque_no']);

        $fieldValues['[Recepient Name]'] = mb_convert_case($receipt['name'], MB_CASE_TITLE);
        //$fieldValues['[Amount in Text]'] =  "RINGGIT MALAYSIA " . ": " . $total_amount_in_text . " Only";

        //$fieldValues['[Payment Mode]'] = "Payment Mode " . ": " . $receipt['p_payment_mode_id'];

        require_once '../library/dompdf/dompdf_config.inc.php';
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');

        $templateDb = new App_Model_Communication_DbTable_Template();
        $template = $templateDb->getTemplateContent(278, 'en_US');


        $html = $template['tpl_content'];

        foreach ($fieldValues as $key => $value) {
            $html = str_replace($key, $value, $html);
        }

        $test = "<table style=\"width:100%\">
	<tbody>
		<tr>
			<td width=\"64%\"><strong>International University of Malaya-Wales Sdn. Bhd. </strong>(958963-T) <strong>&nbsp; &nbsp;</strong></td>
			<td><img alt=\"\" src=\"http://iumw-sms.mtcsb.my/images/call.png\" style=\"height:15; width:15\" /><strong>603 2617 3000</strong></td>
			<td><img alt=\"\" src=\"http://iumw-sms.mtcsb.my/images/email.png\" style=\"height:15; width:15\" /><span style=\"color:#006699\"><strong>finance@iumw.edu.my</strong></span></td>
		</tr>
		<tr>
			<td width=\"64%\">1st Floor, Block A, City Campus, Jalan Tun Ismail, 50480 Kuala Lumpur, Malaysia.</td>
			<td><img alt=\"\" src=\"http://iumw-sms.mtcsb.my/images/fax.png\" style=\"height:15; width:15\" /><strong>603 2617 3203</strong></td>
			<td><img alt=\"\" src=\"http://iumw-sms.mtcsb.my/images/globe.png\" style=\"height:15; width:18\" /><span style=\"color:#FF0000\"><strong>iumw.edu.my</strong></span></td>
		</tr>
	</tbody>
</table>";
//		echo $html;exit;
        $option = array(
            'content' => $html,
            'save' => false,
            'file_name' => 'receipt',
            //body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
            'css' => '@page { margin: 110px 50px 50px 50px}
						body { font-family: Calibri; font-size:12px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;}',

            'header' => '',
            /*'header' => '<script type="text/php">
					if ( isset($pdf) ) {

					  $header = $pdf->open_object();

					  $w = $pdf->get_width();
					  $h = $pdf->get_height();

					  $color = array(0,0,0);


					  $img_w = 150;
					  $img_h = 50;
                      //$pdf->image("images/iumwlogo_receipt.png",  $w-($img_w+35), 20, $img_w, $img_h);


					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (3.5 * $text_height)-10;
					 // $pdf->line(10, $y, $w - 10, $y, $color, 1);

			          $pdf->close_object();

					  $pdf->add_object($header, "all");



					}
					</script>',*/
            //'footer' => $test,
            'footer' => '',
//            'footer' => '<script type="text/php">
//					if ( isset($pdf) ) {
//
//			          $footer = $pdf->open_object();
//
//					  $font = Font_Metrics::get_font("Helvetica", "bold");
//
//					   $size = 8;
//					  $color = array(0,0,0);
//					  $text_height = Font_Metrics::get_font_height($font, $size)+2;
//
//					  $w = $pdf->get_width();
//					  $h = $pdf->get_height();
//
//
//					  // Draw a line along the bottom
//					  $y = $h - (3.5 * $text_height)-10;
//					  //$pdf->line(10, $y, $w - 10, $y, $color, 1);
//
//					  //1st row footer
//					  $text = "International University of Malaya-Wales Sdn. Bhd. (958963-T)";
//			         //$text = "#IUMW TEL:+603 2617 3018 / 3087 / 3193 #WEB: www.iumw.edu.my #EMAIL: finance@iumw.edu.my";
//			           $width = Font_Metrics::get_text_width($text, $font, $size);
//				      $y = $h - (3 * $text_height)-10;
//					  $x = ($w - $width) / 2.0;
//
//					  $pdf->page_text($x, $y, $text, $font, $size, $color);
//
//			          //2nd row footer
//			           $font = Font_Metrics::get_font("Helvetica");
//					  //$text = "Note: This is a computer generated document and does not require signature";
//			          $text = "1st Floor, Block A, City Campus, Jalan Tun Ismail, 50480 Kuala Lumpur, Malaysia.";
//			          $width = Font_Metrics::get_text_width($text, $font, $size);
//				      $y = $h - (2 * $text_height)-10;
//					  $x = ($w - $width) / 2.0;
//
//					  $pdf->page_text($x, $y, $text, $font, $size, $color);
//
//					  // Draw a second line along the bottom
//					  $y = $h - (0.5 * $text_height)-10;
//					//  $pdf->line(10, $y, $w - 10, $y, $color, 1);
//
//					  $pdf->close_object();
//
//					  $pdf->add_object($footer, "all");
//
//
//
//
//					}
//					</script>' ,
            'type'=>'landscape',
            'alternate'=> 0
        );
        
        $pdf = generatePdf($option);
        exit;
    }

public function printReceiptOLDAction()
	{
		$this->view->title = 'Receipt';
		
		$student = $this->studentinfo;
		
		$receiptID = $this->_getParam('id', null);
		
		$receiptDB = new Studentfinance_Model_DbTable_Receipt();
		$receipt = $receiptDB->getData($receiptID);
		
		$receipt = $receipt[0];
		
		$address = trim($student["appl_caddress1"]);
        if($student["appl_caddress2"]){
            $address.='<br/>'.$student["appl_caddress2"].' ';
        }
        if($student["appl_cpostcode"]){
        	$address.='<br/>'.$student["appl_cpostcode"].' ';
        }
        if($student["appl_ccity"]!=99){
        	$address.= '<br />'.$student["CCityName"].' ';
        }else{
        	$address.= '<br/ >'.$student["appl_ccity_others"].' ';
        }
        if($student["appl_cstate"]!=99){
        	$address.=$student["CStateName"].' ';
        }else{
       		$address.= $student["appl_cstate_others"];
        }
        	$address.= '<br/>'.$student["CCountryName"];
                    
		
		$fieldValues = array(
     		 '[Applicant Name]'=>strtoupper( $student["appl_fname"].' '.$student["appl_lname"]) , 
			 '[Applicant Address]'=> strtoupper( $address ) ,
			 '[Date]'=> date('d F Y',strtotime($receipt["rcp_receive_date"]))  ,
			 '[Applicant ID]'=> strtoupper($student["registrationId"] ),
			 '[DatePrint]'=> date('d F Y')  ,
			 
//			 '$[LOGO]'=> APPLICATION_URL."/images/logo_text_high.jpg"
      		);
      	
      	$currency = new Zend_Currency(array('currency'=>$receipt['cur_code']));
      		
      	//invoice
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$invoice_list = $invoiceMainDb->getInvoiceFromReceipt($receiptID);
		
		$inv = array();
		$total_payment = 0.00;
		
		if($invoice_list){
			
			foreach ($invoice_list as $invoice){
				$iv = array(
						'invoice_no' => $invoice['bill_number'],
						'amount' => 0,
						'currency' => $invoice['cur_symbol_prefix']
					);
				
				foreach ($invoice['receipt_invoice'] as $ri){
					$iv['amount'] += $ri['rcp_inv_amount'];
					$total_payment += $ri['rcp_inv_amount'];
					
					$feeID = $ri['fi_id'];
					$receipt_no = $ri['rcp_inv_rcp_no'];
					$bill_paid = $ri['rcp_inv_amount'];
					$currencyPaid = $ri['cur_symbol_prefix'];
				}

				$inv[] = $iv;
			}
		}
		
		//non-invoice
		$nonInvoiceDb = new Studentfinance_Model_DbTable_NonInvoice();
		$non_invoice_list = $nonInvoiceDb->getNonInvoiceFromReceipt($receiptID);
		
		if( $non_invoice_list ){
			
			foreach ($non_invoice_list as $non_invoice){
				$iv = array(
						'invoice_no' => $non_invoice['fi_name'],
						'amount' => $non_invoice['ni_amount'],
						'currency' => $non_invoice['cur_symbol_prefix']
				);
				
				$total_payment += $non_invoice['ni_amount'];
			
				$inv[] = $iv;
			}
		}

		
      		
		
      		
      	$fieldValues['[Receipt No]'] = $receipt['rcp_no'];
		
		$fieldValues['[Data Loop]'] = '<table width="100%" cellpadding="2" cellspacing="0" style="border:1px solid #000000;"><tr><th width="50%" style="border:1px solid #000000;">Invoice No.</th><th style="border:1px solid #000000;">Amount Paid</th></tr>';
		for ($i=0; $i<sizeof($inv); $i++){
			$invoice = $inv[$i];
			
			$fieldValues['[Data Loop]'] .= '<tr>';
			$fieldValues['[Data Loop]'] .= '<td  align="center" style="border:1px solid #000000;">'.$invoice['invoice_no'].'</td>';
			$fieldValues['[Data Loop]'] .= '<td align="right" style="border:1px solid #000000;">'.$currency->toCurrency($invoice['amount'], array('display'=>false)).'&nbsp;</td>';
			$fieldValues['[Data Loop]'] .= '</tr>';
		}

		//Excess Payment
		if ( $receipt['rcp_adv_payment_amt'] > 0 )
		{
			$fieldValues['[Data Loop]'] .= '<tr>';
			$fieldValues['[Data Loop]'] .= '<td  align="center" style="border:1px solid #000000;">Excess Payment</td>';
			$fieldValues['[Data Loop]'] .= '<td align="right" style="border:1px solid #000000;">'.$currency->toCurrency($receipt['rcp_adv_payment_amt'], array('display'=>false)).'&nbsp;</td>';
			$fieldValues['[Data Loop]'] .= '</tr>';

			$total_payment = $total_payment + $receipt['rcp_adv_payment_amt'];
		}

		//
		$fieldValues['[Data Loop]'] .= '<tr><td style="border:1px solid #000000;"><b>Total</b></td><td align="right" style="border:1px solid #000000;"><b>'.$currency->toCurrency($total_payment, array('currency'=>$receipt['cur_code'])).'</b>&nbsp;</td></tr>';
		$fieldValues['[Data Loop]'] .= '</table>';
		
		//number to words
		$total_amount_in_text =  convert_number_to_words($total_payment);
		

		//invoice & non-invoice data
		$fieldValues['[Amount in Text]'] = $receipt['cur_symbol_prefix'].": ".$total_amount_in_text." Only";
		
		
	    require_once '../library/dompdf/dompdf_config.inc.php';
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');
		
		$templateDb = new App_Model_Communication_DbTable_Template();
		$template = $templateDb->getTemplateContent(22,'en_US');
					
		$html = $template['tpl_content'];
		
		foreach ($fieldValues as $key=>$value){
			$html = str_replace($key,$value,$html);	
		}

//		echo $html;exit;
		$option = array(
			'content' => $html,
			'save' => false,
			'file_name' => 'receipt',
			'css' => '@page { margin: 110px 50px 50px 50px}
						body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
						table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
						table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
						table.tftable tr {background-color:#ffffff;}
						table.tftable td {font-size:10px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}',
			
			'header' => '<script type="text/php">
					if ( isset($pdf) ) {
				
					  $header = $pdf->open_object();
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					  
					  $color = array(0,0,0);
						
					  $img_w = 172; 
					  $img_h = 69;
                      //$pdf->image("images/logo_iumw.png",  $w-($img_w+35), 20, $img_w, $img_h);
					  
					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
			
			          $pdf->close_object();
			
					  $pdf->add_object($header, "all");
					}
					</script>',
			'footer' => '<script type="text/php">
					if ( isset($pdf) ) {
				
			          $footer = $pdf->open_object();
			
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
					  $color = array(0,0,0);
					  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					 
				
					  // Draw a line along the bottom
					  $y = $h - (3.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  //1st row footer
					  $text = "#MALAYSIA TEL:+603 7651 4000 #FAX:+603 7651 4094 #WEB: www.inceif.org #EMAIL: bursary@inceif.org";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (3 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
					 
			          //2nd row footer
					  $text = "Note: This is a computer generated document and does not require signature";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (2 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
				
					  // Draw a second line along the bottom
					  $y = $h - (0.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  $pdf->close_object();
			
					  $pdf->add_object($footer, "all");
					 
					}
					</script>'
		);
		
		$pdf = generatePdf($option);
		
		exit;
	}


}