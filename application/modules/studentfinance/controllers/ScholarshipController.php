<?php

class Studentfinance_ScholarshipController extends Zend_Controller_Action
{

    public function init()
    {
        $session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->scholarModel = new Studentfinance_Model_DbTable_Scholarship();
                $this->reappmodel = new Studentfinance_Model_DbTable_Reapplication();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
                    'en_US' => 'English',
                    'ms_MY' => 'Malay'
                );

                Zend_Layout::getMvcInstance()->assign('navActive', 'financeAid');
		
		$this->auth = Zend_Auth::getInstance();
		$this->studentInfo = $this->auth->getIdentity()->info;
                
                //check availablelity
                $this->checkAvailability = $this->scholarModel->getSemesterApply($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram']);
                //$this->checkCourseAvailability = $this->scholarModel->getCourseAvailability($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram']);
	}

    public function indexAction()
    {
        $availability = true;
        
        if (empty($this->checkAvailability)){
            //redirect
            //$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'expired-page'),'default',true));
            $availability = false;
        }
        
        $this->view->availability = $availability;
        
    	$this->view->title = "Financial Assistance";

		$info = $this->scholarModel->getDataByStudent($this->studentInfo['IdStudentRegistration']);

                $checkCourseAvailability = false;
                if ($info){
                    $checkCourseAvailability = $this->scholarModel->getCourseAvailability($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram'], $info['sa_semester_id']);

                    if ($availability == true){
                        foreach ($this->checkAvailability as $avloop){
                            if ($avloop['semester_id']!=$info['sa_semester_id']){
                                $info = false;
                            }
                        }
                    }
                }
                $this->view->courseAvailability = $checkCourseAvailability;

		if ( !empty($info) )
		{
			switch ( $info['sa_status'] )
			{
                            case 1 : $info['sa_status_name'] = 'APPLIED';	break;
                            case 2 : $info['sa_status_name'] = 'APPROVED';	break;
                            case 3 : $info['sa_status_name'] = 'REJECTED';	break;
                            case 4 : $info['sa_status_name'] = 'COMPLETED';	break;
                            case 5 : $info['sa_status_name'] = 'INCOMPLETE';    break;
                            case 6 : $info['sa_status_name'] = 'CANCEL';	break;
                            case 7 : $info['sa_status_name'] = 'APPROVED';	break;
			}
		}

		$this->view->info = $info;
                
                $upl = $this->scholarModel->viewUpl($info['sa_id']);
                $this->view->upl = $upl;
    }
    
    public function redirectAction(){
        $info = $this->scholarModel->getDataByStudent($this->studentInfo['IdStudentRegistration']);
        //var_dump($info); exit;
        $semester = $this->scholarModel->getSemesterApply($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram']);

        $checkCourseAvailability = false;
        if ($info && $info['sa_status']==7){
            $checkCourseAvailability = $this->scholarModel->getCourseAvailabilitySemester($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram'], $info['sa_semester_id']);
            $this->view->courseAvailability = $checkCourseAvailability;
        }
		//var_dump($this->checkAvailability);
        //exit;
        if ($info && $info['sa_status']==7){
            $checkReAppAvailability = $this->reappmodel->getSemesterApply($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram']);

            $info2 = $this->reappmodel->getReApplicationList($this->studentInfo['IdStudentRegistration'], $semester[0]['IdSemesterMaster']);

            if ((empty($checkCourseAvailability) && $checkReAppAvailability) || $info2){
                if(!empty($info2)){
                    $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'index'),'default',true));
                }else{
                    $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'apply'),'default',true));
                }
            }else{
                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'apply'),'default',true));
            }
        }else if ($info && $info['sa_status']==2) {
            $checkCourseAvailability = $this->scholarModel->getCourseAvailability($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram'], $info['sa_semester_id']);

            if ($this->checkAvailability){
                foreach ($this->checkAvailability as $avloop){
                    if ($avloop['semester_id']!=$info['sa_semester_id']){
                        $info = false;
                    }
                }
            }else{
                $info = false;
            }

            //var_dump($checkCourseAvailability); exit;

            if ($info == false && !$checkCourseAvailability){
                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'apply'),'default',true));
            }else {
                $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'scholarship', 'action' => 'apply'), 'default', true));
            }
        }else{
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'apply'),'default',true));
        }
        exit;
    }
	
	public function viewAction()
	{
            $availability = true;
            
            if (empty($this->checkAvailability)){
                //redirect
                //$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true));
                $availability = false;
            }
            
            $this->view->availability = $availability;
            
		$this->view->title = "Financial Assistance Application";
		
		$id = $this->_getParam('id');
		
		$info = $this->scholarModel->getData($id);
                $upl = $this->scholarModel->viewUpl($id);
                $this->view->id = $id;
		$this->view->info = $info;
                $this->view->upl = $upl;
                
                //get highest education
                if ($this->studentInfo['IdScheme']==1){
                    $education = $this->scholarModel->getHighestLevelEdu($this->studentInfo['sp_id']);
                    $this->view->education = $education;
                }else{
                    $education = $this->scholarModel->getHighestLevelEdu2($this->studentInfo['IdProgram']);
                    
                    $registered_semester = $this->scholarModel->getListSemesterRegistered($this->studentInfo['IdStudentRegistration']);
                    $student_grade = false;
                    foreach($registered_semester as $index=>$semester){
                        //get subject registered in each semester
                        if($semester["IsCountable"]==1){				
                            $subject_list = $this->scholarModel->getListCourseRegisteredBySemesterWithAttendanceStatus($this->studentInfo['IdStudentRegistration'],$semester['IdSemesterMain']);
                        }else{			
                            $subject_list = $this->scholarModel->getListCourseRegisteredBySemesterWithoutAttendance($this->studentInfo['IdStudentRegistration'],$semester['IdSemesterMain']);
                        }
                        //var_dump($subject_list);
                        $registered_semester[$index]['subjects']=$subject_list;

                        //get grade info
                        $student_grade = $this->scholarModel->getStudentGrade($this->studentInfo['IdStudentRegistration'],$semester['IdSemesterMain']);
                    }

                    if (!$student_grade){
                        $student_grade['sg_cum_credithour']=0;
                        $student_grade['sg_cgpa']=0;
                    }
                    
                    $education['ae_result'] = $student_grade['sg_cgpa'];
                    
                    $this->view->education = $education;
                }
                
                $eduLvlList = $this->scholarModel->getLevelEducationList();
                $this->view->eduLvlList = $eduLvlList;
                
                $currency = $this->scholarModel->getCurrency();
                $this->view->currency = $currency;
                
                $semester = $this->scholarModel->getSemester($this->studentInfo['IdScheme']);
                $this->view->semester = $semester;
                
                $industryW = $this->scholarModel->getIndustryWeightageList();
                $this->view->industryW = $industryW;
                
                $courseList = $this->scholarModel->getApplicationCourse($id);
                $this->view->courseList = $courseList;
	}

	public function coursesAction(){
        $id = $this->_getParam('id');
		$this->view->title = "Courses";
                
        $info = $this->scholarModel->getData($id);

        $checkCourseAvailability = $this->scholarModel->getCourseAvailability($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram'], $info['sa_semester_id']);
        $this->view->courseAvailability = $checkCourseAvailability;

        $coveredCourseInfo = $this->scholarModel->getCourseCovered($info['sa_semester_id']);

        if ($coveredCourseInfo){
            $coveredCourse = $coveredCourseInfo['total_course'];
        }else{
            $coveredCourse = 0;
        }

        $this->view->coveredCourse = $coveredCourse;
		
		$db = getDB();
		$select = $db->select()
            ->from(array('a' =>'tbl_studentregsubjects'))
            ->joinLeft(array('s'=>'tbl_subjectmaster'),'a.IdSubject=s.IdSubject',array('s.SubjectName','s.SubCode'))
            ->where('a.IdStudentRegistration = ?', $this->studentInfo['IdStudentRegistration'])
            ->where('a.IdSemesterMain = ?', $info['sa_semester_id'])
            ->where('a.Active <> ?', 3)
            ->where('a.IdSubject <> ?', 84)
            ->where('a.IdSubject <> ?', 85)
            ->where('a.IdSubject <> ?', 126);

		$courses = $db->fetchAll($select);
                
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            if (isset($formData['sub']) && count($formData['sub']) > 0){
                foreach ($formData['sub'] as $key => $subloop){
                    $datasub = array(
                        'sac_said'=>$id,
                        'sac_subjectid'=>$subloop,
                        'sac_regsubid'=>$key,
                        'sac_upddate'=>date('Y-m-d H:i:s'),
                        'sac_updby'=>$this->studentInfo['IdStudentRegistration'],
                        'sac_type'=>0
                    );
                    $this->scholarModel->storeSubject($datasub);

                    $this->scholarModel->set_status($id, 7);
                }
            }

            $courseList3 = $this->scholarModel->getApplicationCourse($id, true);

            if ($courseList3) {
                foreach ($courseList3 as $courseLoop3) { //subject loop start
                    if ($courseLoop3['sac_receiptid']==0){
                        $arrinvdet = array();
                        $totalamount = 0;
                        $itemList = $this->scholarModel->getSubjectItem($courseLoop3['sac_regsubid']);

                        if ($itemList) {
                            foreach ($itemList as $itemLoop) {
                                if ($itemLoop['invoice_id'] != null) {
                                    $invoiceList = $this->scholarModel->getInvoiceDetails($itemLoop['invoice_id'], $courseLoop3['sac_subjectid']);
                                    $invmaininfo = $this->scholarModel->getInvoiceMain($itemLoop['invoice_id']);

                                    $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
                                    $currency = $currencyDb->getCurrentExchangeRate($invmaininfo['currency_id']);

                                    if ($invoiceList) {
                                        foreach ($invoiceList as $invoiceLoop) {

                                            $financialaidcoverage = $this->scholarModel->financialAidCoverage($invoiceLoop['fi_id']);

                                            //todo generate bayaran
                                            if ($financialaidcoverage) {
                                                if ($financialaidcoverage['calculation_mode'] == 1) { //amount
                                                    if ($financialaidcoverage['amount'] > $invoiceLoop['amt']) {
                                                        if ($invmaininfo['currency_id'] != 1){
                                                            $amount = ($invoiceLoop['amt']*$currency['cr_exchange_rate']);
                                                        }else{
                                                            $amount = $invoiceLoop['amt'];
                                                        }
                                                    } else {
                                                        if ($invmaininfo['currency_id'] != 1){
                                                            $amount = ($financialaidcoverage['amount']*$currency['cr_exchange_rate']);
                                                        }else{
                                                            $amount = $financialaidcoverage['amount'];
                                                        }
                                                    }
                                                } else { //percentage
                                                    $amount = (($financialaidcoverage['amount'] / 100) * $invoiceLoop['amt']);
                                                }

                                                $totalamount = ($totalamount + $amount);

                                                $desc = $courseLoop3['SubCode'] . ' - ' . $courseLoop3['SubjectName'] . ', ' . $invoiceLoop['fee_item_description'];

                                                $cnDetailData = array(
                                                    //'subregdetid' =>  $itemLoop['id'],
                                                    'sp_invoice_id' => $invoiceLoop['invoice_main_id'],
                                                    'sp_invoice_det_id' => $invoiceLoop['id'],
                                                    'sp_amount' => $amount,
                                                    'sp_balance' => $amount,
                                                    'sp_receipt' => $amount,
                                                    'sp_description' => $desc
                                                );
                                                $arrinvdet[] = $cnDetailData; //array push
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (count($arrinvdet) > 0){
                        $invoiceclass = new icampus_Function_Studentfinance_Invoice();
                        $bill_no =  $invoiceclass->getBillSeq(11, date('Y'));

                        $data_dn = array(
                            'sp_fomulir_id' => $bill_no,
                            'sp_txn_id' => $info['transaction_id'],
                            'sp_IdStudentRegistration' => $info['IdStudentRegistration'],
                            //'sp_batch_id' => $btch_id,
                            'sp_amount' => $totalamount,
                            'sp_balance' => $totalamount,
                            'sp_type' => 3,
                            'sp_type_id' => 1,
                            'sp_invoice_date' => date('Y-m-d'),
                            'sp_description' => 'Financial Assistance',
                            'sp_currency_id' => $invmaininfo['currency_id'],
                            'sp_exchange_rate' => $invmaininfo['exchange_rate'],
                            'sp_creator'=>1,
                            'sp_create_date'=>date('Y-m-d H:i:s'),
                            'sp_status' => 'E',//entry
                        );
                        $spinvmainid = $this->scholarModel->saveSponsorInvMain($data_dn);

                        //todo generate receipt
                        $receipt_no = $this->getReceiptNoSeq();

                        $receipt_data = array(
                            'rcp_no' => $receipt_no,
                            'rcp_account_code' => 96,
                            'rcp_date' => date('Y-m-d H:i:s'),
                            'rcp_receive_date' => date('Y-m-d'),
                            'rcp_batch_id' => 0,
                            'rcp_type' => 3,
                            'rcp_type_id' => 1,
                            'rcp_description' => 'Fisabilillah - '.$info['SemesterMainName'].' - '.$courseLoop3['SubCode'] . ' - ' . $courseLoop3['SubjectName'],
                            'rcp_amount' => $totalamount,
                            'rcp_adv_amount' => 0,
                            'rcp_gainloss' => 0,
                            'rcp_gainloss_amt' => 0,
                            'rcp_cur_id' => $invmaininfo['currency_id'],
                            'rcp_exchange_rate' => $currency['cr_id'],
                            'p_payment_mode_id' => 20,
                            'p_cheque_no' => null,
                            'p_doc_bank' => null,
                            //'p_doc_branch' => isset($formData['payment_doc_branch'])?$formData['payment_doc_branch']:null,
                            'p_terminal_id' => null,
                            'p_card_no' => null,
                            'rcp_create_by'=>1
                        );
                        $receipt_id = $this->scholarModel->insertReceipt($receipt_data);

                        //todo update receipt id
                        $receiptData = array(
                            'sac_receiptid'=>$receipt_id
                        );
                        $this->scholarModel->updateApplicationCourse($receiptData, $courseLoop3['sac_id']);

                        $data_rcp_stud = array(
                            'rcp_inv_rcp_id' => $receipt_id,
                            'IdStudentRegistration' => $info['IdStudentRegistration'],
                            'rcp_inv_amount' => $totalamount,
                            'rcp_adv_amount' => 0,
                            'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                            'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                            'rcp_inv_create_by'=>1
                        );
                        $db->insert('sponsor_invoice_receipt_student',$data_rcp_stud);
                        $idStd = $db->lastInsertId();

                        foreach ($arrinvdet as $arrinvdetLoop){
                            $arrinvdetLoop['sp_id']=$spinvmainid;

                            $spinvdetailid = $this->scholarModel->saveSponsorInvDetail($arrinvdetLoop);

                            $data_rcp_inv = array(
                                'rcp_inv_rcp_id' => $receipt_id,
                                'rcp_inv_sp_id' => $spinvmainid,
                                'rcp_inv_student' => $idStd,
                                'rcp_inv_sponsor_dtl_id' => $spinvdetailid,
                                'rcp_inv_amount' => $arrinvdetLoop['sp_amount'],
                                'rcp_inv_amount_default' => $arrinvdetLoop['sp_amount'],
                                'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                                'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                'rcp_inv_create_by'=>1
                            );
                            $this->scholarModel->insertSponsorReceipt($data_rcp_inv);
                        }

                        $this->scholarModel->approveSponsorInvoive($receipt_id);
                    }
                } //subject loop end
            }

            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => "Subject Submitted"));
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true));
        }

        $this->view->courses = $courses;

        $courseList = $this->scholarModel->getApplicationCourse($id);
        $this->view->courseList = $courseList;

        $courseArr = array();

        if ($courseList){
            foreach ($courseList as $courseLoop){
                array_push($courseArr, $courseLoop['IdSubject']);
            }
        }

        $this->view->courseArr = $courseArr;

        if (empty($checkCourseAvailability)){
            //redirect
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true));
        }
	}

	public function applyAction(){
            $availability = true;
            
            if (empty($this->checkAvailability)){
                //redirect
                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true));
                $availability = false;
            }
            
            $infocheck = $this->scholarModel->getDataByStudent($this->studentInfo['IdStudentRegistration']);
            
            if ($infocheck){
                if ($availability == true){
                    foreach ($this->checkAvailability as $avloop){
                        if ($avloop['semester_id']!=$infocheck['sa_semester_id']){
                            $infocheck = false;
                        }
                    }
                }

                if ($infocheck != false) {
                    //redirect
                    $this->_redirect($this->view->url(array('module' => 'studentfinance', 'controller' => 'scholarship', 'action' => 'index'), 'default', true));
                }
            }
            
            $this->view->title = "Financial Assistance Application";
            
            $recordDb = new AcademicInfo_Model_DbTable_Academicprogress();

            //get highest education
            if ($this->studentInfo['IdScheme']==1){
                $education = $this->scholarModel->getHighestLevelEdu($this->studentInfo['sp_id']);
                $this->view->education = $education;
            }else{
                $education = $this->scholarModel->getHighestLevelEdu2($this->studentInfo['IdProgram']);

                $registered_semester = $this->scholarModel->getListSemesterRegistered($this->studentInfo['IdStudentRegistration']);
                $student_grade = false;
                foreach($registered_semester as $index=>$semester){
                    $subsem = $recordDb->getSemesterById($semester['IdSemesterMain']);
                    
                    $publishresult = false;

                    if ($subsem){
                        $publishInfo = $recordDb->getPublishResult($this->studentInfo['IdProgram'], $subsem['IdSemesterMaster']);

                        if ($publishInfo){
                            $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
                            $publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));

                            if ($publishInfo['pm_cdate'] >= $publishInfo['pm_date_new']){
                                $publishresult = true;
                            }
                        }
                    }
                    
                    //get subject registered in each semester
                    if($semester["IsCountable"]==1){				
                        $subject_list = $this->scholarModel->getListCourseRegisteredBySemesterWithAttendanceStatus($this->studentInfo['IdStudentRegistration'],$semester['IdSemesterMain']);
                    }else{			
                        $subject_list = $this->scholarModel->getListCourseRegisteredBySemesterWithoutAttendance($this->studentInfo['IdStudentRegistration'],$semester['IdSemesterMain']);
                    }
                    $registered_semester[$index]['subjects']=$subject_list;

                    //get grade info
                    $student_grade = $this->scholarModel->getStudentGrade($this->studentInfo['IdStudentRegistration'],$semester['IdSemesterMain']);
                }

                if (!$student_grade){
                    $student_grade['sg_cum_credithour']=0;
                    $student_grade['sg_cgpa']=0;
                }
                
                if($education['Award'] == 628){
                    $award = 26;
                }else if ($education['Award'] == 629){
                    $award = 22;
                }else if ($education['Award'] == 650){
                    $award = 26;
                }

                $education['ae_result'] = $student_grade['sg_cgpa'];
                $education['ae_qualification'] = $award;

                $this->view->education = $education;
            }

            $eduLvlList = $this->scholarModel->getLevelEducationList();
            $this->view->eduLvlList = $eduLvlList;

            $currency = $this->scholarModel->getCurrency();
            $this->view->currency = $currency;

            $semester = $this->scholarModel->getSemesterApply($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram']);
            //var_dump($semester);
            $this->view->semester = $semester;

            $industryW = $this->scholarModel->getIndustryWeightageList();
            $this->view->industryW = $industryW;

            if ($this->getRequest()->isPost()){
                $formData = $this->getRequest()->getPost();
                //$check = $this->scholarModel->getDataByStudent($this->studentInfo['IdStudentRegistration']);
                
                //if (empty($check)){
                    //insert
                    $data = array(
                        'sa_cust_id' => $this->studentInfo['IdStudentRegistration'],
                        'sa_cust_type' => 1,
                        'sa_status' => 1,
                        'sa_semester_id' => $formData['semester'],
                        'sa_createddt' => new Zend_Db_Expr('NOW()'),
                        'sa_createdby' => 1
                    );
                    $app_id = $this->scholarModel->addData($data);

                    //insert details
                    $details = array(	
                        'application_id' => $app_id,
                        'student_id' => $this->studentInfo['IdStudentRegistration'],
                        'totaldependent' => $formData['totaldependent'],
                        'empstatus' => $formData['empstatus'],
                        'cgpa' => $formData['cgpa'],
                        'edulevel' => $formData['edulevel'],
                        'industry' => isset($formData['industry']) ? $formData['industry']:0,
                        'income' => isset($formData['income']) ? $formData['income']:0,
                        'currency' => isset($formData['currency']) ? $formData['currency']:0,
                        'compaddress' => isset($formData['compaddress']) ? $formData['compaddress']:'',
                        'compemail' => isset($formData['compemail']) ? $formData['compemail']:'',
                        //'app_year' => $formData['app_year'],
                        //'semester' => $formData['semester'],
                        'created_date' => new Zend_Db_Expr('NOW()')
                    );
                    $this->scholarModel->addDetails($details);
                    
                    $ts = $this->countTotalScore($app_id);
                    $dataScore = array(
                        'sa_score'=>$ts
                    );
                    $this->scholarModel->updateData($dataScore, $app_id);
                    
                    $this->printApp($app_id);
                //}
                
                //redirect
                $this->_helper->flashMessenger->addMessage(array('success' => "Application submitted successfully."));
                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true));
            }
	}
        
        public function printAction(){
            $id = $this->_getParam('id');
            $this->printApp($id);
            exit;
        }
        
        public function printApp($id){
            //$id = $this->_getParam('id');
            
            global $globalinfo;
            global $globalProgramList;
            global $globalstudentinfo;
            global $globaleducation;
            global $globalindustryW;
            global $globalsemesterInfo;
            global $globalhead;
            //var_dump($this->studentInfo); exit;
            $globalstudentinfo = $this->studentInfo;
		
            $info = $this->scholarModel->getData($id);
            $globalinfo = $info;
            
            $programList = $this->scholarModel->getProgramList();
            $globalProgramList = $programList;
            
            $semesterInfo = $this->scholarModel->getSemesterById($info['sa_semester_id']);
            $globalsemesterInfo = $semesterInfo;
					
            $head = $this->scholarModel->getHeadTemplate($this->studentInfo['IdProgram']);
            $globalhead = $head;
            
            //get highest education
            if ($this->studentInfo['IdScheme']==1){
                $education = $this->scholarModel->getHighestLevelEdu($this->studentInfo['sp_id']);
                $globaleducation = $education;
            }else{
                $education = $this->scholarModel->getHighestLevelEdu2($this->studentInfo['IdProgram']);

                $registered_semester = $this->scholarModel->getListSemesterRegistered($this->studentInfo['IdStudentRegistration']);
                $student_grade = false;
                foreach($registered_semester as $index=>$semester){
                    //get subject registered in each semester
                    if($semester["IsCountable"]==1){				
                        $subject_list = $this->scholarModel->getListCourseRegisteredBySemesterWithAttendanceStatus($this->studentInfo['IdStudentRegistration'],$semester['IdSemesterMain']);
                    }else{			
                        $subject_list = $this->scholarModel->getListCourseRegisteredBySemesterWithoutAttendance($this->studentInfo['IdStudentRegistration'],$semester['IdSemesterMain']);
                    }
                    //var_dump($subject_list);
                    $registered_semester[$index]['subjects']=$subject_list;

                    //get grade info
                    $student_grade = $this->scholarModel->getStudentGrade($this->studentInfo['IdStudentRegistration'],$semester['IdSemesterMain']);
                }

                if (!$student_grade){
                    $student_grade['sg_cum_credithour']=0;
                    $student_grade['sg_cgpa']=0;
                }

                $education['ae_result'] = $student_grade['sg_cgpa'];

                $globaleducation = $education;
            }

            $industryW = $this->scholarModel->getIndustryWeightageList();
            $globalindustryW = $industryW;
            
            $fieldValues = array(
                '$[LOGO]'=> APPLICATION_URL."/images/logo_text_high.jpg"
            );
            
            require_once '../library/dompdf/dompdf_config.inc.php';
            $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
            $autoloader->pushAutoloader('DOMPDF_autoload');

            $html_template_path = APPLICATION_PATH."/document/template/scholarship.html";

            $html = file_get_contents($html_template_path);
            
            foreach ($fieldValues as $key=>$value){
                $html = str_replace($key,$value,$html);	
            }
            //echo $html; exit;
            $dompdf = new DOMPDF();
            $dompdf->load_html($html);
            $dompdf->set_paper('a4', 'potrait');
            $dompdf->render();
            $output_filename = "financial_assistance.pdf";
            $dompdf->stream($output_filename);

            exit;
        }
        
        public function uploadAction(){
            
            if ($this->getRequest()->isPost()){
		$formData = $this->getRequest()->getPost();
                
                $dir = DOCUMENT_PATH.$this->studentInfo['sp_repository'];
                
                if ( !is_dir($dir) ){
                    if ( mkdir_p($dir) === false ){
                        throw new Exception('Cannot create attachment folder ('.$dir.')');
                    }
                }

                //upload file proses
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, array('min' => 1 , 'max' => 10)); //maybe soon we will allow more?
                $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,jpeg,png,gif,zip,doc,docx', 'case' => false));
                $adapter->addValidator('NotExists', false, $dir);
                $adapter->setDestination($dir);

                $files = $adapter->getFileInfo();
                
                if ($files){
                    foreach ($files as $file=>$info){
                        if ($adapter->isValid($file)){
                            //var_dump($info); exit;
                            $fileOriName = $info['name'];
                            $fileRename = date('YmdHis').'_'.str_replace(' ', '', $fileOriName);
                            $filepath = $info['destination'].'/'.$fileRename;

                            if ($adapter->isUploaded($file)){
                                $adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $file);
                                if ($adapter->receive($file)){
                                    $uploadData = array(
                                        'sau_sa_id'=>$formData['id'], 
                                        'sau_filename'=>$fileOriName, 
                                        'sau_filerename'=>$fileRename, 
                                        'sau_filelocation'=>$this->studentInfo['sp_repository'].'/'.$fileRename, 
                                        'sau_filesize'=>$info['size'], 
                                        'sau_mimetype'=>$info['type'], 
                                        'sau_upddate'=>date('Y-m-d H:i:s'), 
                                        'sau_upduser'=>0
                                    );
                                    //var_dump($uploadData); exit;
                                    $this->scholarModel->uploadfile($uploadData);
                                }
                            }
                        }
                    }
                }
                
                //redirect
                $this->_helper->flashMessenger->addMessage(array('success' => "Upload Success"));
                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'view', 'id'=>$formData['id']),'default',true));
            }else{
                //redirect
                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'index'),'default',true));
            }
        }
        
        public function deleteuplAction(){
            $id = $this->_getParam('id');
            $sa_id = $this->_getParam('sa_id');
            
            $this->scholarModel->deleteUpl($id);
            
            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => "Upload Success"));
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'scholarship', 'action'=>'view', 'id'=>$sa_id),'default',true));
        }
        
        public function countTotalScore($id){
            $info = $this->scholarModel->getData($id);
            
            //count industry
            if ($info['empstatus']==2){
                $industyScore = 10;
            }else{
                $getIndutryW = $this->scholarModel->getIndustryWeightage($info['industry']);
                
                if ($getIndutryW){
                    $industyScore = $getIndutryW['score'];
                }else{
                    $industyScore = 0;
                }
            }
            
            //count age
            $age = date('Y-m-d')-date('Y-m-d', strtotime($this->studentInfo['appl_dob']));
            $getAgeW = $this->scholarModel->getAgeWeightage($age);
            
            if ($getAgeW){
                $ageScore = $getAgeW['score'];
            }else{
                $ageScore = 0;
            }
            
            //contribution score
            $cs = $industyScore+$ageScore;
            
            //academic merit
            if ($info['cgpa'] >= 3.5){
                $getacademicW = $this->scholarModel->getAcademicWeightage(1);
            }else if ($info['cgpa'] <= 3.49 && $info['cgpa'] >=3.0){
                $getacademicW = $this->scholarModel->getAcademicWeightage(2);
            }else if ($info['cgpa'] <= 2.99 && $info['cgpa'] >=2.5){
                $getacademicW = $this->scholarModel->getAcademicWeightage(3);
            }else{
                $getacademicW = $this->scholarModel->getAcademicWeightage(4);
            }
            
            $academicScore = $getacademicW['score'];
            
            //affordability (income)
            $incomeScore = 0; 
            
            if ($info['empstatus']!=2){
                $getincomeW = $this->scholarModel->getIncomeWeightage($info['income'], $info['currency']);
                
                if ($getincomeW){
                    $incomeScore = $getincomeW['score'];
                }
            }else{
                $getincomeW = $this->scholarModel->getIncomeWeightage(0, $info['currency']);

                if ($getincomeW){
                    $incomeScore = $getincomeW['score'];
                }
            }
            
            //affordability (dependent)
            $getDependentW = $this->scholarModel->getDependentWeightage($info['totaldependent']);
            
            $dependentScore = 0;
            
            if ($getDependentW){
                $dependentScore = $getDependentW['score'];
            }
            
            //affordability score
            $as = $incomeScore+$dependentScore;
            
            //total score
            $ts = (0.2*$cs)+(0.3*$academicScore)+(0.5*$as);
            return $ts;
        }

    public function getReceiptNoSeq(){

        $seq_data = array(
            12,
            date('Y'),
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS receipt_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['receipt_no'];
    }
}