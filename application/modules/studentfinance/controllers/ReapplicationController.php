<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Studentfinance_ReapplicationController extends Zend_Controller_Action {

    public function init(){
        $session = new Zend_Session_Namespace('sis');		
        $this->session = $session;

        $this->model = new Studentfinance_Model_DbTable_Reapplication();
        $this->scholarModel = new Studentfinance_Model_DbTable_Scholarship();

        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US'	=> 'English',
            'ms_MY'	=> 'Malay'
        );
        
        Zend_Layout::getMvcInstance()->assign('navActive', 'financeAid');
        
        $this->auth = Zend_Auth::getInstance();
        $this->studentInfo = $this->auth->getIdentity()->info;
        
        //check availablelity
        $this->checkAvailability = $this->model->getSemesterApply($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram']);
    }
    
    public function indexAction(){
        $availability = true;

        if (empty($this->checkAvailability)){
            //redirect
            //$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'expired-page'),'default',true));
            $availability = false;
        }

        $this->view->availability = $availability;
        
        $this->view->title = $this->view->translate('Financial Assistance');

        if ($availability == true) {
            $info = $this->model->getReApplicationList($this->studentInfo['IdStudentRegistration'], $this->checkAvailability[0]['semester_id']);
        }else{
            $info = false;
        }
        
        if (!$info){
            $info = $this->model->getReApplicationList($this->studentInfo['IdStudentRegistration'], $this->auth->getIdentity()->semester['current_semester']);
        }

        $checkCourseAvailability = false;
        if ($info){
            $checkCourseAvailability = $this->model->getCourseAvailability($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram'], $info['sre_semester_id']);

            if ($availability == true){
                foreach ($this->checkAvailability as $avloop){
                    if ($avloop['semester_id']!=$info['sre_semester_id']){
                        $info = false;
                    }
                }
            }
        }
        $this->view->courseAvailability = $checkCourseAvailability;
        
        if (!empty($info)){
            switch ($info['sre_status']){
                case 1 : $info['sre_status_name'] = 'APPLIED';	break;
                case 2 : $info['sre_status_name'] = 'APPROVED';	break;
                case 3 : $info['sre_status_name'] = 'REJECTED';	break;
                case 4 : $info['sre_status_name'] = 'COMPLETED'; break;
                case 5 : $info['sre_status_name'] = 'INCOMPLETE'; break;
                case 6 : $info['sre_status_name'] = 'CANCEL'; break;
                case 7 : $info['sre_status_name'] = 'APPROVED';	break;
            }
        }
        $this->view->info = $info;
    }
    
    public function applyAction(){
        if (empty($this->checkAvailability)){
            //redirect
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'index'),'default',true));
        }

        $infocheck = $this->model->getReApplicationList($this->studentInfo['IdStudentRegistration'], $this->checkAvailability[0]['semester_id']);

        if (!$infocheck){
            $infocheck = $this->model->getReApplicationList($this->studentInfo['IdStudentRegistration'], $this->auth->getIdentity()->semester['current_semester']);
        }

		if ($infocheck){
            if ($this->checkAvailability){
                foreach ($this->checkAvailability as $avloop){
                    if ($avloop['semester_id']!=$infocheck['sre_semester_id']){
                        $infocheck = false;
                    }
                }
            }

            if ($infocheck != false){
                //redirect
                $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'index'),'default',true));
            }
        }
        
        $this->view->title = $this->view->translate('Financial Assistance');
        /*echo '<pre>';
        print_r($this->studentInfo);
        echo '</pre>';
        exit;*/
        $this->view->studentInfo = $this->studentInfo;
        $semList = $this->model->getSemesterApply($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram']);
        $this->view->semList = $semList;
        
        /*if ($this->studentInfo['IdScheme'] == 1){
            $curSem = $this->model->getCurSem($this->studentInfo['IdScheme']);
            $lastSem = $this->model->getLastSem($this->studentInfo['IdScheme'], $curSem['IdSemesterMaster'], $curSem['SemesterMainStartDate']);
            $lastSemsub = $this->model->getRegSubLastSem($this->studentInfo['IdStudentRegistration'], $lastSem['IdSemesterMaster']);
            $this->view->lastSemsub = $lastSemsub;
        }else{
            $regcourse = $this->model->getAllregistered($this->studentInfo['IdStudentRegistration']);
            
            $subject_ids = array();
            
            if ($regcourse){
                foreach ($regcourse as $key =>$regcourseLoop){
                    //$subject_ids[$key] = $regcourseLoop['IdSunject'];
                    array_push($subject_ids, $regcourseLoop['IdSubject']);
                }
            }
            
            $nextSemsub = $this->model->getRemainingCourses($this->studentInfo['IdProgram'], $this->studentInfo['IdLandscape'], $subject_ids);
            $this->view->nextSemsub = $nextSemsub;
        }*/
        
        $sectionC = $this->model->getSectionC($this->studentInfo['IdProgram']);
        $this->view->sectionC = $sectionC;
        
        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            //$infocheck = $this->model->getReApplicationList($this->studentInfo['IdStudentRegistration'], $this->checkAvailability[0]['semester_id']);

            /*if (!$infocheck){
                $infocheck = $this->model->getReApplicationList($this->studentInfo['IdStudentRegistration'], $this->auth->getIdentity()->semester['current_semester']);
            }*/
            

            //if (!$infocheck){
                $data = array(
                    'sre_cust_id' => $this->studentInfo['IdStudentRegistration'], 
                    'sre_cust_type' => 1, 
                    'sre_status' => 1, 
                    'sre_semester_id' => $formData['semester'], 
                    'sre_createddt' => new Zend_Db_Expr('NOW()'),
                    'sre_createdby' => 1
                );
                $id = $this->model->insertSponsorData($data);
            //}
            
            /*if ($this->studentInfo['IdScheme'] != 1){
                if (isset($formData['course']) && count($formData['course']) > 0){
                    foreach ($formData['course'] as $courseLoop){
                        $courseData = array(
                            'src_sreid' => $id, 
                            'src_subjectid' => $courseLoop, 
                            'src_upddate' => new Zend_Db_Expr('NOW()'), 
                            'src_updby' => 1
                        );
                        $this->model->insertSponsorCourse($courseData);
                    }
                }
            }*/
            
            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => "Application submitted successfully."));
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'view', 'id'=>$id),'default',true));
        }
    }
    
    public function viewAction(){
        $availability = true;
        
        if (empty($this->checkAvailability)){
            //redirect
            //$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'expired-page'),'default',true));
            $availability = false;
        }
        
        $this->view->availability = $availability;
        
        $this->view->title = $this->view->translate('Financial Assistance');
        
        $this->view->studentInfo = $this->studentInfo;
        $semList = $this->model->getSemester($this->studentInfo['IdScheme']);
        $this->view->semList = $semList;
        
        $id = $this->_getParam('id');
        
        $sectionC = $this->model->getSectionC($this->studentInfo['IdProgram']);
        $this->view->sectionC = $sectionC;
        
        $info = $this->model->getReApplication($id);
        
        if (!empty($info)){
            switch ($info['sre_status']){
                case 1 : $info['sre_status_name'] = 'APPLIED';	break;
                case 2 : $info['sre_status_name'] = 'APPROVED';	break;
                case 3 : $info['sre_status_name'] = 'REJECTED';	break;
                case 4 : $info['sre_status_name'] = 'COMPLETED'; break;
                case 5 : $info['sre_status_name'] = 'INCOMPLETE'; break;
                case 6 : $info['sre_status_name'] = 'CANCEL'; break;
                case 7 : $info['sre_status_name'] = 'APPROVED';	break;
            }
        }
        
        $this->view->info = $info;
        
        $semesterInfo = $this->model->getSemesterById($info['sre_semester_id']);
        $lastSem = $this->model->getLastSem($this->studentInfo['IdScheme'], $semesterInfo['IdSemesterMaster'], $semesterInfo['SemesterMainStartDate'], $this->studentInfo['IdBranch']);
        $lastSemsub = $this->model->getRegSubLastSem($this->studentInfo['IdStudentRegistration'], $lastSem['IdSemesterMaster']);
        $this->view->lastSemsub = $lastSemsub;

        $barringClass = new icampus_Function_Courseregistration_BarringRelease();
        $checkingExamResult = $barringClass->checkingAuthorization($this->studentInfo['IdStudentRegistration'], $semesterInfo, 868);

        $exResultStatus = 0;
        if ($checkingExamResult){
            $exResultStatus = $checkingExamResult['examresult'];
        }
        
        $publishInfo = $this->model->getPublishResult($this->studentInfo['IdProgram'], $lastSem['IdSemesterMaster']);
        if ($publishInfo){
            $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
        }else{
            $publishInfo['pm_date_new'] = strtotime('2300-01-01 00:00:00');
        }
        $publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));
        
        $this->view->publishInfo = $publishInfo;
        $this->view->exResultStatus = $exResultStatus;

        $courseList = $this->model->getReApplicationCourse($id);
        $this->view->courseList = $courseList;
        
        /*if ($this->studentInfo['IdScheme'] == 1){
            $curSem = $this->model->getCurSem($this->studentInfo['IdScheme']);
            $lastSem = $this->model->getLastSem($this->studentInfo['IdScheme'], $curSem['IdSemesterMaster'], $curSem['SemesterMainStartDate']);
            $lastSemsub = $this->model->getRegSubLastSem($this->studentInfo['IdStudentRegistration'], $lastSem['IdSemesterMaster']);
            $this->view->lastSemsub = $lastSemsub;
        }else{
            $this->view->nextSemsub = $this->model->getGSSubject($id);
        }*/
    }
    
    public function getSubjectAction(){
        $id = $this->_getParam('id');
        
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        $barringClass = new icampus_Function_Courseregistration_BarringRelease();
        	    
	$ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();
        
        if ($id != ''){
            $semesterInfo = $this->model->getSemesterById($id);

            $lastSem = $this->model->getLastSem($this->studentInfo['IdScheme'], $semesterInfo['IdSemesterMaster'], $semesterInfo['SemesterMainStartDate'], $this->studentInfo['IdBranch']);
            $lastSemsub = $this->model->getRegSubLastSem($this->studentInfo['IdStudentRegistration'], $lastSem['IdSemesterMaster']);
            $checkingExamResult = $barringClass->checkingAuthorization($this->studentInfo['IdStudentRegistration'], $semesterInfo, 868);

            $exResultStatus = 0;
            if ($checkingExamResult){
                $exResultStatus = $checkingExamResult['examresult'];
            }

            if ($lastSemsub){
                $publishInfo = $this->model->getPublishResult($this->studentInfo['IdProgram'], $lastSem['IdSemesterMaster']);
                
                if ($publishInfo){
                    $publishInfo['pm_date_new'] = strtotime($publishInfo['pm_date'].' '.$publishInfo['pm_time']);
                }else{
                    $publishInfo['pm_date_new'] = strtotime('2300-01-01 00:00:00');
                }
                $publishInfo['pm_cdate']= strtotime(date('Y-m-d H:i:s'));
                //var_dump($publishInfo);
                foreach ($lastSemsub as $key => $lastSemsubLoop){
                    $lastSemsub[$key]['publishinfo']=$publishInfo;
                    $lastSemsub[$key]['exResultStatus']=$exResultStatus;
                }
                
                $data = $lastSemsub;
            }else{
                $data = 0;
            }
        }else{
            $data = 0;
        }
        //var_dump($data); exit;
        $json = Zend_Json::encode($data);
		
	echo $json;
	exit();
    }
    
    public function expiredPageAction(){
        $this->view->title = $this->view->translate('Financial Assistance');
    }
    
    public function coursesAction(){
        $id = $this->_getParam('id');
        $this->view->title = "Courses";

        $info = $this->model->getReApplicationList($this->studentInfo['IdStudentRegistration'], $this->auth->getIdentity()->semester['current_semester']);

        $checkCourseAvailability = $this->model->getCourseAvailability($this->studentInfo['IdScheme'], $this->studentInfo['IdProgram'], $info['sre_semester_id']);
        $this->view->courseAvailability = $checkCourseAvailability;

        $coveredCourseInfo = $this->scholarModel->getCourseCovered($info['sre_semester_id']);

        if ($coveredCourseInfo){
            $coveredCourse = $coveredCourseInfo['total_course'];
        }else{
            $coveredCourse = 0;
        }

        $this->view->coveredCourse = $coveredCourse;

        $db = getDB();
        $select = $db->select()
            ->from(array('a' =>'tbl_studentregsubjects'))
            ->joinLeft(array('s'=>'tbl_subjectmaster'),'a.IdSubject=s.IdSubject',array('s.SubjectName','s.SubCode'))
            ->where('a.IdStudentRegistration = ?', $this->studentInfo['IdStudentRegistration'])
            ->where('a.IdSemesterMain = ?', $info['sre_semester_id'])
            ->where('a.Active <> ?', 3)
            ->where('a.IdSubject <> ?', 84)
            ->where('a.IdSubject <> ?', 85)
            ->where('a.IdSubject <> ?', 126);

        $courses = $db->fetchAll($select);

        if ($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();

            if (isset($formData['sub']) && count($formData['sub']) > 0){
                foreach ($formData['sub'] as $key => $subloop){
                    $courseData = array(
                        'src_sreid' => $id,
                        'src_subjectid' => $subloop,
                        'src_regsubid'=> $key,
                        'src_upddate' => date('Y-m-d H:i:s'),
                        'src_updby' => $this->studentInfo['IdStudentRegistration'],
                        'src_type'=>0
                    );
                    $this->model->insertSponsorCourse($courseData);

                    $status['sre_status']=7;

                    $this->model->updateStatus($status, $id);
                }

                $courseList3 = $this->model->getReApplicationCourse($id, true);

                if ($courseList3) {
                    foreach ($courseList3 as $courseLoop3) { //subject loop start
                        if ($courseLoop3['src_receiptid']==0){
                            $arrinvdet = array();
                            $totalamount = 0;
                            $itemList = $this->scholarModel->getSubjectItem($courseLoop3['src_regsubid']);

                            if ($itemList) {
                                foreach ($itemList as $itemLoop) {
                                    if ($itemLoop['invoice_id'] != null) {
                                        $invoiceList = $this->scholarModel->getInvoiceDetails($itemLoop['invoice_id'], $courseLoop3['src_subjectid']);
                                        $invmaininfo = $this->scholarModel->getInvoiceMain($itemLoop['invoice_id']);

                                        $currencyDb = new Studentfinance_Model_DbTable_CurrencyRate();
                                        $currency = $currencyDb->getCurrentExchangeRate($invmaininfo['currency_id']);

                                        if ($invoiceList) {
                                            foreach ($invoiceList as $invoiceLoop) {

                                                $financialaidcoverage = $this->scholarModel->financialAidCoverage($invoiceLoop['fi_id']);

                                                //todo generate bayaran
                                                if ($financialaidcoverage) {
                                                    if ($financialaidcoverage['calculation_mode'] == 1) { //amount
                                                        /*if ($financialaidcoverage['amount'] > $invoiceLoop['amount']) {
                                                            $amount = $invoiceLoop['amount'];
                                                        } else {
                                                            $amount = $financialaidcoverage['amount'];
                                                        }*/
                                                        if ($financialaidcoverage['amount'] > $invoiceLoop['amt']) {
                                                            if ($invmaininfo['currency_id'] != 1){
                                                                $amount = ($invoiceLoop['amt']*$currency['cr_exchange_rate']);
                                                            }else{
                                                                $amount = $invoiceLoop['amt'];
                                                            }
                                                        } else {
                                                            if ($invmaininfo['currency_id'] != 1){
                                                                $amount = ($financialaidcoverage['amount']*$currency['cr_exchange_rate']);
                                                            }else{
                                                                $amount = $financialaidcoverage['amount'];
                                                            }
                                                        }
                                                    } else { //percentage
                                                        $amount = (($financialaidcoverage['amount'] / 100) * $invoiceLoop['amt']);
                                                    }

                                                    $totalamount = ($totalamount + $amount);

                                                    $desc = $courseLoop3['SubCode'] . ' - ' . $courseLoop3['SubjectName'] . ', ' . $invoiceLoop['fee_item_description'];

                                                    $cnDetailData = array(
                                                        //'subregdetid' =>  $itemLoop['id'],
                                                        'sp_invoice_id' => $invoiceLoop['invoice_main_id'],
                                                        'sp_invoice_det_id' => $invoiceLoop['id'],
                                                        'sp_amount' => $amount,
                                                        'sp_balance' => $amount,
                                                        'sp_receipt' => $amount,
                                                        'sp_description' => $desc
                                                    );
                                                    $arrinvdet[] = $cnDetailData; //array push
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (count($arrinvdet) > 0){
                            $invoiceclass = new icampus_Function_Studentfinance_Invoice();
                            $bill_no =  $invoiceclass->getBillSeq(11, date('Y'));

                            $data_dn = array(
                                'sp_fomulir_id' => $bill_no,
                                'sp_txn_id' => $info['transaction_id'],
                                'sp_IdStudentRegistration' => $info['IdStudentRegistration'],
                                //'sp_batch_id' => $btch_id,
                                'sp_amount' => $totalamount,
                                'sp_balance' => $totalamount,
                                'sp_type' => 3,
                                'sp_type_id' => 1,
                                'sp_invoice_date' => date('Y-m-d'),
                                'sp_description' => 'Financial Assistance',
                                'sp_currency_id' => $invmaininfo['currency_id'],
                                'sp_exchange_rate' => $invmaininfo['exchange_rate'],
                                'sp_creator'=>1,
                                'sp_create_date'=>date('Y-m-d H:i:s'),
                                'sp_status' => 'E',//entry
                            );
                            $spinvmainid = $this->scholarModel->saveSponsorInvMain($data_dn);

                            //todo generate receipt
                            $receipt_no = $this->getReceiptNoSeq();

                            $receipt_data = array(
                                'rcp_no' => $receipt_no,
                                'rcp_account_code' => 96,
                                'rcp_date' => date('Y-m-d H:i:s'),
                                'rcp_receive_date' => date('Y-m-d'),
                                'rcp_batch_id' => 0,
                                'rcp_type' => 3,
                                'rcp_type_id' => 1,
                                'rcp_description' => 'Fisabilillah - '.$info['SemesterMainName'].' - '.$courseLoop3['SubCode'] . ' - ' . $courseLoop3['SubjectName'],
                                'rcp_amount' => $totalamount,
                                'rcp_adv_amount' => 0,
                                'rcp_gainloss' => 0,
                                'rcp_gainloss_amt' => 0,
                                'rcp_cur_id' => $invmaininfo['currency_id'],
                                'rcp_exchange_rate' => $currency['cr_id'],
                                'p_payment_mode_id' => 20,
                                'p_cheque_no' => null,
                                'p_doc_bank' => null,
                                //'p_doc_branch' => isset($formData['payment_doc_branch'])?$formData['payment_doc_branch']:null,
                                'p_terminal_id' => null,
                                'p_card_no' => null,
                                'rcp_create_by'=>1
                            );
                            $receipt_id = $this->scholarModel->insertReceipt($receipt_data);

                            //todo update receipt id
                            $receiptData = array(
                                'src_receiptid'=>$receipt_id
                            );
                            $this->scholarModel->updateReApplicationCourse($receiptData, $courseLoop3['src_id']);

                            $data_rcp_stud = array(
                                'rcp_inv_rcp_id' => $receipt_id,
                                'IdStudentRegistration' => $info['IdStudentRegistration'],
                                'rcp_inv_amount' => $totalamount,
                                'rcp_adv_amount' => 0,
                                'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                                'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                'rcp_inv_create_by'=>1
                            );
                            $db->insert('sponsor_invoice_receipt_student',$data_rcp_stud);
                            $idStd = $db->lastInsertId();

                            foreach ($arrinvdet as $arrinvdetLoop){
                                $arrinvdetLoop['sp_id']=$spinvmainid;

                                $spinvdetailid = $this->scholarModel->saveSponsorInvDetail($arrinvdetLoop);

                                $data_rcp_inv = array(
                                    'rcp_inv_rcp_id' => $receipt_id,
                                    'rcp_inv_sp_id' => $spinvmainid,
                                    'rcp_inv_student' => $idStd,
                                    'rcp_inv_sponsor_dtl_id' => $spinvdetailid,
                                    'rcp_inv_amount' => $arrinvdetLoop['sp_amount'],
                                    'rcp_inv_amount_default' => $arrinvdetLoop['sp_amount'],
                                    'rcp_inv_cur_id' => $invmaininfo['currency_id'],
                                    'rcp_inv_create_date' => date('Y-m-d H:i:s'),
                                    'rcp_inv_create_by'=>1
                                );
                                $this->scholarModel->insertSponsorReceipt($data_rcp_inv);
                            }

                            $this->scholarModel->approveSponsorInvoive($receipt_id);
                        }
                    } //subject loop end
                }
            }

            //redirect
            $this->_helper->flashMessenger->addMessage(array('success' => "Subject Submitted"));
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'index'),'default',true));
        }

        $this->view->courses = $courses;

        $courseList = $this->model->getReApplicationCourse($id);
        $this->view->courseList = $courseList;

        $courseArr = array();

        if ($courseList){
            foreach ($courseList as $courseLoop){
                array_push($courseArr, $courseLoop['IdSubject']);
            }
        }

        $this->view->courseArr = $courseArr;

        if (empty($checkCourseAvailability)){
            //var_dump($checkCourseAvailability); exit;
            //redirect
            $this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'reapplication', 'action'=>'index'),'default',true));
        }
    }

    public function getReceiptNoSeq(){

        $seq_data = array(
            12,
            date('Y'),
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS receipt_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['receipt_no'];
    }
}
?>