<?php
/**
 * @copyright Copyright (c) 2014, MTCSB
 */
class Studentfinance_PaymentController extends Zend_Controller_Action
{

	public function init() {
		
		$this->locale = Zend_Registry::get('Zend_Locale');
    	$this->view->translate = Zend_Registry::get('Zend_Translate');
    	Zend_Form::setDefaultTranslator($this->view->translate);
    	
       Zend_Layout::getMvcInstance()->assign('navActive', 'finance');

        $this->auth = Zend_Auth::getInstance();
		$this->studentInfo = $this->auth->getIdentity()->info;

		$this->studentModel = new Portal_Model_DbTable_Student();

		$studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
		$student = $studentRegDB->getStudentInfo($this->auth->getIdentity()->IdStudentRegistration);

		if ( empty($student) )
		{
			throw new Exception('Invalid Student ID');
		}

		$this->studentinfo = $this->view->studentinfo = $student;
		
	}

	
	public function indexAction()
	{
		$this->view->title = 'Online Payment';
		Zend_Session::namespaceUnset('migs');
		
		$student = $this->studentinfo;
		$pamodel = new Studentfinance_Model_DbTable_PaymentAmount();
		
		$bill = array();
		
		$txnId = $student['IdStudentRegistration'];
		if($txnId==0){
			$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Unknown transactioxxn id')));
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'index'),'default',true));
		}

		//test untuk early bird
		$ebModel = new Studentfinance_Model_DbTable_EarlyBird();
		$ebitList = $ebModel->getEbit($txnId);

		//var_dump($ebitList);
		//exit;
		
		$currency= $this->_getParam('currency',1);  
        $this->view->curr_id = $currency;
        
		//check txnid with profile id
		$auth = Zend_Auth::getInstance();
		
		
		//profile
    	$studentRegistrationDb = new CourseRegistration_Model_DbTable_Studentregistration();
    	$transaction = $studentRegistrationDb->getTheStudentRegistrationDetail($txnId);
    	$appl_id = $transaction['IdApplication'];
	
		if(!$transaction){
			$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Invalid transaction id')));
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'index'),'default',true));
		}
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();
			
			if( !isset($formData['invoice']) ){
				$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Please select item to pay')));
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'index','id' => $txnId),'default',true));
			}
			
			$ses_migs = new Zend_Session_Namespace('migs');
			$ses_migs->txnid = $txnId;
			$ses_migs->appl_id = $txnId;
		
			
			//store invoice in session if any
			if(isset($formData['invoice'])){
				$ses_migs->invoice = $formData['invoice'];
			}
			
			//redirect to confirmation page
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'payment-confirmation'),'default',true));
		}
		
		//proforma invoice
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select_currency = $db->select()
								->from(array('c'=>'tbl_currency'));	
			$row_currency = $db->fetchAll($select_currency);
			$this->view->currency_list = $row_currency;		
						
			
	 
		
			$select_invoice = $db->select()
							->from(array('ivd'=>'invoice_detail'),array('*','idDet'=>'ivd.id'))
							->join(array('im'=>'invoice_main'),'ivd.invoice_main_id=im.id',array('*','record_date'=>'im.invoice_date','idMain'=>'im.id','invsem'=>'im.semester'))
							->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id',array())
							->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject=ivs.subject_id',array('*'))
							->joinLeft(array('fc'=>'fee_item'),'fc.fi_id=ivd.fi_id',array('*'))
						    ->joinLeft(array('c'=>'tbl_currency'),'c.cur_id=im.currency_id', array('cur_code','cur_id','cur_desc'))	
						    ->joinLeft(array('fi'=>'fee_item'),'fi.fi_id=ivd.fi_id', array('fi_name'))	
						    ->joinLeft(array('fsi'=>'fee_structure_item'),'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=im.fs_id', array(''))	
						    ->joinLeft(array('i'=>'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc','item_code'=>'i.DefinitionCode'))	
						    //->where('im.currency_id= ?',$currency)						  
							->where('im.IdStudentRegistration = ?', $txnId)
							->where("im.status = 'A'")
							//->where("ivd.balance > 0")
							->group('ivd.id');
							
			$row = $db->fetchAll($select_invoice);			
			if(!$row){
				$row = null;
			}else{
				foreach ($row as $key => $value){
					$balance = $pamodel->getBalanceDtl($value['idDet']);
					$balance2 = $pamodel->getBalanceMain($value['idMain']);
					$amt = str_replace(',', '', $balance['bill_balance']) < 0.00 ? 0.00:str_replace(',', '', $balance['bill_balance']);

					if (($amt <= 0.00 || $balance2['bill_balance'] <= 0.00) && $balance['bill_balance'] != null){
						unset($row[$key]);
					}else{
						//calculate ebit discount
						$ebAmount = 0.00;
						if ($ebitList){
							foreach ($ebitList as $ebitLoop){
								if ($ebitLoop['eb_semester']==$value['invsem']) {
									$ebitItem = $ebModel->checkEbitItem($ebitLoop['eb_discount_type'], $value['fi_id']);

									if ($ebitItem) {
										if ($ebitItem['CalculationMode'] == 1) { //amount
											$ebAmount = $ebitItem['Amount'];
											$amt = $amt - $ebAmount;
										} else { //percentage
											$ebAmount = (($ebitItem['Amount'] / 100) * $value['amount']);
											$amt = $amt - $ebAmount;
										}
									}
								}
							}
						}

						$row[$key]['balance'] = $amt < 0.00 ? 0.00:$amt;
						$row[$key]['ebAmount'] = $ebAmount;
						$row[$key]['paid'] = $balance['bill_paid'];
					}
				}
			}
			
			//$this->view->account = $row;
		
		$this->view->bill = $row;

	}
	
	public function paymentConfirmationAction(){
		
		$ses_migs = new Zend_Session_Namespace('migs');
		
		$this->view->title = 'Payment Confirmation';
		$pamodel = new Studentfinance_Model_DbTable_PaymentAmount();
		
		
		if ( !isset($ses_migs->txnid) || (!isset($ses_migs->invoice) ) ) {
			$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Invoice not found')));
			$this->_redirect($this->view->url(array('module'=>'studenfinance','controller'=>'payment'),'default',true));
		}
		
		$this->view->txnid = $ses_migs->txnid;
		
		$auth = Zend_Auth::getInstance();
		$txnId = $ses_migs->txnid;
		
		$studentRegistrationDb = new CourseRegistration_Model_DbTable_Studentregistration();
    	$transaction = $studentRegistrationDb->getTheStudentRegistrationDetail($txnId);
    	
		if(!$transaction){
			$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Invalid transaction id')));
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'index'),'default',true));
		}else{
			$transaction = $transaction;
		}

		$ebModel = new Studentfinance_Model_DbTable_EarlyBird();
		$ebitList = $ebModel->getEbit($txnId);
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		
		//proforma
		$invoiceInvoiceDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$invoice_list = array();
		for($i=0; $i<sizeof($ses_migs->invoice); $i++){
			
			$selectData = $db->select()
							->from(array('im'=>'invoice_detail'),array('idDet'=>'im.id', 'im.id', 'im.invoice_main_id', 'im.fi_id', 'im.fee_item_description', 'im.cur_id', 'im.amount', 'im.amount_gst', 'im.amount_default_currency', 'im.paid', 'im.balance', 'im.cn_amount', 'im.dn_amount', 'im.sp_amount', 'im.sp_id', 'id_detail'=>'im.id'))
							->join(array('pim'=>'invoice_main'),'im.invoice_main_id=pim.id',array('*', 'invsem'=>'pim.semester'))
							->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=pim.id and ivs.invoice_detail_id=im.id',array(''))
							->joinLeft(array('s'=>'tbl_subjectmaster'),'s.IdSubject=ivs.subject_id',array('*'))
						    ->joinLeft(array('c'=>'tbl_currency'),'c.cur_id=pim.currency_id', array('*'))	
						    ->joinLeft(array('fi'=>'fee_item'),'fi.fi_id=im.fi_id', array(''))	
						    ->joinLeft(array('fsi'=>'fee_structure_item'),'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=pim.fs_id', array(''))	
						    ->joinLeft(array('i'=>'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc','item_code'=>'i.DefinitionCode'))	
						    
						    
						    
							->where("im.id = ?", (int)$ses_migs->invoice[$i])
							->group('im.id');
							
							
			
			$invoice_list[] = $db->fetchRow($selectData);

			$balance = $pamodel->getBalanceDtl($invoice_list[$i]['idDet']);
			$amt = str_replace(',', '', $balance['bill_balance']) < 0.00 ? 0.00:str_replace(',', '', $balance['bill_balance']);

			$ebAmount = 0.00;
			if ($ebitList){
				foreach ($ebitList as $ebitLoop){
					if ($ebitLoop['eb_semester']==$invoice_list[$i]['invsem']) {
						$ebitItem = $ebModel->checkEbitItem($ebitLoop['eb_discount_type'], $invoice_list[$i]['fi_id']);

						if ($ebitItem) {
							if ($ebitItem['CalculationMode'] == 1) { //amount
								$ebAmount = $ebitItem['Amount'];
								$amt = $amt - $ebAmount;
							} else { //percentage
								$ebAmount = (($ebitItem['Amount'] / 100) * $invoice_list[$i]['amount']);
								$amt = $amt - $ebAmount;
							}
						}
					}
				}
			}

			$invoice_list[$i]['amount'] = $amt < 0.00 ? 0.00:$amt;
			
			if($invoice_list[$i]['currency_id'] != 1){ //if not in MYR, convert to actual currency 1st
				
				//get current currency rate
				$currencyRateDb = new Studentfinance_Model_DbTable_CurrencyRate();
				$rate = $currencyRateDb->getData($invoice_list[$i]['exchange_rate']);
				$exrate = $currencyRateDb->getRateByDate($invoice_list[$i]['currency_id'], $invoice_list[$i]['invoice_date']);
				$total_invoice_amount_default_currency = round($amt * $exrate['cr_exchange_rate'],2);
				$invoice_list[$i]['amount'] = $total_invoice_amount_default_currency;
			
			}
			
		}

		$this->view->invoice_list = $invoice_list;
		
		//set default currency 
		$currencyDb = new Studentfinance_Model_DbTable_Currency();
		$currency = $currencyDb->fetchRow(array('cur_default=?'=>'Y'))->toArray();
		
		$this->view->currency = $currency;
		
		
//		debug($this->view->currency);
//		debug($invoice_list);
		
		if ($this->getRequest()->isPost()) {
		
			$formData = $this->getRequest()->getPost();

			//get total amount from proforma and invoice
			$total_amount = 0.00;
		
			for($i=0; $i<count($invoice_list); $i++){
				$total_amount += $invoice_list[$i]['bill_amount'];
			}

			$total_amount = $formData['amount'];
			
			//store migs transaction
			$migsTransactionDb = new Studentfinance_Model_DbTable_TransactionMigs();
			
			$migsData = array(
				'mt_appl_id' => $transaction['IdApplication'],
				'mt_trans_id' => $transaction['transaction_id'],
				'mt_idStudentRegistration' => $txnId,
				'mt_txn_no' => date('Y').icampus_Function_Studentfinance_Migs::getMigsTxnSeqNextVal(date('Y'))."_".$transaction['at_pes_id'],
				'mt_amount' => $total_amount,
				'mt_currency_id' => $currency['cur_id'],
				'mt_status' => 'ENTRY',
				'mt_from' => '1',
			);
			$migs_id = $migsTransactionDb->insert($migsData);
			
			//create url
			//for debug use amount 100
			$total_amount = $total_amount * 100;
			$url = icampus_Function_Studentfinance_Migs::createPaymentUrl($migs_id, $migsData['mt_txn_no'], $total_amount);
			
			//update url to db
			$migsTransactionDb->update(array('mt_url'=>$url), 'mt_id = '.$migs_id);
			
			
			//store transaction invoice / proforma data
			$migsTransactionDetailDb = new Studentfinance_Model_DbTable_TransactionMigsDetail();
			
			
			for($i=0; $i<count($invoice_list); $i++){
				$dtl = array(
						'tmd_tm_id' => $migs_id,
						'tmd_invoice_id' => $invoice_list[$i]['id_detail'],
						'tmd_date_created' => date('Y-m-d H:i:s')
				);
				$migsTransactionDetailDb->insert($dtl);
			}
			
			//redirect to payment gateway
			header("Location: ".$url);
		}
		
	}
	
	public function paymentSuccessAction(){
		$ses_migs = new Zend_Session_Namespace('migs');

		$auth = Zend_Auth::getInstance();
		
		$this->view->title = 'Success';
		
		$mt_id = $this->_getParam('mt_id');

		$migsTransactionDb = new Studentfinance_Model_DbTable_TransactionMigs();
		$migsTxnData = $migsTransactionDb->fetchRow(array('mt_id = ?'=>$mt_id));
		
		if ( !empty($migsTxnData) )
		{
			$migsTxnData->toArray();
		}


		$this->view->txn_info = $migsTxnData;

	}
	
	public function paymentErrorAction(){
		
		$this->view->title = 'Error';
		
		$ses_migs = new Zend_Session_Namespace('migs');
		$this->view->txn_info = $ses_migs->migs_txn;
		
	}
	
	/*
	 * Get Bill no from mysql function
	*/
	private function getReceiptNoSeq(){
	
		$seq_data = array(
				5,
				date('Y'),
				0,
				0,
				0
		);
			
		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS receipt_no");
		$stmt->execute($seq_data);
		$seq = $stmt->fetch();
	
		return $seq['receipt_no'];
	}
	
	private function knockoffInvoice($receipt_id){
		
		//receipt
		$receiptDb = new Studentfinance_Model_DbTable_Receipt();
		$receipt = $receiptDb->getData($receipt_id);

		$pamodel = new Studentfinance_Model_DbTable_PaymentAmount();
		
		$receipt = $receipt[0];
		
		$auth = Zend_Auth::getInstance();
		$getUserIdentity = $auth->getIdentity();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
		
		$appStatus = 0;
			
		$appTrxId = $receipt['rcp_idStudentRegistration'];
		$trans_id = $receipt['rcp_trans_id'];
		
		$studentRegistrationDb = new CourseRegistration_Model_DbTable_Studentregistration();
    	$txnData = $studentRegistrationDb->getTheStudentRegistrationDetail($appTrxId);

		$ebModel = new Studentfinance_Model_DbTable_EarlyBird();
		$ebitList = $ebModel->getEbit($appTrxId);
    	
		$invoiceMainDb = new Studentfinance_Model_DbTable_InvoiceMain();
		$invoiceDetailDb = new Studentfinance_Model_DbTable_InvoiceDetail();
		$invoiceSubjectDb = new Studentfinance_Model_DbTable_InvoiceSubject();
		
		$totalAmount = 0.00;
		$total_advance_payment_amount = 0;
		$total_payment_amount = $receipt['rcp_amount'];
		$total_paid_receipt = 0;
		$paidReceiptInvoice = 0;
			
			//get receipt invoice
			foreach ($receipt['receipt_invoice'] as $inv){
				$invID = $inv['rcp_inv_invoice_id'];
				$invDetID = $inv['rcp_inv_invoice_dtl_id'];
//				$paidReceipt =  $inv['rcp_inv_amount'];
				$rcpCurrency = $inv['rcp_inv_cur_id'];
				
				$invDetData = $invoiceDetailDb->getData($invDetID);
				
//				echo "<pre>";
//				print_r($invDetData);
//				exit;
				
				$invCurrency = $invDetData['cur_id'];
				
				$currencyDb = new Studentfinance_Model_DbTable_Currency();
				$currency = $currencyDb->fetchRow('cur_id = '.$invCurrency)->toArray();

				/*echo "<pre>";
				print_r($currency);
				exit;*/
//				$amountNew = $paidReceipt;

				$balance = $pamodel->getBalanceDtl($invDetID);
				$invDetData['balance'] = str_replace(',', '', $balance['bill_balance']) < 0.00 ? 0.00:str_replace(',', '', $balance['bill_balance']);

				$amountDefault = $invDetData['balance'];
				
				$paidReceiptInvoice = $inv['rcp_inv_amount'];
				
				if($invCurrency == 1){
					$paidReceipt = $inv['rcp_inv_amount'];
					
				}else{
					$paidReceipt = $inv['rcp_inv_amount_default_currency'];
				}

				//calculate ebit discount
				$ebAmount = 0.00;
				$totalEbAmount = 0.00;
				if ($ebitList){
					foreach ($ebitList as $ebitLoop){
						if ($ebitLoop['eb_semester']==$inv['semester']) {
							$ebitItem = $ebModel->checkEbitItem($ebitLoop['eb_discount_type'], $inv['fi_id']);

							if ($ebitItem) {
								if ($ebitItem['CalculationMode'] == 1) { //amount
									$ebAmount = $ebitItem['Amount'];
								} else { //percentage
									$ebAmount = (($ebitItem['Amount'] / 100) * $inv['amount']);
								}
							}

							$totalEbAmount = $totalEbAmount + $ebAmount;

							if ($ebAmount > 0.00) {
								$bill_no = $ebModel->getBillSeq(9, date('Y'));

								$data_dn = array(
									'dcnt_fomulir_id' => $bill_no,
									'dcnt_IdStudentRegistration' => $appTrxId,
									'dcnt_batch_id' => 0,
									'dcnt_amount' => $ebAmount,
									'dcnt_type_id' => $ebitLoop['eb_discount_type'],
									'dcnt_description' => $ebitLoop['eb_name'],
									'dcnt_currency_id' => $inv['currency_id'],
									'dcnt_creator' => 665,//by system
									'dcnt_create_date' => date('Y-m-d H:i:s'),
									'dcnt_approve_by' => 665,//by system
									'dcnt_approve_date' => date('Y-m-d H:i:s'),
									'dcnt_status' => 'A',//entry
								);
								$dn_id = $ebModel->insertEbDiscount($data_dn);

								$cnDetailData = array(
									'dcnt_id' => $dn_id,
									'dcnt_invoice_id' => $invID,
									'dcnt_invoice_det_id' => $invDetID,
									'dcnt_amount' => $ebAmount,
									'status' => 'A',
									'status_by' => 665,
									'dcnt_description' => $ebitLoop['eb_name']
								);
								$ebModel->insertEbDiscountDtl($cnDetailData);
							}
						}
					}
				}
						
				$balance  = $amountDefault  - $paidReceipt - $totalEbAmount;
				
				//update invoice details
				$dataInvDetail = array(
					'paid'=> $paidReceipt +  $invDetData['paid'] ,
					'balance' => $balance,
				);

//				echo "<pre>";
//				print_r($dataInvDetail);
				$invoiceDetailDb->update($dataInvDetail, array('id =?'=>$invDetID) );
				
				$totalAmount += $paidReceiptInvoice;
				
				//update invoice main
				$invMainData = $invoiceMainDb->getData($invID);

				$balance2 = $pamodel->getBalanceMain($invID);
				$invMainData['bill_balance'] = str_replace(',', '', $balance2['bill_balance']) < 0.00 ? 0.00:str_replace(',', '', $balance2['bill_balance']);
					
				$balanceMain = ($invMainData['bill_balance']) - ($paidReceipt) -($totalEbAmount);
				$paidMain = ($invMainData['bill_paid']) + ($paidReceipt);
				
				//update invoice main
				$dataInvMain = array(
					'bill_paid'=> $paidMain,
					'bill_balance' => $balanceMain,
					'upd_by'=>1,
		            'upd_date'=>date('Y-m-d H:i:s')
				);
				
//				echo "<pre>";
//				print_r($dataInvMain);
				$invoiceMainDb->update($dataInvMain, array('id =?'=>$invID) );

//				TODO : update course status as register
				if($balanceMain <= 0){
					$this->updateCourseStatus($invID);
				}
				
			}
			
			//calculate advance payment amount
			
			if( $total_payment_amount > $totalAmount ){
				$total_advance_payment_amount = $total_payment_amount - ($totalAmount);
			}
			
			//add advance payment (if any)
			if($total_advance_payment_amount > 0){
				$advancePaymentDb = new Studentfinance_Model_DbTable_AdvancePayment();
				$advancePaymentDetailDb = new Studentfinance_Model_DbTable_AdvancePaymentDetail();
				
				$advance_payment_data = array(
					'advpy_appl_id' => $receipt['rcp_appl_id'],
					'advpy_trans_id' => $receipt['rcp_trans_id'],
					'advpy_idStudentRegistration' => $receipt['rcp_idStudentRegistration'],
					'advpy_rcp_id' => $receipt['rcp_id'],
					'advpy_description' => 'Advance payment from receipt no:'.$receipt['rcp_no'],
					'advpy_cur_id' => $receipt['rcp_cur_id'],
					'advpy_amount' => $total_advance_payment_amount,
					'advpy_total_paid' => 0.00,
					'advpy_total_balance' => $total_advance_payment_amount,
					'advpy_status' => 'A'
				);
				
				$advPayID = $advancePaymentDb->insert($advance_payment_data);
				
				$advance_payment_det_data = array(
					'advpydet_advpy_id' => $advPayID,
					'advpydet_total_paid' => 0.00
				);
				
				$advancePaymentDetailDb->insert($advance_payment_det_data);

				//receipt update advpayment
				$receiptDb->update(array('rcp_adv_payment_amt'=>$total_advance_payment_amount, 'rcp_adv_payment_amt_default_currency' => icampus_Function_Studentfinance_Currency::getDefaultCurrencyAmount($total_advance_payment_amount, $receipt['rcp_cur_id']) ), 'rcp_id = '.$receipt['rcp_id']);
			}
			
			
			//update receipt status
			$receiptDb->update(
			array(
				'rcp_status'=>'APPROVE',
				'UpdDate'=>date('Y-m-d H:i:s'),
				'UpdUser'=>1
			), 'rcp_id = '.$receipt_id);
		
		/*knockoff end*/
			
		/*
		 * update status course as registered
		 */
		foreach ($receipt['receipt_invoice'] as $inv) {
			$invID = $inv['rcp_inv_invoice_id'];
			$balance3 = $pamodel->getBalanceMain($invID);
			$balancecheck = str_replace(',', '', $balance3['bill_balance']);
			if($balancecheck <= 0){
				$this->updateCourseStatus($invID);
			}
		}
	}
	
	private function updateCourseStatus($invoiceID){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select_invoice = $db->select()
							->from(array('ivs'=>'invoice_subject'))
							->join(array('im'=>'invoice_main'),'ivs.invoice_main_id=im.id',array('*'))
							->where('ivs.invoice_main_id = ?', $invoiceID)
							->group('ivs.invoice_main_id');
		$row = $db->fetchAll($select_invoice);
		$txnID = $row[0]['IdStudentRegistration'];
		
		
		
		foreach($row as $data){
			$subjectID = $data['subject_id'];
			
			$typeApp =  $data['invoice_type'];
			
			$select_reg = $db->select()
							->from(array('a'=>'tbl_studentregsubjects'))
							->where('a.IdStudentRegistration = ?', $txnID)
							->where('a.IdSubject = ?', $subjectID)
							->order('a.UpdDate DESC')
							->limit('1')
							;
							
			
			$rowReg = $db->fetchRow($select_reg);
			
			$idRegSubject = $rowReg['IdStudentRegSubjects'];
			
			/*case 0: $newval = 'Pre-Registered';	break;
			case 1: $newval = 'Registered';		break;
			case 2: $newval = 'Drop';			break;
			case 3: $newval = 'Withdraw';		break;
			case 4: $newval = 'Repeat';			break;
			case 5: $newval = 'Refer';			break;
			case 6:	$newval = 'Replace';		break;
			case 9: $newval = 'Pre-Repeat';		break;
			case 10:$newval = 'Pre-Replace';	break;*/
			
			$courseStatus = 0;
			if($rowReg['Active'] == 0){//pre-registered
				$courseStatus = 1;
			}elseif($rowReg['Active'] == 9){//pre-Repeat
				$courseStatus = 4; 
			}elseif($rowReg['Active'] == 10){//pre-Replace
				$courseStatus = 6;
			}else{
				$courseStatus = $rowReg['Active'] ;
			}
			
			$updData = array ('Active'=>$courseStatus);
			$db->update('tbl_studentregsubjects',$updData,array('IdStudentRegSubjects = ?' => $idRegSubject));
			
			if($typeApp == 'AU'){
				$select_audit = $db->select()
							->from(array('a'=>'tbl_auditpaperapplication'))
							->where('a.apa_subjectreg_id = ?', $idRegSubject)
							->where('a.apa_invid_0 = ?', $invoiceID)
							->orWhere('a.apa_invid_1 = ?', $invoiceID);
				
				$rowAudit = $db->fetchRow($select_audit);
				
				if($invoiceID == $rowAudit['apa_invid_0']){
					$updData = array ('apa_paid_0'=>1);
					$db->update('tbl_auditpaperapplication',$updData,array('apa_id = ?' => $rowAudit['apa_id']));
				}elseif($invoiceID == $rowAudit['apa_invid_1']){
					$updData = array ('apa_paid_1'=>1);
					$db->update('tbl_auditpaperapplication',$updData,array('apa_id = ?' => $rowAudit['apa_id']));
				}
			
			}
			
			if($typeApp == 'CT' || $typeApp == 'EX'){
				$select_ct = $db->select()
							->from(array('a'=>'tbl_credittransfer'))
							->where('a.invoiceid_0 = ?', $invoiceID)
							->orWhere('a.invoiceid_1 = ?', $invoiceID);
				
				$rowCt = $db->fetchRow($select_ct);
				
				if($invoiceID == $rowAudit['invoiceid_0']){
					$updData = array ('paid_0'=>1);
					$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
				}elseif($invoiceID == $rowAudit['invoiceid_1']){
					$updData = array ('paid_1'=>1);
					$db->update('tbl_credittransfer',$updData,array('IdCreditTransfer = ?' => $rowCt['IdCreditTransfer']));
				}
			}
			
		}
	}
	
	public function respondAction(){
		
		$auth = Zend_Auth::getInstance();
		
		$this->view->title = 'Payment Respond';
		
		$typeid = $this->_getParam('type', null);
		
		$ses_migs = new Zend_Session_Namespace('migs');

		$pamodel = new Studentfinance_Model_DbTable_PaymentAmount();
		
		if ( !isset($ses_migs->txnid ) ) {
			$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Payment not found')));
			
			/*if($typeid == 1){
				$url = 'http://e-university.inceif.org/studentfinance/payment';
				$this->_redirect($url);
			}else{
				//$url = 'http://apply.inceif.org/';
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment'),'default',true));
			}*/
				
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment'),'default',true));
		}
		
		$trans_id = $ses_migs->txnid;
		
		$studentRegistrationDb = new CourseRegistration_Model_DbTable_Studentregistration();
    	$transaction = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);

    	$appl_id = $transaction['IdApplication'];
    	$idtransaction = $transaction['transaction_id'];
    	
		$migs = new icampus_Function_Studentfinance_Migs();
		$migsTransactionDb = new Studentfinance_Model_DbTable_TransactionMigs();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
		
		$result = array('status'=>false);
		$respond = $migs->readRespondParam($_GET);
//		$respond = array(); //TESTING
		
		//if hash is valid then update transaction respond
		if($respond['hash_validation'] == 'CORRECT'){
		
		$mt_id = $respond['receipt']['merchTxnRef'];
			
			$migsTxnData = $migsTransactionDb->fetchRow(array('mt_id = ?'=>$mt_id,'mt_idStudentRegistration'=>$trans_id));//$respond['receipt']['merchTxnRef']
//$migsTxnData = $migsTransactionDb->fetchRow(array('mt_id = ?'=>84, 'mt_appl_id = ?'=>2924,'mt_trans_id'=>2964));//TESTING

			$ses_migs->migs_txn = $migsTxnData;
			
			//check for valid migs txn with applicant id
			if ( !$migsTxnData ) {
				$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Invalid migs transaction')));
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment'),'default',true));
				
				$migsTxnData = $migsTxnData->toArray();
				
				
			}
			
			
			//amount / 100
			$respond['receipt']['amount'] = $respond['receipt']['amount']/100; 
			
			//insert migs payment
				$paymentMigsDb = new Studentfinance_Model_DbTable_PaymentMigs();
				$data_pm_migs = array(
					'pm_txn_id' => $mt_id,
					'pm_amount' => $respond['receipt']['amount'],
					'pm_currency_id' => 1,
					'pm_date_create'=>date('Y-m-d H:i:s')	
				);
				$paymentMigsDb->insert($data_pm_migs);
				
			
			//update migs transaction data
			$data_upd = array(
				'mt_rep_raw' => $_SERVER["QUERY_STRING"],
				'mt_rep_error_status' => (!$respond['error_exists'] ? 0:1),
				'mt_rep_hash_validation' => $respond['hash_validation'],
				'mt_rep_amount' => $respond['receipt']['amount'],
				'mt_rep_locale'	=> $respond['receipt']['locale'],
				'mt_rep_batch_no'	=> $respond['receipt']['batchNo'],
				'mt_rep_command'	=> $respond['receipt']['command'],
				'mt_rep_message'	=> $respond['receipt']['message'],
				'mt_rep_version'	=> $respond['receipt']['version'],
				'mt_rep_card_type'	=> $respond['receipt']['cardType'],
				'mt_rep_order_info'	=> $respond['receipt']['orderInfo'],
				'mt_rep_receipt_no'	=> $respond['receipt']['receiptNo'],
				'mt_rep_merchant_id'	=> $respond['receipt']['merchantID'],
				'mt_rep_authorize_id'	=> $respond['receipt']['authorizeID'],
				'mt_rep_merch_txn_ref'	=> $respond['receipt']['merchTxnRef'],
				'mt_rep_transaction_no'	=> $respond['receipt']['transactionNo'],
				'mt_rep_acq_response_code'	=> $respond['receipt']['acqResponseCode'],
				'mt_rep_txn_response_code'	=> $respond['receipt']['txnResponseCode'],
				'mt_rep_txn_response_code_desc'	=> $respond['receipt']['txnResponseCodeDescription'],
					
				'mt_3d_rep_ver_type'	=> $respond['3d_secure_data']['verType'],
				'mt_3d_rep_ver_status'	=> $respond['3d_secure_data']['verStatus'],
				'mt_3d_rep_ver_status_desc'	=> $respond['3d_secure_data']['verStatusDescription'],
				'mt_3d_rep_token'	=> $respond['3d_secure_data']['token'],
				'mt_3d_rep_ver_secur_level'	=> $respond['3d_secure_data']['verSecurLevel'],
				'mt_3d_rep_enrolled'	=> $respond['3d_secure_data']['enrolled'],
				'mt_3d_rep_xid'	=> $respond['3d_secure_data']['xid'],
				'mt_3d_rep_acq_eci'	=> $respond['3d_secure_data']['acqECI'],
				'mt_3d_rep_auth_status'	=> $respond['3d_secure_data']['authStatus'],
				'mt_from'	=> 1, //from student portal
				
			);

			if( !$respond['error_exists'] && $respond['receipt']['txnResponseCode'] == "0"){
				$data_upd['mt_status'] = 'SUCCESS';
			}else{
				$data_upd['mt_status'] = 'FAIL';
			}
			
			$migsTransactionDb->update($data_upd, 'mt_id = '.$mt_id);
			
			
			//insert payment record if txn success
			if( !$respond['error_exists'] && $respond['receipt']['txnResponseCode'] == "0"){
				
				/*
				 *  1) Pay - simpan proforma yang tick
					2) Respond Success - generate invoice, knockoff, receipt, payment
					3) hantar MYR only masa Pay - convert
					4) proceed once migs status = success only
				 * 
				 */
				
				/*
				 * 1) generate invoice
				 * 2) knockoff invoice
				 * 3) insert payment & receipt & receipt_invoice
				 * 4) update column migs_id at receipt table
				 */

				$checkDuplication = $paymentMigsDb->checkDuplicatePayment($mt_id);

				if ($checkDuplication){
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'payment-success','mt_id'=>$mt_id),'default',true));
				}
					
				//total amount
				$total_payment_amount = $migsTxnData['mt_amount'];
				
				$migsTransactionDetailDb = new Studentfinance_Model_DbTable_TransactionMigsDetail();
				//get migs details data
				$migsTxnDataDetail = $migsTransactionDetailDb->fetchAll(array('tmd_tm_id = ?'=>$mt_id))->toArray();
				
				/*add receipt, payment, receipt_invoice*/
				$receiptDb = new Studentfinance_Model_DbTable_Receipt();
				$receipt_no = $this->getReceiptNoSeq();
				
				
				$receipt_data = array(
					'rcp_no' => $receipt_no,
					'rcp_account_code' => 7,	//bank islam
					'rcp_receive_date' => date('Y-m-d'),
					'rcp_payee_type' => 646, //student
					'rcp_appl_id' => $appl_id,
					'rcp_trans_id' => $idtransaction,
					'rcp_idStudentRegistration' => $trans_id,
					'rcp_description' => 'Payment from MIGS',
					'rcp_amount' => $total_payment_amount,
					'rcp_amount_default_currency' => $total_payment_amount,
					'rcp_adv_payment_amt' => 0.00,
					'rcp_gainloss' => 0,
					'rcp_gainloss_amt' => 0,
					'rcp_cur_id' => 1, //always MYR
					'rcp_status'=>'ENTRY',
					'UpdDate' =>date('Y-m-d H:i:s'),
					'migs_id'=>$mt_id //migs_id //$respond['receipt']['merchTxnRef']
				);
				$receipt_id = $receiptDb->insert($receipt_data);
				
				//add payment
				$paymentDb = new Studentfinance_Model_DbTable_Payment();
				$payment_data = array(
					'p_rcp_id' => $receipt_id,
					'p_payment_mode_id' => 0,
					'p_cheque_no' => null,
					'p_doc_bank' => null,
					'p_doc_branch' => null,
					'p_terminal_id' => null,
					'p_card_no' => null,
					'p_cur_id' => 1, //always MYR
					'p_status'=>'APPROVE',
					'p_migs'=>$mt_id ,//migs_id //$respond['receipt']['merchTxnRef']
					'p_amount' => $total_payment_amount,
					'p_amount_default_currency' => 0,
					'created_date' =>date('Y-m-d H:i:s'),
					'created_by'=>1
				);
				
				$paymentDb->insert($payment_data);

				$ebModel = new Studentfinance_Model_DbTable_EarlyBird();
				$ebitList = $ebModel->getEbit($trans_id);
				
				foreach($migsTxnDataDetail as $pro){
					
					$proformaID = $pro['tmd_invoice_id'];
					
					$invoice_id = $proformaID;
					
					//add receipt-invoice detail
					$receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
					
					$selectData = $db->select()
							->from(array('pim'=>'invoice_detail'), array('pim.*', 'dtl_id'=>'pim.id'))
							->join(array('im'=>'invoice_main'),'pim.invoice_main_id=im.id',array('main_id'=>'im.id','exchange_rate','im.semester', 'im.invoice_date', 'im.currency_id'))
							->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=pim.id',array('subject_id'))
							->join(array('c'=>'tbl_currency'), 'c.cur_id = pim.cur_id')
							->where("pim.id = ?", (int)$proformaID);
				
					$invoiceMainData = $db->fetchAll($selectData);
					
					foreach($invoiceMainData as $inv){
						
						$currencyItem = $inv['cur_id'];
						$currencyPaid = 1;

						//calculate ebit discount
						$ebAmount = 0.00;
						$totalEbAmount = 0.00;
						if ($ebitList){
							foreach ($ebitList as $ebitLoop){
								if ($ebitLoop['eb_semester']==$inv['semester']) {
									$ebitItem = $ebModel->checkEbitItem($ebitLoop['eb_discount_type'], $inv['fi_id']);

									if ($ebitItem) {
										if ($ebitItem['CalculationMode'] == 1) { //amount
											$ebAmount = $ebitItem['Amount'];
										} else { //percentage
											$ebAmount = (($ebitItem['Amount'] / 100) * $inv['amount']);
										}
									}

									$totalEbAmount = $totalEbAmount + $ebAmount;

									/*if ($ebAmount > 0.00){
                                        $bill_no = $ebModel->getBillSeq(9, date('Y'));

                                        $data_dn = array(
                                            'dcnt_fomulir_id' => $bill_no,
                                            'dcnt_IdStudentRegistration' => $trans_id,
                                            'dcnt_batch_id' => 0,
                                            'dcnt_amount' => $ebAmount,
                                            'dcnt_type_id' => $ebitLoop['eb_discount_type'],
                                            'dcnt_description' => $ebitLoop['eb_name'],
                                            'dcnt_currency_id' => $inv['currency_id'],
                                            'dcnt_creator' => 665,//by system
                                            'dcnt_create_date' => date('Y-m-d H:i:s'),
                                            'dcnt_approve_by' => 665,//by system
                                            'dcnt_approve_date' => date('Y-m-d H:i:s'),
                                            'dcnt_status' => 'A',//entry
                                        );
                                        $dn_id = $ebModel->insertEbDiscount($data_dn);

                                        $cnDetailData = array(
                                            'dcnt_id' => $dn_id,
                                            'dcnt_invoice_id' => $inv['main_id'],
                                            'dcnt_invoice_det_id' => $inv['dtl_id'],
                                            'dcnt_amount' => $ebAmount,
                                            'dcnt_description' => $ebitLoop['eb_name']
                                        );
                                        $ebModel->insertEbDiscountDtl($cnDetailData);
                                    }*/
								}
							}
						}

						$balance = $pamodel->getBalanceDtl($inv['dtl_id']);
						$inv['balance'] = str_replace(',', '', $balance['bill_balance']) < 0.00 ? 0.00:str_replace(',', '', $balance['bill_balance']);
						
						if($inv['cur_id'] == 1){
							$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
							//$dataCur = $curRateDB->getCurrentExchangeRate(2);
							$dataCur = $curRateDB->getRateByDate(2, $inv['invoice_date']);
							$amountNew = round((($inv['balance']-$totalEbAmount) / $dataCur['cr_exchange_rate']),2);
							$amountPaid = $inv['balance']-$totalEbAmount;
						}else{
							$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
							//$dataCur = $curRateDB->getData($inv['exchange_rate']);
							$dataCur = $curRateDB->getRateByDate(2, $inv['invoice_date']);
							$amountPaid = round((($inv['balance']-$totalEbAmount) * $dataCur['cr_exchange_rate']),2);
							$amountNew = $inv['balance']-$totalEbAmount;
							
						}
						
						$data_rcp_inv = array(
							'rcp_inv_rcp_id' => $receipt_id,
							'rcp_inv_rcp_no' => $receipt_no,
							'rcp_inv_invoice_id' => $inv['main_id'],
							'rcp_inv_invoice_dtl_id' => $invoice_id,
							'rcp_inv_amount' => $amountPaid,
							'rcp_inv_amount_default_currency' => $amountNew,
							'rcp_inv_cur_id' => 1,
							'rcp_inv_create_date' =>date('Y-m-d H:i:s'),
							'rcp_inv_create_by'=>1
						);
						
					
						$receiptInvoiceDb->insert($data_rcp_inv);
					
					}
				}
				
				$this->knockoffInvoice($receipt_id);
				
				
				/*if($typeid == 1){
					$url = 'http://e-university.inceif.org/studentfinance/payment/payment-success/mt_id/'.$mt_id;
					$this->_redirect($url);
				}else{
					//$url = 'http://apply.inceif.org/';
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'payment-success','mt_id'=>$mt_id),'default',true));
				}*/
				
//				exit;//TESTING
				//redirect success
				
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'payment-success','mt_id'=>$mt_id),'default',true));
				
			}else{
				//redirect error
				
				/*if($typeid == 1){
					$url = 'http://e-university.inceif.org/studentfinance/payment/payment-error';
					$this->_redirect($url);
				}else{
					//$url = 'http://apply.inceif.org/';
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'payment-error'),'default',true));
				}*/
				
				
				$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'payment-error'),'default',true));
			}
			
			
			
		}else{
			//invalid hash, so we will not store this response
			$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Invalid hash from payment gateway')));
			
				/*if($typeid == 1){
					$url = 'http://e-university.inceif.org/studentfinance/payment/payment-error';
					$this->_redirect($url);
				}else{
					//$url = 'http://apply.inceif.org/';
					$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'payment-error'),'default',true));
				}*/
				
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment', 'action'=>'payment-error'),'default',true));
		}
		//exit;
	}
	
	
public function respondTestingAction(){
		
		$auth = Zend_Auth::getInstance();
		
		$ses_migs = new Zend_Session_Namespace('migs');
		
		$trans_id = 3982;
		
		if ( !isset($trans_id ) ) {
			$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Payment not found')));
			$this->_redirect($this->view->url(array('module'=>'studentfinance','controller'=>'payment'),'default',true));
		}
		
		
		
		$studentRegistrationDb = new CourseRegistration_Model_DbTable_Studentregistration();
    	$transaction = $studentRegistrationDb->getTheStudentRegistrationDetail($trans_id);
    	
    	$appl_id = $transaction['IdApplication'];
    	$idtransaction = $transaction['transaction_id'];
    	
		$migs = new icampus_Function_Studentfinance_Migs();
		$migsTransactionDb = new Studentfinance_Model_DbTable_TransactionMigs();
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$paymentClass = new icampus_Function_Studentfinance_PaymentInfo();
		
		$result = array('status'=>false);
		$respond = $migs->readRespondParam($_GET);
//		$respond = array(); //TESTING
		
		//if hash is valid then update transaction respond
		//if($respond['hash_validation'] == 'CORRECT'){
		
		$mt_id = 183;
			
			$migsTxnData = $migsTransactionDb->fetchRow(array('mt_id = ?'=>$mt_id,'mt_idStudentRegistration'=>$trans_id));//$respond['receipt']['merchTxnRef']
//$migsTxnData = $migsTransactionDb->fetchRow(array('mt_id = ?'=>84, 'mt_appl_id = ?'=>2924,'mt_trans_id'=>2964));//TESTING

			$ses_migs->migs_txn = $migsTxnData;
			
			//check for valid migs txn with applicant id
			if ( !$migsTxnData ) {
				$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Invalid migs transaction')));
				$this->_redirect($this->view->url(array('module'=>'default','controller'=>'applicant-portal'),'default',true));
				
				$migsTxnData = $migsTxnData->toArray();
				
				
			}
			
			
			//amount / 100
			$amount = 60.00;
			$respond['receipt']['amount'] = $amount/100; 
			
			//insert migs payment
				$paymentMigsDb = new Studentfinance_Model_DbTable_PaymentMigs();
				$data_pm_migs = array(
					'pm_txn_id' => $mt_id,
					'pm_amount' => $respond['receipt']['amount'],
					'pm_currency_id' => 1,
					'pm_date_create'=>date('Y-m-d H:i:s')	
				);
				$paymentMigsDb->insert($data_pm_migs);
				
			
			//update migs transaction data	
			/*$data_upd = array(
				'mt_rep_raw' => $_SERVER["QUERY_STRING"],
				'mt_rep_error_status' => $respond['error_exists'],
				'mt_rep_hash_validation' => $respond['hash_validation'],
				'mt_rep_amount' => $respond['receipt']['amount'],
				'mt_rep_locale'	=> $respond['receipt']['locale'],
				'mt_rep_batch_no'	=> $respond['receipt']['batchNo'],
				'mt_rep_command'	=> $respond['receipt']['command'],
				'mt_rep_message'	=> $respond['receipt']['message'],
				'mt_rep_version'	=> $respond['receipt']['version'],
				'mt_rep_card_type'	=> $respond['receipt']['cardType'],
				'mt_rep_order_info'	=> $respond['receipt']['orderInfo'],
				'mt_rep_receipt_no'	=> $respond['receipt']['receiptNo'],
				'mt_rep_merchant_id'	=> $respond['receipt']['merchantID'],
				'mt_rep_authorize_id'	=> $respond['receipt']['authorizeID'],
				'mt_rep_merch_txn_ref'	=> $respond['receipt']['merchTxnRef'],
				'mt_rep_transaction_no'	=> $respond['receipt']['transactionNo'],
				'mt_rep_acq_response_code'	=> $respond['receipt']['acqResponseCode'],
				'mt_rep_txn_response_code'	=> $respond['receipt']['txnResponseCode'],
				'mt_rep_txn_response_code_desc'	=> $respond['receipt']['txnResponseCodeDescription'],
					
				'mt_3d_rep_ver_type'	=> $respond['3d_secure_data']['verType'],
				'mt_3d_rep_ver_status'	=> $respond['3d_secure_data']['verStatus'],
				'mt_3d_rep_ver_status_desc'	=> $respond['3d_secure_data']['verStatusDescription'],
				'mt_3d_rep_token'	=> $respond['3d_secure_data']['token'],
				'mt_3d_rep_ver_secur_level'	=> $respond['3d_secure_data']['verSecurLevel'],
				'mt_3d_rep_enrolled'	=> $respond['3d_secure_data']['enrolled'],
				'mt_3d_rep_xid'	=> $respond['3d_secure_data']['xid'],
				'mt_3d_rep_acq_eci'	=> $respond['3d_secure_data']['acqECI'],
				'mt_3d_rep_auth_status'	=> $respond['3d_secure_data']['authStatus'],
				'mt_from'	=> 1, //from student portal
				
			);*/
			
//			if( !$respond['error_exists'] && $respond['receipt']['txnResponseCode'] == 0){
				$data_upd['mt_status'] = 'TESTING';
//			}else{
//				$data_upd['mt_status'] = 'FAIL';
//			}
			
			$migsTransactionDb->update($data_upd, 'mt_id = '.$mt_id);
			
			
			//insert payment record if txn success
			//if( !$respond['error_exists'] && $respond['receipt']['txnResponseCode'] == 0){
				
				/*
				 *  1) Pay - simpan proforma yang tick
					2) Respond Success - generate invoice, knockoff, receipt, payment
					3) hantar MYR only masa Pay - convert
					4) proceed once migs status = success only
				 * 
				 */
				
				/*
				 * 1) generate invoice
				 * 2) knockoff invoice
				 * 3) insert payment & receipt & receipt_invoice
				 * 4) update column migs_id at receipt table
				 */
					
				//total amount
				$total_payment_amount = $migsTxnData['mt_amount'];
				
				$migsTransactionDetailDb = new Studentfinance_Model_DbTable_TransactionMigsDetail();
				//get migs details data
				$migsTxnDataDetail = $migsTransactionDetailDb->fetchAll(array('tmd_tm_id = ?'=>$mt_id))->toArray();
				
				/*add receipt, payment, receipt_invoice*/
				$receiptDb = new Studentfinance_Model_DbTable_Receipt();
				$receipt_no = $this->getReceiptNoSeq();
				
				
				$receipt_data = array(
					'rcp_no' => $receipt_no,
					'rcp_account_code' => 7,	//bank islam
					'rcp_receive_date' => date('Y-m-d'),
					'rcp_payee_type' => 646, //student
					'rcp_appl_id' => $appl_id,
					'rcp_trans_id' => $idtransaction,
					'rcp_idStudentRegistration' => $trans_id,
					'rcp_description' => 'Payment from MIGS',
					'rcp_amount' => $total_payment_amount,
					'rcp_amount_default_currency' => $total_payment_amount,
					'rcp_adv_payment_amt' => 0.00,
					'rcp_gainloss' => 0,
					'rcp_gainloss_amt' => 0,
					'rcp_cur_id' => 1, //always MYR
					'rcp_status'=>'ENTRY',
					'UpdDate' =>date('Y-m-d H:i:s'),
					'migs_id'=>$mt_id //migs_id //$respond['receipt']['merchTxnRef']
				);
				$receipt_id = $receiptDb->insert($receipt_data);
				
				//add payment
				$paymentDb = new Studentfinance_Model_DbTable_Payment();
				$payment_data = array(
					'p_rcp_id' => $receipt_id,
					'p_payment_mode_id' => 0,
					'p_cheque_no' => null,
					'p_doc_bank' => null,
					'p_doc_branch' => null,
					'p_terminal_id' => null,
					'p_card_no' => null,
					'p_cur_id' => 1, //always MYR
					'p_status'=>'APPROVE',
					'p_migs'=>$mt_id ,//migs_id //$respond['receipt']['merchTxnRef']
					'p_amount' => $total_payment_amount,
					'p_amount_default_currency' => 0,
					'created_date' =>date('Y-m-d H:i:s'),
					'created_by'=>1
				);
				
				$paymentDb->insert($payment_data);
				
				echo "<pre>";
				print_r($migsTxnDataDetail);
				
				foreach($migsTxnDataDetail as $pro){
					
					$proformaID = $pro['tmd_invoice_id'];
					
					$invoice_id = $proformaID;
					
					//add receipt-invoice detail
					$receiptInvoiceDb = new Studentfinance_Model_DbTable_ReceiptInvoice();
					
					$selectData = $db->select()
							->from(array('pim'=>'invoice_detail'))
							->join(array('im'=>'invoice_main'),'pim.invoice_main_id=im.id',array('main_id'=>'im.id','exchange_rate','semester'))
							->joinLeft(array('ivs'=>'invoice_subject'),'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=pim.id',array('*'))
							->join(array('c'=>'tbl_currency'), 'c.cur_id = pim.cur_id')
							->where("pim.id = ?", (int)$proformaID);
				
					$invoiceMainData = $db->fetchAll($selectData);
					
					foreach($invoiceMainData as $inv){
						
						$currencyItem = $inv['cur_id'];
						$currencyPaid = 1;
						
						if($inv['cur_id'] ==  1){
							$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
							$dataCur = $curRateDB->getCurrentExchangeRate(2);
							$amountNew = round($inv['amount'] / $dataCur['cr_exchange_rate'],2);
							$amountPaid = $inv['amount'];
						}else{
							$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
							$dataCur = $curRateDB->getData($inv['exchange_rate']);
							$amountPaid = round($inv['amount'] * $dataCur['cr_exchange_rate'],2);
							$amountNew = $inv['amount'];
							
						}
						
						$data_rcp_inv = array(
							'rcp_inv_rcp_id' => $receipt_id,
							'rcp_inv_rcp_no' => $receipt_no,
							'rcp_inv_invoice_id' => $inv['main_id'],
							'rcp_inv_invoice_dtl_id' => $invoice_id,
							'rcp_inv_amount' => $amountPaid,
							'rcp_inv_amount_default_currency' => $amountNew,
							'rcp_inv_cur_id' => 1,
							'rcp_inv_create_date' =>date('Y-m-d H:i:s'),
							'rcp_inv_create_by'=>1
						);
						
					
						$receiptInvoiceDb->insert($data_rcp_inv);
					
					}
				}
				
				$this->knockoffInvoice($receipt_id);
					
				
				exit;//TESTING
				//redirect success
				$this->_redirect($this->view->url(array('module'=>'default','controller'=>'payment', 'action'=>'payment-success','mt_id'=>$mt_id),'default',true));
				
			/*}else{
				//redirect error
				$this->_redirect($this->view->url(array('module'=>'default','controller'=>'payment', 'action'=>'payment-error'),'default',true));
			}*/
			
			
			
		/*}else{
			//invalid hash, so we will not store this response
			$this->_helper->flashMessenger->addMessage(array('error'=>$this->view->translate('Invalid hash from payment gateway')));
			$this->_redirect($this->view->url(array('module'=>'default','controller'=>'payment', 'action'=>'payment-error'),'default',true));
		}*/
		//exit;
	}

	public function testKnockoffAction(){
		$totalEbAmount = 0.00;
		$pamodel = new Studentfinance_Model_DbTable_PaymentAmount();
		$cur_id = 2;

		$balance = $pamodel->getBalanceDtl(58544);
		$inv['balance'] = str_replace(',', '', $balance['bill_balance']) < 0.00 ? 0.00:str_replace(',', '', $balance['bill_balance']);

		if($cur_id == 1){
			$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
			//$dataCur = $curRateDB->getCurrentExchangeRate(2);
			$dataCur = $curRateDB->getRateByDate(2, '2016-09-16');
			$amountNew = round((($inv['balance']-$totalEbAmount) / $dataCur['cr_exchange_rate']),2);
			$amountPaid = $inv['balance'];
		}else{
			$curRateDB = new Studentfinance_Model_DbTable_CurrencyRate();
			//$dataCur = $curRateDB->getData($inv['exchange_rate']);
			$dataCur = $curRateDB->getRateByDate(2, '2016-09-16');
			$amountPaid = round((($inv['balance']-$totalEbAmount) * $dataCur['cr_exchange_rate']),2);
			$amountNew = $inv['balance'];
		}

		var_dump($amountPaid);
		var_dump($amountNew);

		exit;
	}
}