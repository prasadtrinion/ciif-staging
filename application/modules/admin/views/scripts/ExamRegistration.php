<?php 
class Admin_Model_DbTable_ExamRegistration extends Zend_Db_Table_Abstract
{
    protected $_name    = 'exam_registration';
    protected $_primary = "er_id";
    protected $_history = 'exam_registration_history';

    public function getData($er_id, $join = true) {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('er' => $this->_name))
            ->where("er_id = ?", $er_id);

        if($join) {
            $query->join(array('l'  => 'tbl_landscape'), 'l.IdLandscape=er.landscape_id', array('IdStartSemester', 'IdStartSemester as IdIntake', 'IdProgram', 'IdProgramScheme'));
            $query->join(array('es' => 'exam_schedule'), 'es.es_id=er.examschedule_id', array('es_date'));
            // $query->join(array('ec' => 'tbl_exam_center'), 'es.es_exam_center=ec.ec_id', array('ec_name'));
        }

        $result = $db->fetchRow($query);

        return $result;
    }

    public function addData($data) {
        $auth  = Zend_Auth::getInstance();
        $erhDB = new Admin_Model_DbTable_ExamRegistrationHistory();

        if(isset($data['er_id'])) {
            if(!empty($data['er_id'])) {
                $data['modifieddt']  = date('Y-m-d H:i:s');
                $data['modifiedby']  = $auth->getIdentity()->iduser;
                $this->update($data, 'er_id ='. $data['er_id']);

                $er = $this->getData($data['er_id'], false);
                $erhDB->addData($er, 'Update');

                return $data['er_id'];
            }
            unset($data['er_id']);
        }

        $data['createddt']  = date('Y-m-d H:i:s');
        $data['createdby']  = $auth->getIdentity()->iduser;

        $er_id = $this->insert($data);

        $data['er_id'] = $er_id;
        $erhDB->addData($data, 'Insert');

        return $er_id;
    }

    public function getDataByStudentId($student_id, $join = true) {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('er'=>$this->_name))
            ->where('er.student_id = ?', $student_id)
            ->where("er.er_registration_type in (0, 1, 4, 9)")
            ->group("er.examsetupdetail_id");

        $result = $db->fetchAll($query);

        $exam_registration = array();

        foreach($result as $row) {
            $examsetupdetail_id = $row['examsetupdetail_id'];
            $query = $db->select()
                ->from(array('er'=>$this->_name))
                ->where('er.student_id = ?', $student_id)
                ->where('er.examsetupdetail_id = ?', $examsetupdetail_id)
                ->where("er.er_registration_type in (0, 1, 4, 9)")
                // ->where("er.pass is null")
                ->order("er.createddt DESC");

            $query->join(array('esd' => 'exam_setup_detail'), 'esd.esd_id=er.examsetupdetail_id', array());
            $query->join(array('es'  => 'exam_setup'), 'esd.es_id=es.es_id', array('es_name', 'es_idProgram'));
            $query->join(array('p'   => 'tbl_program'), 'p.IdProgram=es.es_idProgram', array('ProgramName', 'ProgramCode'));
            $query->join(array('l'   => 'tbl_landscape'),'l.IdLandscape=es.es_idLandscape',array('IdStartSemester','IdProgramScheme'));
            $query->join(array('str'  => 'tbl_studentregistration'), 'str.IdStudentRegistration = er.student_id', array());
            $query->join(array('std'  => 'student_profile'), 'std.std_id = str.sp_id', array('std_fullname','std_type_nationality','std_nationality','std_idnumber'));


            if($join) {
                $query->joinLeft(array('i'   => 'tbl_intake'),'i.IdIntake = l.IdStartSemester',array('IntakeDesc'));
                $query->join(array('pe'  => 'programme_exam'), 'esd.esd_pe_id=pe.pe_id', array('pe_id', 'pe_name'));
                $query->join(array('scd' => 'exam_schedule'), 'scd.es_id=er.examschedule_id', array('es_date'));
                $query->join(array('j' => 'exam_scheduletaggingslot'), 'j.sts_schedule_id = scd.es_id', array('schedule_tagging_slot_id' => 'sts_id'));
                $query->join(array('k' => 'exam_slot'), 'k.sl_id = j.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'));
                $query->join(array('l2' => 'exam_taggingslotcenterroom'), 'l2.tsc_slotid = tsc_examcenterid', array('tagging_slot_center_room_id' => 'tsc_id'));
                $query->join(array('m' => 'tbl_exam_center'), 'm.ec_id =  l2.tsc_examcenterid', array('exam_center_id' => 'ec_id', 'ec_name'));

                // $query->join(array('def' => 'tbl_definationms'), 'scd.es_session=def.idDefinition', array('DefinitionCode as es_session_text'));
                $query->joinLeft(array('sm' => 'tbl_subjectmaster'), 'esd.esd_idSubject=sm.IdSubject', array('SubjectName','BahasaIndonesia'));
//                $query->join(array('str'  => 'tbl_studentregistration'), 'str.IdStudentRegistration = er.student_id', array());
//                $query->join(array('std'  => 'student_profile'), 'std.std_id = str.sp_id', array('std_fullname','std_type_nationality','std_nationality','std_idnumber'));
                $query->order('es_date asc');
                $query->order('es_start_time asc');
            }

            $result = $db->fetchRow($query);



            if($result) {
                $exam_registration[$examsetupdetail_id] = $result;
            }
        }

        return $exam_registration;
    }

    public function getDataByStudentProfileId($std_id, $join = true) {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('er' => $this->_name))
            ->join(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration = er.student_id', array('IdStudentRegistration'))
            ->join(array('sp' => 'student_profile'), 'sp.std_id = sr.sp_id', array())
            ->where('sp.std_id = ?', $std_id)
            ->where("er.er_registration_type in (0, 1, 4, 9)")
            ->group("er.examsetupdetail_id");

        $result = $db->fetchAll($query);

        $exam_registration = array();

        foreach($result as $row) {
            $examsetupdetail_id = $row['examsetupdetail_id'];
            $query = $db->select()
                ->from(array('er'=>$this->_name))
                ->join(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration = er.student_id', array('IdStudentRegistration'))
                ->join(array('sp' => 'student_profile'), 'sp.std_id = sr.sp_id', array())
                ->where('sp.std_id = ?', $std_id)
                ->where('er.examsetupdetail_id = ?', $examsetupdetail_id)
                ->where("er.er_registration_type in (0, 1, 4, 9)")
                // ->where("er.pass is null")
                ->order("er.createddt DESC");

            if($join) {
                $query->join(array('esd' => 'exam_setup_detail'), 'esd.esd_id=er.examsetupdetail_id', array());
                $query->join(array('es'  => 'exam_setup'), 'esd.es_id=es.es_id', array('es_name', 'es_idProgram'));
                $query->join(array('l'   => 'tbl_landscape'),'l.IdLandscape=es.es_idLandscape',array('IdStartSemester','IdProgramScheme'));
                $query->joinLeft(array('i'   => 'tbl_intake'),'i.IdIntake = l.IdStartSemester',array('IntakeDesc'));
                $query->join(array('p'   => 'tbl_program'), 'p.IdProgram=es.es_idProgram', array('ProgramName', 'ProgramCode'));
                $query->join(array('pe'  => 'programme_exam'), 'esd.esd_pe_id=pe.pe_id', array('pe_id', 'pe_name'));
                $query->join(array('scd' => 'exam_schedule'), 'scd.es_id=er.examschedule_id', array('es_date'));
                
                $query->join(array('j' => 'exam_scheduletaggingslot'), 'j.sts_schedule_id = scd.es_id', array('schedule_tagging_slot_id' => 'sts_id'));
                $query->join(array('k' => 'exam_slot'), 'k.sl_id = j.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'));
                $query->join(array('l2' => 'exam_taggingslotcenterroom'), 'l2.tsc_slotid = tsc_examcenterid', array('tagging_slot_center_room_id' => 'tsc_id'));
                $query->join(array('m' => 'tbl_exam_center'), 'm.ec_id =  l2.tsc_examcenterid', array('exam_center_id' => 'ec_id', 'ec_name'));

                // $query->join(array('def' => 'tbl_definationms'), 'scd.es_session=def.idDefinition', array('DefinitionCode as es_session_text'));
                $query->joinLeft(array('sm' => 'tbl_subjectmaster'), 'esd.esd_idSubject=sm.IdSubject', array('SubjectName','BahasaIndonesia'));
                $query->join(array('str'  => 'tbl_studentregistration'), 'str.IdStudentRegistration = er.student_id', array());
                $query->join(array('std'  => 'student_profile'), 'std.std_id = str.sp_id', array('std_fullname','std_type_nationality','std_nationality','std_idnumber'));
                $query->joinLeft(array('att' => 'tbl_definationms'),'er.attendance_status = att.idDefinition', array('attendance_status_desc' => 'DefinitionDesc'));
                $query->order('es_date asc');
                $query->order('es_start_time asc');
            }

            $result = $db->fetchRow($query);



            if($result) {
                $exam_registration[$examsetupdetail_id] = $result;
            }
        }

        return $exam_registration;
    }

    public function deleteData($er_id) {
        $erhDB = new Admin_Model_DbTable_ExamRegistrationHistory();
        $er    = $this->getData($er_id, false);
        $erhDB->addData($er, 'Delete');
        return $this->delete("er_id = $er_id");
    }

    public function deleteDataByStudentId($student_id) {
        $erhDB = new Admin_Model_DbTable_ExamRegistrationHistory();

        $registrations = $this->getDataByStudentId($student_id, false);
        foreach($registrations as $data) {
            $er_id = $data['er_id'];

            $erhDB->addData($data, 'Delete');
            $this->delete("er_id = $er_id");
        }
    }

    public function getDataByEsId($es_id) {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('er' => $this->_name))
            ->join(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration = er.student_id', array('IdStudentRegistration'))
            ->join(array('subj' => 'tbl_subjectmaster'), 'er.IdProgram = subj.IdSubject', array('SubCode', 'SubjectName'))
            ->join(array('sp' => 'student_profile'), 'sp.std_id = sr.sp_id', array('std_id', 'std_fullname', 'std_idnumber'))
            ->join(array('es' => 'exam_schedule'), 'er.examschedule_id = es.es_id')
            ->join(array('ec' => 'tbl_exam_center'), 'ec.ec_id = er.examcenter_id', array('ec_name'))
            ->where('er.examschedule_id = ?', $es_id);
        $result = $db->fetchAll($query);
        return $result;
    }

    public function getAprrovalDataByEsId($es_id) {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('er'=>$this->_name))
            ->where('er.examschedule_id = ?', $es_id)
            ->where('er.result_status = 1 OR er.result_status = 2');
        $result = $db->fetchAll($query);
        return $result;
    }

    public function updateMark($params, $remarks = 'Update Mark (SMS)') {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $erhDB = new Admin_Model_DbTable_ExamRegistrationHistory();

        $query = $db->select()
            ->from(array('sme' => 'tbl_student_marks_entry'))
            // ->where('IdIntake = ?', $params['IdIntake'])
            ->where('IdProgram = ?', $params['IdProgram'])
            ->where('pe_id = ?', $params['pe_id'])  // programme exam
            ->where('es_id = ?', $params['es_id'])  // exam schedule
            ->where('er_id = ?', $params['er_id']); // exam registration
        $components = $db->fetchAll($query);
        
        $total_marks = 0;

        foreach($components as $mdm) {
            $query = $db->select()
                ->from(array('sdme' => 'tbl_student_detail_marks_entry'), array('total_marks' => 'sum(FinalMarksObtained)'))
                ->where('IdStudentMarksEntry = ?', $mdm['IdStudentMarksEntry']);
            $result       = $db->fetchRow($query);
            $total_marks += $result['total_marks'];
            
            $tbl_student_marks_entry_data = array(
                'TotalMarkObtained'      => $result['total_marks'],
                'FinalTotalMarkObtained' => $result['total_marks']
            );
            $tbl_student_marks_entry_where = array(
                'IdStudentMarksEntry = ?' => $mdm['IdStudentMarksEntry']
            );
            $db->update('tbl_student_marks_entry', $tbl_student_marks_entry_data, $tbl_student_marks_entry_where);
        }

        $data = [
            'total_marks' => $total_marks,
            'pass'        => null,
            'grade_name'  => null
        ];

        if($total_marks == 0) {
            $this->update($data, "er_id = ". $params['er_id']);

            $er = $this->getData($params['er_id'], false);
            $erhDB->addData($er, $remarks);

            return '';
        }
        
        $ExamSetupDB       = new Admin_Model_DbTable_ExamSetup();
        $ExamSetupDetailDB = new Admin_Model_DbTable_ExamSetupDetail();
        $ProgrammeExamDB   = new Admin_Model_DbTable_ProgrammeExam();
        $gradeDB           = new Admin_Model_DbTable_Grade();

        $registration = $this->getData($params['er_id']);
        $IdIntake     = $registration['IdIntake'] = 0;
        $ip_id        = $registration['intakeprogram_id'];
        $IdProgram    = $registration['IdProgram'];
        $exam_date    = $registration['es_date'];
        $grade        = $gradeDB->getGrade($IdIntake, $IdProgram, $total_marks, $exam_date);

        $data['pass']       = $grade['Pass'];
        $data['grade_name'] = $grade['GradeName'];

        $this->update($data, "er_id = ". $params['er_id']);

        $er = $this->getData($params['er_id'], false);
        $erhDB->addData($er, $remarks);

        return $grade['GradeName'];
    }

    public function UpdateStatus($result_status, $er_id) {
        $erhDB = new Admin_Model_DbTable_ExamRegistrationHistory();
        $auth  = Zend_Auth::getInstance();

        $data   = array('result_status' => $result_status);
        
        if($result_status == 2){
            $data['er_registration_type'] = 10;
            $data['result_endorsed_by']   = $auth->getIdentity()->iduser;
            $data['result_endorsed_date'] = date('Y-m-d H:i:s');
            $data['exam_status']          = 'C';
        }

        $status = $this->update($data, "er_id = $er_id");
        
        $er = $this->getData($er_id, false);
        $erhDB->addData($er, 'Update status');
        
        return $status;
    }

    public function getExamSlipData123123($IdStudentRegistration, $ber_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('er_id', 'exam_registrationID' => 'registrationId', 'ber_id', 'bes_id', 'examsetupdetail_id'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array('IdStudentRegistration', 'student_registrationID' => 'registrationId'))
            ->join(array('c' => 'student_profile'),'c.std_id = b.sp_id')
            ->joinLeft(array('d' => 'tbl_countries'), 'd.idCountry = c.std_nationality', array('CountryName'))
            ->joinLeft(array('e' => 'tbl_qualificationmaster'), 'e.IdQualification = c.std_qualification', array('QualificationLevel'))
            // ->join(array('f' => 'exam_schedule'), 'a.examschedule_id = f.es_id', array('es_id', 'es_date','es_start_time', 'es_end_time'))
            // ->join(array('g' => 'tbl_exam_center'), 'f.es_exam_center = g.ec_id', array('ec_name'))
            ->where('a.ber_id = ?', $ber_id)
            ->where('a.student_id = ?', $IdStudentRegistration);
            // ->order('f.es_date ASC');
            
        return $db->fetchAll($select);
    }

    public function getExamSlipData($IdStudentRegistration, $ber_id) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('er' => $this->_name), array('er_id', 'exam_registrationID' => 'registrationId', 'ber_id', 'bes_id', 'examsetupdetail_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration = er.student_id', array('IdStudentRegistration', 'student_registrationID' => 'registrationId'))
            ->join(array('sp' => 'student_profile'),'sp.std_id = sr.sp_id')
            ->join(array('a' => 'batch_exam_schedule'), 'a.bes_id = er.bes_id',  array('bes_id'))
            ->join(array('b' => 'exam_schedule'), 'b.es_id = a.es_id', array('es_id', 'es_date', 'es_start_time', 'es_end_time'))
            ->join(array('c' => 'tbl_exam_center'), 'c.ec_id = b.es_exam_center', array('ec_id', 'ec_name'))
            ->join(array('f' => 'tbl_definationms'), 'f.idDefinition = b.es_session', array('es_session_desc' => 'DefinitionDesc'))
            ->join(array('g' => 'exam_setup'), 'g.es_id = b.es_examsetup_id', array('examsetup_id'=>'es_id', 'examsetup_name'=>'es_name', 'IdLandscape' => 'es_idLandscape'))
            ->join(array('h' => 'exam_setup_detail'), 'h.esd_id = b.es_esd_id', array('esd_id'))
            ->join(array('i' => 'tbl_markdistribution_setup'), 'i.mds_id = h.esd_mds_id', array('mds_id', 'mds_name'))
            ->join(array('j' => 'programme_exam'), 'j.pe_id = i.mds_programme_exam', array('pe_id', 'pe_name'))
            ->join(array('k' => 'tbl_examroom'), 'k.id = er.examroom_id', array('examroom_name' => 'name'))
            ->joinLeft(array('l' => 'tbl_subjectmaster'), 'l.IdSubject = h.esd_idSubject', array('IdSubject', 'SubjectName', 'SubCode'))
            ->where('a.ber_id = ?', $ber_id)
            ->where('er.student_id = ?', $IdStudentRegistration)
            ->where('a.active = 1')
            ->order('b.es_date ASC')
            ->order('b.es_start_time ASC');

        return $db->fetchAll($select);
    }

/*
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.IdLandscape', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('d' => 'tbl_definationms'), 'd.idDefinition = c.mode_of_program', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->join(array('e' => 'tbl_program'), 'e.IdProgram = a.IdProgram', array('IdProgram', 'ProgramCode', 'ProgramName'))
            ->join(array('f' => 'exam_setup'), 'f.es_id = a.examsetup_id', array('examsetup_id' => 'es_id', 'es_name', 'es_online', 'es_awardname'))

*/

    public function getExamlmsSlipData($IdStudentRegistration, $er_id) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('er' => $this->_name), array('er_id', 'exam_registrationID' => 'registrationId', 'ber_id', 'bes_id', 'examsetupdetail_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration = er.student_id', array('IdStudentRegistration', 'student_registrationID' => 'registrationId'))
            ->join(array('sp' => 'student_profile'),'sp.std_id = sr.sp_id')
            //->join(array('a' => 'batch_exam_schedule'), 'a.bes_id = er.bes_id',  array('bes_id'))
            ->join(array('b' => 'exam_schedule'), 'b.es_id = er.examschedule_id', array('es_id', 'es_date', 'es_start_time', 'es_end_time'))
            ->join(array('c' => 'tbl_exam_center'), 'c.ec_id = b.es_exam_center', array('ec_id', 'ec_name'))
            ->join(array('f' => 'tbl_definationms'), 'f.idDefinition = b.es_session', array('es_session_desc' => 'DefinitionDesc'))
            ->join(array('g' => 'exam_setup'), 'g.es_id = b.es_examsetup_id', array('examsetup_id'=>'es_id', 'examsetup_name'=>'es_name', 'IdLandscape' => 'es_idLandscape'))
            ->join(array('h' => 'exam_setup_detail'), 'h.esd_id = b.es_esd_id', array('esd_id'))
            ->join(array('i' => 'tbl_markdistribution_setup'), 'i.mds_id = h.esd_mds_id', array('mds_id', 'mds_name'))
            ->join(array('j' => 'programme_exam'), 'j.pe_id = i.mds_programme_exam', array('pe_id', 'pe_name'))
            ->join(array('k' => 'tbl_examroom'), 'k.id = er.examroom_id', array('examroom_name' => 'name'))
            ->joinLeft(array('l' => 'tbl_subjectmaster'), 'l.IdSubject = h.esd_idSubject', array('IdSubject', 'SubjectName', 'SubCode'))
            ->join(array('m' => 'tbl_landscape'), 'm.IdLandscape = er.landscape_id', array())
            ->join(array('n' => 'tbl_program_scheme'), 'n.IdProgramScheme = m.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('o' => 'tbl_definationms'), 'o.idDefinition = n.mode_of_program', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->join(array('p' => 'tbl_program'), 'p.IdProgram = er.IdProgram', array('IdProgram', 'ProgramCode', 'ProgramName'))
            ->where('er.er_id = ?', $er_id)
            ->where('er.student_id = ?', $IdStudentRegistration);
            //->where('a.active = 1')
            //->order('b.es_date ASC')
            //->order('b.es_start_time ASC');

        return $db->fetchAll($select);
    }

    public function getDataById($er_id, $join = true) {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('er'=>$this->_name))
            ->where('er.er_id = ?', $er_id)
            ->where("er.er_registration_type in (0, 1, 4, 9, 10)")
            ->group("er.examsetupdetail_id");

        if($join) {
            $query->join(array('sr' => 'tbl_studentregistration'), 'sr.IdStudentRegistration = er.student_id', array('IdStudentRegistration', 'student_registrationID' => 'registrationId'));
            $query->join(array('sp' => 'student_profile'),'sp.std_id = sr.sp_id', array('std_idnumber', 'std_fullname', 'std_email','at_repository','std_id'));
            $query->join(array('esd' => 'exam_setup_detail'), 'esd.esd_id=er.examsetupdetail_id', array());
            $query->joinLeft(array('module' => 'tbl_subjectmaster'), 'esd.esd_idSubject = module.IdSubject', array('IdSubject', 'SubjectName', 'SubCode'));
            $query->join(array('es'  => 'exam_setup'), 'esd.es_id=es.es_id', array('es_name', 'es_idProgram'));
            $query->join(array('l'   => 'tbl_landscape'),'l.IdLandscape=es.es_idLandscape',array('IdStartSemester'));
            $query->joinLeft(array('i'   => 'tbl_intake'),'i.IdIntake = l.IdStartSemester',array('IntakeDesc'));
            $query->join(array('p'   => 'tbl_program'), 'p.IdProgram=es.es_idProgram', array('ProgramName', 'ProgramCode','esmtp_id'));
            $query->join(array('ps'   => 'tbl_program_scheme'), 'ps.IdProgramScheme = sr.IdProgramScheme', array())
            ->join(array('x' => 'tbl_definationms'), 'x.idDefinition = ps.mode_of_program', array('mop' => 'DefinitionDesc'));
            $query->join(array('pe'  => 'programme_exam'), 'esd.esd_pe_id=pe.pe_id', array('pe_id', 'pe_name'));
            $query->join(array('scd' => 'exam_schedule'), 'scd.es_id=er.examschedule_id', array(
                'examschedule_id' => 'es_id',
                'es_date',
                'result_publish_date'  => "CONCAT_WS(' ', es_publish_start_date, es_publish_start_time)",
                'allow_publish_result' => "DATEDIFF(CURDATE(), es_publish_start_date)"
            ));
            $query->join(array('j' => 'exam_scheduletaggingslot'), 'j.sts_id = er.examtaggingslot_id', array('schedule_tagging_slot_id' => 'sts_id'));
            $query->join(array('k' => 'exam_slot'), 'k.sl_id = j.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'));
            $query->join(array('l2' => 'exam_taggingslotcenterroom'), 'l2.tsc_slotid = k.sl_id AND l2.tsc_examcenterid = er.examcenter_id', array('tagging_slot_center_room_id' => 'tsc_id'));
            $query->join(array('m' => 'tbl_exam_center'), 'm.ec_id = er.examcenter_id', array('exam_center_id' => 'ec_id', 'ec_name'))
                ->joinLeft(array('n' => 'tbl_examroom'), 'n.id = er.examroom_id', array('examroom_name' => 'name'));
            $query ->joinLeft(array('u' => 'tbl_address'), "u.add_org_id = m.ec_id and u.add_org_name = 'tbl_exam_center'", array('add_address1', 'add_address2', 'add_zipcode', 'add_phone' ))
                ->joinLeft(array('def'=>'tbl_definationms'),'u.add_address_type=def.idDefinition',array('addressType'=>'DefinitionDesc'))
                ->joinLeft(array('coun'=>'tbl_countries'),'u.add_country=coun.idCountry',array('CountryName'))
                ->joinLeft(array('st'=>'tbl_state'),'u.add_state=st.idState',array('StateName'))
                ->joinLeft(array('ct'=>'tbl_city'),'u.add_city=ct.idCity',array('CityName'));


            // $query->join(array('def' => 'tbl_definationms'), 'scd.es_session=def.idDefinition', array('DefinitionCode as es_session_text'));



            $query->joinLeft(array('sm' => 'tbl_subjectmaster'), 'esd.esd_idSubject=sm.IdSubject', array('SubjectName','BahasaIndonesia'));
            $query->order('es_date asc');
            $query->order('es_start_time asc');
        }

        $result = $db->fetchRow($query);

        return $result;
    }

    public function getDataByBerId($ber_id, $group = true, $join = true, $params = array()) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('er_id', 'exam_registrationID' => 'registrationId', 'ber_id', 'bes_id', 'examsetupdetail_id', 'er_registration_type', 'er_main_id'))
            ->where('a.ber_id = ?', $ber_id)
            ->where("a.er_registration_type in (0, 1, 4, 9, 10)");

        if($join) {
            $select->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array('IdStudentRegistration', 'student_registrationID' => 'registrationId'));
            $select->join(array('c' => 'student_profile'),'c.std_id = b.sp_id');
            $select->joinLeft(array('d' => 'tbl_countries'), 'd.idCountry = c.std_nationality', array('CountryName'));
            $select->joinLeft(array('e' => 'tbl_qualificationmaster'), 'e.IdQualification = c.std_qualification', array('QualificationLevel'));
            $select->join(array('f' => 'exam_schedule'), 'f.es_id = a.examschedule_id', array('examschedule_id' => 'es_id', 'es_id', 'es_date'));
            $select->join(array('h' => 'exam_setup_detail'), 'h.esd_id = f.es_esd_id', array('esd_id', 'esd_pe_id'));
            $select->joinLeft(array('i' => 'tbl_subjectmaster'), 'i.IdSubject = h.esd_idSubject', array('IdSubject', 'SubjectName'));
            $select->join(array('j' => 'exam_scheduletaggingslot'), 'j.sts_schedule_id = f.es_id', array('schedule_tagging_slot_id' => 'sts_id'));
            $select->join(array('k' => 'exam_slot'), 'k.sl_id = j.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'));
            $select->join(array('l' => 'exam_taggingslotcenterroom'), 'l.tsc_slotid = k.sl_id AND l.tsc_examcenterid = a.examcenter_id', array('tagging_slot_center_room_id' => 'tsc_id'));
            $select->join(array('m' => 'tbl_exam_center'), 'm.ec_id =  a.examcenter_id', array('exam_center_id' => 'ec_id', 'ec_name'));
            $select->order('g.individual DESC');
            $select->order('c.std_fullname ASC');
        }
        
        if($group) {
            $select->join(array('g' => 'batch_exam_schedule'), 'g.bes_id = a.bes_id', array('individual_schedule' => 'MAX(individual)'));
            $select->group('a.student_id');
        }
        else {
            $select->join(array('g' => 'batch_exam_schedule'), 'g.bes_id = a.bes_id', array('individual_schedule' => 'individual'));
            $select->group('a.er_id');
        }
        
        if (isset($params['sql']) && $params['sql'])
        {
            return $select;
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getDataByBesId($bes_id, $group = true, $join = true) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('er_id', 'exam_registrationID' => 'registrationId', 'ber_id', 'bes_id', 'examschedule_id', 'examsetupdetail_id', 'er_registration_type', 'examcenter_id', 'examtaggingslot_id', 'examroom_id', 'result_status', 'grade_name', 'pass', 'er_main_id'))
            ->where('a.bes_id = ?', $bes_id)
            ->where("a.er_registration_type in (0, 1, 4, 9)");
        
        if($join) {
            $select->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array('IdStudentRegistration', 'student_registrationID' => 'registrationId'));
            $select->join(array('c' => 'student_profile'),'c.std_id = b.sp_id');
            $select->joinLeft(array('d' => 'tbl_countries'), 'd.idCountry = c.std_nationality', array('CountryName'));
            $select->joinLeft(array('e' => 'tbl_qualificationmaster'), 'e.IdQualification = c.std_qualification', array('QualificationLevel'));
            $select->join(array('f' => 'exam_schedule'), 'a.examschedule_id = f.es_id', array('es_id', 'es_date'));
            $select->join(array('g' => 'batch_exam_schedule'), 'g.bes_id = a.bes_id', array('individual_schedule' => 'MAX(individual)'));
            $select->join(array('h' => 'exam_scheduletaggingslot'), 'h.sts_id = g.examtaggingslot_id', array('examtaggingslot_id' => 'sts_id'));
            $select->join(array('i' => 'exam_slot'), 'i.sl_id = h.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'));
            $select->order('g.individual DESC');
            $select->order('c.std_fullname ASC');
        }
        
        
        if($group) {
            $select->group('a.student_id');
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getDataForFinance($ber_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'), array('er_id', 'er_main_id', 'ber_id', 'bes_id', 'er_registration_type'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array('IdStudentRegistration'))
            ->join(array('c' => 'student_profile'), 'c.std_id = b.sp_id', array('std_id', 'std_fullname', 'std_idnumber'))
            ->join(array('d' => 'exam_setup_detail'), 'd.esd_id = a.examsetupdetail_id', array('esd_id', 'esd_pe_id'))
            ->where('a.ber_id = ?', $ber_id)
            ->where('a.er_registration_type in (?)', array(0, 1, 4, 9));
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDataBySchedule($es_id, $examroom_id = 0, $join = false, $params = array()) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'), array('er_id', 'registrationId', 'examschedule_id', 'examroom_id', 'er_registration_type', 'result_status', 'grade_name', 'pass', 'attendance_status', 'mc_reason', 'examtaggingslot_id'))
            ->join(array('b' => 'exam_schedule'), 'b.es_id = a.examschedule_id', array())
            ->join(array('c' => 'tbl_studentregistration'), 'c.IdStudentRegistration = a.student_id', array('IdStudentRegistration'))
            ->join(array('d' => 'student_profile'),'d.std_id = c.sp_id', array('std_id', 'std_fullname', 'std_idnumber', 'std_dob', 'std_email'))
            ->joinLeft(array('f' => 'tbl_countries'), 'f.idCountry = d.std_nationality', array('CountryName'))
            ->joinLeft(array('g' => 'tbl_definationms'),'a.attendance_status = g.idDefinition', array('attendance_status_desc' => 'DefinitionDesc'))
            ->where("a.er_registration_type in (0, 1, 4, 9, 10)")
            ->where('a.examschedule_id = ?', $es_id)
            // ->where('a.examroom_id = ?', $examroom_id)
            ->order('d.std_fullname ASC');

        if($examroom_id) {
            $select->where('a.examroom_id = ?', $examroom_id);
        }

        if ($join && !isset($params['report'])) {
            $select->join(array('h' => 'exam_scheduletaggingslot'), 'h.sts_id = examtaggingslot_id AND h.sts_schedule_id = b.es_id', array(
                'exam_scheduletaggingslot_id' => 'sts_id'
            ));
            $select->join(array('i' => 'exam_slot'), 'i.sl_id = h.sts_slot_id', array(
                'exam_slot_id'   => 'sl_id',
                'exam_slot_name' => 'sl_name'
            ));
            $select->join(array('j' => 'exam_taggingslotcenterroom'), 'j.tsc_slotid = i.sl_id', array());
            $select->join(array('k' => 'tbl_exam_center'), 'k.ec_id = j.tsc_examcenterid', array(
                'exam_center_id'   => 'ec_id',
                'exam_center_name' => 'ec_name'
            ));
            $select->joinLeft(array('l' => 'tbl_examroom'), 'l.id = j.tsc_room_id AND l.id = a.examroom_id', array(
                'exam_room_id'   => 'id',
                'exam_room_name' => 'name'
            ));
        }
        elseif ($join && isset($params['report'])) {
            $select->join(array('h' => 'exam_scheduletaggingslot'), 'h.sts_id = a.examtaggingslot_id', array(
                'exam_scheduletaggingslot_id' => 'sts_id'
            ));
            $select->join(array('i' => 'exam_slot'), 'i.sl_id = h.sts_slot_id', array(
                'exam_slot_id'   => 'sl_id',
                'exam_slot_name' => 'sl_name'
            ));
            $select->join(array('j' => 'exam_taggingslotcenterroom'), 'j.tsc_slotid = i.sl_id AND j.tsc_examcenterid = a.examcenter_id', array());
            $select->join(array('k' => 'tbl_exam_center'), 'k.ec_id = a.examcenter_id', array(
                'exam_center_id'   => 'ec_id',
                'exam_center_name' => 'ec_name'
            ));
            $select->joinLeft(array('l' => 'tbl_examroom'), 'l.id = j.tsc_room_id AND l.id = a.examroom_id', array(
                'exam_room_id'   => 'id',
                'exam_room_name' => 'name'
            ));
            $select->group('a.er_id');
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDataByScheduleAndSlot($es_id, $examtaggingslot_id = 0, $examroom_id = 0, $ec_id = 0) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'), array('er_id', 'registrationId', 'examschedule_id', 'examroom_id', 'er_registration_type', 'result_status', 'grade_name', 'pass', 'attendance_status', 'mc_reason'))
            ->join(array('b' => 'exam_schedule'), 'b.es_id = a.examschedule_id', array())
            ->join(array('c' => 'tbl_studentregistration'), 'c.IdStudentRegistration = a.student_id', array('IdStudentRegistration'))
            ->join(array('d' => 'student_profile'),'d.std_id = c.sp_id', array('std_id', 'std_fullname', 'std_idnumber', 'std_dob', 'std_email'))
            ->joinLeft(array('f' => 'tbl_countries'), 'f.idCountry = d.std_nationality', array('CountryName'))
            ->joinLeft(array('g' => 'tbl_definationms'),'a.attendance_status = g.idDefinition', array('attendance_status_desc' => 'DefinitionDesc'))
            ->where("a.er_registration_type in (0, 1, 4, 9, 10)")
            ->where('a.examschedule_id = ?', $es_id)
            ->order('d.std_fullname ASC');

        if($examtaggingslot_id) {
            $select->where('a.examtaggingslot_id = ?', $examtaggingslot_id);
        }

        if($examroom_id) {
            $select->where('a.examroom_id = ?', $examroom_id);
        }
        
        if ($ec_id)
        {
            $select->where('a.examcenter_id = ?', $ec_id);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getPendingForApproval($es_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'), array('count' => 'COUNT(*)'))
            ->join(array('b' => 'exam_schedule'), 'b.es_id = a.examschedule_id', array())
            ->join(array('c' => 'tbl_studentregistration'), 'c.IdStudentRegistration = a.student_id', array())
            ->join(array('d' => 'student_profile'),'d.std_id = c.sp_id', array())
            ->joinLeft(array('f' => 'tbl_countries'), 'f.idCountry = d.std_nationality', array())
            ->where("a.er_registration_type in (0, 1, 4, 9)")
            ->where('a.examschedule_id = ?', $es_id)
            ->where("a.result_status = 1");

        $result = $db->fetchRow($select);

        return $result['count'];
    }

    public function updateData($data, $er_id, $remarks = null) {
        $auth  = Zend_Auth::getInstance();
        $data['modifieddt']  = date('Y-m-d H:i:s');
        $data['modifiedby']  = isset($auth->getIdentity()->iduser)?$auth->getIdentity()->iduser:665;

        $result = $this->update($data, "er_id = $er_id");

        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('er' => $this->_name))
            ->where('er.er_id = ?', $er_id);
        $data = $db->fetchRow($query);
        $data['remarks'] = $remarks;
        $data['payment_status'] = (int) $data['payment_status'];
        $db->insert($this->_history, $data);

        return $result;
    }

    public function isRegisteredbyBerId($IdStudentRegistration, $ber_id) {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => $this->_name), array('count' => 'COUNT(*)'))
            ->where('a.student_id = ?', $IdStudentRegistration)
            ->where('a.ber_id = ?', $ber_id)
            ->where("a.er_registration_type in (0, 1, 4, 9)")
            ->order('a.createddt DESC');

        $result = $db->fetchRow($query);

        if(!$result['count']) {
            return 0;
        }

        return 1;
    }

     public function getResult($IdStudentRegistration, $ber_id, $examschedule_id) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'), array('er_id', 'IdStudentRegistration' => 'student_id', 'result_status', 'grade_name', 'pass'))
            ->where('a.student_id = ?', $IdStudentRegistration)
            ->where('a.ber_id = ?', $ber_id)
            ->where('a.examschedule_id = ?', $examschedule_id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getCount($examschedule_id, $ber_id = 0, $examcenter_id = 0, $examtaggingslot_id = 0) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'), array('count' => 'COUNT(*)'))
            ->where("a.er_registration_type in (0, 1, 4, 9)")
            ->where('a.examschedule_id = ?', $examschedule_id);

        if($ber_id) {
            $select->where('a.ber_id = ?', $ber_id);
        }

        if($examcenter_id) {
            $select->where('a.examcenter_id = ?', $examcenter_id);
        }

        if($examtaggingslot_id) {
            $select->where('a.examtaggingslot_id = ?', $examtaggingslot_id);
        }

        $result = $db->fetchRow($select);
        return $result['count'];
    }

    public function getPaginationQuery($params = array()) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array())
            ->join(array('c' => 'student_profile'), 'c.std_id = b.sp_id', array('std_fullname', 'std_idnumber'))
            ->join(array('d' => 'exam_schedule'), 'd.es_id = a.examschedule_id', array('examschedule_id' => 'es_id', 'es_id', 'es_date'))
            ->join(array('f' => 'exam_setup_detail'), 'f.esd_id = a.examsetupdetail_id', array())
            ->join(array('g' => 'exam_setup'), 'f.es_id = g.es_id', array('es_name', 'es_idProgram'))
            ->join(array('h' => 'tbl_program'), 'h.IdProgram = g.es_idProgram', array('ProgramName', 'ProgramCode'))
            ->join(array('i' => 'programme_exam'), 'f.esd_pe_id = i.pe_id', array('pe_id', 'pe_name'))

            ->join(array('j' => 'exam_scheduletaggingslot'), 'j.sts_schedule_id = d.es_id AND j.sts_id = a.examtaggingslot_id', array('schedule_tagging_slot_id' => 'sts_id'))
            ->join(array('k' => 'exam_slot'), 'k.sl_id = j.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->join(array('l' => 'exam_taggingslotcenterroom'), 'l.tsc_slotid = k.sl_id', array('tagging_slot_center_room_id' => 'tsc_id'))
            ->join(array('m' => 'tbl_exam_center'), 'm.ec_id =  l.tsc_examcenterid AND m.ec_id = a.examcenter_id', array('exam_center_id' => 'ec_id', 'ec_name'))
            ->joinLeft(array('n' => 'tbl_examroom'), 'n.id = l.tsc_room_id', array('examroom_name' => 'name'))

            ->join(array('v' => 'tbl_landscape'), 'v.IdLandscape = a.landscape_id', array())
            ->join(array('w' => 'tbl_program_scheme'), 'w.IdProgramScheme = v.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('x' => 'tbl_definationms'), 'x.idDefinition = w.mode_of_program', array('mop' => 'DefinitionDesc'))
            ->joinLeft(array('y' => 'tbl_definationms'),'a.attendance_status = y.idDefinition', array('attendance_status_desc' => 'DefinitionDesc'))
            ->order('c.std_fullname ASC')
            ->order('d.es_date ASC')
            ->order('k.sl_starttime ASC')
            ->group('a.er_id');

        if(isset($params['std_fullname']) && $params['std_fullname']) {
            $select->where("c.std_fullname LIKE '%" . $params['std_fullname'] . "%'");
        }

        if(isset($params['IdProgram']) && $params['IdProgram']) {
            $select->where('h.IdProgram = ?', $params['IdProgram']);
        }

        if(isset($params['registrationId']) && $params['registrationId']) {
            $select->where("a.registrationId LIKE '%" . $params['registrationId'] . "%'");
        }

        if(isset($params['std_idnumber']) && $params['std_idnumber']) {
            $select->where("c.std_idnumber LIKE '%" . $params['std_idnumber'] . "%'");
        }

        if(isset($params['migration']) && $params['migration']) {
            $select->where("a.migrate_date IS NOT NULL");
        }

        if(isset($params['cancellation']) && $params['cancellation']) {
            $select->where("a.cancel_apply_date IS NOT NULL");
            $select->where("a.cancel_approve_status = 0");        }

        if(isset($params['ec_id']) && $params['ec_id']) {
            $select->where("m.ec_id = ?",$params['ec_id']);
        }
    
        if (isset($params['date_from']) && $params['date_from']) {
            $date_from = date('Y-m-d', strtotime($params['date_from']));
            
            if (Cms_Common::validateDate($date_from))
            {
                $select->where('d.es_date >= ?', $date_from);
            }
        }
    
        if(isset($params['date_to']) && $params['date_to']) {
            $date_to = date('Y-m-d', strtotime($params['date_to']));
        
            if (Cms_Common::validateDate($date_to))
            {
                $select->where('d.es_date <= ?', $date_to);
            }
        }

        return $select;
    }

    public function getDataByOrderId($order_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.order_id = ?', $order_id);
        return $db->fetchAll($select);
    }

    public function dropStudent($ber_id, $IdStudentRegistration) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('er_id'))
            ->where('a.ber_id = ?', $ber_id)
            ->where('a.student_id = ?', $IdStudentRegistration)
            ->where("a.er_registration_type in (0, 1, 4, 9)");

        $er = $db->fetchAll($select);
        
        foreach($er as $row) {
            $row['er_registration_type'] = 2;
            $this->updateData($row, $row['er_id'], 'Drop Student');
        }
    }

    public function getDataByParams($params = array(), $fetchAll = true) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where("a.er_registration_type in (0, 1, 4, 9)");

        if(isset($params['ber_id']) && $params['ber_id']) {
            $select->where('a.ber_id = ?', $params['ber_id']);
        }
    
        if(isset($params['bes_id']) && $params['bes_id']) {
            $select->where('a.bes_id = ?', $params['bes_id']);
        }

        if(isset($params['esd_id']) && $params['esd_id']) {
            $select->where('a.examsetupdetail_id = ?', $params['esd_id']);
        }

        if(isset($params['not-bes_id']) && $params['not-bes_id']) {
            $select->where('a.bes_id != ?', $params['not-bes_id']);
        }

        if(isset($params['er_main_id']) && $params['er_main_id']) {
            $select->where('a.er_main_id = ?', $params['er_main_id']);
        }

        if($fetchAll) {
            return $db->fetchAll($select);
        }

        return $db->fetchRow($select);
    }

    public function getDataByErMainId($er_main_id, $er_id = 0, $join = true) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->where('a.er_main_id = ?', $er_main_id)
            ->group('a.er_id');
        
        if ($join) {
            $select->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array())
            ->join(array('c' => 'student_profile'), 'c.std_id = b.sp_id', array('std_id', 'std_fullname', 'std_idnumber', 'std_email', 'at_repository'))
            ->join(array('d' => 'exam_schedule'), 'd.es_id = a.examschedule_id', array(
                'examschedule_id'      => 'es_id',
                'es_date',
                'result_publish_date'  => "CONCAT_WS(' ', es_publish_start_date, es_publish_start_time)",
                'allow_publish_result' => "DATEDIFF(CURDATE(), es_publish_start_date)"
            ))
            ->join(array('f' => 'exam_setup_detail'), 'f.esd_id = a.examsetupdetail_id', array('esd_id'))
            ->join(array('g' => 'exam_setup'), 'f.es_id = g.es_id', array('examsetup_id' => 'es_id', 'es_name', 'es_idProgram'))
            ->join(array('h' => 'tbl_program'), 'h.IdProgram = g.es_idProgram', array('ProgramName', 'ProgramCode', 'esmtp_id'))
            ->join(array('i' => 'programme_exam'), 'f.esd_pe_id = i.pe_id', array('pe_id', 'pe_name'))
            //->join(array('j' => 'exam_scheduletaggingslot'), 'j.sts_schedule_id = d.es_id', array('schedule_tagging_slot_id' => 'sts_id'))
            ->join(array('j' => 'exam_scheduletaggingslot'), 'j.sts_id = a.examtaggingslot_id', array('schedule_tagging_slot_id' => 'sts_id'))
            ->join(array('k' => 'exam_slot'), 'k.sl_id = j.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->joinLeft(array('l' => 'exam_taggingslotcenterroom'), 'l.tsc_slotid = k.sl_id AND l.tsc_examcenterid = a.examcenter_id', array('tagging_slot_center_room_id' => 'tsc_id'))
            ->join(array('m' => 'tbl_exam_center'), 'm.ec_id = a.examcenter_id', array('exam_center_id' => 'ec_id', 'ec_name'))
            ->joinLeft(array('n' => 'tbl_examroom'), 'n.id = a.examroom_id', array('examroom_name' => 'name'))
            ->joinLeft(array('t' => 'tbl_subjectmaster'), 'f.esd_idSubject = t.IdSubject', array('IdSubject', 'SubjectName', 'SubCode'))
            ->join(array('v' => 'tbl_landscape'), 'v.IdLandscape = a.landscape_id', array())
            ->join(array('w' => 'tbl_program_scheme'), 'w.IdProgramScheme = v.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('x' => 'tbl_definationms'), 'x.idDefinition = w.mode_of_program', array('mop' => 'DefinitionDesc'))
            ->joinLeft(array('y' => 'tbl_definationms'), 'a.attendance_status = y.idDefinition', array('attendance_status_desc' => 'DefinitionDesc'))
            ->joinLeft(array('u' => 'tbl_address'), 'u.add_org_id = m.ec_id', array('add_address1', 'add_address2', 'add_zipcode', 'add_phone'))
            ->joinLeft(array('def' => 'tbl_definationms'), 'u.add_address_type=def.idDefinition', array('addressType' => 'DefinitionDesc'))
            ->joinLeft(array('coun' => 'tbl_countries'), 'u.add_country=coun.idCountry', array('CountryName'))
            ->joinLeft(array('st' => 'tbl_state'), 'u.add_state=st.idState', array('StateName'))
            ->joinLeft(array('ct' => 'tbl_city'), 'u.add_city=ct.idCity', array('CityName'))
            ->where('u.add_org_name = ?', 'tbl_exam_center')
            ->order('c.std_fullname ASC')
            ->order('d.es_date ASC')
            ->order('k.sl_starttime ASC');
        }
        
        if ($er_id && $er_id > 0)
        {
            $select->where('a.er_id = ?', $er_id);
        }

        return $db->fetchAll($select);
    }
    
    
    public function getProfile($er_id){
    	
    	$db      = Zend_Db_Table::getDefaultAdapter();
        $select  = $db->select()
        			  ->from(array('sp' => 'student_profile'))
        			  ->join(array('sr' => 'tbl_studentregistration'),'sr.sp_id=sp.std_id',array('registrationId'))
        			  ->join(array('er' => 'exam_registration'),'er.student_id=sr.IdStudentRegistration',array('er_id','pass','grade_name','examcenter_id'))
        			  ->join(array('p'  => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('program_name'=>'ProgramName','esmtp_id'))
        			  ->join(array('es' => 'exam_schedule'),'es.es_id=er.examschedule_id',array('es_date','es_id'))
		              ->join(array('esd'=>'exam_setup_detail'),'esd.esd_id=er.examsetupdetail_id AND esd.esd_id=es.es_esd_id',array())
		              ->join(array('pe'=>'programme_exam'),'pe.pe_id=esd.esd_pe_id',array('exam_name'=>'pe_name'))
		              ->join(array('sts'=>'exam_scheduletaggingslot'),'sts.sts_id=er.examtaggingslot_id',array(''))
		              ->join(array('sl'=>'exam_slot'),'sl.sl_id=sts.sts_slot_id',array('sl_name','sl_starttime','sl_endtime'))
        			  ->where('er.er_id = ?', $er_id);
        return $profile = $db->fetchRow($select);
    }


    public function getDataByStudentIdMembership($student_id, $join = true) {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('er'=>'membership_registration'))
            ->where('er.mr_student_id = ?', $student_id);
            //->where("er.er_registration_type in (0, 1, 4, 9)")
            //->group("er.examsetupdetail_id");

        $result = $db->fetchAll($query);

        $exam_registration = array();
        $exam_registration = $result;

        return $exam_registration;
    }


}
?>