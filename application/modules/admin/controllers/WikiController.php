<?php
set_time_limit(0);
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);

class Admin_WikiController extends  Zend_Controller_Action
{
	protected $coursesDb;
	protected $contentBlockDb;
	protected $coursecatDb;
	protected $uploadDir;
	protected $uploadFolder;

	public function init()
	{
		
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'wiki');

		$this->coursesDb = new App_Model_Courses();
		$this->contentBlockDb = new App_Model_ContentBlock();
		$this->blockDataDb = new App_Model_ContentBlockData();
		$this->coursecatDb = new App_Model_CoursesCategory();
		$this->currDb = new App_Model_Curriculum();
		$this->wikiDb = new App_Model_Wiki(); 

		$this->uploadFolder = 'course-assets';
		$this->uploadDir = DOCUMENT_PATH. '/'.$this->uploadFolder;

		//events
		//Cms_Filters::add('block-desc-save', array($this, 'Test'));
	}


	public function indexAction()
	{
		$this->view->title = 'Wiki List';

		$getcourses = $this->wikiDb->getWikis();

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();
			$formData['search'] = trim($formData['search']);

			if ( $formData['search'] != '')
			{
				$getcourses->where('a.title LIKE ?', '%'.$formData['search'].'%');
			}

			$this->view->formdata = $formData;
		}

		$courses = Cms_Paginator::query($getcourses);


		$this->view->results = $courses;
	}

	public function newAction()
	{
		$this->view->title = 'New Wiki';

		$form = new Admin_Form_Wiki();

		//form required
		$form->title->setRequired(true);
	
		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				

					$data = array(
						'title'      	=> $formData['title'],
						'course_id'     => $formData['course_id'],
						'description'	=> $formData['description'],						
						'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'created_by'    => $this->auth->getIdentity()->id,
					);

					$wiki_id = $this->wikiDb->insert($data);

					// add groups
					$groupDb = new App_Model_UserGroupMapping();
					$db = $groupDb->getDefaultAdapter();
					
					foreach ($formData['group_id'] as $group) {
						$data = array(
									'map_type'	=> 'wiki',
									'group_id'	=> $group,
									'course_id' => $formData['course_id'],
									'mapped_id'	=> $wiki_id
							);

						$groupDb->addMappingData($data);
					}

					Cms_Common::notify('success','Wiki successfully created');
					$this->redirect('/admin/wiki/');
					
			}
		}


		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->getParam('id');

		$course = $this->wikiDb->fetchRow(array("id = ?" => $id))->toArray();

		if ( empty($course) )
		{
			throw new Exception('Wiki doesnt exist');
		}

		$this->view->title = 'Edit Wiki';

		$this->view->course = $course;

		//form
		$form = new Admin_Form_Wiki();

		//populate
		$form->populate($course);

		//form required
		$form->title->setRequired(true);

		$groupDb = new App_Model_UserGroupMapping();
		$db = $groupDb->getDefaultAdapter();

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				
					$data = array(
						'title'      	=> $formData['title'],
						'course_id'     => $formData['course_id'],
						'description'	=> $formData['description'],
						'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'visibility'	=> $formData['visibility'],
						'modified_by'   => $this->auth->getIdentity()->id,
					);
					
					$this->wikiDb->update($data, array('id = ?'=> $id));

					// for groups
					$groupDb->deleteMappingByDataId($id, $formData['course_id'], 'wiki');

					foreach ($formData['group_id'] as $group) {
						$data = array(
									'map_type'	=> 'wiki',
									'group_id'	=> $group,
									'course_id' => $formData['course_id'],
									'mapped_id'	=> $id
							);

						$groupDb->addMappingData($data);
					}

					//redirect
					Cms_Common::notify('success','Wiki successfully updated');
					$this->redirect('/admin/wiki/edit/id/'.$id);
			
		}

		// get this content grouping
		$groupdata = $db->select()
							->from(array('a' => 'user_group'))
							->joinLeft(array(
									'c' => 'user_group_mapping'), 
									'(a.group_id = c.group_id AND c.map_type = "wiki" AND c.mapped_id = "'.$id.'")', 
									array('c.map_id'))
							->where('a.course_id = ?', $course['course_id'])
							->group('a.group_id')
							->order('a.group_name');

		$groups = $db->fetchAll($groupdata);

		$this->view->groups = $groups;
		$this->view->wiki_id = $id;
		$this->view->form = $form;
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$wiki = $this->wikiDb->fetchRow(array("id = ?" => $id))->toArray();

		if (empty($wiki)) {
			throw new Exception('Wiki doesnt exist');
		}

		//delete
		$this->wikiDb->delete(array('id = ?' => $id));

		// for groups
		$groupDb = new App_Model_UserGroupMapping();
		$db = $groupDb->getDefaultAdapter();
		
		$groupDb->deleteMappingByDataId($id, $wiki['course_id'], 'wiki');

		Cms_Common::notify('success','Wiki successfully deleted');
		$this->redirect('/admin/wiki/');
	}

	public function getWikiGroupAction() 
	{
		if ($this->getRequest()->isPost()) 
		{
			$formData = $this->getRequest()->getPost();

			// get groups for this course
			$groupDb = new App_Model_UserGroup();
			$db = $groupDb->getDefaultAdapter();

			$groupdata = $db->select()
								->from(array('a' => 'user_group'))
								->joinLeft(array(
									'c' => 'user_group_mapping'), 
									'(a.group_id = c.group_id AND c.map_type = "wiki" AND c.mapped_id = "' . @$formData['wiki_id'] . '")', 
									array('c.map_id'))
								->where('a.course_id = ?', $formData['course_id'])
								->group('a.group_id')
								->order('a.group_name');

			$groups = $db->fetchAll($groupdata);

			die(json_encode($groups));
		}
	}
	
}

