<?php
class Admin_BulkAdmissionUpdateController extends Zend_Controller_Action
{
	protected $itemLog = array();

	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'calendar');

		$this->eventsDb = new App_Model_CalendarEvent(); 
		$this->locale = Zend_Registry::get('Zend_Locale');
		$this->bulkDB = new Admin_Model_DbTable_Bulk();
		$this->uploadDir = UPLOAD_PATH.'/bulk';

		require_once APPLICATION_PATH.'/../library/Others/parsecsv.lib.php';
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
		$common = new Cms_Common();
		         $this->definationDB =new Admin_Model_DbTable_Definition();

	}

	public function indexAction()
	{
	
		$this->view->title = "Bulk Admission";
		
		$p_data = $this->bulkDB->getPaginateData();

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
    	
	}

	public function uploadAction()
	{
		/*	
			problem/missing
			- Designation
			- program_scheme
			- Medium of Instruction
			- Proficiency in English -- kena filter by code
		*/
					$common = new Cms_Common();

		$this->view->title = "Bulk Admission - Upload";
		
		if ($this->_request->isPost() && $this->_request->getPost('save')) 
		{
			$larrformData = $this->_request->getPost();

			
			
			$total = 0;

			try 
			 {
				if ( !is_dir( $this->uploadDir ) )
				{
					if ( $common->mkdir_p($this->uploadDir) )
					{
						throw new Exception('Cannot create document folder ('.$this->uploadDir.')');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
				
			
				$files = $adapter->getFileInfo();
				
				$adapter->addValidator('NotExists', false, $this->uploadDir );
				$adapter->setDestination( $this->uploadDir );
				$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
				$adapter->addValidator('Size', false, array('min' => 100 , 'max' => 4194304 , 'bytestring' => true));

				foreach ($files as $no => $fileinfo) 
				{
					$adapter->addValidator('Extension', false, array('extension' => 'csv', 'case' => false));
				
					if ($adapter->isUploaded($no))
					{
						$ext =  $common->getext($fileinfo['name']);
						$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
						
						$fileUrl = $this->uploadDir.'/'.$fileName;

						$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
						$adapter->setOptions(array('useByteString' => false));
						
						 $size = $adapter->getFileSize($no);
					
						//die();
						if( !$adapter->receive($no))
						{
							$errmessage = array();
							if ( is_array($adapter->getMessages() ))
							{	
								foreach(  $adapter->getMessages() as $errtype => $errmsg )
								{
									$errmessage[] = $errmsg;
								}
								
								throw new Exception(implode('<br />',$errmessage));
							}
						}

						
						//process - CSV
						$csv = new parseCSV();

						$csv->auto($fileUrl);

						// echo "<pre>";
						// print_r($fileinfo);
						// die();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            						
					
						$totalfields = count($csv->data[0]);
						
						// echo $totalfields;
						// die();
						
						// if ($totalfields != 41 && $totalfields != 3 && $totalfields != 8)
						// {
						// 	$this->view->result =1;
						
						// 	//throw new Zend_Exception('Invalid CSV format');
						// }


						//save into db
						if ( !isset($this->bulk_id) )
						{
							$auth = Zend_Auth::getInstance(); 
							$data = array(
											'b_filename'		=> $fileinfo['name'],
											'b_fileurl'			=> '/bulk/'.$fileName,
											'b_status'			=> 'NEW',
											'b_totalapp'		=> count($csv->data),
											'b_created_by'		=> $auth->getIdentity()->id,
											'b_created_date'	=> date("Y-m-d H:i:s")
										);
							// echo "<pre>";
							// print_r($data);
							// die();
							
							$this->bulk_id = $this->bulkDB->addData($data);
						}

						

					} //isuploaded
					
				} //foreach
				
				//$this->bulkDB->updateData(array('b_totalapp' => $total), $this->bulk_id);
                
				$this->_redirect('/admin/bulk-admission-update/review/id/'.$this->bulk_id);	

			}
			catch (Exception $e) 
			{
				throw new Exception( $e->getMessage() );
			}

			

		} // if 
	}


	
	
	protected function getItemID($row)
	{
		
			return $row['Name'];
		
	}

	public function reviewAction()
	{
		

		$this->view->title = "Bulk Admission - Review";

		$id = $this->_getParam('id',null);

		

		$bulkinfo = $this->bulkDB->getData($id);
		
		if ( empty($bulkinfo) )
		{
			throw new Exception('Invalid Bulk ID');
		}
		
		$this->bulk_id = $bulkinfo['b_id'];
		$importedItems = $this->bulkDB->getItems($id);

		
		$importedBID = array();
		foreach ( $importedItems as $iitem ) 
		{
			$importedBID[] = $iitem['bi_bid'];
		}

		//parse CSV
		$csv = new parseCSV();
		$csv->auto(UPLOAD_PATH.$bulkinfo['b_fileurl']);
		
		//start
		$items = array();
		// echo "<pre>";
		// print_r($csv->data);
		// die();
		$fileds = count($csv->data[0]);
		
		if($fileds>0)
		{ 

 			foreach ( $csv->data as $row_id => $row )
		 {	$bid = $this->getItemID($row);
			
			$items[$bid] = array('BID' => $bid) + $row;

			//$this->processItem(array('BID' => $bid) + $row);
		 }
		}
		
		//post
		if($fileds>0)
		{
		//  if ($this->_request->isPost() && $this->_request->getPost('save')) 
		// {
			$formData = $this->_request->getPost();
			$count = $formData['item'];

			if ( $count > 0 )
			{
				$total = 0;
				foreach ( $formData['item'] as $BID )
				{
					$this->processItem($items[$BID], 1);
					$total++;
				}
				
				$this->bulkDB->updateData(array('b_status' => 'IMPORTED', 'b_totaladded' => new Zend_Db_Expr('b_totaladded+'.$total)), $this->bulk_id);

				$this->_helper->flashMessenger->addMessage(array('success' => $total." Applicant imported"));
				$this->_redirect('/admin/bulk-admission-update/review/id/'.$this->bulk_id);
			}
			else
			{
				//error
			}
		// }
		
}
		
		//views
		$this->view->importedBID = $importedBID;
		$this->view->items = $items;
		$this->view->itemLog = $this->itemLog;
		
	}


	protected function logItem($bid, $field='', $msg='' )
	{
		$this->itemLog[$bid][] = array('field'	=> $field,
										'msg'	=> $msg);
	}

	protected function processItem($row, $insert=0)
	{
			// echo $row['DiscountAmount'];
			// die();



			

		//Models
		$userDb = new App_Model_User();
		$profileId  = $userDb->get_by_name($row['Name']);

		$profileId['id'] = $profileId[0]['id'];
					

			
		 $objdeftypeDB = new App_Model_Definitiontype();

		
           

 $member_status = $this->definationDB->getByCode('Status');
 if($profileId['id'])
 {
// if(!empty($row['PROCESSING FEE STATUS']))
// 		{
// 				$formData['payment_status'] = 1;
// 				$appl_info['approve_status'] = 1;

// 			$appl_info['approve_status1']	= 1;
// 			}else{
// 				$appl_info['approve_status'] = 0;
// 			$appl_info['approve_status1']	= 0;

// 		}

		


			// $appl_info['total_amount'] = $row['PROCESSING FEE'];
			// $appl_info['payment_status'] = $formData['payment_status'];
			
			

		
			// $userid = $userDb->update($appl_info, array('id = ?' => $profileId['id']));
			//  $invoice = new App_Model_PerformaInvoice();
                  
   //          $data_invioce = array('user_id' => $profileId['id'],
   //                       'description' => 'Membership Payment',
   //                          'fee_type' => 1,
   //                            'amount' => $row['PROCESSING FEE'],
   //                  'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
   //                  'created_user' => $profileId['id'],
   //                  'created_date' => date("Y-m-d"),
     
          
   //                       );
   //                  $invoice_no = $invoice->insert($data_invioce);
                    

   //           $invoice_data= new App_Model_PerformaInvoiceDetails();
               
                 
   //          $data_invioce_detail = array('user_id' => $profileId['id'],
   //                                  'performa_id' =>  $invoice_no,
   //                                  'id_program' =>0,
   //                                 'description' => 'Membership Payment',
   //                                 'amount' => $row['PROCESSING FEE'],
   //                                 'invoice_no' => 'CIIF'.  date('YHims').$id,
   //                                 'created_user' => $profileId['id'],
   //                                 'created_date' => date("Y-m-d"),
   //                       );
   //                  $invoice_no = $invoice_data->insert($data_invioce_detail);
   //        $invoice = new App_Model_InvoiceMain();
               
                
   //          $data_invioce = array('user_id' => $profileId['id'],
   //                                 'description' => 'Membership Payment',
   //                          'fee_type' => 1,
   //                            'amount' => $row['PROCESSING FEE'],
   //                  'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
   //                  'created_user' => $profileId['id'],
   //                  'created_date' => date("Y-m-d"),
   //                  'payment_status'=>1
   //                       );
   //                  $invoice_no = $invoice->insert($data_invioce);
                    
            
               
   //            $invoice_data = new App_Model_InvoiceMainDetails();   
   //          $data_invioce_details = array('user_id' => $profileId['id'],
   //               'description' => 'Membership Payment',
   //                'amount' => $row['PROCESSING FEE'],
   //                'invoice_id' => $invoice_no,
   //                'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
   //                'created_user' => $profileId['id'],
   //                'created_date' => date("Y-m-d"),
   //                'payment_status'=>1
   //                       );
   //                  $invoice_no = $invoice_data->insert($data_invioce_details);

			
		
 

//         	 if($profileId['id'] && $row['PAYMENT 2016'] =="PAID")
//          {


//           $invoice = new App_Model_PerformaInvoice();
               
                 
//             $data_invioce = array('user_id' => $profileId['id'],
//                                    'description' => 'Renewal Payment',
//                             'fee_type' => 2,
//                               'amount' => $row['INVOICE 2016'],
//                     'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
//                     'created_user' => $profileId['id'],
//                     'created_date' => date("Y-m-d"),
     
          
//                          );
//                     $invoice_no = $invoice->insert($data_invioce);
                    

//              $invoice_data= new App_Model_PerformaInvoiceDetails();
               
                 
//             $data_invioce_detail = array('user_id' => $profileId['id'],
//                                     'performa_id' =>  $invoice_no,
//                                     'id_program' =>0,
//                                    'description' => 'Renewal Payment',
//                                    'amount' => $row['INVOICE 2016'],
//                                    'invoice_no' => 'CIIF'.  date('YHims').$id,
//                                    'created_user' => $profileId['id'],
//                                    'created_date' => date("Y-m-d"),
//                          );
//                     $invoice_no = $invoice_data->insert($data_invioce_detail);
//           $invoice = new App_Model_InvoiceMain();
               
                
//             $data_invioce = array('user_id' => $profileId['id'],
//                                    'description' => 'Renewal Payment',
//                             'fee_type' => 2,
//                               'amount' => $row['INVOICE 2016'],
//                     'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
//                     'created_user' => $profileId['id'],
//                     'created_date' => date("Y-m-d"),
//                     'payment_status'=>1
//                          );
//                     $invoice_no = $invoice->insert($data_invioce);
                    
            
               
//               $invoice_data = new App_Model_InvoiceMainDetails();   
//             $data_invioce_details = array('user_id' => $profileId['id'],
//                  'description' => 'Renewal Payment',
//                   'amount' => $row['INVOICE 2016'],
//                   'invoice_id' => $invoice_no,
//                   'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
//                   'created_user' => $profileId['id'],
//                   'created_date' => date("Y-m-d"),
//                   'payment_status'=>1
//                          );
//                     $invoice_no = $invoice_data->insert($data_invioce_details);

                  
 
            
//             $renewData['sp_id'] = $profileId['id'];
//             $renewData['ma_type'] = 2; //renew
//             $renewData['ma_createddt'] = date('Y-m-d H:i:s');
//             $renewData['ma_createdby'] = $profileId['id'];
//             $renewData['ma_submit_date'] = date('Y-m-d H:i:s');
//         $renewData['ma_status'] = $member_status[14]['idDefinition'];
//             $renewData['ma_payment_status'] = 1;
//             $renewData['mf_id'] = 1;
//             $renewData['ma_renewal_amount'] = $row['INVOICE 2016'];
//             $renewData['membershipId'] = $profileId['id'];
//             $renewData['renewal_year'] = 2016 ;
//             $renewData['ma_application_status'] = 1;
//             $renewData['ma_expiry_date'] = date('2016-12-31 H:i:s');
//             $renewData['ma_activation_date'] = date('2016-1-1 H:i:s');
        
//     $renewData['prev_status'] = 596;


               

//  $Members = new App_Model_Membership_MemberRegistration();
// $member_reg = $Members->getListMember($profileId['id']);


// 	$renewData['prev_reg_date'] = $member_reg[0]['mr_activation_date'];
//             $renewData['prev_member_id'] = $profileId[0]['id_member'];
//         $renewData['prev_member_sub_id'] = $profileId[0]['id_member_subtype'];
// $renewData['prev_expiry_date'] = $member_reg[0]['mr_expiry_date'];
//     $renewData['prev_status'] = 596;
//             $MembershipAppDB = new App_Model_Membership_MembershipApplication();
//             $MembershipAppDB->addData($renewData);
             
//             $renew['mr_activation_date'] = date('2016-1-1 H:i:s');
//             $renew['mr_expiry_date'] = date('2016-12-31 H:i:s');
//             $renew['mr_status'] = 1641;
            
//         $Members->update($renew,array('mr_id = ?' => $member_reg[0]['mr_id']));
//  }
//         	 if($profileId['id'] && $row['PAYMENT 2017'] == "PAID")
//          {

//        $invoice = new App_Model_PerformaInvoice();
               
                 
//             $data_invioce = array('user_id' => $profileId['id'],
//                                    'description' => 'Renewal Payment',
//                             'fee_type' => 2,
//                               'amount' => $row['INVOICE 2017'],
//                     'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
//                     'created_user' => $profileId['id'],
//                     'created_date' => date("Y-m-d"),
              
                       
//                          );
//                     $invoice_no = $invoice->insert($data_invioce);
                    

//              $invoice_data= new App_Model_PerformaInvoiceDetails();
               
                 
//             $data_invioce_detail = array('user_id' => $profileId['id'],
//                                     'performa_id' =>  $invoice_no,
//                                     'id_program' =>0,
//                                    'description' => 'Renewal Payment',
//                                  'amount' => $row['INVOICE 2017'],
//                                    'invoice_no' => 'CIIF'.  date('YHims').$id,
//                                    'created_user' => $profileId['id'],
//                                    'created_date' => date("Y-m-d"),
//                          );
//                     $invoice_no = $invoice_data->insert($data_invioce_detail);

//           $invoice = new App_Model_InvoiceMain();
               
                
//             $data_invioce = array('user_id' => $profileId['id'],
//                                    'description' => 'Renewal Payment',
//                            'fee_type' => 2,
//                               'amount' => $row['INVOICE 2017'],
//                     'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
//                     'created_user' => $profileId['id'],
//                     'created_date' => date("Y-m-d"),
//                     'payment_status'=>1
//                          );
//                     $invoice_no = $invoice->insert($data_invioce);

//    $invoice_data = new App_Model_InvoiceMainDetails();

//                     $data_invioce_details = array('user_id' => $profileId['id'],
//                                    'description' => 'Renewal Payment',
//                               'amount' => $row['INVOICE 2017'],
//                       'invoice_id' => $invoice_no,
//                     'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
//                     'created_user' => $profileId['id'],
//                     'created_date' => date("Y-m-d"),
//                     'payment_status'=>1
//                          );
//                     $invoice_no = $invoice_data->insert($data_invioce_details);
                    
 
            
//             $renewData['sp_id'] = $profileId['id'];
//             $renewData['ma_type'] = 2; //renew
//             $renewData['ma_createddt'] = date('Y-m-d H:i:s');
//             $renewData['ma_createdby'] = $profileId['id'];
//             $renewData['ma_submit_date'] = date('Y-m-d H:i:s');
//         $renewData['ma_status'] = $member_status[14]['idDefinition'];
//             $renewData['ma_payment_status'] = 1;
//             $renewData['mf_id'] = 1;
//             $renewData['ma_renewal_amount'] = $row['INVOICE 2017'];
//             $renewData['membershipId'] = $profileId['id'];
//             $renewData['renewal_year'] = 2018 ;
//             $renewData['ma_application_status'] = 1;
//             $renewData['ma_expiry_date'] = date('2017-12-31 H:i:s');
//             $renewData['ma_activation_date'] = date('2017-1-1 H:i:s');
        
//     		$renewData['prev_status'] = 596;
               

//  $Members = new App_Model_Membership_MemberRegistration();
// $member_reg = $Members->getListMember($profileId['id']);
		
// 	$renewData['prev_reg_date'] = $member_reg[0]['mr_activation_date'];
//             $renewData['prev_member_id'] = $profileId[0]['id_member'];
//         $renewData['prev_member_sub_id'] = $profileId[0]['id_member_subtype'];
// $renewData['prev_expiry_date'] = $member_reg[0]['mr_expiry_date'];
//     $renewData['prev_status'] = 596;

//             $MembershipAppDB = new App_Model_Membership_MembershipApplication();
//             $MembershipAppDB->addData($renewData);
             
//             $renew['mr_activation_date'] = date('2017-1-1 H:i:s');
//             $renew['mr_expiry_date'] = date('2017-12-31 H:i:s');
//             $renew['mr_status'] = 1641;
            
//         $Members->update($renew,array('mr_id = ?' => $member_reg['mr_id']));


// }
        	 if($profileId['id'] && $row['PAYMENT 2018'] =="PAID" )
         {

         	//$invoice = new App_Model_PerformaInvoice();
               
                 
//             $data_invioce = array('user_id' => $profileId['id'],
//                                    'description' => 'Renewal Payment',
//                             'fee_type' => 2,
//                      'amount' => $row['INVOICE 2018'],
//                     'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
//                     'created_user' => $profileId['id'],
//                     'created_date' => date("Y-m-d"),
                    
//                          );
//                     $invoice_no = $invoice->insert($data_invioce);
                    

//              $invoice_data= new App_Model_PerformaInvoiceDetails();
               
                 
//             $data_invioce_detail = array('user_id' => $profileId['id'],
//                                     'performa_id' =>  $invoice_no,
//                                     'id_program' =>0,
//                                    'description' => 'Renewal Payment',
//                                    'amount' => $row['INVOICE 2018'],
//                                    'invoice_no' => 'CIIF'.  date('YHims').$id,
//                                    'created_user' => $profileId['id'],
//                                    'created_date' => date("Y-m-d"),
//                          );
//                     $invoice_no = $invoice_data->insert($data_invioce_detail);

//           $invoice = new App_Model_InvoiceMain();
               
                
//             $data_invioce = array('user_id' => $profileId['id'],
//                  'description' => 'Renewal Payment',
//                  'fee_type' => 2,
//                   'amount' => $row['INVOICE 2018'],
//                   'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
//                  'created_user' => $profileId['id'],
//                   'created_date' => date("Y-m-d"),
//                   'payment_status'=>1
//                          );
//                     $invoice_no = $invoice->insert($data_invioce);
//  $invoice_data = new App_Model_InvoiceMainDetails();
//                     $data_invioce_details = array('user_id' => $profileId['id'],
//                                    'description' => 'Renewal Payment',
//                               'amount' => $row['INVOICE 2018'],
//               'invoice_id' => $invoice_no,
//                     'invoice_no' => 'CIIF'.  date('YHims').$profileId['id'],
//                     'created_user' => $profileId['id'],
//                     'created_date' => date("Y-m-d"),
//                     'payment_status'=>1
//                          );
//                     $invoice_no = $invoice_data->insert($data_invioce_details);
                    
 
            
//             $renewData['sp_id'] = $profileId['id'];
//             $renewData['ma_type'] = 2; //renew
//             $renewData['ma_createddt'] = date('Y-m-d H:i:s');
//             $renewData['ma_createdby'] = $profileId['id'];
//             $renewData['ma_submit_date'] = date('Y-m-d H:i:s');
//         $renewData['ma_status'] = 1641;
//             $renewData['ma_payment_status'] = 1;
//             $renewData['mf_id'] = 1;
//             $renewData['ma_renewal_amount'] = $row['INVOICE 2018'];
//             $renewData['membershipId'] = $profileId['id'];
//             $renewData['renewal_year'] = 2016 ;
//             $renewData['ma_application_status'] = 1;
//             $renewData['ma_expiry_date'] = date('2018-12-31 H:i:s');
//             $renewData['ma_activation_date'] = date('2018-1-1 H:i:s');
        
//     $renewData['prev_status'] = 596;
               

 $Members = new App_Model_Membership_MemberRegistration();
$member_reg = $Members->getListMember($profileId['id']);

// 	$renewData['prev_reg_date'] = $member_reg[0]['mr_activation_date'];
//             $renewData['prev_member_id'] = $profileId[0]['id_member'];
//         $renewData['prev_member_sub_id'] = $profileId[0]['id_member_subtype'];
// $renewData['prev_expiry_date'] = $member_reg[0]['mr_expiry_date'];
//     $renewData['prev_status'] = 596;
//             $MembershipAppDB = new App_Model_Membership_MembershipApplication();
//             $MembershipAppDB->addData($renewData);
             
            $renew['mr_activation_date'] = date('2018-1-1 H:i:s');
            $renew['mr_grace_date'] = date('2018-12-31 H:i:s');
            $renew['mr_expiry_date'] = date('2019-3-1 H:i:s');
            $renew['mr_status'] = 1384;
            
        $Members->update($renew,array('mr_id = ?' => $member_reg['mr_id']));
        
		
			

}
		}
	



}


	function getState($state)
	{
		$db = getDB();
	  	$select = $db->select()
	                 ->from(array('s'=>'tbl_state'))
	                 ->where('LOWER(s.StateName) = ?', strtolower($state));
        $row = $db->fetchRow($select);
		
		if ( !empty($row) )
		{
			return $row['idState'];
		}
		else
		{
			return '';
		}
	}
	

	public function getProgram($name)
	{
		// echo $name;
		// die();
		$db = getDb();

		$sql =  $db->select()
	                 ->from(array('a'=>'tbl_program'),array('a.*'))
	                 ->joinleft(array('b'=>'tbl_program_scheme'),'a.IdProgram=b.IdProgram')
	                 ->where('LOWER(a.ProgramName) = ?', strtolower($name));

		$result = $db->fetchRow($sql);

		if ( empty($result) )
		{
			return '';
		}
		else
		{
			return $result;
		}
	}
	
	public function getIntake($name='')
	{
		$db = getDb();
        $sql = "Select IdIntake from tbl_intake where IntakeId ='$name'";
		// $sql =  $db->select()
	 //     	->from(array('a'=>'tbl_intake'),array('IdIntake'))
	 //         ->where('a.IntakeDesc = ?', $name);

		$result = $db->fetchRow($sql);

		if ( empty($result) )
		{
			return '';
		}
		else
		{
			return $result['IdIntake'];
		}
	}

	public function getIdByDef($id, $data, $map = array('k'=>'DefinitionDesc','v'=>'idDefinition') )

	{
		// echo $id;
		// echo "----";
		// print_r($data);
  		//        echo "\n";

		//die();
		if ( !empty($data) )
		{
			
			foreach ( $data as $row )
			{
				if ( strtolower($row[$map['k']]) == strtolower($id) )
				{
					$val = $row[$map['v']];
				}
			}
		}

		if ( empty($val) )
		{
			return 0;
		}
		else
		{
			return $val;
		}
	}

	function generateApplicantID(){

		return mt_rand(1,9999999999);
	 	// echo 	$applicantID = date("Y-m-d H:i:s").$randomNO;
	 	// return $applicantID;
		
		//generate applicant ID
		$applicantid_format = array();
		
		$seqnoDb = new App_Model_Application_DbTable_ApplicantSeqno();
		$sequence = $seqnoDb->getData(date('Y'));
		
		$tblconfigDB = new App_Model_General_DbTable_TblConfig();
		$config = $tblconfigDB->getConfig(1);
		
		//format
		 $format_config = explode('-',$config['ApplicantIdFormat']);
		
		for($i=0; $i<count($format_config); $i++){
			$format = $format_config[$i];
			if($format=='px'){ //prefix
				$result = $config['ApplicantPrefix'];
			}elseif($format=='yyyy'){
				$result = date('Y');
			}elseif($format=='yy'){
				$result = date('yy');
			}elseif($format=='seqno'){				
				$result = sprintf("%03d", $sequence['seq_no']);
			}
			array_push($applicantid_format,$result);
		}
	
		//var_dump(implode("-",$applicantID));
		$applicantID = implode("-",$applicantid_format);
		
		//update sequence		
		$seqnoDb->updateData(array('seq_no'=>$sequence['seq_no']+1),$sequence['seq_id']);
		
		return	$applicantID;
	}
}
?>