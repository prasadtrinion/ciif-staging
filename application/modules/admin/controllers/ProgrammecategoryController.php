<?php
class Admin_ProgrammecategoryController extends  Zend_Controller_Action
{
	protected $coursecatDb;


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'Programmecategory');

		$this->programDb = new App_Model_ProgramCategory();
		$this->courseDb = new App_Model_Courses();
		$this->definationDB =new Admin_Model_DbTable_Definition();
		


	}

	public function indexAction()
	{

		$form = new Admin_Form_CourseCategory();
		$session  = new Zend_Session_Namespace('AdminCoursecategoryIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();
        $learningLists = $this->definationDB->getByCode('Program Route');
        $programLists = $this->definationDB->getByCode('Program Level');
        $programCategoryLists = $this->programDb->getProgram();


		foreach ($learningLists as $opt )
		{
			$form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
		}

		foreach ($programLists as $opt1 )
		{
			$form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
		}
		$result = '';
		foreach ($programCategoryLists as $opt2 )
		{
			$form->program_category->addMultiOption($opt2['id'], $opt2['name']);
		}
		if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();
            
            if ($formdata)
				{		
					$result = $this->programDb->getProgramSearch($formdata);

					
				}
	    }
	    if($result)
	    {
	    	$this->view->results = $result;
	    }
	    else
	    {
	    	$this->view->results = $programCategoryLists;
	    }

		$this->view->formdata = $formdata;
		$this->view->form = $form;
	}

	public function addAction()
	{
		$form = new Admin_Form_CourseCategory();

		$pid = $this->_getParam('pid');

		$this->view->title = "New Program Category";

		$cats = $this->programDb->fetchAll()->toArray();
		$learningLists = $this->definationDB->getByCode('Program Route');

		foreach ($learningLists as $opt )
		{
			$form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
		}

		$programMode = $this->definationDB->getByCode('Mode of Program');

		foreach ($programMode as $opt5 )
		{
			$form->mode_of_program->addMultiOption($opt5['idDefinition'], $opt5['DefinitionDesc']);
		}

		$programLists = $this->definationDB->getByCode('Program Level');

		foreach ($programLists as $opt1 )
		{
			$form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
		}
		

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();


			
				$fail = 0;

				$data = array(
						'name'      	=> $formData['name'],
						'code'      	=> $formData['code'],
						'description'	=> $formData['description'],
						'start_date'	=> date('Y-m-d', strtotime($formData['start_date'])),
					'end_date'	    =>date('Y-m-d', strtotime($formData['end_date'])),
						'program_route'	=> $formData['program_route'],
						'module_type'	=> $formData['module_type'],
						'program_level'	=> $formData['program_level'],
						'mode_of_program' => $formData['mode_of_program'],
						'name_safe'		=> Cms_Common::safename($formData['name']),
					
						'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'created_by'    => $this->auth->getIdentity()->id,
						'active'		=> 1
					);

				$course_id = $this->programDb->insert($data);

				Cms_Common::notify('success','Programme category successfully created');
				$this->redirect('/admin/programmecategory/');

			
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->_getParam('id');

		$cat = $this->programDb->fetchRow(array("id = ?" => $id))->toArray();

		// echo "<pre>";
		// print_r($cat);
		// die();

		$this->view->program = $cat;

		if (empty($cat)) {
			throw new Exception('Invalid Program Category');
		}

		$form = new Admin_Form_CourseCategory();

		$learningLists = $this->definationDB->getByCode('Program Route');

		foreach ($learningLists as $opt )
		{
			$form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
		}

		$programMode = $this->definationDB->getByCode('Mode of Program');

		foreach ($programMode as $opt5 )
		{
			$form->mode_of_program->addMultiOption($opt5['idDefinition'], $opt5['DefinitionDesc']);
		}


		$programLists = $this->definationDB->getByCode('Program Level');

		foreach ($programLists as $opt1 )
		{
			$form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
		}

		//modified
		if ($cat['updated_by'] != null)
		{
			$userDb = new App_Model_User;
			$user = $userDb->getUser($cat['updated_by']);
			$this->view->modified_user = $user;
		}

		$this->view->title = "Edit Program Category";

		$cats = $this->programDb->fetchAll(array('active = 1'))->toArray();


		// foreach ( $this->programDb->printTree($this->programDb->buildTree($cats,0)) as $opt )
		// {
		// 	$form->parent_id->addMultiOption($opt['id'], $opt['name']);
		// }

		//populate
		$form->populate($cat);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				$data = array(
					'name'      	=> $formData['name'],
					'code'      	=> $formData['code'],
					'description'	=> $formData['description'],
					'start_date'	=> $formData['start_date'],
					'end_date'	    => $formData['end_date'],
					'program_route'	=> $formData['program_route'],
					'program_level'	=> $formData['program_level'],
					'module_type'	=> $formData['module_type'],
					'mode_of_program' => $formData['mode_of_program'],
					'name_safe'		=> Cms_Common::safename($formData['name']),
					'updated_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'updated_by'   => $this->auth->getIdentity()->id,
					'active'		=> $formData['active']
				);

				$this->programDb->update($data, array('id = ?' => $id));

				Cms_Common::notify('success','Programme category successfully edited');
				$this->redirect('/admin/programmecategory/edit/id/'.$id);

			
		}

		$this->view->form = $form;
		$this->view->cat = $cat;
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$cat = $this->coursecatDb->fetchRow(array("id = ?" => $id))->toArray();

		if (empty($cat)) {
			throw new Exception('Invalid Course Category');
		}

		//move all existing categories
		$this->courseDb->update(array('category_id' => 0), array('category_id = ?' => $id) );


		//delete
		$this->coursecatDb->delete(array('id = ?' => $id));

		Cms_Common::notify('success','Course category successfully deleted');
		$this->redirect('/admin/coursecategory/');
	}

	public function validiateProgramAction()
{
	$this->_helper->layout->disableLayout();
	$programDb = new App_Model_ProgramCategory();

	$program_route = $this->_getParam('program_route',null);
	$program_level = $this->_getParam('program_level',null);
    $program_category = $this->_getParam('program_category',null);

	$programdata = $programDb->getValidProgram($program_route,$program_level,$program_category);

	
     if(empty($programdata))
     {
     	echo 0;
     }else{
     	// $programdata = $programDb->getprogramdata($program_route,$program_level);
     	 $json = Zend_Json::encode($programdata);
         print_r($json);
     }
	 exit();
}

}

