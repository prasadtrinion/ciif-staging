<?php
class Admin_MemberDocumentController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
		
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_MemberDocumentSearch(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view	
		$lobjemailTemplateModel = new Admin_Model_DbTable_MemberDocument();  // email template model object
		$larrresult = $lobjemailTemplateModel->getsearch(); // get template details	
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();						
				if ($lobjsearchform->isValid($larrformData)) {
				$post_data=$this->getRequest()->getPost();
           
           				 $id=$post_data['field2'];	
					$larrresult = $lobjemailTemplateModel->search($id);										
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
			$this->_redirect( $this->baseUrl . '/admin/MemberExperience/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function addAction() { 	



  			$member_form = new Admin_Form_MemberDocument();
            $this->view->form = $member_form;

            if($this->getRequest()->isPost())
            {    
        

                $formdata =$this->getRequest()->getPost();
              
                if($formdata)
                {
                    $name = $this->getparam('name'); 

                    
                    $dropdown_member = $this->getparam('dropdown_member');
                    $remarks = $this->getparam('remarks'); 
 

                    
                 $add_member = new Admin_Model_DbTable_MemberDocument();
                  
                    $add_member->addData($dropdown_member,$name,$remarks);
                    if($add_member)
                    {
                        $this->_redirect( $this->baseUrl . '/admin/MemberExperience/index');
                    }
                    
                }


            }
	}

	public function editAction() {		
 		
 	 	
            $editform = new Admin_Form_MemberDocumentEdit();
    		$this->view->form = $editform;
		if($this->getRequest()->isPost())

		{

			$member_form = $this->getRequest()->getPost();
			
			if($member_form)
		{

				$id = $this->getRequest()->getparam('Id');
			
                    $name = $this->getparam('mrc_name'); 

                    
                    $dropdown_member = $this->getparam('mrc_type');
                    $remarks = $this->getparam('mrc_remarks');

                 
				$edit_member = new Admin_Model_DbTable_MemberDocument();
				
                      
				$edit_member->editmember($id,$dropdown_member,$name,$remarks);

                    if($edit_member)
                    {
                         $this->_redirect( $this->baseUrl . '/admin/MemberDocument/index');
                    }

                  //Line 5
//$this->_helper->redirector('index');
			}

		else
		{
    		 $editform->populate($formData);
		}


		}
		 else
		              {
                        $id = $this->getRequest()->getparam('Id');

                         $file = new Admin_Model_DbTable_MemberDocument();
                         $files = $file->fetchRow('mrc_id='.$id);
                         $editform->populate($files->toArray());
                        
                     }    
$this->view->form = $editform;  	  	
}



public function getsubtypeAction()
{
	$this->_helper->layout->disableLayout();

	$memberId = $this->_getParam('member_id',null);

	$query =  new Admin_Model_DbTable_Member();

	$memberdata = $query->getmeberdata($memberId);

	echo $memberdata['sub_type'];

	 exit();
}


}