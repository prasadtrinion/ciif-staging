<?php
class Admin_BulkAdmissionController extends Zend_Controller_Action
{
	protected $itemLog = array();

	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'calendar');

		$this->eventsDb = new App_Model_CalendarEvent(); 
		$this->locale = Zend_Registry::get('Zend_Locale');
		$this->bulkDB = new Admin_Model_DbTable_Bulk();
		$this->uploadDir = UPLOAD_PATH.'/bulk';

		require_once APPLICATION_PATH.'/../library/Others/parsecsv.lib.php';
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
		$common = new Cms_Common();
	}

	public function indexAction()
	{
	
		$this->view->title = "Bulk Admission";
		
		$p_data = $this->bulkDB->getPaginateData();

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
    	
	}

	public function uploadAction()
	{
		/*	
			problem/missing
			- Designation
			- program_scheme
			- Medium of Instruction
			- Proficiency in English -- kena filter by code
		*/
					$common = new Cms_Common();

		$this->view->title = "Bulk Admission - Upload";
		
		if ($this->_request->isPost() && $this->_request->getPost('save')) 
		{
			$larrformData = $this->_request->getPost();

			
			
			$total = 0;

			try 
			 {
				if ( !is_dir( $this->uploadDir ) )
				{
					if ( $common->mkdir_p($this->uploadDir) )
					{
						throw new Exception('Cannot create document folder ('.$this->uploadDir.')');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
				
			
				$files = $adapter->getFileInfo();
				
				$adapter->addValidator('NotExists', false, $this->uploadDir );
				$adapter->setDestination( $this->uploadDir );
				$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 10000));
				$adapter->addValidator('Size', false, array('min' => 100 , 'max' => 419478304 , 'bytestring' => true));

				foreach ($files as $no => $fileinfo) 
				{
					$adapter->addValidator('Extension', false, array('extension' => 'csv', 'case' => false));
				
					if ($adapter->isUploaded($no))
					{
						$ext =  $common->getext($fileinfo['name']);
						$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
						
						$fileUrl = $this->uploadDir.'/'.$fileName;

						$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
						$adapter->setOptions(array('useByteString' => false));
						
						 $size = $adapter->getFileSize($no);
					
						//die();
						if( !$adapter->receive($no))
						{
							$errmessage = array();
							if ( is_array($adapter->getMessages() ))
							{	
								foreach(  $adapter->getMessages() as $errtype => $errmsg )
								{
									$errmessage[] = $errmsg;
								}
								
								throw new Exception(implode('<br />',$errmessage));
							}
						}

						
						//process - CSV
						$csv = new parseCSV();

						$csv->auto($fileUrl);

						// echo "<pre>";
						// print_r($fileinfo);
						// die();
						
					
						$totalfields = count($csv->data[0]);
						
						// echo $totalfields;
						// die();
						
						// if ($totalfields != 41 && $totalfields != 3 && $totalfields != 8)
						// {
						// 	$this->view->result =1;
						
						// 	//throw new Zend_Exception('Invalid CSV format');
						// }


						//save into db
						if ( !isset($this->bulk_id) )
						{
							$auth = Zend_Auth::getInstance(); 
							$data = array(
											'b_filename'		=> $fileinfo['name'],
											'b_fileurl'			=> '/bulk/'.$fileName,
											'b_status'			=> 'NEW',
											'b_totalapp'		=> count($csv->data),
											'b_created_by'		=> $auth->getIdentity()->id,
											'b_created_date'	=> date("Y-m-d H:i:s")
										);
							// echo "<pre>";
							// print_r($data);
							// die();
							
							$this->bulk_id = $this->bulkDB->addData($data);
						}

						

					} //isuploaded
					
				} //foreach
				
				//$this->bulkDB->updateData(array('b_totalapp' => $total), $this->bulk_id);
                
				$this->_redirect('/admin/bulk-admission/review/id/'.$this->bulk_id);	

			}
			catch (Exception $e) 
			{
				throw new Exception( $e->getMessage() );
			}

			

		} // if 
	}


	
	
	protected function getItemID($row)
	{
		
			return $row['Name'];
		
	}

	public function reviewAction()
	{
		

		$this->view->title = "Bulk Admission - Review";

		$id = $this->_getParam('id',null);

		// echo $id;
		// die();

		$bulkinfo = $this->bulkDB->getData($id);
		// print_r($bulkinfo);
		// die();

		if ( empty($bulkinfo) )
		{
			throw new Exception('Invalid Bulk ID');
		}
		
		$this->bulk_id = $bulkinfo['b_id'];
		$importedItems = $this->bulkDB->getItems($id);

		// echo "<pre>";
		// print_r($importedItems);
		// die();
		$importedBID = array();
		foreach ( $importedItems as $iitem ) 
		{
			$importedBID[] = $iitem['bi_bid'];
		}

		//parse CSV
		$csv = new parseCSV();
		$csv->auto(UPLOAD_PATH.$bulkinfo['b_fileurl']);
		
		//start
		$items = array();
		// echo "<pre>";
		// print_r($csv->data);
		// die();
		$fileds = count($csv->data[0]);
		// echo $fileds;
		// die();
		if($fileds>0)
		{
 			foreach ( $csv->data as $row_id => $row )
		 {
			$bid = $this->getItemID($row);
			// echo $bid;
			// die();
			$items[$bid] = array('BID' => $bid) + $row;

			//$this->processItem(array('BID' => $bid) + $row);
		 }
		}
		
		//post
		if($fileds>0)
		{
		//  if ($this->_request->isPost() && $this->_request->getPost('save')) 
		// {
			$formData = $this->_request->getPost();
			$count = $formData['item'];
			
			if ( $count > 0 )
			{
				$total = 0;
				foreach ( $formData['item'] as $BID )
				{
					$this->processItem($items[$BID], 1);
					$total++;
				}
				
				$this->bulkDB->updateData(array('b_status' => 'IMPORTED', 'b_totaladded' => new Zend_Db_Expr('b_totaladded+'.$total)), $this->bulk_id);

				$this->_helper->flashMessenger->addMessage(array('success' => $total." Applicant imported"));
				$this->_redirect('/admin/bulk-admission/review/id/'.$this->bulk_id);
			}
			else
			{
				//error
			}
		// }
		
}
		
		//views
		$this->view->importedBID = $importedBID;
		$this->view->items = $items;
		$this->view->itemLog = $this->itemLog;
		
	}


	protected function logItem($bid, $field='', $msg='' )
	{
		$this->itemLog[$bid][] = array('field'	=> $field,
										'msg'	=> $msg);
	}

	protected function processItem($row, $insert=0)
	{
			// echo $row['DiscountAmount'];
			// die();



			// echo "<pre>";
			// print_r($row);
			// // //var_dump($row);
			//   die();

		//Models
		$userDb = new App_Model_User();
		// $appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		// $appTransactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		 $objdeftypeDB = new App_Model_Definitiontype();

		$formData['password']  = 123456;
		  $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
           
          if ( empty($row['Classification']))
			{
				$appl_info['id_member'] = 0;
			}
			else
			{
				$member = new Admin_Model_DbTable_Member();
				$getMember = $member->getMemberData($row['Classification']);
				$appl_info['id_member'] = $getMember['idmember'];
			}

		if ( empty($row['Sub-Classification']))
			{
				if($appl_info['id_member'] == 1)
				{
					$appl_info['id_member_subtype'] = 1;
				}else{
				$appl_info['id_member_subtype'] = 0;
			}
			}
			else
			{
				
				$member = new Admin_Model_DbTable_MemberSub();
				$getMember = $member->getMemberData($row['Sub-Classification']);
				if($appl_info['id_member'] == 1)
				{
					$appl_info['id_member_subtype'] = 1;
				}else{
				
				$appl_info['id_member_subtype'] = $getMember['id_subtype'];
				}

				
			}

		if(empty($row['Correspondence State']))
		{
				$appl_info['cor_state'] = 0;
			}
			else
			{
				$State = new App_Model_State();
				$getState = $State->getState($row['Correspondence State']);
				$appl_info['cor_state'] = $getState['idState'];
			}
		if(empty($row['Residential State']))
		{
				$appl_info['state'] = 0;
			}
			else
			{
				$State = new App_Model_State();
				$getState = $State->getState($row['Residential State']);
				$appl_info['state'] = $getState['idState'];
			}

		if(empty($row['Business State']))
		{
				$bus_info['c_state'] = 0;
			}
			else
			{
				$State = new App_Model_State();
				$getState = $State->getState($row['Business State']);
				$bus_info['c_state'] = $getState['idState'];
			}

		// if(empty($row['Correspondence Country']))
		// {
		// 		$appl_info['c_country'] = 0;
		// 	}
		// 	else
		// 	{
		// 		$Country = new App_Model_Country();
		// 		$getState = $Country->getCountry($row['Correspondence Country']);
		// 		$appl_info['c_country'] = $getState['idCountry'];
		// 	}

		if(empty($row['Correspondence Country']))
		{
				$appl_info['cor_country'] = 0;
			}
			else
			{
				$Country = new App_Model_Country();
				$getState = $Country->getCountry($row['Correspondence Country']);
				$appl_info['cor_country'] = $getState['idCountry'];
			}
	if(empty($row['Residential Country']))
		{
				$appl_info['country'] = 0;
			}
			else
			{
				$Country = new App_Model_Country();
				$getState = $Country->getCountry($row['Residential Country']);
				$appl_info['country'] = $getState['idCountry'];
			}

		if(empty($row['Bus. Country']))
		{
				$bus_info['c_country'] = 0;
			}
			else
			{
				$Country = new App_Model_Country();
				$getState = $Country->getCountry($row['Bus. Country']);
				$bus_info['c_country'] = $getState['idCountry'];
			}

			if(!empty($getState['idCountry']))
			{
			$countryDb = new App_Model_Country();
                    $country =  $countryDb->fetchRow(array("idCountry = ?" => $getState['idCountry']));

                  if($country['CountryName'] == 'MALAYSIA')
                  {

                    $formData['nationality'] = 'MY';
                  }else{

                    $formData['nationality'] = 'OTH';
                  }      
}
		// if ( $insert == 0 )
		// {
		// 	return false;
		// }
		
		if ( $row['Name'] && !empty($row['Classification']))
		{

			//applicant_profile
			$appl_info['firstname']    = $row['Name'];
			$appl_info['username'] 	   = '';
			$appl_info["cor_address1"] = $row['Correspondence Address Line 1'];
			$appl_info["cor_address2"] = $row['Correspondence Address Line 2'];
			$appl_info["cor_address3"] = $row['Correspondence Address Line 3'];
			$appl_info['email'] 			= $row['Primary E-mail'];
			$appl_info['role'] 				= 'student';
			$appl_info['register_date']		= date('Y-m-d');
			$appl_info['approve_status'] 	= 1;
			$appl_info['approve_status1']	= 1;
			$appl_info['city'] 		= $row['Residential City'];
			$appl_info["address1"]  = $row['Residential Address Line 1'];
			$appl_info["address2"]  = $row['Residential Address Line 2'];
			$appl_info["address2"]  = $row['Residential Address Line 3'];
			$appl_info['postcode']  = $row['Residential Postcode'];
			$appl_info['cor_postcode'] = $row['Correspondence Postal'];
			$appl_info['amc_approve_date'] = date('Y-m-d', strtotime($row['GC Endorsement']));
			$appl_info['tel_office'] = $row['Telephone (Office)'];
			$appl_info['sec_mobile'] = $row['Mobile (Secondary)'];
			$appl_info['city'] = $row['Residential City'];
			$appl_info["nationality"]	= $formData['nationality'];
			$appl_info["password"]	= $hash;		
			$appl_info['salt']          	= $salt;
			$appl_info['admin_notes_amc']   = $row['Remarks'];
			
			
 
		
			$user_id = $userDb->insert($appl_info);
			$memberReg =new App_Model_Membership_MemberRegistration();
            $data = array('mr_membershipId' => $user_id,
            'mr_sp_id' =>  $row['Membership No'],
            'mr_status'=> 1384,
            'mr_activation_date' => date('Y-m-d H:i:s'),
            'mr_expiry_date' => date('Y-m-d H:i:s', strtotime(date("Y-m-d", time()) . " + 1 year")),
            'mr_payment_status' => 1);
          
        $memberReg->insert($data);

        $bus_info['c_postcode']  = $row['Business Postcode'];
        $bus_info['	c_address1'] = $row['Business Address Line 1'];
        $bus_info['	c_address2'] = $row['Business Address Line 2'];
        $bus_info['	c_address3'] = $row['Business Address Line 3'];
		}

		if ( $row['Name'] && empty($row['Classification']))
		{

			//applicant_profile
			$appl_info['firstname']    = $row['Name'];
			$appl_info['username'] 	   = '';
			$appl_info["cor_address1"] = $row['Correspondence Address Line 1'];
			$appl_info["cor_address2"] = $row['Correspondence Address Line 2'];
			$appl_info["cor_address3"] = $row['Correspondence Address Line 3'];
			$appl_info['email'] 			= $row['Primary E-mail'];
			$appl_info['role'] 				= 'student';
			$appl_info['register_date']		= date('Y-m-d');
			$appl_info['approve_status'] 	= 0;
			$appl_info['approve_status1']	= 0;
			$appl_info['city'] 		= $row['Residential City'];
			$appl_info["address1"]  = $row['Residential Address Line 1'];
			$appl_info["address2"]  = $row['Residential Address Line 2'];
			$appl_info["address2"]  = $row['Residential Address Line 3'];
			$appl_info['postcode']  = $row['Residential Postcode'];
			$appl_info['cor_postcode'] = $row['Correspondence Postal'];
			$appl_info['amc_approve_date'] = date('Y-m-d', strtotime($row['GC Endorsement']));
			$appl_info['tel_office'] = $row['Telephone (Office)'];
			$appl_info['sec_mobile'] = $row['Mobile (Secondary)'];
			$appl_info['city'] = $row['Residential City'];
			$appl_info["nationality"]	= $formData['nationality'];
			$appl_info["password"]	= $hash;		
			$appl_info['salt']          	= $salt;
			$appl_info['admin_notes_amc']   = $row['Remarks'];
			
			
 
		
			// $user_id = $userDb->insert($appl_info);
			// $memberReg =new App_Model_Membership_MemberRegistration();
   //          $data = array('mr_membershipId' => $user_id,
   //          'mr_sp_id' =>  $row['Membership No'],
   //          'mr_status'=> 1384,
   //          'mr_activation_date' => date('Y-m-d H:i:s'),
   //          'mr_expiry_date' => date('Y-m-d H:i:s', strtotime(date("Y-m-d", time()) . " + 1 year")),
   //          'mr_payment_status' => 1);
          
        // $memberReg->insert($data);

        $bus_info['c_postcode']  = $row['Business Postcode'];
        $bus_info['	c_address1'] = $row['Business Address Line 1'];
        $bus_info['	c_address2'] = $row['Business Address Line 2'];
        $bus_info['	c_address3'] = $row['Business Address Line 3'];
		}



	}



	protected function ProcessSponser($row, $insert=0)
	{

		// die('sponser');
		//Models
		$sponser= new GeneralSetup_Model_DbTable_Sponsor();
		
		if ( $row['SPON_ID'])
		{
			
			$sponser_info['SponsorCode'] = $row['CODE'];
			$sponser_info["fName"] = $row['SPON_NAME'];
			            					
			$sponser_info= $sponser->addData($sponser_info);
		}
	}

protected function ProcessStudentSponser($row, $insert=0)
	{
	
		$sponser_student= new GeneralSetup_Model_DbTable_sponsortag();
		if ( $row['FS101SPONSOR_ID'])
		{
		
			$sponser_tag['IdSponsorTag'] = $row['FS101SPONSOR_ID'];
			$sponser_tag['Sponsor'] = $row['FS101CMPYCODE'];
			$sponser_tag["StudentId"] = $row['FS101STUD_ID'];
			$sponser_tag["StartDate"] = $row['FS101FROM_DATE'];
			$sponser_tag["EndDate"] = $row['FS101TO_DATE'];
			$sponser_tag["AggrementStatus"] = $row['FS101AGG_STATUS']; 
			$sponser_tag["AggrementNo"] = $row['FS101AGG_NO'];
			$sponser_tag["Amount"] = $row['FS101SPONSOR_AMT'];

			$sponser_details= $sponser_student->addData($sponser_tag);
						
		}
	}


	function backupprocess()
	{
		/*
		Array
		(
			[CP ID] => INCEIF-HQ
			[Salutation] => Ms
			[First Name] => AZILA
			[Last Name] => Ali
			[Id type] => MyKad
			[Id Number] => 850412-02-5812
			[Dob] => 04/12/1985
			[Intake Session] => 2014 June Semester
			[Nationality] => MALAYSIAN
			[Religion] => ISLAM
			[Email Id] => nnadianadzri@gmail.com
			[Address (Permanent)] => 37,JALAN BOLA LISUT, 13/17 SEKSYEN 13
			[City (Permanent)] => SHAH ALAM
			[State (Permanent)] => SELANGOR
			[Country (Permanent)] => Malaysia
			[Postcode (Permanent)] => 40100
			[Address (Correspondance)] => 
			[City (Correspondance)] => 
			[State (Correspondance)] => 
			[Country (Correspondance)] => 
			[Postcode (Correspondance)] => +60122733090
			[Fax] => 
			[Moblie] => +60123589936
			[Employment Status] => Self-employed
			[Company Name] => NADS MEDIA PRODUCTION SDN BHD
			[Company Address] => 2-01-2 PRESINT ALAMI, PERSIARAN AKUATIK SEKSYEN 13
			[Company Telephone Number] => +60355108236
			[Company Fax Number] => 
			[Designation] => EXECUTIVE PRODUCER
			[Position] => Officer
			[Duration of service (Start Date)] => 
			[Duration of service (End Date)] => 
			[Industry] => Marketing/Sales
			[Industry Working Experience] => 
			[Years in Industry (Working Experience)] => 
			[Health condition] => NORMAL
			[Financial Support] => Self-funding
			[Name of Sponsor] => 
			[Sponsor Code] => 
			[Scholarship Plan] => 
			[Previous Student] => yes
			[Student ID] => 1200133
			[Program] => Masters in Islamic Finance Practice
			[Mode of study] => Full-time
			[Mode of program] => Online
			[Programme Type] => 
			[Race] => MELAYU
			[Gender] => Female
			[Age] => 
			[Marital Status] => Married
			[Highest Level] => Bachelor
			[Name of University/College] => UNIVERSITI KEBANGSAAN MALAYSIA
			[Diploma/Degree Awarded] => BACHELOR OF SCIENCE (STATISTICS)
			[Class Degree] => 2nd Class Lower/Division 2
			[Result/CGPA] => 2.65
			[Year Graduated] => 2003
			[Medium of Instruction] => 
			[Proficiency in English] => MUET
			[English Language Proficiency  - Date Taken] => 
			[Result for English proficiency] => 
			[Malaysian Visa] => Yes
			[Visa Status] => Student
			[Visa Expiry Date] => 
		)*/

		// $branch = $branchDB->getData($row['CP ID'], 'BranchCode');
		// 	$branchID = $branch['IdBranch'];
		// 	$row['Nationality'] = $row['Nationality'] == 'MALAYSIAN' ? 'MALAYSIA':$row['Nationality'];

		// 	//applicant_profile
		// 	$appl_info['appl_fname'] = $row['First Name'];
		// 	$appl_info['appl_lname'] = $row['Last Name'];
		// 	$appl_info["branch_id"] = $branchID;            
		// 	$appl_info["appl_email"]= $formData['Email Id'];
		// 	$appl_info["appl_password"]= '';				
		// 	$appl_info["appl_role"]=0;
		// 	$appl_info["create_date"]=date("Y-m-d H:i:s");
		// 	$appl_info['appl_dob']=date('Y-m-d',strtotime($row['Dob']));
		// 	$appl_info['appl_salutation']= $this->getIdByDef($row['Salutation'], $objdeftypeDB->fnGetDefinationsByLocale('Salutation'));
		// 	$appl_info['appl_idnumber'] = $row['Id Number'];
		// 	$appl_info['appl_idnumber_type'] = $this->getIdByDef($row['Id type'],$objdeftypeDB->fnGetDefinations('ID Type'));
		// 	$appl_info['appl_nationality'] = $this->getIdByDef($row['Nationality'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
		// 	$appl_info['appl_religion'] = $this->getIdByDef($row['Religion'],$objdeftypeDB->fnGetDefinations('Religion'));
		// 	$appl_info['appl_address1'] = $row['Address'];
		// 	$appl_info['appl_city'] = $row['City'];
		// 	$appl_info['appl_state'] = $this->getState($row['State']);
		// 	$appl_info['appl_country'] = $this->getIdByDef($row['Country'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
		// 	$appl_info['appl_postcode'] = $row['Postcode'];
		// 	$appl_info['appl_phone_home'] = $row['Contact No.'];
		// 	$appl_info['appl_fax'] = $row['Fax'];
		// 	$appl_info['appl_phone_mobile'] = $row['Moblie'];
		// 	$appl_info['prev_student'] = $row['Previous Student'] == 'yes' ? 1 : 0;
		// 	$appl_info['prev_studentID'] = $row['Student ID'];
		// 	$appl_info['appl_race'] = $this->getIdByDef($row['Race'],$objdeftypeDB->fnGetDefinations('Race'));
		// 	$appl_info['appl_gender'] = $row['Gender'] == 'Female' ? 2 : 1;
		// 	$appl_info['appl_marital_status'] = $this->getIdByDef($row['Marital Status'],$objdeftypeDB->fnGetDefinations('Marital Status'));
		// 	//

		// 	$applicant_id = $appProfileDB->addData($appl_info);
			
		// 	//generate applicant ID
		// 	$gen_applicantId =  $this->generateApplicantID();

		// 	//applicant_transaction
		// 	$info2["at_appl_id"]=$applicant_id;
		// 	$info2["at_pes_id"]=$gen_applicantId;
		// 	$info2["at_create_by"]=$applicant_id;
		// 	$info2["at_status"]=593; //offered
		// 	$info2["at_create_date"]=date("Y-m-d H:i:s");
		// 	$info2["entry_type"]=0;//manual			
		// 	$info2["at_default_qlevel"]=null;	
		// 	$info2["at_intake"] = $this->getIntake($row['ARD_INTAKE]']);
		
		
		// 	//trans ID		    					
		// 	$at_trans_id = $appTransactionDB->addData($info2);
			
		// 	//program
			
		// 	$program =$this->getProgram($row["Program"]);

			
		// 	$info3['mode_study'] = $this->getIdByDef(str_replace('-','',$row['Mode of study']),$objdeftypeDB->fnGetDefinations('Mode of Study'));
		// 	$info3['program_mode'] = $this->getIdByDef($row['Mode of program'],$objdeftypeDB->fnGetDefinations('Mode of Program'));
		// 	$info3['program_type'] = '0';
		// 	$info3['ap_prog_scheme'] = '0';
		// 	$info3["ap_at_trans_id"]=$at_trans_id;
		// 	$info3["ap_prog_code"]=$program["ProgramCode"];
		// 	$info3["ap_prog_id"]=$program['IdProgram'];		
			
		// 	$appProgramDB  = new App_Model_Application_DbTable_ApplicantProgram();			
		// 	$appProgramDB->addData($info3);
			
		// 	//qualification
		// 	$data6 = array(	    		   
		// 		'ae_appl_id' => $applicant_id,
		// 		'ae_transaction_id' => $at_trans_id,
		// 		'ae_qualification' => $this->getIdByDef($row['Highest Level'],$entryReqDB->getListGeneralReq(),array('k'=>'QualificationLevel','v'=>'IdQualification')),
		// 		'ae_degree_awarded' => $row['Diploma/Degree Awarded'],
		// 		'ae_class_degree' => $this->getIdByDef($row['Class Degree'],$objdeftypeDB->fnGetDefinations('Class Degree')),
		// 		'ae_result' => $row['Result/CGPA'],
		// 		'ae_year_graduate' => $row['Year Graduated'],
		// 		'ae_institution_country' => '0',
		// 		'ae_institution' => '0',
		// 		'ae_medium_instruction' => '0',	
		// 		'others' => '0',	    		  	    		
		// 		'createDate' => date("Y-m-d H:i:s")
		// 	);



		// 	$ae_id = $appQualificationDB->addData($data6);

		// 	//english
		// 	$data4 = array(    			 
		// 		'ep_transaction_id' => $at_trans_id,
		// 		'ep_test' => $this->getIdByDef($row['Proficiency in English'],$objdeftypeDB->fnGetDefinations('English Proficiency Test List')),
		// 		'ep_test_detail' => $formData['english_test_detail'],
		// 		'ep_date_taken' => date('Y-m-d',strtotime($formData['English Language Proficiency - Date Taken'])),
		// 		'ep_score' => $formData['Result for English proficiency'] == '' ? 0 : $formData['Result for English proficiency'],
		// 		'ep_updatedt' => date("Y-m-d H:i:s"),
		// 		'ep_updateby'=>$applicant_id
		// 	);
			
		// 	$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
		// 	$ep_id=$appEngProfDB->addData($data4);
			
		
		// 	//employment info
		// 	$data8 = array(	    	
		// 		'ae_appl_id' => $applicant_id,
		// 		'ae_trans_id' => $at_trans_id,	 
		// 		'ae_status' => $this->getIdByDef($row['Employment Status'],$objdeftypeDB->fnGetDefinations('Employment Status')),
		// 		'ae_comp_name' => $row['Company Name'],
		// 		'ae_comp_address' => $row['Company Address'],
		// 		'ae_comp_phone' => $row['Company Telephone Number'],
		// 		'ae_comp_fax' => $row['Company Fax Number'],
		// 		'ae_designation' => $row['Designation'],
		// 		'ae_position' => (int)$formData['Position'],
		// 		'ae_from' => date('Y-m-d',strtotime($row['Duration of service (Start Date)'])),
		// 		'ae_to' => date('Y-m-d',strtotime($row['Duration of service (End Date)'])),	    		  
		// 		'emply_year_service' => '0',
		// 		'ae_industry' => $this->getIdByDef($row['Industry'],$objdeftypeDB->fnGetDefinations('Industry Working Experience')),	 
		// 		'ae_job_desc' => '',	    		   	    		
		// 		'upd_date' => date("Y-m-d H:i:s")
		// 	);
							
		// 	$appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
		// 	$ae_id = $appEmploymentDB->addData($data8);
			
		// 	//health condition
		// 	$appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
		// 	$data9 = array(    				   
		// 		'ah_appl_id' => $applicant_id,
		// 		'ah_trans_id' => $at_trans_id,
		// 		'ah_status' => $this->getIdByDef($row['Health condition'],$objdeftypeDB->fnGetDefinations('Industry Working Experience')),
		// 		'upd_date' => date("Y-m-d H:i:s")
		// 	);
						
		// 	$appHealthDB->addData($data9);

		// 	//financial support
		// 	$appFinancial = new App_Model_Application_DbTable_ApplicantFinancial();

		// 	$data10 = array(
		// 		'af_appl_id' => $applicant_id,
		// 		'af_trans_id' => $at_trans_id,
		// 		'af_method' => $this->getIdByDef($row['Financial Support'],$objdeftypeDB->fnGetDefinations('Funding Method')),
		// 		'af_sponsor_name' => $row['Name of Sponsor'],
		// 		'af_type_scholarship' => '0',
		// 		'af_scholarship_secured' => $row['Scholarship Plan'],
		// 		'af_scholarship_apply' => '0',
		// 		'upd_date' => date("Y-m-d H:i:s")
		// 	);

		// 	$af_id = $appFinancial->addData($data10);

		// 	//visa
		// 	$data11 = array(
		// 		'av_appl_id' => $applicant_id,
		// 		'av_trans_id' => $at_trans_id,
		// 		'av_malaysian_visa' => $row['Malaysian Visa'] == 'Yes' ? 1 : 0,
		// 		'av_status' => $this->getIdByDef($row['Visa Status'],$objdeftypeDB->fnGetDefinations('Visa Status')),
		// 		'av_expiry' => date('Y-m-d',strtotime($row['Visa Expiry Date'])),
		// 		'upd_date' => date("Y-m-d H:i:s")
		// 	);
			
		// 	$appVisaDB->insert($data11);

		// 	//item
		// 	$data = array(
		// 					'b_id'				=> $this->bulk_id,
		// 					'bi_trans_id'		=> $at_trans_id,
		// 					'bi_appl_id'		=> $applicant_id
		// 				);
			
		// 	$bulk_id = $this->bulkDB->addItem($data);

		// 	$total++;
	}

	function getState($state)
	{
		$db = getDB();
	  	$select = $db->select()
	                 ->from(array('s'=>'tbl_state'))
	                 ->where('LOWER(s.StateName) = ?', strtolower($state));
        $row = $db->fetchRow($select);
		
		if ( !empty($row) )
		{
			return $row['idState'];
		}
		else
		{
			return '';
		}
	}
	

	public function getProgram($name)
	{
		// echo $name;
		// die();
		$db = getDb();

		$sql =  $db->select()
	                 ->from(array('a'=>'tbl_program'),array('a.*'))
	                 ->joinleft(array('b'=>'tbl_program_scheme'),'a.IdProgram=b.IdProgram')
	                 ->where('LOWER(a.ProgramName) = ?', strtolower($name));

		$result = $db->fetchRow($sql);

		if ( empty($result) )
		{
			return '';
		}
		else
		{
			return $result;
		}
	}
	
	public function getIntake($name='')
	{
		$db = getDb();
        $sql = "Select IdIntake from tbl_intake where IntakeId ='$name'";
		// $sql =  $db->select()
	 //     	->from(array('a'=>'tbl_intake'),array('IdIntake'))
	 //         ->where('a.IntakeDesc = ?', $name);

		$result = $db->fetchRow($sql);

		if ( empty($result) )
		{
			return '';
		}
		else
		{
			return $result['IdIntake'];
		}
	}

	public function getIdByDef($id, $data, $map = array('k'=>'DefinitionDesc','v'=>'idDefinition') )

	{
		// echo $id;
		// echo "----";
		// print_r($data);
  		//        echo "\n";

		//die();
		if ( !empty($data) )
		{
			
			foreach ( $data as $row )
			{
				if ( strtolower($row[$map['k']]) == strtolower($id) )
				{
					$val = $row[$map['v']];
				}
			}
		}

		if ( empty($val) )
		{
			return 0;
		}
		else
		{
			return $val;
		}
	}

	function generateApplicantID(){

		return mt_rand(1,9999999999);
	 	// echo 	$applicantID = date("Y-m-d H:i:s").$randomNO;
	 	// return $applicantID;
		
		//generate applicant ID
		$applicantid_format = array();
		
		$seqnoDb = new App_Model_Application_DbTable_ApplicantSeqno();
		$sequence = $seqnoDb->getData(date('Y'));
		
		$tblconfigDB = new App_Model_General_DbTable_TblConfig();
		$config = $tblconfigDB->getConfig(1);
		
		//format
		 $format_config = explode('-',$config['ApplicantIdFormat']);
		
		for($i=0; $i<count($format_config); $i++){
			$format = $format_config[$i];
			if($format=='px'){ //prefix
				$result = $config['ApplicantPrefix'];
			}elseif($format=='yyyy'){
				$result = date('Y');
			}elseif($format=='yy'){
				$result = date('yy');
			}elseif($format=='seqno'){				
				$result = sprintf("%03d", $sequence['seq_no']);
			}
			array_push($applicantid_format,$result);
		}
	
		//var_dump(implode("-",$applicantID));
		$applicantID = implode("-",$applicantid_format);
		
		//update sequence		
		$seqnoDb->updateData(array('seq_no'=>$sequence['seq_no']+1),$sequence['seq_id']);
		
		return	$applicantID;
	}
}
?>