<?php

class Admin_ReportamcController extends  Zend_Controller_Action
{
    protected $coursesDb;
    protected $contentBlockDb;
    protected $coursecatDb;
    protected $uploadDir;
    protected $uploadFolder;

    public function init()
    {
        
        $this->_helper->layout()->setLayout('/admin');

        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->auth = Zend_Auth::getInstance();

        //Zend_Layout::getMvcInstance()->assign('nav', 'courses');

      

        $this->uploadFolder = 'tracking-assets';
        $this->uploadDir = DOCUMENT_PATH. '/'.$this->uploadFolder;

    }

    public function indexAction()
    {
        $this->view->title = 'Tracking Report';


        $session  = new Zend_Session_Namespace('AdminTrackingIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        
       

           
        $data= new Admin_Model_DbTable_User();
        $def = $data->getmember();
        
         $this->view->programlevel = $def;

            if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            
            $programDB = new Admin_Model_DbTable_User();
            $larrresult = $programDB->getDetails($formData);
           
             $this->view->program =$formData['program'];
            $this->view->paginator = $larrresult;

        
      }

       

    }

    public function viewAction()
    {

        $programlevel = $this->getParam('id');
         $auth = Zend_Auth::getInstance()->getIdentity();
        $userId = $auth->id;
         
        $programDB = new Admin_Model_DbTable_User();
            $larrresult = $programDB->getViewDetails($programlevel); 

            // echo '<pre>';
            // print_r($larrresult);
            // die();

          $k=0;$j=0;$l=0;$m=0;$n=0;
          foreach ($larrresult as $key) {
            if($key['program level'] == 'Student Member')
            {
                $j++;
            }
            else if($key['program level'] == 'Associate  Member')
            {
                $k++;            
            }

            else if($key['program level'] == 'Ordinary Member')
            {
                $l++;            
            }
            else if($key['program level'] == 'Fellow Member')
            {
                $m++;            
            }
            // else if($key['program level'] == 'Category2')
            // {
            //     $n++;            
            // }
        }
        $Total = $k+$j+$l+$m;

      $processing_fee="

    
   <table align='center' border=0 style='width:100%' >
         <tr>
          <td width='10%'>
               &nbsp;
            </td>
            <td width='50%'>
               <img src='./assets/images/ciifp_new_logo1.jpg' alt='ciifp logo' >
            </td>

            <td width='40%'>
              <table>
                 <tr><td style='text-align:right;'>KNOWLEDGE | INTEGRITY | SERVICE</td></tr>
                                  <tr><td  style='text-align:right;'> 3rd Floor, Menara Takaful Malaysia,<br/>
                           Jalan Sultan Sulaiman, 50000 Kuala Lumpur<br/>
                           03-2273 2135 / 2136</td></tr>
                           <tr>
                           <td style='text-align:right;color:#459bca;font-size: 12px;'>www.ciif-global.org</td></tr>

                 
              </table>
            </td>
        </tr>
               
      </table>
      <br><br><br><br>";
      
        $processing_fee .="<body>
        <table align='center' border='1' style='border-collapse: collapse;width: 50%;' > 
    <p align='center'><b>THE CHARTERED INSTITUTE OF ISLAMIC FINANCE PROFESSIONALS<br>
    (1148700 – X)<br><br>
    16th ADMISSION & MEMBERSHIP COMMITTEE MEETING ON 12th JULY 2018</p>
    <br><br>
    </table>

    <p>AGENDA 4:<br><br>
    ADMISSION OF NEW MEMBERS</p>

<hr>

1. OBJECTIVE <br><br></p>
    
<p>The objective of this paper is to present to the AMC the candidates to be admitted as new members of the CIIF.
</p>
<br>
<p>
2. BACKGROUND <br><br></p>

<p>The CIIF Management received several membership applications submitted via email.

The list of proposed members for the AMC to review and consider for admission is in Appendix 4.1 below. Meanwhile, there are still outstanding applications with incomplete information, which the CIIF Management has put on a “K.I.V.” list for the time being due to the lack of response to requests for further information. </p>


    <p>A summary of the total proposal for admission is as follows:</p></table>";

    $processing_fee .=" 
     <br><br><table align='center' border='1' style='border-collapse: collapse; width: 70%;font-size: 12px;'>
     <tr>
     <td style='background-color:#6acee8;' ><b>Membership Classification</td>
     <td text-align:'center'; style='background-color:#6acee8;'><b>Admission Pax</td>
     </tr>
     <tr><td>Student Member</td>
     <td style='text-align:center;'>$j</td></tr>
     <tr><td>Associate  Member</td>
     <td style='text-align:center;'>$k</td></tr>
     <tr><td>Ordinary Member</td>
     <td style='text-align:center;'>$l</td></tr>
     <tr><td>Fellow Member</td>
     <td style='text-align:center;'>$m</td></tr>
    
     <tr style='background-color:#a5bde5;'><td>Total</td><td style='text-align:center;'>$Total</td></tr>
     </table></table>


     <p>As agreed in the previous AMC meetings, the requirements of CIIF standards and guidelines will apply. Additionally, for the time being, other qualifications not yet officially recognised by the CIIF may also be accepted before the CIIF professional exam and qualification is completely developed. The members who have been approved for admission will be endorsed by the CIIF Executive Committee (ExCo), a delegated authority agreed during the 6th CIIF Grand Council meeting held on 29th July 2016.</p><br><br><br>
        <p>3. JUSTIFICATIONS / RECOMMENDATION <br><br></p>
     <p>

i) The AMC is requested to review the list of candidates proposed for admission and approve the proposal by the CIIF management.
<br>
<br>

ii) Once the AMC accepts the proposal for admission of the members listed, then the CIIF management will prepare a paper on behalf of the AMC to seek the ExCo’s endorsement accordingly.</p>
    

     ";

     
  
$processing_fee .="
        </table><table  align='center' border='1' style='border-collapse: collapse; width: 50%;font-size: 12px;'>
     <tr>
     <td align='center' style='font-size:10px;'><b>Submitted By:</td>
     <td align='center' style='font-size:10px;'><b>Date:</td>
     </tr>

     <tr>
     <td>MOHD AIDZEEF AHMAD <br> SHARIFAH SAFWAH ALHABSHI</td>
     <td></td>
     </tr>

     <tr align='center' style='font-size:10px;'><td><b>Reviewed By:</td>
     <td><b>Date:</td></tr><tr>
     <td>DR AZURA OTHMAN <br> Chief Executive Officer </td>
     <td></td>
     </tr>
     
     </table>  <table style='page-break-before: always'></table><br><br><br><br><br><br><br><br>

    ";

    if($k!=0)
    {

     $processing_fee .= "<p align='center'>PROPOSED MEMBERSHIP CLASSIFICATION: <b>Student Member<b></p>
      <table  align='center' border='1' style='border-collapse: collapse;width: 80%;font-size: 12px;background-color:#00bcd3;'>

         <tr>
         <td align='center' style='font-size:10px;'> <b>NO<b></td>
            <td align='center' style='font-size:10px;'>
              <b> Name and Profile</b>
            </td>
            <td align='center' style='font-size:10px;'>
             <b>  Work Experience</b><br/>
              
            </td>
            <td align='center' style='font-size:10px;'>
              <b>Academic Qualification<br>(*=Completed)</b>
            </td>
            <td align='center' style='font-size:10px;'><b>Rationale</b></td>
         </tr>";


        $i=1;  
        foreach ($larrresult as $key) {
             if($key['program level'] == 'Student Member') {

        $name = $key['firstname'].' '.$key['lastname'];
        $gender = $key['Gender'];
        $dob=$key['birthdate'];
        $age = (date('Y') - date('Y',strtotime($dob)));

        $nationality = $key['nationality'];

        if ($key['nationality']=='MALAYSIA'){
            $country='MALAYSIAN';
        }
        else
        {
           $country='NON MALAYSIAN'; 
        }
        // $country=$key['residency'];

        $CompanyName= $key['Company Name'];
        $CompanyDes = $key['Company Designation'];
        $CompanyfromDate = date('m/d/Y',strtotime($key['fromDate']));
        $CompanytoDate = date('m/d/Y',strtotime($key['toDate']));
        $Qualification= $key['Quali'];
        $Qualificationyear = $key['Qyear'];
        $Qualificationuniversity = $key['QUniversity'];

        $Rationale = $key['rationale'];
        if($Rationale=='1> Year ') {
            $defin = "Have experience in Islamic Finance";
        } else{
            $defin = "Not have experience in Islamic Finance";
        }


           $processing_fee .="<tr>
         <td width='10%' align='center' style='font-size:8px;background-color:#fff;'>$i</td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$name <br> $age years old,$gender <br> Residency : $nationality <br> Nationality : $country </td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$CompanyName <br> $CompanyDes <br> $CompanyfromDate to $CompanytoDate</td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$Qualification <br> $Qualificationyear <br> $Qualificationuniversity </td>
         <td align='center' style='font-size:8px;background-color:#fff;'>$defin</td>
         </tr>";
         $i++;
        }
    }
}
     
if($j!=0)
    {
    $processing_fee .="</table><br><p align='center'>PROPOSED MEMBERSHIP CLASSIFICATION: <b>Associate  Member<b></p>


      <table  align='center' border='1' style='border-collapse: collapse;width: 80%;font-size: 12px;background-color:#00bcd3;'>

         <tr>
         <td align='center' style='font-size:10px;'> <b>NO<b></td>
            <td align='center' style='font-size:10px;'>
              <b> Name and Profile</b>
            </td>
            <td align='center' style='font-size:10px;'>
             <b>  Work Experience</b><br/>
              
            </td>
            <td align='center' style='font-size:10px;'>
              <b>Academic Qualification<br>(*=Completed)</b>
            </td>
            <td align='center' style='font-size:10px;'><b>Rationale</b></td>
         </tr>";


        $i=1;  
        foreach ($larrresult as $key) {
             if($key['program level'] == 'Associate  Member') {

        $name = $key['firstname'].' '.$key['lastname'];
        $gender = $key['Gender'];
        $dob=$key['birthdate'];
        $age = (date('Y') - date('Y',strtotime($dob)));
        $nationality = $key['nationality'];
        if ($key['nationality']=='MALAYSIA'){
            $country='MALAYSIAN';
        }
        else
        {
           $country='NON MALAYSIAN'; 
        }
        // $country=$key['residency'];
        $CompanyName= $key['Company Name'];
        $CompanyDes = $key['Company Designation'];
        $CompanyfromDate = date('m/d/Y',strtotime($key['fromDate']));
        $CompanytoDate = date('m/d/Y',strtotime($key['toDate']));
        $Qualification= $key['Quali'];
        $Qualificationyear = $key['Qyear'];
        $Qualificationuniversity = $key['QUniversity'];

        $Rationale = $key['rationale'];
        if($Rationale=='1> Year ') {
            $defin = "Have experience in Islamic Finance";
        } else{
            $defin = "Not have experience in Islamic Finance";
        }


           $processing_fee .="<tr>
         <td width='10%' align='center' style='font-size:8px;background-color:#fff;'>$i</td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$name <br> $age years old,$gender <br> Residency : $nationality <br> Nationality : $country </td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$CompanyName <br> $CompanyDes <br> $CompanyfromDate to $CompanytoDate</td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$Qualification <br> $Qualificationyear <br> $Qualificationuniversity </td>
         <td align='center' style='font-size:8px;background-color:#fff;'>$defin</td>
         </tr>";

         $i++;

        
        }

    }
}


if($l!=0)
    {
    $processing_fee .= "</table><br><p align='center'>PROPOSED MEMBERSHIP CLASSIFICATION: <b>Ordinary Member<b></p>
      <table  align='center' border='1' style='border-collapse: collapse;width: 80%;font-size: 12px;background-color:#00bcd3;'>

         <tr>
         <td align='center' style='font-size:10px;'> <b>NO<b></td>
            <td align='center' style='font-size:10px;'>
              <b> Name and Profile</b>
            </td>
            <td align='center' style='font-size:10px;'>
             <b>  Work Experience</b><br/>
              
            </td>
            <td align='center' style='font-size:10px;'>
              <b>Academic Qualification<br>(*=Completed)</b>
            </td>
            <td align='center' style='font-size:10px;'><b>Rationale</b></td>
         </tr>";


        $i=1;  
        foreach ($larrresult as $key) {
             if($key['program level'] == 'Ordinary Member') {

        $name = $key['firstname'].' '.$key['lastname'];
        $gender = $key['Gender'];
        $dob=$key['birthdate'];
        $age = (date('Y') - date('Y',strtotime($dob)));
        $nationality = $key['nationality'];

        if ($key['nationality']=='MALAYSIA'){
            $country='MALAYSIAN';
        }
        else
        {
           $country='NON MALAYSIAN'; 
        }

        $country=$key['residency'];
        $CompanyName= $key['Company Name'];
        $CompanyDes = $key['Company Designation'];
        $CompanyfromDate = date('m/d/Y',strtotime($key['fromDate']));
        $CompanytoDate = date('m/d/Y',strtotime($key['toDate']));
        $Qualification= $key['Quali'];
        $Qualificationyear = $key['Qyear'];
        $Qualificationuniversity = $key['QUniversity'];

        $Rationale = $key['rationale'];
        if($Rationale=='1> Year ') {
            $defin = "Have experience in Islamic Finance";
        } else{
            $defin = "Not have experience in Islamic Finance";
        }


           $processing_fee .="<tr>
         <td width='10%' align='center' style='font-size:8px;background-color:#fff;'>$i</td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$name <br> $age years old,$gender <br> Residency : $nationality <br> Nationality : $country </td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$CompanyName <br> $CompanyDes <br> $CompanyfromDate to $CompanytoDate</td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$Qualification <br> $Qualificationyear <br> $Qualificationuniversity </td>
         <td align='center' style='font-size:8px;background-color:#fff;'>$defin</td>
         </tr>";

         $i++;

        
        }

    }
}
    if($m!=0)
    {
        $processing_fee .= "</table><br>

    <p align='center'>PROPOSED MEMBERSHIP CLASSIFICATION: <b>Fellow Member<b></p>
      <table  align='center' border='1' style='border-collapse: collapse;width: 80%;font-size: 12px;background-color:#00bcd3;'>

         <tr>
         <td align='center' style='font-size:10px;'> <b>NO<b></td>
            <td align='center' style='font-size:10px;'>
              <b> Name and Profile</b>
            </td>
            <td align='center' style='font-size:10px;'>
             <b>  Work Experience</b><br/>
              
            </td>
            <td align='center' style='font-size:10px;'>
              <b>Academic Qualification<br>(*=Completed)</b>
            </td>
            <td align='center' style='font-size:10px;'><b>Rationale</b></td>
         </tr>";


        $i=1; 

        foreach ($larrresult as $key) {
             if($key['program level'] == 'Fellow Member') {

        $name = $key['firstname'].' '.$key['lastname'];
        $gender = $key['Gender'];
        $dob=$key['birthdate'];
        $age = (date('Y') - date('Y',strtotime($dob)));
        $nationality = $key['nationality'];
        if ($key['nationality']=='MALAYSIA'){
            $country='MALAYSIAN';
        }
        else
        {
           $country='NON MALAYSIAN'; 
        }
        // $country=$key['residency'];
        $CompanyName= $key['Company Name'];
        $CompanyDes = $key['Company Designation'];
        $CompanyfromDate = date('m/d/Y',strtotime($key['fromDate']));
        $CompanytoDate = date('m/d/Y',strtotime($key['toDate']));
        $Qualification= $key['Quali'];
        $Qualificationyear = $key['Qyear'];
        $Qualificationuniversity = $key['QUniversity'];

        $Rationale = $key['rationale'];
        if($Rationale=='1> Year ') {
            $defin = "Have experience in Islamic Finance";
        } else{
            $defin = "Not have experience in Islamic Finance";
        }


           $processing_fee .="<tr>
         <td width='10%' align='center' style='font-size:8px;background-color:#fff;'>$i</td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$name <br> $age years old,$gender <br> Residency : $nationality <br> Nationality : $country </td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$CompanyName <br> $CompanyDes <br> $CompanyfromDate to $CompanytoDate</td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$Qualification <br> $Qualificationyear <br> $Qualificationuniversity </td>
         <td align='center' style='font-size:8px;background-color:#fff;'>$defin</td>
         </tr>";

         $i++;

        
        }

    }
    }
    if($n!=0)
    {
        $processing_fee .= "</table><br>

    <p align='center'>PROPOSED MEMBERSHIP CLASSIFICATION: <b>Category2<b></p>
      <table  align='center' border='1' style='border-collapse: collapse;width: 80%;font-size: 12px;background-color:#00bcd3;'>

         <tr>
         <td align='center' style='font-size:10px;'> <b>NO<b></td>
            <td align='center' style='font-size:10px;'>
              <b> Name and Profile</b>
            </td>
            <td align='center' style='font-size:10px;'>
             <b>  Work Experience</b><br/>
              
            </td>
            <td align='center' style='font-size:10px;'>
              <b>Academic Qualification<br>(*=Completed)</b>
            </td>
            <td align='center' style='font-size:10px;'><b>Rationale</b></td>
         </tr>";


        $i=1; 

        foreach ($larrresult as $key) {
             if($key['program level'] == 'Category2') {

        $name = $key['firstname'].' '.$key['lastname'];
        $gender = $key['Gender'];
        $dob=$key['birthdate'];
        $age = (date('Y') - date('Y',strtotime($dob)));
        $nationality = $key['nationality'];
        if ($key['nationality']=='MALAYSIA'){
            $country='MALAYSIAN';
        }
        else
        {
           $country='NON MALAYSIAN'; 
        }
        // $country=$key['residency'];
        $CompanyName= $key['Company Name'];
        $CompanyDes = $key['Company Designation'];
        $CompanyfromDate = date('m/d/Y',strtotime($key['fromDate']));
        $CompanytoDate = date('m/d/Y',strtotime($key['toDate']));
        $Qualification= $key['Quali'];
        $Qualificationyear = $key['Qyear'];
        $Qualificationuniversity = $key['QUniversity'];

        $Rationale = $key['rationale'];
        if($Rationale=='1> Year ') {
            $defin = "Have experience in Islamic Finance";
        } else{
            $defin = "Not have experience in Islamic Finance";
        }


           $processing_fee .="<tr>
         <td width='10%' align='center' style='font-size:8px;background-color:#fff;'>$i</td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$name <br> $age years old,$gender <br> Residency : $nationality <br> Nationality : $country </td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$CompanyName <br> $CompanyDes <br> $CompanyfromDate to $CompanytoDate</td>
         <td width='25%' align='center' style='font-size:8px;background-color:#fff;'>$Qualification <br> $Qualificationyear <br> $Qualificationuniversity </td>
         <td align='center' style='font-size:8px;background-color:#fff;'>$defin</td>
         </tr>";

         $i++;

        
        }

    }
    }
        

      $processing_fee .="</table>

     
      
</body>
";

 $fieldValues['[Invoice]'] = $processing_fee;
        
        require_once '../library/dompdf/dompdf_config.inc.php';
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');
 $option = array(
            'content' => $processing_fee,
            'save' => false,
            'file_name' =>'report.pdf',
            'css' => '@page { margin: 20px 20px 20px 20px}
                        body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                        table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                        table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
                        table.tftable tr {background-color:#ffffff;}
                        table.tftable td {font-size:10px;padding: 5px;}
                        '
                        ,

            'header' => ''
        );
 $generate = new Cms_Common();
        $pdf = $generate->generatePdf($option);
        // $slip = Cms_Curl::__(SMS_API . '/get/Registration/do/registrationSlip', $params);

        echo $pdf;
        exit();
    }
}

