<?php
class Admin_CpdController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
		
        $this->userDb = new App_Model_User();

        $this->definationDB =new Admin_Model_DbTable_Definition();
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_MemberExperienceSearch(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view	
		$lobjemailTemplateModel = new Admin_Model_DbTable_MemberExperience();  // email template model object
		$larrresult = $lobjemailTemplateModel->getsearch(); // get template details	
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();						
				if ($lobjsearchform->isValid($larrformData)) {
				$post_data=$this->getRequest()->getPost();
           
           				 $id=$post_data['field2'];	
					$larrresult = $lobjemailTemplateModel->search($id);										
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
			$this->_redirect( $this->baseUrl . '/admin/MemberExperience/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function addAction() { 	

 $this->view->title = $this->view->translate("Add New CPD Material");

        $auth = Zend_Auth::getInstance();

        $form = new Admin_Form_CpdActivity();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //print_r($formData);exit;


            $activity = array();
            //$activity['spid'] = $spId;
            $activity['cpdm_type'] = $formData['cpdm_type'];
            $activity['cpdm_category'] = $formData['cpdm_category'];
            $activity['cpdm_activity'] = $formData['cpdm_activity'];
            $activity['cpdm_title'] = $formData['cpdm_title'];
            $activity['cpdm_venue'] = $formData['cpdm_venue'];
            $activity['cpdm_provider'] = $formData['cpdm_provider'];
            $activity['cpdm_desc'] = $formData['cpdm_desc'];
            $activity['cpdm_hours'] = $formData['cpdm_hours'];
            $activity['date_started'] = date("Y-m-d", strtotime($formData['date_started']));
            $activity['date_completion'] = date("Y-m-d", strtotime($formData['date_completion']));
            $activity['created_by'] = $auth->getIdentity()->iduser;
            $activity['created_date'] = date('Y-m-d H:i:s');
            $activity["cpdm_content"] = $formData['cpdm_content'];

           

            $cpdDB = new Admin_Model_DbTable_CpdMaterial();
            $cpdDB->addData($activity);
              $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $this->_redirect($this->view->url(array('module' => 'admin', 'controller' => 'cpd', 'action' => 'material-list'), 'default', true));

                }
            


          

        

	}

    public function materialListAction()
    {
        //title
        $this->view->title = "Continuing Profesional Development (CPD)";

        $materialDB = new Admin_Model_DbTable_CpdMaterial();
        $definationDB = new Admin_Model_DbTable_Definationms();

        $query = $materialDB->getMaterialList();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            $query = $materialDB->getMaterialList($formData);
            
             $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage(20);

        $this->view->paginator = $paginator;
        //$this->view->filter = $filter;

        $learningLists = $definationDB->getByCode('CPD Learning');
        $this->view->learningLists = $learningLists;

        $categoryLists = $definationDB->getByCode('CPD Category');
        $this->view->categoryLists = $categoryLists;
           
        }

        $filter = array(
            'name'      => null,
            'id-number' => null,
            'company'   => null,
            'migration' => null
        );

        if ($this->_getparam('search', 0) && isset($this->gobjsessionsis->studentprofilequery)) {
            $query = $this->gobjsessionsis->studentprofilequery;
            $filter = $this->gobjsessionsis->studentprofilefilter;
        } else {
            unset($this->gobjsessionsis->studentprofilequery);
            unset($this->gobjsessionsis->studentprofilefilter);
        }


        $paginator = Zend_Paginator::factory($query);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage(20);

        $this->view->paginator = $paginator;
        $this->view->filter = $filter;

        $learningLists = $definationDB->getByCode('CPD Learning');
        $this->view->learningLists = $learningLists;

        $categoryLists = $definationDB->getByCode('CPD Category');
        $this->view->categoryLists = $categoryLists;
    }

	public function editMaterialAction() {		
 		
 	 	
            $this->view->title = $this->view->translate("Edit CPD Material");

        $auth = Zend_Auth::getInstance();

        $mid = $this->_getParam('id', null);
        $this->view->mid = $mid;

        $materialDB = new Admin_Model_DbTable_CpdMaterial();
        $material = $materialDB->fetchRow('cpdm_id ='.$mid);
        $this->view->material = $material;
        $form = new Admin_Form_CpdActivity(array('mid' => $mid));
        $form->populate($material->toArray());
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $activity = array();
            $activity['cpdm_type'] = $formData['cpdm_type'];
            $activity['cpdm_category'] = $formData['cpdm_category'];
            $activity['cpdm_activity'] = $formData['cpdm_activity'];
            $activity['cpdm_title'] = $formData['cpdm_title'];
            $activity['cpdm_desc'] = $formData['cpdm_desc'];
            $activity['cpdm_hours'] = $formData['cpdm_hours'];
            $activity['created_by'] = $auth->getIdentity()->iduser;
            $activity['created_date'] = date('Y-m-d H:i:s');
            $activity["cpdm_content"] = $formData['cpdm_content'];

            // if ($formData['cpdm_content'] == '1637') {//if contetnt=video
            //     $activity["cpdm_url"] = $formData['cpdm_url'];
            // }

            $cpdDB = new Admin_Model_DbTable_CpdMaterial();
            $cpdDB->updateData($activity, $mid);

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
            $this->_redirect($this->view->url(array('module' => 'admin', 'controller' => 'cpd', 'action' => 'edit-material'), 'default', true) . '/id/' . $mid);
  	  	
}
}



public function getsubtypeAction()
{
	$this->_helper->layout->disableLayout();

	$memberId = $this->_getParam('member_id',null);

	$query =  new Admin_Model_DbTable_Member();

	$memberdata = $query->getmeberdata($memberId);

	echo $memberdata['sub_type'];

	 exit();
}

public function getLearningCategoryAction()
    {

        $learning_id = $this->_getParam('idLearning');

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        //$learning_id = 1570;

        $defaultActivityDB = new Admin_Model_DbTable_Cpd();
        $result = $defaultActivityDB->getLearningCategory($learning_id);
       
       

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

    public function getLearningActivityAction()
    {

        $learning_id = $this->_getParam('idLearning');
        $category_id = $this->_getParam('idCategory');

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $defaultActivityDB = new Admin_Model_DbTable_Cpd();
        $result = $defaultActivityDB->getLearningActivity($learning_id, $category_id);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

    public function approvalAction()
{

        $get_cdp  = new Admin_Model_DbTable_Cpd();

        $cpd_list =$get_cdp->getCheckMaterialList();
       
       $getStatus = $this->definationDB->getByCode('Member Pending');
       $this->view->approval_status = $getStatus;
      
        $this->view->users = $cpd_list;
        


}
public function detailsAction()
    {
        $id = $this->_getParam('id');
        
        $this->definationDB =new Admin_Model_DbTable_Definition();
        $form = new Admin_Form_CourseCategory();
        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();
               $getStatus = $this->definationDB->getByCode('Member Pending');
       $this->view->approval_status = $getStatus;

        
        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $userDocDb = new App_Model_UserDocuments();
        $userDocuments = $userDocDb->getUserDoc($id);


        $getMember= $this->userDb->getCpdList($id);

        


         $this->view->getMember = $getMember;
            

        $form = new App_Form_User();
        $form->populate($user);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $num = count($formData);
            echo "<pre>";
            $id = $formData['id'];
            $cpdStatus = $formData['cpd_status'];
            $cpdPoint = $formData['cpd_point'];
            for($i=0 ;$i<$num;$i++)
            {
            
                     echo '<pre>';
        print_r($cpdPoint[$i]);
}
            
       
        die();    
                
                            

             $get_mail = new Admin_Model_DbTable_Emailtemplate();
             $mail_detail = $get_mail->fnViewTemplte(26);
             $applicant_name = $user['firstname'].''.$user['lastname'];
             $applicant_email = $user['email'];

             if($get_doc_status)
        {
            

           $mail = new Cms_SendMail();
           $doclist ="<table>";

           foreach ($get_doc_status as $key => $value) {
                                  $doc = $value['doc_name'];   
                                  $doclist.="<tr>
                                    $doc
                                    </tr>";
                                 }
                                
                       $doclist .= "</table>";
           $message = 'Dear '.''.$applicant_name .$mail_detail['TemplateBody'].$doclist;

            
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
            $this->redirect('/admin/users/edit/id/'.$id);
           }
           
            
        }
        

          
    
           

        }
        $this->view->user = $user;
        $this->view->form = $form;

    }



}