<?php

class Admin_ReportController extends  Zend_Controller_Action
{
	protected $coursesDb;
	protected $contentBlockDb;
	protected $coursecatDb;
	protected $uploadDir;
	protected $uploadFolder;

	public function init()
	{
		
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'courses');

		$this->userDb = new App_Model_User();
		$this->coursesDb = new App_Model_Courses();
		$this->contentBlockDb = new App_Model_ContentBlock();
		$this->blockDataDb = new App_Model_ContentBlockData();
		$this->coursecatDb = new App_Model_CoursesCategory();
		$this->trackingDb = new App_Model_UserContentTracking();

		$this->uploadFolder = 'tracking-assets';
		$this->uploadDir = DOCUMENT_PATH. '/'.$this->uploadFolder;

	}

	public function indexAction()
	{
		$this->view->title = 'Tracking Report';


		$session  = new Zend_Session_Namespace('AdminTrackingIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/report/');
            }
            $formdata['search'] = trim($formdata['search']);
            $session->search = $formdata['search'];
            $this->redirect('/admin/report/index/search/1');
        }


        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $getcourses = $this->coursesDb->getCourses('title');

        if ($search) {
            $getcourses->where('title LIKE ? OR code LIKE ?', '%'.$formdata['search'].'%');
        }

		$courses = Cms_Paginator::query($getcourses);

		$this->view->results  = $courses;
		$this->view->formdata = $formdata;

	}

	public function viewAction()
	{
		$course_id = $id = $this->getParam('id');

		$course = $this->coursesDb->fetchRow(array("id = ?" => $course_id))->toArray();

        $course = $this->coursesDb->getCourse(array("id = ?" => $course_id));

        if ( empty($course)  )
        {
            return Cms_Render::error('Course doesn\'t exist.');
        }

		$user_id = $this->getParam('user', 0);
		$user    = $this->userDb->fetchRow(array('id = ?' => $user_id));

		if($user) {
			$user = $user->toArray();
		}

        //blocks
        $blocks = $this->contentBlockDb->getBlocks(array('course_id = ?' => $course['id']));

        //block content
        $content = $this->blockDataDb->getByBlock($course['id'], array('data_type', 'title'));
        
        // get tracking report
        foreach ($content as $val) {

        }

		$this->view->title = 'Tracking';
		$this->view->course_id = $id;
        $this->view->blocks = $blocks;
        $this->view->content = $content;
        $this->view->course  = $course;
		$this->view->user_id = $user_id;
		$this->view->user    = $user;

	}

	public function viewScormAction()
	{
		$id = $this->getParam('id', 0);	// course_id
		$data_id = $this->getParam('content'); // data_id

		$user_id = $this->getParam('user', 0);
		if($user_id) {
			$this->redirect("/admin/report/view-scorm-detail/id/$id/content/$data_id/user/$user_id");
		}

		$db = Zend_Db_Table::getDefaultAdapter();
		
		$onlineDb = new App_Model_UserOnline();
		$onlineusers = $onlineDb->getOnlineIds();

		$course = $this->coursesDb->fetchRow(array("id = ?" => $id))->toArray();
		$data_content = $this->blockDataDb->fetchAll(array("data_id = ?" => $data_id))->toArray();

		foreach ($data_content as $content_data) 
		{
			if ($content_data['data_type'] == 'scorm') 
			{
				$userContent = $db->select()
						->from(array('a' => 'user'))
						->joinInner(array('c' => 'user_scorm'), 'c.user_id = a.id', array('c.*'))
						->joinInner(array('e' => 'data_scorm'), 'e.id = c.data_id')
						->joinInner(array('d' => 'content_block_data'), 'e.data_id = d.data_id', array('d.data_id AS data_id'))
						->where('d.course_id = ?', $id)
						->where('d.data_id = ?', $data_id)
						->order('a.firstname');
			}
			else
			{
				$userContent = $db->select()
						->from(array('a' => 'user'))
						->joinLeft(array('c' => 'user_content_tracking'), 'c.user_id = a.id', array('c.*'))
						->joinLeft(array('d' => 'content_block_data'), 'd.data_id = c.data_id', array('d.data_id AS data_id'))
						->where('d.course_id = ?', $id)
						->where('c.data_id = ?', $data_id)
						->order('c.created_date')
						->order('a.firstname');
			}

			$result = $db->fetchAll($userContent);

			$i = 0;
			$user_list = array();

			foreach ($result as $r) 
			{
				if (!isset($user_list[$r['user_id']]))
				{
					$i = 0;
					$user_list[$r['user_id']] = $r;
					$user_list[$r['user_id']]['total'] = 1;
					$user_list[$r['user_id']]["first_access"] = $r["created_date"];
				} 
				else 
				{
					$user_list[$r['user_id']]['total']++;
					$user_list[$r['user_id']]["last_access"] = $r["created_date"];
				}
				$i++;
			}

			if ($this->getRequest()->isPost()) 
			{
				$formData = $this->getRequest()->getPost();
				$formData['search'] = trim($formData['search']);

				if ( $formData['search'] != '')
				{
					$userContent->where('username LIKE ? OR firstname LIKE ? OR lastname LIKE ? OR CONCAT_WS(" ",firstname,lastname) LIKE ?', '%'.$formData['search'].'%');
				}

				$this->view->formdata = $formData;
			}

			$tracking_data = Cms_Paginator::query($userContent);
		}

		$this->view->title = 'Content Tracking';
		$this->view->course = $course;
		$this->view->course_id = $id;
		$this->view->data_id = $data_id;
		$this->view->content = $data_content[0];
		$this->view->users = $user_list;
		$this->view->onlineusers = $onlineusers;
	}

	public function viewScormDetailAction()
	{
		$id = $this->getParam('id', 0);	// course_id
		$data_id = $this->getParam('content'); // data_id
		$user_id = $this->getParam('user'); // user_id

		$db = Zend_Db_Table::getDefaultAdapter();
		
		$onlineDb = new App_Model_UserOnline();
		$onlineusers = $onlineDb->getOnlineIds();

		$user_data = $this->userDb->fetchRow(array("id = ?" => $user_id))->toArray();

		$course = $this->coursesDb->fetchRow(array("id = ?" => $id))->toArray();
		$data_content = $this->blockDataDb->fetchAll(array("data_id = ?" => $data_id))->toArray();

		foreach ($data_content as $content_data) 
		{
			if ($content_data['data_type'] == 'scorm') 
			{
				$userContent = $db->select()
						->from(array('a' => 'scorm_api'))
						->joinInner(array('c' => 'user_scorm'), 'c.user_id = a.user_id AND c.data_id = a.scorm_id', array(
							'lastaccessed', 'duration', 'status'
						))
						->joinInner(array('e' => 'data_scorm'), 'e.id = a.scorm_id', array())
						->joinInner(array('d' => 'content_block_data'), 'd.data_id = e.data_id', array('d.data_id AS data_id'))
						->joinInner(array('f' => 'user'), 'f.id = a.user_id', array('firstname', 'lastname'))
						->where('a.user_id = ?', $user_id)
						// ->where('a.datakey LIKE ?', '%completion_status%')
						->where('d.data_id = ?', $data_id);

			}
			else
			{
				$userContent = $db->select()
						->from(array('a' => 'user'))
						->joinLeft(array('c' => 'user_content_tracking'), 'c.user_id = a.id', array('c.*'))
						->joinLeft(array('d' => 'content_block_data'), 'd.data_id = c.data_id', array('d.data_id AS data_id'))
						->where('a.id =?', $user_id)
						->where('d.course_id = ?', $id)
						->where('c.data_id = ?', $data_id)
						->order('c.created_date DESC');
			}

			$tracking_data = Cms_Paginator::query($userContent);
		}

		$result = $db->fetchAll($userContent);

		$interactions = $score = $status = $general = array();
		
		if ($content_data['data_type'] == 'scorm') 
		{
			// populate scorm details to arrays
			foreach($result as $res)
			{
				if (strpos($res['datakey'],"score") !== false)
				{
					$score[$res['datakey']] = $res['value'];
				}
				if (strpos($res['datakey'],"interactions") !== false)
				{
					$interactions[$res['datakey']] = $res['value'];
				}
				if (strpos($res['datakey'],"status") !== false)
				{
					$status[$res['datakey']] = $res['value'];
				}
				
				$general['lastaccessed'] = $res['lastaccessed'];
				$general['duration'] = $res['duration'];
				$general['status'] = $res['status'];
			}
		}

		elseif($this->getParam('nowin', 0))

		{
			$this->_helper->layout()->disableLayout();
		}

		// get tracking data
		$trackingDb = new App_Model_UserContentTracking();
        $tracking = $trackingDb->getTrackingByDataId($user_id, $data_id);

        $general = array_merge($general, $tracking);

		$interaction = array();
		foreach ($interactions as $key => $value) {
			$temp = explode('.', $key);
			$interaction[$temp[0]][$temp[1]][$temp[2]][$temp[3]] = $value;
		}


		// general data
		$this->view->title = 'Content Tracking Detail';
		$this->view->course_id = $id;
		$this->view->course = $course;

		// content data
		$this->view->data_id = $data_id;
		$this->view->content = $data_content[0];

		// user data
		$this->view->users = $tracking_data;
		$this->view->user_data = $user_data;
		$this->view->onlineusers = $onlineusers;

		// scorm details
		$this->view->score = $score;
		$this->view->status = $status;
		$this->view->general = $general;
		$this->view->interactions = $interaction;

		$this->view->nowin = $this->getParam('nowin', 0);

	}
}

