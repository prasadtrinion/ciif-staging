<?php
class Admin_MembershipQualificationController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
	$this->Model = new Admin_Model_DbTable_MembershipQualification();
	$this->auth = Zend_Auth::getInstance(); 

		
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_MembershipQualification(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view	
		$larrresult = $this->Model->fnGet(); // get template details	
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->getRequest()->isPost()) { // search operation
	
			$larrformData = $this->getRequest()->getPost();	
							
				if ($larrformData) {	
					
					$larrresult = $this->Model->fnSearch($larrformData['member_type']);

											
					if(empty($larrresult))
					{
						$this->_redirect( $this->baseUrl . '/admin/MembershipQualification/index');
					}				
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
			$this->_redirect( $this->baseUrl . '/admin/emailtemplate/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function addAction() { 	
  			$member_form = new Admin_Form_MembershipQualification();
            $this->view->form = $member_form;

            if($this->getRequest()->isPost())
            {    
        

                $formdata =$this->getRequest()->getPost();
                // echo "<pre>";
                // print_r($formdata);
                // die();
                
            if($member_form->isValid($formdata))
             {
               $data = array('member_type' => $formdata['member_type'],
                			'program_route' => $formdata['program_route'],
                			 'program_level' => $formdata['program_level'],
                			 'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
							 'created_by'    => $this->auth->getIdentity()->id,);
                   
                    
                    $add_member = new Admin_Model_DbTable_MembershipQualification();
                    $add_member->insert($data);
                    if($add_member)
                    {
                        $this->_redirect( $this->baseUrl .'/admin/MembershipQualification/index');
                    }
                    
                }


            }
	}

	public function editAction() {		
 		
 	 	
            $editform = new Admin_Form_MembershipQualification();
    		$this->view->form = $editform;
    		
		if($this->getRequest()->isPost())

		{

			$formData = $this->getRequest()->getPost();
			if($editform->isValid($formData))
		{

			$id = $this->getRequest()->getparam('Id');                                                                               //ine 4
			$data = array('member_type' => $formData['member_type'],
                			'program_route' => $formData['program_route'],
                			 'program_level' => $formData['program_level'],
                			 'updated_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
							 'updated_by'    => $this->auth->getIdentity()->id,);
                 
				$edit_member = new Admin_Model_DbTable_MembershipQualification();
				$edit_member->update($data, array('id_mq = ?' => $id));

                    if($edit_member)
                    {
                         $this->_redirect( $this->baseUrl . '/admin/MembershipQualification/index');
                    }

                  //Line 5
//$this->_helper->redirector('index');
			}

		else
		{
    		 $editform->populate($formData);
		}


		}
		 else
		              {
                        $id = $this->getRequest()->getparam('Id');
                        	
                    
                         $file = new Admin_Model_DbTable_MembershipQualification();
                         $files = $file->fetchRow('id_mq='.$id);
                         $editform->populate($files->toArray());
                        
                     }    
$this->view->form = $editform;  	  	
}
}