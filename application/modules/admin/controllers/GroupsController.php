<?php
class Admin_GroupsController extends  Zend_Controller_Action
{
	protected $groupsDb;


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'courses');

		$this->groupsDb = new App_Model_UserGroup();
		$this->groupsDataDb = new App_Model_UserGroupData();
		$this->groupsMappingDb = new App_Model_UserGroupMapping();
	}

	public function indexAction()
    {
        $this->view->title = 'Groups';

        $session = new Zend_Session_Namespace('AdminGroupsIndex');
        $session->setExpirationSeconds(60*5);
        $where   = array();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if(empty($formData['search'])) {
                unset($session->where);
                $this->redirect('/admin/groups/');
            }

            $where = array (
                'group_name' => $formData['search'],
                'group_desc' => $formData['search'],
            );
            $session->where = $where;
            $this->redirect('/admin/groups/index/search/1');
        }

        if(!$this->_getParam('search', 0)) {
            unset($session->where);
        }

        if(isset($session->where)) {
            $where = $session->where;
        }

        if (!empty($where)) {
            $cats = $this->groupsDb->getAllGroupQuery(false, $where);
            $this->view->formdata = array('search' => $where['group_name']);
        } else {
            $cats = $this->groupsDb->getAllGroupQuery(false);
        }
        $users = Cms_Paginator::query($cats);

        $this->view->results = $users;

    }

	public function addAction()
	{
		$form = new Admin_Form_UserGroup();
		$programDb = new App_Model_Courses();

		$this->view->title = "New Group";

		$cats = $this->groupsDb->fetchAll()->toArray();

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				// utk by program
				if (isset($formData['by_program']) && $formData['by_program'] == 1) {

					$program = $programDb->getCoursesByProgram($formData['programId']);
					//pr($formData['programId']);
					//pr($formData['select_courses_program']);
					//pr($program);

					if (!isset($formData['select_courses_program']) || !$formData['select_courses_program']) {
						foreach($program as $programs) {
							$data = array(
								'group_name'    => $programs['code'] . ' - ' . $formData['group_name'],
								'group_desc'	=> $formData['group_desc'],
								'by_program'	=> $formData['programId'],
								'course_id'		=> $programs['id'],
								'active'		=> $formData['active'],
								'created_date'	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
								'created_by'	=> $this->auth->getIdentity()->id
							);

							$course_id = $this->groupsDb->insert($data);
						}
					}
					else
					{
						foreach($formData['select_courses_program'] as $programs) {
							$course = $programDb->getCoursesTitle($programs);
							$data = array(
								'group_name'    => $course['code'] . ' - ' . $formData['group_name'],
								'group_desc'	=> $formData['group_desc'],
								'by_program'	=> $formData['programId'],
								'course_id'		=> $programs,
								'active'		=> $formData['active'],
								'created_date'	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
								'created_by'	=> $this->auth->getIdentity()->id
							);
							$course_id = $this->groupsDb->insert($data);
						}
					}

					Cms_Common::notify('success','Group successfully created');
					$this->redirect('/admin/groups/');
				}

				// utk by course
				else{
					$fail = 0;

					$data = array(
						'group_name'    => $formData['group_name'],
						'group_desc'	=> $formData['group_desc'],
						'by_program'	=> NULL,
						'course_id'		=> $formData['course_id'],
						'active'		=> $formData['active'],
						'created_date'	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'created_by'	=> $this->auth->getIdentity()->id
					);

					$course_id = $this->groupsDb->insert($data);

					Cms_Common::notify('success','Group successfully created');
					$this->redirect('/admin/groups/');
				}
			}
		}

		$this->view->form = $form;

		//get list program
		$programList = array();
		$programGroup = $programDb->getProgramGroup();
		$programList = $programGroup;
		foreach ($programGroup as $n => $progGroup) {

			$idProgram = $progGroup['id'];
			$programDetail = $programDb->getProgramByGroup($idProgram);
			if ($programDetail) {
				$programList[$n]['program'] = $programDetail;
			}
		}

		$this->view->program = $programList;
	}

	public function editAction()
	{
		$id = $this->_getParam('id');

		$cat = $this->groupsDb->fetchRow(array("group_id = ?" => $id))->toArray();

		if (empty($cat)) {
			throw new Exception('Invalid Group');
		}

		$form = new Admin_Form_UserGroup();
		$programDb = new App_Model_Courses();

		$this->view->title = "Edit Group";

		// get all courses for this program group
		if ($cat['by_program'] > 0) {
			$cat['courses'] = $programDb->getAllCourseByDesc($cat['by_program'], $cat['group_desc']);
		}

		//pr($cat);
		//populate
		$form->populate($cat);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				// utk by program
				if (isset($formData['by_program']) && $formData['by_program'] == 1) {

					$program = $programDb->getCoursesByProgram($formData['programId']);
					//pr($formData['programId']);
					//pr($formData['select_courses_program']);
					//pr($program);

					if (!isset($formData['select_courses_program']) || !$formData['select_courses_program']) {
						foreach($program as $programs) {
							$data = array(
								'group_name'    => $programs['code'] . ' - ' . $formData['group_name'],
								'group_desc'	=> $formData['group_desc'],
								'by_program'	=> 1,
								'course_id'		=> $programs['id'],
								'active'		=> $formData['active'],
								'created_date'	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
								'created_by'	=> $this->auth->getIdentity()->id
							);

							$course_id = $this->groupsDb->update($data, 'id=' . $id);
						}
					}
					else
					{
						foreach($formData['select_courses_program'] as $programs) {
							$course = $programDb->getCoursesTitle($programs);
							$data = array(
								'group_name'    => $course['code'] . ' - ' . $formData['group_name'],
								'group_desc'	=> $formData['group_desc'],
								//'by_program'	=> 1,
								'course_id'		=> $programs,
								'active'		=> $formData['active'],
								'created_date'	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
								'created_by'	=> $this->auth->getIdentity()->id
							);

							$course_id = $this->groupsDb->update($data, 'id=' . $id);
						}
					}
					//exit;
					Cms_Common::notify('success','Group successfully created');
					$this->redirect('/admin/groups/');
				}

				// utk by course
				else{
					$fail = 0;

					$data = array(
						'group_name'    => $formData['group_name'],
						'group_desc'	=> $formData['group_desc'],
						//'by_program'	=> NULL,
						'course_id'		=> $formData['course_id'],
						'active'		=> $formData['active'],
						'created_date'	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'created_by'	=> $this->auth->getIdentity()->id
					);

					$course_id = $this->groupsDb->insert($data);

					Cms_Common::notify('success','Group successfully edited');
					$this->redirect('/admin/groups/');
				}
			}
		}

		$this->view->form = $form;
		$this->view->cat = $cat;

		//get list program
		$programList = array();
		$programGroup = $programDb->getProgramGroup();
		$programList = $programGroup;
		foreach ($programGroup as $n => $progGroup) {

			$idProgram = $progGroup['id'];
			$programDetail = $programDb->getProgramByGroup($idProgram);
			if ($programDetail) {
				$programList[$n]['program'] = $programDetail;
			}
		}

		$this->view->program = $programList;
	}

	/*public function editAction()
	{
		$id = $this->_getParam('id');

		$cat = $this->groupsDb->fetchRow(array("group_id = ?" => $id))->toArray();

		if (empty($cat)) {
			throw new Exception('Invalid Group');
		}

		$form = new Admin_Form_UserGroup();

		$this->view->title = "Edit Group";

		//populate
		$form->populate($cat);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$fail = 0;

				$data = array(
					'group_name'    => $formData['group_name'],
					'group_desc'	=> $formData['group_desc'],
					'course_id'		=> $formData['course_id'],
					'active'		=> $formData['active'],
					'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'modified_by'   => $this->auth->getIdentity()->id
				);

				$this->groupsDb->update($data, array('group_id = ?' => $id));

				Cms_Common::notify('success','Group successfully edited');
				$this->redirect('/admin/groups/');

			}
		}

		$this->view->form = $form;
		$this->view->cat = $cat;
	}*/

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$cat = $this->groupsDb->fetchRow(array("group_id = ?" => $id))->toArray();

		if (empty($cat)) {
			throw new Exception('Invalid Group');
		}

		$this->groupsDataDb->delete(array('group_id = ?' => $id));
		$this->groupsMappingDb->delete(array('group_id = ?' => $id));
		$this->groupsDb->update(array('active' => 0), array('group_id = ?' => $id));

		Cms_Common::notify('success','Group and all associated tags successfully deleted');
		$this->redirect('/admin/groups/');
	}

	public function viewAction()
	{
		$id = $this->_getParam('id');

		$group = $this->groupsDb->getUserGroup($id);
		$groupDatas = $this->groupsDataDb->getAllUser($id, false);

		if (empty($group)) {
			throw new Exception('Invalid Group');
		}

		$form = new Admin_Form_UserGroupData();

		$this->view->title = "Add User to Group";

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ( $formData['search'] != '')
			{
				$groupDatas->where('d.firstname LIKE ? OR d.lastname LIKE ? OR d.email LIKE ?', '%'.$formData['search'].'%');
			}
		}

		$groupData = Cms_Paginator::query($groupDatas);

		// total number of users in group
		$db = Zend_Db_Table::getDefaultAdapter();
		$result_total = $db->fetchAll($groupDatas);
		$total_user = sizeof($result_total);

		$this->_helper->layout->disableLayout();
		
		$this->view->form = $form;
		$this->view->formdata = @$formData;
		$this->view->group_id = $id;
		$this->view->group = $group;
		$this->view->users = $groupData;
		$this->view->total_user = $total_user;
	}

	public function tagUserAction()
	{
		$id = $this->_getParam('id');

		$group = $this->groupsDb->getUserGroup($id);
		$groupDatas = $this->groupsDataDb->getAllUser($id, false);

		if (empty($group)) {
			throw new Exception('Invalid Group');
		}

		$form = new Admin_Form_UserGroupData();

		$this->view->title = "Add User to Group";

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ( $formData['search'] != '')
			{
				$groupDatas->where('d.firstname LIKE ? OR d.lastname LIKE ? OR d.email LIKE ?', '%'.$formData['search'].'%');
			}
		}

		$groupData = Cms_Paginator::query($groupDatas);

		// total number of users in group
		$db = Zend_Db_Table::getDefaultAdapter();
		$result_total = $db->fetchAll($groupDatas);
		$total_user = sizeof($result_total);

		$this->view->form = $form;
		$this->view->formdata = @$formData;
		$this->view->group_id = $id;
		$this->view->group = $group;
		$this->view->users = $groupData;
		$this->view->total_user = $total_user;
	}

	public function deleteUserTagAction() {
		$group_id = $this->_getParam('group');
		$user_id = $this->_getParam('id');

		$group = $this->groupsDb->getUserGroup($group_id);
		$groupUser = $this->groupsDataDb->getUser($user_id, $group_id);

		if (empty($group)) {
			throw new Exception('Invalid Group');
		}

		if (empty($groupUser)) {
			throw new Exception('No such user');
		}

		$this->groupsDataDb->removeUser($user_id, $group_id);

		Cms_Common::notify('success','User successfully removed');
		$this->redirect('/admin/groups/tag-user/id/'.$group_id);
	}

	public function getUserForGroupAction()
	{
		$id = $this->_getParam('id');
		$group = $this->groupsDb->getUserGroup($id);

		// get users enrollment
		$onlineDb = new App_Model_UserOnline();
		$onlineusers = $onlineDb->getOnlineIds();

		$userDb = new App_Model_User();
		$getusers = $userDb->getUserByEnrollment($group['course_id'], $group['group_id']);

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();
			$formData['search'] = trim($formData['search']);

			if ( $formData['search'] != '')
			{
				$getusers->where('username LIKE ? OR firstname LIKE ? OR lastname LIKE ? OR CONCAT_WS(" ",firstname,lastname) LIKE ?', '%'.$formData['search'].'%');
			}

			$this->view->formdata = $formData;

			if (!empty($formData['user_id'])) 
			{
				foreach ($formData['user_id'] as $key => $val) {
					$data = array(
							'group_id'		=> $id,
							'user_id'		=> $val,
							'active'		=> 1,
							'created_date'	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
							'created_by'	=> $this->auth->getIdentity()->id
						);

					$user_group_id = $this->groupsDataDb->insert($data);
				}
				Cms_Common::notify('success','User successfully added');
				$this->redirect('/admin/groups/tag-user/id/'.$group['group_id']);
			}
		}

		$users = Cms_Paginator::query($getusers);

		$this->view->group = $group;
		$this->view->onlineusers = $onlineusers;
		$this->view->users = $users;
	}

	public function createAction()
	{
		$enrolDb = new App_Model_Enrol();
		$userGroupDb = new App_Model_UserGroup();
		$userGroupDataDb = new App_Model_UserGroupData();
		$courseDb = new App_Model_Courses();

		$courses = $courseDb->getCourses('a.title', true);
		foreach ($courses as $course)
		{
			$learningMode = array ("OL" => "Online", "FTF" => "Face to Face");
			foreach ($learningMode as $id => $learnMode)
			{
				$data = array(
							'group_name'	=> $course['code'] . ' - ' . $learnMode,
							'group_desc'	=> $course['code'] . ' (' . $course['title'] . ') - ' . $learnMode,
							'course_id'		=> $course['id'],
							'active'		=> 1,
							'created_date'	=> new Zend_Db_Expr('NOW()'),
							'created_by'	=> 1
					);

				$group_id = $userGroupDb->insertGroup($data);

				echo $group_id . " - " . $course['code'] . ' - ' . $learnMode . ' created .. ';

				$userList = $enrolDb->getEnrols(array('a.course_id = ?' => $course['id'], 'learningmode = ?' => $id));

				$total = 0;
				foreach ($userList as $user)
				{
					$dataGroup = array(
									'group_id'		=> $group_id,
									'user_id'		=> $user['user_id'],
									'active'		=> 1,
									'created_date'	=> new Zend_Db_Expr('NOW()'),
									'created_by'	=> 1
					);

					$userGroupDataDb->insertGroupData($dataGroup);
					$total++;
				}

				echo $total  . " users<br>";
			}
			echo ' ------- ';
		}
		exit;
	}

	public function getCourseAction()
	{
		$idProgram = $this->_getParam('idProgram', 0);

		//if ($this->getRequest()->isXmlHttpRequest()) {
		$this->_helper->layout->disableLayout();
		//}

//		$ajaxContext = $this->_helper->getHelper('AjaxContext');
//		$ajaxContext->addActionContext('view', 'html');
//		$ajaxContext->initContext();

		$courseDb = new App_Model_Courses();
		if ($idProgram == 0)
		{
			// senaraikan semua course
			$courses = $courseDb->getCourses('a.title', true, array('id', 'title', 'code', 'code_safe'));
		}
		else
		{
			// bagi course yang progra yg diselect sahaja
			$courses = $courseDb->getCoursesByProgram($idProgram);
		}

//		$ajaxContext->addActionContext('view', 'html')
//			->addActionContext('form', 'html')
//			->addActionContext('process', 'json')
//			->initContext();

		$json = Zend_Json::encode($courses);

		echo $json;
		exit();
	}
}

