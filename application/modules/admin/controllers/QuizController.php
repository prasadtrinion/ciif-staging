<?php
class Admin_QuizController extends  Zend_Controller_Action
{
    protected $permResourcesDb;

    public function init()
    {
        $this->auth = Zend_Auth::getInstance();
        $this->quizDb = new App_Model_Quiz();
        $this->attemptquizDb = new App_Model_Quizattempt(); 
        $this->coursesDb = new App_Model_Courses();
    }

    public function indexAction()
    {
        
    } 

  public function newAction()
    {
        $bid = $this->_getParam('bid');
        $cid = $this->_getParam('cid');


        $this->view->title = 'New Quiz Question';

        $form = new Admin_Form_Quiz();

        $quizDb = new App_Model_Quiz();

        //form required
        $form->question->setRequired(true);
    
        //process post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

                
                    $data = array(
                        'course_id'   => $formData['course_id'],
                        'data_id'   => $bid,
                        'question'   => $formData['question'],
                        'answer1'   => $formData['answer1'],
                        'answer2'   => $formData['answer2'],
                        'answer3'   => $formData['answer3'],
                        'answer4'   => $formData['answer4'],
                        'correct_answer'   => $formData['correct_answer'],
                        'mark'   => $formData['mark'],
                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'   => $this->auth->getIdentity()->id,
                    );
                    
                    $quizDb->insert($data);      

                    Cms_Common::notify('success','Question successfully updated');
                    $this->redirect('/admin/quiz/view/bid/'.$bid.'/course/'.$cid);
            
        }

        $this->view->cid = $cid;
        $this->view->bid = $bid;
        $this->view->form = $form;
    }

    public function viewAction()
    {

        $bid = $this->_getParam('bid');
        $id = $this->_getParam('course');
        //courses
        $CoursesDb = new App_Model_Courses;
        $getcoursesdata = $CoursesDb->getCoursesTitle($id);

        $qcount = $this->quizDb->fetchAll(array("course_id = ?" => $id,"data_id = ?" => $bid))->toArray();
        //pr(@count($qcount));exit;


        
        //pr($totalcount);exit;

        $this->view->title = 'Quiz List';

            $quizDb = new App_Model_Quiz(); 
            $getquestion = $quizDb->getQuizques($id,$bid);

            if ($this->getRequest()->isPost()) {

                $formData = $this->getRequest()->getPost();
                $formData['search'] = trim($formData['search']);

                if ( $formData['search'] != '')
                {
                    $getquestion->where('a.title LIKE ?', '%'.$formData['search'].'%');
                }

                $this->view->formdata = $formData;
        }
        
            $courses = Cms_Paginator::query($getquestion);

            //question count
            $totalcount = @count($qcount);

        $this->view->bid = $bid;
        $this->view->results = $courses;
        $this->view->getcoursesdata = $getcoursesdata;
        $this->view->totalcount = $totalcount;
    }

    public function uploadQAction()
    {
        $bid = $this->_getParam('bid');
        $cid = $this->_getParam('cid');

        $questionUploadDB = new App_Model_QuestionUpload();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
                        //pr($formData);exit;

           $type = $formData['type'];

            $folder = '/qbank/upload/';
            $dir = DOCUMENT_PATH . $folder;

            if (!is_dir($dir)) {
                if (mkdir_p($dir) === false) {
                    throw new Exception('Cannot create attachment folder (' . $dir . ')');
                }
            }
            //upload file proses
            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Extension', false, array('extension' => 'xlsx', 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);

            // $files = Cms_Common::uploadFiles($dir, 'xlsx');
            $files = $adapter->getFileInfo();

            if ($files) {

                foreach ($files as $file => $info) {
                    if ($file) {

                        $fileOriName = $info['name'];
                        $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                        $filepath = $info['destination'] . '/' . $fileRename;

                        if ($adapter->isUploaded($file)) {
                            $adapter->addFilter('Rename', array('target' => $filepath, 'overwrite' => true), $file);

                            if ($adapter->receive($file) == 1) {

                                $updFile["question_type"] = $type;
                                $updFile["course_id"] = $cid;
                                $updFile["upl_filename"] = $fileOriName;
                                $updFile["upl_filerename"] = $fileRename;
                                $updFile["upl_filelocation"] = $folder . $fileRename;
                                $updFile["upl_filesize"] = $info['size'];
                                $updFile["upl_mimetype"] = $info['type'];
                                $updFile["upl_upddate"] = date('Y-m-d H:i:s');
                                $updFile["upl_upduser"] =  $this->auth->getIdentity()->id;

                                $uploadid = $questionUploadDB->addData($updFile);
                                
                                $this->readExcel($folder . $fileRename, $uploadid, $type, $cid,$bid);
                            }
                        }
                    }
                }
            }
             $this->_redirect('admin/quiz/view/bid/'.$bid.'/course/'.$cid);
            
        }



    }

     public function readExcel($filePath, $uplId, $type, $cid,$bid)
     {
        $questionUploadDB = new App_Model_QuestionUpload();
        $questionUploadDataDB = new App_Model_QuestionUploadData();
        $quizqbankDB = new App_Model_Quiz();
        $db = getDb();

        require_once('../library/PHPExcel/Classes/PHPExcel.php');

        $inputFileName = DOCUMENT_PATH . '/' . $filePath;

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                . '": ' . $e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        //  Loop through each row of the worksheet in turn
        $totalRow = 0;
        $totalUploaded = 0;

        for ($row = 2; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL, TRUE, FALSE);

            $rowData = array_map('array_filter', $rowData);
            $rowData = array_filter($rowData);
                //pr($rowData);exit;
       
            if ($rowData[0]) {

                $rowData = $rowData[0];

                $questioncode = isset($rowData[0]) ? $rowData[0] : $questioncode;
                $topicname = isset($rowData[1]) ? $rowData[1] : $topicname;
                $mark = isset($rowData[2]) ? $rowData[2] : $mark;
                $parentid = isset($rowData[0]) ? 0 : $parentid;

                $question = nl2br($rowData[3]);

                //qbs_question_upload_data

                $uplData['upl_id'] = $uplId;
                //$uplData['parent_id'] = $parentid;
                $uplData['question_code'] = $questioncode;
                $uplData['topic_name'] = $topicname;
                $uplData['mark'] = $mark;
                $uplData['question'] = $question;
                $uplData['course_id'] = $cid;
                $uplData['data_id'] = $bid;
                    // $uplData['A'] = nl2br($rowData[4]);
                    // $uplData['B'] = nl2br($rowData[5]);
                    // $uplData['C'] = nl2br($rowData[6]);
                    // $uplData['D'] = nl2br($rowData[7]);
                    $uplData['answer1'] = nl2br($rowData[4]);
                    $uplData['answer2'] = nl2br($rowData[5]);
                    $uplData['answer3'] = nl2br($rowData[6]);
                    $uplData['answer4'] = nl2br($rowData[7]);
                    $uplData['correct_answer'] = $rowData[8];


                $uplData["created_date"] = date('Y-m-d H:i:s');
                $uplData["created_by"] = $this->auth->getIdentity()->id;
               
                //$idParent = $questionUploadDataDB->addData($uplData);
                $idParent = $quizqbankDB->addData($uplData);
                // if ($idQuestionMain && $idQuestion) {
                //     $uplDataUpd['status'] = 1;
                //     $totalUploaded++;
                // } else {
                //     $uplDataUpd['status'] = 2;
                // }

                //update qbs_question_upload_data
                // $uplDataUpd['IdQuestionMain'] = isset($idQuestionMain) ? $idQuestionMain : 0;
                // $uplDataUpd['IdQuestion'] = isset($idQuestion) ? $idQuestion : 0;
                // $questionUploadDataDB->updateData($uplDataUpd, $idParent);


                $parentid = $idParent;
                $questioncode = $questioncode;
                $topicname = $topicname;
                $mark = $mark;

                $totalRow++;

            }
        }
        
                //$qbank = $quizqbankDB->getData($uplId);
                //pr($qbank);exit;

        $this->_helper->flashMessenger->addMessage(array('success' => $totalUploaded . " Questions successfully uploaded"));

        $db->update('quizqbs_question_upload', array('total_question' => $totalRow, 'total_uploaded' => $totalUploaded), array('id = ?' => $uplId));

        
    }

    public function editqbankAction()
    
    {
        $id = $this->getParam('id');
        $bid = $this->getParam('bid');


        $quizDb = new App_Model_Quiz(); 
        $course = $quizDb->fetchRow(array("id = ?" => $id))->toArray();
        //pr($course);exit;

        if ( empty($course) )
        {
            throw new Exception('Question doesnt exist');
        }

        $this->view->title = 'Edit Quiz Question';

        $this->view->course = $course;

        //form
        $form = new Admin_Form_Quiz();

        //populate
        $form->populate($course);

        //form required
        $form->question->setRequired(true);

        $groupDb = new App_Model_UserGroupMapping();
        $db = $groupDb->getDefaultAdapter();

        //process post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //pr($formData);exit;

                
                    $data = array(
                        'question'   => $formData['question'],
                        'answer1'   => $formData['answer1'],
                        'answer2'   => $formData['answer2'],
                        'answer3'   => $formData['answer3'],
                        'answer4'   => $formData['answer4'],
                        'correct_answer'   => $formData['correct_answer'],
                        'mark'   => $formData['mark'],
                        'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'modified_by'   => $this->auth->getIdentity()->id,
                    );
                    
                    $quizDb->update($data, array('id = ?'=> $id));

                    // // for groups
                    // $groupDb->deleteMappingByDataId($id, $formData['course_id'], 'quiz');

                    // foreach ($formData['group_id'] as $group) {
                    //     $data = array(
                    //                 'map_type'  => 'quiz',
                    //                 'group_id'  => $group,
                    //                 'course_id' => $formData['course_id'],
                    //                 'mapped_id' => $id
                    //         );

                    //     $groupDb->addMappingData($data);
                    // }
                    //pr($formData);exit;
                    //redirect
                    Cms_Common::notify('success','Question successfully updated');
                    //$this->redirect('/admin/quiz/editqbank/id/'.$id);
                    $this->redirect('/admin/quiz/view/bid/'.$bid.'/course/'.$course['course_id']);
            
        }

        // // get this content grouping
        // $groupdata = $db->select()
        //                     ->from(array('a' => 'user_group'))
        //                     ->joinLeft(array(
        //                             'c' => 'user_group_mapping'), 
        //                             '(a.group_id = c.group_id AND c.map_type = "quiz" AND c.mapped_id = "'.$id.'")', 
        //                             array('c.map_id'))
        //                     ->where('a.course_id = ?', $course['course_id'])
        //                     ->group('a.group_id')
        //                     ->order('a.group_name');

        // $groups = $db->fetchAll($groupdata);

        // $this->view->groups = $groups;
        // $this->view->wiki_id = $id;
        $this->view->cid = $course['course_id'];
        $this->view->bid = $bid;
        $this->view->form = $form;
        $this->view->qbank = $course;
    }

    public function deleteAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $id = $this->_getParam('id');
        $cid = $this->_getParam('cid');
        $bid = $this->getParam('bid');

        $quiz = $this->quizDb->fetchRow(array("id = ?" => $id))->toArray();

        if (empty($quiz)) {
            throw new Exception('Quiz doesnt exist');
        }

        //delete
        $this->quizDb->delete(array('id = ?' => $id));

        // // for groups
        // $groupDb = new App_Model_UserGroupMapping();
        // $db = $groupDb->getDefaultAdapter();
        
        // $groupDb->deleteMappingByDataId($id, $wiki['course_id'], 'wiki');

        Cms_Common::notify('success','Quiz question successfully deleted');
        $this->redirect('/admin/quiz/view/bid/'.$bid.'/course/'.$cid);
    }


    public function createattquizAction(){

        $bid = $this->_getParam('bid');
        $cid = $this->_getParam('cid');

            $quizanswerDb = new App_Model_Quizanswer();
            $quizattemptDb = new App_Model_Quizattempt();
            $quizDb = new App_Model_Quiz();

                    $data = array(
                                'user_id' => $this->auth->getIdentity()->id,
                                'course_id' => $cid,
                                'data_id' => $bid,
                                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by'   => $this->auth->getIdentity()->id,
                                'date_attempt' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                              ); 

                    $atid = $quizattemptDb->insert($data);

                $quizatt = $quizattemptDb->fetchAll(array("id = ?" => $atid))->toArray();
                

                //insert question
                $quiz = $quizDb->getranquest($cid,$bid);
                
                    foreach ($quiz as $key=>$value) {
                        $data = array(
                                'answer'   => NULL,
                                //'id_question' => substr($key,7,1),
                                'id_question' => $quiz[$key]['id'],
                                //'correct_answer' => $quiz[$key]['correct_answer'],
                                // 'quesnum' => substr($key,2,1),
                                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by'   => $this->auth->getIdentity()->id,
                                'attempt_id'   => $atid,

                              );    
                    //pr($data);exit;
                    $quizanswerDb->insert($data);
                    }

                $this->redirect('/admin/quiz/attemptquiz/bid/'.$bid.'/cid/'.$cid.'/atid/'.$atid);

    }
    
        public function attemptquizAction()
    {
        $this->view->title = ' Quiz 1';

        $bid = $this->_getParam('bid');
        $cid = $this->_getParam('cid');
        $atid = $this->_getParam('atid');

        $coursesDb     = new App_Model_Courses();
        $blockDataDb   = new App_Model_ContentBlockData();
        $quizDb        = new App_Model_Quiz();
        $quizanswerDb  = new App_Model_Quizanswer();
        $quizattemptDb = new App_Model_Quizattempt();

        $attempt =  $quizattemptDb->getAttempt($atid);

        if ($attempt['user_id'] != $this->auth->getIdentity()->id || $attempt['status'] == 1) {
            $this->_helper->flashMessenger->addMessage(array('error' => "You are not authorized"));
            $this->redirect("/courses/block/id/$bid/course/$cid");
        }
        
        $getquestion = $quizDb->getquescount($bid);
        $quiz        = $quizDb->getranquest($cid,$bid);
        $quizanswer  = $quizanswerDb->getquizresult($atid);

        $blockcontent = $blockDataDb->fetchRow(array('data_id = ?' => $bid))->toArray();
        $course       = $coursesDb->getCourse(array("a.id = ?" => $blockcontent['course_id']));
        

        $this->view->quiz = $quizanswer;
        $this->view->bid = $bid;
        $this->view->atid = $atid; 
        $this->view->countquestion = count($getquestion);
        $this->view->blockcontent = $blockcontent;
        $this->view->course = $course;    
               
    }

    public function sumattemptquizAction()
    {
        $this->view->title = ' Quiz 1';
        $atid = $this->_getParam('atid');

        $quizasnwerDb = new App_Model_Quizanswer();
        $quizanswer = $quizasnwerDb->getquizresult($atid);

        $quizDb = new App_Model_Quiz(); 
        $gettmark = $quizDb->getmarksum($quizanswer[0]['data_id']);

         $quizattemptDb = new App_Model_Quizattempt();

                $data = array(
                                'user_id' => $this->auth->getIdentity()->id,
                                'course_id' => $cid,
                                'data_id' => $bid,
                                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by'   => $this->auth->getIdentity()->id,
                                'date_attempt' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                              ); 

                 $quizattemptDb->update($data, array('id = ?'=> $atid));

        $this->view->gettmark = $gettmark['sum'];  
    }

     public function resultquizAction()
    {
        $this->view->title = ' Quiz 1';

        //$bid = $this->_getParam('bid');
        // $cid = $this->_getParam('cid');
        $atid = $this->_getParam('atid');

        $quizanswerDb = new App_Model_Quizanswer();
        $quizattemptDb = new App_Model_Quizattempt();
        $quizanswer = $quizanswerDb->getquizresult($atid);
        $bid = $quizanswer[0]['data_id'];
        //pr($quizanswer);exit;

        $blockDataDb  = new App_Model_ContentBlockData();
        $coursesDb    = new App_Model_Courses();
        $blockcontent = $blockDataDb->fetchRow(array('data_id = ?' => $bid))->toArray();
        $course       = $coursesDb->getCourse(array("a.id = ?" => $blockcontent['course_id']));
        $this->view->blockcontent = $blockcontent;
        $this->view->course = $course;

        $quizDb = new App_Model_Quiz(); 
        $getquestion = $quizDb->getquescount($quizanswer[0]['data_id']);
        $gettmark = $quizDb->getmarksum($quizanswer[0]['data_id']);
        $getpointsum = $quizDb->getpointsum($quizanswer[0]['data_id']);
        $gettotalcor = $quizanswerDb->gettotalcorrect($atid);
        //pr($gettotalcor);exit;
        $gettotalsum = $quizattemptDb->gettotalmark($atid);

        $attempt =  $quizattemptDb->getAttempt($atid);

        $this->view->gettmark = $gettmark['sum'];
        $this->view->getpointsum = $getpointsum['point'];
        $this->view->quiz = $quizanswer;
        $this->view->bid = $bid;
        $this->view->atid = $atid;
        $this->view->attempt = $attempt;  

        //calculate
        $point = count($gettotalcor)/count($getquestion) * $getpointsum['point'] ;
        $percent = count($gettotalcor)/count($getquestion) * 100 ;
        $this->view->point = $point;
        $this->view->percent = $percent;

        //update
        $updategrade = $quizattemptDb->updategrade($atid,$point,$percent);
        
    }

     public function sumpreattemptquizAction()
    {
        
        $bid = $this->_getParam('bid');

        $dataDb = new App_Model_DataContent();
        $contentdata = $dataDb->fetchRow(array('data_id = ?' => $bid ));

        $quizanswerDb = new App_Model_Quizanswer();

        $uid = $this->auth->getIdentity()->id ;
        $list = $this->attemptquizDb->getallAttempt($uid,$bid);

        $quizDb = new App_Model_Quiz(); 
        $gettmark = $quizDb->getmarksum($list[0]['data_id']);
        $getpointsum = $quizDb->getpointsum($bid);

        $blockDataDb  = new App_Model_ContentBlockData();
        $coursesDb    = new App_Model_Courses();
        $blockcontent = $blockDataDb->fetchRow(array('data_id = ?' => $bid))->toArray();
        $course       = $coursesDb->getCourse(array("a.id = ?" => $blockcontent['course_id']));
        $this->view->blockcontent = $blockcontent;
        $this->view->course = $course;

        //pr($list);exit;
        $this->view->allattempt = $list;
        $this->view->gettmark = $gettmark['sum'];
        $this->view->bid = $list[0]['data_id'] ;
        $this->view->cid = $list[0]['course_id'] ;
        $this->view->title = $contentdata['title'];
        $this->view->getpointsum = $getpointsum['point'];

    }

    public function resultsubmitAction()
    {
        $this->view->title = ' Quiz 1';

        $bid = $this->_getParam('bid');
        $cid = $this->_getParam('cid');
        $atid = $this->_getParam('atid');

        $quizanswerDb = new App_Model_Quizanswer();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            unset($formData['submit']);
            

            foreach ($formData as $key=>$value) {

                $canswer = $quizanswerDb->getqbankid(substr($key,0,11),$atid);

            $data = array(
                        'answer'   => $formData[$key],
                        //'id_question' => substr($key,7,1),
                        'id_question' => substr($key,0,11),
                        'correct_answer' => $canswer['correct_answer'],
                        'quesnum' => substr($key,2,1),
                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'   => $this->auth->getIdentity()->id,
                        'attempt_id'   => $atid,
                        'id'   => $canswer['qaid'],
                        'mark'   => $canswer['mark'],

                      );    
                      echo "<br>" . substr($key,0,11);
                      //echo "<br>" . substr($key,4,2);
                      //pr($data);exit;
            //$quizanswerDb->insert($data);
            ////$quizanswerDb->update('quiz_answer', $data, 'id_question = ' . substr($key,0,11));
            //pr($data);exit;
            $updateanswer = $quizanswerDb->updateanswer($data,substr($key,0,11),$this->auth->getIdentity()->id,$canswer['qaid']);
            }
            //update mark
            //$updatemark = $quizanswerDb->getlistanswer($atid);
            //pr($updatemark);exit;

            //update status
            $updatestatus = $this->attemptquizDb->updatestat($atid);

        }

            $this->redirect('/admin/quiz/resultquiz/atid/'.$atid);
        
    }

    public function gradingAction()
    {
        $this->_helper->layout()->setLayout('/admin');
        $id = $this->_getParam('id');
        $cid = $this->_getParam('cid');
        $course = $this->coursesDb->fetchRow(array('id = ?' => $id));
        $this->view->title = 'Grading List';

        $quizaDb = new App_Model_Quizattempt();
        $getstudent = $quizaDb->getCourses($id,$cid);
        $getnametitle = $quizaDb->getTitle($cid);
        $gettmark = $this->quizDb->getmarksum($cid);

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
            $formData['search'] = trim($formData['search']);

            if ( $formData['search'] != '')
            {
                $getstudent->where('firstname LIKE ? OR code LIKE ?', '%'.$formData['search'].'%');
            }

            $this->view->formdata = $formData;
        }

        $grading = Cms_Paginator::query($getstudent);
        
        $this->view->gettmark = $gettmark['sum'];
        $this->view->gradepoint = $getnametitle['gradepoint'];
        $this->view->nametitle = $getnametitle['title'];
        $this->view->title = $course['title'];
        $this->view->results = $grading;
        $this->view->cid = $cid;
        $this->view->id = $id;
    }


}