<?php
class Admin_ForumController extends  Zend_Controller_Action
{
	protected $currDb;


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'forum');

		$this->forumDb = new App_Model_Forum();
		$this->forumThreadDb = new App_Model_ForumThread();
		$this->forumReplyDb = new App_Model_ForumReply();
		$this->courseDb = new App_Model_Courses();
		$this->curcorDb = new App_Model_CurriculumCourses();
		$this->coursecatDb = new App_Model_CoursesCategory();

	}

	public function indexAction()
	{
		$this->view->title = 'All Forum';

		$results = $this->forumDb->getForums();

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();
			$formData['search'] = trim($formData['search']);

			if ( $formData['search'] != '')
			{
				$results->where('a.name LIKE ? OR f.thread_content LIKE ?', '%'.$formData['search'].'%');
			}

			$this->view->formdata = $formData;
		}

		$results = Zend_Paginator::factory($results);

		$this->view->results = $results;
	}

	public function addAction()
	{
		$form = new Admin_Form_Forum();

		$pid = $this->_getParam('pid');

		$this->view->title = "New Forum";

		//pid
		if ( !empty($pid) )
		{
			$form->parent_id->setValue($pid);
		}

		$forum = $this->forumDb->fetchAll()->toArray();
		foreach ( $this->forumDb->printTree($this->forumDb->buildTree($forum,0),0,0,true,1) as $opt )
		{
			//$form->parent_id->addMultiOption($opt['id'], $opt['name']);
		}

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{

				$dbv = Zend_Db_Table::getDefaultAdapter();
				$this->forumDb = new App_Model_Forum();

				$data = array(
						'name'			=> $formData['name'],
						'course_id'		=> $formData['course_id'],
						'category_id'	=> 0,
						'parent_id'		=> 0,
						'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'created_by'    => $this->auth->getIdentity()->id,
						'visibility'	=> 1
					);

				//$forum_id = $dbv->insert(array('a' => 'forum'),$data);
				$forum_id = $this->forumDb->insert($data);

				$dataThread = array(
						'forum_id'			=> $forum_id,
						'thread_subject'	=> $formData['name'],
						'thread_content'	=> $formData['description'],
						'reply_count'		=> 0,
						'created_by'		=> $this->auth->getIdentity()->id,
						'created_date'		=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'active'			=> 1
					);

				$forum_thread_id = $this->forumThreadDb->insert($dataThread);

				// add groups
				$groupDb = new App_Model_UserGroupMapping();
				$db = $groupDb->getDefaultAdapter();
				
				foreach ($formData['group_id'] as $group) {
					$data = array(
								'map_type'	=> 'forum',
								'group_id'	=> $group,
								'course_id' => $formData['course_id'],
								'mapped_id'	=> $forum_id
						);

					$groupDb->addMappingData($data);
				}

				Cms_Common::notify('success','Course forum successfully created');
				$this->redirect('/admin/forum/');

			}
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->_getParam('id');

		$this->view->title = 'Edit Forum';
		$this->view->forum_id = $id;

		$forum = $this->forumDb->getForum( array('id = ?' => $id));

		$forumThread = $this->forumThreadDb->getForumThreadById($id);
		$forum["message"] = $forumThread['thread_content'];

		if (empty($forum)) {
			throw new Exception('Invalid Forum');
		}

		$form = new Admin_Form_Forum();
		$form->populate( $forum );

		$forr = $this->forumDb->fetchAll()->toArray();

		// grouping		
		$groupDb = new App_Model_UserGroupMapping();
		$db = $groupDb->getDefaultAdapter();


		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$dbv = Zend_Db_Table::getDefaultAdapter();

				$data = array(
						'name'			=> $formData['name'],
						'course_id'		=> $formData['course_id'],
						'category_id'	=> 0,
						'parent_id'		=> 0,
						'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'modified_by'   => $this->auth->getIdentity()->id,
						'visibility'	=> 1
					);

				$dbv->update(array('a' => 'forum'), $data, array('id = ? ' => $formData['id']));

				$dataThread = array(
						'thread_subject'	=> $formData['name'],
						'thread_content'	=> $formData['description'],
						'reply_count'		=> 0,
						'modified_by'		=> $this->auth->getIdentity()->id,
						'modified_date'		=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'active'			=> 1
					);

				$forum_thread_id = $this->forumThreadDb->update($dataThread, array('forum_id' => $formData['id']));

				// for groups
				$groupDb->deleteMappingByDataId($formData['id'], $formData['course_id'], 'forum');

				foreach ($formData['group_id'] as $group) {
					$data = array(
								'map_type'	=> 'forum',
								'group_id'	=> $group,
								'course_id' => $formData['course_id'],
								'mapped_id'	=> $formData['id']
						);

					$groupDb->addMappingData($data);
				}

				Cms_Common::notify('success','forum successfully updated.');
				$this->redirect('/admin/forum/edit/id/'.$id);

			}
		}

		$this->view->forr = $forum;
		$this->view->form = $form;
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$forum = $this->forumDb->fetchRow(array("id = ?" => $id))->toArray();
		$forumThread = $this->forumThreadDb->fetchRow(array('forum_id = ?' => $id)); //->toArray();
		$forumThread_id = $forumThread['id'];

		if (empty($forum)) {
			throw new Exception('Invalid Forum');
		}

		//delete forum
		$this->forumDb->delete( array('id = ?' => $id) );

		//delete thread
		$this->forumThreadDb->delete(array('forum_id = ?' => $id));

		//delete replies
		$this->forumReplyDb->delete(array('thread_id = ?' => $forumThread_id));

		Cms_Common::notify('success','Forum successfully deleted');
		$this->redirect('/admin/forum/');
	}


}

