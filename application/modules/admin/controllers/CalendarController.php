<?php

class Admin_CalendarController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'calendar');

		$this->eventsDb = new App_Model_CalendarEvent(); 
	}

	public function indexAction()
	{
		$this->view->title = 'Events Calendar';

		$getevents = $this->eventsDb->getEvents();

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();
			$formData['search'] = trim($formData['search']);

			if ( $formData['search'] != '')
			{
				$getevents->where('a.title LIKE ? OR a.description LIKE ?', '%'.$formData['search'].'%');
			}

			$this->view->formdata = $formData;
		}

		$events = Cms_Paginator::query($getevents);


		$this->view->event = $events;
	}

	public function newAction()
	{
		$this->view->title = 'New Event';

		$form = new Admin_Form_Calendar();

		//form required
		$form->title->setRequired(true);
	
		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			if ($formData)
			{
				

					$data = array(
						'title'      	=> $formData['title'],
						'description'	=> $formData['description'],
						'course_id'     => 0,
						'date_start'	=> $formData['date_start'],
						'date_end'		=> $formData['date_end'],
						'time_start'	=> $formData['time_start'],
						'time_end'		=> $formData['time_end'],
						'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'created_by'    => $this->auth->getIdentity()->id,
					);

					$wiki_id = $this->eventsDb->insert($data);

					// add groups
					// $groupDb = new App_Model_UserGroupMapping();
					// $db = $groupDb->getDefaultAdapter();
					
					// foreach ($formData['group_id'] as $group) {
					// 	$data = array(
					// 				'map_type'	=> 'calendar',
					// 				'group_id'	=> $group,
					// 				'course_id' => $formData['course_id'],
					// 				'mapped_id'	=> $wiki_id
					// 		);

					// 	$groupDb->addMappingData($data);
					// }

					Cms_Common::notify('success','Event successfully created');
					$this->redirect('/admin/calendar/');
					
			}
		}
		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->getParam('id');

		$event = $this->eventsDb->fetchRow(array("id = ?" => $id))->toArray();

		if ( empty($event) )
		{
			throw new Exception('Event doesnt exist');
		}

		$this->view->title = 'Edit Event';

		$this->view->event = $event;

		//form
		$form = new Admin_Form_Calendar();
		$groupDb = new App_Model_UserGroupMapping();
		$db = $groupDb->getDefaultAdapter();

		//populate
		$form->populate($event);

		//form required
		$form->title->setRequired(true);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				
					$data = array(
						'title'      	=> $formData['title'],
						'description'	=> $formData['description'],
						'course_id'     => $formData['course_id'],
						'date_start'	=> $formData['date_start'],
						'date_end'		=> $formData['date_end'],
						'time_start'	=> $formData['time_start'],
						'time_end'		=> $formData['time_end'],
						'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'visibility'	=> 1,
						'modified_by'   => $this->auth->getIdentity()->id,
					);
					
					$this->eventsDb->update($data, array('id = ?'=> $id));

					// for groups
					$groupDb->deleteMappingByDataId($id, $formData['course_id'], 'calendar');

					foreach ($formData['group_id'] as $group) {
						$data = array(
									'map_type'	=> 'calendar',
									'group_id'	=> $group,
									'course_id' => $formData['course_id'],
									'mapped_id'	=> $id
							);

						$groupDb->addMappingData($data);


					}


					//redirect
					Cms_Common::notify('success','Event successfully updated');
					$this->redirect('/admin/calendar/edit/id/'.$id);
			
		}

		
		
		$this->view->form = $form;
		$this->view->id = $id;
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$events = $this->eventsDb->fetchRow(array("id = ?" => $id))->toArray();

		if (empty($events)) {
			throw new Exception('Event doesnt exist');
		}

		//delete
		$this->eventsDb->delete(array('id = ?' => $id));

		// for groups
		$groupDb = new App_Model_UserGroupMapping();
		$db = $groupDb->getDefaultAdapter();
		
		$groupDb->deleteMappingByDataId($id, $events['course_id'], 'calendar');

		Cms_Common::notify('success','Event successfully deleted');
		$this->redirect('/admin/calendar/');
	}

	public function getEventGroupAction() 
	{
		if ($this->getRequest()->isPost()) 
		{
			$formData = $this->getRequest()->getPost();

			// get groups for this course
			$groupDb = new App_Model_UserGroup();
			$db = $groupDb->getDefaultAdapter();

			$groupdata = $db->select()
								->from(array('a' => 'user_group'))
								->joinLeft(array(
									'c' => 'user_group_mapping'), 
									'(a.group_id = c.group_id AND c.map_type = "calendar" AND c.mapped_id = "' . @$formData['event_id'] . '")', 
									array('c.map_id'))
								->where('a.course_id = ?', $formData['course_id'])
								->group('a.group_id')
								->order('a.group_name');

			$groups = $db->fetchAll($groupdata);

			die(json_encode($groups));
		}
	}

}