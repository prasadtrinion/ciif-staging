<?php
class Admin_BulkAdmissionController extends Zend_Controller_Action
{
	protected $itemLog = array();

	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'calendar');

		$this->eventsDb = new App_Model_CalendarEvent(); 
		$this->locale = Zend_Registry::get('Zend_Locale');
		$this->bulkDB = new Admin_Model_DbTable_Bulk();
		$this->uploadDir = UPLOAD_PATH.'/bulk';

		require_once APPLICATION_PATH.'/../library/Others/parsecsv.lib.php';
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
		$common = new Cms_Common();
	}

	public function indexAction()
	{
	
		$this->view->title = "Bulk Admission";
		
		$p_data = $this->bulkDB->getPaginateData();

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage($this->gintPageCount);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
    	
	}

	public function uploadAction()
	{
		/*	
			problem/missing
			- Designation
			- program_scheme
			- Medium of Instruction
			- Proficiency in English -- kena filter by code
		*/
					$common = new Cms_Common();

		$this->view->title = "Bulk Admission - Upload";
		
		if ($this->_request->isPost() && $this->_request->getPost('save')) 
		{
			$larrformData = $this->_request->getPost();

			
			
			$total = 0;

			try 
			 {
				if ( !is_dir( $this->uploadDir ) )
				{
					if ( $common->mkdir_p($this->uploadDir) )
					{
						throw new Exception('Cannot create document folder ('.$this->uploadDir.')');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
				
			
				$files = $adapter->getFileInfo();
				
				$adapter->addValidator('NotExists', false, $this->uploadDir );
				$adapter->setDestination( $this->uploadDir );
				$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
				$adapter->addValidator('Size', false, array('min' => 100 , 'max' => 4194304 , 'bytestring' => true));

				foreach ($files as $no => $fileinfo) 
				{
					$adapter->addValidator('Extension', false, array('extension' => 'csv', 'case' => false));
				
					if ($adapter->isUploaded($no))
					{
						$ext =  $common->getext($fileinfo['name']);
						$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
						
						$fileUrl = $this->uploadDir.'/'.$fileName;

						$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
						$adapter->setOptions(array('useByteString' => false));
						
						 $size = $adapter->getFileSize($no);
					
						//die();
						if( !$adapter->receive($no))
						{
							$errmessage = array();
							if ( is_array($adapter->getMessages() ))
							{	
								foreach(  $adapter->getMessages() as $errtype => $errmsg )
								{
									$errmessage[] = $errmsg;
								}
								
								throw new Exception(implode('<br />',$errmessage));
							}
						}

						
						//process - CSV
						$csv = new parseCSV();

						$csv->auto($fileUrl);

						// echo "<pre>";
						// print_r($fileinfo);
						// die();
						
					
						$totalfields = count($csv->data[0]);
						
						// echo $totalfields;
						// die();
						
						if ($totalfields != 41 && $totalfields != 3 && $totalfields != 8)
						{
							$this->view->result =1;
						
							//throw new Zend_Exception('Invalid CSV format');
						}


						//save into db
						if ( !isset($this->bulk_id) )
						{
							$auth = Zend_Auth::getInstance(); 
							$data = array(
											'b_filename'		=> $fileinfo['name'],
											'b_fileurl'			=> '/bulk/'.$fileName,
											'b_status'			=> 'NEW',
											'b_totalapp'		=> count($csv->data),
											'b_created_by'		=> $auth->getIdentity()->id,
											'b_created_date'	=> date("Y-m-d H:i:s")
										);
							// echo "<pre>";
							// print_r($data);
							// die();
							
							$this->bulk_id = $this->bulkDB->addData($data);
						}

						

					} //isuploaded
					
				} //foreach
				
				//$this->bulkDB->updateData(array('b_totalapp' => $total), $this->bulk_id);
                
				$this->_redirect('/admin/bulk-admission/review/id/'.$this->bulk_id);	

			}
			catch (Exception $e) 
			{
				throw new Exception( $e->getMessage() );
			}

			

		} // if 
	}
	
	protected function getItemID($row)
	{
		// echo '<pre>';
		// print_r($row);
		// die();
			return $row['ARD_NAME'];
		
	}

	public function reviewAction()
	{
		$this->view->title = "Bulk Admission - Review";

		$id = $this->_getParam('id',null);

		// echo $id;
		// die();

		$bulkinfo = $this->bulkDB->getData($id);
		// print_r($bulkinfo);
		// die();

		if ( empty($bulkinfo) )
		{
			throw new Exception('Invalid Bulk ID');
		}
		
		$this->bulk_id = $bulkinfo['b_id'];
		$importedItems = $this->bulkDB->getItems($id);

		// echo "<pre>";
		// print_r($importedItems);
		// die();
		$importedBID = array();
		foreach ( $importedItems as $iitem ) 
		{
			$importedBID[] = $iitem['bi_bid'];
		}

		//parse CSV
		$csv = new parseCSV();
		$csv->auto(UPLOAD_PATH.$bulkinfo['b_fileurl']);
		
		//start
		$items = array();
		// echo "<pre>";
		// print_r($csv->data);
		// die();
		$fileds = count($csv->data[0]);
		// echo $fileds;
		// die();
		if($fileds==41)
		{
 			foreach ( $csv->data as $row_id => $row )
		 {
			$bid = $this->getItemID($row);

			$items[$bid] = array('BID' => $bid) + $row;

			$this->processItem(array('BID' => $bid) + $row);
		 }
		}
		elseif($fileds==3)
		{
			
				foreach ( $csv->data as $row_id => $row )
		 		{
					$bid = $this->getItemID($row);

					$items[$bid] = array('BID' => $bid) + $row;

					$this->ProcessSponser(array('BID' => $bid) + $row);
		 		}
		 		$this->_helper->flashMessenger->addMessage(array('success' => "Sponsership Details Imported"));

		}

		elseif($fileds==8)
		{
			
				foreach ( $csv->data as $row_id => $row )
		 		{
					$bid = $this->getItemID($row);

					$items[$bid] = array('BID' => $bid) + $row;

					$this->ProcessStudentSponser(array('BID' => $bid) + $row);
		 		}
		 		$this->_helper->flashMessenger->addMessage(array('success' => "Sponsership Student imported"));
		}
    
		//post
		if($fileds==41)
		{
		 if ($this->_request->isPost() && $this->_request->getPost('save')) 
		{
			$formData = $this->_request->getPost();
			$count = $formData['item'];
			if ( $count > 0 )
			{
				$total = 0;
				foreach ( $formData['item'] as $BID )
				{
					$this->processItem($items[$BID], 1);
					$total++;
				}
				
				$this->bulkDB->updateData(array('b_status' => 'IMPORTED', 'b_totaladded' => new Zend_Db_Expr('b_totaladded+'.$total)), $this->bulk_id);

				$this->_helper->flashMessenger->addMessage(array('success' => $total." Applicant imported"));
				$this->_redirect('/application/bulk-admission/review/id/'.$this->bulk_id);
			}
			else
			{
				//error
			}
		}
		
}
		
		//views
		$this->view->importedBID = $importedBID;
		$this->view->items = $items;
		$this->view->itemLog = $this->itemLog;
		
	}


	protected function logItem($bid, $field='', $msg='' )
	{
		$this->itemLog[$bid][] = array('field'	=> $field,
										'msg'	=> $msg);
	}

	protected function processItem($row, $insert=0)
	{
			// echo $row['DiscountAmount'];
			// die();



			// echo "<pre>";
			// print_r($row);
			// // //var_dump($row);
			//  die();

		//Models
		$branchDB = new GeneralSetup_Model_DbTable_Branchofficevenue();
		// $appProfileDB  = new App_Model_Application_DbTable_ApplicantProfile();
		// $appTransactionDB  = new App_Model_Application_DbTable_ApplicantTransaction();
		 $objdeftypeDB = new App_Model_Definitiontype();
		// $countryDB = new App_Model_General_DbTable_Country();
		// $entryReqDB= new App_Model_Application_DbTable_EntryRequirement();
		// $appQualificationDB  = new App_Model_Application_DbTable_ApplicantQualification();
		// $appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
		// $appVisaDB = new App_Model_Application_DbTable_ApplicantVisa();
		
		

		//BID
		$bid = $row['ARD_ID'];

		/* some remapping */
        $row['ARD_CITIZEN'] = $row['ARD_CITIZEN'] == 'MALAYSIAN' ? 'MALAYSIA':$row['ARD_CITIZEN'];
		$row['ARD_RACE'] = $row['ARD_RACE'] == 'MELAYU' ? 'Malay' : $row['ARD_RACE'];
		$row['DEGREE_ID'] = str_replace('/',' / ',$row['DEGREE_ID']);
		// $row['Proficiency in English'] = $row['Proficiency in English'] == 'MUET' ? 'Malaysian University English Test (MUET)' : $row['Proficiency in English'];

		/* process fields */
		foreach ( $row as $field => $value )
		{
			switch ($field)
			{
				case 'ARD_BRANCH_ID':
					$branch_code_id = (int)($row['ARD_BRANCH_ID']);
					
					$branch = $branchDB->getData($branch_code_id, 'BranchCode');
					// print_r($branch);
					// exit;
				 	$branch['IdBranch'];
				 	//die('here');
				
					if ( empty($branch ) ) $this->logItem($bid,$field, 'Empty');
				
				break;
				
				case 'Salutation':
					$salutation = $this->getIdByDef($row['Salutation'], $objdeftypeDB->fnGetDefinationsByLocale('Salutation'));
					if ( empty($salutation ) ) $this->logItem($bid,$field, 'Empty');
				break;

				// case 'First Name': 
				// case 'Last Name':
				// case 'Id Number':
				// case 'Dob':
				// case 'Email Id':
				// case 'Address (Permanent)':
				// case 'City (Permanent)':
				// case 'Postcode (Permanent)':
				// case 'Address (Correspondance)':
				// case 'City (Correspondance)':
				// case 'Postcode (Correspondance)':
				// case 'Fax':
				// case 'Moblie':
				// case 'Company Name':
				// case 'Company Address':
				// case 'Company Telephone Number':
				// case 'Company Fax Number':
				// case 'Designation':
				// case 'Duration of service (Start Date)':
				// case 'Duration of service (End Date)':
				// case 'Industry':
				// case 'Years in Industry (Working Experience)':
				// case 'Name of Sponsor':
				// case 'Sponsor Code':
				// case 'Scholarship Plan':
				// case 'Previous Student':
				// case 'Student ID':
				// case 'Gender':
				// case 'Diploma/Degree Awarded':
				// case 'Result/CGPA':
				// case 'Year Graduated':
				// case 'Medium of Instruction':
				// case 'English Language Proficiency  - Date Taken':
				// case 'Result for English proficiency':
				// case 'Malaysian Visa':

					if ( $value == '' ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'ARD_ID':
					$id_type = $this->getIdByDef($row['ARD_ID'],$objdeftypeDB->fnGetDefinations('ID Type'));

					if ( empty($id_type ) ) $this->logItem($bid,$field, 'Empty');
				break;

				
				case 'ARD_INTAKE':
					$intake_id = $this->getIntake($row['ARD_INTAKE']);
					if ( empty($intake_id) ) $this->logItem($bid,$field, 'Empty');
				break;
				
				case 'ARD_CITIZEN':
					$nationality = $this->getIdByDef($row['ARD_CITIZEN'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
					if ( empty($nationality) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'ARD_RELIGION':
					$religion = $this->getIdByDef($row['ARD_RELIGION'],$objdeftypeDB->fnGetDefinations('Religion'));
					if ( empty($religion) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'ARD_BIRTH_STATE':
					$perm_state = $this->getState($row['ARD_BIRTH_STATE']);
					if ( empty($perm_state) ) $this->logItem($bid,$field, 'Empty');
				break;
				
				case 'ARD_COUNTRY':
					$perm_country = $this->getIdByDef($row['ARD_COUNTRY'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
					if ( empty($perm_country) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'ARD_BIRTH_STATE':
					$corr_state = $this->getState($row['ARD_BIRTH_STATE']);
					if ( empty($corr_state) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'ARD_COUNTRY':
					$corr_country = $this->getIdByDef($row['ARD_COUNTRY'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
					if ( empty($corr_country) ) $this->logItem($bid,$field, 'Empty');
				break;

				// case 'Employment Status':
				// 	$employement_status = $this->getIdByDef($row['Employment Status'],$objdeftypeDB->fnGetDefinations('Employment Status'));
				// 	if ( empty($employement_status) ) $this->logItem($bid,$field, 'Empty');
				// break;

				// case 'Position':
				// 	$position_id = $this->getIdByDef($row['Position'],$objdeftypeDB->fnGetDefinations('Position Level'));
				// 	// echo $position_id;
				// 	// exit("Position");
				// 	if ( empty($position_id) ) $this->logItem($bid,$field, 'Empty');
				// break;
				
				// case 'Industry':
				// 	$industry = $this->getIdByDef($row['Industry'],$objdeftypeDB->fnGetDefinations('Industry Working Experience'));
				// 	if ( empty($position_id) ) $this->logItem($bid,$field, 'Empty');
				// break;

				case 'ARD_DISABILITY':
					$health_name = ucfirst(strtolower($row['ARD_DISABILITY']));

					$health_condition = $this->getIdByDef($health_name,$objdeftypeDB->fnGetDefinations('Health Condition'));
					// print_r($health_condition);
					// exit;

					if ( empty($health_condition) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'ARD_FINANCIAL_AID':
					$financial_support = $this->getIdByDef($row['ARD_FINANCIAL_AID'],$objdeftypeDB->fnGetDefinations('Funding Method'));
					// echo $financial_support."<br>";
					// exit('Financial Support');
					if ( empty($financial_support) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'DEGREE_DESC_ENG':

					$program =$this->getProgram($row['DEGREE_DESC_ENG']);

					
					if ( empty($program) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'PROG_MODE':
					$mode_study = $this->getIdByDef(str_replace('-',' ',$row['PROG_MODE']),$objdeftypeDB->fnGetDefinations('Mode of Study'));
					// print_r($mode_study);
					// exit;
					if ( empty($mode_study) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Mode of program':
					$mode_program = $this->getIdByDef($row['PROG_MODE'],$objdeftypeDB->fnGetDefinations('Mode of Program'));
					// print_r($mode_program);
					// exit;
					if ( empty($mode_program) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'Programme Type':
					$program_type = $this->getIdByDef($row['Mode of program'],$objdeftypeDB->fnGetDefinations('Program Type'));
					// print_r($program_type);
					// exit('Programme Type');
					if ( empty($program_type) ) $this->logItem($bid,$field, 'Empty');
				break;

				case 'ARD_RACE':
					$race_name = ucfirst(strtolower($row['ARD_RACE']));
					$race = $this->getIdByDef($race_name,$objdeftypeDB->fnGetDefinations('Race'));
					if ( empty($race) ) $this->logItem($bid,$field, 'Empty');
				break;
				
				case 'ARD_MARITAL':
					$marital_status = $this->getIdByDef($row['ARD_MARITAL'],$objdeftypeDB->fnGetDefinations('Marital Status'));
					if ( empty($race) ) $this->logItem($bid,$field, 'Empty');
				break;

 
				// case 'Highest Level':
				// //print_r($row['Highest Level']);
				// // /exit;
				// 	$highest_level = $this->getIdByDef($row['Highest Level'],$entryReqDB->getListGeneralReq(),array('k'=>'QualificationLevel','v'=>'IdQualification'));
				// 	//print_R($highest_level);
				// 	//exit;
				// 	if ( empty($highest_level) ) $this->logItem($bid,$field, 'Empty');
				// break;

				// case 'Name of University/College':
				// 	$name_of_university = $row['Name of University/College'];
				// 	if ( empty($name_of_university) ) $this->logItem($bid,$field, 'Empty');
				// break;

				// case 'Class Degree':
				// 	$class_degree = $this->getIdByDef($row['Class Degree'],$objdeftypeDB->fnGetDefinations('Class Degree'));
				// 	// print_r($class_degree);
				// 	// exit('Class Degree');
				// 	if ( empty($class_degree) ) $this->logItem($bid,$field, 'Empty');
				// break;

				// case 'Proficiency in English':
				// 	$proficiency_english = $this->getIdByDef($row['Proficiency in English'],$objdeftypeDB->fnGetDefinations('English Proficiency Test List'));
				// 	if ( empty($proficiency_english) ) $this->logItem($bid,$field, 'Empty');
				// break;

				// case 'Visa Status':
				// 	$visa_status = $this->getIdByDef($row['Visa Status'],$objdeftypeDB->fnGetDefinations('Visa Status'));
				// 	if ( empty($visa_status) ) $this->logItem($bid,$field, 'Empty');
				// break;
				//case Employee Kids Discount (Aky)
				case 'Employee_Kin(Y/N)':
					$empDiscountAmount= "";
					if(strtolower($row['Employee_Kin(Y/N)']) !=="y" )
					{
						$this->logItem($bid,$field,'Discount not Selected');
					}
					else
					{
						$EmployeeDiscountModel = new Studentfinance_Model_DbTable_Employeequery();
						$getEmpDiscountData = $EmployeeDiscountModel->getSalaryPercentage($row['Fathers_Salary']);
						if(empty($getEmpDiscountData))
						{
							$this->logItem($bid,$field,'Discount not available ');

						}	
						else
						{
							$this->empDiscountAmount = $getEmpDiscountData['discount'];
							
						}	
					}
				break;		

				// Sibling Discount (Aky)

				case 'SiblingDiscount(Y/N)':
					$SiblingDiscountAmount = "";

					if(strtolower($row['SiblingDiscount(Y/N)']) !== "y")
					{	
						$this->logItem($bid,$field,'Discount not Selected');
					}
					else
					{
						$SiblingDiscountModel = new Studentfinance_Model_DbTable_Siblingquey();
						$SiblingDiscountData = $SiblingDiscountModel->checkSiblingDiscount($row['No_of_Sibblings']); 
						if(empty($SiblingDiscountData))
						{
							$this->logItem($bid,$field,'No Discount available for NO of Sibling ');
						}
						else
						{
							$this->SiblingDiscountAmount = $SiblingDiscountData['discount'];
						}
					}

				break;

				// Group Discount (Aky)
				case 'Group_discount(Y/N)':
					$GroupDiscountAmount = "";	
					if(strtolower($row['Group_discount(Y/N)']) !== "y")
					{
						$this->logItem($bid,$field,'Group Discount not Selected');
					}
					else
					{
						$GroupDiscountModel = new Studentfinance_Model_DbTable_Groupdiscountquey();
						$GroupDiscountData = $GroupDiscountModel->search($row['GroupName']);
						if(empty($GroupDiscountData))
						{
							$this->logItem($bid,$field,'No Discount available for GROUP ');
						}
						else
						{
							$this->GroupDiscountAmount = $GroupDiscountData['discount_amount'];
						}
					}
				break;

				// Scholarship Discount (Aky)
				case 'Scholarship Discount':
					$ScholarshipDiscountAmount = "";	
					if(strtolower($row['Scholarship Discount']) !== "y")
					{
						$this->logItem($bid,$field,'Scholarship Discount not Selected');
					}
					else
					{
						$this->ScholarshipDiscountAmount = 790;
						// $ScholarshipModel = new Studentfinance_Model_DbTable_Groupdiscountquey();
						// $ScholarshipDiscountData = $ScholarshipDiscountModel->search($row['Grade']);
						// if(empty($ScholarshipDiscountData))
						// {
						// 	$this->logItem($bid,$field,'No Discount available for GROUP ');
						// }
						// else
						// {
						// 	$this->ScholarshipDiscountAmount = $ScholarshipDiscountData['discount_amount'];
						// }
					}
				break;


				// case 'DiscountAmount':
				// case 'DiscountStatus':

			}
		}
		
// print_r($program);
// exit;
	// echo 	$row['Email Id'];
	// exit;


		if ( $insert == 0 )
		{
			return false;
		}
		
		if ( $row['ARD_NAME'])
		{
			//applicant_profile
			$appl_info['appl_fname'] = $row['ARD_NAME'];
			//$appl_info['appl_lname'] = $row['ARD_NAME'];
			$appl_info["branch_id"] = $row['ARD_BRANCH_ID'];            
			//$appl_info["appl_email"]= $row['Email Id'];
			$appl_info["appl_password"]= md5(123456);			
			$appl_info["appl_role"]=0;
			$appl_info["create_date"]=$row['ARD_CREATE_DATE'];
			$appl_info['appl_dob']=date('Y-m-d',strtotime($row['ARD_DOB']));
			//$appl_info['appl_salutation']= $salutation;
			$appl_info['appl_idnumber'] = $row['ARD_IC'];
			//$appl_info['appl_idnumber_type'] = $id_type;
			$appl_info['appl_nationality'] = $row['ARD_CITIZEN'];
			$appl_info['appl_religion'] = $row['ARD_RACE'];
			//$appl_info['appl_address1'] = $row['Address (Permanent)'];
			
			$appl_info['appl_city'] = 99;
			//$appl_info['appl_city_others'] = $row['City (Permanent)'];

			if ( empty($perm_state) && $row['ARD_BIRTH_STATE'] != '' )
			{
				$appl_info['appl_state'] = 99;
				$appl_info['appl_state_others'] = $row['ARD_BIRTH_STATE'];
			}
			else
			{
				$appl_info['appl_state'] = $perm_state;
			}

			$appl_info['appl_country'] = $row['ARD_COUNTRY'];
			//$appl_info['appl_postcode'] = $row['Postcode (Permanent)'];
			
			//$appl_info['appl_caddress1'] = $row['Address (Correspondance)'];
			
            $objdeftypeDB= new App_Model_Definitiontype();
			$defresult = $objdeftypeDB->fnGetDefinations('Student Category');

			// echo "<pre>";
			// print_r($defresult);
			// die();

			if ( $row['ARD_RACE']== 121 || $row['ARD_RACE']==101)
			{

				// echo('here in local');
				// echo $defresult[0]['idDefinition'];
				// die();
				$appl_info['appl_category']=$defresult[0]['idDefinition'];
				//$appl_info['appl_ccity_others'] = $row['City (Correspondance)'];
			}
			else
			{
				// die('here in Intenational');
				$appl_info['appl_category']=$defresult[1]['idDefinition'];
			}

			// if ( empty($corr_state) && $row['ARD_BIRTH_STATE'] != '' )
			// {
			// 	$appl_info['appl_cstate'] = 99;
			// 	$appl_info['appl_cstate_others'] =(int) $row['ARD_BIRTH_STATE'];
			// }
			// else
			// {
			// 	$appl_info['appl_cstate'] = $corr_state;
			// }

			$appl_info['appl_ccountry'] = $row['ARD_COUNTRY'];
			$appl_info['appl_phone_home'] = 00000;
			$appl_info['appl_fax'] = 00000;
			$appl_info['appl_phone_mobile'] =00000;
			$appl_info['prev_student'] = $row['ARD_STUD_ID'] == TRUE ? 1 : 0;
			$appl_info['prev_studentID'] = $row['ARD_STUD_ID'];
			$appl_info['appl_race'] = $row['ARD_RACE'];
			$appl_info['appl_gender'] = $row['SEX'] == 'Female' ? 2 : 1;
			$appl_info['appl_marital_status'] = $row['ARD_MARITAL'];


			// New field (Introducer) (Aky)
			$appl_info['appl_introducer'] = NULL;
			$appl_info['appl_first_preference'] = NULL;
			$appl_info['appl_second_preference'] = NULL;
			// echo "<pre>";
			// print_r($appl_info);
			// die();
			$applicant_id = $appProfileDB->addData($appl_info);
			//print_r($applicant_id);
			
			//generate applicant ID
			$gen_applicantId =  $this->generateApplicantID();

			//applicant_transaction
			if(!isset($info2['at_appl_type'])) {
					$info2['at_appl_type'] = 2;
			}

			if(!isset($info2['at_period'])) {
					$info2['at_period'] = 0;
			}
			if(!isset($info2['upd_date'])) {
					$info2['upd_date'] = date('Y-m-d');
			}
			if(!isset($info2['at_reason'])) {
					$info2['at_reason'] = 0;
			}
			

			$info2["at_appl_id"]=$applicant_id;
			$info2["at_pes_id"]=$gen_applicantId;
			$info2["at_create_by"]=$row['ARD_CREATE_USER'];

			$info2["at_status"]=591; //offered
			$info2["at_create_date"]=$row['ARD_CREATE_DATE'];
			$info2["entry_type"]=$row['ARD_SOURCE_DATA'];			
			$info2["at_default_qlevel"]=null;	
			$info2["at_intake"] = $intake_id;
		
		  
			//trans ID		    					
			$at_trans_id = $appTransactionDB->addData($info2);
			
			//program
			
			$info3['mode_study'] = $program['mode_of_study'];
			$info3['program_mode'] = $program['mode_of_program'];
			$info3['program_type'] = $row['PROG_MODE'];
			$info3['ap_prog_scheme'] = $program['IdProgramScheme'];
			$info3["ap_at_trans_id"]=$at_trans_id;
			$info3["ap_prog_code"]=$program["ProgramCode"];
			$info3["ap_prog_id"]=$program['IdProgram'];	

		    // print_r($info3);
		    // exit;
			
			$appProgramDB  = new App_Model_Application_DbTable_ApplicantProgram();			
			$appProgramDB->addData($info3);
			
			//qualification
			$data6 = array(	    		   
				'ae_appl_id' => $applicant_id,
				'ae_transaction_id' => $at_trans_id,
				'ae_qualification' => NULL,
				'ae_degree_awarded' => NULL,
				'ae_class_degree' => NULL,
				'ae_result' => NULL,
				'ae_year_graduate' => NULL,
				'ae_institution_country' => '0',
				'ae_institution' => 999,
				'ae_medium_instruction' => NULL,
				'others' => NULL,	    		  	    		
				'createDate' => date("Y-m-d H:i:s")
			);

			$ae_id = $appQualificationDB->addData($data6);

			// //english
			$data4 = array(    			 
				'ep_transaction_id' => $at_trans_id,
				'ep_test' => NULL,
				'ep_test_detail' =>NULL,
				'ep_date_taken' => NULL,
				'ep_score' => NULL,
				'ep_updatedt' => date("Y-m-d H:i:s"),
				'ep_updateby'=>$applicant_id
			);
			
			$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
			$ep_id=$appEngProfDB->addData($data4);
			
		
			//employment info
			$data8 = array(	    	
				'ae_appl_id' => $applicant_id,
				'ae_trans_id' => $at_trans_id,	 
				'ae_status' => 0,
				'ae_comp_name' => NULL,
				'ae_comp_address' => NULL,
				'ae_comp_phone' => NULL,
				'ae_comp_fax' => NULL,
				'ae_designation' => NULL,
				'ae_position' => $position_id,
				'ae_from' => NULL,
				'ae_to' => NULL,   		  
				'emply_year_service' => '0',
				'ae_industry' => NULL, 
				'ae_job_desc' => '',	    		   	    		
				'upd_date' => date("Y-m-d H:i:s")
			);
							
			$appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
			$ae_id = $appEmploymentDB->addData($data8);
			
			//health condition
			$appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
			$data9 = array(    				   
				'ah_appl_id' => $applicant_id,
				'ah_trans_id' => $at_trans_id,
				'ah_status' => $health_condition,
				'upd_date' => date("Y-m-d H:i:s")
			);
						
			$appHealthDB->addData($data9);

			//financial support
			$appFinancial = new App_Model_Application_DbTable_ApplicantFinancial();

			$data10 = array(
				'af_appl_id' => $applicant_id,
				'af_trans_id' => $at_trans_id,
				'af_method' => 0,
				'af_sponsor_name' => NULL,
				'af_type_scholarship' => '0',
				'af_scholarship_secured' => NULL,
				'af_scholarship_apply' => '0',
				'upd_date' => date("Y-m-d H:i:s")
			);

			$af_id = $appFinancial->addData($data10);

			//visa
			$data11 = array(
				'av_appl_id' => $applicant_id,
				'av_trans_id' => $at_trans_id,
				'av_malaysian_visa' =>0,
				'av_status' => NULL,
				'av_expiry' => date('Y-m-d',strtotime($row['Visa Expiry Date'])),
				'upd_date' => date("Y-m-d H:i:s")
			);
			
			$appVisaDB->insert($data11);

		
			
			// Employee Discount (Aky)
			
			// if($this->empDiscountAmount != '')
			// {

			// 	$discountModel = new App_Model_Application_DbTable_ApplicantDiscount();
			// 	$data12 = array(
			// 				'dcnt_appl_id' =>$applicant_id ,
			// 				'dcnt_txn_id' =>$at_trans_id ,
			// 				'dcnt_type_id' =>191 ,
			// 				//'dcnt_fomulir_id' =>$row['DiscountFomularID'] ,
			// 				'dcnt_amount' =>$this->empDiscountAmount, 
			// 				'dcnt_IdStudentRegistration' =>0,
			// 				'dcnt_status' => 0, 
			// 				);
              
			// 	$discount_data = $discountModel->addData($data12);

			// } else {
			// 	$discountModel = new App_Model_Application_DbTable_ApplicantDiscount();
			// 	$data12 = array(
			// 				'dcnt_appl_id' =>$applicant_id ,
			// 				'dcnt_txn_id' =>$at_trans_id ,
			// 				'dcnt_type_id' =>191 ,
			// 				//'dcnt_fomulir_id' =>$row['DiscountFomularID'] ,
			// 				'dcnt_amount' =>0, 
			// 				'dcnt_IdStudentRegistration' =>0,
			// 				'dcnt_status' => 0, 
			// 				);
              
			// 	$discount_data = $discountModel->addData($data12);
			// }
			


			// // Sibling  Discount (Aky)

			// if($this->SiblingDiscountAmount)
			// {
			// 	$discountModel = new App_Model_Application_DbTable_ApplicantDiscount();
			// 	$data13 = array(
			// 				'dcnt_appl_id' =>$applicant_id ,
			// 				'dcnt_txn_id' =>$at_trans_id ,
			// 				'dcnt_type_id' =>193 ,
			// 				//'dcnt_fomulir_id' =>$row['DiscountFomularID'] ,
			// 				'dcnt_IdStudentRegistration' =>0,
			// 				'dcnt_amount' =>$this->SiblingDiscountAmount, 
			// 				'dcnt_status' => 0, 
			// 				);
			// 	$discount_data = $discountModel->addData($data13);

			// } else {
			// 	$discountModel = new App_Model_Application_DbTable_ApplicantDiscount();
			// 	$data13 = array(
			// 				'dcnt_appl_id' =>$applicant_id ,
			// 				'dcnt_txn_id' =>$at_trans_id ,
			// 				'dcnt_type_id' =>193 ,
			// 				//'dcnt_fomulir_id' =>$row['DiscountFomularID'] ,
			// 				'dcnt_IdStudentRegistration' =>0,
			// 				'dcnt_amount' =>0, 
			// 				'dcnt_status' => 0, 
			// 				);
			// 	$discount_data = $discountModel->addData($data13);
			// }
			

			// // Group Discount (Aky)
			// if($this->GroupDiscountAmount != "" )
			// {
			// 	$discountModel = new App_Model_Application_DbTable_ApplicantDiscount();
			// 	$data14 = array(
			// 				'dcnt_appl_id' =>$applicant_id ,
			// 				'dcnt_txn_id' =>$at_trans_id ,
			// 				'dcnt_type_id' =>192 ,
			// 				//'dcnt_fomulir_id' =>$row['DiscountFomularID'] ,
			// 				'dcnt_IdStudentRegistration' =>0,
			// 				'dcnt_amount' =>$this->GroupDiscountAmount , 
			// 				'dcnt_status' => 0, 
			// 				);
			// 	$discount_data = $discountModel->addData($data14);
			// } else {
			// 	$discountModel = new App_Model_Application_DbTable_ApplicantDiscount();
			// 	$data14 = array(
			// 				'dcnt_appl_id' =>$applicant_id ,
			// 				'dcnt_txn_id' =>$at_trans_id ,
			// 				'dcnt_type_id' =>192 ,
			// 				//'dcnt_fomulir_id' =>$row['DiscountFomularID'] ,
			// 				'dcnt_IdStudentRegistration' =>0,
			// 				'dcnt_amount' =>0 , 
			// 				'dcnt_status' => 0, 
			// 				);
			// 	$discount_data = $discountModel->addData($data14);
			// }



			// // Scholarship Discount (Aky)

			// if($this->ScholarshipDiscountAmount != "" )
			// {
			// 	$discountModel = new App_Model_Application_DbTable_ApplicantDiscount();
			// 	$data15 = array(
			// 				'dcnt_appl_id' =>$applicant_id ,
			// 				'dcnt_txn_id' =>$at_trans_id ,
			// 				'dcnt_type_id' =>195 ,
			// 				//'dcnt_fomulir_id' =>$row['DiscountFomularID'] ,
			// 				'dcnt_IdStudentRegistration' =>0,
			// 				//'dcnt_amount' =>$this->ScholarshipDiscountAmount ,
			// 				'dcnt_amount' =>$this->ScholarshipDiscountAmount, 
			// 				'dcnt_status' => 0, 
			// 				);
			// 	$discount_data = $discountModel->addData($data15);

			// }
			// else
			// {
			// 	$discountModel = new App_Model_Application_DbTable_ApplicantDiscount();
			// 	$data15 = array(
			// 				'dcnt_appl_id' =>$applicant_id ,
			// 				'dcnt_txn_id' =>$at_trans_id ,
			// 				'dcnt_type_id' =>195 ,
			// 				//'dcnt_fomulir_id' =>$row['DiscountFomularID'] ,
			// 				'dcnt_IdStudentRegistration' =>0,
			// 				'dcnt_amount' =>0 , 
			// 				'dcnt_status' => 0, 
			// 				);
			// 	$discount_data = $discountModel->addData($data15);

			// }
			




			//item
			$data = array(
							'b_id'				=> $this->bulk_id,
							'bi_trans_id'		=> $at_trans_id,
							'bi_appl_id'		=> $applicant_id,
							'bi_bid'			=> $bid
						);
			
			$bulk_id = $this->bulkDB->addItem($data);
		}
	}



	protected function ProcessSponser($row, $insert=0)
	{

		// die('sponser');
		//Models
		$sponser= new GeneralSetup_Model_DbTable_Sponsor();
		
		if ( $row['SPON_ID'])
		{
			
			$sponser_info['SponsorCode'] = $row['CODE'];
			$sponser_info["fName"] = $row['SPON_NAME'];
			            					
			$sponser_info= $sponser->addData($sponser_info);
		}
	}

protected function ProcessStudentSponser($row, $insert=0)
	{
	
		$sponser_student= new GeneralSetup_Model_DbTable_sponsortag();
		if ( $row['FS101SPONSOR_ID'])
		{
		
			$sponser_tag['IdSponsorTag'] = $row['FS101SPONSOR_ID'];
			$sponser_tag['Sponsor'] = $row['FS101CMPYCODE'];
			$sponser_tag["StudentId"] = $row['FS101STUD_ID'];
			$sponser_tag["StartDate"] = $row['FS101FROM_DATE'];
			$sponser_tag["EndDate"] = $row['FS101TO_DATE'];
			$sponser_tag["AggrementStatus"] = $row['FS101AGG_STATUS']; 
			$sponser_tag["AggrementNo"] = $row['FS101AGG_NO'];
			$sponser_tag["Amount"] = $row['FS101SPONSOR_AMT'];

			$sponser_details= $sponser_student->addData($sponser_tag);
						
		}
	}


	function backupprocess()
	{
		/*
		Array
		(
			[CP ID] => INCEIF-HQ
			[Salutation] => Ms
			[First Name] => AZILA
			[Last Name] => Ali
			[Id type] => MyKad
			[Id Number] => 850412-02-5812
			[Dob] => 04/12/1985
			[Intake Session] => 2014 June Semester
			[Nationality] => MALAYSIAN
			[Religion] => ISLAM
			[Email Id] => nnadianadzri@gmail.com
			[Address (Permanent)] => 37,JALAN BOLA LISUT, 13/17 SEKSYEN 13
			[City (Permanent)] => SHAH ALAM
			[State (Permanent)] => SELANGOR
			[Country (Permanent)] => Malaysia
			[Postcode (Permanent)] => 40100
			[Address (Correspondance)] => 
			[City (Correspondance)] => 
			[State (Correspondance)] => 
			[Country (Correspondance)] => 
			[Postcode (Correspondance)] => +60122733090
			[Fax] => 
			[Moblie] => +60123589936
			[Employment Status] => Self-employed
			[Company Name] => NADS MEDIA PRODUCTION SDN BHD
			[Company Address] => 2-01-2 PRESINT ALAMI, PERSIARAN AKUATIK SEKSYEN 13
			[Company Telephone Number] => +60355108236
			[Company Fax Number] => 
			[Designation] => EXECUTIVE PRODUCER
			[Position] => Officer
			[Duration of service (Start Date)] => 
			[Duration of service (End Date)] => 
			[Industry] => Marketing/Sales
			[Industry Working Experience] => 
			[Years in Industry (Working Experience)] => 
			[Health condition] => NORMAL
			[Financial Support] => Self-funding
			[Name of Sponsor] => 
			[Sponsor Code] => 
			[Scholarship Plan] => 
			[Previous Student] => yes
			[Student ID] => 1200133
			[Program] => Masters in Islamic Finance Practice
			[Mode of study] => Full-time
			[Mode of program] => Online
			[Programme Type] => 
			[Race] => MELAYU
			[Gender] => Female
			[Age] => 
			[Marital Status] => Married
			[Highest Level] => Bachelor
			[Name of University/College] => UNIVERSITI KEBANGSAAN MALAYSIA
			[Diploma/Degree Awarded] => BACHELOR OF SCIENCE (STATISTICS)
			[Class Degree] => 2nd Class Lower/Division 2
			[Result/CGPA] => 2.65
			[Year Graduated] => 2003
			[Medium of Instruction] => 
			[Proficiency in English] => MUET
			[English Language Proficiency  - Date Taken] => 
			[Result for English proficiency] => 
			[Malaysian Visa] => Yes
			[Visa Status] => Student
			[Visa Expiry Date] => 
		)*/

		// $branch = $branchDB->getData($row['CP ID'], 'BranchCode');
		// 	$branchID = $branch['IdBranch'];
		// 	$row['Nationality'] = $row['Nationality'] == 'MALAYSIAN' ? 'MALAYSIA':$row['Nationality'];

		// 	//applicant_profile
		// 	$appl_info['appl_fname'] = $row['First Name'];
		// 	$appl_info['appl_lname'] = $row['Last Name'];
		// 	$appl_info["branch_id"] = $branchID;            
		// 	$appl_info["appl_email"]= $formData['Email Id'];
		// 	$appl_info["appl_password"]= '';				
		// 	$appl_info["appl_role"]=0;
		// 	$appl_info["create_date"]=date("Y-m-d H:i:s");
		// 	$appl_info['appl_dob']=date('Y-m-d',strtotime($row['Dob']));
		// 	$appl_info['appl_salutation']= $this->getIdByDef($row['Salutation'], $objdeftypeDB->fnGetDefinationsByLocale('Salutation'));
		// 	$appl_info['appl_idnumber'] = $row['Id Number'];
		// 	$appl_info['appl_idnumber_type'] = $this->getIdByDef($row['Id type'],$objdeftypeDB->fnGetDefinations('ID Type'));
		// 	$appl_info['appl_nationality'] = $this->getIdByDef($row['Nationality'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
		// 	$appl_info['appl_religion'] = $this->getIdByDef($row['Religion'],$objdeftypeDB->fnGetDefinations('Religion'));
		// 	$appl_info['appl_address1'] = $row['Address'];
		// 	$appl_info['appl_city'] = $row['City'];
		// 	$appl_info['appl_state'] = $this->getState($row['State']);
		// 	$appl_info['appl_country'] = $this->getIdByDef($row['Country'],$countryDB->getData(),array('k'=>'CountryName','v'=>'idCountry'));
		// 	$appl_info['appl_postcode'] = $row['Postcode'];
		// 	$appl_info['appl_phone_home'] = $row['Contact No.'];
		// 	$appl_info['appl_fax'] = $row['Fax'];
		// 	$appl_info['appl_phone_mobile'] = $row['Moblie'];
		// 	$appl_info['prev_student'] = $row['Previous Student'] == 'yes' ? 1 : 0;
		// 	$appl_info['prev_studentID'] = $row['Student ID'];
		// 	$appl_info['appl_race'] = $this->getIdByDef($row['Race'],$objdeftypeDB->fnGetDefinations('Race'));
		// 	$appl_info['appl_gender'] = $row['Gender'] == 'Female' ? 2 : 1;
		// 	$appl_info['appl_marital_status'] = $this->getIdByDef($row['Marital Status'],$objdeftypeDB->fnGetDefinations('Marital Status'));
		// 	//

		// 	$applicant_id = $appProfileDB->addData($appl_info);
			
		// 	//generate applicant ID
		// 	$gen_applicantId =  $this->generateApplicantID();

		// 	//applicant_transaction
		// 	$info2["at_appl_id"]=$applicant_id;
		// 	$info2["at_pes_id"]=$gen_applicantId;
		// 	$info2["at_create_by"]=$applicant_id;
		// 	$info2["at_status"]=593; //offered
		// 	$info2["at_create_date"]=date("Y-m-d H:i:s");
		// 	$info2["entry_type"]=0;//manual			
		// 	$info2["at_default_qlevel"]=null;	
		// 	$info2["at_intake"] = $this->getIntake($row['ARD_INTAKE]']);
		
		
		// 	//trans ID		    					
		// 	$at_trans_id = $appTransactionDB->addData($info2);
			
		// 	//program
			
		// 	$program =$this->getProgram($row["Program"]);

			
		// 	$info3['mode_study'] = $this->getIdByDef(str_replace('-','',$row['Mode of study']),$objdeftypeDB->fnGetDefinations('Mode of Study'));
		// 	$info3['program_mode'] = $this->getIdByDef($row['Mode of program'],$objdeftypeDB->fnGetDefinations('Mode of Program'));
		// 	$info3['program_type'] = '0';
		// 	$info3['ap_prog_scheme'] = '0';
		// 	$info3["ap_at_trans_id"]=$at_trans_id;
		// 	$info3["ap_prog_code"]=$program["ProgramCode"];
		// 	$info3["ap_prog_id"]=$program['IdProgram'];		
			
		// 	$appProgramDB  = new App_Model_Application_DbTable_ApplicantProgram();			
		// 	$appProgramDB->addData($info3);
			
		// 	//qualification
		// 	$data6 = array(	    		   
		// 		'ae_appl_id' => $applicant_id,
		// 		'ae_transaction_id' => $at_trans_id,
		// 		'ae_qualification' => $this->getIdByDef($row['Highest Level'],$entryReqDB->getListGeneralReq(),array('k'=>'QualificationLevel','v'=>'IdQualification')),
		// 		'ae_degree_awarded' => $row['Diploma/Degree Awarded'],
		// 		'ae_class_degree' => $this->getIdByDef($row['Class Degree'],$objdeftypeDB->fnGetDefinations('Class Degree')),
		// 		'ae_result' => $row['Result/CGPA'],
		// 		'ae_year_graduate' => $row['Year Graduated'],
		// 		'ae_institution_country' => '0',
		// 		'ae_institution' => '0',
		// 		'ae_medium_instruction' => '0',	
		// 		'others' => '0',	    		  	    		
		// 		'createDate' => date("Y-m-d H:i:s")
		// 	);



		// 	$ae_id = $appQualificationDB->addData($data6);

		// 	//english
		// 	$data4 = array(    			 
		// 		'ep_transaction_id' => $at_trans_id,
		// 		'ep_test' => $this->getIdByDef($row['Proficiency in English'],$objdeftypeDB->fnGetDefinations('English Proficiency Test List')),
		// 		'ep_test_detail' => $formData['english_test_detail'],
		// 		'ep_date_taken' => date('Y-m-d',strtotime($formData['English Language Proficiency - Date Taken'])),
		// 		'ep_score' => $formData['Result for English proficiency'] == '' ? 0 : $formData['Result for English proficiency'],
		// 		'ep_updatedt' => date("Y-m-d H:i:s"),
		// 		'ep_updateby'=>$applicant_id
		// 	);
			
		// 	$appEngProfDB = new App_Model_Application_DbTable_ApplicantEnglishProficiency();
		// 	$ep_id=$appEngProfDB->addData($data4);
			
		
		// 	//employment info
		// 	$data8 = array(	    	
		// 		'ae_appl_id' => $applicant_id,
		// 		'ae_trans_id' => $at_trans_id,	 
		// 		'ae_status' => $this->getIdByDef($row['Employment Status'],$objdeftypeDB->fnGetDefinations('Employment Status')),
		// 		'ae_comp_name' => $row['Company Name'],
		// 		'ae_comp_address' => $row['Company Address'],
		// 		'ae_comp_phone' => $row['Company Telephone Number'],
		// 		'ae_comp_fax' => $row['Company Fax Number'],
		// 		'ae_designation' => $row['Designation'],
		// 		'ae_position' => (int)$formData['Position'],
		// 		'ae_from' => date('Y-m-d',strtotime($row['Duration of service (Start Date)'])),
		// 		'ae_to' => date('Y-m-d',strtotime($row['Duration of service (End Date)'])),	    		  
		// 		'emply_year_service' => '0',
		// 		'ae_industry' => $this->getIdByDef($row['Industry'],$objdeftypeDB->fnGetDefinations('Industry Working Experience')),	 
		// 		'ae_job_desc' => '',	    		   	    		
		// 		'upd_date' => date("Y-m-d H:i:s")
		// 	);
							
		// 	$appEmploymentDB = new App_Model_Application_DbTable_ApplicantEmployment();
		// 	$ae_id = $appEmploymentDB->addData($data8);
			
		// 	//health condition
		// 	$appHealthDB = new App_Model_Application_DbTable_ApplicantHealthCondition();
		// 	$data9 = array(    				   
		// 		'ah_appl_id' => $applicant_id,
		// 		'ah_trans_id' => $at_trans_id,
		// 		'ah_status' => $this->getIdByDef($row['Health condition'],$objdeftypeDB->fnGetDefinations('Industry Working Experience')),
		// 		'upd_date' => date("Y-m-d H:i:s")
		// 	);
						
		// 	$appHealthDB->addData($data9);

		// 	//financial support
		// 	$appFinancial = new App_Model_Application_DbTable_ApplicantFinancial();

		// 	$data10 = array(
		// 		'af_appl_id' => $applicant_id,
		// 		'af_trans_id' => $at_trans_id,
		// 		'af_method' => $this->getIdByDef($row['Financial Support'],$objdeftypeDB->fnGetDefinations('Funding Method')),
		// 		'af_sponsor_name' => $row['Name of Sponsor'],
		// 		'af_type_scholarship' => '0',
		// 		'af_scholarship_secured' => $row['Scholarship Plan'],
		// 		'af_scholarship_apply' => '0',
		// 		'upd_date' => date("Y-m-d H:i:s")
		// 	);

		// 	$af_id = $appFinancial->addData($data10);

		// 	//visa
		// 	$data11 = array(
		// 		'av_appl_id' => $applicant_id,
		// 		'av_trans_id' => $at_trans_id,
		// 		'av_malaysian_visa' => $row['Malaysian Visa'] == 'Yes' ? 1 : 0,
		// 		'av_status' => $this->getIdByDef($row['Visa Status'],$objdeftypeDB->fnGetDefinations('Visa Status')),
		// 		'av_expiry' => date('Y-m-d',strtotime($row['Visa Expiry Date'])),
		// 		'upd_date' => date("Y-m-d H:i:s")
		// 	);
			
		// 	$appVisaDB->insert($data11);

		// 	//item
		// 	$data = array(
		// 					'b_id'				=> $this->bulk_id,
		// 					'bi_trans_id'		=> $at_trans_id,
		// 					'bi_appl_id'		=> $applicant_id
		// 				);
			
		// 	$bulk_id = $this->bulkDB->addItem($data);

		// 	$total++;
	}

	function getState($state)
	{
		$db = getDB();
	  	$select = $db->select()
	                 ->from(array('s'=>'tbl_state'))
	                 ->where('LOWER(s.StateName) = ?', strtolower($state));
        $row = $db->fetchRow($select);
		
		if ( !empty($row) )
		{
			return $row['idState'];
		}
		else
		{
			return '';
		}
	}
	

	public function getProgram($name)
	{
		// echo $name;
		// die();
		$db = getDb();

		$sql =  $db->select()
	                 ->from(array('a'=>'tbl_program'),array('a.*'))
	                 ->joinleft(array('b'=>'tbl_program_scheme'),'a.IdProgram=b.IdProgram')
	                 ->where('LOWER(a.ProgramName) = ?', strtolower($name));

		$result = $db->fetchRow($sql);

		if ( empty($result) )
		{
			return '';
		}
		else
		{
			return $result;
		}
	}
	
	public function getIntake($name='')
	{
		$db = getDb();
        $sql = "Select IdIntake from tbl_intake where IntakeId ='$name'";
		// $sql =  $db->select()
	 //     	->from(array('a'=>'tbl_intake'),array('IdIntake'))
	 //         ->where('a.IntakeDesc = ?', $name);

		$result = $db->fetchRow($sql);

		if ( empty($result) )
		{
			return '';
		}
		else
		{
			return $result['IdIntake'];
		}
	}

	public function getIdByDef($id, $data, $map = array('k'=>'DefinitionDesc','v'=>'idDefinition') )

	{
		// echo $id;
		// echo "----";
		// print_r($data);
  		//        echo "\n";

		//die();
		if ( !empty($data) )
		{
			
			foreach ( $data as $row )
			{
				if ( strtolower($row[$map['k']]) == strtolower($id) )
				{
					$val = $row[$map['v']];
				}
			}
		}

		if ( empty($val) )
		{
			return 0;
		}
		else
		{
			return $val;
		}
	}

	function generateApplicantID(){

		return mt_rand(1,9999999999);
	 	// echo 	$applicantID = date("Y-m-d H:i:s").$randomNO;
	 	// return $applicantID;
		
		//generate applicant ID
		$applicantid_format = array();
		
		$seqnoDb = new App_Model_Application_DbTable_ApplicantSeqno();
		$sequence = $seqnoDb->getData(date('Y'));
		
		$tblconfigDB = new App_Model_General_DbTable_TblConfig();
		$config = $tblconfigDB->getConfig(1);
		
		//format
		 $format_config = explode('-',$config['ApplicantIdFormat']);
		
		for($i=0; $i<count($format_config); $i++){
			$format = $format_config[$i];
			if($format=='px'){ //prefix
				$result = $config['ApplicantPrefix'];
			}elseif($format=='yyyy'){
				$result = date('Y');
			}elseif($format=='yy'){
				$result = date('yy');
			}elseif($format=='seqno'){				
				$result = sprintf("%03d", $sequence['seq_no']);
			}
			array_push($applicantid_format,$result);
		}
	
		//var_dump(implode("-",$applicantID));
		$applicantID = implode("-",$applicantid_format);
		
		//update sequence		
		$seqnoDb->updateData(array('seq_no'=>$sequence['seq_no']+1),$sequence['seq_id']);
		
		return	$applicantID;
	}
}
?>