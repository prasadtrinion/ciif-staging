<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
include_once 'Zend/CIMB/Cimb.php';

class PaymentController extends Zend_Controller_Action
{

	public function init()
	{
		/* Initialize action controller here */
		$this->Session = new Zend_Session_Namespace('session');
    }

       public function ipay88Action(){
          $auth = Zend_Auth::getInstance();
          $appl_id = $auth->getIdentity()->appl_id;
          $studentData = new App_Model_Student_DbTable_StudentProfile();
          $student = $studentData->getDataById($appl_id);
          $ses_migs = new Zend_Session_Namespace('migs');
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$auth = Zend_Auth::getInstance();
		$appl_id = $auth->getIdentity()->appl_id;
		//$txnId = $ses_migs->txnid;
		
		$proformaInvoiceDb = new Studentfinance_Model_DbTable_ProformaInvoiceMain();
		$proforma_list = array();
			$proforma_list = $proformaInvoiceDb->getDataByApplid($appl_id);
			//print_r($proforma_list[0]);
			//die();
			$total = 0;
			$currency="";
			if(count($proforma_list)>0){ 
					foreach($proforma_list as $proforma):
		 		$total=$total+$proforma['bill_amount'];
		 		$currency = $proforma['cur_code'];
		 		endforeach;
		 		
		 	}
		 	//echo $currency;
			//echo $total;
			//		 die();
          //echo $student['appl_fname']."<br>";
          //echo $student['appl_email']."<br>";
          //echo $student['appl_phone_mobile']."<br>";
          //print_r($student);
          $amount = number_format((float)$total, 2, '.', '');
          //echo $appl_id;
          //die();
          $ipay88 = new IPay88('M10617');
          $ipay88->setMerchantKey('332501c0-a953-4771-a0fa-9db3011ecc7f');
          $ipay88->setField('PaymentId', 2);
          $ipay88->setField('RefNo', 'IPAY0000000001');
          $ipay88->setField('Amount', $amount);
          $ipay88->setField('Currency', $currency);
          $ipay88->setField('ProdDesc', 'Testing');
          $ipay88->setField('UserName', $student['appl_fname']);
          $ipay88->setField('UserEmail', $student['appl_email']);
          $ipay88->setField('UserContact', $student['appl_phone_mobile']);
          $ipay88->setField('Remark', 'Some remarks here..');
          $ipay88->setField('Lang', 'utf-8');
          $ipay88->setField('ResponseURL', 'http://iukl-studentportal.mtcsb.my/payment/response');
          $ipay88->generateSignature();
          $ipay88_fields = $ipay88->getFields();
          
          $url = Ipay88::$epayment_url;
          $data = $ipay88_fields;
          $options = array(
                      'http' => array(
                      'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                      'method'  => 'POST',
                      'content' => http_build_query($data)
                )
          );
          $context  = stream_context_create($options);
          $result = file_get_contents($url, false, $context);
          if ($result === FALSE) {
            echo "error";
            die();
          }
          die($result);

          //print_r($ipay88_fields);
          //die();

        }
        public function responseAction(){
          
        }

        public function getpayAction(){

          $post = curl_init();
          $securevalue = hash('sha256', '100812195521159000096241V0000010907000891PLAINAPPROVED3');

          if($_POST) {

             curl_setopt($post, CURLOPT_VERBOSE, true);
             curl_setopt($post, CURLOPT_URL, 'https://uat-emerchant.cimbbank.com.my/BPG/admin/payment/PaymentWindow.jsp');
             curl_setopt($post, CURLOPT_POST, count($_POST));
             curl_setopt($post, CURLOPT_POSTFIELDS, $_POST);
             curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);

             $result = curl_exec($post);

             // output result from gateway as JSON
             header('Content-Type: application/json');

             echo $result;

             curl_close($post);

             exit();
        }
      }
	
}