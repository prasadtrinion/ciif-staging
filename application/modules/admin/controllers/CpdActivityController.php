<?php
class Admin_CpdActivityController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
		 
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_DefinitionSearch(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view	
		$lobjemailTemplateModel = new Admin_Model_DbTable_CpdActivity();  // email template model object
		$larrresult = $lobjemailTemplateModel->getDefination(202); // get template details	
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();	
			
			 // echo $larrformData['field3']	;
			 // die();			
				if ($lobjsearchform->isValid($larrformData)) {	
					$larrresult = $lobjemailTemplateModel->fnSearchTemplate($lobjsearchform->getValues());
					if(empty($larrresult))
					{
						$this->_redirect( $this->baseUrl . '/admin/programme-module/index');
					}										
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
$this->_redirect( $this->baseUrl . '/admin/programme-module/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function addAction() { 	
  		$member_form = new Admin_Form_Definitionadd();
            $this->view->form = $member_form;

            if($this->getRequest()->isPost())
            {    
        

                $formdata =$this->getRequest()->getPost();
                
                if($member_form->isValid($formdata))
                {

                    $definition = $member_form->getValue('definition_ms'); 

                    $description = $member_form->getValue('description');
                      
                    $add_member = new Admin_Model_DbTable_CpdActivity();
                    $add_member->addData($definition,$description);
                    if($add_member)
                    {
                     $this->_redirect( $this->baseUrl . '/admin/programme-module/index');
                    }
                    
                }


            }
	}

	public function editAction() {		
 		
 	 	
            $editform = new Admin_Form_DefinitionEdit();
    		$this->view->form = $editform;
		if($this->getRequest()->isPost())

		{

			$formData = $this->getRequest()->getPost();
			if($editform->isValid($formData))
		{

			$id = $this->getRequest()->getparam('Id');   	
			                                                                           //ine 4
			$definition=$editform->getValue('DefinitionDesc');

				
             $description=$editform->getValue('Description');
                 
				$edit_member = new Admin_Model_DbTable_CpdActivity();
				$edit_member->editmember($id,$definition,$description);

                    if($edit_member)
                    {
                     $this->_redirect( $this->baseUrl . '/admin/programme-module/index');
                    }

			}

		else
		{
    		 $editform->populate($formData);
		}


		}
		 else
		              {
                        $id = $this->getRequest()->getparam('Id');
                        	
                    
                         $file = new Admin_Model_DbTable_CpdActivity();
                         $files = $file->fetchRow('idDefinition='.$id);
                         $editform->populate($files->toArray());
                        
                     }    
$this->view->form = $editform;  	  	
}
}