<?php
class Admin_ProgramController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
		// $this->gobjsessionsis = Zend_Registry::get('sis'); //initialize session variable
		// $lobjinitialconfigModel = new GeneralSetup_Model_DbTable_Initialconfiguration(); //user model object
		// $larrInitialSettings = $lobjinitialconfigModel->fnGetInitialConfigDetails($this->gobjsessionsis->idUniversity);
		// $this->gintPageCount = isset($larrInitialSettings['noofrowsingrid'])?$larrInitialSettings['noofrowsingrid']:"5";
		// $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object	  
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_MemberExperienceSearch(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view	
		$lobjemailTemplateModel = new Admin_Model_DbTable_MemberExperience();  // email template model object
		$larrresult = $lobjemailTemplateModel->getsearch(); // get template details	
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();						
				if ($lobjsearchform->isValid($larrformData)) {
				$post_data=$this->getRequest()->getPost();
           
           				 $id=$post_data['field2'];	
					$larrresult = $lobjemailTemplateModel->search($id);										
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
			$this->_redirect( $this->baseUrl . '/admin/MemberExperience/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function addIntakeAction() { 	



  			$member_form = new Admin_Form_MemberExperience();
            $this->view->form = $member_form;

            if($this->getRequest()->isPost())
            {    
        

                $formdata =$this->getRequest()->getPost();
                
                if($member_form->isValid($formdata))
                {

                    // echo "<pre>";
                    // print_r($formdata);
                    // die();
                    $experience_name = $member_form->getValue('experience_name'); 

                    
                    $dropdown_member = $member_form->getValue('dropdown_member');
                    $dropdown_degree = $member_form->getValue('dropdown_degree'); 
 

                    $sub_type = $member_form->getValue('sub_type');
                      
                    $add_member = new Admin_Model_DbTable_MemberExperience();
                  
                    $add_member->addData($experience_name,$sub_type,$dropdown_member,$dropdown_degree);
                    if($add_member)
                    {
                        $this->_redirect( $this->baseUrl . '/admin/MemberExperience/index');
                    }
                    
                }


            }
	}

	public function editAction() {		
 		
 	 	
            $editform = new Admin_Form_MemberExperienceEdit();
    		$this->view->form = $editform;
		if($this->getRequest()->isPost())

		{

			$member_form = $this->getRequest()->getPost();
			if($editform->isValid($member_form))
		{

				$id = $this->getRequest()->getparam('Id');
			 $experience_name = $editform->getValue('exp_name'); 

                    
                    $dropdown_member = $editform->getValue('id_member');
                    $dropdown_degree = $editform->getValue('id_degree'); 
 

                    $sub_type = $editform->getValue('id_member_subtype');

                 
				$edit_member = new Admin_Model_DbTable_MemberExperience();
				$edit_member->editmember($id,$experience_name,$sub_type,$dropdown_member,$dropdown_degree);

                    if($edit_member)
                    {
                         $this->_redirect( $this->baseUrl . '/admin/MemberExperience/index');
                    }

                  //Line 5
//$this->_helper->redirector('index');
			}

		else
		{
    		 $editform->populate($formData);
		}


		}
		 else
		              {
                        $id = $this->getRequest()->getparam('Id');

                         $file = new Admin_Model_DbTable_MemberExperience();
                         $files = $file->fetchRow('id_exp='.$id);
                         $editform->populate($files->toArray());
                        
                     }    
$this->view->form = $editform;  	  	
}



public function getsubtypeAction()
{
	$this->_helper->layout->disableLayout();

	$memberId = $this->_getParam('member_id',null);

	$query =  new Admin_Model_DbTable_Member();

	$memberdata = $query->getmeberdata($memberId);

	echo $memberdata['sub_type'];

	 exit();
}


}