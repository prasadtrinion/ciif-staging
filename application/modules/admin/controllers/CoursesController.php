<?php
set_time_limit(0);
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);

class Admin_CoursesController extends  Zend_Controller_Action
{
	protected $coursesDb;
	protected $contentBlockDb;
	protected $coursecatDb;
	protected $uploadDir;
	protected $uploadFolder;

	public function init()
	{

		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'courses');

		$this->coursesDb = new App_Model_Courses();
		$this->contentBlockDb = new App_Model_ContentBlock();
		$this->blockDataDb = new App_Model_ContentBlockData();
		$this->coursecatDb = new App_Model_CoursesCategory();

		$this->uploadFolder = 'course-assets';
		$this->uploadDir = DOCUMENT_PATH. '/'.$this->uploadFolder;

		$auth = Zend_Auth::getInstance();
        $this->auth = $auth;

		//events
		//Cms_Filters::add('block-desc-save', array($this, 'Test'));
	}

	public static function Test($e)
	{
		$data = $e->getParams();

		$content = $data['content'].' modify 1';

		$e->setParam('content', $content);

		return $e->getParams();
	}

	public function indexAction()
	{
		$this->view->title = 'Courses List';

		$session  = new Zend_Session_Namespace('AdminCoursesIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/courses/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/courses/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $getcourses = $this->coursesDb->getCourses('title');

        if ($search) {
            $getcourses->where('title LIKE ? OR code LIKE ?', '%'.$formdata['search'].'%');
        }

		$courses = Cms_Paginator::query($getcourses);

		$this->view->results  = $courses;
		$this->view->formdata = $formdata;
	}

	public function newAction()
	{
		$this->view->title = 'New Course';

		$form = new Admin_Form_Course();


		//form required
		$form->title->setRequired(true);
		$form->code->setRequired(true);

		//category
		$cats = $this->coursecatDb->fetchAll()->toArray();
		$form->category_id->addMultiOption('', '');
		foreach ( $this->coursecatDb->printTree($this->coursecatDb->buildTree($cats,0)) as $opt )
		{
			$form->category_id->addMultiOption($opt['id'], $opt['name']);
		}

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$fail = 0;

				//check code
				$course = $this->coursesDb->fetchRow(array("code = ?" => $formData['code']));
				if (!empty($course)) {
					$fail = 1;
					Cms_Common::notify('error', 'Course code must be unique.');
				}

				if ( !$fail )
				{

					$data = array(
						'title'      	=> $formData['title'],
						'description'	=> $formData['description'],
						'welcome'		=> $formData['welcome'],
						'excerpt'		=> $formData['excerpt'],
						'category_id'	=> $formData['category_id'],
						'code'			=> $formData['code'],
						'code_safe'		=> Cms_Common::safename($formData['code']),
						'visibility'	=> $formData['visibility'],
						'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'created_by'    => $this->auth->getIdentity()->id,
						'use_comments'	=> $formData['use_comments'],
						'use_forum'		=> $formData['use_forum'],
						'learning_online' => (isset($formData['learning_online']) ? $formData['learning_online'] : 0),
						'learning_ftf'    => (isset($formData['learning_ftf']) ? $formData['learning_ftf'] : 0)
					);

					$course_id = $this->coursesDb->insert($data);

					Cms_Common::notify('success','Course successfully created');
					$this->redirect('/admin/courses/');
				}
			}
			else
			{
				Cms_Common::notify('error','Please fill in all the required fields');
			}
		}


		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->getParam('id');

		$course = $this->coursesDb->fetchRow(array("id = ?" => $id))->toArray();

		if ( empty($course) )
		{
			throw new Exception('Invalid Course ID');
		}

		$this->view->title = 'Edit Course';

		$this->view->course = $course;

		//form
		$form = new Admin_Form_Course();

		//populate
		$form->populate($course);
		//print_r($course);exit;

		//form required
		$form->title->setRequired(true);
		$form->code->setRequired(true);

		//category
		$cats = $this->coursecatDb->fetchAll()->toArray();
		$form->category_id->addMultiOption('', '');
		foreach ( $this->coursecatDb->printTree($this->coursecatDb->buildTree($cats,0)) as $opt )
		{
			$form->category_id->addMultiOption($opt['id'], $opt['name']);
		}

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$fail = 0;

				//check code
				$course = $this->coursesDb->fetchRow(array("code = ?" => $formData['code'], 'id != ?' => $id));
				if (!empty($course)) {
					$fail = 1;
					Cms_Common::notify('error', 'Course code must be unique.');
				}

				if ( !$fail )
				{
					$accessperiod =  $formData['accessperiod_opt'] == '1' ? $formData['accessperiod_num'].$formData['accessperiod_type'] : null;

					$data = array(
						'title'      	=> $formData['title'],
						'description'	=> $formData['description'],
						'welcome'		=> $formData['welcome'],
						'category_id'	=> $formData['category_id'],
						'excerpt'		=> $formData['excerpt'],
						'code'			=> $formData['code'],
						'code_safe'		=> Cms_Common::safename($formData['code']),
						'visibility'	=> $formData['visibility'],
						'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'modified_by'   => $this->auth->getIdentity()->id,
						'use_comments'	=> $formData['use_comments'],
						'use_forum'		=> $formData['use_forum'],
						'accessperiod' 	=> $accessperiod,
						'learning_online'=> $formData['learning_online'],
						'learning_ftf'	=> $formData['learning_ftf']
					);

					//upload file
					$files = Cms_Common::uploadFiles($this->uploadDir);

					//upload file
					if ( $course['thumbnail'] != '' || isset($formData['deletephoto']) )
					{
						@unlink(DOCUMENT_PATH.'/'.$course['thumbnail']);
						$data['thumbnail'] = null;
					}

					if ( !empty($files) )
					{
						//clear old
						if ( $course['thumbnail'] != '' )
						{
							@unlink( DOCUMENT_PATH.'/'.$course['thumbnail'] );
						}

						$data['thumbnail'] = $this->uploadFolder.'/'.$files[0]['fileurl'];
					}



					$this->coursesDb->update($data, array('id = ?'=> $id));

					//history
					$history = array(
										'entry_id'=>$id,
										'entry_table' => 'courses',
										'entry_tag' => '',
										'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
										'created_by' => $this->auth->getIdentity()->id,
										'history_data' => json_encode($data)
									);

					Cms_History::addHistory($history);

					//redirect
					Cms_Common::notify('success','Course successfully updated');
					$this->redirect('/admin/courses/edit/id/'.$id);
				}
			}
			else
			{
				Cms_Common::notify('error','Please fill in all the required fields');
			}
		}


		$this->view->form = $form;
	}

	public function viewAction()
	{
		$id = $this->getParam('id');

		$course = $this->coursesDb->fetchRow(array("id = ?" => $id))->toArray();

		$this->view->title = 'View Course';


		//blocks
		$blocks = $this->contentBlockDb->fetchAll(array('course_id = ?' => $id))->toArray();

		//block content
		$content = $this->blockDataDb->getByBlock($id);

		$total = 0;
		foreach ( $content as $c )
		{
			$total += count($c);
		}

		/*
		 * fix orphan content block data.
		 */
		if ( $this->getParam('fix') == '1' )
		{
			$db = getDb();

			$select = $db->select()->distinct()->from('content_block_data',array('course_id'))->where('course_id != 1');
			foreach ( $db->fetchAll($select) as $row )
			{
				$check = $db->select()->from('content_block')->where('course_id = ?',$row['course_id']);


				if ( empty($db->fetchRow($check)) )
				{
					$data = array(
									'course_id'		=> $row['course_id'],
									'title'			=> 'Course Material',
									'title_safe' 	=> 'course-material',
									'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
									'created_by'	=> 1
					);

					$db->insert('content_block',$data);
					$block_id = $db->lastInsertId();

					//now assign back all the course block data
					$db->update('content_block_data', array('block_id' => $block_id), array('course_id = ?' => $row['course_id']));
				}
			}

			echo 'done';
			exit;
		}

		// get student count
		$enrolDb = new App_Model_Enrol();
		$db = $enrolDb->getDefaultAdapter();

		$enroldata = $db->select()
				->from(array('a' => 'enrol'))
				->where('a.course_id = ?', $course['id'])
				->where('active = ?', 1)
				->group('a.user_id');

		$enrol = $db->fetchAll($enroldata);


		// get this content grouping
		$groupDb = new App_Model_UserGroupMapping();

		$db = $groupDb->getDefaultAdapter();
		$groupdata = $db->select()
				->from(array('a' => 'user_group'))
				->where('a.course_id = ?', $course['id'])
				->group('a.group_id');

		$groups = $db->fetchAll($groupdata);


		// get this announcement count
		$announcementDb = new App_Model_Announcements();

		$db = $announcementDb->getDefaultAdapter();
		$announcementdata = $db->select()
				->from(array('a' => 'announcements'))
				->where('a.course_id = ?', $course['id'])
				->where('visibility = ?', 1);

		$announcements = $db->fetchAll($announcementdata);

		//view
		$this->view->announcements = $announcements;
		$this->view->groups = $groups;
		$this->view->enrol = $enrol;
		$this->view->course = $course;
		$this->view->blocks = $blocks;
		$this->view->content = $content;
		$this->view->totalcontent = $total;
		$this->view->id = $id;



		if ($this->_request->isPost () && $this->_request->getPost ( 'addblock' )) {

			$formData = $this->_request->getPost();

			$formData['block-desc'] = Cms_Filters::apply('block-desc-save', $formData['block-desc']);

			$data = array(
				'title'      	=> $formData['block-name'],
				'title_safe'	=> Cms_Common::safename($formData['block-name']),
				'description'	=> $formData['block-desc'],
				'course_id'		=> $id,
				'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
				'created_by'    => $this->auth->getIdentity()->id,
			);



			$this->contentBlockDb->insert($data);

			Cms_Common::notify('success','Block successfully created');
			$this->redirect('/admin/courses/view/id/'.$id);

		}

		//[x] content
		//[x] scorm
		//[x] support files
		//[ ] link

		//CONTENT PAGE
		$data_id = 0;

		if ($this->_request->isPost () && $this->_request->getPost ( 'type' ) == 'content') {
			$formData = $this->_request->getPost();

			if ( $formData['block_id'] == '' )
			{
				throw new Exception('Invalid Block ID');
			}

			//add new content
			$formData['course_id'] = $id;

			$data_id = $this->blockDataDb->addContent($formData);

			//add groups
			$groupDb = new App_Model_UserGroupMapping();

			$groupDb->deleteMappingByDataId($data_id, $id);
			foreach ($formData['group'] as $group) {
				$data = array(
							'map_type'	=> 'content',
							'group_id'	=> $group,
							'course_id' => $id,
							'mapped_id'	=> $data_id
					);

				$groupDb->addMappingData($data);
			}


			Cms_Common::notify('success','Content successfully created');
			$this->redirect('/admin/courses/view/id/'.$id);

		}

		//SCORM
		if ($this->_request->isPost () && $this->_request->getPost ( 'type' ) == 'scorm') {

			$fail = false;
			$formData = $this->_request->getPost();
			$scormurl = '';

			//upload file
			$files = Cms_Common::uploadFiles(Cms_Scorm_Api::path(),'zip');

			if ( !empty($files) )
			{
				if ( Cms_Scorm_Package::check(Cms_Scorm_Api::path($files[0]['fileurl'])) )
				{
                    //process extraction
                    $file = pathinfo($files[0]['filename']);
                    $basename = $file['filename'];

                    $path_extract = Cms_Scorm_Api::pathData($id, $basename);
                    $scormurl = Cms_Scorm_Api::urlData($id, $basename);

                    $manifest = Cms_Scorm_Package::extract(Cms_Scorm_Api::path($files[0]['fileurl']), $path_extract);

					$scormurl = pathinfo($manifest)['dirname'] == '' ? '' : $scormurl .'/'.pathinfo($manifest)['dirname'];

                    if ( empty($manifest) )
                    {
						$error = 'Unable to find scorm package.';
                        $fail = true;
                    }
				}
				else
				{
					$error = 'Scorm Package failed validation.';
					$fail = true;
				}
			}


			//woopsie
			if ( $fail === true )
			{
				unlink(DOCUMENT_PATH . $files[0]['fileurl'] );
				if ( $scormurl != '' )
                {
                    Cms_Common::removeDir($scormurl);
                }
				throw new Exception('Error occurred while processing SCORM Package. { '.$error.'}');
			}
			else
			{
				//data content first
				$data = array(
					'course_id'     => $id,
					'block_id'      => $formData['block_id'],
					'data_type'     => 'scorm',
					'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
					'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'title'			=> $formData['title']
				);

				$data_id = $this->blockDataDb->insert($data);

				//scorm data
				$datacontent = array(
					'data_id'	=> $data_id,
					'title'		=> $formData['title'],
					'url'		=> $scormurl,
					'zipfile'	=> Cms_Scorm_Api::zipfile($files[0]['fileurl'])
				);

				$scormDb = new App_Model_DataScorm();
				$scormDb->insert($datacontent);

				//add groups
				$groupDb = new App_Model_UserGroupMapping();

				if (!isset($formData['group'])) {
					$formData['group'] = array();
				}

				$groupDb->deleteMappingByDataId($data_id, $id);
				foreach ($formData['group'] as $group) {
					$data = array(
								'map_type'	=> 'content',
								'group_id'	=> $group,
								'course_id' => $id,
								'mapped_id'	=> $data_id
						);

					$groupDb->addMappingData($data);
				}

				Cms_Common::notify('success','SCORM Content successfully added');
				$this->redirect('/admin/courses/view/id/'.$id);
			}
		}

		//OTHERS
		if ($this->_request->isPost () ) {
			$formData = $this->_request->getPost();

			if ( $formData['block_id'] == '' )
			{
				throw new Exception('Invalid Block ID');
			}

			//add new content
			$formData['course_id'] = $id;

			$data_id = $this->blockDataDb->addContent($formData);

			//add groups
			$groupDb = new App_Model_UserGroupMapping();

			$groupDb->deleteMappingByDataId($data_id, $id);
			foreach ($formData['group'] as $group) {
				$data = array(
							'map_type'	=> 'content',
							'group_id'	=> $group,
							'course_id' => $id,
							'mapped_id'	=> $data_id
					);

				$groupDb->addMappingData($data);
			}

			Cms_Common::notify('success','Content successfully created');
			$this->redirect('/admin/courses/view/id/'.$id);

		}
	}

	public function blockAction()
	{
		$this->view->ajax = 0;

		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->_helper->layout->disableLayout();
			$this->view->ajax = 1;
		}

		$id = $this->_getParam('id');

		$block = $this->contentBlockDb->fetchRow(array("id = ?" => $id))->toArray();

		if ( empty($block) )
		{
			throw new Exception('Invalid Block');
		}

		$course = $this->coursesDb->fetchRow(array("id = ?" => $block['course_id']))->toArray();

		if ( empty($course) )
		{
			throw new Exception('Invalid Course');
		}

		$this->view->title = 'Modify Block';
		$this->view->block = $block;
		$this->view->course = $course;
		$this->view->id = $id;

		if ($this->_request->isPost () && $this->_request->getPost ( 'editblock' )) {

			$formData = $this->_request->getPost();

			$formData['block-desc'] = Cms_Filters::apply('block-desc-save', $formData['block-desc']);

			$data = array(
				'title'      	=> $formData['block-name'],
				'title_safe'	=> Cms_Common::safename($formData['block-name']),
				'description'	=> $formData['block-desc'],
				'modified_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
				'modified_by'    => $this->auth->getIdentity()->id,
			);

			$this->contentBlockDb->update($data, array('id = ?' => $id));

			Cms_Common::notify('success','Block successfully updated');
			$this->redirect('/admin/courses/view/id/'.$course['id']);

		}
	}

	public function blockcontentAction()
	{
		$this->view->ajax = 0;

		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->_helper->layout->disableLayout();
			$this->view->ajax = 1;
		}

		$id = $this->_getParam('id');

		$blockcontent = $this->blockDataDb->fetchRow(array('data_id = ?' => $id))->toArray();

		if ( empty($blockcontent) )
		{
			throw new Exception('Invalid Content');
		}

		$block = $this->contentBlockDb->fetchRow(array("id = ?" => $blockcontent['block_id']))->toArray();
		$course = $this->coursesDb->fetchRow(array("id = ?" => $block['course_id']))->toArray();

		if ( empty($block) )
		{
			throw new Exception('Invalid Block');
		}

		if ( empty($course) )
		{
			throw new Exception('Invalid Course');
		}

		$dataDb = new App_Model_DataContent();
		$dataWikiDb = new App_Model_DataWiki();
		$filesDb = new App_Model_DataFiles();
		$linkDb = new App_Model_DataLink();
		$videoDb = new App_Model_DataVideo();
		$scormDb = new App_Model_DataScorm();
		$groupDb = new App_Model_UserGroupMapping();

		switch ( $blockcontent['data_type'] )
		{
			case "content":
				$contentdata = $dataDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));
			break;

			case "assignment":
				$contentdata = $filesDb->fetchAll(array('file_type = ?' => 'blockfile', 'parent_id = ?' => $blockcontent['data_id']));

			break;

			
			case "quiz":
				$contentdata = $dataDb->fetchAll(array('data_id = ?' => $blockcontent['data_id']));
				$id=$blockcontent['data_id'];
				$quizDb = new App_Model_Quiz(); 
            	$getquestion = $quizDb->getquescount($id);
            	//$gettmark = $quizDb->getmarksum($id);
            	//pr($gettmark);exit;
            	$this->view->countquestion = count($getquestion);

				
			break;

			case "support":
				$contentdata = $filesDb->fetchAll(array('file_type = ?' => 'blockfile', 'parent_id = ?' => $blockcontent['data_id']));
			break;

			case "scorm":
				$contentdata = $scormDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));

			break;

			case "link":
				$contentdata = $linkDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));
			break;

			case "video":
				$contentdata = $videoDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));
			break;

			case "wiki":
				$contentdata = $dataWikiDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));
			break;

			case "audio":
				$contentdata = $filesDb->fetchAll(array('file_type = ?' => 'audiofile', 'parent_id = ?' => $blockcontent['data_id']));
			break;
		}

		if ( !isset($contentdata) || empty($contentdata) )
		{
			//throw new Exception('Invalid Content Data for Block');
			return Cms_Render::error('Invalid Content Data for Block');
		}
		else
		{
			$contentdata = !is_array($contentdata) ? $contentdata->toArray() : $contentdata;
			//$contentdata = $contentdata->toArray();
		}

		//UPDATE
		if ($this->_request->isPost ()) {
			$formData = $this->_request->getPost();

			// for groups
			$groupDb->deleteMappingByDataId($id, $formData['course_id']);

			if (isset($formData['group']) && is_array($formData['group'])) {
				foreach ($formData['group'] as $group) {
					$data = array(
								'map_type'	=> 'content',
								'group_id'	=> $group,
								'course_id' => $formData['course_id'],
								'mapped_id'	=> $id
						);

					$groupDb->addMappingData($data);
				}
			}

			$this->blockDataDb->updateContent($formData, $id);
			//pr($formData);exit;


			Cms_Common::notify('success','Content successfully updated');
			$this->redirect('/admin/courses/blockcontent/id/'.$id.'/blockid/'.$block['id']);

		}

		$db = $groupDb->getDefaultAdapter();

		// get this content grouping
		$groupdata = $db->select()
							->from(array('a' => 'user_group'))
							->joinLeft(array(
									'c' => 'user_group_mapping'),
									'(a.group_id = c.group_id AND c.map_type = "content" AND c.mapped_id = "'.$blockcontent['data_id'].'")',
									array('c.map_id'))
							->where('a.course_id = ?', $course['id'])
							->group('a.group_id')
							->order('a.group_name');

		$groups = $db->fetchAll($groupdata);

		//pr($groupdata);exit;

		$this->view->groups = $groups;
		//$this->view->title = 'Modify Content';
		$this->view->title = $course['title'];
		$this->view->content = $blockcontent;
		$this->view->contentdata = $contentdata;
		$this->view->block = $block;
		$this->view->course = $course;
		$this->view->id = $id;
	}

	public function scormAction()
	{
		$scormDb = new App_Model_DataScorm();
		$userScorm = new App_Model_UserScorm();

		$id = $this->_getParam('id');

		$blockcontent = $this->blockDataDb->fetchRow(array('data_id = ?' => $id))->toArray();

		if ( empty($blockcontent) )
		{
			throw new Exception('Invalid Content');
		}

		$course = $this->coursesDb->fetchRow(array("id = ?" => $blockcontent['course_id']))->toArray();

		//scorm
		$contentdata = $scormDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));

		if ( !isset($contentdata) || empty($contentdata) )
		{
			throw new Exception('Invalid Content Data for Block');
		}
		else
		{
			$contentdata = !is_array($contentdata) ? $contentdata->toArray() : $contentdata;
			//$contentdata = $contentdata->toArray();
		}

		//get results
		$results = $userScorm->getUserData($contentdata['id']);

		$this->view->results = $results;
		$this->view->course = $course;
		$this->view->scorm = $contentdata;


	}

	public function gradingAction()
	{
		$id     = $this->_getParam('id');
		$cid    = $this->_getParam('cid');
		$course = $this->coursesDb->fetchRow(array('id = ?' => $id));
		$this->view->title = 'Grading List';

		$session  = new Zend_Session_Namespace('AdminCoursesGrading');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

		if ($this->getRequest()->isPost()) 
		{
		    $formdata = $this->getRequest()->getPost();
		    $session->formdata = $formdata;
		    $this->redirect("/admin/courses/grading/id/$id/cid/$cid/search/1");
		}

		if (!$this->_getParam('search', 0)) 
		{
		    unset($session->formdata);
		    $session->setExpirationSeconds(1);
		}

		if (isset($session->formdata)) 
		{
		    $formdata = $session->formdata;
		}

		$assignmDb  = new App_Model_DataAssignments();
		$assignment = $assignmDb->getTitle($cid);
		$query      = $assignmDb->getCourses($id,$cid);

		if (isset($formdata['search']) && $formdata['search'])
		{
			$query->where("CONCAT_WS(' ', b.firstname, b.lastname) like ?", '%'.$formdata['search'].'%');
		}
		if (isset($formdata['status']))
		{
			if ($formdata['status'] != '')
			{
				$query->where('a.status = ?', $formdata['status']);
			}
		}

		$grading = Cms_Paginator::query($query);
		
		$this->view->nametitle  = $assignment['title'];
		$this->view->title      = $course['title'];
		$this->view->results    = $grading;
		$this->view->cid        = $cid;
		$this->view->id         = $id;
		$this->view->course     = $course;
		$this->view->assignment = $assignment;
		$this->view->formdata   = $formdata;
		$this->view->id         = $id;
		$this->view->cid        = $cid;
	}

	public function gradeAction()
	{
		$id = $this->getParam('id');
		$cid = $this->getParam('cid');

		$assignmDb  = new App_Model_DataAssignments();
		$course     = $assignmDb->fetchRow(array("id = ?" => $id))->toArray();
		$gradepoint = $assignmDb->getGradepoint($course['parent_id']);

		$db = $assignmDb->getDefaultAdapter();
		$select = $db->select()
			->from(array('a' => 'data_assignments'), array('id',  'user_id', 'grade', 'feedback'))
			->join(array('d' => 'courses'), 'd.id = a.course_id', array('course_id' => 'id', 'course_title' => 'title', 'course_code' => 'code'))
			->join(array('b' => 'user'), 'b.id = a.user_id', array('student_name' => "CONCAT_WS(' ', firstname, lastname)", 'student_email' => 'email'))
			->join(array('c' => 'content_block_data'), 'c.data_id = a.parent_id', array('data_id', 'title', 'gradepoint', 'gradepass', 'grading_method'))
			->joinLeft(array('e' => 'data_files'), 'e.id = a.data_id', array('file_name', 'file_url', 'file_size', 'created_date'))
			->where('a.id = ?', $id);
		$assignment = $db->fetchRow($select);
		$this->view->assignment = $assignment;

		if ( empty($course) )
		{
			throw new Exception('Grade doesnt exist');
		}

		$this->view->title = 'Grade';

		$this->view->course = $course;

		$this->view->gradepoint = $gradepoint['gradepoint'];

		//form
		$form = new Admin_Form_Grade();

		//populate
		$form->populate($course);

		//form required
		$form->grade->setRequired(true);
		//pr($form);exit;
		if ($this->getRequest()->isPost()) {
                    $formData = $this->getRequest()->getPost();
                    $data = array(
                            'grade'     => $formData['grade'],
                            'grade_on'     => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'grade_by'     => $this->auth->getIdentity()->id,
                            'feedback'     => $formData['feedback'],
                            'status'     => 2,
                        );
        //update data
        $assignmDb->update($data, array('id = ?' => $id));

        Cms_Common::notify('success', 'Information Updated');
        //$link = $this->view->baseUrl()."admin/courses/grade/id/".$id;
        $link = $this->view->baseUrl()."admin/courses/grading/id/".$course['course_id']."/cid/".$course['parent_id'];
        $this->redirect( $link );
		}
        //pr($data);exit;
		$this->view->form = $form;

	}

		public function download2AllAction() {
			//$id='148';

 			//$filesDb = new App_Model_DataFiles();
			//$getasgmtdata = $filesDb->getlimitData(array('file_type = ?' => 'asgmfile', 
            //                    'parent_id = ?' => $id,
             //                   'created_by = ?' => $this->auth->getIdentity()->id));
			/*
			$appDocsTbl = new App_Model_Application_DbTable_ApplicantDocument();
				foreach($appDocsTbl->getData() as $docs){
					$offer_letter[ $docs['ad_txn_id'] ] = $docs['ad_filepath'] . '/' . $docs['ad_filename'];
				}
			*/
		/*$groupDb = new App_Model_UserGroupMapping();
		$db = $groupDb->getDefaultAdapter();
		// get this content grouping
		$groupdata = $db->select()
							->from(array('a' => 'user_group'));
		$groups = $db->fetchAll($groupdata);
		$first_names = array_column($groups, 'group_name');
		$data = array($first_names);
		pr($groups);exit;*/
		//group ;
		$id = $this->_getParam('id');
		$cid = $this->_getParam('cid');		
		//pr($cid);exit;
		//$assignmDb = new App_Model_DataAssignments();
		//$getstudent = $assignmDb->getCourses($id,$cid);
		//pr($getstudent);exit;
		$assignmDb = new App_Model_DataAssignments();
		$db = $assignmDb->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'data_assignments'))
            ->joinLeft(array('b' => 'user'), 'b.id=a.user_id',array('b.id as u_id','firstname','email'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('title','code'))
            ->joinLeft(array('d' => 'content_block_data'), 'd.data_id=a.parent_id', array('gradepoint','title'))
            ->joinLeft(array('e' => 'data_files'), 'e.id=a.data_id', array('id as data_id','file_url'))
            ->where('a.course_id = ?', $id)
            ->where('d.data_id = ?', $cid);

        $result = $db->fetchAll($select);

        $file_url = array_column($result, 'file_url');
        $file_name = array_column($result, 'file_name');
		$files = ($file_url);
		
		foreach($files as $index=>$docs){
					$files[$index] = DOCUMENT_URL.$docs;
				}

        //pr($files);exit;

		/*
		$filesDb = new App_Model_DataFiles();
		$db = $filesDb->getDefaultAdapter();
		// get this content grouping
		$datafiles = $db->select()
		->from(array('a' => 'data_files'))
		//->where('a.id IN (317,288,287)');
		->where('a.file_type = "asgmfile"')
		->where('a.parent_id = ?' ,$cid);
		$data = $db->fetchAll($datafiles);
		$file_names = array_column($data, 'file_url');
		$files = ($file_names);
		
		foreach($files as $index=>$docs){
					$files[$index] = DOCUMENT_URL.$docs;
				}
		*/


		# create new zip opbject
		$zip = new ZipArchive();

		# create a temp file & open it
		$tmp_file = tempnam('.','');
		$zip->open($tmp_file, ZipArchive::CREATE);

		//$filename = 'assignments_'.date('Y-m-d H:i:s').'.zip';
		$filename = 'assignments_'.date('Y-m-d H:i:s').'-'.$id.'-'.$cid.'.zip';
		
		# loop through each file
		foreach($files as $index=>$file){

			/*$files[$index] = $file_name;
			pr($file_name);
			pr($files);
			pr($files[$index]);exit;*/
			//pr($files[$index]);exit;
		    # download file
		    $download_file = file_get_contents($files[$index]);

		    #add it to the zip
		    $zip->addFromString(basename($files[$index]),$download_file);
		    $zip->renameName(basename($files[$index]), $file_name[$index]);

		}

		# close zip
		$zip->close();

		# send the file to the browser as a download
		//header('Content-disposition: attachment; filename=download.zip');
		header('Content-Disposition: attachment; filename="'.basename($filename).'"');
		header('Content-type: application/zip');
		readfile($tmp_file);
		exit;
		/*
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//if ($this->getRequest()->isPost()) {

			//$formData = $this->getRequest()->getPost();

			//echo "<pre>";echo print_r($formData);exit();
			
			$filename = 'assignments_'.date('ymdHis').'.zip';
			$zip_name = DOCUMENT_PATH . '/files/' . $filename;
			//pr($filename);exit;

			$success = $this->create_zip($data2, $zip_name, true);
			if($success){
				echo 'Success';
			}else{
				echo 'Failed';
			}

			header('Content-type: application/zip');
			header('Content-Disposition: attachment; filename="'.basename($filename).'"');
			ob_clean(); 			//Fix to solve "corrupted compressed file" error!
			readfile($zip_name);
			unlink($zip_name);

			$this->view->file_path = $zip_name;
		*/
		//}
	}


		public function downloadAllAction() {
			//$id='148';

 			//$filesDb = new App_Model_DataFiles();
			//$getasgmtdata = $filesDb->getlimitData(array('file_type = ?' => 'asgmfile', 
            //                    'parent_id = ?' => $id,
             //                   'created_by = ?' => $this->auth->getIdentity()->id));
			/*
			$appDocsTbl = new App_Model_Application_DbTable_ApplicantDocument();
				foreach($appDocsTbl->getData() as $docs){
					$offer_letter[ $docs['ad_txn_id'] ] = $docs['ad_filepath'] . '/' . $docs['ad_filename'];
				}
			*/
		/*$groupDb = new App_Model_UserGroupMapping();
		$db = $groupDb->getDefaultAdapter();
		// get this content grouping
		$groupdata = $db->select()
							->from(array('a' => 'user_group'));
		$groups = $db->fetchAll($groupdata);
		$first_names = array_column($groups, 'group_name');
		$data = array($first_names);
		pr($groups);exit;*/
		//group ;
		$id = $this->_getParam('id');
		$cid = $this->_getParam('cid');		
		//pr($cid);exit;
		//$assignmDb = new App_Model_DataAssignments();
		//$getstudent = $assignmDb->getCourses($id,$cid);
		//pr($getstudent);exit;
		$assignmDb = new App_Model_DataAssignments();
		$db = $assignmDb->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'data_assignments'))
            ->joinLeft(array('b' => 'user'), 'b.id=a.user_id',array('b.id as u_id','firstname','email'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('title','code'))
            ->joinLeft(array('d' => 'content_block_data'), 'd.data_id=a.parent_id', array('gradepoint','title'))
            ->joinLeft(array('e' => 'data_files'), 'e.id=a.data_id', array('id as data_id','file_url'))
            ->where('a.course_id = ?', $id)
            ->where('d.data_id = ?', $cid);

        $result = $db->fetchAll($select);

        $file_url = array_column($result, 'file_url');
        $file_name = array_column($result, 'file_name');
		$files = ($file_url);
		
		foreach($files as $index=>$docs){
					$files[$index] = DOCUMENT_URL.$docs;
				}

        //pr($files);exit;

		/*
		$filesDb = new App_Model_DataFiles();
		$db = $filesDb->getDefaultAdapter();
		// get this content grouping
		$datafiles = $db->select()
		->from(array('a' => 'data_files'))
		//->where('a.id IN (317,288,287)');
		->where('a.file_type = "asgmfile"')
		->where('a.parent_id = ?' ,$cid);
		$data = $db->fetchAll($datafiles);
		$file_names = array_column($data, 'file_url');
		$files = ($file_names);
		
		foreach($files as $index=>$docs){
					$files[$index] = DOCUMENT_URL.$docs;
				}
		*/


		# create new zip opbject
		$zip = new ZipArchive();

		# create a temp file & open it
		$tmp_file = tempnam('.','');
		$zip->open($tmp_file, ZipArchive::CREATE);

		//$filename = 'assignments_'.date('Y-m-d H:i:s').'.zip';
		$filename = 'assignments_'.date('Y-m-d H:i:s').'-'.$id.'-'.$cid.'.zip';
		
		# loop through each file
		foreach($files as $index=>$file){

			/*$files[$index] = $file_name;
			pr($file_name);
			pr($files);
			pr($files[$index]);exit;*/
			//pr($files[$index]);exit;
		    # download file
		    $download_file = file_get_contents($files[$index]);

		    #add it to the zip
		    $zip->addFromString(basename($files[$index]),$download_file);
		    $zip->renameName(basename($files[$index]), $file_name[$index]);

		}

		# close zip
		$zip->close();

		# send the file to the browser as a download
		//header('Content-disposition: attachment; filename=download.zip');
		header('Content-Disposition: attachment; filename="'.basename($filename).'"');
		header('Content-type: application/zip');
		readfile($tmp_file);
		exit;
		/*
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//if ($this->getRequest()->isPost()) {

			//$formData = $this->getRequest()->getPost();

			//echo "<pre>";echo print_r($formData);exit();
			
			$filename = 'assignments_'.date('ymdHis').'.zip';
			$zip_name = DOCUMENT_PATH . '/files/' . $filename;
			//pr($filename);exit;

			$success = $this->create_zip($data2, $zip_name, true);
			if($success){
				echo 'Success';
			}else{
				echo 'Failed';
			}

			header('Content-type: application/zip');
			header('Content-Disposition: attachment; filename="'.basename($filename).'"');
			ob_clean(); 			//Fix to solve "corrupted compressed file" error!
			readfile($zip_name);
			unlink($zip_name);

			$this->view->file_path = $zip_name;
		*/
		//}
	}

	/*function create_zip($files = array(), $destination = '', $overwrite = false) {

		if(file_exists($destination) && !$overwrite) { return false; }

		$valid_files = array();

		if(is_array($files)) {
			foreach($files as $file) {
				if(file_exists(DOCUMENT_PATH.'\files'.$file)) {
					$valid_files[] = DOCUMENT_PATH.'\files'.$file;
				}
			}
		}

		if(count($valid_files)) {

			$zip = new ZipArchive();
			if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
				return false;
			}

			foreach($valid_files as $file) {
				$filename = substr($file, strrpos($file, '/') + 1);
				$zip->addFile($file,$filename);
			}
			//debug
			//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

			$zip->close();

			return file_exists($destination);
		}
		else
		{
			return false;
		}
	}*/




	/*function create_zip($files = array(), $destination = '', $overwrite = false) {

		if(file_exists($destination) && !$overwrite) { return false; }

		$valid_files = array();

		if(is_array($files)) {
			foreach($files as $file) {
				if(file_exists(DOCUMENT_PATH.'\files'.$file)) {
					$valid_files[] = DOCUMENT_PATH.'\files'.$file;
				}
			}
		}

		if(count($valid_files)) {

			$zip = new ZipArchive();
			if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
				return false;
			}

			foreach($valid_files as $file) {
				$filename = substr($file, strrpos($file, '/') + 1);
				$zip->addFile($file,$filename);
			}
			//debug
			//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

			$zip->close();

			return file_exists($destination);
		}
		else
		{
			return false;
		}
	}*/

}
