<?php
class Admin_CoursecategoryController extends  Zend_Controller_Action
{
	protected $coursecatDb;


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'courses');

		$this->coursecatDb = new App_Model_CoursesCategory();
		$this->courseDb = new App_Model_Courses();

	}

	public function indexAction()
	{
		$session  = new Zend_Session_Namespace('AdminCoursecategoryIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/coursecategory/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/coursecategory/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

		$this->view->title = 'Course Categories';

		$db     = getDB();
		$select = $db->select()->from(array('a' => 'courses_category'));

		if (isset($formdata['search']) && $formdata['search'])
		{
			$select->where('name LIKE ?', '%'.$formdata['search'].'%');
		}

		$cats    = $db->fetchAll($select);
		$results = $this->coursecatDb->printTree($this->coursecatDb->buildTree($cats,0),0,0,false) ;

		$this->view->results = $results;
		$this->view->formdata = $formdata;

	}

	public function addAction()
	{
		$form = new Admin_Form_CourseCategory();

		$pid = $this->_getParam('pid');

		$this->view->title = "New Course Category";

		$cats = $this->coursecatDb->fetchAll()->toArray();


		foreach ( $this->coursecatDb->printTree($this->coursecatDb->buildTree($cats,0)) as $opt )
		{
			$form->parent_id->addMultiOption($opt['id'], $opt['name']);
		}

		//pid
		if ( !empty($pid) )
		{
			$form->parent_id->setValue($pid);
		}

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$fail = 0;

				$data = array(
						'name'      	=> $formData['name'],
						'description'	=> $formData['description'],
						'parent_id'		=> $formData['parent_id'],
						'name_safe'		=> Cms_Common::safename($formData['name']),
						'thumbnail'		=> '',
						'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'created_by'    => $this->auth->getIdentity()->id,
						'active'		=> 1
					);

				$course_id = $this->coursecatDb->insert($data);

				Cms_Common::notify('success','Course category successfully created');
				$this->redirect('/admin/coursecategory/');

			}
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->_getParam('id');

		$cat = $this->coursecatDb->fetchRow(array("id = ?" => $id))->toArray();

		if (empty($cat)) {
			throw new Exception('Invalid Course Category');
		}

		$form = new Admin_Form_CourseCategory();

		//modified
		if ($cat['modified_by'] != null)
		{
			$userDb = new App_Model_User;
			$user = $userDb->getUser($cat['modified_by']);
			$this->view->modified_user = $user;
		}

		$this->view->title = "Edit Course Category";

		$cats = $this->coursecatDb->fetchAll(array('active = 1'))->toArray();


		foreach ( $this->coursecatDb->printTree($this->coursecatDb->buildTree($cats,0)) as $opt )
		{
			$form->parent_id->addMultiOption($opt['id'], $opt['name']);
		}

		//populate
		$form->populate($cat);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$fail = 0;

				$data = array(
					'name'      	=> $formData['name'],
					'description'	=> $formData['description'],
					'parent_id'		=> $formData['parent_id'],
					'name_safe'		=> Cms_Common::safename($formData['name']),
					'thumbnail'		=> '',
					'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'modified_by'   => $this->auth->getIdentity()->id,
					'active'		=> $formData['active']
				);

				$this->coursecatDb->update($data, array('id = ?' => $id));

				Cms_Common::notify('success','Course category successfully edited');
				$this->redirect('/admin/coursecategory/edit/id/'.$id);

			}
		}

		$this->view->form = $form;
		$this->view->cat = $cat;
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$cat = $this->coursecatDb->fetchRow(array("id = ?" => $id))->toArray();

		if (empty($cat)) {
			throw new Exception('Invalid Course Category');
		}

		//move all existing categories
		$this->courseDb->update(array('category_id' => 0), array('category_id = ?' => $id) );


		//delete
		$this->coursecatDb->delete(array('id = ?' => $id));

		Cms_Common::notify('success','Course category successfully deleted');
		$this->redirect('/admin/coursecategory/');
	}


}

