

<?php

class Admin_ReceiptController extends  Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'dashboard');
	}

	public function indexAction()
	{
		$form = new Admin_Form_Receipt();
		$this->view->form = $form;
		 if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
             $receiptdata = new Admin_Model_DbTable_Receipt();
             $result = $receiptdata->getReceipt($formData['receipt']);
             $this->view->result = $result;
}

	}
	public function addAction()
	{
		$form = new Admin_Form_Receipt();
		$this->view->form = $form;

		  if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $receiptinsert = new Admin_Model_DbTable_Receipt();
            $amount =0; 
            $id = $receiptinsert->insertreceipt($formData);
            $receiptdetails = new  Admin_Model_DbTable_ReceiptDetails();
            $receiptdetails->insertreceiptdetails($formData,$id);
            $this->redirect('admin/receipt');
            	}
}

	public function getinvoicedataAction()
	{
		$id = $this->getParam('id','');
		
		$invoice = new Admin_Model_DbTable_Invoice();
		$data = $invoice->getInvoice($id);

		$json = Zend_Json::encode($data);
		echo $json;
	
				
                  
                   // die();

	 exit();
	
	}


	public function receiptAction()
    {
        $data = $this->_getParam('Id');

        $result = new Admin_Model_DbTable_Receipt();
        $data = $result->ReceiptReport($data);
        $number = $data[0]['receipt_number'];
        $date = date('d/m/y');

       
        $processing_fee="

    
   <table align='center' border=0 style='width:100%' >
         <tr>
          <td width='10%'>
               &nbsp;
            </td>
            <td width='50%'>
               <img src='./assets/images/ciifp_new_logo1.jpg' alt='ciifp logo' >
            </td>

            <td width='40%'>
              <table>
                 <tr><td style='text-align:right;'>KNOWLEDGE | INTEGRITY | SERVICE</td></tr>
                                  <tr><td  style='text-align:right;'> 3rd Floor, Menara Takaful Malaysia,<br/>
                           Jalan Sultan Sulaiman, 50000 Kuala Lumpur<br/>
                           03-2273 2135 / 2136</td></tr>
                           <tr>
                           <td style='text-align:right;color:#459bca;font-size: 12px;'>www.ciif-global.org</td></tr>

                 
              </table>
            </td>
        </tr>
               
      </table>
      <br><br><br><br>";
		$processing_fee .=" <table  align='center' border='0' style='border-collapse: collapse;width: 80%;font-size: 12px; '>
		<tr><td>PRIVATE	AND	CONFIDENTIAL</td><td width='40%'>RECEIPT NO:	</td></tr>
		<tr><td>$date</td><td>$number	</td></tr>

		</table><br><br><br><br>";
		$processing_fee .="<table  align='center' border='0' style='border-collapse: collapse;width: 80%;font-size: 12px; '>
		<tr><td>Ariffhidayat	Ali,<br>	
Centre	of	Consulting	@	Executive	Programmes,	<br>
INCEIF,	Jalan	Universiti	A,<br>	
59100	Kuala	Lumpur	</td></tr></table>
<br><br><br><br>";    

$processing_fee .="<table  align='center' border='0' style='border-collapse: collapse;width: 80%;font-size: 12px; '>
		<tr><td>Dear	Respected	Member,		</td></tr></table>
<br><br><br><br>";    

$processing_fee .="<table  align='center' border='1' style='border-collapse: collapse;width: 80%;font-size: 12px; '>
		<tr style='background-color:#00BCD3'><td><b>Mode Of Payment</td><td><b>Amount</td></tr>
		";    
		$total=0;
		foreach ($data as $key => $value) {
			$receipt_amount = $value['receipt_amount'].' RM';
			$mode=$value['DefinitionCode'];
			$processing_fee .="<tr><td>$mode</td>
			<td>$receipt_amount</td></tr>";
			$total =$total+$receipt_amount;
		}

		
		$processing_fee .="<tr><td align='right'>Total</td><td>$total RM</td></tr></table><br><br><br><br>";

      $processing_fee .="<table  align='center' border='0' style='border-collapse: collapse;width: 80%;font-size: 12px; '><tr><td>Thank	you	for	being	a	member	of	the	Chartered	Institute	of	Islamic	Finance	Professionals	(CIIF).		
If	you	have	any	queries	about	your	membership,	kindly	contact	our	membership	team	at	+603	2276	
5279	or	email	to	membership@ciif-global.org	
This	receipt	is	computer	generated	and	no	signature	is	required.	</td></tr></table>";
       $fieldValues['[Invoice]'] = $processing_fee;
        
        require_once '../library/dompdf/dompdf_config.inc.php';
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');
 $option = array(
            'content' => $processing_fee,
            'save' => false,
            'file_name' =>'receipt',
            'css' => '@page { margin: 20px 20px 20px 20px}
                        body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                        table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                        table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
                        table.tftable tr {background-color:#ffffff;}
                        table.tftable td {font-size:10px;padding: 5px;}
                        '
                        ,

            'header' => ''
        );
 $generate = new Cms_Common();
        $pdf = $generate->generatePdf($option);
        // $slip = Cms_Curl::__(SMS_API . '/get/Registration/do/registrationSlip', $params);

        echo $pdf;
        exit();

    }

	
}

