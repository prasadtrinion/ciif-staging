<?php
class Admin_RolesubmenuController extends  Zend_Controller_Action
{
	


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'Programmecategory');

		$this->dbMenu = new App_Model_SubMenu();
		
		     $this->userDb = new App_Model_User();


	}

	public function indexAction()
	{

		$form = new App_Form_Roleregister();
		
        $userLists = $this->dbMenu->get_role();

        $formdata = array( );
       // echo "<pre>";
       // print_r($userLists);
       // die();
       
		if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            
            if ($formdata)
				{		
					$results = $this->programDb->getProgramSearch($formdata);

					$this->view->users = $results;
				}
			
    }

		$this->view->users = $userLists;
		$this->view->formdata = $formdata;
$this->view->form = $form;
	}

	public function addAction()
	{
		$form = new App_Form_Roleregister();


		 
		$id = $this->_getParam('id');

		$this->view->id = $id;

		 $auth = Zend_Auth::getInstance();
   
        $dbSubMenu = new App_Model_Menu();
       $user = $dbSubMenu->fetchRow(array('id = ?' => $id));



		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			
			 $link = new App_Model_Link();
 			$get_link = $link->get_role($formData['link_id']);

 	
			$get_name = $get_link[0]['module'].$get_link[0]['controller'].$get_link[0]['action'];
			
				$salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
                   

                    $data = array(	'role_id'        => $user['role_id'],
                    				'menu_id'		=>	$formData['menu_id'],
                                    'user_id'        => $user['user_id'],
                                    'link_id'           => $get_name,
                                    'sub_menu'           => $formData['sub_menu'],
                                    'description'    =>	$formData['description'],
                                    'created_date' 	 => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                    'created_by' 	 => $auth->getIdentity()->id,
                                    'status'         => 1,
                                   
                              );

                                  
    
                  $user_id = $this->dbMenu->insert($data);

                    
                   

				Cms_Common::notify('success','Role successfully created');
				$this->redirect('/admin/rolesubmenu/');

			
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$form = new App_Form_Roleregister();
		$id = $this->_getParam('id');

		 $cat = $this->dbMenu->fetchRow(array("id = ?" => $id))->toArray();




		//populate
		$form->populate($cat);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				$salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
                   

                    $data = array(
                                   'sub_menu'           => $formData['sub_menu'],
                                    'description'    =>	$formData['description'],
                                    
                                   
                              );

				$this->dbMenu->update($data, array('id = ?' => $id));

				Cms_Common::notify('success','Role category successfully edited');
				$this->redirect('/admin/rolemenu');

			
		}

		$this->view->form = $form;
		$this->view->cat = $cat;
	}

	public function rolemenulistAction()
	{

		$form = new App_Form_Roleregister();
		
        $userLists = $this->dbMenu->get_role_menu();
        
		$this->view->users = $userLists;
		
$this->view->form = $form;
	}

}

