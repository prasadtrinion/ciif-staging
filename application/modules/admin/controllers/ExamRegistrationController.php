<?php
class Admin_ExamRegistrationController extends  Zend_Controller_Action
{
    protected $coursecatDb;


    public function init()
    {
        $this->_helper->layout()->setLayout('/admin');

        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->auth = Zend_Auth::getInstance();

        Zend_Layout::getMvcInstance()->assign('nav', 'Programmecategory');

        $this->programDb = new App_Model_ProgramCategory();
        $this->courseDb = new App_Model_Courses();
        $this->definationDB =new Admin_Model_DbTable_Definition();

        $this->examCentre = new Admin_Model_DbTable_ExamCenter();
        


    }

    public function indexAction()
    {
        $session  = new Zend_Session_Namespace('AdminCoursecategoryIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $form = new Admin_Form_CourseCategory();
        $this->view->form = $form;

         $cats = $this->programDb->fetchAll()->toArray();
        $learningLists = $this->definationDB->getByCode('Program Route');

        foreach ($learningLists as $opt )
        {
            $form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
        }

        $examCentre = $this->examCentre->fetchAll();

        foreach ($examCentre as $opt5 )
        {
            $form->exam_center->addMultiOption($opt5['ec_id'], $opt5['ec_name']);
        }

        $programLists = $this->definationDB->getByCode('Program Level');

        foreach ($programLists as $opt1 )
        {
            $form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
        }

        $formdata = array();

        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();


            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/exam-registration/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/exam-registration/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $this->view->title = 'Exam Registration';

        
        $results = $this->programDb->getProgram();

        if (isset($formdata['search']) && $formdata['search'])
        {
            $results = $this->programDb->getProgramSearch($formdata['search']);
        }

        

        $this->view->results = $results;
        $this->view->formdata = $formdata;

    }

    public function addAction()
    {
        $form = new Admin_Form_CourseCategory();

        $pid = $this->_getParam('pid');

        $this->view->title = "New Program Category";

        $cats = $this->programDb->fetchAll()->toArray();
        $learningLists = $this->definationDB->getByCode('Program Route');

        foreach ($learningLists as $opt )
        {
            $form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
        }

        $programMode = $this->definationDB->getByCode('Mode of Program');

        foreach ($programMode as $opt5 )
        {
            $form->mode_of_program->addMultiOption($opt5['idDefinition'], $opt5['DefinitionDesc']);
        }

        $programLists = $this->definationDB->getByCode('Program Level');

        foreach ($programLists as $opt1 )
        {
            $form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
        }
        

        //process post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            
                $fail = 0;

                $data = array(
                        'name'          => $formData['name'],
                        'code'          => $formData['code'],
                        'description'   => $formData['description'],
                        'start_date'    => $formData['start_date'],
                        'end_date'      => $formData['end_date'],
                        'program_route' => $formData['program_route'],
                        'module_type'   => $formData['module_type'],
                        'program_level' => $formData['program_level'],
                        'mode_of_program' => $formData['mode_of_program'],
                        'name_safe'     => Cms_Common::safename($formData['name']),
                    
                        'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'    => $this->auth->getIdentity()->id,
                        'active'        => 1
                    );

                $course_id = $this->programDb->insert($data);

                Cms_Common::notify('success','Programme category successfully created');
                $this->redirect('/admin/programmecategory/');

            
        }

        $this->view->form = $form;
    }

    public function registerAction()
    {
        $form = new Admin_Form_CourseCategory();

        $pid = $this->_getParam('pid');

        $this->view->title = "Add Exam Registration";

        $cats = $this->programDb->fetchAll()->toArray();
        $learningLists = $this->definationDB->getByCode('Program Route');

        foreach ($learningLists as $opt )
        {
            $form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
        }

        $programMode = $this->definationDB->getByCode('Mode of Program');

        foreach ($programMode as $opt5 )
        {
            $form->mode_of_program->addMultiOption($opt5['idDefinition'], $opt5['DefinitionDesc']);
        }

        $programLists = $this->definationDB->getByCode('Program Level');

        foreach ($programLists as $opt1 )
        {
            $form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
        }
        

        //process post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            
                $fail = 0;

                $data = array(
                        'name'          => $formData['name'],
                        'code'          => $formData['code'],
                        'description'   => $formData['description'],
                        'start_date'    => $formData['start_date'],
                        'end_date'      => $formData['end_date'],
                        'program_route' => $formData['program_route'],
                        'module_type'   => $formData['module_type'],
                        'program_level' => $formData['program_level'],
                        'mode_of_program' => $formData['mode_of_program'],
                        'name_safe'     => Cms_Common::safename($formData['name']),
                    
                        'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'    => $this->auth->getIdentity()->id,
                        'active'        => 1
                    );

                $course_id = $this->programDb->insert($data);

                Cms_Common::notify('success','Programme category successfully created');
                $this->redirect('/admin/programmecategory/');

            
        }

        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->_getParam('id');

        $cat = $this->programDb->fetchRow(array("id = ?" => $id))->toArray();



        if (empty($cat)) {
            throw new Exception('Invalid Program Category');
        }

        $form = new Admin_Form_CourseCategory();

        $learningLists = $this->definationDB->getByCode('Program Route');

        foreach ($learningLists as $opt )
        {
            $form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
        }

        $programMode = $this->definationDB->getByCode('Mode of Program');

        foreach ($programMode as $opt5 )
        {
            $form->mode_of_program->addMultiOption($opt5['idDefinition'], $opt5['DefinitionDesc']);
        }


        $programLists = $this->definationDB->getByCode('Program Level');

        foreach ($programLists as $opt1 )
        {
            $form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
        }

        //modified
        if ($cat['updated_by'] != null)
        {
            $userDb = new App_Model_User;
            $user = $userDb->getUser($cat['updated_by']);
            $this->view->modified_user = $user;
        }

        $this->view->title = "Edit Program Category";

        $cats = $this->programDb->fetchAll(array('active = 1'))->toArray();


        foreach ( $this->programDb->printTree($this->programDb->buildTree($cats,0)) as $opt )
        {
            $form->parent_id->addMultiOption($opt['id'], $opt['name']);
        }

        //populate
        $form->populate($cat);

        //process post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

                $data = array(
                    'name'          => $formData['name'],
                        'code'          => $formData['code'],
                        'description'   => $formData['description'],
                        'start_date'    => $formData['start_date'],
                        'end_date'      => $formData['end_date'],
                        'program_route' => $formData['program_route'],
                        'program_level' => $formData['program_level'],
                        'module_type'   => $formData['module_type'],
                        'mode_of_program' => $formData['mode_of_program'],
                    'name_safe'     => Cms_Common::safename($formData['name']),
                    
                    'updated_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'updated_by'   => $this->auth->getIdentity()->id,
                    'active'        => $formData['active']
                );

                $this->programDb->update($data, array('id = ?' => $id));

                Cms_Common::notify('success','Programme category successfully edited');
                $this->redirect('/admin/programmecategory/edit/id/'.$id);

            
        }

        $this->view->form = $form;
        $this->view->cat = $cat;
    }

    public function deleteAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $id = $this->_getParam('id');

        $cat = $this->coursecatDb->fetchRow(array("id = ?" => $id))->toArray();

        if (empty($cat)) {
            throw new Exception('Invalid Course Category');
        }

        //move all existing categories
        $this->courseDb->update(array('category_id' => 0), array('category_id = ?' => $id) );


        //delete
        $this->coursecatDb->delete(array('id = ?' => $id));

        Cms_Common::notify('success','Course category successfully deleted');
        $this->redirect('/admin/coursecategory/');
    }


}

