<?php
class Admin_AjaxController extends Zend_Controller_Action
{
	protected $db;

	public function init()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$this->db = getDB();
	}

	public function courseCodeAction()
	{
		$value = $this->_getParam('code');
		$current = $this->_getParam('current');

		$courseDb = new App_Model_Courses();

		$sql = $courseDb->select()->where('code =?',$value);

		if ( $current != '' )
		{
			$sql->where('code != ?', $current);
		}

		$results = $this->db->fetchAll( $sql );

		$output = array();

		if ( empty($results) )
		{
			$output['valid'] = 'true';
		}
		else
		{
			$output['valid'] = 'false';
		}


		echo Zend_Json::encode($output);
	}

	public function deleteContentAction()
	{
		$id = $this->_getParam('data_id');

		if ( $id != '' )
		{
			$model = new App_Model_ContentBlockData();

			$data = $model->fetchRow(array('data_id = ?' => $id));

			if ( empty($data) )
			{
				$data = array('msg' => 'Invalid Content ID');
				echo Zend_Json::encode($data);
				exit;
			}

			$data = $data->toArray();

			$model->deleteContent($data);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}

	public function toggleContentAction()
	{
		$id = $this->_getParam('data_id');

		if ( $id != '' )
		{
			$model = new App_Model_ContentBlockData();

			$data = $model->fetchRow(array('data_id = ?' => $id));

			if ( empty($data) )
			{
				$data = array('msg' => 'Invalid Content ID');
				echo Zend_Json::encode($data);
				exit;
			}

			$data = $data->toArray();

			//toggle
			$update = array(
								'trackable' => $data['trackable'] == 1 ? 0 : 1,
								'modified_date'	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
								'modified_by' => Zend_Auth::getInstance()->getIdentity()->id
			);

			$model->update($update, array("data_id = ?" => $id));

			$data = array('msg' => '','res' => $update['trackable']);

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}

	public function deleteBlockAction()
	{
		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$model = new App_Model_ContentBlock();
			$model->delete(array('id = ?' => $id ));

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}

	public static function getYoutubeId($text) {
		$text = preg_replace('~
        # Match non-linked youtube URL in the wild. (Rev:20130823)
        https?://         # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube         # or youtube.com or
          (?:-nocookie)?  # youtube-nocookie.com
          \.com           # followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\s-]       # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w-]{11})      # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w-]|$)     # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w.-]*    # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w.-]*        # Consume any URL (query) remainder.
        ~ix',
			'$1',
			$text);
		return $text;
	}

	public function checkUrlAction()
	{
		$url = $this->_getParam('url');

		$url = $url != '' && !preg_match("/(http|https):\/\/(.*)\.(.*)/", $url) ? 'http://'.$url : $url;

		if ( empty($url) || !preg_match("/(http|https):\/\/(.*)\.(.*)/", $url) )
		{
			$data = array('msg' => 'Invalid Url');

			echo Zend_Json::encode($data);
			return;
		}

		//youtube link
		$isyoutube = 0;
		$youtube_id = self::getYoutubeId($url) ;
		if ( !preg_match('/http/i',$youtube_id) )
		{
			$isyoutube = 1;
		}

		if ( $isyoutube == 1 )
		{
			$get = file_get_contents('https://www.googleapis.com/youtube/v3/videos?id='.$youtube_id.'&key='.API_YOUTUBE.'&part=snippet');

			$video_data  = json_decode($get, true);

			$thumb = $video_data['items'][0]['snippet']['thumbnails']['standard']['url'];
			$data = array('msg' => '','title' => $video_data['items'][0]['snippet']['title'],'thumb'=>$thumb);

			echo Zend_Json::encode($data);
			return;
		}


		//step 1 - fetch the page
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 25);
		@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$content = curl_exec($ch);
		curl_close($ch);

		//step 2 - analyze the results
		if ( $content == '' )
		{
			$data = array('msg' => '404. Page is empty');

			echo Zend_Json::encode($data);
			return;
		}



		$title = Cms_Common::getTag($content,'<title>','</title>');

		$data = array('msg' => '','title' => trim(htmlspecialchars($title)));

		echo Zend_Json::encode($data);
	}

	public function deleteFileAction()
	{
		$fileDb = new App_Model_DataFiles();

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$file = $fileDb->fetchRow(array('id = ?' => $id));

			if ( empty($file) )
			{
				$data = array('msg' => 'Invalid ID');

				echo Zend_Json::encode($data);
				return;
			}

			$file = $file->toArray();

			$fileDb->delete(array('id = ?' => $id));

			//unlink
			@unlink(DOCUMENT_PATH . $file['file_url']);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
		}
	}

	public function getSubTypeAction()
    {
        
        $this->_helper->layout->disableLayout();

        $memberId = $this->_getParam('member_id',null);
        // die();
        $query =  new Admin_Model_DbTable_MemberGet();

        $memberdata = $query->getmemberdata($memberId);
        $json = Zend_Json::encode($memberdata);

        
                    
                        print_r($json);
                       // die();

         exit();
    }


	
}