<?php 
class Admin_ReportingController extends  Zend_Controller_Action 
{
    protected $alphabets;
    protected $alphabets_flip;

    public function init()
    {
        $this->_helper->layout()->setLayout('/admin');
        Zend_Layout::getMvcInstance()->assign('nav', 'reporting');
        
        $alphabets = array();
        $alp       = 'A';
        while ($alp !== 'AAA')
        {
            $alphabets[] = $alp++;
        }
        $this->alphabets      = array_combine(range(1, count($alphabets)), array_values($alphabets));
        $this->alphabets_flip = array_flip($this->alphabets);
    }

    public function indexAction()
    {

    }

    public function completionStatusAction() 
    {
        $this->view->title = "List of Users (Completion Status)";

        $coursesDb = new App_Model_Courses();

        $tmp = $coursesDb->getCourses(array('id', 'code', 'title'), true);
        $courses      = array();
        $courses_list = array();
        $courses_code = array();

        foreach ($tmp as $course)
        {
            $course['type']              = null;
            $courses[$course['code']]    = $course;
            $courses_list[$course['id']] = $course['code'] .' - '. $course['title'];
            $courses_code[]              = $course['code'];
        }

        $countries = Cms_Common::getCountryList();
        $this->view->countries = $countries;

        $this->view->courses_list = $courses_list;

        if (!$this->getRequest()->isPost())
        {
            return;
        }

        ini_set('max_execution_time', 0);

        $formData = $this->getRequest()->getPost();

        $scorms       = array();
        $scorms_users = array();
        $nationality  = null;

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $dataScormDb = new App_Model_DataScorm();
        $userScormDb = new App_Model_UserScorm();

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'enrol'), array('enrol_id' => 'id', 'course_id', 'completed'))
            ->join(array('b' => 'user'), 'b.id = a.user_id', array('user_id' => 'id', 'firstname', 'lastname', 'nationality'))
            ->join(array('c' => 'courses'), 'c.id = a.course_id', array('code', 'title'))
            ->joinLeft(array('d' => 'external_data'), 'd.code = b.company AND d.type_id = 4', array('company' => 'name'))
//            ->joinLeft(array('e' => 'user_group_data'), "e.user_id = b.id", array('gdata_id'))
//            ->joinLeft(array('f' => 'user_group' ), "f.course_id = c.id AND f.group_id = e.group_id", array("group_id"))

            ->joinLeft(array('e' => 'user_group'), 'e.course_id = c.id', array())
            ->joinLeft(array('f'=> 'user_group_data'),'f.group_id = e.group_id and f.user_id = b.id', array('gdata_id', 'group_id' => "GROUP_CONCAT(f.group_id)"))
            ->group('a.id')
            ->where('a.regtype = ?', 'course')
            ->where('a.learningmode = ?', 'OL')
            ->order('c.code')
            ->order('b.firstname')
            ->order('a.id')
            ->order('CONCAT_WS(" ", b.firstname, b.lastname)');

        if (isset($formData['course']) && $formData['course'])
        {
            $select->where('c.id = ?', $formData['course']);
            $scorms  = $dataScormDb->getDataByCourse($formData['course']);
        }

        if (isset($formData['nationality']) && $formData['nationality'])
        {
            $select->where('b.nationality = ?', $formData['nationality']);
            $nationality = $formData['nationality'];
        }

        $enrols = $db->fetchAll($select);

        $smsDB = getDB2();
        $select = $smsDB->select()
            ->from(array('a' => 'tbl_subjectmaster'), array('SubCode', 'SubjectName'))
            ->join(array('b' => 'tbl_landscapesubject'), 'b.IdSubject = a.IdSubject', array())
            ->join(array('c' => 'tbl_program'), 'c.IdProgram = b.IdProgram', array())
            ->joinLeft(array("z" => "tbl_definationms"), "z.idDefinition = c.programme_type", array("type" => "DefinitionDesc"))
            ->where('a.SubCode in (?)', $courses_code);

        $subjects = $smsDB->fetchAll($select);

        foreach ($subjects as $row)
        {
            $SubCode = $row['SubCode'];

            if (isset($courses_list[$SubCode]))
            {
                $courses[$SubCode]['type'] = $row['type'];
            }   
        }

        // ---------- EXCEL ----------
        $objPHPExcel = self::createExcel($this->view->title);

        $headers = array('No', 'Name', 'Nationality', 'Company', 'Type', 'Course Code', 'Course', 'Completion Status');
        self::setExcelHeaders($objPHPExcel, $headers);

        $cell_alp = self::traverseAlphabet('A', count($headers));

        if ($scorms && count($scorms))
        {
            $cell_num = 1;
            $value    = 'SCORM';
            self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, true, 'FFFFFF', '16365C');

            $merge_alp = self::traverseAlphabet($cell_alp, count($scorms)-1);
            $objPHPExcel->getActiveSheet()->mergeCells($cell_alp. '1:'. $merge_alp .'1');
            self::setAlignCenter($objPHPExcel, $cell_alp.$cell_num);

            $merge_alp = self::traverseAlphabet($cell_alp, -1);
            $objPHPExcel->getActiveSheet()->mergeCells('A2:'. $merge_alp .'2');
            self::setCellValue($objPHPExcel, 'A2', '', true, true, 'FFFFFF', '16365C');

//
        }


        $cell_alp = self::traverseAlphabet('A', count($headers));

        foreach ($scorms as $i => $row)
        {
            $data_id  = $row['data_id'];
            $cell_num = 2;
            $value    = $row['title'];
            self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, true, 'FFFFFF', '0774c6');
            self::setAlignCenter($objPHPExcel, $cell_alp . $cell_num);
            $cell_alp  = self::traverseAlphabet($cell_alp);

            $users = $userScormDb->checkForCompletedUser($data_id, array('nationality' => $nationality));
            $scorms[$data_id]['users'] = $users;
        }

        foreach ($enrols as $i => $row)
        {
            $user_id = $row['user_id'];

            foreach ($scorms as $data_id => $scorm)
            {
                $status = null;

                if (empty($scorm['groups']))
                {
                    if (isset($scorm['users]'][$user_id]))
                    {
                        $status = ($scorm['users'][$user_id]['status'] == 'completed' ? 'Completed' : 'Incomplete');
                    }
                    else
                    {
                        $status = 'No Attempt';
                    }
                }
                else
                {
                    $user_groups  = (empty($row['group_id']) ? array() : explode(',', $row['group_id']));
                    $scorm_groups = $scorm['groups'];
                    $has_access   = (!empty(array_intersect($user_groups, $scorm_groups)) ? 1 : 0);

                    if ($has_access)
                    {
                        if (isset($scorm['users]'][$user_id]))
                        {
                            $status = ($scorm['users'][$user_id]['status'] == 'completed' ? 'Completed' : 'Incomplete');
                        }
                        else
                        {
                            $status = 'No Attempt';
                        }
                    }
                    else
                    {
                        $status = 'Hidden (Different Learning Mode)';
                    }
                }
                $enrols[$i]['scorms'][$data_id] = $status;
            }
        }

        $objPHPExcel->getActiveSheet()->setTitle('Online');

        $cell_num = (count($scorms) ? 3 : 2);

        foreach ($enrols as $i => $row) {
            $cell_alp = 'A';
            $value    = $i + 1;
            $value = $row['user_id'];
            self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, false);

            $cell_alp = self::traverseAlphabet($cell_alp);
            $value    = $row['firstname'] . ' ' . $row['lastname'];
            self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, false);

            $cell_alp = self::traverseAlphabet($cell_alp);
            $value    = (isset($countries[$row['nationality']]) ? $countries[$row['nationality']] : '');
            self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, false);

            $cell_alp = self::traverseAlphabet($cell_alp);
            $value    = $row['company'];
            self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, false);

            $cell_alp = self::traverseAlphabet($cell_alp);
            $value    = $courses[$row['code']]['type'];
            self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, false);

            $cell_alp = self::traverseAlphabet($cell_alp);
            $value    = $row['code'];
            self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, false);

            $cell_alp = self::traverseAlphabet($cell_alp);
            $value    = $row['title'];
            self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, false);

            $cell_alp = self::traverseAlphabet($cell_alp);
            $value    = ($row['completed'] ? 'Completed' : 'Incomplete');
            self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, false);

            if (isset($row['scorms']) && count($row['scorms']))
            {
                foreach ($row['scorms'] as $data_id => $status)
                {
                    $cell_alp = self::traverseAlphabet($cell_alp);
                    $value    = $status;
                    self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value, true, false);
                }
            }

            $cell_num++;
        }

        self::generateExcel($objPHPExcel, 'Report_of_Completion_Status');

    }

    private function traverseAlphabet($alp, $move = 1)
    {
        $index = $this->alphabets_flip[$alp] + $move;

        if (isset($this->alphabets[$index]))
        {
            return $this->alphabets[$index];
        }

        die('Invalid alphabet index #'. $index);
    }

    private function createExcel($title = 'IBFIM')
    {
        require_once('../library/PHPExcel/Classes/PHPExcel.php');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("IBFIM");
        $objPHPExcel->getProperties()->setLastModifiedBy("IBFIM");
        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setSubject($title);
        $objPHPExcel->getProperties()->setDescription($title);
        $objPHPExcel->setActiveSheetIndex(0);

        return $objPHPExcel;
    }

    private function setExcelHeaders(&$objPHPExcel, $headers = array(), $cell_num = 1, $cell_alp = 'A') {

        foreach ($headers as $i => $desc)
        {
            self::setCellValue($objPHPExcel, $cell_alp.$cell_num, $desc, true, true, 'FFFFFF', '16365C');
            self::setAlignCenter($objPHPExcel, $cell_alp.$cell_num);
            $cell_alp = self::traverseAlphabet($cell_alp);
        }

        $cell_num++;
        return $cell_num;
    }

    private function setCellValue(&$objPHPExcel, $cell, $value = '', $border = false, $bold = false, $color = null, $background = null)
    {
        $objPHPExcel->getActiveSheet()->SetCellValue($cell, "$value");

        if ($border)
        {
            self::setCellBordered($objPHPExcel, $cell);
        }

        if ($bold) 
        {
            self::setCellBold($objPHPExcel, $cell);
        }

        if ($color)
        {
            self::setCellColor($objPHPExcel, $cell, $color);
        }

        if ($background)
        {
            self::setCellBackgroundColor($objPHPExcel, $cell, $background);
        }
    }

    private function setCellBordered(&$objPHPExcel, $cell)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    }

    private function setCellBold(&$objPHPExcel, $cell)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setBold(true);
    }

    private function setCellColor(&$objPHPExcel, $cell, $color)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->getColor()->setRGB($color);
    }

    private function setCellBackgroundColor(&$objPHPExcel, $cell, $color)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($color);
    }

    private function setCellFormatNumber(&$objPHPExcel, $cell)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
    }

    private function setAlignCenter(&$objPHPExcel, $cell)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    }

    private function generateExcel(&$objPHPExcel, $filename = 'report')
    {
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$filename.'.xls"');

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save('php://output');
    }
}
?>