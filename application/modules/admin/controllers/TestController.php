<?php
class Admin_TestController extends  Zend_Controller_Action
{
    public function curlAction()
    {
        $requests = array();
        
        $params   = array('test' => 1);
        $request  = array(
            'url'      => SMS_API . '/get/Registration/do/test',
            'body'     => $params,
            'response' => array()
        );
        
        try {
            $request['response'] = Cms_Curl::__($request['url'], $request['body']);
        }
        catch (Exception $e)
        {
            $request['response'] = $e->getMessage();
        }
        $requests[] = $request;
        
        
        if (substr($request['url'], 0, 5) == 'http:')
        {
            $request['url'] = str_replace('http', 'https', $request['url']);
        }
        else
        {
            $request['url'] = str_replace('https', 'http', $request['url']);
        }
        
        try {
            $request['response'] = Cms_Curl::__($request['url'], $request['body']);
        }
        catch (Exception $e)
        {
            $request['response'] = $e->getMessage();
        }
        $requests[] = $request;
        
        pr($requests);
        exit;
    }
}