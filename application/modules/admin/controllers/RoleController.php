<?php
class Admin_RoleController extends  Zend_Controller_Action
{
	protected $coursecatDb;


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');
		 error_reporting(0);
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'Programmecategory');

		$this->dbUser = new App_Model_User();
		$this->dbMenu = new App_Model_Menu();
		
		


	}

	public function indexAction()
	{

		$form = new App_Form_Roleregister();
		
        $userLists = $this->dbUser->get_admin_role();

        $formdata = array( );
      
       
		$this->view->users = $userLists;
        
		if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

      
            if ($formdata)
				{		
					$results = $this->dbUser->get_role_search($formdata['role_id']);

					$this->view->users = $results;
				}
			
    }


		

		$this->view->formdata = $formdata;
        $this->view->form = $form;
	}

	public function addAction()
	{
		$form = new App_Form_Roleregister();


		 $dbUser = new App_Model_User;
		

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			
			
				$salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
                   

                    $data = array(
                                    'username'      => $formData['username'],
                                    'role_id'      => $formData['role_id'],
                                    'password'      => $hash,
                                   
                                    'salt'          => $salt,
                                    'firstname'     => $formData['firstname'],
                                    'lastname'      => $formData['lastname'],
                                    'email'         => $formData['email'],
                                    'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                    'role'          => 'administrator',
                                    'contactno'     => $formData['c_code'].'-'.$formData['contactno'],
                                    'approve_status'=> 1,
                                    'active'        => 1,
                                   
                              );

                                  
    
                  $user_id = $dbUser->insert($data);

                    
                   

				Cms_Common::notify('success','Role successfully created');
				$this->redirect('/admin/role/');

			
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$form = new App_Form_Roleregister();
		$id = $this->_getParam('id');

		$cat = $this->dbUser->fetchRow(array("id = ?" => $id))->toArray();




		//populate
		$form->populate($cat);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				$salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
                   

                    $data = array(
                                    'username'      => $formData['username'],
                                    'role_id'      => $formData['role_id'],
                                    'firstname'     => $formData['firstname'],
                                    'lastname'      => $formData['lastname'],
                             );

				$this->dbUser->update($data, array('id = ?' => $id));

				Cms_Common::notify('success','Role category successfully edited');
				$this->redirect('/admin/role/');

			
		}

		$this->view->form = $form;
		$this->view->cat = $cat;
	}

	public function rolelistAction()
	{
$form = new App_Form_Roleregister();
		
        $userLists = $this->dbMenu->get_role_menu();
        
		$this->view->users = $userLists;
		
$this->view->form = $form;
	
	}


	public function rolelistpermissionAction()
	{

		$form = new App_Form_Roleregister();
		
        $userLists = $this->dbUser->get_admin_role_list();

        
		$this->view->users = $userLists;
		
$this->view->form = $form;
	}

	public function roleaccessAction()
	{
		$menudb =new App_Model_Menu();

		$submenudb =new App_Model_SubMenu();

		$form = new App_Form_Roleregister();
		
		$id = $this->_getParam('id');
			if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
				$access['access'] = 0;

			$menudb->update($access, array('role_id = ?' => $id));
				$data['access'] = 0;

				$submenudb->update($data, array('role_id = ?' => $id));
			foreach ($formData['m_menu'] as  $value) {
				$access['access'] = 1;

				$menudb->update($access, array('id = ?' => $value));
			}

			foreach ($formData['sub'] as $value1) {
				$data['access'] = 1;

				$submenudb->update($data, array('id = ?' => $value1));
			}

			
		}
		 
                    $listmenu = $menudb->get_access_menu($id);
        
		$this->view->listmenu = $listmenu;
		
$this->view->form = $form;
	}

}

