<?php
class Admin_ProgrammeFeeController extends  Zend_Controller_Action
{
	


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'Programmecategory');

		$this->programDb = new App_Model_ProgrammeFee();
		$this->program = new App_Model_ProgramCategory();
		$this->gst = new Admin_Model_DbTable_Gst();
	
		$this->definationDB =new Admin_Model_DbTable_Definition();
		


	}

	public function indexAction()
	{

		$form = new Admin_Form_CourseCategory();
		$this->view->form = $form;

		$session  = new Zend_Session_Namespace('AdminCoursecategoryIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        $learningLists = $this->definationDB->getByCode('Program Route');
        $programLists = $this->definationDB->getByCode('Program Level');
        $programCategoryLists = $this->program->fetchAll();


        $results = $this->programDb->getfee();

		foreach ($learningLists as $opt )
		{
			$form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
		}

		foreach ($programLists as $opt1 )
		{
			$form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
		}

		foreach ($programCategoryLists as $opt2 )
		{
			$form->program_category->addMultiOption($opt2['id'], $opt2['name']);
		}



        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            
            if ($formdata)
				{		
					$results = $this->programDb->get_fee_search($formdata);
				}
			
    }

		$this->view->title = 'Program Categories';
		$this->view->results = $results;
		$this->view->formdata = $formdata;


	}

	public function addAction()
	{
		$form = new Admin_Form_CourseCategory();

		$currencyDb = new Admin_Model_DbTable_Currency();
		$cur_date = $currencyDb->getDate();
		$this->view->cur_date = $cur_date;
		// echo "<pre>";
		// print_r($cur_date);
		// die();
		$pid = $this->_getParam('pid');

		$this->view->title = "Program Fee";

		$cats = $this->programDb->fetchAll()->toArray();
		$learningLists = $this->definationDB->getByCode('Program Route');

		$gst = $this->gst->fetchAll();

        

		foreach ($gst as $opt4 )
		{
			$form->gst->addMultiOption($opt4['percentage'], $opt4['name']);
		}

		foreach ($learningLists as $opt )
		{
			$form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
		}

		$programCategoryLists = $this->program->fetchAll();

		foreach ($programCategoryLists as $opt2 )
		{
			$form->program_category->addMultiOption($opt2['id'], $opt2['name']);
		}

		$programLists = $this->definationDB->getByCode('Program Level');

		foreach ($programLists as $opt1 )
		{
			$form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
		}

		$amountLists = $this->definationDB->getByCode('Currency');

		foreach ($amountLists as $opt3 )
		{
			$form->amount_type->addMultiOption($opt3['idDefinition'], $opt3['DefinitionDesc']);
		}
		

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();


			
				$fail = 0;

				$data = array(
						
						
			'effective_date'	=> $formData['effective_date'],
			'amount'			=> $formData['amount'],
			'amount_type'		=> $formData['amount_type'],
			'program_category'	=> $formData['program_category'],
			'program_route'	    => $formData['program_route'],
			'program_level'	    => $formData['program_level'],
			'gst'               => $formData['gst'],
			'calculated_amount'	=> $formData['calculated_amount'],
			'description'	    => $formData['description'],
			'effective_date'=> $formData['effective_date'],
			'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
			'created_by'    => $this->auth->getIdentity()->id,
			'active'		=> $formData['active']
					);

				$course_id = $this->programDb->insert($data);

				Cms_Common::notify('success','Program Fee successfully created');
				$this->redirect('/admin/programme-fee/');

			
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->_getParam('id_program');
		
$form = new Admin_Form_CourseCategory();
	$cat = $this->programDb->fetchRow(array("id_program = ?" => $id))->toArray();

		if (empty($cat)) {
			throw new Exception('Invalid Program Category');
		}

		$learningLists = $this->definationDB->getByCode('Program Route');

		foreach ($learningLists as $opt )
		{
			$form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
		}

		$programCategoryLists = $this->program->fetchAll();

		$gst = $this->gst->fetchAll();

        

		foreach ($gst as $opt4 )
		{
			$form->gst->addMultiOption($opt4['percentage'], $opt4['name']);
		}

		foreach ($programCategoryLists as $opt2 )
		{
			$form->program_category->addMultiOption($opt2['id'], $opt2['name']);
		}

		$programLists = $this->definationDB->getByCode('Program Level');

		foreach ($programLists as $opt1 )
		{
			$form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
		}

		$amountLists = $this->definationDB->getByCode('Currency');

		foreach ($amountLists as $opt3 )
		{
			$form->amount_type->addMultiOption($opt3['idDefinition'], $opt3['DefinitionDesc']);
		}

		

		//modified
		if ($cat['updated_by'] != null)
		{
			$userDb = new App_Model_User;
			$user = $userDb->getUser($cat['updated_by']);
			$this->view->modified_user = $user;
		}

		$this->view->title = "Edit Program Fee";

		$cats = $this->program->fetchAll(array('active = 1'))->toArray();


		// foreach ( $this->programDb->printTree($this->programDb->buildTree($cats,0)) as $opt )
		// {
		// 	$form->parent_id->addMultiOption($opt['id'], $opt['name']);
		// }

		//populate
		$form->populate($cat);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			$data = array(
			'effective_date'		=> $formData['effective_date'],
						'amount'		=> $formData['amount'],
					'amount_type'		=> $formData['amount_type'],
						'program_category'	=> $formData['program_category'],
						'program_route'		=> $formData['program_route'],
						'program_level'		=> $formData['program_level'],
						'gst'           	=> $formData['gst'],
						'calculated_amount'	=> $formData['calculated_amount'],
						'description'		=> $formData['description'],
						'effective_date'	=> $formData['effective_date'],
					
						'updated_date'		=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'updated_by'   		=> $this->auth->getIdentity()->id,
						'active'			=> $formData['active']
				);

				$this->programDb->update($data, array('id_program = ?' => $id));

				Cms_Common::notify('success','Programme Fee successfully edited');
				$this->redirect('/admin/programme-fee');

			
		}

		$this->view->form = $form;
		$this->view->cat = $cat;
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$cat = $this->coursecatDb->fetchRow(array("id = ?" => $id))->toArray();

		if (empty($cat)) {
			throw new Exception('Invalid Course Category');
		}

		//move all existing categories
		$this->courseDb->update(array('category_id' => 0), array('category_id = ?' => $id) );


		//delete
		$this->coursecatDb->delete(array('id = ?' => $id));

		Cms_Common::notify('success','Course category successfully deleted');
		$this->redirect('/admin/coursecategory/');
	}

	public function getprogramAction()
{
	$this->_helper->layout->disableLayout();
	$programDb = new App_Model_ProgramCategory();

	$program_route = $this->_getParam('program_route',null);
	$program_level = $this->_getParam('program_level',null);
	
	$programdata = $programDb->getprogramdata($program_route,$program_level);
	 $json = Zend_Json::encode($programdata);
         print_r($json);

	 exit();
}

public function validiateProgramAction()
{
	$this->_helper->layout->disableLayout();
	$programDb = new App_Model_ProgramCategory();

	$program_route = $this->_getParam('program_route',null);
	$program_level = $this->_getParam('program_level',null);
	$program_category = $this->_getParam('program_category',null);
	$programdata = $programDb->programvaliadiate($program_route,$program_level,$program_category);

	
     if(!empty($programdata))
     {
     	echo 0;
     }else{
     	$programdata = $programDb->getprogramdata($program_route,$program_level);
     	 $json = Zend_Json::encode($programdata);
         print_r($json);
     }
	 exit();
}



}

