<?php
class Admin_BarringController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
		
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_Barring(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view	
		$lobjemailTemplateModel = new Admin_Model_DbTable_Barring();  // email template model object
		$larrresult = $lobjemailTemplateModel->fnGetTemplateDetails(); // get template details	
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();	
							
				if ($lobjsearchform->isValid($larrformData)) {	
					
					$larrresult = $lobjemailTemplateModel->fnSearchTemplate($lobjsearchform->getValues());						
					if(empty($larrresult))
					{
						$this->_redirect( $this->baseUrl . '/admin/barring/index');
					}				
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
			$this->_redirect( $this->baseUrl . '/admin/barring/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function addAction() { 	
  					$member_form = new Admin_Form_Barring();
            $this->view->form = $member_form;

            if($this->getRequest()->isPost())
            {    
        

                $formdata =$this->getRequest()->getPost();
                
                if($member_form->isValid($formdata))
                {

                    // echo "<pre>";
                    // print_r($formdata);
                    // die();
                $barring = $member_form->getValue('barring_name'); 

                    $amount = $member_form->getValue('amount');
                      
                    $add_member = new Admin_Model_DbTable_Barring();
                    $add_member->addData($barring,$amount);
                    if($add_member)
                    {
                        $this->_redirect( $this->baseUrl . '/admin/barring/index');
                    }
                    
                }


            }
	}

	public function editAction() {		
 		
 	 	
            $editform = new Admin_Form_Barring();
    		$this->view->form = $editform;
		if($this->getRequest()->isPost())

		{

			$formData = $this->getRequest()->getPost();
			if($editform->isValid($formData))
		{

			$id = $this->getRequest()->getparam('Id');                                                                               //ine 4
			$barring=$editform->getValue('barring_name');

             $amount=$editform->getValue('amount');
                 
				$edit_member = new Admin_Model_DbTable_Barring();
				$edit_member->editmember($id,$barring,$amount);

                    if($edit_member)
                    {
                         $this->_redirect( $this->baseUrl . '/admin/barring/index');
                    }

                  //Line 5
//$this->_helper->redirector('index');
			}

		else
		{
    		 $editform->populate($formData);
		}


		}
		 else
		              {
                        $id = $this->getRequest()->getparam('Id');
                        	
                    
                         $file = new Admin_Model_DbTable_Barring();
                         $files = $file->fetchRow('id_barring='.$id);
                         $editform->populate($files->toArray());
                        
                     }    
$this->view->form = $editform;  	  	
}
}