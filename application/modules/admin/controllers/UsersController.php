<?php
header('Access-Control-Allow-Origin: *');
class Admin_UsersController extends  Zend_Controller_Action
{
    protected $userDb;
    protected $uploadDir;

    public function init()
    {
    error_reporting(0);
       $this->_helper->layout()->setLayout('/admin');

        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->auth = Zend_Auth::getInstance();

       
        $this->userDb = new App_Model_User();
        

        $this->view->folder_name = '/userphoto';
        $this->uploadDir = DOCUMENT_PATH . $this->view->folder_name;
        $this->view->uploadDir = $this->uploadDir;
         $this->programReg = new App_Model_ProgramRegister();
         


       $this->definationDB =new Admin_Model_DbTable_Definition();
       $auth = Zend_Auth::getInstance();
        $loggedIn = $auth->hasIdentity() ? 1 : 0;
        if ( $loggedIn )    
            {
                $fullname = $auth->getIdentity()->firstname.' '.$auth->getIdentity()->lastname;

  // for chat
                $onlineDb = new App_Model_UserOnline();
                $onlineusers = $onlineDb->getOnline();
                $totalonlineusers = $onlineDb->getOnlineInt();
            }else{
                     $this->redirect('/login');
            }

    }

    public function indexAction()
    {
        $onlineDb = new App_Model_UserOnline();
        $onlineusers = $onlineDb->getOnlineIds();
        $userDb = new App_Model_User();
        $session  = new Zend_Session_Namespace('AdminUsersIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        if ($this->getRequest()->isPost()) {



            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/users/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/users/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $check_payment_status = 1;

        $getusers = $this->userDb->get_app_status();

        if ($search) {
            
            $getusers = $userDb->getUserfirstname($search);
                  }
        if (isset($formData['Approve']) && $formData['Approve']=='Approve'){
                
                if (isset($formData['at_trans_id']) && count($formData['at_trans_id']) > 0){
                   
                    $i=0;
                    
                    foreach ($formData['at_trans_id'] as $transLoop){
                        
                        $this->offered($transLoop);
                        $i++;
                    }
                }
                $searchForm = new Application_Form_ApplicantSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);

                $this->_redirect( '/application/applicant-approval/index/');
            }

        // /$users = Cms_Paginator::arrays($getusers);
        // echo '<pre>';
        // print_r($getusers);
        // die();
        $this->view->onlineusers = $onlineusers;
        $this->view->users       = $getusers;
        $this->view->formdata    = $formdata;

    }


     public function deleteAction()
    {
        $id = $this->getParam('id');

        $user = $this->userDb->delete(array('id = ?' => $id));
        $this->redirect("admin/users/");
        
    }

public function listMemberAction()
    {
        $onlineDb = new App_Model_UserOnline();
        $onlineusers = $onlineDb->getOnlineIds();
        $userDb = new App_Model_User();
        $session  = new Zend_Session_Namespace('AdminUsersIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        if ($this->getRequest()->isPost()) {



            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/users/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/users/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $check_payment_status = 1;

        $getusers = $this->userDb->get_member_status();

        if ($search) {
            
            $getusers = $userDb->getUserfirstname($search);
                  }
        if (isset($formData['Approve']) && $formData['Approve']=='Approve'){
                
                if (isset($formData['at_trans_id']) && count($formData['at_trans_id']) > 0){
                   
                    $i=0;
                    
                    foreach ($formData['at_trans_id'] as $transLoop){
                        
                        $this->offered($transLoop);
                        $i++;
                    }
                }
                $searchForm = new Application_Form_ApplicantSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);

                $this->_redirect( '/application/applicant-approval/index/');
            }

        // /$users = Cms_Paginator::arrays($getusers);
        // echo '<pre>';
        // print_r($getusers);
        // die();
        $this->view->onlineusers = $onlineusers;
        $this->view->users       = $getusers;
        $this->view->formdata    = $formdata;

    }



    public function userpayAction()

    {
         $dbUser   = new App_Model_User;
          $memberModel = new App_Model_Member();
        $memberReg =new App_Model_Membership_MemberRegistration();
        $get_mail = new Admin_Model_DbTable_Emailtemplate();
        
         $get_user = $dbUser->get_payed_list();
               if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
           

            foreach ($formData as $key => $value) {
                # code...
                $data = array('payment_status' => 1);
                 $query_result= $dbUser->update($data, array('id = ?' => $key));
                echo "<script>parent.location='userpay'</script>";exit;
                 // $userModel = new App_Model_User();
                 // $get_user_detail = $dbUser->getreguser($key);

        //          $appl_id = $get_user_detail['id'] ;
        //  $applicant_email = $get_user_detail['email'];
        //  $applicant_name = $get_user_detail['firstname'].' '.$get_user_detail['lastname'];
         
       
        


        //      $mail_detail = $get_mail->fnViewTemplte(20);

            
        // $uniqueId = 'CIIFP'.date('my');
        // $membershipId = $uniqueId.$appl_id;
       

        // if($query_result)
        // {
        //     $data = array('mr_membershipId' => $appl_id,
        //     'mr_sp_id' =>  $membershipId,
        //     'mr_status'=> 1384,
        //     'mr_activation_date' => date('Y-m-d H:i:s'),
        //     'mr_expiry_date' => date('Y-m-d H:i:s', strtotime(date("Y-m-d", time()) . " + 1 year")),
        //     'mr_payment_status' => 1);
          
        // $query = $memberReg->insert($data);
        // if($query)
        // {
        //      $mail = new Cms_SendMail();
        
        //    $message = 'Dear '.''.$applicant_name .$mail_detail['TemplateBody'];

 
        //    $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);


        //  }    

        //     }
           
            
        }

         
    }
    $this->view->get_user = $get_user;
}


    public function applicantlistAction()
    {
        $onlineDb = new App_Model_UserOnline();
        $onlineusers = $onlineDb->getOnlineIds();
        $userDb = new App_Model_User();
        $session  = new Zend_Session_Namespace('AdminUsersIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        if ($this->getRequest()->isPost()) {



            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/users/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/users/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $check_payment_status = 1;

        $getusers = $this->userDb->get_applicant();

        if ($search) {
            
            $getusers = $userDb->getUserfirstname($search);
                  }
        if (isset($formData['Approve']) && $formData['Approve']=='Approve'){
                
                if (isset($formData['at_trans_id']) && count($formData['at_trans_id']) > 0){
                   
                    $i=0;
                    
                    foreach ($formData['at_trans_id'] as $transLoop){
                        
                        $this->offered($transLoop);
                        $i++;
                    }
                }
                $searchForm = new Application_Form_ApplicantSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);

                $this->_redirect( '/application/applicant-approval/index/');
            }

        $users = Cms_Paginator::arrays($getusers);
        // echo '<pre>';
        // print_r($getusers);
        // die();
        $this->view->onlineusers = $onlineusers;
        $this->view->users       = $users;
        $this->view->formdata    = $formdata;

    }


    public function memberlistAction()
    
    {


        $onlineDb = new App_Model_UserOnline();
        $onlineusers = $onlineDb->getOnlineIds();
        $userDb = new App_Model_User();
        $session  = new Zend_Session_Namespace('AdminUsersIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();
 $getusers = $this->userDb->get_memberlist();
        if ($this->getRequest()->isPost()) {



            $formdata = $this->getRequest()->getPost();

         
    $getusers = $this->userDb->get_memberlist_search($formdata['search']);
            
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $check_payment_status = 1;

       
       
     
        

             $users = Zend_Paginator::factory($getusers);
        $users->setCurrentPageNumber($this->_getParam('page', 1));
        $users->setItemCountPerPage(20);

       
        // echo '<pre>';
        // print_r($getusers);
        // die();
        $this->view->onlineusers = $onlineusers;
        $this->view->users       = $users;
        $this->view->formdata    = $formdata;

    }




    public function memberApprovalAction()
    {
        $onlineDb = new App_Model_UserOnline();
        $onlineusers = $onlineDb->getOnlineIds();
        $userDb = new App_Model_User();
        $session  = new Zend_Session_Namespace('AdminUsersIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/users/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/users/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $check_payment_status = 1;

        $getusers = $this->userDb->get_app_status();

        if ($search) {
            
            $getusers = $userDb->getUserfirstname($search);
                  }
        if (isset($formData['Approve']) && $formData['Approve']=='Approve'){
                
                if (isset($formData['at_trans_id']) && count($formData['at_trans_id']) > 0){
                   
                    $i=0;
                    
                    foreach ($formData['at_trans_id'] as $transLoop){
                        
                        $this->offered($transLoop);
                        $i++;
                    }
                }
                $searchForm = new Application_Form_ApplicantSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);

                $this->_redirect( '/application/applicant-approval/index/');
            }

        $users = Cms_Paginator::arrays($getusers);
        // echo '<pre>';
        // print_r($getusers);
        // die();
        $this->view->onlineusers = $onlineusers;
        $this->view->users       = $users;
        $this->view->formdata    = $formdata;

    }

    public function renewusersAction()
    {
        $onlineDb = new App_Model_UserOnline();
        $onlineusers = $onlineDb->getOnlineIds();
        $userDb = new App_Model_User();
        $session  = new Zend_Session_Namespace('AdminUsersIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/users/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/users/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $check_payment_status = 1;

        $getusers = $this->userDb->get_renewal_status();

        if ($search) {
            
            $getusers = $userDb->getRenewfirstname($search);
                  }
        if (isset($formData['Approve']) && $formData['Approve']=='Approve'){
                
                if (isset($formData['at_trans_id']) && count($formData['at_trans_id']) > 0){
                   
                    $i=0;
                    
                    foreach ($formData['at_trans_id'] as $transLoop){
                        
                        $this->offered($transLoop);
                        $i++;
                    }
                }
                $searchForm = new Application_Form_ApplicantSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);

                $this->_redirect( '/application/applicant-approval/index/');
            }

        $users = Cms_Paginator::arrays($getusers);
        // echo '<pre>';
        // print_r($getusers);
        // die();
        $this->view->onlineusers = $onlineusers;
        $this->view->users       = $users;
        $this->view->formdata    = $formdata;

    }

    public function userprogramAction()
    {
        $onlineDb = new App_Model_UserOnline();
        $onlineusers = $onlineDb->getOnlineIds();
        $userDb = new App_Model_User();
        $session  = new Zend_Session_Namespace('AdminUsersIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/users/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/users/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $check_payment_status = 1;

        $getusers = $this->userDb->get_program_status();

        if ($search) {
            
            $getusers = $userDb->getprogramfirstname($search);
                  }
        if (isset($formData['Approve']) && $formData['Approve']=='Approve'){
                
                if (isset($formData['at_trans_id']) && count($formData['at_trans_id']) > 0){
                   
                    $i=0;
                    
                    foreach ($formData['at_trans_id'] as $transLoop){
                        
                        $this->offered($transLoop);
                        $i++;
                    }
                }
                $searchForm = new Application_Form_ApplicantSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);

                $this->_redirect( '/application/applicant-approval/index/');
            }

        $users = Cms_Paginator::arrays($getusers);
        // echo '<pre>';
        // print_r($getusers);
        // die();
        $this->view->onlineusers = $onlineusers;
        $this->view->users       = $users;
        $this->view->formdata    = $formdata;

    }

     public function exemptionprogramAction()
    {
       $id = $this->_getParam('id');

        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $userDocDb = new App_Model_UserDocuments();
        $userDocuments = $userDocDb->getExemptionDoc($id);
        $userid = $this->auth->getIdentity()->id;
        $MembershipAppDB = new App_Model_Membership_MembershipApplication();
        // $upgrade_list = $MembershipAppDB->getDetailsInfo($id, 1);

        $this->view->documents = $userDocuments;

        $form = new App_Form_User();
        $form->populate($user);
        $getMember= $this->userDb->getMember($id);

            $getLecExp = $this->userDb->getLecExp($id);

        $getIndExp = $this->userDb->getIndExp($id);

        $get_qualification = $this->programReg->get_exem_qualification($id);

       
       
        $this->view->get_qualification = $get_qualification;


            $this->view->getMember = $getMember;
            $this->view->getIndExp = $getIndExp;

            $this->view->getLecExp = $getLecExp;
            

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            

            if ($form->isValid($formData))
            {
                //update
                $data = array(
                                'firstname'     => $formData['firstname'],
                                'lastname'      => $formData['lastname'],
                                'email'         => $formData['email'],
                                'modifiedby'    => $this->auth->getIdentity()->id,
                                'modifiedrole'  => $this->auth->getIdentity()->role,
                                'modifieddate'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'timezone'      => $formData['timezone']
                );

                //newpass
                if ( $formData['newpassword'] != '' )
                {
                    $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['newpassword'], PASSWORD_BCRYPT, $options);

                    $data['password'] = $hash;
                    $data['salt'] = $salt;
                }

                //upload file
                if ( $user['photo'] != '' || isset($formData['deletephoto']) )
                {
                    @unlink($this->uploadDir.$user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if ( !empty($files) )
                {
                    $temp = array();
                    foreach ( $files as $file ) 
                    {
                        $extension = array ( "jpg", "jpeg", "png", "gif" );

                        if ( in_array(substr(strrchr($file['filename'], '.'), 1), $extension) )
                        {
                            $data['photo'] = '/userphoto/'.$files[0]['fileurl'];
                            $photochanged = 1;
                        }
                        else
                        {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }
 
                // save document data (if any)
                if ( !empty($temp) )
                {
                    $dataFile['user_id']        = $id;
                    $dataFile['doc_title']      = $formData['title'];
                    $dataFile['doc_filename']   = $temp['filename'];
                    $dataFile['doc_hashname']   = $temp['fileurl'];
                    $dataFile['doc_url']        = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location']   = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension']  = $temp['ext'];
                    $dataFile['doc_filesize']   = $temp['filesize'];

                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if ( ( $user['id'] == $this->auth->getIdentity()->id ) && $photochanged )
                {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $this->userDb->update($data, array('id = ?' => $id));

                Cms_Common::notify('success','Information Updated');
                $this->redirect('/admin/users/edit/id/'.$id);
            }
        }
        $this->view->user = $user;
        $this->view->form = $form;


    }

    public function upgradeusersAction()
    {
        $onlineDb = new App_Model_UserOnline();
        $onlineusers = $onlineDb->getOnlineIds();
        $userDb = new App_Model_User();
        $session  = new Zend_Session_Namespace('AdminUsersIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/users/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/users/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $check_payment_status = 1;

        $getusers = $this->userDb->get_upgrade_status();

        if ($search) {
            
            $getusers = $userDb->getUpgradefirstname($search);
                  }
        if (isset($formData['Approve']) && $formData['Approve']=='Approve'){
                
                if (isset($formData['at_trans_id']) && count($formData['at_trans_id']) > 0){
                   
                    $i=0;
                    
                    foreach ($formData['at_trans_id'] as $transLoop){
                        
                        $this->offered($transLoop);
                        $i++;
                    }
                }
                $searchForm = new Application_Form_ApplicantSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);

                $this->_redirect( '/application/applicant-approval/index/');
            }

        $users = Cms_Paginator::arrays($getusers);
        // echo '<pre>';
        // print_r($getusers);
        // die();
        $this->view->onlineusers = $onlineusers;
        $this->view->users       = $users;
        $this->view->formdata    = $formdata;

    }


    


    public function emailNotificationAction()
    {
        $onlineDb = new App_Model_UserOnline();
        $onlineusers = $onlineDb->getOnlineIds();
        $get_mail = new Admin_Model_DbTable_Emailtemplate();
        $userDb = new App_Model_User();
        $session  = new Zend_Session_Namespace('AdminUsersIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();



        if ($thisemailN->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/users/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/users/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

        $check_payment_status = 1;
        $mail_detail = $get_mail->fnViewTemplte(21);

        $getusers = $this->userDb->get_email_status();

        // echo "<pre>";
        // print_r($getusers);
       $num = count($getusers);
       for($i=0;$i<$num;$i++)
       {

        $applicant_email = $getusers[$i]['firstname'].$getusers[$i]['lastname'];
        $applicant_email = $getusers[$i]['email'];

       if(round(((strtotime($getusers[$i]['mr_expiry_date']) - strtotime($getusers[$i]['mr_activation_date']))/(60*60*24)))== 60)
       {
       
        // echo $i;
        // die();

        $data = array('email_status' => 1);
        $query_result =  $userDb->update($data, array('id = ?' => $getusers[$i]['id']));

        if($query_result)
        {
            
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];
            
 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);

           if($sendEmail)
           {
        Cms_Common::notify('success', 'Membership Renewal Successfull!');
                $this->redirect('/admin/users/email-notificaion');
           }
           else
           {
               Cms_Common::notify('success', 'Membership Not Renewal Successfull!');
                $this->redirect('/admin/users/email-notificaion');
           }
            
        }
        
       }
       if(round((strtotime($getusers[$i]['mr_expiry_date']) - strtotime($getusers[$i]['mr_activation_date']))/(60*60*24))== 30 && $getusers[$i]['id']==1)
       {
       
        
        $data = array('email_status' => 2);
        $query_result =  $userDb->update($data, array('id = ?' => $getusers[$i]['id']));
        if($query_result)
        {
            
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                Cms_Common::notify('success', 'Membership Renewal Successfull!');
               $this->redirect('/admin/users/email-notificaion');
           }
           else
           {
               Cms_Common::notify('success', 'Membership Not Renewal Successfull!');
                $this->redirect('/admin/users/email-notificaion');
           }
            
        }
        
       }
       if(round(((strtotime($getusers[$i]['mr_expiry_date']) - strtotime($getusers[$i]['mr_activation_date']))/(60*60*24)))== 2 && $getusers[$i]['id']==2)
       {
       
        $data = array('email_status' => 1);
        $query_result =  $userDb->update($data, array('id = ?' => $getusers[$i]['id']));
        if($query_result)
        {
            
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                Cms_Common::notify('success', 'Membership Renewal Successfull!');
                $this->redirect('/admin/users/email-notificaion');
           }
           else
           {
               Cms_Common::notify('success', 'Membership Not Renewal Successfull!');
               $this->redirect('/admin/users/email-notificaion');
           }
            
        }
        
       }
   }

       
        // echo "<pre>";
        // print_r($getusers);

        // // echo $getusers[0]['mr_activation_date'];
        // // echo $getusers[0]['mr_expiry_date'];
        // die();

        if ($search) {
            
            $getusers = $userDb->getname($search);
                  }
        if (isset($formData['Approve']) && $formData['Approve']=='Approve'){
                
                if (isset($formData['at_trans_id']) && count($formData['at_trans_id']) > 0){
                   
                    $i=0;
                    
                    foreach ($formData['at_trans_id'] as $transLoop){
                        
                        $this->offered($transLoop);
                        $i++;
                    }
                }
                $searchForm = new Application_Form_Applica1425ntSearch();
                $applicantList = $this->checklistVerification->fnGetApplicantList($where);


                $this->_redirect( '/application/applicant-approval/index/');
            }

        $users = Cms_Paginator::arrays($getusers);

       
        $this->view->onlineusers = $onlineusers;
        $this->view->users       = $users;
        $this->view->formdata    = $formdata;

    }

    public function deleteDocAction()
    {
        $doc_id = $this->_getParam('doc_id');
        $user_id = $this->_getParam('id');

        $userDocDb = new App_Model_UserDoc();
        $userDocuments = $userDocDb->getDoc($doc_id);

        if ($userDocuments['user_id'] == $user_id)
        {
            @unlink($this->uploadDir . '/' . $userDocuments['doc_hashname']);
            $userDocDb->deleteDoc($doc_id);

            Cms_Common::notify('success','Document deleted');
            $this->redirect('/admin/users/edit/id/'.$user_id);
        }
        else
        {
            Cms_Common::notify('success','Invalid document');
            $this->redirect('/admin/users/edit/id/'.$user_id);
        }
    }
    
    public function upgradeAction()
    {
        $id = $this->_getParam('id');

        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $userDocDb = new App_Model_UserDocuments();
        $userDocuments = $userDocDb->getUpgradeDoc($id);
        $userid = $this->auth->getIdentity()->id;
        $MembershipAppDB = new App_Model_Membership_MembershipApplication();
        // $upgrade_list = $MembershipAppDB->getDetailsInfo($id, 1);

        $this->view->documents = $userDocuments;

        $form = new App_Form_User();
        $form->populate($user);
        $getMember= $this->userDb->getMember($id);
        
            $this->view->getMember = $getMember;

            $MembershipDB = new App_Model_Membership_Membership();
         $get_renewal = $MembershipDB->get_member($id);


         $this->view->renewal = $get_renewal;

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            

            if ($form->isValid($formData))
            {
                //update
                $data = array(
                                'firstname'     => $formData['firstname'],
                                'lastname'      => $formData['lastname'],
                                'email'         => $formData['email'],
                                'modifiedby'    => $this->auth->getIdentity()->id,
                                'modifiedrole'  => $this->auth->getIdentity()->role,
                                'modifieddate'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'timezone'      => $formData['timezone']
                );

                //newpass
                if ( $formData['newpassword'] != '' )
                {
                    $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['newpassword'], PASSWORD_BCRYPT, $options);

                    $data['password'] = $hash;
                    $data['salt'] = $salt;
                }

                //upload file
                if ( $user['photo'] != '' || isset($formData['deletephoto']) )
                {
                    @unlink($this->uploadDir.$user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if ( !empty($files) )
                {
                    $temp = array();
                    foreach ( $files as $file ) 
                    {
                        $extension = array ( "jpg", "jpeg", "png", "gif" );

                        if ( in_array(substr(strrchr($file['filename'], '.'), 1), $extension) )
                        {
                            $data['photo'] = '/userphoto/'.$files[0]['fileurl'];
                            $photochanged = 1;
                        }
                        else
                        {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }
 
                // save document data (if any)
                if ( !empty($temp) )
                {
                    $dataFile['user_id']        = $id;
                    $dataFile['doc_title']      = $formData['title'];
                    $dataFile['doc_filename']   = $temp['filename'];
                    $dataFile['doc_hashname']   = $temp['fileurl'];
                    $dataFile['doc_url']        = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location']   = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension']  = $temp['ext'];
                    $dataFile['doc_filesize']   = $temp['filesize'];

                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if ( ( $user['id'] == $this->auth->getIdentity()->id ) && $photochanged )
                {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $this->userDb->update($data, array('id = ?' => $id));

                Cms_Common::notify('success','Information Updated');
                $this->redirect('/admin/users/edit/id/'.$id);
            }
        }
        $this->view->user = $user;
        $this->view->form = $form;

    }
    
    public function exemptionAction()
    {
        $dbUser   = new App_Model_User;
         $get_user = $dbUser->get_exemption_status();
         $this->view->get_user = $get_user;

    }



public function programAction()
    {
        $id = $this->_getParam('id');

        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $userDocDb = new App_Model_UserDocuments();
        $userDocuments = $userDocDb->getProgramDoc($id);


        $this->view->documents = $userDocuments;

        $form = new App_Form_User();
        $form->populate($user);

        $photochanged = 0;

        $getMember= $this->userDb->getMember($id);

        $getLecExp = $this->userDb->getLecExp($id);

        $getIndExp = $this->userDb->getIndExp($id);

        $get_qualification = $this->programReg->get_qualification($id);

       
        $exem_program = $this->programReg->get_exem_qualification($id);
        // echo "<pre>";
        // print_r($exem_program);
        // die();
         $this->view->exem_program = $exem_program;
       
        $this->view->get_qualification = $get_qualification;


            $this->view->getMember = $getMember;
            $this->view->getIndExp = $getIndExp;

            $this->view->getLecExp = $getLecExp;
            

           
        

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                //update
                $data = array(
                                'firstname'     => $formData['firstname'],
                                'lastname'      => $formData['lastname'],
                                'email'         => $formData['email'],
                                'modifiedby'    => $this->auth->getIdentity()->id,
                                'modifiedrole'  => $this->auth->getIdentity()->role,
                                'modifieddate'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'timezone'      => $formData['timezone']
                );

                //newpass
                if ( $formData['newpassword'] != '' )
                {
                    $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['newpassword'], PASSWORD_BCRYPT, $options);

                    $data['password'] = $hash;
                    $data['salt'] = $salt;
                }

                //upload file
                if ( $user['photo'] != '' || isset($formData['deletephoto']) )
                {
                    @unlink($this->uploadDir.$user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if ( !empty($files) )
                {
                    $temp = array();
                    foreach ( $files as $file ) 
                    {
                        $extension = array ( "jpg", "jpeg", "png", "gif" );

                        if ( in_array(substr(strrchr($file['filename'], '.'), 1), $extension) )
                        {
                            $data['photo'] = '/userphoto/'.$files[0]['fileurl'];
                            $photochanged = 1;
                        }
                        else
                        {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }
 
                // save document data (if any)
                if ( !empty($temp) )
                {
                    $dataFile['user_id']        = $id;
                    $dataFile['doc_title']      = $formData['title'];
                    $dataFile['doc_filename']   = $temp['filename'];
                    $dataFile['doc_hashname']   = $temp['fileurl'];
                    $dataFile['doc_url']        = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location']   = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension']  = $temp['ext'];
                    $dataFile['doc_filesize']   = $temp['filesize'];

                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if ( ( $user['id'] == $this->auth->getIdentity()->id ) && $photochanged )
                {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $this->userDb->update($data, array('id = ?' => $id));

                Cms_Common::notify('success','Information Updated');
                $this->redirect('/admin/users/edit/id/'.$id);
            }
        }
        $this->view->user = $user;
        $this->view->form = $form;


    }

    public function renewAction()
    {
        $id = $this->_getParam('id');

        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $userDocDb = new App_Model_UserDocuments();
        $userDocuments = $userDocDb->getRenewDoc($id);

        $this->view->documents = $userDocuments;

        $form = new App_Form_User();
        $form->populate($user);

         $MembershipDB = new App_Model_Membership_Membership();
         $get_renewal = $MembershipDB->get_member($id);


         $this->view->renewal = $get_renewal;

        $photochanged = 0;
        $getMember= $this->userDb->getMember($id);

            $this->view->getMember = $getMember;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                //update
                $data = array(
                                'firstname'     => $formData['firstname'],
                                'lastname'      => $formData['lastname'],
                                'email'         => $formData['email'],
                                'modifiedby'    => $this->auth->getIdentity()->id,
                                'modifiedrole'  => $this->auth->getIdentity()->role,
                                'modifieddate'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'timezone'      => $formData['timezone']
                );

                //newpass
                if ( $formData['newpassword'] != '' )
                {
                    $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['newpassword'], PASSWORD_BCRYPT, $options);

                    $data['password'] = $hash;
                    $data['salt'] = $salt;
                }

                //upload file
                if ( $user['photo'] != '' || isset($formData['deletephoto']) )
                {
                    @unlink($this->uploadDir.$user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if ( !empty($files) )
                {
                    $temp = array();
                    foreach ( $files as $file ) 
                    {
                        $extension = array ( "jpg", "jpeg", "png", "gif" );

                        if ( in_array(substr(strrchr($file['filename'], '.'), 1), $extension) )
                        {
                            $data['photo'] = '/userphoto/'.$files[0]['fileurl'];
                            $photochanged = 1;
                        }
                        else
                        {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }
 
                // save document data (if any)
                if ( !empty($temp) )
                {
                    $dataFile['user_id']        = $id;
                    $dataFile['doc_title']      = $formData['title'];
                    $dataFile['doc_filename']   = $temp['filename'];
                    $dataFile['doc_hashname']   = $temp['fileurl'];
                    $dataFile['doc_url']        = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location']   = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension']  = $temp['ext'];
                    $dataFile['doc_filesize']   = $temp['filesize'];

                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if ( ( $user['id'] == $this->auth->getIdentity()->id ) && $photochanged )
                {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $this->userDb->update($data, array('id = ?' => $id));

                Cms_Common::notify('success','Information Updated');
                $this->redirect('/admin/users/edit/id/'.$id);
            }
        }
        $this->view->user = $user;
        $this->view->form = $form;

    }

    public function editAction()
    {
        $id = $this->_getParam('id');
        
        $this->definationDB =new Admin_Model_DbTable_Definition();
        $form = new Admin_Form_CourseCategory();
        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();


        $userDb = new App_Model_User();
          $get_currency = $userDb->getcurrency();
          
         $form_member = new App_Form_Register();

         $this->view->member_form = $form_member;

        $programMode = $this->definationDB->getByCode('Member Pending');
        foreach ($programMode as $opt5 )
        {
            $form->mode_of_program->addMultiOption($opt5['idDefinition'], $opt5['DefinitionDesc']);
        }

        $this->view->programMode = $programMode;
      

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $userDocDb = new App_Model_UserDocuments();
        // $userDocuments = $userDocDb->getUserDoc($id);


        $getMember= $this->userDb->getMember($id);
       if(empty($getMember))
        {
            $getMember= $this->userDb->Details($id);
             $this->view->getMember = $getMember;
                
        }

            $this->view->getMember = $getMember;



        
    $get_usercompany =  $this->userDb->get_usercompany($id);

      
        $get_userqualification =  $this->userDb->get_userqualification($id);

        $this->view->get_userqualification = $get_userqualification;

        $this->view->get_usercompany = $get_usercompany;

  

           $getICDoc = $userDocDb->getICDoc($id);


            $this->view->IC_doc = $getICDoc;

            $getAcDoc = $userDocDb->getAcDoc($id);
           

            $this->view->AC_doc = $getAcDoc;

            $getAtDoc = $userDocDb->getAtDoc($id);

            $this->view->AT_doc = $getAtDoc;
            $getCLDoc = $userDocDb->getCLDoc($id);
            $this->view->CL_doc = $getCLDoc;
            $getPreDoc = $userDocDb->getPreDoc($id);
            $this->view->PreDoc = $getPreDoc;

            $getCvDoc = $userDocDb->getCvDoc($id);

          
           $this->view->CV_doc = $getCvDoc; 

        $form = new App_Form_User();
        $form->populate($user);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
                 $formData = $_POST; 
    // print_r($formData);
    // die();
            if($formData['ic'])

                {    $data = array('doc_type'=> 1,
                    'status' => $formData['ic']);
                    $doc = 'IC';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like  ? "%"'=>$doc ));
             }
             if($formData['academic_certificate'])

                {    $data = array('doc_type'=> 2,
                    'status' => $formData['academic_certificate']);
                    $doc = 'Academic Certificate';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }
             if($formData['academic_trans'])

                {    $data = array('doc_type'=> 3
                    ,'status' => $formData['academic_trans']);
                    $doc = 'Academic transcript';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }


             if($formData['confirmation'])

                {    $data = array('doc_type'=> 4,'status' => $formData['confirmation']);
                    $doc = 'Confirmation';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }


             if($formData['cv'])

                {    $data = array('doc_type'=> 5,
                    'status' => $formData['cv']);
                    $doc = 'CV';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }

             if($formData['prev'])

                {    $data = array('doc_type'=> 6,
                    'status' => $formData['prev']);
                    $doc = 'Previous';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }

             $get_doc_status = $userDocDb->get_doc_status($id);

            if($user['nationality'] == 'MY')
            {
                $formData['total_fee_amount'] = $formData['total_fee_amount'];
            }else{
                $formData['total_fee_amount'] = $formData['total_fee_amount']*$get_currency['cr_exchange_rate'];
            }

             $get_currency = $userDb->getcurrency();
             $member_data = array('id_member'         => $formData['membershiptype'],
                                  'id_member_subtype' => $formData['subclassifaication'],
                                  'prof_des'          => $formData['prof_des'] ,
                                  'admin_notes_amc'   => $formData['admin_notes'],
                                  'banking_notes_amc' => $formData['banking_notes'],
                                  'total_amount' => $formData['total_fee_amount'],
                                  'amc_approve_date' =>date('Y-m-d')
                              );
             $feeModel =  new App_Model_MemberFeeStructure();
         

            //update data
            $update_user = $userDb->update($member_data, array('id = ?' => $id));

            $user_data = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();
      $result = $feeModel->getMemberamount($user_data['id_member'],$user_data['id_member_subtype'],$user_data['nationality']);


     

             $invoice = new App_Model_PerformaInvoice();
               
                 
            $data_invioce = array('user_id' => $id,
                                   'description' => 'Member Payment',
                                   'fee_type' => 0,
                                   'amount' => $formData['total_fee_amount'],
                                   'invoice_no' => 'CIIF'.  date('YHims').$id,
                                   'created_user' => $id,
                                   'created_date' => date("Y-m-d"),
                         );
            
                    $invoice_no = $invoice->insert($data_invioce);
               

try{


            //  $invoice_data= new App_Model_PerformaInvoiceDetails();
               
                 
            // $data_invioce_detail = array('user_id' => $id,
            //                         'performa_id' =>  $invoice_no,
            //                         'id_program' => $result['id'],
            //                        'description' => 'Member Payment',
            //                        'amount' => $formData['total_fee_amount'],
            //                        'invoice_no' => 'CIIF'.  date('YHims').$id,
            //                        'created_user' => $id,
            //                        'created_date' => date("Y-m-d"),
            //              );
            //         $invoice_no = $invoice_data->insert($data_invioce_detail);
                    

            
             $get_mail = new Admin_Model_DbTable_Emailtemplate();
             $mail_detail = $get_mail->fnViewTemplte(26);
             $applicant_name = $user['firstname'].' '.$user['lastname'];
             $applicant_email = $user['email'];
}
catch(Exception $e){
    echo $e;
}
             if($get_doc_status)
        {
            

           $mail = new Cms_SendMail();
           // $doclist ="<table>";

           // foreach ($get_doc_status as $key => $value) {
           //                        $doc = $value['doc_name'];   
           //                        $doclist.="<tr>
           //                          $doc
           //                          </tr>";
           //                       }
                                
           //             $doclist .= "</table>";
           $message = 'Dear '.''.$applicant_name .$mail_detail['TemplateBody'];

            
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($get_doc_status)
           { 


            $this->redirect('/admin/users');
           }
           
            
        }
        


            $this->redirect('/admin/users');
    
           

        }
        $this->view->user = $user;
        $this->view->form = $form;

    }

     public function profileAction()
    {
        $id = $this->_getParam('id');
        
        $this->definationDB =new Admin_Model_DbTable_Definition();
        $form = new Admin_Form_CourseCategory();
        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();


        $userDb = new App_Model_User();
          $get_currency = $userDb->getcurrency();
          
         $form_member = new App_Form_Register();

         $this->view->member_form = $form_member;

        $programMode = $this->definationDB->getByCode('Member Pending');
        foreach ($programMode as $opt5 )
        {
            $form->mode_of_program->addMultiOption($opt5['idDefinition'], $opt5['DefinitionDesc']);
        }

        $this->view->programMode = $programMode;
      

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $userDocDb = new App_Model_UserDocuments();
        // $userDocuments = $userDocDb->getUserDoc($id);


        $getMember= $this->userDb->getMember($id);
       if(empty($getMember))
        {
            $getMember= $this->userDb->Details($id);
             $this->view->getMember = $getMember;
                
        }

            $this->view->getMember = $getMember;



        
    $get_usercompany =  $this->userDb->get_usercompany($id);

      
        $get_userqualification =  $this->userDb->get_userqualification($id);

        $this->view->get_userqualification = $get_userqualification;

        $this->view->get_usercompany = $get_usercompany;

  

           $getICDoc = $userDocDb->getICDoc($id);


            $this->view->IC_doc = $getICDoc;

            $getAcDoc = $userDocDb->getAcDoc($id);
           

            $this->view->AC_doc = $getAcDoc;

            $getAtDoc = $userDocDb->getAtDoc($id);

            $this->view->AT_doc = $getAtDoc;
            $getCLDoc = $userDocDb->getCLDoc($id);
            $this->view->CL_doc = $getCLDoc;
            $getPreDoc = $userDocDb->getPreDoc($id);
            $this->view->PreDoc = $getPreDoc;

            $getCvDoc = $userDocDb->getCvDoc($id);

          
           $this->view->CV_doc = $getCvDoc; 

        $form = new App_Form_User();
        $form->populate($user);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
                  

            if($formData['ic'])

                {    $data = array('doc_type'=> 1,
                    'status' => $formData['ic']);
                    $doc = 'IC';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like  ? "%"'=>$doc ));
             }

             if($formData['academic_certificate'])

                {    $data = array('doc_type'=> 2,
                    'status' => $formData['academic_certificate']);
                    $doc = 'Academic Certificate';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }
             if($formData['academic_trans'])

                {    $data = array('doc_type'=> 3
                    ,'status' => $formData['academic_trans']);
                    $doc = 'Academic transcript';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }


             if($formData['confirmation'])

                {    $data = array('doc_type'=> 4,'status' => $formData['confirmation']);
                    $doc = 'Confirmation';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }


             if($formData['cv'])

                {    $data = array('doc_type'=> 5,
                    'status' => $formData['cv']);
                    $doc = 'CV';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }

             if($formData['prev'])

                {    $data = array('doc_type'=> 6,
                    'status' => $formData['prev']);
                    $doc = 'Previous';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }

             $get_doc_status = $userDocDb->get_doc_status($id);

            if($user['nationality'] == 'MY')
            {
                $formData['total_fee_amount'] = $formData['total_fee_amount'];
            }else{
                $formData['total_fee_amount'] = $formData['total_fee_amount']*$get_currency['cr_exchange_rate'];
            }

             $get_currency = $userDb->getcurrency();
             $member_data = array('id_member'         => $formData['membershiptype'],
                                  'id_member_subtype' => $formData['subclassifaication'],
                                  'prof_des'          => $formData['prof_des'] ,
                                  'admin_notes_amc'   => $formData['admin_notes'],
                                  'banking_notes_amc' => $formData['banking_notes'],
                                  'total_amount' => $formData['total_fee_amount'],
                                  'amc_approve_date' =>date('Y-m-d')
                              );
             $feeModel =  new App_Model_MemberFeeStructure();
         

            //update data
            $update_user = $userDb->update($member_data, array('id = ?' => $id));


            $user_data = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();
      $result = $feeModel->getMemberamount($user_data['id_member'],$user_data['id_member_subtype'],$user_data['nationality']);


      

             $invoice = new App_Model_PerformaInvoice();
               
                 
            $data_invioce = array('user_id' => $id,
                                   'description' => 'Member Payment',
                                   'fee_type' => 0,
                                   'amount' => $formData['total_fee_amount'],
                                   'invoice_no' => 'CIIF'.  date('YHims').$id,
                                   'created_user' => $id,
                                   'created_date' => date("Y-m-d"),
                         );
                    $invoice_no = $invoice->insert($data_invioce);
                    

             $invoice_data= new App_Model_PerformaInvoiceDetails();
               
                 
            $data_invioce_detail = array('user_id' => $id,
                                    'performa_id' =>  $invoice_no,
                                    'id_program' => $result['id'],
                                   'description' => 'Member Payment',
                                   'amount' => $formData['total_fee_amount'],
                                   'invoice_no' => 'CIIF'.  date('YHims').$id,
                                   'created_user' => $id,
                                   'created_date' => date("Y-m-d"),
                         );
                    $invoice_no = $invoice_data->insert($data_invioce_detail);
                    

            
             $get_mail = new Admin_Model_DbTable_Emailtemplate();
             $mail_detail = $get_mail->fnViewTemplte(26);
             $applicant_name = $user['firstname'].' '.$user['lastname'];
             $applicant_email = $user['email'];

             if($get_doc_status)
        {
            

           $mail = new Cms_SendMail();
           // $doclist ="<table>";

           // foreach ($get_doc_status as $key => $value) {
           //                        $doc = $value['doc_name'];   
           //                        $doclist.="<tr>
           //                          $doc
           //                          </tr>";
           //                       }
                                
           //             $doclist .= "</table>";
           $message = 'Dear '.''.$applicant_name .$mail_detail['TemplateBody'];

            
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($get_doc_status)
           { 


            $this->redirect('/admin/users/edit/id/'.$id);
           }
           
            
        }
        

          
    
           

        }
        $this->view->user = $user;
        $this->view->form = $form;

    }

    public function rolelisteditAction()
    {
        $id = $this->_getParam('id');

          $form_member = new App_Form_Register();

         $this->view->member_form = $form_member;
        
        $this->definationDB =new Admin_Model_DbTable_Definition();
        $form = new Admin_Form_CourseCategory();
        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();


        $programMode = $this->definationDB->getByCode('Member Pending');
        foreach ($programMode as $opt5 )
        {
            $form->mode_of_program->addMultiOption($opt5['idDefinition'], $opt5['DefinitionDesc']);
        }
         $this->view->programMode = $programMode;

            
      

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $userDocDb = new App_Model_UserDocuments();
        //$userDocuments = $userDocDb->getUserDoc($id);


        $getMember= $this->userDb->getMember($id);


        $get_usercompany =  $this->userDb->get_usercompany($id);

        
        $get_userqualification =  $this->userDb->get_userqualification($id);

        $this->view->get_userqualification = $get_userqualification;

        $this->view->get_usercompany = $get_usercompany;

    

        $getICDoc = $userDocDb->getICDoc($id);


            $this->view->IC_doc = $getICDoc;

            $getAcDoc = $userDocDb->getAcDoc($id);
           

            $this->view->AC_doc = $getAcDoc;

            $getAtDoc = $userDocDb->getAtDoc($id);

            $this->view->AT_doc = $getAtDoc;
            $getCLDoc = $userDocDb->getCLDoc($id);
            $this->view->CL_doc = $getCLDoc;
            $getPreDoc = $userDocDb->getPreDoc($id);
            $this->view->PreDoc = $getPreDoc;

            $getCvDoc = $userDocDb->getCvDoc($id);

          
           $this->view->CV_doc = $getCvDoc; 


        $form = new App_Form_User();
        $form->populate($user);

         if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();



            if($formData['Acade'])

                {    $data = array('doc_type'=> 1,
                    'status' => $formData['Acade']);
                    $doc = 'Acade';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }

             if($formData['Work'])

                {    $data = array('doc_type'=> 2,
                    'status' => $formData['Work']);
                    $doc = 'Work';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }
             if($formData['Previ'])

                {    $data = array('doc_type'=> 3
                    ,'status' => $formData['Previ']);
                    $doc = 'Previ';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }


             if($formData['Nomin'])

                {    $data = array('doc_type'=> 4,'status' => $formData['Nomin']);
                    $doc = 'Nomin';
                  $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             }


             // if($formData['Other'])

             //    {    $data = array('doc_type'=> 5,
             //        'status' => $formData['Other']);
             //        $doc = 'Other';
             //      $userDocDb->update($data,array('membership_id' =>$id ,'doc_name like "%" ? "%"'=>$doc ));
             // }

             $get_doc_status = $userDocDb->get_doc_status($id);

             $member_data = array('admin_notes_exco'   => $formData['admin_notes'],
                                  'banking_notes_exco' => $formData['banking_notes']);

              

            //update data
            $update_user = $this->userDb->update($member_data, array('id = ?' => $id));

            
             $get_mail = new Admin_Model_DbTable_Emailtemplate();
             $mail_detail = $get_mail->fnViewTemplte(26);
             $applicant_name = $user['firstname'].' '.$user['lastname'];
             $applicant_email = $user['email'];

             if($get_doc_status)
        {
            

           $mail = new Cms_SendMail();
           $doclist ="<table>";

           foreach ($get_doc_status as $key => $value) {
                                  $doc = $value['doc_name'];   
                                  $doclist.="<tr>
                                    $doc
                                    </tr>";
                                 }
                                
                       $doclist .= "</table>";
           $message = 'Dear '.''.$applicant_name .$mail_detail['TemplateBody'].$doclist;

            
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
          
            $this->redirect('/admin/users/rolelistedit/id/'.$id);
           
            
        }
        

          
    
           

        }

        
        $this->view->user = $user;
        $this->view->form = $form;

    }

    public function getDocAction()
    {

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            echo '<pre>';
            print_r($formData);
            die();
        }
    }
    
    public function approveAction()
    {
        $id = $this->_getParam('id');

        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $userDocDb = new App_Model_UserDocuments();
        $userDocuments = $userDocDb->getUserDoc($id);

        $this->view->documents = $userDocuments;

        $form = new App_Form_User();
        $form->populate($user);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            if ($form->isValid($formData))
            {
                //update
                $data = array(
                                'firstname'     => $formData['firstname'],
                                'lastname'      => $formData['lastname'],
                                'email'         => $formData['email'],
                                'modifiedrole'  => $this->auth->getIdentity()->role,
                                'approve_status' => '1',
                                'modifiedby'    => $this->auth->getIdentity()->id,
                                'modifieddate'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'timezone'      => $formData['timezone']
                );

                //newpass
                if ( $formData['newpassword'] != '' )
                {
                    $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['newpassword'], PASSWORD_BCRYPT, $options);

                    $data['password'] = $hash;
                    $data['salt'] = $salt;
                }

                //upload file
                if ( $user['photo'] != '' || isset($formData['deletephoto']) )
                {
                    @unlink($this->uploadDir.$user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if ( !empty($files) )
                {
                    $temp = array();
                    foreach ( $files as $file ) 
                    {
                        $extension = array ( "jpg", "jpeg", "png", "gif" );

                        if ( in_array(substr(strrchr($file['filename'], '.'), 1), $extension) )
                        {
                            $data['photo'] = '/userphoto/'.$files[0]['fileurl'];
                            $photochanged = 1;
                        }
                        else
                        {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }
 
                // save document data (if any)
                if ( !empty($temp) )
                {
                    $dataFile['user_id']        = $id;
                    $dataFile['doc_title']      = $formData['title'];
                    $dataFile['doc_filename']   = $temp['filename'];
                    $dataFile['doc_hashname']   = $temp['fileurl'];
                    $dataFile['doc_url']        = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location']   = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension']  = $temp['ext'];
                    $dataFile['doc_filesize']   = $temp['filesize'];


                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if ( ( $user['id'] == $this->auth->getIdentity()->id ) && $photochanged )
                {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $this->userDb->update($data, array('id = ?' => $id));

                Cms_Common::notify('success','Information Updated');
                $this->redirect('/admin/users/approve/id/'.$id);
            }
        }
        $this->view->user = $user;
        $this->view->form = $form;

    }

    public function loginAsAction() {

        $id   = $this->_getParam('id');
        $user = $this->userDb->fetchRow(array('id = ?' => $id));
        
        if (!$user) {
            return Cms_Render::error('User not found');
        }

        $user = $user->toArray();

        if($user['role'] == 'administrator') {
            return Cms_Render::error('You cannot login as another administrator');
        }

        unset($user["password"]);
        unset($user["salt"]);

        $auth        = Zend_Auth::getInstance();
        $data        = new stdClass();
        $idnumber    = $user['username'];
        $external_id = $user['external_id'];
        $db2         = getDB2();

        $select = $db2->select()
            ->from(array('a'=>'student_profile'))
            ->where('a.std_idnumber = ?', $idnumber);
        $profile = $db2->fetchRow($select);

        $data->login_as = $auth->getIdentity()->id;

        if($profile) {
            $external_id       = $profile['std_id'];
            $data->external_id = $external_id;
            $data->sp_id       = $external_id;
        }

        foreach ($user as $key => $value) {
            $data->$key = $value;
        }

        $auth->getStorage()->write($data);

        $this->userDb->update(array(
            'lastlogin'   => new Zend_Db_Expr('UTC_TIMESTAMP()'),
            'external_id' => $external_id
        ), array(
            'id = ?' => $data->id
        ));
        
        Cms_Events::trigger('auth.login.success', $this, (array) $data);
        Cms_Common::notify('success', 'You are now logged in as : '. $data->firstname .' '. $data->lastname);
//        $this->redirect($this->view->url(array('module' => 'default','controller'=>'dashboard'), 'default', true));
        $this->redirect($this->view->url(array('module' => 'user','controller'=>'main'), 'default', true));
    }

    public function trackingAction() {
        $id   = $this->_getParam('id');
        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $this->view->user = $user;

        $enrolDb  = new App_Model_Enrol();
        $programs = $enrolDb->getEnrolledProgrammeSimple($id, true);

        foreach($programs as $i => $row) {
            $programs[$i]['courses'] = $enrolDb->getEnrolledCoursesSimple($id, $row['id'], false);
        }

        $this->view->programs = $programs;
        $this->view->user_id  = $id;
    }

    public function roleAction()

    {
         $dbUser   = new App_Model_User;
         $get_user = $dbUser->get_role();
         $this->view->get_user = $get_user;
    }

    public function rolelistAction()

    {
         $dbUser   = new App_Model_User;
          $memberModel = new App_Model_Member();
        $memberReg =new App_Model_Membership_MemberRegistration();
        $get_mail = new Admin_Model_DbTable_Emailtemplate();
        $form = new Admin_Form_CourseCategory();

        $this->view->form = $form;
         $get_user = $dbUser->get_role_list();

       
        $userDocDb = new App_Model_UserDocuments();
         
        // $getICDoc = $userDocDb->getICDoc($id);


        //     $this->view->IC_doc = $getICDoc;

        //     $getAcDoc = $userDocDb->getAcDoc($id);
           

        //     $this->view->AC_doc = $getAcDoc;

        //     $getAtDoc = $userDocDb->getAtDoc($id);

        //     $this->view->AT_doc = $getAtDoc;
        //     $getCLDoc = $userDocDb->getCLDoc($id);
        //     $this->view->CL_doc = $getCLDoc;
        //     $getPreDoc = $userDocDb->getPreDoc($id);
        //     $this->view->PreDoc = $getPreDoc;

        //     $getCvDoc = $userDocDb->getCvDoc($id);

          
        //    $this->view->CV_doc = $getCvDoc; 
        
               if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            if($formData['Search'])
            {
                $get_user = $dbUser->get_approve_date($formData['start_date'],$formData['end_date']);
               $this->view->get_user = $get_user;


                
            }

            if($formData['reject'])

            {
                foreach ($formData as $key => $value) {

                 $data = array('estatus' => 594,
                                'reject_notes' => $formData['reject_notes']);
                 $query_result= $dbUser->update($data, array('id = ?' => intval($key)));

                 $userModel = new App_Model_User();
                 $get_user_detail = $dbUser->getreguser(intval($key));

                 $appl_id = $get_user_detail['id'] ;
            
         $applicant_email = $get_user_detail['email'];
         $applicant_name = $get_user_detail['firstname'].' '.$get_user_detail['lastname'];
         
       
        
             $get_user = $dbUser->get_role_list();
             $num_user = count($get_user);

             $mail_detail = $get_mail->fnViewTemplte(28);

              $user = $this->userDb->fetchRow(array('id = ?' => intval($key)));

            
        // $uniqueId = 'CIIFP'.date('my');
        $membershipId = $uniqueId;
       

        if($query_result)
        {
           
        if($query)
        {
             $mail = new Cms_SendMail();
        
           $message = 'Dear '.''.$applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);


         }    

            }
           
          }
      }

            if($formData['approve'])
            {
           

            foreach ($formData as $key => $value) {
                # code...

       
                $data = array('approve_status' => 1,
                            'approve_date' => date('Y-m-d',strtotime($formData['approve_date'])),
                            'ref_no' => $formData['ref_no']);
                 $query_result= $dbUser->update($data, array('id = ?' => intval($key)));

                 $userModel = new App_Model_User();
                 $get_user_detail = $dbUser->getreguser(intval($key));

                 $appl_id = $get_user_detail['id'] ;
         $applicant_email = $get_user_detail['email'];
         $applicant_name = $get_user_detail['firstname'].' '.$get_user_detail['lastname'];
         
       
        
             $get_user = $dbUser->get_role_list();
             $num_user = count($get_user);

             $mail_detail = $get_mail->fnViewTemplte(20);

              $user = $this->userDb->fetchRow(array('id = ?' => intval($key)));

             if($get_user_detail['id_member'] == 1 && $get_user_detail['id_member_subtype'] == 8){
                $uniqueId = 'ST00000'.$appl_id;
             }elseif ($get_user_detail['id_member'] == 3 && $get_user_detail['id_member_subtype'] == 1 ) {
                 # code...
                $uniqueId = 'AJ00000'.$appl_id;
             }elseif ($get_user_detail['id_member'] == 3 && $get_user_detail['id_member_subtype'] == 2) {
                 # code...
                $uniqueId = 'AS00000'.$appl_id;
             }else{
                 $uniqueId = 'OM00000'.$appl_id;
             }

            
        // $uniqueId = 'CIIFP'.date('my');
        $membershipId = $uniqueId;
       

        if($query_result)
        {
            $data = array('mr_membershipId' => $appl_id,
            'mr_sp_id' =>  $membershipId,
            'mr_status'=> 1384,
            'mr_activation_date' => date('Y-m-d H:i:s'),
            'mr_expiry_date' => date('Y-m-d H:i:s', strtotime(date("Y-m-d", time()) . " + 1 year")),
            'mr_payment_status' => 1);
          
        $query = $memberReg->insert($data);
        if($query)
        {
             $mail = new Cms_SendMail();
        
           $message = 'Dear '.''.$applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);


         }    

            }
           
           

   }

         
    }


         if($formData['comments'])
              { 


              foreach ($formData as $key => $value) { 
                $data = array(
                                'comment_notes' => $formData['comment_notes']);
                 $query_result= $dbUser->update($data, array('id = ?' => intval($key)));

                 $userModel = new App_Model_User();
                 $get_user_detail = $dbUser->getreguser(intval($key));

                 $appl_id = $get_user_detail['id'] ;
         $applicant_email = $get_user_detail['email'];
         $applicant_name = $get_user_detail['firstname'].' '.$get_user_detail['lastname'];
         
       
        
             $get_user = $dbUser->get_role_list();
             $num_user = count($get_user);

             $mail_detail = $get_mail->fnViewTemplte(20);

              $user = $this->userDb->fetchRow(array('id = ?' => intval($key)));

              $mail = new Cms_SendMail();
        
           $message = 'Dear '.''.$applicant_name .$mail_detail['TemplateBody'] . $formData['comment_notes'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
            
        // $uniqueId = 'CIIFP'.date('my');
        $membershipId = $uniqueId;
       

           }
          } 
     
}
    $this->view->get_user = $get_user;
}

    public function barringlistAction()

    {
         $dbUser   = new App_Model_User;
         $get_user = $dbUser->get_barring_list();
         $this->view->get_user = $get_user;
    }

    public function barredmemberAction()

    {
         $dbUser   = new App_Model_User;
         $get_user = $dbUser->get_barred_member();
         $this->view->get_user = $get_user;
    }

    public function releasememberAction()
    {
            $this->_helper->layout->disableLayout();
         $id = $this->_getParam('id');


       
        $barredMember = new Admin_Model_DbTable_MemberBarring();

        $get_id = $barredMember->fetchRow(array('id_bm =?'=> $id));
       
          $user = $this->userDb->fetchRow(array('id = ?' => $get_id['bmem_id']))->toArray();

           $update['barring_status'] = 0;
    $this->userDb->update($update,array('id' => $user['id'] ));

           
         if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $data = array('member_status' => 1, );


             $barredMember->update($data, array('id_bm = ?' => $id));
             $update['barring_status'] = 0 ;
   $update_user = $this->userDb->update($update, array('id = ?' => $get_id['bmem_id']));
   if($update_user)
   {
    $this->redirect('/admin/users/barredmember');
        }

   }
            $this->redirect('/admin/users/barredmember');
        

    }


    public function barringamountAction()
    {

    $this->_helper->layout->disableLayout();

     $id = $this->_getParam('case_id',null);
    //die();
    $query =  new Admin_Model_DbTable_Barring();
   

    $barringdata = $query->fetchRow(array('id_barring= ?' => $id));
    echo $barringdata['amount'];


    // $json = Zend_Json::encode($barringdata);
    //  print_r($json);
                   // die();

     exit();
    }

    public function caseregisterAction()
    {
        $id = $this->_getParam('id');
        
        $this->definationDB =new Admin_Model_DbTable_Definition();
        $form = new Admin_Form_CourseCategory();
        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();

        $get_barring = new Admin_Model_DbTable_Barring();

        $barring_list = $get_barring->fetchAll();


        $member_barring = new Admin_Model_DbTable_MemberBarring();

               
       $this->view->barring_list = $barring_list;

        
        

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
           
            $data = array('bmem_id' => $user['id'],
                'case_id' => $formData['case_id'],
                'amount' => $formData['amount'],
                'created_date' => date("Y-m-d h:i:s"),
                'created_by' => $this->auth->getIdentity()->id,
                'status' => 1,
                'member_status'=> 1
          );

    $barring = $member_barring->insert($data);


    $update['barring_status'] = 1;
    $this->userDb->update($update,array('id' => $user['id'] ));


        }
        $this->view->user = $user;
        $this->view->form = $form;

    }



    public function roleeditAction()

    {

         $roleLists = $this->definationDB->getByCode('Role');
            

             $this->view->roles = $roleLists;
          $dbUser   = new App_Model_User;

        $id = $this->_getParam('id');

        $user = $dbUser->fetchRow(array("id = ?" => $id))->toArray();
        
        $this->view->user = $user;
     
         if($this->getRequest()->isPost())
         {
             $formData = $this->getRequest()->getPost();

            
            $dbUser   = new App_Model_User;
            $fail     = 0;
            $errmsg   = '';



            $user = $dbUser->fetchRow(array("username = ?" => $formData['username']));
            if (!empty($user)) 
            {
                $fail   = 1;
                $errmsg = $this->view->translate('Username already exists. <a href="/forgotpass">Forgot your password?</a>');
            }

            //check email
            $email = $dbUser->fetchRow(array("email = ?" => $formData['email']));
            if (!empty($email)) 
            {
                $fail   = 1;
                $errmsg = $this->view->translate('Email already in use. <a href="/forgotpass">Forgot your password?</a>');
            }

            //check for illegal characters
            if ( preg_match("/[^a-zA-Z0-9]+/", $formData['username']) && $fail != 1 )
            {
                $fail   = 1;
                $errmsg = $this->view->translate('Username contains illegal characters. Try again. Only alphanumeric characters are allowed.',0);
            }

            if ( ( strlen($formData['username']) < 3 || strlen($formData['username']) > 25 ) && $fail != 1 )
            {
                if ( strlen($formData['username']) < 3 )
                {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username is too short. You need to have at least 3 characters',0);
                }
                else
                {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username is too long. Your username cannot be longer than 25 characters.',0);
                }
            }

            if ($fail)
            {
                $salt      =  Cms_Common::generateRandomString(22);
                $options   = array('salt' => $salt);
            

                foreach ($formData as $index => $value)
                {
                    if (!$value)
                    {
                        $formData[$index] = null;
                    }
                }

               
                $data = array(
                    'username'      => $formData['username'],
                    
                    'firstname'     => $formData['firstname'],
                    'lastname'      => $formData['lastname'],
                    'email'         => $formData['email'],
                    'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'role'          => $formData['role'],
                    'contactno'     => $formData['contactno'],
                    'birthdate'     => $formData['dob'],
                    // 'qualification' => $formData['qualification'],
                    'modifieddate'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'modifiedby'    => $this->auth->getIdentity()->id,
                    'modifiedrole'  => $this->auth->getIdentity()->role,
                    // 'company'    => $formData['company'],
                    'active'        => 1,
                    'id_member'     => 0,
                    'id_member_subtype'     => 0,
                    'id_qualification'     => 0,
                    'id_experience'     => 0,
                    'industrial_exp'     => 0,
                    'lecturship_exp'     => 0,
                    'total_amount'     => 0,
                    'payment_status'     => 0,
                    'approve_status'=> 1
                );

             
                $user_id = $dbUser->update($data ,'id ='.$id);

                Cms_Common::notify('success', 'User successfully created');
                $this->redirect('/admin/users/role');
         }
    }
}

    public function newAction()
    {

        $roleLists = $this->definationDB->getByCode('Role');
            

             $this->view->roles = $roleLists;
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();
            
            $dbUser   = new App_Model_User;
            $fail     = 0;
            $errmsg   = '';



            $user = $dbUser->fetchRow(array("username = ?" => $formData['username']));
            if (!empty($user)) 
            {
                $fail   = 1;
                $errmsg = $this->view->translate('Username already exists. <a href="/forgotpass">Forgot your password?</a>');
            }

            //check email
            $email = $dbUser->fetchRow(array("email = ?" => $formData['email']));
            if (!empty($email)) 
            {
                $fail   = 1;
                $errmsg = $this->view->translate('Email already in use. <a href="/forgotpass">Forgot your password?</a>');
            }

            //check for illegal characters
            if ( preg_match("/[^a-zA-Z0-9]+/", $formData['username']) && $fail != 1 )
            {
                $fail   = 1;
                $errmsg = $this->view->translate('Username contains illegal characters. Try again. Only alphanumeric characters are allowed.',0);
            }

            if ( ( strlen($formData['username']) < 3 || strlen($formData['username']) > 25 ) && $fail != 1 )
            {
                if ( strlen($formData['username']) < 3 )
                {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username is too short. You need to have at least 3 characters',0);
                }
                else
                {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username is too long. Your username cannot be longer than 25 characters.',0);
                }
            }

            if (!$fail)
            {
                $salt      =  Cms_Common::generateRandomString(22);
                $options   = array('salt' => $salt);
                $hash      = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
                // $birthdate = date('Y-m-d', strtotime($formData['dob_year'].'-'.$formData['dob_month'].'-'.$formData['dob_day']));
               //$birthdate = ($birthdate == '1970-01-01' ? null : $birthdate);

                foreach ($formData as $index => $value)
                {
                    if (!$value)
                    {
                        $formData[$index] = null;
                    }
                }
                echo $hash;
                $data = array(
                    'username'      => $formData['username'],
                    'password'      => $hash,
                    'salt'          => $salt,
                    'firstname'     => $formData['firstname'],
                    'lastname'      => $formData['lastname'],
                    'email'         => $formData['email'],
                    'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'role'          => $formData['role'],
                    'contactno'     => $formData['contactno'],
                     'birthdate'     => $formData['dob'],
                    // 'qualification' => $formData['qualification'],
                    'modifieddate'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'modifiedby'    => $this->auth->getIdentity()->id,
                    'modifiedrole'  => $this->auth->getIdentity()->role,
                    // 'company'    => $formData['company'],
                    'active'        => 1,
                    'id_member'     => 0,
                    'id_member_subtype'     => 0,
                    'id_qualification'     => 0,
                    'id_experience'     => 0,
                    'industrial_exp'     => 0,
                    'lecturship_exp'     => 0,
                    'total_amount'     => 0,
                    'payment_status'     => 0,
                    'approve_status'=> 1
                );

              
                $user_id = $dbUser->insert($data);

                Cms_Common::notify('success', 'User successfully created');
                $this->redirect('/admin/users/');
            }
            else
            {
                unset($formData['password']);
                $this->view->formData_json = json_encode($formData);
                $this->view->errmsg = $errmsg;
            }
        }

        $dataDb       = new Admin_Model_DbTable_ExternalData();
        $company      = $dataDb->getData('Company');
        $companies    = array();
        $company_list = array();

        foreach( $company as $row )
        {
            $companies[] = addslashes($row['name']);
            $company_list[$row['id']] = $row['name'];
        }

        $qualification = $dataDb->getData('Qualification');

        $qualification_list = array('' => '');
        foreach( $qualification as $row ) 
        {
            $qualification_list[ $row['id'] ] = $row['name'];
        }

        $this->view->company            = json_encode($companies);
        $this->view->company_list       = json_encode($company_list);
        $this->view->qualification_list = $qualification_list;
    }

    public function viewQualificationAction()
    {
        $id = $this->_getParam('id');

        $user = $this->userDb->fetchRow(array('id = ?' => $id))->toArray();

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        $userDocDb = new App_Model_UserDoc();
        $userDocuments = $userDocDb->getUserDoc($id);

        $this->view->documents = $userDocuments;

        $form = new App_Form_User();
        $form->populate($user);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                //update
                $data = array(
                    'firstname'     => $formData['firstname'],
                    'lastname'      => $formData['lastname'],
                    'email'         => $formData['email'],
                    'modifiedby'    => $this->auth->getIdentity()->id,
                    'modifiedrole'  => $this->auth->getIdentity()->role,
                    'modifieddate'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'timezone'      => $formData['timezone']
                );

                //newpass
                if ( $formData['newpassword'] != '' )
                {
                    $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['newpassword'], PASSWORD_BCRYPT, $options);

                    $data['password'] = $hash;
                    $data['salt'] = $salt;
                }

                //upload file
                if ( $user['photo'] != '' || isset($formData['deletephoto']) )
                {
                    @unlink($this->uploadDir.$user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if ( !empty($files) )
                {
                    $temp = array();
                    foreach ( $files as $file )
                    {
                        $extension = array ( "jpg", "jpeg", "png", "gif" );

                        if ( in_array(substr(strrchr($file['filename'], '.'), 1), $extension) )
                        {
                            $data['photo'] = '/userphoto/'.$files[0]['fileurl'];
                            $photochanged = 1;
                        }
                        else
                        {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }

                // save document data (if any)
                if ( !empty($temp) )
                {
                    $dataFile['user_id']        = $id;
                    $dataFile['doc_title']      = $formData['title'];
                    $dataFile['doc_filename']   = $temp['filename'];
                    $dataFile['doc_hashname']   = $temp['fileurl'];
                    $dataFile['doc_url']        = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location']   = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension']  = $temp['ext'];
                    $dataFile['doc_filesize']   = $temp['filesize'];

                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if ( ( $user['id'] == $this->auth->getIdentity()->id ) && $photochanged )
                {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $this->userDb->update($data, array('id = ?' => $id));

                Cms_Common::notify('success','Information Updated');
                $this->redirect('/admin/users/edit/id/'.$id);
            }
        }
        $this->view->id = $id;
        $this->view->user = $user;
        $this->view->form = $form;

    }



    public function downloadfileAction()
    {

          // echo DOCUMENT_PATH1;
           
         $params = $this->getRequest()->getParams();
        
             if($params['file'])
             { 

                $file = UPLOAD_PATH.$params['file'];
                // echo $file;
                // die();
                
               //$file = $this->file_path;
        
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: data; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit();
             }
         

    }


    public function deletefileAction()
    {
       
            $fileDb = new App_Model_Ciifpdoc();

        $id = $this->_getParam('id');

        if ( $id != '' )
        {
            $file = $fileDb->fetchRow(array('id = ?' => $id));

           

            $file = $file->toArray();

            $fileDb->delete(array('id = ?' => $id));

            //unlink
            @unlink(UPLOAD_PATH."\\".$file['doc_name']);
        
        }
       
    }

 public function apporvefisrtAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
         $memberModel = new App_Model_Member();
        $memberReg =new App_Model_Membership_MemberRegistration();
$get_mail = new Admin_Model_DbTable_Emailtemplate();


 $user_id = $this->auth->getIdentity()->id;
             $query_result = $userModel->approveUser($appl_id);

              // $data = array('update_profile'=> 0 );
                    
              //    $query_result =   $userModel->update($data, array('id = ?' =>$user_id));

       
       

        if($query_result)
        {
           
                echo 1;
                exit();
           }
           else
           {
                echo 3;
                exit();
           }
            
        
          
    }
    
    public function apporveuserAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
         $memberModel = new App_Model_Member();
        $memberReg =new App_Model_Membership_MemberRegistration();
        $get_mail = new Admin_Model_DbTable_Emailtemplate();
         $mail_detail = $get_mail->fnViewTemplte(20);

            $user_id = $this->auth->getIdentity()->id;
            $query_result = $userModel->approveUserlist($appl_id);

        $uniqueId = 'CIIFP'.date('my');
        $membershipId = $uniqueId.$appl_id;
       

        if($query_result)
        {
            $data = array('mr_membershipId' => $appl_id,
            'mr_sp_id' =>  $membershipId,
            'mr_status'=> 1384,
            'mr_activation_date' => date('Y-m-d H:i:s'),
            'mr_expiry_date' => date('Y-m-d H:i:s', strtotime(date("Y-m-d", time()) . " + 1 year")),
            'mr_payment_status' => 1);
          
        $memberReg->insert($data);

          // echo "approved";
          // die();
           $mail = new Cms_SendMail();
           $message = 'Dear '.''.$applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                echo 1;
                exit();
            }
           else
           {
                echo 3;
                exit();
           }
            
        }
        else
        {
            echo 0;
            exit();
        }

        //$appl_name= $this->_getParam('name',null);

        //$arrayName = array('id' =>$appl_id ,'type'=>'student','year'=>2018 );
        //$json = Zend_Json::encode($arrayName);
        //echo ($json);    
        exit();    
    }
    public function addprogramAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
         $memberModel = new App_Model_Member();
        $memberReg =new App_Model_Membership_MemberRegistration();
$get_mail = new Admin_Model_DbTable_Emailtemplate();
 $mail_detail = $get_mail->fnViewTemplte(24);
 //  print_r($mail_detail);

 // die();
        //$user_data =  $userModel->getUserInfo($appl_id);

        //$query_result = $userModel->app_mailroveUser($appl_id);
 $user_id = $this->auth->getIdentity()->id;
             $query_result = $userModel->approveProgram($appl_id);

        $uniqueId = 'CIIFP'.date('my');
        $membershipId = $uniqueId.$appl_id;
       

        if($query_result)
        {
           
          // echo "approved";
          // die();
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                echo 1;
                exit();
           }
           else
           {
                echo 3;
                exit();
           }
            
        }
        else
        {
            echo 0;
            exit();
        }

        //$appl_name= $this->_getParam('name',null);

        //$arrayName = array('id' =>$appl_id ,'type'=>'student','year'=>2018 );
        //$json = Zend_Json::encode($arrayName);
        //echo ($json);    
        exit();    
    }
    
     public function rejectprogramAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
  
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
         $memberModel = new App_Model_Member();
$get_mail = new Admin_Model_DbTable_Emailtemplate();
 $mail_detail = $get_mail->fnViewTemplte(24);
 
        $query_result =  $userModel->rejectProgramUser($appl_id);
        // approval error
        //$user_id = $this->auth->getIdentity()->id;
        

        if($query_result)
        {
            $programRegister = new App_Model_ProgramRegister();
             $get_amount = $programRegister->get_regamount($appl_id);

              $get_exem_amount = $programRegister->get_exemamount($appl_id);
              
               $this->view->get_exem_amount = $get_exem_amount;
            
             $amount = $get_amount+$get_exem_amount;

             $data  = array('cerdit' =>$amount);



            $invoiceMain = new App_Model_InvoiceMain();
            $invoiceMain->update($data, array('id = ?' =>$appl_id));
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                echo 1;
                exit();
           }
           else
           {
                echo 3;
                exit();
           }
            
        }
        else
        {
            echo 0;
            exit();
        }
  
        exit();    
    }


    public function pendingAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
  
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
         $memberModel = new App_Model_Member();
$get_mail = new Admin_Model_DbTable_Emailtemplate();
 $mail_detail = $get_mail->fnViewTemplte(25);
 
      

        if($mail_detail)
        {
            
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                echo 'Email Sent';
                exit();
           }
           
            
        }
        
      
    }

     public function rejectstatusAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
  
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
         $memberModel = new App_Model_Member();
$get_mail = new Admin_Model_DbTable_Emailtemplate();
 $mail_detail = $get_mail->fnViewTemplte(20);
 
        $query_result =  $userModel->rejectUser($appl_id);
        // approval error
        //$user_id = $this->auth->getIdentity()->id;
        

        if($query_result)
        {
            
           $mail = new Cms_SendMail();
           $message = 'Dear'. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                echo 1;
                exit();
           }
           else
           {
                echo 3;
                exit();
           }
            
        }
        else
        {
            echo 0;
            exit();
        }

        //$appl_name= $this->_getParam('name',null);

        //$arrayName = array('id' =>$appl_id ,'type'=>'student','year'=>2018 );
        //$json = Zend_Json::encode($arrayName);
        //echo ($json);    
        exit();    
    }

    public function rejectuserAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
  
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
         $memberModel = new App_Model_Member();
$get_mail = new Admin_Model_DbTable_Emailtemplate();
 $mail_detail = $get_mail->fnViewTemplte(20);
 
        $query_result =  $userModel->rejectUser($appl_id);
        // approval error
        //$user_id = $this->auth->getIdentity()->id;
        

        if($query_result)
        {
            
           $mail = new Cms_SendMail();
           $message = 'Dear'. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                echo 1;
                exit();
           }
           else
           {
                echo 3;
                exit();
           }
            
        }
        else
        {
            echo 0;
            exit();
        }

        //$appl_name= $this->_getParam('name',null);

        //$arrayName = array('id' =>$appl_id ,'type'=>'student','year'=>2018 );
        //$json = Zend_Json::encode($arrayName);
        //echo ($json);    
        exit();    
    }

    public function renewuserAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
  
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
         $memberModel = new App_Model_Member();
$get_mail = new Admin_Model_DbTable_Emailtemplate();
 $mail_detail = $get_mail->fnViewTemplte(21);
 
        $query_result =  $userModel->renewUser($appl_id);

        $get_status = $this->definationDB->getdefCode('Active');
           

            
            $member_reg = new App_Model_Membership_MemberRegistration();

                   $data = array('mr_status' => $get_status[0]['idDefinition'] );
                   $member_reg= $member_reg->update($data, array('mr_membershipId = ?' =>$appl_id));


        
        if($query_result)
        {

            
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                echo 1;
                exit();
           }
           else
           {
                echo 3;
                exit();
           }
            
        }
        else
        {
            echo 0;
            exit();
        }

        //$appl_name= $this->_getParam('name',null);

        //$arrayName = array('id' =>$appl_id ,'type'=>'student','year'=>2018 );
        //$json = Zend_Json::encode($arrayName);
        //echo ($json);    
        exit();    
    }

    public function upgradeuserAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
  
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
         $memberModel = new App_Model_Member();
$get_mail = new Admin_Model_DbTable_Emailtemplate();
 $mail_detail = $get_mail->fnViewTemplte(20);
  // ->order('s.ss_core_subject DESC')

         
        $query_result =  $userModel->upgradeUser($appl_id);

         $get_status = $this->definationDB->getdefCode('Active');
           

            
            $member_reg = new App_Model_Membership_MemberRegistration();

                   $data = array('mr_status' => $get_status[0]['idDefinition'] );
                   $member_reg= $member_reg->update($data, array('mr_membershipId = ?' =>$appl_id));

        
        if($query_result)
        {
            
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                echo 1;
                exit();
           }
           else
           {
                echo 3;
                exit();
           }
            
        }
        else
        {
            echo 0;
            exit();
        }

       
        exit();    
    }

    public function rejectexemptionapprovalAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
  
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
        $memberModel = new App_Model_Member();
        $get_mail = new Admin_Model_DbTable_Emailtemplate();
        $mail_detail = $get_mail->fnViewTemplte(20);
         
        $query_result =  $userModel->updateexemption($appl_id);
        
        if($query_result)
        {
            
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                echo 1;
                exit();
           }
           else
           {
                echo 3;
                exit();
           }
            
        }
        else
        {
            echo 0;
            exit();
        }

       
        exit();    
    }

    
    public function exemptionapprovalAction()
    {
        $this->_helper->layout->disableLayout();

         $appl_id = $this->_getParam('applicant_id',null);
  
         $applicant_email = $this->_getParam('applicant_email',null);
         $applicant_name = $this->_getParam('applicant_name',null);
         
        $userModel = new App_Model_User();
        $memberModel = new App_Model_Member();
        $get_mail = new Admin_Model_DbTable_Emailtemplate();
        $mail_detail = $get_mail->fnViewTemplte(20);
         
        $query_result =  $userModel->updateexemption($appl_id);
        
        if($query_result)
        {
            
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
           if($sendEmail)
           {
                echo 1;
                exit();
           }
           else
           {
                echo 3;
                exit();
           }
            
        }
        else
        {
            echo 0;
            exit();
        }

       
        exit();    
    }

     public function addMaterialAction()
    {

        $this->view->title = $this->view->translate("Add New CPD Material");

        //$auth = Zend_Auth::getInstance();

        $form = new Admin_Form_MaterialForm();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            //print_r($formData);exit;


            $activity = array();
            //$activity['spid'] = $spId;
            $activity['cpdm_type'] = $formData['cpdm_type'];
            $activity['cpdm_category'] = $formData['cpdm_category'];
            $activity['cpdm_activity'] = $formData['cpdm_activity'];
            $activity['cpdm_title'] = $formData['cpdm_title'];
            $activity['cpdm_venue'] = $formData['cpdm_venue'];
            $activity['cpdm_provider'] = $formData['cpdm_provider'];
            $activity['cpdm_desc'] = $formData['cpdm_desc'];
            $activity['cpdm_hours'] = $formData['cpdm_hours'];
            $activity['date_started'] = date("Y-m-d", strtotime($formData['date_started']));
            $activity['date_completion'] = date("Y-m-d", strtotime($formData['date_completion']));
            $activity['created_by'] = $auth->getIdentity()->iduser;
            $activity['created_date'] = date('Y-m-d H:i:s');
            $activity["cpdm_content"] = $formData['cpdm_content'];

            if ($formData['cpdm_content'] == '1637') {//if contetnt=video
                $activity["cpdm_url"] = $formData['cpdm_url'];
            }
            // dd($formData);exit;

            $folder = '/material/';
            $dir = DOCUMENT_PATH . $folder;

            if (!is_dir($dir)) {
                if (mkdir_p($dir) === false) {
                    throw new Exception('Cannot create attachment folder (' . $dir . ')');
                    echo json_encode(array('error' => 'Cannot create attachment folder (' . $dir . ')'));
                    exit;
                }
            }

            $file_adapter = new Zend_File_Transfer_Adapter_Http();
            $file_adapter->addValidator('Extension', false, array('extension' => 'txt,pdf,jpg,png,gif,zip,doc,docx,xls,xlsx', 'case' => false));
            $file_adapter->addValidator('NotExists', false, $dir);
            $file_adapter->setDestination($dir);

            $files = $file_adapter->getFileInfo();

            foreach ($files as $file => $info) {
                if (!$info['name']) {
                    continue;
                }

                if ($file && !$file_adapter->isValid()) {
                    echo json_encode(array('error' => 'Failed to upload file. CODE:1'));
                    exit;
                } elseif ($file && $file_adapter->isValid()) {
                    $fileOriName = $info['name'];
                    $fileRename = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                    $filepath = $info['destination'] . '/' . $fileRename;

                    if (!$file_adapter->isUploaded($file)) {
                        echo json_encode(array('error' => 'Failed to upload file. CODE:2'));
                        exit;
                    }

                    $file_adapter->addFilter('Rename', array('target' => $filepath, 'overwrite' => true), $file);

                    if ($file_adapter->receive($file) != 1) {
                        echo json_encode(array('error' => 'Failed to upload file. CODE:3'));
                        exit;
                    }

                    if (!is_file($filepath)) {
                        if (!move_uploaded_file($file['tmp'], $filepath)) {
                            echo json_encode(array('error' => 'Failed to upload file. CODE:4'));
                            exit;
                        }
                    }


                    $activity["cpdm_filename"] = $fileOriName;
                    $activity["cpdm_url"] = $filepath;
                    $activity["cpdm_location"] = $folder . $fileRename;
                    $activity["cpdm_filesize"] = $info['size'];
                    $activity["cpdm_extension"] = $info['type'];

                }
            }

            $cpdDB = new Membership_Model_DbTable_CpdMaterial();
            $cpdDB->addData($activity);

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

            $this->_redirect($this->view->url(array('module' => 'membership', 'controller' => 'cpd', 'action' => 'material-list'), 'default', true));


        }

    }

    public function editMaterialAction()
    {

        $this->view->title = $this->view->translate("Edit CPD Material");

        $auth = Zend_Auth::getInstance();

        $mid = $this->_getParam('id', null);
        $this->view->mid = $mid;

        $materialDB = new Membership_Model_DbTable_CpdMaterial();
        $material = $materialDB->getMaterialList($mid);
        $this->view->material = $material;
        $form = new Membership_Form_MaterialForm(array('mid' => $mid));
        $form->populate($material);
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $activity = array();
            $activity['cpdm_type'] = $formData['cpdm_type'];
            $activity['cpdm_category'] = $formData['cpdm_category'];
            $activity['cpdm_activity'] = $formData['cpdm_activity'];
            $activity['cpdm_title'] = $formData['cpdm_title'];
            $activity['cpdm_desc'] = $formData['cpdm_desc'];
            $activity['cpdm_hours'] = $formData['cpdm_hours'];
            $activity['created_by'] = $auth->getIdentity()->iduser;
            $activity['created_date'] = date('Y-m-d H:i:s');
            $activity["cpdm_content"] = $formData['cpdm_content'];

            if ($formData['cpdm_content'] == '1637') {//if contetnt=video
                $activity["cpdm_url"] = $formData['cpdm_url'];
            }

            $cpdDB = new Membership_Model_DbTable_CpdMaterial();
            $cpdDB->updateData($activity, $mid);

            $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
            $this->_redirect($this->view->url(array('module' => 'membership', 'controller' => 'cpd', 'action' => 'edit-material'), 'default', true) . '/id/' . $mid);

        }

    }



}


