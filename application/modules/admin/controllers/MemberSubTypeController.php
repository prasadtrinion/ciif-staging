<?php
class Admin_MemberSubTypeController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
		// $this->gobjsessionsis = Zend_Registry::get('sis'); //initialize session variable
		// $lobjinitialconfigModel = new GeneralSetup_Model_DbTable_Initialconfiguration(); //user model object
		// $larrInitialSettings = $lobjinitialconfigModel->fnGetInitialConfigDetails($this->gobjsessionsis->idUniversity);
		// $this->gintPageCount = isset($larrInitialSettings['noofrowsingrid'])?$larrInitialSettings['noofrowsingrid']:"5";
		// $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object	  
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_MemberSub(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view	
		$lobjemailTemplateModel = new Admin_Model_DbTable_MemberSub();  // email template model object
		$larrresult = $lobjemailTemplateModel->getsearch(); // get template details	
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();						
				if ($lobjsearchform->isValid($larrformData)) {
				$post_data=$this->getRequest()->getPost();
           
           				 $id=$post_data['field2'];	
					$larrresult = $lobjemailTemplateModel->search($id);	
					if(empty($larrresult))
					{
						$this->_redirect( $this->baseUrl . '/admin/MemberSubType/index');
					}
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
			$this->_redirect( $this->baseUrl . '/admin/MemberFeeStructure/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function addAction() { 	



  			$member_form = new Admin_Form_Memberaddsub();
            $this->view->form = $member_form;

            if($this->getRequest()->isPost())
            {    
        

                $formdata =$this->getRequest()->getPost();
                
                if($member_form->isValid($formdata))
                {

                    // echo "<pre>";
                    // print_r($formdata);
                    // die();
                    $dropdown_member = $member_form->getValue('dropdown_member'); 


                   
                    $sub_type = $member_form->getValue('sub_type');

                      
                    $add_member = new Admin_Model_DbTable_MemberSub();
                  
                    $add_member->addData($dropdown_member,$sub_type);
                    if($add_member)
                    {
                        $this->_redirect( $this->baseUrl . '/admin/MemberSubType/index');
                    }
                    
                }


            }
	}

	public function editAction() {		
 		
 	 	
            $editform = new Admin_Form_Membereditsub();
    		$this->view->form = $editform;
		if($this->getRequest()->isPost())

		{

			$member_form = $this->getRequest()->getPost();
			if($editform->isValid($member_form))
		{

					$id = $this->getRequest()->getparam('Id');

					$dropdown_member = $editform->getValue('id_member'); 
						

                   
                    $sub_type = $editform->getValue('member_subtype_name');

                 
				$edit_member = new Admin_Model_DbTable_MemberSub();
				$edit_member->editmember($id,$dropdown_member,$sub_type);

                    if($edit_member)
                    {
                         $this->_redirect( $this->baseUrl . '/admin/MemberSubType/index');
                    }

                  //Line 5
//$this->_helper->redirector('index');
			}

		else
		{
    		 $editform->populate($formData);
		}


		}
		 else
		              {
                        $id = $this->getRequest()->getparam('Id');
                        	
                         $file = new Admin_Model_DbTable_MemberSub();
                         $files = $file->fetchRow('id_subtype='.$id);
                         $editform->populate($files->toArray());
                        
                     }    
$this->view->form = $editform;  	  	
}



public function getsubtypeAction()
{
	$this->_helper->layout->disableLayout();

	$memberId = $this->_getParam('member_id',null);


	$query =  new Admin_Model_DbTable_MemberGet();

	$memberdata = $query->getmemberdata($memberId);
	$json = Zend_Json::encode($memberdata);

	
				
                    print_r($json);
                    die();

	 exit();
}


}