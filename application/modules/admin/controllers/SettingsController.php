<?php
class Admin_SettingsController extends  Zend_Controller_Action
{
	protected $permResourcesDb;

	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'settings');

		//
		$this->permResourcesDb = new App_Model_PermissionResources();
		$this->permRoleDb = new App_Model_PermissionRole();

		//events
		Cms_Events::attach('options.update', array('Cms_Options','updateCache'));
	}

	public function indexAction()
	{
		$this->view->title = 'Server';
	}

	public function generalAction()
	{
		$this->view->title = 'General';

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();

			if ( !isset($formData['options']) )
			{
				//must have else blank
				$formData['options'] = array();

				//checkboxes
				$formData['options']['nestedcategory'] = '';
			}

			Cms_Options::updateOptions($formData['options']);

			Cms_Common::notify('success','Settings updated successfully');
			$this->redirect('/admin/settings/general/');
		}
	}

	public function catalogAction()
	{
		$this->view->title = 'Catalog';

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();

			if ( !isset($formData['options']) )
			{
				//must have else blank
				$formData['options'] = array();


			}

			Cms_Options::updateOptions($formData['options']);

			Cms_Common::notify('success','Settings updated successfully');
			$this->redirect('/admin/settings/catalog/');
		}
	}

	public function languagesAction()
	{
		$this->view->title = 'Languages';
	}

	public function appearanceAction()
	{
		$this->view->title = 'Appearance';
	}

	public function socialAction()
	{
		$this->view->title = 'Social';
	}

	public function privacyAction()
	{
		$this->view->title = 'Privacy';

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();

			if ( !isset($formData['options']) )
			{
				//must have else blank
				$formData['options'] = array();
			}

			Cms_Options::updateOptions($formData['options']);

			Cms_Common::notify('success','Settings updated successfully');
			$this->redirect('/admin/settings/privacy/');
		}
	}

	public function permissionsAction()
	{
		$this->view->title = 'Permissions';

		$roles = Cms_Dataset::get('roles');

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();

			$this->permRoleDb->updateAll($formData);

			//update cache
			$cache = Zend_Registry::get('cache');
			$cache->save($this->permRoleDb->getAll(true), 'permission');

			Cms_Common::notify('success','Permission updated successfully');
			$this->redirect('/admin/settings/permissions/');

		}

		$perm = $this->permRoleDb->getAll();


		$this->view->perm = $perm;
		$this->view->roles = $roles;
		$this->view->resources = $this->permResourcesDb->fetchAll()->toArray();

	}

	public function maintenanceAction()
	{
		$this->view->title = 'Maintenance';

		$do = $this->_getParam('do');

		if ( $do == 'cache' )
		{
			$cache = Zend_Registry::get('cache');
			$cache->clean(Zend_Cache::CLEANING_MODE_ALL);

			Cms_Common::notify('success','Cache cleared successfully');
			$this->redirect('/admin/settings/maintenance/');
		}

		//external
		$externalDb = new Admin_Model_DbTable_ExternalDatatype();

		$this->view->external = $externalDb->getData();
	}

	public function maintenanceRefreshAction()
	{
		global $db;

		$type = $this->_getParam('type');

		$path = realpath(APPLICATION_PATH.'/../cron/service');

		$db = getDB();

		include_once $path . '/external.php';

		Service_External::initMaintenance($type);

		Cms_Common::notify('success','Dataset "'.$type.'" refreshed successfully');
		$this->redirect('/admin/settings/maintenance/');
		exit;
	}

	public function maintenanceSyncAction()
	{
		$type = $this->_getParam('type');

		if ( !is_callable(array(__CLASS__, $type)) )
		{
			throw new Exception('No Action Found');
		}

		call_user_func(array(__CLASS__, 'sync'.$type));
	}

	public function syncCurriculum()
	{
		$currDb = new App_Model_Curriculum();

		$curriculum = $currDb->fetchAll()->toArray();
		$current_curr = array_column($curriculum, 'code');

		$currbyid = array();
		$parents = array();
		foreach ( $curriculum as $row )
		{
			if ( $row['display_only'] == 1 )
			{
				$parents[] = $row['external_id'];
				$currbyid[$row['external_id']] = $row['id'];
			}
		}


		//external
		$dataDb = new Admin_Model_DbTable_ExternalData();
		$results = $dataDb->getData('Curriculum');
		$groupresults = $dataDb->getData('CurriculumGroup');


		$progByGroup = array();
		foreach ( $results as $row )
		{
			$rowData = json_decode($row['fulldata'], true);

			$progByGroup[$rowData['group_id']][] = $rowData;
		}

		$newgroup  = 0;
		$newcur = 0;


		foreach ( $groupresults as $group )
		{
			$group_id = isset($currbyid[$group['code']]) ? $currbyid[$group['code']] : 0;

			if ( !in_array( intval($group['code']), $parents ))
			{
				$data = array(
					'name'      	=> $group['name'],
					'code'			=> $group['code'],
					'description'	=> '',
					'visibility'	=> 1,
					'display_only'	=> 1,
					'parent_id'		=> 0,
					'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
					'external_id'	=> $group['code']
				);

				$group_id = $currDb->insert($data);

				$newgroup++;
			}

			//add curr
			if ( isset($progByGroup[$group['code']])) {
				foreach ($progByGroup[$group['code']] as $prog) {
					if (!in_array($prog['ProgramCode'], $current_curr)) {
						$data = array(
							'name' => $prog['ProgramName'],
							'code' => $prog['ProgramCode'],
							'description' => '',
							'visibility' => 1,
							'parent_id' => $group_id,
							'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
							'created_by' => Zend_Auth::getInstance()->getIdentity()->id,
							'external_id' => $prog['IdProgram']
						);

						$currDb->insert($data);

						$newcur++;
					}
				}
			}
		}

		//log
		Cms_SystemLog::add('syncCurriculum', 'New Curr Parent: '.$newgroup.', New Curriculum:'.$newcur);

		$path = realpath(APPLICATION_PATH.'/../cron/service');
		include_once $path . '/external.php';

		global $db;
		$db = getDB();
		Service_External::CurriculumCourses();


		Cms_Common::notify('success','Curriculum synced');
		$this->redirect('/admin/settings/maintenance/');
		exit;
	}

	public function syncCourses()
	{
		$courseDb = new App_Model_Courses();

		$courses = $courseDb->getCourses();
		$courses = Zend_Db_Table::getDefaultAdapter()->fetchAll( $courses );

		$current_courses = array_column($courses, 'code');

		//external
		$dataDb = new Admin_Model_DbTable_ExternalData();

		$results = $dataDb->getData('Courses');

		$added = 0;

		$coursesDb = new App_Model_Courses();

		foreach ( $results as $row )
		{
			if ( !in_array( $row['code'], $current_courses ))
			{
				$data = array(
					'title'      	=> $row['name'],
					'description'	=> '',
					'welcome'		=> '',
					'excerpt'		=> '',
					'category_id'	=> 10, //core
					'code'			=> $row['code'],
					'code_safe'		=> Cms_Common::safename($row['code']),
					'visibility'	=> 1,
					'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
					'use_comments'	=> 1,
					'use_forum'		=> 1,
					'external_id'	=> $row['value'],
					'learning_online'=> 1,
					'learning_ftf'	=> 1
				);

				$course_id = $coursesDb->insert($data);

				$added++;
			}
		}

		Cms_Common::notify('success','Courses synced. New Courses: '.$added);

		//add into log
		Cms_SystemLog::add('Course Sync', 'New Courses: '.$added);

		$this->redirect('/admin/settings/maintenance/');
		exit;
	}
	
	public function initCountryAction()
    {
        $countries = Cms_Common::getCountryList();
        $db = getDB();
        
        foreach ($countries as $code => $name)
        {
            $country = array(
                'name'          => strtoupper($name),
                'code'          => $code,
                'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()')
            );
            $db->insert('country', $country);
        }
        
        exit;
    }
    
    public function syncCountryAction()
    {
        $db = getDB();
        $db2 = getDB2();
    
        $exclude_countries = array('IL');
        
        $select      = $db->select()->from(array('a' => 'country'));
        $tmp         = $db->fetchAll($select);
        $lms_countries = array();
        
        foreach ($tmp as $row)
        {
            $country_code = $row['code'];
            $lms_countries[$country_code] = $row;
        }
    
        $select        = $db2->select()->from(array('a' => 'tbl_countries'));
        $sms_countries = $db2->fetchAll($select);
        
        foreach ($sms_countries as $i => $row)
        {
            $country_code = strtoupper($row['CountryIso']);
            
            if (!$country_code || in_array($country_code, $exclude_countries))
            {
                continue;
            }
            
            $external_id = $row['idCountry'];
            
            if (!isset($lms_countries[$country_code]))
            {
                $country = array(
                    'name'          => strtoupper($row['CountryName']),
                    'code'          => $country_code,
                    'external_id'   => $external_id,
                    'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()')
                );
                $db->insert('country', $country);
                $country['id'] = $db->lastInsertId();
                
                $lms_countries[$country_code] = $country;
            }
            elseif (!$lms_countries[$country_code]['external_id'])
            {
                $country_id = $lms_countries[$country_code]['id'];
                $lms_countries[$country_code]['external_id'] = $external_id;
                $country = array(
                    'external_id'   => $external_id,
                    'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()')
                );
                $db->update('country', $country, array('id = ?' => $country_id));
            }
    
            $lms_countries[$country_code]['states'] = array();
        }
        
        $select = $db->select()
            ->from(array('a' => 'state'))
            ->join(array('b' => 'country'), 'b.id = a.country_id', array('country_code' => 'code'));
        $tmp = $db->fetchAll($select);
        
        foreach ($tmp as $row)
        {
            $country_code = strtoupper($row['country_code']);
            $state_name   = strtoupper(trim($row['name']));
            $lms_countries[$country_code]['states'][$state_name] = $row;
        }
        
        $select = $db2->select()
            ->from(array('a' => 'tbl_state'))
            ->join(array('b' => 'tbl_countries'), 'b.idCountry = a.idCountry', array('CountryISO'));
        $sms_states = $db2->fetchAll($select);
        
        foreach ($sms_states as $row)
        {
            $country_code = strtoupper(trim($row['CountryISO']));
            $name         = strtoupper(trim($row['StateName']));
            $state_code   = ($row['StateCode'] == '' ? null : strtoupper(trim($row['StateCode'])));
            
            if (!$country_code || in_array($country_code, $exclude_countries))
            {
                continue;
            }
            
            if (!isset($lms_countries[$country_code]['states'][$name]))
            {
                if (!isset($lms_countries[$country_code]))
                {
                    die('No country with '. $country_code);
                }
    
                if (!$lms_countries[$country_code]['id'])
                {
                    pr($lms_countries[$country_code]);
                    die('Country ID is null for '. $country_code);
                }
                
                $country_id = $lms_countries[$country_code]['id'];
                
                $state = array(
                    'name'                => $name,
                    'code'                => $state_code,
                    'country_id'          => $country_id,
                    'external_id'         => $row['idState'],
                    'external_country_id' => $row['idCountry'],
                    'modified_date'       => new Zend_Db_Expr('UTC_TIMESTAMP()')
                );
                $db->insert('state', $state);
                $state['id'] = $db->lastInsertId();
                
                $lms_countries[$country_code]['states'][$name] = $state;
            }
            elseif (!$lms_countries[$country_code]['states'][$name]['external_id'])
            {
                $state_id = $lms_countries[$country_code]['states'][$name]['id'];
                $state = array(
                    'external_id'   => $row['idState'],
                    'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()')
                );
                $db->update('state', $state, array('id = ?', $state_id));
                $lms_countries[$country_code]['states'][$name]['external_id'] = $row['idState'];
            }
    
            $lms_countries[$country_code]['states'][$name]['cities'] = array();
        }
    
        $select = $db->select()
            ->from(array('a' => 'city'))
            ->join(array('b' => 'state'), 'b.id = a.state_id', array('state_name' => 'name'))
            ->join(array('c' => 'country'), 'c.id = b.country_id', array('country_code' => 'code'));
        $tmp = $db->fetchAll($select);
    
        foreach ($tmp as $row)
        {
            $country_code = strtoupper(trim($row['country_code']));
            $state_name   = strtoupper(trim($row['state_name']));
            $city_name    = strtoupper(trim($row['name']));
            $lms_countries[$country_code]['states'][$state_name]['cities'][$city_name] = $row;
        }
    
        $select = $db2->select()
            ->from(array('a' => 'tbl_city'), array('idCity', 'CityCode', 'CityName'))
            ->join(array('b' => 'tbl_state'), 'b.idState = a.idState', array('idState', 'state_name' => 'StateName'))
            ->join(array('c' => 'tbl_countries'), 'c.idCountry = b.idCountry', array('idCountry', 'country_code' => 'CountryISO'));
        $sms_cities = $db2->fetchAll($select);
    
        foreach ($sms_cities as $row)
        {
            $country_code = strtoupper(trim($row['country_code']));
    
            if (!$country_code || in_array($country_code, $exclude_countries))
            {
                continue;
            }
            
            $state_name = strtoupper(trim($row['state_name']));
            $state_id   = $lms_countries[$country_code]['states'][$state_name]['id'];
            $city_name  = strtoupper(trim($row['CityName']));
            $city_code  = ($row['CityCode'] == '' ? null : strtoupper(trim($row['CityCode'])));
            
            if (!$state_id)
            {
                pr($lms_countries[$country_code]['states'][$state_name]);
                die('state_id is null');
            }
            
            if (!isset($lms_countries[$country_code]['states'][$state_name]['cities'][$city_name]))
            {
                $city = array(
                    'name'              => $city_name,
                    'code'              => $city_code,
                    'state_id'          => $state_id,
                    'external_id'       => $row['idCity'],
                    'external_state_id' => $row['idState'],
                    'modified_date'     => new Zend_Db_Expr('UTC_TIMESTAMP()')
                );
                $db->insert('city', $city);
                $city['id'] = $db->lastInsertId();
    
                $lms_countries[$country_code]['states'][$state_name]['cities'][$city_name] = $city;
            }
            elseif (!$lms_countries[$country_code]['states'][$state_name]['cities'][$city_name]['external_id'])
            {
                $city_id = $lms_countries[$country_code]['states'][$state_name]['cities'][$city_name]['id'];
                $city    = array(
                    'external_id'   => $row['idCity'],
                    'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()')
                );
                $db->update('city', $city, array('id = ?', $city_id));
                $lms_countries[$country_code]['states'][$state_name]['cities'][$city_name]['external_id'] = $row['idCity'];
            }
        }
    
        Cms_Common::notify('success','Country, State and City updated successfully');
        $this->redirect('/admin/settings/maintenance/');
    }

}

