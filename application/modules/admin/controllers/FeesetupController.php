<?php
class Admin_FeesetupController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
		// $this->gobjsessionsis = Zend_Registry::get('sis'); //initialize session variable
		// $lobjinitialconfigModel = new GeneralSetup_Model_DbTable_Initialconfiguration(); //user model object
		// $larrInitialSettings = $lobjinitialconfigModel->fnGetInitialConfigDetails($this->gobjsessionsis->idUniversity);
		// $this->gintPageCount = isset($larrInitialSettings['noofrowsingrid'])?$larrInitialSettings['noofrowsingrid']:"5";
		// $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object	  
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_Gst(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view	
		$lobjemailTemplateModel = new Admin_Model_DbTable_Gst();  // email template model object
		$larrresult = $lobjemailTemplateModel->fnGetTemplateDetails(); // get template details	
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();	
							
				if ($lobjsearchform->isValid($larrformData)) {	
					
					$larrresult = $lobjemailTemplateModel->fnSearchTemplate($lobjsearchform->getValues());						
					if(empty($larrresult))
					{
						$this->_redirect( $this->baseUrl . '/admin/gstmaster/index');
					}				
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
			$this->_redirect( $this->baseUrl . '/admin/gstmaster/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function addAction() { 	
  					$member_form = new Admin_Form_Gst();
            $this->view->form = $member_form;

            if($this->getRequest()->isPost())
            {    
        

                $formdata =$this->getRequest()->getPost();
                
                if($member_form->isValid($formdata))
                {

                    // echo "<pre>";
                    // print_r($formdata);
                    // die();
                    $member_name = $member_form->getValue('name'); 

                    $sub_type = $member_form->getValue('description');
                    $percentage=$member_form->getValue('percentage');
                      
                    $add_member = new Admin_Model_DbTable_Gst();
                    $add_member->addData($member_name,$sub_type,$percentage);
                    if($add_member)
                    {
                        $this->_redirect( $this->baseUrl .'/admin/gstmaster/index');
                    }
                    
                }


            }
	}

	public function editAction() {		
 		
 	 	
            $editform = new Admin_Form_Gst();
    		$this->view->form = $editform;
		if($this->getRequest()->isPost())

		{

			$formData = $this->getRequest()->getPost();
			if($editform->isValid($formData))
		{

			$id = $this->getRequest()->getparam('Id');                                                                               //ine 4
			$memberName=$editform->getValue('name');

             $subType=$editform->getValue('description');

             $percentage=$editform->getValue('percentage');
                 
				$edit_member = new Admin_Model_DbTable_Gst();
				$edit_member->editmember($id,$memberName,$subType,$percentage);

                    if($edit_member)
                    {
                         $this->_redirect( $this->baseUrl . '/admin/gstmaster/index');
                    }

                  //Line 5
//$this->_helper->redirector('index');
			}

		else
		{
    		 $editform->populate($formData);
		}


		}
		 else
		              {
                        $id = $this->getRequest()->getparam('Id');
                        	
                    
                         $file = new Admin_Model_DbTable_Company();
                         $files = $file->fetchRow('id_cmp='.$id);
                         $editform->populate($files->toArray());
                        
                     }    
$this->view->form = $editform;  	  	
}
}