<?php
class Admin_ProgrammeLandscapeController extends  Zend_Controller_Action
{
	protected $coursecatDb;


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'Programmecategory');

		$this->programDb = new App_Model_ProgramLandscape();
		$this->courseDb = new App_Model_Courses();
		$this->definationDB =new Admin_Model_DbTable_Definition();
		


	}

	public function indexAction()
	{


		$form = new Admin_Form_CourseCategory();
		$this->view->form = $form;
	
		$session  = new Zend_Session_Namespace('AdminCoursecategoryIndex');
        $session->setExpirationSeconds(60*5);
        $search   = null;
        $formdata = array();

        $effective_intake = $this->definationDB->getByCode('Enroll Intake');

		foreach ($effective_intake as $opt8 )
		{
			$form->effective_intake->addMultiOption($opt8['idDefinition'], $opt8['DefinitionDesc']);
		}

		$learningLists = $this->definationDB->getByCode('Program Route');

		foreach ($learningLists as $opt )
		{
			$form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
		}

		$programLists = $this->definationDB->getByCode('Program Level');

		foreach ($programLists as $opt1 )
		{
			$form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
		}



        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            if (empty($formdata['search'])) {
                unset($session->search);
                $this->redirect('/admin/programme-landscape/');
            }
            $session->search = $formdata['search'];
            $this->redirect('/admin/programme-landscape/index/search/1');
        }

        if (!$this->_getParam('search', 0)) {
            unset($session->search);
        }

        if (isset($session->search)) {
            $search = $session->search;
            $formdata['search'] = $search;
        }

		$this->view->title = 'Program Landscape';

				$results = $this->programDb->get_details();


		if (isset($formdata['search']) && $formdata['search'])
		{
		$results = $this->programDb->get_landscape($formdata['search']);

			
		}

		

		$this->view->results = $results;
		$this->view->formdata = $formdata;

	}

	public function addAction()
	{
		$form = new Admin_Form_CourseCategory();

		$programLists = $this->definationDB->getByCode('Program Level');

		foreach ($programLists as $opt1 )
		{
			$form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
		}

		$pid = $this->_getParam('pid');

		$this->view->title = "Program Landscape";

		$cats = $this->programDb->fetchAll()->toArray();

		$Landscape = $this->definationDB->getByCode('Landscape');

		foreach ($Landscape as $opt6 )
		{
			$form->program_landscape->addMultiOption($opt6['idDefinition'], $opt6['DefinitionDesc']);
		}

		$learningLists = $this->definationDB->getByCode('Program Route');

		foreach ($learningLists as $opt )
		{
			$form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
		}
		$effective_intake = $this->definationDB->getByCode('Enroll Intake');

		foreach ($effective_intake as $opt8 )
		{
			$form->effective_intake->addMultiOption($opt8['idDefinition'], $opt8['DefinitionDesc']);
		}
		$statusLists = $this->definationDB->getByCode('Status');

		foreach ($statusLists as $opt7 )
		{
			$form->l_status->addMultiOption($opt7['idDefinition'], $opt7['DefinitionDesc']);
		}

		$learningLists = $this->definationDB->getByCode('Program Route');

		foreach ($learningLists as $opt )
		{
			$form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
		}


		$programMode = $this->definationDB->getByCode('Mode of Program');

		foreach ($programMode as $opt5 )
		{
			$form->mode_of_program->addMultiOption($opt5['idDefinition'], $opt5['DefinitionDesc']);
		}

		$programLists = $this->definationDB->getByCode('Program Level');

		foreach ($programLists as $opt1 )
		{
			$form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
		}
		

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				
				$fail = 0;

				$data = array(
				'program_landscape'   => $formData['program_landscape'],
						
				'effective_date'	=> $formData['effective_date'],
						
				'program_route'	=> $formData['program_route'],
				'program_category'	=> $formData['program_category'],		
				'program_level'	=> $formData['program_level'],
				'mode_of_program' => $formData['mode_of_program'],
				'effective_intake' => $formData['effective _intake'],
				'l_status'		=> $formData['l_status'],
					
				'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
				'created_by'    => $this->auth->getIdentity()->id,
				'active'		=> $formData['active']
					);

				$course_id = $this->programDb->insert($data);

				Cms_Common::notify('success','Programme Landscape successfully created');
				$this->redirect('/admin/programme-landscape/');

			
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->_getParam('id');

		$cat = $this->programDb->fetchRow(array("id = ?" => $id))->toArray();



		if (empty($cat)) {
			throw new Exception('Invalid Program Category');
		}

		$form = new Admin_Form_CourseCategory();

		$learningLists = $this->definationDB->getByCode('Program Route');

		foreach ($learningLists as $opt )
		{
			$form->program_route->addMultiOption($opt['idDefinition'], $opt['DefinitionDesc']);
		}

		$Landscape = $this->definationDB->getByCode('Landscape');

		foreach ($Landscape as $opt6 )
		{
			$form->program_landscape->addMultiOption($opt6['idDefinition'], $opt6['DefinitionDesc']);
		}
		
		$statusLists = $this->definationDB->getByCode('Status');

		foreach ($statusLists as $opt7 )
		{
			$form->l_status->addMultiOption($opt7['idDefinition'], $opt7['DefinitionDesc']);
		}


		$programMode = $this->definationDB->getByCode('Mode of Program');

		foreach ($programMode as $opt5 )
		{
			$form->mode_of_program->addMultiOption($opt5['idDefinition'], $opt5['DefinitionDesc']);
		}

		$effective_intake = $this->definationDB->getByCode('Enroll Intake');

		foreach ($effective_intake as $opt8 )
		{
			$form->effective_intake->addMultiOption($opt8['idDefinition'], $opt8['DefinitionDesc']);
		}


		$programLists = $this->definationDB->getByCode('Program Level');

		foreach ($programLists as $opt1 )
		{
			$form->program_level->addMultiOption($opt1['idDefinition'], $opt1['DefinitionDesc']);
		}

		//modified
		if ($cat['updated_by'] != null)
		{
			$userDb = new App_Model_User;
			$user = $userDb->getUser($cat['updated_by']);
			$this->view->modified_user = $user;
		}

		$this->view->title = "Edit Program Category";

		$cats = $this->programDb->fetchAll(array('active = 1'))->toArray();



		//populate
		$form->populate($cat);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				$data = array(
					'program_landscape'      	=> $formData['program_landscape'],
						
					'effective_date'	=> $formData['effective_date'],
						'program_category'	=> $formData['program_category'],
					'program_route'	=> $formData['program_route'],
						
					'program_level'	=> $formData['program_level'],
					'mode_of_program' => $formData['mode_of_program'],
				'effective_intake' => $formData['effective _intake'],
						'l_status'		=> $formData['l_status'],
					
					'updated_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'updated_by'   => $this->auth->getIdentity()->id,
					'active'		=> $formData['active']
				);

				$this->programDb->update($data, array('id = ?' => $id));

				Cms_Common::notify('success','Programme Landscape successfully edited');
				$this->redirect('/admin/programme-landscape');

			
		}

		$this->view->form = $form;
		$this->view->cat = $cat;
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$cat = $this->coursecatDb->fetchRow(array("id = ?" => $id))->toArray();

		if (empty($cat)) {
			throw new Exception('Invalid Course Category');
		}

		//move all existing categories
		$this->courseDb->update(array('category_id' => 0), array('category_id = ?' => $id) );


		//delete
		$this->coursecatDb->delete(array('id = ?' => $id));

		Cms_Common::notify('success','Course category successfully deleted');
		$this->redirect('/admin/programme-landscape/');
	}


}

