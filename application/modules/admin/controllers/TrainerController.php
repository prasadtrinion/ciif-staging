<?php
class Admin_TrainerController extends  Zend_Controller_Action
{
	protected $userDb;
	protected $uploadDir;

	public function init()
	{
		//$this->_helper->layout()->setLayout('/admin');
		$this->auth = Zend_Auth::getInstance();
		if ($this->auth->getIdentity()->role == 'administrator' )
			{
				$this->_helper->layout()->setLayout('/admin');
			}
		else if ( $this->auth->getIdentity()->role == 'manager' )
			{
				$this->_helper->layout()->setLayout('/manager');
			}
		else if ( $this->auth->getIdentity()->role == 'lecturer' )
			{
				$this->_helper->layout()->setLayout('/trainer');
			}


		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'users');
		$this->userDb = new App_Model_User();

		$this->uploadDir = DOCUMENT_PATH . '/userphoto';
		$this->view->uploadDir = $this->uploadDir;
	}

	public function indexAction()
	{

		$onlineDb = new App_Model_UserOnline();
		$onlineusers = $onlineDb->getOnlineIds();


		$getusers = $this->userDb->select();

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();
			$formData['search'] = trim($formData['search']);

			if ( $formData['search'] != '')
			{
				$getusers->where('username LIKE ? OR firstname LIKE ? OR lastname LIKE ? OR CONCAT_WS(" ",firstname,lastname) LIKE ?', '%'.$formData['search'].'%');
			}


			$this->view->formdata = $formData;
		}

		$users = Cms_Paginator::query($getusers);

		$this->view->onlineusers = $onlineusers;
		$this->view->users = $users;

	}

}

