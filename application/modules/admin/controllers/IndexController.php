

<?php

class Admin_IndexController extends  Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'dashboard');
	}

	public function indexAction()
	{
		$db = getDb();
		$online = new App_Model_UserOnline();

		$count = $db->fetchOne( 'SELECT COUNT(*) AS total FROM user' );

		$coursecount = $db->fetchOne( 'SELECT COUNT(*) AS total FROM courses WHERE visibility != 0' );


		$this->view->totalusers = $count;
		$this->view->totalactivecourses = $coursecount;
		$this->view->totalonline = $online->getOnlineInt();
		$this->view->totalonlineadmin = $online->getOnlineInt('','administrator');

		 $auth        = Zend_Auth::getInstance();
       $role_id = $auth->getIdentity()->role_id;

            if($role_id == '1')
                {
                $this->redirect('/admin/users/');
            }   
            if($role_id == '2'){
                $this->redirect('/admin/users/rolelist');

            }
	}
	public function sendAction(){

        $mail = new Cms_SendMail();
		$db = getDb();
		$emails = $db->fetchAll( 'SELECT email FROM user' );
		print_r($emails);
		die();
        $msg = str_replace('[Name]', $NotiExam['firstname'] , $msg);
        $mail->fnSendMail($email, $msg['tpl_name'],$msg['tpl_content']);

	}
}

