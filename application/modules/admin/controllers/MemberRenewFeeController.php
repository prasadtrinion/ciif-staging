<?php
class Admin_MemberRenewFeeController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
		
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_MemberFeeSearch(); 
		$this->view->form = $lobjsearchform; 
	$lobjemailTemplateModel = new Admin_Model_DbTable_MemberRenewFee();  
		$larrresult = $lobjemailTemplateModel->getsearch();
		
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();						
				if ($lobjsearchform->isValid($larrformData)) {
				$post_data=$this->getRequest()->getPost();
           
           				 $id=$post_data['field2'];	
					$larrresult = $lobjemailTemplateModel->search($id);	
					
					
		    		if(empty($larrresult))
		    		{
		    			$this->_redirect( $this->baseUrl . '/admin/MemberRenewFee/index');
		
		    		}
		    		$paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);

		    		$this->view->paginator = $paginator;
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
			$this->_redirect( $this->baseUrl . '/admin/MemberRenewFee/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function addAction() { 	



  			$member_form = new Admin_Form_MemberFee();
            $this->view->form = $member_form;

            if($this->getRequest()->isPost())
            {    
        

                $formdata =$this->getRequest()->getPost();

                if($member_form->isValid($formdata))
                {

                    
                    $dropdown_fee = $member_form->getValue('dropdown_fee'); 
                    $dropdown_currency = $member_form->getValue('dropdown_currency'); 


                    
                    $dropdown_member = $member_form->getValue('dropdown_member');
                    
 

                    $sub_type = $member_form->getValue('Sub_type');
                    $amount = $member_form->getValue('amount');
                    $nationality = $member_form->getValue('ddl_nationality');
                    $degree = $member_form->getValue('dropdown_degree');
                    $date = $member_form->getValue('effective_date');
                    $active = $member_form->getValue('active');
                    $gst    = $member_form->getValue('gst');
                    $calculated_amount = $member_form->getValue('calculated_amount');
                    $renewal_year = $member_form->getValue('renewal_year');
                      
                    $add_member = new Admin_Model_DbTable_MemberRenewFee();
                  
                    $add_member->addData($dropdown_fee,$dropdown_currency,$dropdown_member,$sub_type,$amount,$nationality,$degree,$date,$active,$gst,$calculated_amount,$renewal_year);
                    if($add_member)
                    {
                        $this->_redirect( $this->baseUrl . '/admin/MemberRenewFee/index');
                    }
                    
                }


            }
	}

	public function editAction() {		
 		
 	 	
            $editform = new Admin_Form_MemberFeeEdit();
    		$this->view->form = $editform;
		if($this->getRequest()->isPost())

		{

			$member_form = $this->getRequest()->getPost();

                
			if($editform->isValid($member_form))
		{


					$id = $this->getRequest()->getparam('Id');

					$dropdown_fee = $editform->getValue('id_fees'); 

                    $dropdown_currency = $editform->getValue('id_currency'); 

                    $dropdown_member = $editform->getValue('id_member');
                    
 					
                    

                    $sub_type = $editform->getValue('id_member_subtype');
					$nationality = 	$editform->getValue('nationality');
                    $amount = $editform->getValue('amount');
                    $active = $editform->getValue('active');
                    $gst    = $editform->getValue('gst');
                
                    $calculated_amount = $editform->getValue('calculated_amount');
                    $renewal_year = $editform->getValue('renewal_year');
                    
             $degree = $editform->getValue('id_degree');
              $date = $editform->getValue('effective_date');
            
			$edit_member = new Admin_Model_DbTable_MemberRenewFee();
				$edit_member->editmember($id,$dropdown_fee,$dropdown_currency,$dropdown_member,$sub_type,$amount,$nationality,$degree,$date,$active,$gst,$calculated_amount,$renewal_year);

                    if($edit_member)
                    {
                         $this->_redirect( $this->baseUrl . '/admin/MemberRenewFee/index');
                    }

                  //Line 5
//$this->_helper->redirector('index');
			}

		else
		{
    		 $editform->populate($member_form);
		}


		}
		 else
		              {
                        $id = $this->getRequest()->getparam('Id');
                        	
                         $file = new Admin_Model_DbTable_MemberRenewFee();
                         $files = $file->fetchRow('id='.$id);
                         $editform->populate($files->toArray());
                        
                     }    
$this->view->form = $editform;  	  	
}



public function getsubtypeAction()
{
	$this->_helper->layout->disableLayout();

	$memberId = $this->_getParam('member_id',null);


	$query =  new Admin_Model_DbTable_MemberRenewFee();

	$memberdata = $query->getmemberdata($memberId);
	$json = Zend_Json::encode($memberdata);

	
				
                    print_r($json);
                   // die();

	 exit();
}


}