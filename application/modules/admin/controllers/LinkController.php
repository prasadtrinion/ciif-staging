<?php
class Admin_LinkController extends  Zend_Controller_Action
{
	


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'Programmecategory');

		$this->dbMenu = new App_Model_Link();
		
		     $this->userDb = new App_Model_User();


	}

	public function indexAction()
	{

		$form = new App_Form_Roleregister();
		
        $userLists = $this->dbMenu->fetchAll();

        $formdata = array( );
       // echo "<pre>";
       // print_r($userLists);
       // die();
       
        
		if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            
            
			
    }


		

		$this->view->users = $userLists;
		$this->view->formdata = $formdata;
$this->view->form = $form;
	}

	public function addAction()
	{
		$form = new App_Form_Roleregister();


		 $dbMenu = new App_Model_Link;
		 $auth = Zend_Auth::getInstance();
		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				   $data = array(	'name'           =>$formData['name'],
                                    'module'         => '/'.$formData['module'],
                                    'controller'     => '/'.$formData['controller'],
                                    'action'         => '/'.$formData['action'],
                                    'created_date' 	 => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                    'created_by' 	 => $auth->getIdentity()->id,
                                    'status'         => 1,
                                   
                              );

                                  
    
                  $user_id = $dbMenu->insert($data);

                    
                   

				Cms_Common::notify('success','Role successfully created');
				$this->redirect('/admin/rolemenu/');

			
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$form = new App_Form_Roleregister();
		$id = $this->_getParam('id');

		 $cat = $this->dbMenu->fetchRow(array("id = ?" => $id))->toArray();




		//populate
		$form->populate($cat);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				$salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
                   

                    $data = array(
                                   'name'           => $formData['name'],
                                    'module'         => $formData['module'],
                                    'controller'     => $formData['controller'],
                                    'action'         => $formData['action'],
                                   
                              );

				$this->dbMenu->update($data, array('id = ?' => $id));

				Cms_Common::notify('success','Role category successfully edited');
				$this->redirect('/admin/rolemenu');

			
		}

		$this->view->form = $form;
		$this->view->cat = $cat;
	}

	public function rolemenulistAction()
	{

		$form = new App_Form_Roleregister();
		
        $userLists = $this->dbMenu->get_role_menu();
        
		$this->view->users = $userLists;
		
$this->view->form = $form;
	}

}

