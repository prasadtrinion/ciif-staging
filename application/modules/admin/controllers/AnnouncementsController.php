<?php
set_time_limit(0);
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);

class Admin_AnnouncementsController extends  Zend_Controller_Action
{
	protected $coursesDb;
	protected $contentBlockDb;
	protected $coursecatDb;
	protected $uploadDir;
	protected $uploadFolder;

	public function init()
	{
		$this->auth = Zend_Auth::getInstance();

		//$this->_helper->layout()->setLayout('/admin');
		if ($this->auth->getIdentity()->role == 'administrator' )
			{
				$this->_helper->layout()->setLayout('/admin');
			}
		else if ( $this->auth->getIdentity()->role == 'manager' )
			{
				$this->_helper->layout()->setLayout('/manager');
			}
		else if ( $this->auth->getIdentity()->role == 'lecturer' )
			{
				$this->_helper->layout()->setLayout('/trainer');
			}

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'announcements');

		$this->coursesDb = new App_Model_Courses();
		$this->contentBlockDb = new App_Model_ContentBlock();
		$this->blockDataDb = new App_Model_ContentBlockData();
		$this->coursecatDb = new App_Model_CoursesCategory();
		$this->currDb = new App_Model_Curriculum();
		$this->announcementsDb = new App_Model_Announcements(); 

		$this->uploadFolder = 'course-assets';
		$this->uploadDir = DOCUMENT_PATH. '/'.$this->uploadFolder;

		//events
		//Cms_Filters::add('block-desc-save', array($this, 'Test'));
	}


	public function indexAction()
	{
		$this->view->title = 'Announcement List';

		$getcourses = $this->announcementsDb->getAnnouncements();

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();
			$formData['search'] = trim($formData['search']);

			if ( $formData['search'] != '')
			{
				$getcourses->where('a.title LIKE ? OR a.description LIKE ?', '%'.$formData['search'].'%');
			}

			$this->view->formdata = $formData;
		}

		$courses = Cms_Paginator::query($getcourses);


		$this->view->results = $courses;
	}

	public function newAction()
	{
		$this->view->title = 'New Announcement';

		$form = new Admin_Form_Announcements();

		//form required
		$form->title->setRequired(true);
	
		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$data = array(
					'title'      	=> $formData['title'],
					'course_id'     => $formData['course_id'],
					'description'	=> $formData['description'],						
					'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'created_by'    => $this->auth->getIdentity()->id,
				);

				$wiki_id = $this->announcementsDb->insert($data);

				// add groups
				$groupDb = new App_Model_UserGroupMapping();
				$db = $groupDb->getDefaultAdapter();
				
				foreach ($formData['group_id'] as $group) {
					$data = array(
								'map_type'	=> 'announcements',
								'group_id'	=> $group,
								'course_id' => $formData['course_id'],
								'mapped_id'	=> $wiki_id
						);

					$groupDb->addMappingData($data);
				}

				Cms_Common::notify('success','Announcement successfully created');
				$this->redirect('/admin/announcements/');		
			}
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->getParam('id');
		$course_id = $this->getParam('course');

		$course = $this->announcementsDb->fetchRow(array("id = ?" => $id))->toArray();

		if ( empty($course) )
		{
			throw new Exception('Announcement doesnt exist');
		}

		$this->view->title = 'Edit Announcement';

		$this->view->course = $course;

		//form
		$form = new Admin_Form_Announcements();

		//populate
		$form->populate($course);

		//form required
		$form->title->setRequired(true);

		$groupDb = new App_Model_UserGroupMapping();
		$db = $groupDb->getDefaultAdapter();

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
		
			$data = array(
				'title'      	=> $formData['title'],
				'course_id'     => $formData['course_id'],
				'description'	=> $formData['description'],
				'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
				'visibility'	=> $formData['visibility'],
				'modified_by'   => $this->auth->getIdentity()->id,
			);
			
			$this->announcementsDb->update($data, array('id = ?'=> $id));

			// for groups
			$groupDb->deleteMappingByDataId($id, $formData['course_id'], 'announcements');

			foreach ($formData['group_id'] as $group) {
				$data = array(
							'map_type'	=> 'announcements',
							'group_id'	=> $group,
							'course_id' => $formData['course_id'],
							'mapped_id'	=> $id
					);

				$groupDb->addMappingData($data);


			}

			//redirect
			Cms_Common::notify('success','Announcement successfully updated');
			$this->redirect('/admin/announcements/edit/id/'.$id . '/course/' . $course_id);
			
		}

		// get this content grouping
		$groupdata = $db->select()
							->from(array('a' => 'user_group'))
							->joinLeft(array(
									'c' => 'user_group_mapping'), 
									'(a.group_id = c.group_id AND c.map_type = "announcements" AND c.mapped_id = "'.$id.'")', 
									array('c.map_id'))
							->where('a.course_id = ?', $course_id)
							->group('a.group_id')
							->order('a.group_name');

		$groups = $db->fetchAll($groupdata);

		$this->view->announcements_id = $id;
		$this->view->groups = $groups;
		$this->view->form = $form;
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$announcements = $this->announcementsDb->fetchRow(array("id = ?" => $id))->toArray();

		if (empty($announcements)) {
			throw new Exception('Announcement doesnt exist');
		}

		//delete
		$this->announcementsDb->delete(array('id = ?' => $id));

		// for groups
		$groupDb = new App_Model_UserGroupMapping();
		$db = $groupDb->getDefaultAdapter();
		
		$groupDb->deleteMappingByDataId($id, $announcements['course_id'], 'announcements');

		Cms_Common::notify('success','Announcements successfully deleted');
		$this->redirect('/admin/announcements/');
	}

	public function getAnnouncementsGroupAction() 
	{
		if ($this->getRequest()->isPost()) 
		{
			$formData = $this->getRequest()->getPost();

			// get groups for this course
			$groupDb = new App_Model_UserGroup();
			$db = $groupDb->getDefaultAdapter();

			$groupdata = $db->select()
								->from(array('a' => 'user_group'))
								->joinLeft(array(
									'c' => 'user_group_mapping'), 
									'(a.group_id = c.group_id AND c.map_type = "announcements" AND c.mapped_id = "' . @$formData['announcements_id'] . '")', 
									array('c.map_id'))
								->where('a.course_id = ?', $formData['course_id'])
								->group('a.group_id')
								->order('a.group_name');

			$groups = $db->fetchAll($groupdata);

			die(json_encode($groups));
		}
	}
}

