<?php
class Admin_CurriculumController extends  Zend_Controller_Action
{
	protected $currDb;


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'courses');

		$this->currDb = new App_Model_Curriculum();
		$this->courseDb = new App_Model_Courses();
		$this->curcorDb = new App_Model_CurriculumCourses();
		$this->coursecatDb = new App_Model_CoursesCategory();

	}

	public function indexAction()
	{
		$this->view->title = 'Curriculum';

		//$results = $this->currDb->fetchAll()->toArray();
		$currs = $this->currDb->fetchAll()->toArray();
		$results = $this->currDb->printTree($this->currDb->buildTree($currs,0),0,0,false) ;

		$this->view->results = $results;

	}

	public function addAction()
	{
		$form = new Admin_Form_Curriculum();

		$pid = $this->_getParam('pid');

		$this->view->title = "New Curriculum";

		//pid
		if ( !empty($pid) )
		{
			$form->parent_id->setValue($pid);
		}

		$curr = $this->currDb->fetchAll()->toArray();
		foreach ( $this->currDb->printTree($this->currDb->buildTree($curr,0),0,0,true,1) as $opt )
		{
			$form->parent_id->addMultiOption($opt['id'], $opt['name']);
		}

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$fail = 0;

				$accessperiod =  $formData['accessperiod_opt'] == '1' ? $formData['accessperiod_num'].$formData['accessperiod_type'] : null;

				$data = array(
						'name'      	=> $formData['name'],
						'parent_id'		=> $formData['parent_id'],
						'display_only'	=> intval($formData['display_only']),
						'code'			=> $formData['code'],
						'description'	=> $formData['description'],
						'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'created_by'    => $this->auth->getIdentity()->id,
						'visibility'	=> $formData['visibility'],
						'excerpt'		=> $formData['excerpt'],
						'accessperiod' 	=> $accessperiod,
						'allow_course_registration' => $formData['allow_course_registration'],
						'mode'          => $formData['mode']
					);

				$course_id = $this->currDb->insert($data);

				Cms_Common::notify('success','Course curriculum successfully created');
				$this->redirect('/admin/curriculum/');

			}
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$id = $this->_getParam('id');

		$this->view->title = 'Curriculum';

		$curriculum = $this->currDb->getCurriculum( array('id = ?' => $id));

		if (empty($curriculum)) {
			throw new Exception('Invalid Curriculum');
		}

		$form = new Admin_Form_Curriculum();

		$form->populate( $curriculum );

		$curr = $this->currDb->fetchAll()->toArray();


		foreach ( $this->currDb->printTree($this->currDb->buildTree($curr,0),0,0,true,1) as $opt )
		{
			$form->parent_id->addMultiOption($opt['id'], $opt['name']);
		}

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if ($form->isValid($formData))
			{
				$fail = 0;

				$accessperiod =  $formData['accessperiod_opt'] == '1' ? $formData['accessperiod_num'].$formData['accessperiod_type'] : null;

				$data = array(
					'name'      	=> $formData['name'],
					'parent_id'		=> $formData['parent_id'],
					'display_only'	=> intval($formData['display_only']),
					'code'			=> $formData['code'],
					'description'	=> $formData['description'],
					'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'created_by'    => $this->auth->getIdentity()->id,
					'visibility'	=> $formData['visibility'],
					'accessperiod' 	=> $accessperiod,
					'excerpt'		=> $formData['excerpt'],
					'allow_course_registration' => $formData['allow_course_registration'],
					'mode'          => $formData['mode']
				);

				$this->currDb->update($data, array('id = ? ' => $id));

				Cms_Common::notify('success','Course curriculum successfully updated.');
				$this->redirect('/admin/curriculum/edit/id/'.$id);

			}
		}

		$this->view->curr = $curriculum;
		$this->view->form = $form;
	}

	public function viewAction()
	{
		$id = $this->_getParam('id');

		$this->view->title = 'Curriculum';

		$curriculum = $this->currDb->fetchRow(array("id = ?" => $id))->toArray();

		if (empty($curriculum)) {
			throw new Exception('Invalid Curriculum');
		}

		//categories
		$cats = $this->coursecatDb->fetchAll()->toArray();


		//courses
		$courses = $this->curcorDb->getData($id);

		$coursesByCat = array();
		foreach ( $courses as $row )
		{
			$coursesByCat[ $row['category_id'] ][] = $row;
		}


		$this->view->categories = $cats;
		$this->view->coursesByCat = $coursesByCat;
		$this->view->courses = $courses;
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$id = $this->_getParam('id');

		$curriculum = $this->currDb->fetchRow(array("id = ?" => $id))->toArray();

		if (empty($curriculum)) {
			throw new Exception('Invalid Curriculum');
		}

		//delete child
		$this->curcorDb->delete( array('curriculum_id = ?' => $id) );

		//delete
		$this->currDb->delete(array('id = ?' => $id));

		Cms_Common::notify('success','Curriculum successfully deleted');
		$this->redirect('/admin/curriculum/');
	}


}

