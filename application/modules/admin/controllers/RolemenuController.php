<?php
class Admin_RolemenuController extends  Zend_Controller_Action
{
	


	public function init()
	{
		$this->_helper->layout()->setLayout('/admin');

		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();

		Zend_Layout::getMvcInstance()->assign('nav', 'Programmecategory');

		$this->dbMenu = new App_Model_Menu();
		
		     $this->userDb = new App_Model_User();


	}

	public function indexAction()
	{

		$form = new App_Form_Roleregister();
		
        $userLists = $this->dbMenu->get_role();

        $formdata = array( );
       // echo "<pre>";
       // print_r($userLists);
       // die();
       
        
		if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();

            
            if ($formdata)
				{		
					$results = $this->programDb->getProgramSearch($formdata);
					$this->view->users = $results;
				}
			
    }


		

		$this->view->users = $userLists;
		$this->view->formdata = $formdata;
$this->view->form = $form;
	}

	public function addAction()
	{
		$form = new App_Form_Roleregister();


		 $dbMenu = new App_Model_Menu;
		$id = $this->_getParam('id');

		 $auth = Zend_Auth::getInstance();
   
        
 //$user = $this->userDb->fetchRow(array('id = ?' => $id));

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			// echo "<pre>";
			// print_r($formData);
			// die();
			
				$salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
                   

                    $data = array(	'role_id'        => $id,
                                    'user_id'        => $id,
                                    'menu'           => $formData['menu'],
                                    'description'    =>	$formData['description'],
                                    'created_date' 	 => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                    'created_by' 	 => $auth->getIdentity()->id,
                                    'status'         => 1,
                                   
                              );

                                  
    
                  $user_id = $dbMenu->insert($data);

                    
                   

				Cms_Common::notify('success','Role successfully created');
				$this->redirect('/admin/rolemenu/rolemenulist/');

			
		}

		$this->view->form = $form;
	}

	public function editAction()
	{
		$form = new App_Form_Roleregister();
		$id = $this->_getParam('id');
		

		 $cat = $this->dbMenu->fetchRow(array("id = ?" => $id))->toArray();

		
	
		//populate
		$form->populate($cat);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

				$salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
                    $data = array(
                                    'menu'           => $formData['menu'],
                                    'description'    =>	$formData['description'],
                                    
                                   
                              );

				$this->dbMenu->update($data, array('id = ?' => $id));

				Cms_Common::notify('success','Role category successfully edited');
				$this->redirect('/admin/rolemenu');

			
		}

		$this->view->form = $form;
		$this->view->cat = $cat;
	}

	public function rolemenulistAction()
	{

		$form = new App_Form_Roleregister();
		
        $userLists = $this->dbMenu->get_role_menu();
        
		$this->view->users = $userLists;
		
$this->view->form = $form;
	}

}

