<?php
class Admin_EmailtemplateController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
		// $this->gobjsessionsis = Zend_Registry::get('sis'); //initialize session variable
		// $lobjinitialconfigModel = new GeneralSetup_Model_DbTable_Initialconfiguration(); //user model object
		// $larrInitialSettings = $lobjinitialconfigModel->fnGetInitialConfigDetails($this->gobjsessionsis->idUniversity);
		// $this->gintPageCount = isset($larrInitialSettings['noofrowsingrid'])?$larrInitialSettings['noofrowsingrid']:"5";
		// $this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object	  
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_Search(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view	
		$lobjemailTemplateModel = new Admin_Model_DbTable_Emailtemplate();  // email template model object
		$larrresult = $lobjemailTemplateModel->fnGetTemplateDetails(); // get template details	
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();						
				if ($lobjsearchform->isValid($larrformData)) {	
					$larrresult = $lobjemailTemplateModel->fnSearchTemplate($lobjsearchform->getValues());										
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
			$this->_redirect( $this->baseUrl . '/admin/emailtemplate/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function newtemplateAction() { 	

 		$lobjemailTemplateForm = new Admin_Form_Emailtemplate();  // email template form
		$this->view->form = $lobjemailTemplateForm;	
		$auth = Zend_Auth::getInstance();
		//$this->view->form->UpdUser->setValue ( $auth->getIdentity()->iduser);		
		
    	$lobjDefinitionTypeModel = new App_Model_Definitiontype();	// model object	
		$larrDropdownValues = $lobjDefinitionTypeModel->fnGetDefinationMs('Email Template Module Name');  // get all 'Room Type' Definitions from DMS Table 
				      $this->view->form->idDefinition->addMultiOptions($larrDropdownValues);  		       						 
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) { // save opeartion
			$larrformData = $this->_request->getPost();		
					// echo "<pre>";
					// print_r($larrformData);
					// die();	
			if ($lobjemailTemplateForm->isValid($larrformData)) {		
				$editorData = $larrformData['TemplateBody'];		
			
					// echo "<pre>";
					// print_r($larrformData);
					// die();	

				$lobjemailTemplateModel = new Admin_Model_DbTable_Emailtemplate();	// email template row 								   
			    $lintresult = $lobjemailTemplateModel->fnAddEmailTemplate($lobjemailTemplateForm->getValues(),$editorData); // add method		

			    $this->_redirect($this->view->url(array('module'=>'admin' ,'controller'=>'emailtemplate', 'action'=>'index'),'default',true));
			    $this->_redirect( $this->baseUrl . '/admin/emailtemplate/index');        
			 
			}			
		}
	}

	public function edittemplateAction() {		
 		
 	 	$idTemplate = $this->_getParam('id');


 		$lobjemailTemplateForm = new Admin_Form_Emailtemplate(); // email template form
		$this->view->form = $lobjemailTemplateForm;


		
    	$lobjDefinitionTypeModel = new App_Model_Definitiontype();	// model object	
		$larrDropdownValues = $lobjDefinitionTypeModel->fnGetDefinationMs('Email Template Module Name');  // get all 'Room Type' Definitions from DMS Table 
			  $this->view->form->idDefinition->addMultiOptions($larrDropdownValues);  		     
				//send the lobjemailTemplateForm object to the view		
        if ($this->_request->isPost() && $this->_request->getPost('Save')) {  // update operation	
            $larrformData = $this->_request->getPost();   
            unset($larrformData['Save']);            				
            if ($lobjemailTemplateForm->isValid($larrformData)) {

            		// echo "<pre>";
            		// print_r($larrformData);
            		// die();
            		//$idTemplate = $larrformData['idTemplate'];
                	$where = 'idTemplate = '. $idTemplate;	
                					           
				 	$lobjemailTemplateModel = new Admin_Model_DbTable_Emailtemplate();	
				 	$editorData = $larrformData['TemplateBody'];							 	
                    $lobjemailTemplateModel->fnUpdateTemplate($where,$larrformData,$editorData); // update email template details		 	
                   
                   
                   $this->_redirect( $this->baseUrl . '/admin/emailtemplate/index'); 
				} 			
        } else {
        		// 	get external panel row values by id and populate
				$lobjemailTemplateModel = new Admin_Model_DbTable_Emailtemplate(); //email template model object
		        $larrresult = $lobjemailTemplateModel->fnViewTemplte($idTemplate);
		        
		       	$this->view->TemplateBody = $larrresult['TemplateBody'];

		        
				$lobjemailTemplateForm->populate($larrresult); // populate all the values to the form according to id			
  		}  	
  	}

  	public function uploadfileAction(){

  		 $this->_helper->layout->disableLayout();
  		 
  		 if (file_exists(UPLOAD_PATH. $_FILES["upload"]["name"]))
{
	$result['satus'] = 402;
	$result['url'] = "already exists";
	$result['uploaded'] =1;
	$result['fileName'] = "asdf.png";
    echo  json_encode($result);
     // array(
     //            'uploaded'  => '1',
     //            'fileName'  => $image->getName(),
     //            'url'       => $image->getWebPath()
     //        )

}
else
{
 move_uploaded_file($_FILES["upload"]["tmp_name"], UPLOAD_PATH. $_FILES["upload"]["name"]);
$result['satus'] = 402;
	$result['url'] = APP_URL.'/'.$_FILES["upload"]["name"];;
  $result['uploaded'] =1;
	$result['fileName'] = $_FILES["upload"]["name"];
    echo  json_encode($result);
// echo "Stored in: " . "images/" . $_FILES["upload"]["name"];
}
exit;
  	}	  	
}