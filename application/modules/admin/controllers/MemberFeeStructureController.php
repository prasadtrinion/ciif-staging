<?php
class Admin_MemberFeeStructureController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	private $_gobjlog;
	
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/admin');
		
	}

	public function indexAction() {			
 		$lobjsearchform = new Admin_Form_MemberFeeSearch(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view	
		$lobjemailTemplateModel = new Admin_Model_DbTable_MemberFee();  // email template model object
		$larrresult = $lobjemailTemplateModel->getsearch(); // get template details	
		
		  if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->emailtemplatepaginatorresult);
					
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();						
				if ($lobjsearchform->isValid($larrformData)) {
				$post_data=$this->getRequest()->getPost();
           			
           				 $id=$post_data['field2'];	
					$larrresult = $lobjemailTemplateModel->search($id);	
					if(empty($larrresult))
					{
						$this->_redirect( $this->baseUrl . '/admin/MemberFeeStructure/index');
					}
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		//$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/generalsetup/emailtemplate');
			$this->_redirect( $this->baseUrl . '/admin/MemberFeeStructure/index');
		
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function addAction() { 	



  			$member_form = new Admin_Form_MemberFee();
            $this->view->form = $member_form;

            if($this->getRequest()->isPost())
            {    
        

                $formdata =$this->getRequest()->getPost();

                
                if($member_form->isValid($formdata))
                {

                  
                    $dropdown_fee = $member_form->getValue('dropdown_fee'); 
                    $dropdown_currency = $member_form->getValue('dropdown_currency'); 


                    
                    $dropdown_member = $member_form->getValue('dropdown_member');
                    
 

                    $sub_type = $member_form->getValue('Sub_type');
                    $amount = $member_form->getValue('amount');
                    $nationality = $member_form->getValue('ddl_nationality');
                     $active = $member_form->getValue('active');
                    $gst    = $member_form->getValue('gst');
                    $calculate_amount = $member_form->getValue('calculated_amount');

                     

                    $degree = $member_form->getValue('dropdown_degree');
                    $date = date('Y-m-d', strtotime($member_form->getValue('effective_date')));

                      
                    $add_member = new Admin_Model_DbTable_MemberFee();
                  
                    $add_member->addData($dropdown_fee,$dropdown_currency,$sub_type,$dropdown_member,$amount,$nationality,$degree,$date,$active,$gst,$calculated_amount);
                    if($add_member)
                    {
                        $this->_redirect( $this->baseUrl . '/admin/MemberFeeStructure/index');
                    }
                    
                }


            }
	}

	public function editAction() {		
 		
 	 	
            $editform = new Admin_Form_MemberFeeEdit();
    		$this->view->form = $editform;
		if($this->getRequest()->isPost())

		{

			$member_form = $this->getRequest()->getPost();
			if($editform->isValid($member_form))
		{

					$id = $this->getRequest()->getparam('Id');

					$dropdown_fee = $editform->getValue('id_fees'); 

                    $dropdown_currency = $editform->getValue('id_currency'); 

                    $dropdown_member = $editform->getValue('id_member');
                    
 					
                    

                    $sub_type = $editform->getValue('id_member_subtype');

                    	
                    $amount = $editform->getValue('amount');
                    
                 	$degree = $editform->getValue('id_degree');
                 	$date = date('Y-m-d', strtotime($editform->getValue('effective_date')));
                 	$active = $editform->getValue('active');
                    $gst    = $editform->getValue('gst');
                
                    $calculated_amount = $editform->getValue('calculated_amount');
				$edit_member = new Admin_Model_DbTable_MemberFee();
				$edit_member->editmember($id,$dropdown_fee,$dropdown_currency,$sub_type,$dropdown_member,$amount,$degree,$date,$active,$gst,$calculated_amount);

                    if($edit_member)
                    {
                         $this->_redirect( $this->baseUrl . '/admin/MemberFeeStructure/index');
                    }

                  //Line 5
//$this->_helper->redirector('index');
			}

		else
		{
    		 $editform->populate($formData);
		}


		}
		 else
		              {
                        $id = $this->getRequest()->getparam('Id');
                        	
                         $file = new Admin_Model_DbTable_MemberFee();
                         $files = $file->fetchRow('id='.$id);
                         // print_r($files['effective_date']);
                         // exit;
                         $files['effective_date'] = date('d-m-Y',strtotime($files['effective_date']));
                         $editform->populate($files->toArray());

                        
                     }    
                     
$this->view->form = $editform;  	  	
}



public function getsubtypeAction()
{

	//die('getsubtypeAction');
	
	$this->_helper->layout->disableLayout();

 	 $memberId = $this->_getParam('member_id',null);
 	//die();
	$query =  new Admin_Model_DbTable_MemberGet();

	$memberdata = $query->getmemberdata($memberId);
	$json = Zend_Json::encode($memberdata);

	
				
                    print_r($json);
                   // die();

	 exit();
}

public function getfeeAction()
{

	//die('getsubtypeAction');
	
	$this->_helper->layout->disableLayout();

 	 $memberId = $this->_getParam('member_id',null);
 	 $sub = $this->_getParam('sub',null);
 	 $year = $this->_getParam('year',null);
 	//die();
	$query =  new Admin_Model_DbTable_MemberRenewFee();

	$memberdata = $query->getrenewalfees($id,$sub,$year);
	$json = Zend_Json::encode($memberdata);

	
				
                    print_r($json);
                   // die();

	 exit();
}


}