<?php
class Admin_Model_DbTable_ApplicationFee extends Zend_Db_Table {
	
	protected $_name = 'tbl_application_fee';
	protected $_PRIMARY = 'id_fee';

	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}
	
	public function getmembertype()
	{
		$db = $this->getDefaultAdapter();

		$select = $db->select()
			->from(array($this->_name));

		$result = $db->fetchAll( $select );

		// print_r($result);
		// die();

		return $result;
	}

	public function get_fee_amount($id)
	{
		$db = $this->getDefaultAdapter();

		$select = $db->select()
			->from(array($this->_name))
			->where('amount_type = ?',$id);

		$result = $db->fetchRow( $select );

		
		return $result;

	}  
	 public function getListingData($table ,$where="",$order="", $id=0)
    {
        // $db     = getDB();
        $select = $this->select()
            ->from(array('a' => $table));
            

        if($where !=""){
            $select ->where($where);
        }
        if($order !=""){
            $select ->order($order);
        }

        $result = $this->fetchAll($select);
        return $result;
    }

	  public function addData($member_name, $sub_type,$amount,$amount_type)
    {
    		

    		$data = array (	
    						'name'=>$member_name,
    						'description' => $sub_type,
    						'amount' => $amount,
    						'amount_type' => $amount_type
    						
    						);

    		$this->insert($data);
    }
    public function fnGetTemplateDetails() {   			    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->join(array('a' => 'tbl_application_fee'))
				 ->join(array("c" => "tbl_definationms"), 'a.amount_type = c.idDefinition',array("amount_type" => "c.DefinitionDesc"));			
				
			$result = $this->fetchAll($select);

			return $result->toArray(); 		    
   	}

   	 public function local() {   	

   	 		$val = '1365';		    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->join(array('a' => 'tbl_application_fee'),array('id_fee'))
				 ->join(array("c" => "tbl_definationms"), 'a.amount_type = c.idDefinition',array("amount_type" => "c.DefinitionDesc"))
				 ->where('a.amount_type like "%" ? "%"',$val);
				 		
				
			$result = $this->fetchAll($select);

			return $result->toArray(); 		    
   	}

   	public function international() {   	

   	 		$val = '1380';		    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->join(array('a' => 'tbl_application_fee'),array('id_fee'))
				 ->join(array("c" => "tbl_definationms"), 'a.amount_type = c.idDefinition',array("amount_type" => "c.DefinitionDesc"))
				 ->where('a.amount_type like "%" ? "%"',$val);
				 		
				
			$result = $this->fetchAll($select);

			return $result->toArray(); 		    
   	}

   	/*
   	 * search by criteria   
   	 */
	public function fnSearchTemplate($post = array()) {		
	
		
		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
							
							->from(array('a' => 'tbl_application_fee'))														
							->where('a.name like "%" ? "%"',$post['field2']);
							
													
			$result = $db->fetchAll($select);
				
			return $result;			
	}
	
	
	
     
     public function editmember($id,$memberName,$subType,$amount,$amount_type)

    {
        $data=array( 
                    'name'=>$memberName,
                      'description'=>$subType,
                     'amount' => $amount,
    				'amount_type' => $amount_type
    						
                           
                          );  
     //    print_r($data);
           // echo $id;
           //   die();
      $this->update($data,'id_fee = '.$id);  
    }
   


} 