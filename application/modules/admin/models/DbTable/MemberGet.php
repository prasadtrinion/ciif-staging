<?php
class Admin_Model_DbTable_MemberGet extends Zend_Db_Table {
	
	protected $_name = 'tbl_member_subtype';
	protected $_PRIMARY = 'id_member'; // table name
	
	/*
	 * get all active email templates rows 
	 */
  

  public function getmemberdata($memberid)
  {
    $db = $this->getDefaultAdapter();

        $select = $this->select()->where('id_member = ?', $memberid);

        return $db->fetchAll($select);
  }


   
    public function addData($member_name, $sub_type)
    {
    		

    		$data = array (	
    						'member_name'=>$member_name,
    						'sub_type' => $sub_type,

    						
    						
    						);
    		// print_r($data);
    		// die('data from model');

    		$this->insert($data);
    }

    public function getListingData($table ,$where="",$order="", $id=0)
    {
        // $db     = getDB();
        $select = $this->select()
            ->from(array('a' => $table))
            ->where('1');
            

        if($where !=""){
            $select ->where($where);
        }
        if($order !=""){
            $select ->order($order);
        }

        $result = $this->fetchAll($select);
        return $result;
    }

    public function fnGetTemplateDetails() {   			    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->join(array('a' => 'member'),array('idmember'));			
				
			$result = $this->fetchAll($select);		
			return $result->toArray(); 		    
   	}
     public function getDefination($deftypeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $deftypeid);

        $result = $db->fetchAll($select);
        return $result;
    }

   	/*
   	 * search by criteria   
   	 */
	public function fnSearchTemplate($post = array()) {		
		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $this->select()
							->setIntegrityCheck(false)  	
							->join(array('a' => 'member'),array('idTemplate', 'member_name','sub_type'))														
							->where('a.member_name like "%" ? "%"',$post['field3'])
							->where('a.sub_type like "%" ? "%"',$post['field2']);
													
			$result = $this->fetchAll($select);
			return $result->toArray();			
	}
	
	/*
	 * Add Email Template
	 */
	public function fnAddEmailTemplate($post,$editorData) {
		
		$post['TemplateBody'] = $editorData;			
		$this->insert($post);
	}
	
	/*
	 * get single email template row values bu $id
	 */
    public function fnViewTemplte($idmember) {
    			
		$result = $this->fetchRow( "idmember = '$idmember'") ;
        return @$result->toArray();     // @symbol is used to avoid warning    	
    }
     
     public function editmember($id,$memberName,$subType)

    {
        $data=array( 
                    'member_name'=>$memberName,
                      'sub_type'=>$subType,
                        
                           
                          );  
     //    print_r($data);
           // echo $id;
           //   die();
      $this->update($data,'idmember = '.$id);  
    }
    /*
     * update email template 
     */
    public function fnUpdateTemplate($whereId,$formData,$editorData) {
    		unset($formData['idDefination']);
    			    				
			$this->update($formData,$whereId);
    }
    	
}
