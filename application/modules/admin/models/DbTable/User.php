<?php
class Admin_Model_DbTable_User extends Zend_Db_Table {
	
	protected $_name = 'user';


	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}
	
	public function getDetails($data)
	{

		$db = $this->db;
		 $Select = $db->select()
		->from(array('u'=>'user'))
		->joinLeft(array('p'=>'member'),'p.idmember = u.id_member')
		->where('u.approve_status1=?','1')
		->where('u.approve_status=?','0')
		
		->group('u.id');


		if($data['program']!='')
		{
		  $Select->where('p.idmember =?',$data['program']);
		}
		$Result = $db->fetchAll($Select);

		return $Result;
	    
	}

	public function getUser()
	{
		
		$db = $this->db;
		 $Select = $db->select()
		->from(array('u'=>'user'));
		$Result = $db->fetchAll($Select);

		return $Result;
	    
	}


	public function getmember()
	{
		
		$db = $this->db;
		 $Select = $db->select()
		->from(array('m'=>'member'));
		$Result = $db->fetchAll($Select);

		return $Result;
	    
	}

	public function getViewDetails($data)
	{
		
		 $db = $this->db;
		 $Select = $db->select()
		->from(array('u'=>'user'))
		->joinLeft(array('uc'=>'tbl_user_company'),'
			uc.user_id=u.id',array('Company Name' =>'uc.u_company','Company Designation' =>'uc.u_designation','fromDate' =>'uc.c_fromdate','toDate' =>'uc.c_todate'))
		->joinLeft(array('uq'=>'tbl_user_qualification'),'
			uq.user_id=u.id',array('Quali' =>'uq.qualification','Qyear' =>'uq.q_year','QUniversity' =>'uq.q_university'))
		->joinLeft(array('tm'=>'member'),'
			tm.idmember=u.id_member',array('program level' =>'tm.member_name'))
		->joinLeft(array('g'=>'tbl_definationms'),'
			g.idDefinition= u.gender',array('Gender'=>'g.DefinitionDesc'))
		->joinLeft(array('q'=>'tbl_definationms'),'
			q.idDefinition= u.industrial_exp',array('rationale'=>'q.DefinitionDesc'))
		->joinLeft(array('n'=>'tbl_countries'),
			'n.idCountry= u.country',array('nationality'=>'n.CountryName'))
		->joinLeft(array('cou'=>'country'),'
			cou.id= u.country',array('residency'=>'cou.name'))
		->where('u.approve_status1=?','1')
		->where('u.approve_status=?','0')
		->group('uq.user_id'); 
		
		
		
		if($data!='')
		{
		  $Select->where('tm.idmember =?',$data);
		}
		
		$Result = $db->fetchAll($Select);
		return $Result;
	    
	}	

}
?> 