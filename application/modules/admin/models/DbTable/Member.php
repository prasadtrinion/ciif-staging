<?php
class Admin_Model_DbTable_Member extends Zend_Db_Table {
	
	protected $_name = 'member';
	protected $_PRIMARY = 'idmember';

	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}

	 public function getMemberData($name){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
          
            ->where('member_name like ?',"%".$name."%");
           
           $result = $db->fetchRow($select); 
         
            return $result;
 
    }


	public function getmembertype()
	{
		$db = $this->getDefaultAdapter();

		$select = $db->select()
			->from(array($this->_name));

		$result = $db->fetchAll( $select );

		// print_r($result);
		// die();

		return $result;
	}


	 public function getListingData($table ,$where="",$order="", $id=0)
    {
        // $db     = getDB();
        $select = $this->select()
            ->from(array('a' => $table));
            

        if($where !=""){
            $select ->where($where);
        }
        if($order !=""){
            $select ->order($order);
        }

        $result = $this->fetchAll($select);
        return $result;
    }

	  public function addData($member_name, $sub_type)
    {
    		

    		$data = array (	
    						'member_name'=>$member_name,
    						'sub_type' => $sub_type,

    						
    						
    						);
    		$this->insert($data);
    }
    public function fnGetTemplateDetails() {   			    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->join(array('a' => 'member'),array('idmember'));			
				
			$result = $this->fetchAll($select);		
			return $result->toArray(); 		    
   	}

   	/*
   	 * search by criteria   
   	 */
	public function fnSearchTemplate($post = array()) {		
		
		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
							
							->join(array('a' => 'member'),array('idTemplate', 'member_name','sub_type'))														
							->where('a.member_name like "%" ? "%"',$post['field2']);
							
													
			$result = $db->fetchAll($select);
			return $result;			
	}
	
	/*
	 * Add Email Template
	 */
	public function fnAddEmailTemplate($post,$editorData) {
		
		$post['TemplateBody'] = $editorData;			
		$this->insert($post);
	}
	
	/*
	 * get single email template row values bu $id
	 */
    public function fnViewTemplte($idmember) {
    			
		$result = $this->fetchRow( "idmember = '$idmember'") ;
        return @$result->toArray();     // @symbol is used to avoid warning    	
    }
     
     public function editmember($id,$memberName,$subType)

    {
        $data=array( 
                    'member_name'=>$memberName,
                      'sub_type'=>$subType,
                        
                           
                          );  
     //    print_r($data);
           // echo $id;
           //   die();
      $this->update($data,'idmember = '.$id);  
    }
    /*
     * update email template 
     */
    public function fnUpdateTemplate($whereId,$formData,$editorData) {
    		unset($formData['idDefination']);
    			    				
			$this->update($formData,$whereId);
    }




} 