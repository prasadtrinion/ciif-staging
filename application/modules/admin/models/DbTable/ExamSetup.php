<?php 

class Admin_Model_DbTable_ExamSetup extends Zend_Db_Table_Abstract {
	
	protected $_name = 'exam_setup';
	protected $_primary = "es_id";
	protected $_name_detail = 'exam_setup_detail';
	protected $_primary_detail = "esd_id";
	
	public function addData($data){
		$this->insert($data);
		$db = Zend_Db_Table::getDefaultAdapter();
		return $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		$this->update($data,"es_id ='".$id."'");
	}
	
	public function deleteData($add_id){
		$this->delete("es_id ='".$add_id."'");
	}	
	
	public function getExamList($idLandscape){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
            ->from(array('es'=>$this->_name))
            ->where('es.es_idLandscape = ?',$idLandscape)
            ->order('es.es_name ASC');
		
		 $row = $db->fetchAll($select);
		 return $row;
	}

    public function getExamCount($idLandscape){
        
        $auth = Zend_Auth::getInstance();
        $db   = Zend_Db_Table::getDefaultAdapter();       
        
        $select = $db ->select()
            ->from(array('es'=>$this->_name), array('count' => 'COUNT(*)'))
            ->where('es.es_idLandscape = ?',$idLandscape);
        
         $row = $db->fetchRow($select);
         return $row['count'];
    }

    public function getDataById($es_id) {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('es'=>'exam_setup'))
            ->join(array('l'   => 'tbl_landscape'),'l.IdLandscape=es.es_idLandscape',array('IdLandscape', 'IdStartSemester', 'IdStartSemester AS IdIntake'))
            ->where('es.es_id = ?', $es_id);
        return $db->fetchRow($query);
    }

    public function getExamDetail($es_id, $category_id = 0, $examschedule_id = 0, $params = array()) {
        $auth   = Zend_Auth::getInstance();
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('b' => $this->_name_detail), array('esd_id', 'esd_type', 'esd_idSubject'))
            ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array('examschedule_id' => 'es_id', 'es_id'))
            ->join(array('d' => 'tbl_markdistribution_setup'), 'd.mds_id = esd_mds_id', array('mds_id', 'mds_name'))
            ->join(array('e' => 'programme_exam'), 'e.pe_id = b.esd_pe_id AND e.pe_id = d.mds_programme_exam', array('pe_id', 'pe_name'))
            ->joinLeft(array('f' => 'tbl_subjectmaster'), 'f.IdSubject = b.esd_idSubject', array('IdSubject', 'SubjectName', 'SubCode'))
            ->join(array('g' => 'tbl_program'), 'g.IdProgram = e.IdProgram', array())
            ->joinLeft(array('h' => 'tbl_programclosingdate'), 'h.IdProgram = g.IdProgram AND h.registration_type = 1 AND h.category_id = '. $category_id, array())

            ->join(array('l' => 'exam_scheduletaggingslot'), 'l.sts_schedule_id = c.es_id', array('schedule_tagging_slot_id' => 'sts_id', 'sts_id'))
            ->join(array('m' => 'exam_slot'), 'm.sl_id = l.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->join(array('n' => 'exam_taggingslotcenterroom'), 'n.tsc_slotid = tsc_examcenterid', array('tagging_slot_center_room_id' => 'tsc_id'))
            ->join(array('o' => 'tbl_exam_center'), 'o.ec_id =  n.tsc_examcenterid', array('es_exam_center' => 'ec_id', 'ec_id', 'ec_name'))

            ->where('b.es_id = ?', $es_id)
            // ->where('c.es_date > ?', date('Y-m-d', strtotime('+14 days')))
            ->group('b.esd_id');

        if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
        }
        else {
            $select->where('CURDATE() < DATE_SUB(c.es_date, INTERVAL CONVERT(COALESCE(NULLIF(h.days, 0), '.DEFAULT_EXAM_CLOSING_DATE.'), SIGNED INTEGER) DAY)');
        }

        if($examschedule_id) {
            $select->where('c.es_id = ?', $examschedule_id);   
        }
        
        $result = $db->fetchAll($select);

        $icampus_examregistration = new icampus_Function_Examination_ExamRegistration();

        foreach($result as $index => $row) {
            /*$select = $db->select()
                ->from(array('b' => $this->_name_detail), array())
                ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array('examschedule_id' => 'es_id', 'es_id', 'es_esd_id'))
                ->join(array('f' => 'tbl_exam_center'), 'f.ec_id = c.es_exam_center', array('ec_id', 'ec_name'))
                ->join(array('a' => $this->_name), 'a.es_id = b.es_id', array())
                ->join(array('g' => 'tbl_program'), 'g.IdProgram = a.es_idProgram', array())
                ->joinLeft(array('h' => 'tbl_programclosingdate'), 'h.IdProgram = g.IdProgram AND h.registration_type = 1 AND h.category_id = '. $category_id, array())
                ->where('b.es_id = ?', $es_id)
                ->where('b.esd_id = ?', $row['esd_id'])
                // ->where('c.es_date > ?', date('Y-m-d', strtotime('+14 days')))
                ->group('f.ec_id');

            if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
                $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
            }
            else {
                $select->where('CURDATE() < DATE_SUB(c.es_date, INTERVAL CONVERT(COALESCE(NULLIF(h.days, 0), '.DEFAULT_EXAM_CLOSING_DATE.'), SIGNED INTEGER) DAY)');
            }*/

            // $exam_centers = $db->fetchAll($select);
            $exam_centers = $icampus_examregistration->icampus_getExamCenterByPeId($row['pe_id'], $category_id);
            $result[$index]['exam_centers']  = $exam_centers;
            $result[$index]['esd_type_desc'] = '';
            $result[$index]['module_desc']   = '';
            
            if($row['IdSubject']) {
                $result[$index]['module_desc'] = $row['SubCode'] .' — '. $row['SubjectName'];
            }

            if($row['esd_type'] == 1) {
                $result[$index]['esd_type_desc'] = 'Programme Based';
            }
            elseif($row['esd_type'] == 2) {
                $result[$index]['esd_type_desc'] = 'Modular Based';
            }
        }

        if($examschedule_id && count($result)) {
            $result = $result[0];
        }

        return $result;
    }
	
}

?>