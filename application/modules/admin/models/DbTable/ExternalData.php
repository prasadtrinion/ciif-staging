<?php
class Admin_Model_DbTable_ExternalData extends Zend_Db_Table {
	
	protected $_name = 'external_data';

	public function getData($type='')
	{
		$db = $this->getDefaultAdapter();

		$select = $db->select()
			->from(array('a'=>$this->_name))
			->joinLeft(array('c'=>'external_datatype'),'c.id=a.type_id',array())
			->where('c.name = ?', $type)
			 ->order('a.defOrder ASC');

		$result = $db->fetchAll( $select );

		return $result;
	}

	public static function getValue($id=0, $cache=0)
	{
		if ( $id == 0 ) return false;

		$session = new Zend_Session_Namespace('datacache');

		if( isset($session->$id) && $cache == 1 )
		{
			return $session->$id;
		}
		else
		{
			$db = getDB();

			$select = $db->select()
				->from(array('a' => 'external_data'))
				->joinLeft(array('c' => 'external_datatype'), 'c.id=a.type_id', array())
				//->where('c.name = ?', $type)
				->where('a.id = ?', $id);

			$result = $db->fetchRow($select);

			if ( $cache == 1 )
			{
				//cache
				$session->$id = $result;
			}

			return $result;
		}
	}

    public function getIdByValue($type, $id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>$this->_name))
            ->join(array('c'=>'external_datatype'),'c.id=a.type_id',array())
            ->where('a.type_id = ?', $type)
            ->where('a.value = ?', $id);

        $result = $db->fetchRow( $select );

        return $result;
    }

    //get country, city, state
    public function getExternalCountry($table , $id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $table))
            ->where('a.external_id = ?',$id);

        $result = $db->fetchRow( $select );

        return $result;
    }

    // public function getListingData($table ,$where="",$order="", $id=0)
    // {
    //     $db     = getDB2();
    //     $select = $db->select()
    //         ->from(array('a' => $table))
    //         ->where('1');

    //     if($where !=""){
    //         $select ->where($where);
    //     }
    //     if($order !=""){
    //         $select ->order($order);
    //     }

    //     $result = $db->fetchAll($select);
    //     return $result;
    // }

    public function fngetAllentry( $IdProgram ) {

        $db     = getDB2();
        $sql = $db->select()
            ->from( array( 'a' => 'tbl_programentryrequirement' ), array( 'a.*' ) )
            ->joinLeft( array( 'h' => 'tbl_programentry' ), 'h.IdProgramEntry = a.IdProgramEntry  ', array( 'h.IdProgram' ) )
            ->joinLeft( array( 'e' => 'tbl_qualificationmaster' ), 'a.EntryLevel=e.IdQualification  ', array( 'e.QualificationLevel as EntryLevelname' ) )
            ->joinLeft( array( 'c' => 'tbl_definationms' ), 'a.GroupId =c.idDefinition', array( 'c.DefinitionDesc as Groupname' ) )
            ->joinLeft( array( 'b' => 'tbl_definationms' ), 'a.Item =b.idDefinition', array( 'b.DefinitionDesc as Itemname' ) )
            ->joinLeft( array( 'd' => 'tbl_definationms' ), 'a.Condition=d.idDefinition', array( 'd.DefinitionDesc as Conditionname' ) )
            ->joinLeft( array( 'g' => 'tbl_specialization' ), 'a.IdSpecialization=g.IdSpecialization', array( 'g.Specialization as SpecializationName' ) )
            //->where( 'IdProgramEntry = ?', $IdProgramEntry )
            ->where( 'h.IdProgram = ?', $IdProgram )
            ->where('RequirementType = ?', 1);//echo $sql;

        $result = $db->fetchAll( $sql );
        return $result;
    }

    public function getMembershipProgram($IdProgram = 0)
    {
        $db     = getDB2();
        $select = $db->select()
            ->from(array('a' => 'tbl_program'),array('a.ProgramName as ProgMembership','a.IdProgram as ProgMembershipId'))
            ->join( array('b' =>'tbl_program'), 'b.ProgramMembership = a.IdProgram', array( 'b.*' ) );

            if($IdProgram != 0){
                 $select->where( 'b.IdProgram = ?', $IdProgram );
            }else{
                $select->where( 1);
            }
//echo $select;
        $result = $db->fetchAll($select);
        return $result;
    }



    public function getDefination(){

        $deftypeid =191 ;
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $deftypeid);

        $result = $db->fetchAll($select);
        return $result;
    }

    


}