
<?php
class Admin_Model_DbTable_CpdActivity extends Zend_Db_Table {
	
	protected $_name = 'tbl_definationms';
	protected $_PRIMARY = 'idDefinition'; // table name
	
	/*
	 * get all active email templates rows 
	 */
  

  public function getmeberdata($memberid)
  {
    $db = $this->getDefaultAdapter();

        $select = $this->select()->where('idmember = ?', $memberid);

        return $db->fetchRow($select);
  }

public function getByCode($definition) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('Definitions'=>$this->_name) )
            ->joinLeft(array('DefinitionType' => 'tbl_definationtypems'), 'DefinitionType.idDefType = Definitions.idDefType', array('BahasaIndonesiaType' => 'DefinitionType.BahasaIndonesia'))
            ->where("DefinitionType.defTypeDesc = '".$definition."'")
            ->where("Definitions.status = ?", 1)
      ->order('Definitions.defOrder ASC');

        $row = $db->fetchAll($select);
        return $row;
    }

    public function getdefCode($definition) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('Definitions'=>$this->_name) )
            
            ->where("Definitions.DefinitionCode = '".$definition."'")
            ->where("Definitions.status = ?", 1)
      ->order('Definitions.defOrder ASC');

        $row = $db->fetchAll($select);
        return $row;
    }
   
    public function addData($definition,$description)
    {


    		$data = array (	
                'idDefType' => 202,
    						'DefinitionDesc'=>$definition,
    						'Description' => $description,
                'DefinitionCode' => $description,
                'BahasaIndonesia' => $description,
                'BhasaIndonesia' => $description,    						
    						);    	
                // print_r($data);
                // die();	

    		$this->insert($data);
    }

   
   
     public function getDefination($deftypeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $deftypeid);

        $result = $db->fetchAll($select);
        
        return $result;
    }

   	/*
   	 * search by criteria   
   	 */
	public function fnSearchTemplate($post = array()) {	

		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $this->select()
           ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
             ->where('a.idDefType = ?', 202)
					->where('DefinitionDesc like "%" ? "%"',$post['field3']);
													
			$result = $this->fetchAll($select);
			return $result->toArray();			
	}

	/*
	 * get single email template row values bu $id
	 */
    public function fnViewTemplte($idmember) {
    			
		$result = $this->fetchRow( "idmember = '$idmember'") ;
        return @$result->toArray();     // @symbol is used to avoid warning    	
    }
     
     public function editmember($id,$definition,$description)

    {
        $data=array( 
                    'DefinitionDesc'=>$definition,
                'Description' => $description,

                        
                           
                          );  
    
      $this->update($data,'idDefinition = '.$id);  
    }

}
