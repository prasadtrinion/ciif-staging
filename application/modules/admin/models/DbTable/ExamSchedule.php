<?php

class Admin_Model_DbTable_ExamSchedule extends Zend_Db_Table_Abstract
{
    protected $_name = 'exam_schedule';
    protected $_primary = "es_id";

    public function getDatabyId($id = 0, $join = true)
    {
        $id = (int)$id;

        $db = Zend_Db_Table::getDefaultAdapter();
        $DefinitiontypeDB = new Admin_Model_Definitiontype();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('g' => 'tbl_takafuloperator'), 'g.Id = a.corporate_id', array('corporate_name' => 'name'));

        if ($join) {
//            $select->join(array('b' => 'tbl_exam_center'), 'a.es_exam_center = b.ec_id', array('ec_name'));
            $select->join(array('d' => 'exam_setup_detail'), 'd.esd_id= a.es_esd_id', array('esd_id'));
            $select->join(array('c' => 'exam_setup'), 'c.es_id = d.es_id', array('examsetup_id' => 'es_id'));
            $select->join(array('e' => 'programme_exam'), 'e.pe_id = d.esd_pe_id', array('pe_name'));
            $select->join(array('f' => 'tbl_program'), 'f.IdProgram = c.es_idProgram', array('ProgramName', 'ProgramCode'));
//            $select->order('f.ProgramName ASC');
        }

        $select->order('a.es_date ASC');
//        $select->order('a.es_start_time ASC');
        $select->where('a.es_id = ' . $id);

        /* if ($join) {
             $select->order('b.ec_name ASC');
         }*/


        $row = $db->fetchRow($select);

        /*  if (!empty($row) && $join) {
              $exam_sessions = $DefinitiontypeDB->fetchDetailAward($row['es_session']);
              $row['session_DefinitionDesc'] = $exam_sessions['DefinitionDesc'];
              $row['session_BahasaIndonesia'] = $exam_sessions['BahasaIndonesia'];
          }*/
        return $row;

    }

    public function getData($id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('es' => $this->_name));

        if ($id) {
            $select->where('es.es_id = ?', $id);
            $row = $db->fetchRow($select);
        } else {
            $row = $db->fetchAll($select);
        }

        return $row;

    }

    public function addData($data)
    {
        $this->insert($data);
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->lastInsertId();
    }

    public function addDataTagging($data)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('exam_scheduletaggingslot', $data);
        return $db->lastInsertId();
    }

    public function updateData($data, $id)
    {
        $this->update($data, "es_id ='" . $id . "'");
    }

    public function deleteData($add_id)
    {
        $this->delete("es_id ='" . $add_id . "'");
    }

    public function deleteDataTagging($add_id, $sts_slot_id = 0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        if ($sts_slot_id)
        {
            $db->delete('exam_scheduletaggingslot', array(
                'sts_schedule_id = ?' => $add_id,
                'sts_slot_id = ?'     => $sts_slot_id
            ));
        }
        else
        {
            $db->delete('exam_scheduletaggingslot', "sts_schedule_id ='" . $add_id . "'");
        }
        
    }


    public function getScheduleByExamCenter($formData)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('es' => $this->_name))
            ->join(array('sm' => 'tbl_subjectmaster'), 'sm.IdSubject=es.es_course', array('IdSubject', 'subjectMainDefaultLanguage', 'SubCode', 'SubjectName'))
            ->join(array('s' => 'tbl_semestermaster'), 's.IdSemesterMaster=es.es_semester', array('IdSemesterMaster', 'SemesterMainName', 'SemesterMainDefaultLanguage'))
            ->order('es.es_date desc');


        if (isset($formData['IdSemester']) && $formData['IdSemester'] != '') {
            $select->where('es_semester = ?', $formData['IdSemester']);
        }

        if (isset($formData['IdSubject']) && $formData['IdSubject'] != '') {
            $select->where('es_course = ?', $formData['IdSubject']);
        }

        $row = $db->fetchAll($select);
        return $row;

    }

    public function getScheduleBySubject($idSemester, $idSubject, $ec_id = 0, $student_type = 740)
    {

        $db = Zend_Db_Table::getDefaultAdapter();


        $select = $db->select()
            ->from(array('es' => $this->_name))
            ->where('es.es_course = ?', $idSubject);

        if ($student_type == 741) {
            //VISITING NO NEED TO QUERY  by semester
        } else {
            $select->where('es.es_semester = ?', $idSemester);
        }

        if (isset($ec_id) && $ec_id != 0) {
            $select->where('es.es_exam_center = ?', $ec_id);
        }

        $row = $db->fetchRow($select);

        if (!$row) {

            $select2 = $db->select()
                ->from(array('es' => $this->_name))
                ->where('es.es_course = ?', $idSubject);

            if ($student_type == 741) {
                //VISITING NO NEED TO QUERY  by semester
            } else {
                $select2->where('es.es_semester = ?', $idSemester);
            }

            $row2 = $db->fetchRow($select2);
            $row = $row2;
        }

        return $row;

    }

    /*public function getScheduleBySubject($idSemester,$idSubject,$ec_id=0,$student_type=740){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
                    ->from(array('es'=>$this->_name))
                    ->where('es.es_semester = ?',$idSemester)
                    ->where('es.es_course = ?',$idSubject);

        if(isset($ec_id) && $ec_id!=0){
                $select->where('es.es_exam_center = ?',$ec_id);
        }

        $row = $db->fetchRow($select);

        if(!$row){

            $select2 = $db->select()
                    ->from(array('es'=>$this->_name))
                    ->where('es.es_semester = ?',$idSemester)
                    ->where('es.es_course = ?',$idSubject);

               $row2 = $db->fetchRow($select2);
               $row = $row2;
        }

        return $row;

    }*/

    public function getDataByEsdId($esd_id, $result = true)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $query = $db->select()
            ->from(array('es' => $this->_name))
            ->where('es.es_esd_id = ?', $esd_id)
            ->order(array('es.es_date DESC'));

        if ($result)
        {
            $result = $db->fetchAll($query);

            return $result;
        }

        return $query;
    }

    public function getDataTaggingBySchedule($sc_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $query = $db->select()
            ->from(array('a' => 'exam_scheduletaggingslot'))
            ->join(array('b' => 'exam_slot'), 'b.sl_id = a.sts_slot_id')
            ->where('a.sts_schedule_id = ?', $sc_id);
        $result = $db->fetchAll($query);

        return $result;
    }

    public function getDataTaggingByIds($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $query = $db->select()
            ->from(array('a' => 'exam_scheduletaggingslot'))
            ->join(array('b' => 'exam_schedule'), 'a.sts_schedule_id = b.es_id')
            ->join(array('e' => 'exam_slot'), 'e.sl_id = a.sts_slot_id')
            ->join(array('c' => 'exam_setup'), 'b.es_examsetup_id = c.es_id', array())
            ->join(array('d' => 'exam_setup_detail'), 'd.esd_id = b.es_esd_id', array())
            ->join(array('f' => 'programme_exam'), 'f.pe_id = d.esd_pe_id', array('*'))
            ->where("a.sts_id IN ($id)" );


       /* select f.*
from exam_scheduletaggingslot a
join exam_schedule b on b.es_id = a.sts_schedule_id
join exam_setup c on b.es_examsetup_id = c.es_id
join exam_setup_detail d on d.esd_id = b.es_esd_id
join programme_exam f on f.pe_id = d.esd_pe_id
where a.sts_id = 12*/

        $result = $db->fetchAll($query);

        return $result;
    }

    public function getDataTaggingById($id, $pe_id, $ec_id=0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $query = $db->select()
            ->from(array('a' => 'exam_scheduletaggingslot'))
            ->join(array('b' => 'exam_schedule'), 'a.sts_schedule_id = b.es_id')
            ->join(array('e' => 'exam_slot'), 'e.sl_id = a.sts_slot_id')
            ->join(array('c' => 'exam_setup'), 'b.es_examsetup_id = c.es_id', array())
            ->join(array('d' => 'exam_setup_detail'), 'd.esd_id = b.es_esd_id', array())
            ->join(array('f' => 'programme_exam'), 'f.pe_id = d.esd_pe_id', array('*'))
          ->where("a.sts_id IN ($id)" )
//          ->where("b.es_id IN ($idSchedule)" )
          ->where("f.pe_id = ?", $pe_id );


        /* select f.*
 from exam_scheduletaggingslot a
 join exam_schedule b on b.es_id = a.sts_schedule_id
 join exam_setup c on b.es_examsetup_id = c.es_id
 join exam_setup_detail d on d.esd_id = b.es_esd_id
 join programme_exam f on f.pe_id = d.esd_pe_id
 where a.sts_id = 12*/

        $result = $db->fetchRow($query);

        return $result;
    }

    public function findDataByEsdId($esd_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $DefinitiontypeDB = new Admin_Model_Definitiontype();

        $query = $db->select()
            ->from(array('es' => $this->_name))
//            ->join(array('ec'=>'tbl_exam_center'), 'es.es_exam_center=ec.ec_id', array('ec_name'))
            ->where('es.es_esd_id = ?', $esd_id)
            ->order(array('es.es_date desc'));
        $result = $db->fetchAll($query);

        /*foreach($result as $index => $session) {
            $exam_sessions                             = $DefinitiontypeDB->fetchDetailAward($session['es_session']);
            $result[$index]['session_DefinitionDesc']  = $exam_sessions['DefinitionDesc'];
            $result[$index]['session_BahasaIndonesia'] = $exam_sessions['BahasaIndonesia'];
        }*/

        return $result;
    }

    public function flushDataByEsdId($esd_id)
    {
        return $this->delete("es_esd_id ='" . $esd_id . "'");
    }

    public function flushDataByExamsetupId($es_examsetup_id)
    {
        return $this->delete("es_examsetup_id ='" . $es_examsetup_id . "'");
    }

    public function checkAvailability($data, $es_id = null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $query = $db->select()
            ->from(array('es' => $this->_name))
            ->where('es_date = ?', $data['es_date'])
            ->where('es_exam_center = ?', $data['es_exam_center'])
            ->where("es_start_time between '" . $data["es_start_time"] . "' and '" . $data["es_end_time"] . "' OR es_end_time between '" . $data["es_start_time"] . "' and '" . $data["es_end_time"] . "'");

        if (!empty($es_id)) {
            $query->where("es.es_id != $es_id");
        }

        $result = $db->fetchRow($query);

        if (empty($result)) {
            return true;
        }

        return false;
    }

    public function getPaginationQuery($params = array())
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'exam_scheduletaggingslot'), 'b.sts_schedule_id = a.es_id', array('schedule_tagging_slot_id' => 'sts_id', 'sts_id'))
            ->join(array('c' => 'exam_slot'), 'c.sl_id = b.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->join(array('d' => 'exam_taggingslotcenterroom'), 'd.tsc_slotid = tsc_examcenterid', array('tagging_slot_center_room_id' => 'tsc_id'))
            ->join(array('e' => 'tbl_exam_center'), 'e.ec_id =  d.tsc_examcenterid', array('exam_center_id' => 'ec_id', 'ec_name'))
            /*
            ->join(array('b' => 'tbl_exam_center'), 'a.es_exam_center = b.ec_id', array('ec_name'))
            */
            ->join(array('f' => 'exam_setup'), 'f.es_id = a.es_examsetup_id', array('es_idProgram'))
            ->join(array('g' => 'exam_setup_detail'), 'g.esd_id= a.es_esd_id', array())
            ->join(array('h' => 'programme_exam'), 'h.pe_id = g.esd_pe_id', array('pe_name'))
            ->join(array('i' => 'tbl_program'), 'i.IdProgram = f.es_idProgram', array('ProgramName', 'ProgramCode'))
            ->order('i.ProgramName ASC')
            ->order('a.es_date DESC')
            ->order('c.sl_starttime ASC')
            ->order('e.ec_name ASC');

        if (isset($params['IdProgram']) && $params['IdProgram']) {
            $query->where('f.es_idProgram = ?', $params['IdProgram']);
        }

        if (isset($params['start_date']) && $params['start_date']) {
            $query->where('a.es_date >= ?', date('Y-m-d', strtotime($params['start_date'])));
        }

        if (isset($params['end_date']) && $params['end_date']) {
            $query->where('a.es_date <= ?', date('Y-m-d', strtotime($params['end_date'])));
        }

        if (isset($params['ec_id']) && $params['ec_id']) {
            $query->where('e.ec_id = ?', $params['ec_id']);
        }

        if (isset($params['group_es_id']) && $params['group_es_id']) {
            $query->group('a.es_id');
        }

        return $query;
    }
    
    public function getSimplePaginationQuery($params = array())
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('f' => 'exam_setup'), 'f.es_id = a.es_examsetup_id', array('es_idProgram'))
            ->join(array('g' => 'exam_setup_detail'), 'g.esd_id= a.es_esd_id', array())
            ->join(array('h' => 'programme_exam'), 'h.pe_id = g.esd_pe_id', array('pe_name'))
            ->join(array('i' => 'tbl_program'), 'i.IdProgram = f.es_idProgram', array('ProgramName', 'ProgramCode'))
            ->order('i.ProgramName ASC')
            ->order('a.es_date DESC');
    
        if (isset($params['IdProgram']) && $params['IdProgram']) {
            $query->where('f.es_idProgram = ?', $params['IdProgram']);
        }
    
        if (isset($params['start_date']) && $params['start_date']) {
            $query->where('a.es_date >= ?', date('Y-m-d', strtotime($params['start_date'])));
        }
    
        if (isset($params['end_date']) && $params['end_date']) {
            $query->where('a.es_date <= ?', date('Y-m-d', strtotime($params['end_date'])));
        }
    
        if (isset($params['ec_id']) && $params['ec_id']) {
            $query->where('e.ec_id = ?', $params['ec_id']);
        }
    
        if (isset($params['group_es_id']) && $params['group_es_id']) {
            $query->group('a.es_id');
        }
        
        return $query;
    }

    public function getActiveScheduleCount($esd_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => $this->_name), array('count' => new Zend_Db_Expr('COUNT(*)')))
            ->where('a.es_esd_id = ?', $esd_id)
            ->where('a.es_date > ?', date('Y-m-d'));
        $result = $db->fetchRow($query);

        return $result['count'];
    }

    public function getScheduleByExamCenterForCron($date_end, $date_start = '', $exam_center = '')
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $DefinitiontypeDB = new Admin_Model_Definitiontype();

         $select = $db->select()
            ->from(array('es' => $this->_name))
            ->join(array('ests' => 'exam_scheduletaggingslot'), 'es.es_id=ests.sts_schedule_id')
            ->join(array('etsc' => 'exam_taggingslotcenterroom'), 'ests.sts_slot_id=etsc.tsc_slotid')
            ->join(array('esl' => 'exam_slot'), 'esl.sl_id=ests.sts_slot_id')
            ->join(array('ec' => 'tbl_exam_center'), 'etsc.tsc_examcenterid=ec.ec_id', 'ec.*')
            ->joinLeft(array('ls' => 'ol_examcenter_server'), 'ec.ec_id=ls.svr_ec_id', 'ls.*')
            ->join(array('sm' => 'tbl_subjectmaster'), 'sm.IdSubject=es.es_course', array('IdSubject', 'subjectMainDefaultLanguage', 'SubCode', 'SubjectName'))
            ->order('es.es_date')
            ->order('ec.ec_id')
            //->group('etsc.tsc_slotid');
            ->group('es.es_id');

        if ($exam_center) {
            echo $select->where('etsc.tsc_examcenterid = ?', $exam_center);
        }

        $date_today = ($date_start) ? $date_start : date('Y-m-d');
        $date_end = date('Y-m-d', strtotime($date_end));

        $select->where('es_date >= ?', $date_today)
            ->where('es_date <= ?', $date_end);
            
        $result = $db->fetchAll($select);

        return $result;
    }

      public function getScheduleByExamCenter_ForCron($date_end, $date_start = '', $exam_center = '')
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $DefinitiontypeDB = new Admin_Model_Definitiontype();

         $select = $db->select()
            ->from(array('es' => $this->_name),'es.*')
            ->join(array('ests' => 'exam_scheduletaggingslot'), 'es.es_id=ests.sts_schedule_id')
            ->joinleft(array('etsc' => 'exam_taggingslotcenterroom'), 'ests.sts_slot_id=etsc.tsc_slotid','etsc.*')
             ->join(array('esl' => 'exam_slot'), 'esl.sl_id=ests.sts_slot_id')
            ->join(array('ec' => 'tbl_exam_center'), 'etsc.tsc_examcenterid=ec.ec_id', 'ec.*')
            //->joinLeft(array('ls' => 'ol_examcenter_server'), 'ec.ec_id=ls.svr_ec_id', 'ls.*')
            //->joinLeft(array('ese' => 'exam_schedule_exception'), 'ese.schedule_tagging_slot_id=ests.sts_schedule_id', 'ese.*')
             ->join(array('sm' => 'tbl_subjectmaster'), 'sm.IdSubject=es.es_course', array('IdSubject', 'subjectMainDefaultLanguage', 'SubCode', 'SubjectName'))
            ->where('ec.is_local = 1')
            ->order('es.es_date')
            ->order('ec.ec_id');
          // ->group('ests.sts_schedule_id') ;
            //->group('etsc.tsc_slotid')
            //->group('etsc.tsc_examcenterid')
           //->group('es.es_date');

        if ($exam_center) {
            $select->where('ec.ec_id = ?', $exam_center);
        }

        $date_end = date('Y-m-d', strtotime($date_end));
        $date_today = ($date_start) ? $date_start : ((date('Y-m-d') > $date_end) ? date('Y-m-d', strtotime('-1 day ' . $date_end)) : date('Y-m-d'));

        $select->where('es_date >= ?', $date_today)
            ->where('es_date <= ?', $date_end);

        $result = $db->fetchAll($select);

        return $result;
    }

      public function getSchedulereportByExamCenter($date_end, $date_start = '', $exam_center = '')
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $DefinitiontypeDB = new Admin_Model_Definitiontype();
         $select = $db->select()
            ->from(array('os' => 'olc_central_exam'), 'os.*');
            //->joinLeft(array('ox' => 'olc_central_exam'),'ox.ec_id=os.ocs_ec_id');
            //  ->joinLeft(array('es' => $this->_name),'os.ocs_es_id=es.es_id')
            //  ->joinLeft(array('ests' => 'exam_scheduletaggingslot'), 'es.es_id=ests.sts_schedule_id')
            //  ->joinleft(array('etsc' => 'exam_taggingslotcenterroom'), 'ests.sts_slot_id=etsc.tsc_slotid','etsc.*')
            // ->join(array('ec' => 'tbl_exam_center'), 'os.ocs_ec_id=ec.ec_id', 'ec.*');

        if ($exam_center) {
            $select->where('os.ec_id = ?', $exam_center);
        }

       echo $date_end = date('Y-m-d', strtotime($date_end));
       echo $date_today = ($date_start) ? $date_start : ((date('Y-m-d') > $date_end) ? date('Y-m-d', strtotime('-1 day ' . $date_end)) : date('Y-m-d'));

    echo    $select->where('exam_date >= ?', $date_today)
            ->where('exam_date <= ?', $date_end);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getScheduleByIdForCron($id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $DefinitiontypeDB = new Admin_Model_Definitiontype();

        echo $select = $db->select()
            ->from(array('es' => $this->_name))
            ->join(array('ests' => 'exam_scheduletaggingslot'), 'es.es_id=ests.sts_schedule_id')
            ->join(array('etsc' => 'exam_taggingslotcenterroom'), 'ests.sts_slot_id=etsc.tsc_slotid','etsc.*')
            ->join(array('esl' => 'exam_slot'), 'esl.sl_id=ests.sts_slot_id')
            ->join(array('ec' => 'tbl_exam_center'), 'etsc.tsc_examcenterid=ec.ec_id', 'ec.*')
            ->joinLeft(array('ls' => 'ol_examcenter_server'), 'ec.ec_id=ls.svr_ec_id', 'ls.*')
            ->join(array('sm' => 'tbl_subjectmaster'), 'sm.IdSubject=es.es_course', array('IdSubject', 'subjectMainDefaultLanguage', 'SubCode', 'SubjectName'))
            ->order('es.es_date')
            ->order('ec.ec_id')
            ->group('etsc.tsc_slotid')
            ->where('es.es_id = ?', $id);exit;

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getFutureScheduleDate($date = '')
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $date = ($date == '') ? date('Y-m-d') : $date;

        $select = $db->select()
            ->from(array('es' => $this->_name))
            ->where('es_date >= ?', $date)
            ->group('es_date');

        $row = $db->fetchAll($select);
        return $row;
    }

    public function getTaggingSchedule($es_id, $slot_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => 'exam_scheduletaggingslot'))
            ->where('a.sts_schedule_id = ?', $es_id)
            ->where('a.sts_slot_id = ?', $slot_id);
        $result = $db->fetchRow($query);

        return $result;
    }

    public function getDataByDate($date = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name),array('es_date', 'es_id'=>'GROUP_CONCAT(es_id)','*'))
            ->group('a.es_date')
            ->order('a.es_date asc');

        if ($date) {
            $select->where('a.es_date = ?', $date);
        }

        $row = $db->fetchAll($select);

        return $row;

    }

    public function getTaggingScheduleByDate($date)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => 'exam_scheduletaggingslot'), array('sts_id'=>'GROUP_CONCAT(a.sts_id)'))
            ->join(array('b' => 'exam_schedule'), 'a.sts_schedule_id = b.es_id')
            ->join(array('c' => 'exam_slot'), 'c.sl_id = a.sts_slot_id')
            ->where('b.es_date = ?', $date)
            ->group('c.sl_id');
        $result = $db->fetchAll($query);

        return $result;
    }

    public function getExamBySchedule($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => 'exam_schedule'))
            ->join(array('b' => 'exam_setup'), 'a.es_examsetup_id = b.es_id', array())
            ->join(array('c' => 'exam_setup_detail'), 'c.esd_id = a.es_esd_id', array())
            ->join(array('d' => 'programme_exam'), 'c.esd_pe_id = d.pe_id', array('*'))
            ->where("a.es_id IN ($id)");
        $result = $db->fetchAll($query);

        return $result;
    }

    public function getSessions($es_id, $params = array()) 
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'exam_scheduletaggingslot'), 'b.sts_schedule_id = a.es_id', array('schedule_tagging_slot_id' => 'sts_id', 'sts_id'))
            ->join(array('c' => 'exam_slot'), 'c.sl_id = b.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->join(array('d' => 'exam_taggingslotcenterroom'), 'd.tsc_slotid = tsc_examcenterid', array('tagging_slot_center_room_id' => 'tsc_id'))
            ->join(array('e' => 'tbl_exam_center'), 'e.ec_id =  d.tsc_examcenterid', array('exam_center_id' => 'ec_id', 'ec_name'))
            ->join(array('f' => 'tbl_examroom'), 'f.id = d.tsc_room_id', array('room_id' => 'id', 'room_name' => 'name'))
            ->where('a.es_id = ?', $es_id)
            ->order('e.ec_name')
            ->order('c.sl_starttime');

        if(isset($params['group_examcenter']) && $params['group_examcenter']) {
            $select->group('e.ec_id');
        }
    
        if(isset($params['no_room']) && $params['no_room']) {
            
        }
        
        

        return $db->fetchAll($select);
    }

    public function getDataWithSession($es_id, $examtaggingslot_id, $ec_id = 0)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'exam_scheduletaggingslot'), 'b.sts_schedule_id = a.es_id', array('schedule_tagging_slot_id' => 'sts_id', 'sts_id'))
            ->join(array('c' => 'exam_slot'), 'c.sl_id = b.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->where('a.es_id = ?', $es_id)
            ->where('b.sts_id  = ?', $examtaggingslot_id)
            //->order('e.ec_name')
            ->order('c.sl_starttime');
        
        if ($ec_id)
        {
            $select->join(array('e' => 'tbl_exam_center'), 'e.ec_id =  '. $ec_id, array('exam_center_id' => 'ec_id', 'ec_name'));
        }
        else
        {
            $select->join(array('d' => 'exam_taggingslotcenterroom'), 'd.tsc_slotid = tsc_examcenterid', array('tagging_slot_center_room_id' => 'tsc_id'));
            $select->join(array('e' => 'tbl_exam_center'), 'e.ec_id =  d.tsc_examcenterid', array('exam_center_id' => 'ec_id', 'ec_name'));
        }
    
        return $db->fetchRow($select);
    }

    public function checkSameDate($esd_id, $es_date) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.es_esd_id = ?', $esd_id)
            ->where('a.es_date = ?', $es_date);

        $result = $db->fetchRow($select);

        if($result) {
            return 1;
        }
        else {
            return 0;
        }
    }

    public function getSchedules($schedule_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => $this->_name), array(
                'exam_schedule_id'    => 'es_id',
                'exam_setup_id'       => 'es_examsetup_id',
                'exam_setupdetail_id' => 'es_esd_id',
                'exam_date'           => 'es_date'
            ))
            ->join(array('b' => 'exam_scheduletaggingslot'), 'b.sts_schedule_id = a.es_id', array('exam_scheduletaggingslot_id' => 'sts_id'))
            ->join(array('c' => 'exam_slot'), 'c.sl_id = b.sts_slot_id', array(
                'exam_slot_id'        => 'sl_id', 
                'exam_slot_name'      => 'sl_name',
                'exam_slot_starttime' => 'sl_starttime', 
                'exam_slot_endtime'   => 'sl_endtime', 
                // 'es_start_time'       => 'sl_starttime', 
                // 'es_end_time'         => 'sl_endtime'
            ))
            // ->join(array('d' => 'exam_taggingslotcenterroom'), 'd.tsc_slotid = b.sts_slot_id', array('tsc_id'))
            // ->join(array('e' => 'tbl_exam_center'), 'e.ec_id =  d.tsc_examcenterid', array('exam_center_id' => 'ec_id', 'exam_center_name' => 'ec_name'))
            // ->join(array('f' => 'tbl_examroom'), 'f.id = d.tsc_room_id AND f.examcenter_id = e.ec_id', array('exam_room_id' => 'id', 'exam_room_name' => 'name'))
            ->order('c.sl_starttime ASC')
            ->where('a.es_id = ?', $schedule_id);
        $result = $db->fetchAll($query);
        return $result;

    }
    
    public function addBatchScheduleAction()
    {
        $esd_id = $this->_getParam('id', 0);
        
        if (!$esd_id) {
            $this->redirect('/examination/exam-setup');
        }
        
        
        $esDb           = new Admin_Model_DbTable_ExamSetup();
        $esdDb          = new Admin_Model_DbTable_ExamSetupDetail();
        $examSlotDB     = new Admin_Model_DbTable_ExamSlot();
        $programDB      = new Admin_Model_DbTable_Program();
        $examScheduleDB = new Admin_Model_DbTable_ExamSchedule();
        $auth           = Zend_Auth::getInstance();
        
        $examsetupdetail = $esdDb->getDataById($esd_id);
        
        if (!$examsetupdetail) {
            $this->redirect('/examination/exam-setup');
        }
        
        $es_id = $examsetupdetail['examsetup_id'];
        
        
        if ($this->getRequest()->isPost()) {
            $created  = 0;
            $failed   = array();
            $formData = $this->getRequest()->getPost();
            
            $dates = explode(', ', $formData['dates']);
            $slots = array();
            
            foreach ($formData['slot'] as $sl_id => $selected)
            {
                if ($selected)
                {
                    $slots[] = $sl_id;
                }
            }
            
            foreach ($dates as $date) {
                $publish_date = date('Y-m-d', strtotime("+$formData[days_to_publish] days", strtotime($date)));
                
                $exam_schedule                           = array();
                $exam_schedule['es_esd_id']              = $esd_id;
                $exam_schedule['es_examsetup_id']        = $es_id;
                $exam_schedule['es_date']                = $date;
                $exam_schedule['es_publish_start_date']  = $publish_date;
                $exam_schedule['es_publish_start_time']  = '00:00:00';
                $exam_schedule['es_publish_immediately'] = ($formData['days_to_publish'] == 0 ? 1 : 0);
                $exam_schedule['es_course']              = $examsetupdetail['IdProgram'];
                $exam_schedule['es_createddt']           = date("Y-m-d H:i:s");
                $exam_schedule['es_createdby']           = $auth->getIdentity()->id;
                $exam_schedule['es_modifydt']            = '0000-00-00 00:00:00';
                $exam_schedule['es_modifyby']            = 0;
                $exam_schedule['es_active']              = $formData['active'];
                
                $conflict = $examScheduleDB->checkSameDate($esd_id, $date);
                
                if ($conflict)
                {
                    $failed[] = date('d-m-Y', strtotime($date));
                    continue;
                }
                
                $created++;
                
                $examschedule_id = $examScheduleDB->addData($exam_schedule);
                
                foreach ($slots as $slot)
                {
                    $exam_scheduletaggingslot = array();
                    $exam_scheduletaggingslot['sts_schedule_id'] = $examschedule_id;
                    $exam_scheduletaggingslot['sts_slot_id']     = $slot;
                    $exam_scheduletaggingslot['created_date']    = date("Y-m-d H:i:s");
                    $exam_scheduletaggingslot['created_by']      = $auth->getIdentity()->id;
                    $examScheduleDB->addDataTagging($exam_scheduletaggingslot);
                }
            }
            
            if ($created)
            {
                $this->_helper->flashMessenger->addMessage(array('success' => "Exam schedules successfully created"));
            }
            
            if (!empty($failed))
            {
                $failed = implode(', ', $failed);
                $this->_helper->flashMessenger->addMessage(array('error' => "Failed to create exam schedule on $failed"));
            }
            
            $this->redirect("/examination/exam-setup/schedules/id/$esd_id/es_id/$es_id");
            exit;
        }
        
        
        $examSlot = $examSlotDB->getData(0, 1);
        
        foreach ($examSlot as $a => $slot) {
            $room = $examSlotDB->getDataTagging($slot['sl_id']);
            
            if ($room) {
                $examSlot[$a]['totalcenter'] = count($room);
                $examSlot[$a]['center']      = $room;
            } else {
                unset($examSlot[$a]);
            }
        }
        
        $awards = $programDB->getListExcept($examsetupdetail['IdProgram']);
        
        
        $this->view->examSlot        = $examSlot;
        $this->view->esd_id          = $esd_id;
        $this->view->es_id           = $examsetupdetail['examsetup_id'];
        $this->view->examsetupdetail = $examsetupdetail;
        $this->view->awards          = $awards;
        $this->view->title           = 'Add Batch Schedule';
        //pr($examSlot);
        
    }

}

?>