<?php 

class Admin_Model_DbTable_ExamSetupDetail extends Zend_Db_Table_Abstract {
	
	protected $_name = 'exam_setup_detail';
	protected $_primary = "esd_id";
	
	public function addData($data){
		$this->insert($data);
		$db = Zend_Db_Table::getDefaultAdapter();
		return $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		$this->update($data,"esd_id ='".$id."'");
	}
	
	public function deleteData($add_id){
		$this->delete("esd_id ='".$add_id."'");
	}

    public function getDataByEsId($es_id, $check_prerequisite = true) {
        $db    = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('esd' => $this->_name))
            ->join(array('pe'  => 'programme_exam'), 'esd.esd_pe_id=pe.pe_id', array('pe_name'))
            ->join(array('mds' => 'tbl_markdistribution_setup'), 'mds.mds_id = esd.esd_mds_id', array('mds_id', 'mds_name'))
            ->where('es_id = ?', $es_id);
        $result = $db->fetchAll($query);

        $pepDB = new Admin_Model_DbTable_ProgrammeExamPrerequisite();
        
        if($check_prerequisite) {
            foreach($result as $index => $es) {
                $result[$index]['prerequisites'] = $pepDB->getDataByPeId($es['esd_pe_id']);
            }
        }

        return $result;
    }

    public function getDataById($esd_id, $join = true) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('esd' => 'exam_setup_detail'), array('esd_id', 'esd_type', 'esd_compulsory'))
            ->join(array('es'  => 'exam_setup'), 'esd.es_id=es.es_id', array('examsetup_id' => 'es_id', 'es_name', 'es_idProgram', 'es_compulsory', 'es_online', 'es_awardname'))
            ->join(array('l'   => 'tbl_landscape'),'l.IdLandscape=es.es_idLandscape',array('IdLandscape', 'IdStartSemester'))
            ->where('esd.esd_id = ?', $esd_id);
    
        if($join) {
            $query->join(array('p'   => 'tbl_program'), 'p.IdProgram=es.es_idProgram', array('IdProgram', 'ProgramName', 'ProgramCode'));
            $query->join(array('pe'  => 'programme_exam'), 'esd.esd_pe_id=pe.pe_id', array('pe_id', 'pe_name'));
            $query->join(array('mds' => 'tbl_markdistribution_setup'), 'mds.mds_id = esd.esd_mds_id', array('mds_id', 'mds_name'));
            $query->joinLeft(array('sm'  => 'tbl_subjectmaster'), 'esd.esd_idSubject=sm.IdSubject', array('IdSubject', 'SubjectName','BahasaIndonesia','SubCode','CreditHours','CourseType'));
            $query->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme = l.IdProgramScheme', array('IdProgramScheme'));
            $query->join(array('lm' => 'tbl_definationms'), 'lm.IdDefinition = ps.mode_of_program', array('mode_of_program_desc' => 'DefinitionDesc'));
        }
    
        $result = $db->fetchRow($query);

        return $result;
    }

    public function flushDataByEsId($es_id) {
        return $this->delete("es_id ='".$es_id."'");
    }
	

    public function getPaginationQuery() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('esd' => 'exam_setup_detail'))
            ->join(array('es'  => 'exam_setup'), 'esd.es_id=es.es_id', array('es_name', 'es_idProgram'))
            ->join(array('l'   => 'tbl_landscape'),'l.IdLandscape=es.es_idLandscape',array('IdStartSemester'))
            ->join(array('p'   => 'tbl_program'), 'p.IdProgram=es.es_idProgram', array('ProgramName', 'ProgramCode'))
            ->join(array('pe'  => 'programme_exam'), 'esd.esd_pe_id=pe.pe_id', array('pe_name'))
            ->join(array('mds' => 'tbl_markdistribution_setup'), 'mds.mds_id = esd.esd_mds_id', array('mds_id', 'mds_name'))
            ->joinLeft(array('sm'  => 'tbl_subjectmaster'), 'esd.esd_idSubject=sm.IdSubject', array('SubjectName','BahasaIndonesia','SubCode','CreditHours','CourseType'))
            ->order('p.ProgramName ASC')
            ->order('es.es_name ASC')
            ->order('pe.pe_name ASC');
        return $query;
    }

    public function getDataByPeId($pe_id=0) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_setup_detail'), array('esd_id'))
            ->join(array('b' => 'exam_setup'), 'b.es_id = a.es_id', array('es_id'))
            ->join(array('c' => 'programme_exam'), 'c.pe_id = a.esd_pe_id', array('pe_name'));

        if($pe_id){
            $select->where('a.esd_pe_id = ?', $pe_id);
            $result = $db->fetchRow($select);
        }else{
            $result = $db->fetchAll($select);
        }

        return $result;
    }
}

?>