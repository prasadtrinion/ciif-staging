<?php
class Admin_Model_DbTable_MemberDocument extends Zend_Db_Table {
	
	protected $_name = 'membership_registration_checklist';
	protected $_PRIMARY = 'mrc_id'; // table name
	
	/*
	 * get all active email templates rows 
	 */
   
     

     public function getDefination($deftypeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $deftypeid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function addData($dropdown_member,$name,$remarks)
    {
    		 $auth        = Zend_Auth::getInstance();
        $user_id = $auth->getIdentity()->id;

    		$data = array (
    						'mrc_type' => $dropdown_member,
                'mrc_name'=>$name,
                'mrc_remarks' => $remarks,
                'mrc_status' => 1,
                'mrc_createddt'=> date('Y-m-d'),
                'mrc_createdby' => $user_id

                );
    
    		$this->insert($data);
    }

    public function search($id)
    {
      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()                         
                         ->from(array('a' => 'membership_registration_checklist'),array('a.*'))    
                        ->join(array('b'=>'member'),'a.mrc_type = b.idmember') 
                        ->where('a.mrc_type = ' . $id);
         $select = $lobjDbAdpt->fetchAll($lstrSelect);
      // $select = $this->select();
      // $result =  $this->fetchAll($select);
      if($select)
      {

        
        return $select;
      }
      return false;
      // echo "<pre>";
      // print_r($result);
      // die();
    }
    public function getsearch()
    {
        
        
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()                         
                 ->from(array('a' => 'membership_registration_checklist'),array('a.*'))    
                        ->join(array('b'=>'member'),'a.mrc_type = b.idmember'); 
                         
        $Result = $lobjDbAdpt->fetchAll($lstrSelect);

        // echo "<pre>";
        // print_r($Result);
        // die();

        return $Result;
    print_r($Result);
    }

    public function fnGetTemplateDetails() {   			    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->join(array('a' => 'member'),array('idmember'));			
				
			$result = $this->fetchAll($select);		
			return $result->toArray(); 		    
   	}

   	/*
   	 * search by criteria   
   	 */
	public function fnSearchTemplate($post = array()) {		
		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $this->select()
							->setIntegrityCheck(false)  	
							->join(array('a' => 'member'),array('idTemplate', 'member_name','sub_type'))														
							->where('a.member_name like "%" ? "%"',$post['field3'])
							->where('a.sub_type like "%" ? "%"',$post['field2']);
													
			$result = $this->fetchAll($select);
			return $result->toArray();			
	}
	
	/*
	 * Add Email Template
	 */
	public function fnAddEmailTemplate($post,$editorData) {
		
		$post['TemplateBody'] = $editorData;			
		$this->insert($post);
	}
	
	/*
	 * get single email template row values bu $id
	 */
    public function fnViewTemplte($idmember) {
    			
		$result = $this->fetchRow( "idmember = '$idmember'") ;
        return @$result->toArray();     // @symbol is used to avoid warning    	
    }
     
     public function editmember($id,$dropdown_member,$name,$remarks)

    {
       $auth        = Zend_Auth::getInstance();
        $user_id = $auth->getIdentity()->id;


        $data = array (
                'mrc_type' => $dropdown_member,
                'mrc_name'=>$name,
                'mrc_remarks' => $remarks,
                'mrc_status' => 1,
                'mrc_createddt'=> date('Y-m-d'),
                'mrc_createdby' => $user_id

                );  
        // print_r($data);
        //    echo $id;
        //      die();
      $this->update($data,'mrc_id = '.$id);  
    }
    /*
     * update email template 
     */
    public function fnUpdateTemplate($whereId,$formData,$editorData) {
    		unset($formData['idDefination']);
    			    				
			$this->update($formData,$whereId);
    }
    	
}
