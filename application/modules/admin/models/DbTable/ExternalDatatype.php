<?php
class Admin_Model_DbTable_ExternalDatatype extends Zend_Db_Table {
	
	protected $_name = 'external_datatype';

	public function getData()
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select()
			->from(array('a'=>$this->_name))
			->joinLeft(array('b'=>'external_data'),'b.type_id=a.id', array('COUNT(b.id) as total_data'))
			->group('a.id');

		return $db->fetchAll($select);
	}
}