<?php
class Admin_Model_DbTable_MemberRenewFee extends Zend_Db_Table {
	
	protected $_name = 'tbl_member_renew_fee_structure';
	protected $_PRIMARY = 'id'; // table name
	
	/*
	 * get all active email templates rows 
	 */
    
     

     public function getDefination($deftypeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $deftypeid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function addData($dropdown_fee,$dropdown_currency,$dropdown_member,$sub_type,$amount,$nationality,$degree,$date,$active,$gst,$calculated_amount,$renewal_year)
    {
    		

    		$data = array (	
    						'id_fees'=>$dropdown_fee,
                'id_currency'=>$dropdown_currency,
                'nationality'=> $nationality,  
    						'id_member_subtype' => $sub_type,
                'id_member' => $dropdown_member,
                'amount' => $amount,
                 'effective_date' => date('Y-m-d', strtotime($date)), 	
                'id_degree' => $degree,
                'active'    => $active,
                'gst'               => $gst,
                'calculated_amount' => $calculated_amount,
                'renewal_year' => $renewal_year				
    						
    						);

    		
    		$this->insert($data);
    }

    public function search($id)
    {
      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()                         
                        ->from(array('a' => 'tbl_member_renew_fee_structure'),array('a.*'))    
                        ->join(array('b'=>'member'),'a.id_member = b.idmember') 
                         ->join(array('d'=>'tbl_member_subtype'),'a.id_member_subtype = d.id_subtype')
                        
                        //->join(array('f'=>'tbl_definationms'),'a.id_fees = f.idDefinition')
                        ->join(array("feetyp"=>"vw_feetypes"),'feetyp.idDefinition=a.id_fees',array('Feetype'=>'DefinitionDesc'))
                        ->join(array("c"=>"tbl_definationms"),'c.idDefinition=a.id_degree',array('degree'=>'DefinitionDesc'))
                         ->join(array('f'=>'tbl_definationms'),'a.id_currency = f.idDefinition')             ->where('a.id_member = ' . $id);
         $select = $lobjDbAdpt->fetchAll($lstrSelect);

         // echo "<pre>";
         // print_r($select);
         // die();
      // $select = $this->select();
      // $result =  $this->fetchAll($select);
      if($select)
      {

        
        return $select;
      }
      return false;
      // echo "<pre>";
      // print_r($result);
      // die();
    }
    public function getsearch()
    {
        
        
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()                         
                ->from(array('a' => 'tbl_member_renew_fee_structure'),array('a.*'))    
                ->join(array('b'=>'member'),'a.id_member = b.idmember') 
                ->join(array('d'=>'tbl_member_subtype'),'a.id_member_subtype = d.id_subtype')
                        
                  ->join(array("feetyp"=>"vw_feetypes"),'feetyp.idDefinition=a.id_fees',array('Feetype'=>'DefinitionDesc'))
                      
                 
                ->join(array('c'=>'tbl_definationms'),'a.id_currency = c.idDefinition') ;  
                        
        $Result = $lobjDbAdpt->fetchAll($lstrSelect);

        // echo "<pre>";
        // print_r($Result);
        // die();

        return $Result;
    
    }

    public function getfees()
    {
        
        
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()                         
                        ->from(array('a' => 'tbl_member_renew_fee_structure'),array('a.*'))    
                        
                        
                        ->join(array('f'=>'tbl_definationms'),'a.id_fees =f.idDefinition');  
                        
        $Result = $lobjDbAdpt->fetchAll($lstrSelect);

        return $Result;
  
    }

    public function getrenewalfees($id,$sub,$year,$date,$nationality)
    {

      $get_date = date("Y-m-d",strtotime("December 31st"));
        
        
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()                         
           ->from(array('a' => 'tbl_member_renew_fee_structure'),array('sum(calculated_amount) as sum'))
            ->where('a.effective_date >= ?', $date)
           ->where('a.effective_date <= ?',$get_date) 
           ->where('a.nationality = ?',$nationality)
           ->where('a.id_member = '. $id)
           ->where('a.id_member_subtype = '. $sub)
           ->where('a.renewal_year = ?', $year);
        $Result = $lobjDbAdpt->fetchAll($lstrSelect);
       
        return $Result;
   
    }

    public function fnGetTemplateDetails() {   			    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->join(array('a' => 'member'),array('idmember'));			
				
			$result = $this->fetchAll($select);		
			return $result->toArray(); 		    
   	}

   	/*
   	 * search by criteria   
   	 */
	public function fnSearchTemplate($post = array()) {		
		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $this->select()
							->setIntegrityCheck(false)  	
							->join(array('a' => 'member'),array('idTemplate', 'member_name','sub_type'))														
							->where('a.member_name like "%" ? "%"',$post['field3'])
							->where('a.sub_type like "%" ? "%"',$post['field2']);
													
			$result = $this->fetchAll($select);
			return $result->toArray();			
	}
	
	/*
	 * Add Email Template
	 */
	public function fnAddEmailTemplate($post,$editorData) {
		
		$post['TemplateBody'] = $editorData;			
		$this->insert($post);
	}
	
	/*
	 * get single email template row values bu $id
	 */
    public function fnViewTemplte($idmember) {
    			
		$result = $this->fetchRow( "idmember = '$idmember'") ;
        return @$result->toArray();     // @symbol is used to avoid warning    	
    }
     
     public function editmember($id,$dropdown_fee,$dropdown_currency,$dropdown_member,$sub_type,$amount,$nationality,$degree,$date,$active,$gst,$calculated_amount,$renewal_year)

    {

      
                    $data = array ( 
                'id_fees'   =>$dropdown_fee,
                'id_currency' =>$dropdown_currency,
                'id_member_subtype'=> $sub_type,
                'id_member' => $dropdown_member,
                'amount' => $amount,
                'nationality'=> $nationality, 
                'effective_date' =>date('Y-m-d', strtotime($date)),
                'id_degree' => $degree,
                'active'    => $active,
        'gst'               => $gst,
      'calculated_amount' => $calculated_amount,
      'renewal_year' => $renewal_year

                
                
                );
                           
       
      $this->update($data,'id = '.$id);  
      
    }
    /*
     * update email template 
     */
    public function fnUpdateTemplate($whereId,$formData,$editorData) {
    		unset($formData['idDefination']);
    			    				
			$this->update($formData,$whereId);
    }
    	
}
