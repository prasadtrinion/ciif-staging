<?php
class Admin_Model_DbTable_Receipt extends Zend_Db_Table {
	
	protected $_name = 'tbl_receipt';
	protected $_PRIMARY = 'receipt_id'; // table name
	
	public function insertreceipt($formdata)
  {
        $db = Zend_Db_Table::getDefaultAdapter();

         $invoice = explode('@@',$formdata['invoice']);
         
         $data = array ( 
                'receipt_number'=>'RECP'.date('YmH'),
                'user_id'=>$formdata['name'][0],
                'invoice_id' => $invoice[0],

                'created_by'=>1,
                 'created_date'=>date('Y-m-H'),
                'updated_by'=>1,
                'updated_date'=>date('Y-m-H')                
                );  
       $id =$this->insert($data);

        $amount = 0;
        for($i=0; $i  < count($formdata['amount']);$i++)
        {
          $amount = $amount + $formdata['amount'][$i];
        }
     
        $sql = "update tbl_receipt set amount = '$amount'
          where receipt_id = $id";       
        $receipt_id = $db->exec($sql);
       return $id;
  }

  public function getReceipt($recno)
  {
       $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a"=>"tbl_receipt"))
            ->joinLeft(array('b'=>'user'),'a.user_id=b.id')
            ->where('a.receipt_number = ?',$recno);

        $row = $db->fetchAll($select);


        return $row;

  }

  public function ReceiptReport($recno)
  {
       $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a"=>"tbl_receipt"))
            ->joinLeft(array('c' =>'tbl_receipt_details'),'a.receipt_id = c.receipt_id')
            ->joinLeft(array('d' =>'tbl_definationms'),'d.idDefinition = c.id_paytype')
            ->where('a.receipt_number = ?',$recno);

        $row = $db->fetchAll($select);

       
        return $row;

  }


  public function getReceiptNumber()
  {
       $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a"=>"tbl_receipt"));

        $row = $db->fetchAll($select);
        return $row;

  }



    	
}
