<?php
class Admin_Model_DbTable_Registrationlocation extends Zend_Db_Table {
	protected $_name = 'tbl_registrationlocation';

	/**
	 *
	 * @see Zend_Db_Table_Abstract::init()
	 */

	public function fnGetConf($idUniversity){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_config'))
		->where('a.idUniversity  = ?',$idUniversity);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}

	public function fnaddreglocation($data) {
		$this->insert($data);
		$insertId = $this->lobjDbAdpt->lastInsertId($this->_name,'IdRegistrationLocation');
		return $insertId;
	}

	public function fnaddregInfo($data){
		$regMappingtable = new Zend_Db_Table('tbl_registration_info');
		$regMappingtable->insert($data);
	}

	public function fngetreglocations(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()->from(array('a' => 'tbl_registrationlocation'));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}


	public function fnSearchreglocation($post = array()) { //Function for searching the Activity details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("is"=>"tbl_registrationlocation"),array("is.*"))
		->where('is.RegistrationLocationCode like "%" ? "%"',$post['field3'])
		->where('is.RegistrationLocationName like "%" ? "%"',$post['field2'])
		->where('is.RegistrationLocationShortName like "%" ? "%"',$post['field4'])
		->order("is.RegistrationLocationShortName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}


	public function fngetsubjectgradepoints($id){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		//    $lstrSelect = $lobjDbAdpt->select()
		//    ->from(array("a"=>"tbl_registration_info"),array("a.*"))
		//    ->where('a.IdRegistrationLocation = ?',$id);
		$lstrSelect = $lobjDbAdpt->select()
		->from(array('a' => 'tbl_registration_info'),array('a.*'))
		->join(array('b' => 'tbl_scheme'),'a.RegistrationLocationScheme = b.IdScheme',array('b.EnglishDescription'))
		->joinLeft(array('Branch' => 'tbl_branchofficevenue'),'a.RegistrationLocationBranch = Branch.IdBranch',array('Branch.BranchName'))
		->joinLeft(array('Program' => 'tbl_program'),'a.RegistrationLocationProgram = Program.IdProgram',array('Program.ProgramName'))
		->where('a.IdRegistrationLocation = ?',$id);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fngetreglocation($id){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("a"=>"tbl_registrationlocation"),array("a.*"))
		->where('a.IdRegistrationLocation = ?',$id);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}

	public function fnupdatereglocation($reglocationArray,$id){
		$where = 'IdRegistrationLocation = '.$id;
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->update('tbl_registrationlocation' , $reglocationArray , $where);
	}

	public function fndeletereglocInfo($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$where = $db->quoteInto('IdRegistrationLocation = ?', $id);
		$db->delete('tbl_registration_info', $where);
	}
	
	public function fngetReglocationdet($intake,$scheme) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
					->from(array("a"=>"tbl_registrationlocation"),array('a.RegistrationLocationAddress1','a.RegistrationLocationAddress2'))
					->join(array("b"=>"tbl_registration_info"),'a.IdRegistrationLocation = b.IdRegistrationLocation',array("b.*"))
					->join(array("c"=>"tbl_countries"),'a.country = c.idCountry',array("c.CountryName","c.DefaultCountryLanguage as arabicstatename"))
					->join(array("d"=>"tbl_state"),'a.state = d.idState',array("d.StateName","d.DefaultCountryLanguage as arabicstatename"))
					->join(array("e"=>"tbl_city"),'a.city = e.idCity',array("e.CityName","d.DefaultCountryLanguage as arabiccityname"))
					->where('a.RegistrationLocationIntake = ?',$intake)
					->where('b.RegistrationLocationScheme = ?',$scheme);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}


}
