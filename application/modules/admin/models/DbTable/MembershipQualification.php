<?php
class Admin_Model_DbTable_MembershipQualification extends Zend_Db_Table {
	
	protected $_name = 'membership_qualification';
	protected $_PRIMARY = 'id_mq';

	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}
	
	public function getmembertype()
	{
		$db = $this->getDefaultAdapter();

		$select = $db->select()
			->from(array($this->_name));

		$result = $db->fetchAll( $select );

		// print_r($result);
		// die();

		return $result;
	}


	 public function getListingData($table ,$where="",$order="", $id=0)
    {
        // $db     = getDB();
        $select = $this->select()
            ->from(array('a' => $table));
            

        if($where !=""){
            $select ->where($where);
        }
        if($order !=""){
            $select ->order($order);
        }

        $result = $this->fetchAll($select);
        return $result;
    }

	  public function addData($member_name, $sub_type)
    {
    		

    		$data = array (	
    						'member_name'=>$member_name,
    						'sub_type' => $sub_type,

    						
    						
    						);
    		$this->insert($data);
    }
    public function fnGet() {   			    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
			->from(array('a' => 'membership_qualification'))
			->join(array('m' => 'member'), 'm.idmember =a.member_type')
			->join(array('b' => 'tbl_definationms'),'b.idDefinition = a.program_route', array('level'=>'DefinitionDesc'))
            ->join(array('c' => 'tbl_definationms'),'c.idDefinition = a.program_level', array('route'=>'DefinitionDesc'));			
				
			$result = $this->fetchAll($select);	
				
			return $result->toArray(); 		    
   	}

   	/*
   	 * search by criteria   
   	 */
	public function fnSearch($post) {	


		
		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
							
							->from(array('a' => 'membership_qualification'))
			->join(array('m' => 'member'), 'm.idmember =a.member_type')
			->join(array('b' => 'tbl_definationms'),'b.idDefinition = a.program_route', array('level'=>'DefinitionDesc'))
            ->join(array('c' => 'tbl_definationms'),'c.idDefinition = a.program_level', array('route'=>'DefinitionDesc'))														
							->where('a.member_type = ?',$post);
							
													
			$result = $db->fetchAll($select);
			return $result;			
	}
	
	/*
	 * Add Email Template
	 */
	public function fnAddEmailTemplate($post,$editorData) {
		
		$post['TemplateBody'] = $editorData;			
		$this->insert($post);
	}
	
	/*
	 * get single email template row values bu $id
	 */
    public function fnViewTemplte($idmember) {
    			
		$result = $this->fetchRow( "idmember = '$idmember'") ;
        return @$result->toArray();     // @symbol is used to avoid warning    	
    }
     
     public function editmember($id,$memberName,$subType)

    {
        $data=array( 
                    'member_name'=>$memberName,
                      'sub_type'=>$subType,
                        
                           
                          );  
     //    print_r($data);
           // echo $id;
           //   die();
      $this->update($data,'idmember = '.$id);  
    }
    /*
     * update email template 
     */
    public function fnUpdateTemplate($whereId,$formData,$editorData) {
    		unset($formData['idDefination']);
    			    				
			$this->update($formData,$whereId);
    }




} 