<?php
class Portal_Model_DbTable_Messages extends Zend_Db_Table {
	
	protected $_name = 'messages';
	protected $_attach = 'messages_attachment';

	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}

	public function getData($id, $by = 'id', $single=1)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('u' => 'student_profile'), 'u.id=a.from_id', array('CONCAT_WS(" ",u.appl_fname,u.appl_mname,u.appl_lname) as from_name','u.photo as from_photo'))
					->joinLeft(array('u2' => 'student_profile'), 'u2.id=a.to_id', array('CONCAT_WS(" ",u2.appl_fname,u2.appl_mname,u2.appl_lname) as to_name','u2.photo as to_photo'))
					->where('a.'.$by.' = ?', $id)
					->order('a.created_date ASC');
		
		if ( $single == 0 )
		{
			$select->where('a.parent_id != 0');
		}

		return $single == 1 ? $db->fetchRow($select) : $db->fetchAll($select);
	}
	
	public function getMessages($from_id=null, $to_id=null, $folder_id = 0)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('u' => 'student_profile'), 'u.id=a.from_id', array('CONCAT_WS(" ",u.appl_fname,u.appl_mname,u.appl_lname) as from_name'))
					->joinLeft(array('u2' => 'student_profile'), 'u2.id=a.to_id', array('CONCAT_WS(" ",u2.appl_fname,u2.appl_mname,u2.appl_lname) as to_name'))
					
					->where('a.folder_id = ?',$folder_id)
					->order('a.created_date DESC');
		
		if ( $from_id )
		{
			$select->where('a.to_id = ?', $from_id);
		}

		if  ($to_id)
		{
			$select->where('a.from_id = ?', $to_id);
		}
		return $select;
	}

	public function getMessagesInbox($user_id)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('u' => 'student_profile'), 'u.id=a.from_id', array('CONCAT_WS(" ",u.appl_fname,u.appl_mname,u.appl_lname) as from_name'))
					->joinLeft(array('u2' => 'student_profile'), 'u2.id=a.to_id', array('CONCAT_WS(" ",u2.appl_fname,u2.appl_mname,u2.appl_lname) as to_name'))
					->order('a.created_date DESC');
		
		$select->where('a.to_id=?',$user_id)
				->where('a.folder_id = 0');
		
		return $select;
	}
	
	public function add($data)
	{
		$db = $this->db;
		
		$db->insert($this->_name, $data );
		
		$_id = $db->lastInsertId();

		return $_id;
	}

	public function updateMsg($data, $id)
	{
		$db = $this->db;
		
		$db->update($this->_name, $data, 'id='.(int)$id );
	}

	public function updateMsgs($data, $ids)
	{
		$db = $this->db;
		
		$db->update($this->_name, $data, 'id IN ('.$ids.')' );
	}

	public function unreadParent($parent_id )
	{
		$db = $this->db;

		$db->update($this->_name, array('haveunread' => 1), 'id = '.(int) $parent_id );
	}

	public static function getUnread($user_id)
	{
		$db = getDb();
		$getTotal = $db->query("SELECT id FROM `messages` WHERE to_id='".$user_id."' AND haveunread=1 AND folder_id = 0")->rowCount();

		return $getTotal;
	}
}