<?php 
class Admin_Model_DbTable_Cpd extends Zend_Db_Table_Abstract
{
   
    protected $_name = 'tbl_cpd_material';
    protected $_primary = "cpdm_id";
    
    public function getLearningCategory($learning_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();


        $select = $db->select()
            ->from(array("a" => "tbl_cpd_default_activities"), array())
            ->join(array("b" => "tbl_definationms"), 'a.def_cpd_category_id = b.idDefinition', array("cat_id" => "b.idDefinition", "description" => "b.DefinitionCode"))
            ->where("a.def_cpd_learning_id = ?", $learning_id)
        ->group("a.def_cpd_category_id");

        $result = $db->fetchAll($select);
        return $result;

    }

    public function getLearningActivity($learning_id, $category_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();


        $select = $db->select()
            ->from(array("a" => "tbl_cpd_default_activities"), array())
            ->join(array("b" => "tbl_definationms"), 'a.def_cpd_activity_id = b.idDefinition', array("key" => "b.idDefinition", "description" => "b.DefinitionDesc"))
            ->where("a.def_cpd_learning_id = ?", $learning_id)
            ->where("a.def_cpd_category_id = ?", $category_id);

        $result = $db->fetchAll($select);
        return $result;

    }

    public function getCheckMaterialList()
    {
        $db = getDB();
        $select = $db->select()
            ->from(array("a" => "tbl_cpd_details"), array("a.*"))
             
            ->join(array('u' => 'user'), 'u.id=a.spid')
            ->join(array('m' => 'member'), 'm.idmember =u.id_member')
            ->group('a.spid')
            ->order("a.id");

        //echo $select;exit;

        $row = $db->fetchAll($select);

        return $row;
    }

}