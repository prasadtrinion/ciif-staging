<?php
class Admin_Model_DbTable_MemberBarring extends Zend_Db_Table {
	
	protected $_name = 'tbl_barred_member';
	protected $_PRIMARY = 'id_bm';

	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();
	}
	 
	public function getBarMem($id)
    {
       
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            
            ->where('a.bmem_id = ?',$id);

        $result = $db->fetchAll($select);
        return $result;
    }

} 