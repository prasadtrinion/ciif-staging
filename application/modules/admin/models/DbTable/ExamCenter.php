<?php

class Admin_Model_DbTable_ExamCenter extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_exam_center';
    protected $_primary = "ec_id";

    public function getDatabyId($id = 0)
    {
        $id = (int)$id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('ec' => $this->_name))
            ->where($this->_primary . ' = ' . $id);

        $row = $db->fetchRow($select);
        return $row;

    }

    public function getData($ec_id=0,$local=0)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('ec' => $this->_name))
            ->order('ec_name');

        if($local){
            $select->where('ec.is_local = ?',$local);
        }
        if($ec_id){
            $select->where('ec.ec_id = ?',$ec_id);
            $row = $db->fetchRow($select);
        }else{
            $row = $db->fetchAll($select);
        }

        return $row;

    }

    /*public function updateDataExamCenter($data,$id) {
        $this->update($data,"ec_id = '".$id."'");
    }

    public function updateData2($data,$id,$ec_code){

        $db = Zend_Db_Table::getDefaultAdapter();
        $where = array(
            'ec_id = ?' => $id,
            'ec_code = ?' => $ec_code
        );

        $db->update('tbl_exam_center', $data, $where);

        //$this->update($data,"ec_id ='".$id."'");
    }*/

    public function updateDefault($data, $ec_id, $ec_code)
    {


        $this->update($data, "ec_id ='$ec_id' AND ec_code = '$ec_code'");
    }


    public function getDataInfo($data = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('ec' => $this->_name))
            ->joinLeft(array('add' => 'tbl_address'), 'add.add_org_id=ec.ec_id', array())
            ->joinLeft(array('d' => 'tbl_definationms'), 'add.add_address_type=d.idDefinition', array('addressType' => 'DefinitionCode'))
            ->joinLeft(array('c' => 'tbl_countries'), 'add.add_country=c.idCountry', array('CountryName'))
            ->joinLeft(array('s' => 'tbl_state'), 'add.add_state=s.idState', array('StateName'))
            ->joinLeft(array('ct' => 'tbl_city'), 'add.add_city=ct.idCity', array('CityName'))
            ->where('ec.is_local = 1')
            ->group('ec.ec_id')
            //->order('c.idCountry')
            //->order('ct.idCity')
            ->order('ec_name');

        if (isset($data)) {

            if (isset($data['ec_name']) && $data['ec_name'] != '') {
                $select->where('ec.ec_name LIKE ?', '%' . $data['ec_name'] . '%');
            }

            if (isset($data['ec_code']) && $data['ec_code'] != '') {
                $select->where('ec.ec_code LIKE ?', '%' . $data['ec_code'] . '%');
            }

            if (isset($data['ec_country']) && $data['ec_country'] != '') {
                $select->where('add.add_country = ?', $data['ec_country']);
            }

            if (isset($data['ec_state']) && $data['ec_state'] != '') {
                $select->where('add.add_state = ?', $data['ec_state']);
            }

            if (isset($data['ec_city']) && $data['ec_city'] != '') {
                $select->where('add.add_city = ?', $data['ec_city']);
            }

            if (isset($data['is_local']) && $data['is_local'] != '') {
                $select->where('ec.is_local = ?', $data['is_local']);
            }

            if (isset($data['ec_active']) && $data['ec_active'] != '') {
                $select->where('ec.ec_active = ?', $data['ec_active']);
            }

            if (isset($data['ec_id']) && $data['ec_id'] != '') {
                $select->where('ec.ec_id = ?', $data['ec_id']);
            }

        }


        $row = $db->fetchAll($select);
        return $row;

    }

    public function addData($data)
    {
        $this->insert($data);
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->lastInsertId();
    }

    public function updateData($data, $id = 0)
    {

        if ($id) {
            $this->update($data, "ec_id ='" . $id . "'");
        } else {
            $this->update($data);
        }

    }

    public function deleteData($add_id)
    {
        $this->delete("ec_id ='" . $add_id . "'");
    }

    public function getExamCenterCountry()
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('add' => 'tbl_address'), array())
            ->join(array('c' => 'tbl_countries'), 'c.idCountry=add.add_country', array('CountryName', 'DefaultLanguage', 'idCountry'))
            ->where('add_org_name = "tbl_exam_center"')
            ->where('add_address_type = 615')
            ->group('add_country')
            ->order('idCountry');

        $row = $db->fetchAll($select);
        return $row;

    }

    public function getExamCenterCity($idCountry = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('add' => 'tbl_address'), array())
            ->join(array('c' => 'tbl_city'), 'c.idCity=add.add_city', array('CityName', 'idCity'))
            ->where('add_org_name = "tbl_exam_center"')
            ->where('add_address_type = 615')
            ->group('add_city')
            //->order('CityName')
            ->order(new Zend_Db_Expr("CASE WHEN CityName!='Others' THEN 0 ELSE 1 END"));
        if (isset($idCountry)) {
            $select->where('add_country = ?', $idCountry);
        }
        $row = $db->fetchAll($select);
        return $row;

    }

    public function getExamCenter($idCountry = null, $idCity = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('add' => 'tbl_address'), array())
            ->join(array('ec' => $this->_name), 'ec.ec_id=add.add_org_id', array('ec_id', 'ec_name', 'ec_default_language'))
            ->joinLeft(array('c' => 'tbl_countries'), 'c.idCountry=add.add_country', array('CountryName'))
            ->joinLeft(array('ct' => 'tbl_city'), 'ct.idCity=add.add_city', array('CityName'))
            ->where('add_org_name = "tbl_exam_center"')
            ->where('add_address_type = 615')
            ->order('add.add_country')
            ->order('add.add_city');

        if (isset($idCountry) && $idCountry != '') {
            $select->where('add_country = ?', $idCountry);
        }

        if (isset($idCity) && $idCity != '') {

            if ($idCity == 99) {
                //display all center
            } else {
                $select->where('add_city = ?', $idCity);
            }
        }
        //  echo $select;
        $row = $db->fetchAll($select);
        return $row;

    }

    public function getExamCenterBySemester($idSemester, $idCountry = null, $idCity = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('add' => 'tbl_address'), array())
            ->join(array('ec' => $this->_name), 'ec.ec_id=add.add_org_id', array('ec_id', 'ec_name', 'ec_default_language'))
            ->joinLeft(array('c' => 'tbl_countries'), 'c.idCountry=add.add_country', array('CountryName'))
            ->joinLeft(array('ct' => 'tbl_city'), 'ct.idCity=add.add_city', array('CityName'))
            ->where('ec.ec_id IN (SELECT ec_id FROM exam_center_setup WHERE idSemester=?)', $idSemester)
            ->where('add_org_name = "tbl_exam_center"')
            ->where('add_address_type = 615')
            ->order('add.add_country')
            ->order('add.add_city');

        if (isset($idCountry) && $idCountry != '') {
            $select->where('add_country = ?', $idCountry);
        }

        if (isset($idCountry) && $idCountry != '') {
            $select->where('add_city = ?', $idCity);
        }
        //echo $select;
        $row = $db->fetchAll($select);
        return $row;

    }

    public function getExamCenterList($data, $subject_arr = null, $search_group = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $semester_arr = $this->getEqSemesterOpen($data['IdSemester']);
        array_push($semester_arr, $data['IdSemester']);

        $select = $db->select()
            ->from(array('ec' => $this->_name))
            ->join(array('er' => 'exam_registration'), 'er.er_ec_id=ec.ec_id')
            ->join(array('sm' => 'tbl_subjectmaster'), 'sm.IdSubject=er.er_idSubject', array('SubjectName', 'SubCode', 'subjectMainDefaultLanguage'));


        if ($search_group == 'ajax') {
            //to get list exam center only
            $select->where('er.er_idSubject IN (?)', $subject_arr);
            $select->group('ec.ec_id');
        } else {
            //default search for attendance list
            $select->group('ec.ec_id');
            $select->group('er.er_idSubject');
        }


        if (isset($data['IdSemester']) && $data['IdSemester'] != '') {
            //$select->where('er.er_idSemester = ?',$data['IdSemester']);
            $select->where('er.er_idSemester IN (?)', $semester_arr);
        }

        if (isset($data['ec_id']) && $data['ec_id'] != '') {
            $select->where('ec.ec_id = ?', $data['ec_id']);
        }

        if (isset($data['ec_name']) && $data['ec_name'] != '') {
            $select->where('ec.ec_name LIKE ?', '%' . $data['ec_name'] . '%');
        }

        if (isset($data['subject_code']) && $data['subject_code'] != '') {
            $select->where('sm.SubCode LIKE ?', '%' . $data['subject_code'] . '%');
        }


        $row = $db->fetchAll($select);
        return $row;

    }

    public function getEqSemesterOpen($idSemester)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $semesterDB = new Admin_Model_DbTable_Semestermaster();
        $semester = $semesterDB->getData($idSemester);

        $sql = $db->select()
            ->from(array('sm' => 'tbl_semestermaster'), array('IdSemesterMaster'))
            ->where('sm.IdScheme = ?', 12)
            ->where('sm.AcademicYear = ?', $semester['AcademicYear'])
            ->where('sm.sem_seq = ?', $semester['sem_seq']);
        $result = $db->fetchAll($sql);

        return $result;

    }

    public function searchSetupExamCenter($data = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('ec' => $this->_name))
            ->join(array('add' => 'tbl_address'), 'add.add_org_id=ec.ec_id')
            ->join(array('d' => 'tbl_definationms'), 'add.add_address_type=d.idDefinition', array('addressType' => 'DefinitionCode'))
            ->join(array('c' => 'tbl_countries'), 'add.add_country=c.idCountry', array('CountryName', 'idCountry'))
            ->joinLeft(array('s' => 'tbl_state'), 'add.add_state=s.idState', array('StateName'))
            ->joinLeft(array('ct' => 'tbl_city'), 'add.add_city=ct.idCity', array('CityName'))
            ->where('add.add_org_name = "tbl_exam_center"')
            ->group('ec.ec_id')
            ->order('c.idCountry')
            ->order('ct.idCity')
            ->order('ec_name');

        if (isset($data['idCountry']) && $data['idCountry'] != '') {
            $select->where('add.add_country = ?', $data['idCountry']);
        }

        if (isset($data['idCity']) && $data['idCity'] != '') {
            $select->where('add.add_city = ?', $data['idCity']);
        }

        if (isset($data['status'])) {
            if ($data['status'] == 1) {
                $select->where('ec_id NOT IN (SELECT ec_id FROM exam_center_setup WHERE idSemester = ?)', $data['idSemester']);
            }
            if ($data['status'] == 2) {
                //$select->where('ec_id IN (SELECT ec_id FROM exam_center_setup WHERE idSemester = ?)',$data['idSemester']);
                $select->join(array('ecs' => 'exam_center_setup'), 'ecs.ec_id=ec.ec_id')->where('ecs.idSemester = ?', $data['idSemester']);
            }
        }

        $row = $db->fetchAll($select);
        return $row;

    }


    public function getExamCenterById($id)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('add' => 'tbl_address'))
            ->join(array('ec' => $this->_name), 'ec.ec_id=add.add_org_id', array('ec_id', 'ec_name', 'ec_default_language'))
            ->joinLeft(array('c' => 'tbl_countries'), 'c.idCountry=add.add_country', array('CountryName'))
            ->joinLeft(array('ct' => 'tbl_city'), 'ct.idCity=add.add_city', array('CityName'))
            ->where('add_org_name = "tbl_exam_center"')
            ->where('add_address_type = 615')
            ->where('ec_id = ?', $id);

        //echo $select;
        $row = $db->fetchRow($select);
        return $row;

    }


    public function getcity($country_id = 0)
    {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('ec' => 'tbl_exam_center'), array('ec_id', 'ec_name'))
            ->join(array('add' => 'tbl_address'), 'add.add_org_id=ec.ec_id', array('add.add_city', 'add.add_city_others'))
            ->joinLeft(array('c' => 'tbl_countries'), 'add.add_country=c.idCountry', array('CountryName'))
            ->joinLeft(array('s' => 'tbl_state'), 'add.add_state=s.idState', array('StateName'))
            ->joinLeft(array('ct' => 'tbl_city'), 'add.add_city=ct.idCity', array('CityName', 'idCity'))
            ->where('add.add_country = ?', $country_id)
            ->where('add.add_org_name = ?', 'tbl_exam_center')
            ->group('add.add_city');

        $stmt = $db->query($select);
        $rows = $stmt->fetchAll();

        $cities = array();

        foreach ($rows as $row) {
            $cities[] = array('city' => $row['CityName'], 'key' => $row['idCity']);
        }

        //others
        $select2 = $db->select()
            ->from(array('ec' => 'tbl_exam_center'), array('ec_id', 'ec_name'))
            ->join(array('add' => 'tbl_address'), 'add.add_org_id=ec.ec_id', array('add.add_city', 'add.add_city_others as CityName'))
            ->joinLeft(array('c' => 'tbl_countries'), 'add.add_country=c.idCountry', array('CountryName'))
            ->joinLeft(array('s' => 'tbl_state'), 'add.add_state=s.idState', array('StateName'))
            ->where('add.add_country = ?', $country_id)
            ->where('add.add_city = ?', 99)
            ->where('add.add_org_name = ?', 'tbl_exam_center')
            ->group('add.add_city_others');

        $stmt2 = $db->query($select2);
        $rows2 = $stmt2->fetchAll();

        foreach ($rows2 as $row) {
            $cities[] = array('city' => $row['CityName'], 'key' => $row['CityName']);
        }


        //real others
        $others = array(array('city' => 'Others', 'key' => 'Others'));

        $cities = array_merge($cities, $others);

        return $cities;
    }


    public function getExamCenterByCountryCity($idSemester, $idCountry, $idCity, $idCityOthers = null)
    {

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('add' => 'tbl_address'))
            ->join(array('ec' => $this->_name), 'ec.ec_id=add.add_org_id', array('ec_id', 'ec_name', 'ec_default_language'))
            ->joinLeft(array('c' => 'tbl_countries'), 'c.idCountry=add.add_country', array('CountryName'))
            ->joinLeft(array('ct' => 'tbl_city'), 'ct.idCity=add.add_city', array('CityName'))
            ->join(array('ecs' => 'exam_center_setup'), 'ecs.ec_id=ec.ec_id')
            ->where('add_org_name = "tbl_exam_center"')
            ->where('add_address_type = 615')
            ->where('ecs.idSemester = ?', $idSemester)
            ->where('add.add_country = ?', $idCountry)
            ->where('add.add_city = ?', $idCity);

        if (($idCity == 99) && (isset($idCityOthers) && $idCityOthers != '')) {
            $select->where('add.add_city_others = ?', $idCityOthers);
        }

        $row = $db->fetchAll($select);
        return $row;

    }

    public function getDataByUsername($username)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('ec' => $this->_name))
            ->where('ec.ec_invigilatorusername = ?', $username);
        $row = $db->fetchRow($query);
        return $row;
    }

    public function getRoomCenter()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.ec_active = ?', 1)
            ->order('a.ec_name');
        $row = $db->fetchAll($query);

        $data = array();

        $i=0;
        foreach($row as $key=>$room){
            $query = $db->select()
                ->from(array('a' => 'tbl_examroom'))
                ->where('a.status = ?', 1)
                ->where('a.examcenter_id = ?', $room['ec_id'])
                ->order('a.name');
            $rowRoom = $db->fetchAll($query);

            if($rowRoom) {
                $data[$i] = $room;
                $data[$i]['totalroom'] = count($rowRoom);
                $data[$i]['room'] = $rowRoom;
                $i++;
            }


        }
        return $data;
    }

    public function getOptionList()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array($this->_name))
            ->order(array('ec_name ASC'));

        $result = $db->fetchAll($query);
        $list = array();

        foreach ($result as $data) {
            $list[$data['ec_id']] = $data['ec_name'];
        }

        return $list;
    }

    public function getDataByExam($pe_id, $category_id = 1)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => $this->_name), array('ec_id', 'ec_name'))
            ->join(array('b' => 'exam_schedule'), 'b.es_exam_center = a.ec_id', array())
            ->join(array('c' => 'exam_setup'), 'c.es_id = b.es_examsetup_id', array())
            ->join(array('d' => 'exam_setup_detail'), 'd.esd_id = b.es_esd_id', array())
            ->joinLeft(array('z' => 'tbl_programclosingdate'), 'z.IdProgram = c.es_idProgram AND z.registration_type = 1 AND z.category_id = ' . $category_id, array())
            ->where('CURDATE() < DATE_SUB(b.es_date, INTERVAL CONVERT(COALESCE(NULLIF(z.days, 0), ' . DEFAULT_EXAM_CLOSING_DATE . '), SIGNED INTEGER) DAY)')
            ->group('a.ec_id');

        $result = $db->fetchAll($query);
        return $result;
    }

    public function getCenterByExamDate($date_from, $date_to = NULL,$ec_type=null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        if (!$date_to){
            $date_to = date('Y-m-d');
        }

        $query = $db->select()
            ->from(array('a' => $this->_name), array('ec_id', 'ec_name'))
            ->join(array('b' => 'exam_taggingslotcenterroom'), 'b.tsc_examcenterid = a.ec_id', array())
            ->join(array('c' => 'exam_slot'), 'c.sl_id = b.tsc_slotid', array())
            ->join(array('d' => 'exam_scheduletaggingslot'), 'd.sts_slot_id = c.sl_id', array())
            ->join(array('e' => 'exam_schedule'), 'e.es_id = d.sts_schedule_id', array())
            ->where('e.es_date >= ?',$date_from)
            ->where('e.es_date <= ?',$date_to)
            ->group('a.ec_id')
            ->order('a.ec_name');

            if(isset($ec_type)&&$ec_type!=''){
            	$learning_center = ($ec_type==2) ? 0:1;
            	$query->where('a.is_learning_center=?',$learning_center);
            }
          
        $result = $db->fetchAll($query);
        return $result;
    }
}

?>