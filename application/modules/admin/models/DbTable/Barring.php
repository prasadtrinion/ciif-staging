<?php
class Admin_Model_DbTable_Barring extends Zend_Db_Table {
	
	protected $_name = 'tbl_barring';
	protected $_PRIMARY = 'id_barring';

	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
		$this->auth = Zend_Auth::getInstance();
	}
	
	public function getmembertype()
	{
		$db = $this->getDefaultAdapter();

		$select = $db->select()
			->from(array($this->_name));

		$result = $db->fetchAll( $select );

		// print_r($result);
		// die();

		return $result;
	}


	 public function getListingData($table ,$where="",$order="", $id=0)
    {
        // $db     = getDB();
        $select = $this->select()
            ->from(array('a' => $table));
            

        if($where !=""){
            $select ->where($where);
        }
        if($order !=""){
            $select ->order($order);
        }

        $result = $this->fetchAll($select);
        return $result;
    }

	  public function addData($barring, $amount)
    {
    		

    		$data = array (	
    						'barring_name'=>$barring,
                      'amount'=>$amount,
                      'created_by'=> $this->auth->getIdentity()->id,
                      'created_date' => date("Y-m-d h:i:s"),
                      'status' => 1);
    		

    		$this->insert($data);
    }
    public function fnGetTemplateDetails() {   			    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->from(array('a' => 'tbl_barring'));			
				
			$result = $this->fetchAll($select);		
			return $result->toArray(); 		    
   	}

   	/*
   	 * search by criteria   
   	 */
	public function fnSearchTemplate($post = array()) {		
		
		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
							
							->join(array('a' => 'tbl_barring'))														
							->where('a.barring_name like "%" ? "%"',$post['barring_name']);
							
													
			$result = $db->fetchAll($select);
			return $result;			
	}
	
	/*
	 * Add Email Template
	 */
	public function fnAddEmailTemplate($post,$editorData) {
		
		$post['TemplateBody'] = $editorData;			
		$this->insert($post);
	}
	
	/*
	 * get single email template row values bu $id
	 */
    public function fnViewTemplte($idmember) {
    			
		$result = $this->fetchRow( "idmember = '$idmember'") ;
        return @$result->toArray();     // @symbol is used to avoid warning    	
    }
     
     public function editmember($id,$barring,$amount)

    {
        $data=array( 
                    'barring_name'=>$barring,
                      'amount'=>$amount,
                        'updated_by'=> $this->auth->getIdentity()->id,
                      'updated_date' => date("Y-m-d h:i:s")
                            );  
     //    print_r($data);
           // echo $id;
           //   die();
      $this->update($data,'id_barring = '.$id);  
    }
   
    




} 