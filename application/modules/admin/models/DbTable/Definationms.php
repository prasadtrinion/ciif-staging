<?php 
class Admin_Model_DbTable_Definationms extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_definationms';
	protected $_primary = "idDefinition";
	
	public function getData($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('d'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getDataByType($idType=0){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('d'=>$this->_name) ) 
	                ->where("d.idDefType = '".$idType."'")
					->where("d.Status = ?", 1)
					->order('d.defOrder ASC');
						                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getDataByTypeDocumentType( $idScheme, $studentcategory){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('d'=>$this->_name) ) 
	                 ->joinLeft(array('a' => 'tbl_documentchecklist_dcl'), 'a.dcl_uplType = d.idDefinition', array(''))
	                ->where("d.idDefType = 10")
					->where("d.Status = ?", 1)
					->where("a.dcl_finance = ?", 1)
					->where("a.dcl_programScheme = ?",$idScheme)
					->where("a.dcl_stdCtgy = ?", $studentcategory)
					->order('d.defOrder ASC');
						                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}

    public function getByCode($definition) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('Definitions'=>$this->_name) )
            ->joinLeft(array('DefinitionType' => 'tbl_definationtypems'), 'DefinitionType.idDefType = Definitions.idDefType', array('BahasaIndonesiaType' => 'DefinitionType.BahasaIndonesia'))
            ->where("DefinitionType.defTypeDesc = '".$definition."'")
            ->where("Definitions.status = ?", 1)
			->order('Definitions.defOrder ASC');

        $row = $db->fetchAll($select);
        return $row;
    }

	/*
	* getIdByDefType('DefCode','DefTypeName');
	*/
	public function getIdByDefType($defTypeDesc, $DefinitionCode){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $db->select()
						->from(array('a' => 'tbl_definationtypems'),array(''))
						->join(array('b' => 'tbl_definationms'), " a.idDefType = b.idDefType "  ,array('key' => 'b.idDefinition','value' => 'b.DefinitionCode'))
						->where("a.defTypeDesc= ?",$defTypeDesc)
						->where("b.DefinitionCode= ?",$DefinitionCode)
						->order('b.defOrder ASC');
		$larrResult = $db->fetchRow($lstrSelect);
		
		return $larrResult;
	}

    public function getByCodeAsList($definition) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $db->select()
            ->from(array('Definitions'=>$this->_name),null, array())
            ->joinLeft(array('DefinitionType' => 'tbl_definationtypems'), 'DefinitionType.idDefType = Definitions.idDefType',array("key"=>"Definitions.idDefinition","value"=>"Definitions.DefinitionDesc") )
            ->where("DefinitionType.defTypeDesc = ?",$definition)
            ->where("Definitions.Status = ?", 1)
			->order('Definitions.defOrder ASC');
        $result = $db->fetchAll($lstrSelect);
        return $result;

    }
    
	public function getDataByDefCode($type,$code){
			
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('d'=>$this->_name) ) 
	                ->where('d.DefinitionCode = ?',$code)
	                ->where('d.idDefType = ?',$type);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}

    public function getIdByDefCode($type,$code){
            
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select = $db->select()
                    ->from(array('d'=>$this->_name) ) 
                    ->where('d.DefinitionCode = ?',$code)
                    ->where('d.idDefType = ?',$type);                                
        
        $row = $db->fetchRow($select);

        if(!$row) {
            return null;
        }

        return $row['idDefinition'];
    }

    public function getIdByDefDesc($type, $desc){
            
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select = $db->select()
            ->from(array('d'=>$this->_name)) 
            ->where('d.DefinitionDesc = ?',$desc)
            ->where('d.idDefType = ?',$type);                                

        $row = $db->fetchRow($select);

        if(!$row) {
            return null;
        }
        
        return $row['idDefinition'];
    }

    public function getCodeById($idDefinition) {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select = $db->select()
            ->from(array('d'=>$this->_name)) 
            ->where('d.idDefinition = ?',$idDefinition);                            

        $row = $db->fetchRow($select);

        if(!$row) {
            return null;
        }
        
        return $row['DefinitionCode'];
    }

    public function getIdbyType($type, $desc){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('d'=>$this->_name))
            ->joinLeft(array('a' => 'tbl_definationtypems'), 'a.idDefType = d.idDefType', array('*'))
            ->where('d.DefinitionDesc = ?',$desc)
            ->where('a.defTypeDesc = ?',$type);

        $row = $db->fetchRow($select);

        if(!$row) {
            return null;
        }

        return $row['idDefinition'];
    }
    public function getDataByParam($table, $column, $params)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('mr'=>$table), array('mr.*', "num"=>"COUNT(*)"))
            ->join(array('sp'=>'student_profile'),'sp.std_id=mr.mr_sp_id')
            ->join(array('m'=>'tbl_membership'),'m.m_id = mr.mr_membershipId',array('MembershipName'=>'m_name','m_id'))
            ->joinLeft(array('u'=>'tbl_user'),'u.iduser=mr.mr_approve_by',array('approve_by'=>'fName'))
            ->where("$column = ?", $params);

        //echo $select;
        $row = $db->fetchRow($select);

        return $row['num'];
    }
    public function getDataByParamIn($table, $column, $params)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('mr'=>$table), array('mr.*', "num"=>"COUNT(*)"))
            ->join(array('sp'=>'student_profile'),'sp.std_id=mr.mr_sp_id')
            ->join(array('m'=>'tbl_membership'),'m.m_id = mr.mr_membershipId',array('MembershipName'=>'m_name','m_id'))
            ->joinLeft(array('u'=>'tbl_user'),'u.iduser=mr.mr_approve_by',array('approve_by'=>'fName'))
            ->where("$column IN ($params)");

        //echo $select;exit;
        $row = $db->fetchRow($select);

        return $row['num'];
    }

    public function getDataByParams($table, $column, $params)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array($table), array('*'))
            ->where("$column = ?", $params);

        //echo $select;exit;
        $row = $db->fetchAll($select);

        return $row;
    }

    public function getDataByParamsInLimitOrder($table, $column, $params, $limit, $order_column, $order)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array($table), array('*'))
            ->where("category_id = ?", 3)
            ->where("$column IN ($params)")
            ->where("$order_column != ''")
            ->order("$order_column $order")
        ->limit($limit);

       // echo $select;//exit;
        $row = $db->fetchAll($select);

        return $row;
    }

    public function getAppTransaction($appl_id=0,$status=0,$status2='1602'){

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from( array('a'=>'applicant_transaction'),array('num'=>'COUNT(*)'))
            ->join( array('b'=>'tbl_studentregistration'), 'b.IdTransaction = a.at_trans_id')
            ->join(array('c'=>'applicant_program'),'c.ap_at_trans_id = a.at_trans_id')
            ->join(array('d'=>'tbl_definationms'),'d.idDefinition = b.TransactionStatus',array('applicationstatus'=>'d.DefinitionDesc'))
            ->join(array('e'=>'tbl_program'),'e.IdProgram = c.ap_prog_id',array('e.ProgramName'))
            ->join(array('f'=>'student_profile'),'f.std_id = a.at_appl_id',array('fullname'=>'f.std_fullname','std_username','std_idnumber','std_email'))
            ->join(array('g'=>'tbl_registration_type'),'g.rt_id = b.IdRegistrationType',array('rt_activity'))
            ->joinLeft(array('h'=>'membership_registration'),'h.mr_transaction_id = a.at_trans_id',array('mr_membershipId'))
            ->joinLeft(array('i'=>'tbl_membership'),'i.m_id = h.mr_membershipId',array('membershipname'=>'m_name'))
            ->where("a.at_appl_type != 1")
            ->order("a.at_trans_id desc");

        if($status != 0){
            $select->where("a.at_status = ?",$status);
            $select->where("h.mr_status != ?",$status2);
            $result = $db->fetchRow($select);
            return $result['num'];
        }else{
            $result = $db->fetchAll($select);
        }
echo $select."<br>";
        return $result;
    }

    public function getStudentsByMemberBank()
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'))
            ->join(array('b'=>'student_profile'),'b.std_id=a.sp_id', array('count(employer_id) as CountEmployer','employer_id'))
            ->join(array('m'=>'membership_registration'),'b.std_id=m.mr_sp_id', array())
            ->where("b.employer_id != ''")
            ->group('b.employer_id')
            ->order('CountEmployer desc')
            ->limit(10);

       // echo $select;exit;
        $row = $db->fetchAll($select);
        return $row;
    }
}
?>