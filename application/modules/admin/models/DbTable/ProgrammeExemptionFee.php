<?php

class Admin_Model_DbTable_ProgrammeExemptionFee extends Zend_Db_Table
{

    protected $_name = 'tbl_program_exemptionfee';
    protected $_primary = 'id_program';
    private static $tree = array();
    private static $html = '';

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
    }
 
    public function getfee(){
          $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_program_category'),'a.id = u.program_category')
            ->join(array('b' => 'tbl_definationms'),'b.idDefinition = u.program_route', array('level'=>'DefinitionDesc'))
            ->join(array('c' => 'tbl_definationms'),'c.idDefinition = u.program_level', array('route'=>'DefinitionDesc'));
            
           
           $result = $db->fetchAll($select);
          
            return $result;
    }
    public function get_fee_search($formdata){
          $db = $this->getDefaultAdapter();
       
        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_program_category'),'a.id = u.program_category')
            ->join(array('b' => 'tbl_definationms'),'b.idDefinition = u.program_route', array('level'=>'DefinitionDesc'))
            ->join(array('c' => 'tbl_definationms'),'c.idDefinition = u.program_level', array('route'=>'DefinitionDesc'));
         


            if (isset($formdata['program_route']) && $formdata['program_route']!=0){
              
            $select->where(('u.program_route ='.$formdata['program_route']));
                
            }
            if (isset($formdata['program_category']) && $formdata['program_category']!=0){
 
            $select->where(('u.program_category ='.$formdata['program_category']));
                
            }
            if (isset($formdata['program_level']) && $formdata['program_level']!=0){

            $select->where(('u.program_level ='.$formdata['program_level']));
                
            }
            
           
           $result = $db->fetchAll($select);
           // echo  '<pre>';
           // print_r($result);
           // die();  
            return $result;
    }
    public static function printNav($tree, $r = 0, $p = null )
    {
        foreach ($tree as $i => $t)
        {

            if ($t['parent_id'] == $p) {
                // reset $r
                $r = 0;
            }

            self::$html .= '<li '.(isset($t['_children'])?' class="uk-parent"':'').'><a href="#">'.$t['name'].'</a>';

            if (isset($t['_children'])) {
                self::$html .= '<ul class="uk-nav-sub">';
                self::printNav($t['_children'], ++$r, $t['parent_id']);
                self::$html .= '</ul>'."\n";
            }

            self::$html .= '</li>'."\n";
        }

        return self::$html;
    }

    public static function printTree($tree, $r = 0, $p = 0, $printdash = true) {

        foreach ($tree as $i => $t) {

            $dash = $printdash == true ? (($t['program'] == 0) ? '' : str_repeat('-', $r) .' ') : '';

            if ($t['program'] == $p) {
                // reset $r
                $r = 0;
            }

            self::$tree[] = array_merge( $t, array('name' => $dash.$t['name'], '_children' => '', 'depth' => $r) );


            if (isset($t['_children']) && !empty($t['_children'])) {
                self::printTree($t['_children'], ++$r, $t['parent_id'], $printdash);
            }
        }

        return self::$tree;
    }

    public static function buildTree(Array $data, $parent = 0) {
        $tree = array();
        foreach ($data as $d) {
            if ($d['program'] == $parent) {
                $children = self::buildTree($data, $d['id_program']);
                // set a trivial key
                if (!empty($children)) {
                    $d['_children'] = $children;
                }
                $tree[] = $d;
            }
        }
        return $tree;
    }
}
