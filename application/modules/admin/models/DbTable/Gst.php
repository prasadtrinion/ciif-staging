<?php
class Admin_Model_DbTable_Gst extends Zend_Db_Table {
	
	protected $_name = 'tbl_gst';
	protected $_PRIMARY = 'id_gst';

	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}
	
	public function getmembertype()
	{
		$db = $this->getDefaultAdapter();

		$select = $db->select()
			->from(array($this->_name));

		$result = $db->fetchAll( $select );

		// print_r($result);
		// die();

		return $result;
	}


	 public function getListingData($table ,$where="",$order="", $id=0)
    {
        // $db     = getDB();
        $select = $this->select()
            ->from(array('a' => $table));
            

        if($where !=""){
            $select ->where($where);
        }
        if($order !=""){
            $select ->order($order);
        }

        $result = $this->fetchAll($select);
        return $result;
    }

	  public function addData($member_name, $sub_type,$percentage)
    {
    		

    		$data = array (	
    						'name'=>$member_name,
    						'description' => $sub_type,
    						'percentage' => $percentage
    						
    						
    						);

    		$this->insert($data);
    }
    public function fnGetTemplateDetails() {   			    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->join(array('a' => 'tbl_gst'),array('id_gst'));			
				
			$result = $this->fetchAll($select);

			return $result->toArray(); 		    
   	}

   	/*
   	 * search by criteria   
   	 */
	public function fnSearchTemplate($post = array()) {		
	
		
		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
							
							->from(array('a' => 'tbl_gst'))														
							->where('a.name like "%" ? "%"',$post['field2']);
							
													
			$result = $db->fetchAll($select);
				
			return $result;			
	}
	
	
	
     
     public function editmember($id,$memberName,$subType,$percentage)

    {
        $data=array( 
                    'name'=>$memberName,
                      'description'=>$subType,
                      'percentage' => $percentage
                        
                           
                          );  
     //    print_r($data);
           // echo $id;
           //   die();
      $this->update($data,'id_gst = '.$id);  
    }
   


} 