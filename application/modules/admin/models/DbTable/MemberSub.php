<?php
class Admin_Model_DbTable_MemberSub extends Zend_Db_Table {
	
	protected $_name = 'tbl_member_subtype';
	protected $_PRIMARY = '  id_subtype'; // table name
	
	/*
	 * get all active email templates rows 
	 */

     public function getMemberData($name){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
          
            ->where('member_subtype_name like ?',"%".$name."%");
           
           $result = $db->fetchRow($select); 
           
            return $result;
 
    }
   
     

     public function getDefination($deftypeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $deftypeid);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function addData($dropdown_member,$sub_type)
    {
    		

    		$data = array (	
    						
    						
                'id_member' => $dropdown_member,
                'member_subtype_name' => $sub_type,
    						
    						);
    		// print_r($data);
    		// die('data from model');
    		$this->insert($data);
    }

    public function search($id)
    {
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()                         
                      ->from(array('a' => 'tbl_member_subtype'),array('a.*'))    
                      ->join(array('b'=>'member'),'a.id_member = b.idmember') 
                      ->where('a.id_member = ' . $id);
         $select = $lobjDbAdpt->fetchAll($lstrSelect);
      // $select = $this->select();
      // $result =  $this->fetchAll($select);
      if($select)
      {

        
        return $select;
      }
      return false;
      // echo "<pre>";
      // print_r($result);
      // die();
    }
    public function getsearch()
    {
        
        
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()                         
                        ->from(array('a' => 'tbl_member_subtype'),array('a.*'))    
                        ->join(array('b'=>'member'),'a.id_member = b.idmember') 
                        ;  
                        
        $Result = $lobjDbAdpt->fetchAll($lstrSelect);

        return $Result;
    print_r($Result);
    }

    public function getfees()
    {
        
        
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()                         
                        ->from(array('a' => 'tbl_member_fee_structure'),array('a.*'))    
                        
                        
                        ->join(array('f'=>'tbl_definationms'),'a.id_fees =f.idDefinition');  
                        
        $Result = $lobjDbAdpt->fetchAll($lstrSelect);

        return $Result;
    print_r($Result);
    }

    public function fnGetTemplateDetails() {   			    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->join(array('a' => 'member'),array('idmember'));			
				
			$result = $this->fetchAll($select);		
			return $result->toArray(); 		    
   	}

   	/*
   	 * search by criteria   
   	 */
	public function fnSearchTemplate($post = array()) {		
		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $this->select()
							->setIntegrityCheck(false)  	
							->join(array('a' => 'member'),array('idTemplate', 'member_name','sub_type'))														
							->where('a.member_name like "%" ? "%"',$post['field3'])
							->where('a.sub_type like "%" ? "%"',$post['field2']);
													
			$result = $this->fetchAll($select);
			return $result->toArray();			
	}
	
	/*
	 * Add Email Template
	 */
	public function fnAddEmailTemplate($post,$editorData) {
		
		$post['TemplateBody'] = $editorData;			
		$this->insert($post);
	}
	
	/*
	 * get single email template row values bu $id
	 */
    public function fnViewTemplte($idmember) {
    			
		$result = $this->fetchRow( "idmember = '$idmember'") ;
        return @$result->toArray();     // @symbol is used to avoid warning    	
    }
     
     public function editmember($id,$dropdown_member,$sub_type)

    {

      
                    
                
                
        $data = array ( 
                
                
                'id_member' => $dropdown_member,
                'member_subtype_name' => $sub_type,
                
                );
                           
        // print_r($data);
        //    echo $id;
        //      die();
      $this->update($data,'id_subtype = '.$id);  
    }
    /*
     * update email template 
     */
    public function fnUpdateTemplate($whereId,$formData,$editorData) {
    		unset($formData['idDefination']);
    			    				
			$this->update($formData,$whereId);
    }
    	
}
