<?php
class Admin_Model_DbTable_CpdMaterial extends Zend_Db_Table {

    protected $_name = 'tbl_cpd_material';
    protected $_primary = "cpdm_id";
//    protected $_secondary = "spid";

    public function addData($data){
        $this->insert($data);
        $db = Zend_Db_Table::getDefaultAdapter();
        $id = $db->lastInsertId();
        return $id;
    }

    public function getMaterialList($formData=null)
    {
       
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a"=>"tbl_cpd_material"),array("a.*"))
            ->join(array('b' => 'tbl_definationms'), "b.idDefinition = a. cpdm_type"  ,array('mattype' => 'b.DefinitionCode'))
            ->join(array('c' => 'tbl_definationms'), "c.idDefinition = a. cpdm_category"  ,array('matcategory' => 'c.DefinitionCode'))
           // ->where('a.spid = ?',$spid)
            ->order("a.cpdm_id");

          if(isset($formData)){

             if (isset($formData['type']) && $formData['type']!=''){
            
               $select->where('a.cpdm_type = '. $formData['type']);
            }
            if (isset($formData['category']) && $formData['category']!=''){
            
                $select->where('a.cpdm_category = '. $formData['category']);
            }

          }
          $row = $db->fetchAll($select);
        return $row;
    }


    public function MaterialList($formData=null)
    {
       $date = date("Y-m-d H:i:s");
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a"=>"tbl_cpd_material"),array("a.*"))
            ->join(array('b' => 'tbl_definationms'), "b.idDefinition = a. cpdm_type"  ,array('mattype' => 'b.DefinitionCode'))
            ->join(array('c' => 'tbl_definationms'), "c.idDefinition = a. cpdm_category"  ,array('matcategory' => 'c.DefinitionCode'))
           ->where('a.date_started <= ?',$date)
           ->where('a.date_completion >= ?',$date)
            ->order("a.cpdm_id");

          if(isset($formData)){

             if (isset($formData['type']) && $formData['type']!=''){
            
               $select->where('a.cpdm_type = '. $formData['type']);
            }
            if (isset($formData['category']) && $formData['category']!=''){
            
                $select->where('a.cpdm_category = '. $formData['category']);
            }

          }
   
          $row = $db->fetchAll($select);
        
        return $row;
    }

    public function eventList($formData=null)
    {
       $date = date("Y-m-d H:i:s");
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a"=>"calendar_event"),array("a.*"))
            
           ->where('a.date_start <= ?',$date)
           ->where('a.date_end >= ?',$date);

          
          $row = $db->fetchAll($select);
        
        return $row;
    }

    public function getDataById($id='')
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a"=>"tbl_cpd_details"),array("a.*"))
            ->where('a.id = ?',$id)
            ->order("a.id");

        $row = $db->fetchRow($select);
        return $row;
    }

    public function updateData($data,$id) {
        unset ( $data ['Save'] );
        $where = 'cpdm_id = '.$id;
        $this->update($data,$where);
    }

    public function getDataByCpid($id='')
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a"=>"tbl_cpd_doc"),array("a.*"))
            ->where('a.cpd_id = ?',$id)
            ->order("a.doc_id");

        $row = $db->fetchAll($select);
        return $row;
    }
    public function getOneProjectDocument($document_id){

        $document_id = (int) $document_id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from('tbl_cpd_doc')
            ->where('tbl_cpd_doc'.".".'doc_id'.' = ' .$document_id);

        $stmt = $db->query($select);

        $row = $stmt->fetch();
        return $row;
    }

}