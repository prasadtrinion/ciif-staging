<?php
class Admin_Form_MemberFeeEdit extends Zend_Dojo_Form {
    public function init() {        
        $gstrtranslate =Zend_Registry::get('Zend_Translate'); 
        $strSystemDate = date('Y-m-d H:i:s');
        
       

                    
       
         $dropdown_member = $this->createElement('select','id_member');
        $dropdown_member->setAttrib('required',true)
                            ->setAttrib('id','member')    
                         ->setAttrib('class','form-control')
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper");
        $dropdown_member
                        ->addMultiOptions(array('' => '--Select Member--' ));
                      

           $dataDb = new Admin_Model_DbTable_Member();
        $appcategoryList = $dataDb->getListingData('member');  
          
            foreach ($appcategoryList as $key => $value) {
           
                $dropdown_member->addMultiOption($value['idmember'],$value['member_name']);
                
            


            }  
                        

        $effective_date = new Zend_Form_Element_Text('effective_date');
            $effective_date-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
                    ->setAttrib('class','form-control')
                     ->setAttrib('id','effective_date')
                    
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')                   
                    ->removeDecorator("Label") 
                    ->removeDecorator("DtDdWrapper")
                    ->removeDecorator('HtmlTag');

         $dropdown_fees = $this->createElement('select','id_fees');
        $dropdown_fees->setAttrib('required',true)
                            ->setAttrib('id','degree')    
                         ->setAttrib('class','form-control')
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper");
        $dropdown_fees
                        ->addMultiOptions(array('' => '--Select Fee Type --' ));

             $this->addElement('checkbox', 'active', array(
            'required'   => false,
            'class'      => 'uk-checkbox-toggle',
            'value'       => 1
        ));
        $this->addElement('text', 'calculated_amount', array(
            'required'   => true,
            'class' => 'form-control',
            'id'    => 'calculated_amount',

            'filters'    => array('StringTrim'),
        ));

           $gst = $this->createElement('select','gst');
        $gst->setAttrib('required',true)
                            ->setAttrib('id','gst')    
                         ->setAttrib('class','form-control')
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper");
        $gst
                        ->addMultiOptions(array('' => '--Select --' ));
                      


 $getGst = new Admin_Model_DbTable_Gst();
         $appcategoryList = $getGst->fetchAll();  
       
        if ($appcategoryList){
            foreach($appcategoryList as $reasonLoop){
                  $gst->addMultiOption($reasonLoop['percentage'], $reasonLoop['name']);
            }
        }


                      


 $dataDb = new Admin_Model_DbTable_MemberExperience(); 
         $appcategoryList = $dataDb->getDefination(190);  
       
        if ($appcategoryList){
            foreach($appcategoryList as $reasonLoop){
                  $dropdown_fees->addMultiOption($reasonLoop['idDefinition'], $reasonLoop['DefinitionDesc']);
            }
        }
       
       $renewal_year = $this->createElement('select','renewal_year');
        $renewal_year->setAttrib('required',true)
                            ->setAttrib('id','renewal_year')    
                         ->setAttrib('class','form-control')
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper");
        $renewal_year
                        ->addMultiOptions(array('' => '--Select Renewal Year--' ));
                      

        $definationDB = new Admin_Model_DbTable_Definition();
        $yearLists = $definationDB->getByCode('Renewal Year');

            foreach ($yearLists as $key => $value) {
              
            $renewal_year->addMultiOption($value['idDefinition'],$value['DefinitionDesc']);
              
            }
        $dropdown_degree = $this->createElement('select','id_degree');
        $dropdown_degree->setAttrib('required',true)
                            ->setAttrib('id','degree')    
                         ->setAttrib('class','form-control')
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper");
        $dropdown_degree
                        ->addMultiOptions(array('' => '--Select Degree--' ));
                      


 $dataDb = new Admin_Model_DbTable_MemberExperience(); 
         $appcategoryList = $dataDb->getDefination(191);  
       
        if ($appcategoryList){
            foreach($appcategoryList as $reasonLoop){
                  $dropdown_degree->addMultiOption($reasonLoop['idDefinition'], $reasonLoop['DefinitionDesc']);
            }
        }
        
        $dropdown_currency = $this->createElement('select','id_currency');
        $dropdown_currency->setAttrib('required',true)
                            ->setAttrib('id','degree')    
                         ->setAttrib('class','form-control')
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper");
        $dropdown_currency
                        ->addMultiOptions(array('' => '--Select Curency--' ));
                      


 $dataDb = new Admin_Model_DbTable_MemberExperience(); 
         $appcategoryList = $dataDb->getDefination(194);  
       
        if ($appcategoryList){
            foreach($appcategoryList as $reasonLoop){
                  $dropdown_currency->addMultiOption($reasonLoop['idDefinition'], $reasonLoop['DefinitionDesc']);
            }
        }
             




              $ddl_nationality = $this->createElement('select','nationality');
        $ddl_nationality->setAttrib('required',true)
                            ->setAttrib('id','nationality')   
                         ->setAttrib('class','form-control')
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper");
        $ddl_nationality
                        ->addMultiOptions(array('' => '--Select Nationality--' ));


        $dataDb = new Admin_Model_DbTable_MemberExperience();
         $appcategoryList = $dataDb->getDefination(199); 
      
        if ($appcategoryList){
            foreach($appcategoryList as $reasonLoop){
                  $ddl_nationality->addMultiOption($reasonLoop['DefinitionCode'], $reasonLoop['DefinitionDesc']);
            }
        }
       
                     
                     
        
                        
            $Sub_type = new Zend_Form_Element_Text('id_member_subtype');
            $Sub_type-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
                   // ->setAttrib('class','txt_put')
                    ->setAttrib('id','sub')
                    ->setAttrib('maxlength','50')
                    ->setAttrib('class','form-control')
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')                   
                    ->removeDecorator("Label") 
                    ->removeDecorator("DtDdWrapper")
                    ->removeDecorator('HtmlTag');

        $amount = new Zend_Form_Element_Text('amount');
            $amount-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
                   // ->setAttrib('class','txt_put')
                    ->setAttrib('id','amt')
                    ->setAttrib('class','form-control')
                    ->setAttrib('maxlength','50')
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')                   
                    ->removeDecorator("Label") 
                    ->removeDecorator("DtDdWrapper")
                    ->removeDecorator('HtmlTag');

        
       
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
                    
                    
      
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->setAttrib('class','uk-button');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag');
        
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
        $Back->label = $gstrtranslate->_("Close");
        $Back->setAttrib('class','uk-button')
                ->removeDecorator("Label")
                ->removeDecorator("DtDdWrapper")
                ->removeDecorator('HtmlTag');

        $this->addElements(array($dropdown_member,$dropdown_degree,$effective_date,$dropdown_currency,$ddl_nationality,$dropdown_fees,$Sub_type,$amount,$UpdUser,$Save,$Back,$gst,$renewal_year));

    }
}