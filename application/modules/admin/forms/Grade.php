<?php

class Admin_Form_Grade extends App_Form_Base {

	public function init(){

        $translate = Zend_Registry::get('Zend_Translate');

        //grade
        $this->addElement('text', 'grade', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //grade
        $this->addElement('text', 'feedback', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //description
        /*$this->addElement('textarea', 'description', array(
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));*/


	}
}
