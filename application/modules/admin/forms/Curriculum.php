<?php

class Admin_Form_Curriculum extends App_Form_Base {
	
	public function init(){

        $translate = Zend_Registry::get('Zend_Translate');

        //name
        $this->addElement('text', 'name', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //parent
        $parent_opt = array(0 => $translate->_('None'));

        $this->addElement('select', 'parent_id', array(
            'required'    => true,
            'multiOptions'=> $parent_opt
        ));

        //code
        $this->addElement('text', 'code', array(
            'filters'    => array('StringTrim'),
        ));

        //excerpt
        $this->addElement('textarea', 'excerpt', array(
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));

        //description
        $this->addElement('textarea', 'description', array(
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));

        $this->addElement('select', 'mode', array(
            'required'    => true,
            'value'       => 'N',
            'multiOptions'=> array(
                'N'  => 'Normal',
                'CS' => 'Challenge Status',
                'FT' => 'Fast Track',
                'E'  => 'Exam Only'
            )
        ));

        //visibility
        $visibility_opt = array('' => $translate->_('Select'));

        foreach ( Cms_Dataset::get('visibility') as $key => $value ) {
            $visibility_opt[ $key ] = $value;

        }

        $this->addElement('select', 'visibility', array(
            'required'    => true,
            'value'       => 1,
            'multiOptions'=> $visibility_opt
        ));
	}
}