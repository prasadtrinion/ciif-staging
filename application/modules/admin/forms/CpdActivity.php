<?php
class Admin_Form_CpdActivity extends Zend_Dojo_Form {
    public function init() {  


           $definationDB =new Admin_Model_DbTable_Definition;  	
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	$strSystemDate = date('Y-m-d H:i:s');
    	
       

        			
        $ExperienceName = new Zend_Form_Element_Text('experience_name');
		$ExperienceName-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
			        -> setAttrib('required',"true") 
					->setAttrib('maxlength','25')
					->addFilter('StripTags')
					->addFilter('StringTrim')

					->removeDecorator("Label") 
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
        			
        $cpd_learning_id = new Zend_Form_Element_Select('cpdm_type');
        $cpd_learning_id->removeDecorator("DtDdWrapper");
        $cpd_learning_id->setAttrib('required', "true");
        $cpd_learning_id->setAttrib('class', 'form-control');
        $cpd_learning_id->setAttrib('id', 'cpdm_type');
        $cpd_learning_id->removeDecorator("Label");
        $cpd_learning_id->setAttrib('onchange', 'getLearningCategory(this.value);');
        $cpd_learning_id->addMultiOption('', '-- Select --');

        $learningLists = $definationDB->getByCode('CPD Learning');
        foreach ($learningLists as $cat) {
            $cpd_learning_id->addMultiOption($cat['idDefinition'], $cat['DefinitionCode']);
        }  

         $cpd_category_id = new Zend_Form_Element_Select('cpdm_category');
        $cpd_category_id->setAttrib('required', "true");
        $cpd_category_id->removeDecorator("DtDdWrapper");
        $cpd_category_id->setAttrib('class', 'form-control');
       
        $cpd_category_id->setAttrib('id', 'cpdm_category');
        $cpd_category_id->removeDecorator("Label");
        $cpd_category_id->setAttrib('onchange', 'getLearningActivity(this.value);');
        $cpd_category_id->setRegisterInArrayValidator(false);
        $cpd_category_id->addMultiOption('', '-- Select --');

        $cpd_activity_id = new Zend_Form_Element_Select('cpdm_activity');
        $cpd_activity_id->setAttrib('required', "true");
        $cpd_activity_id->removeDecorator("DtDdWrapper");
        $cpd_activity_id->setAttribs(array('style' => 'width: 250%;'));
        $cpd_activity_id->setAttrib('class', 'form-control');
        $cpd_activity_id->setAttrib('id', 'cpdm_activity');
        $cpd_activity_id->removeDecorator("Label");
        $cpd_activity_id->setRegisterInArrayValidator(false);
        $cpd_activity_id->addMultiOption('', '-- Select --');


                        
         $dropdown_degree = $this->createElement('select','dropdown_degree');
        $dropdown_degree->setAttrib('required',true)
                            ->setAttrib('id','degree')    
                         ->setAttrib('style','width:150px;')
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper");
        $dropdown_degree
                        ->addMultiOptions(array('' => '--Select Degree--' ));
                      


 $dataDb = new Admin_Model_DbTable_MemberExperience(); 
         $appcategoryList = $dataDb->getDefination(191);  
       
        if ($appcategoryList){
            foreach($appcategoryList as $reasonLoop){
                  $dropdown_degree->addMultiOption($reasonLoop['idDefinition'], $reasonLoop['DefinitionDesc']);
            }
        }
                  
        			 
        			 
        
        				
        	//Title
        $cpd_title = new Zend_Form_Element_Text('cpdm_title');
        $cpd_title->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $cpd_title->setAttrib('required', "true")
            ->setAttrib('cols', '100')
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->setAttrib('class', 'form-control')
            ->removeDecorator('HtmlTag');

        //Material Type
        $cpd_materialtype = new Zend_Form_Element_Select('cpdm_content');
        $cpd_materialtype->removeDecorator("DtDdWrapper");
    
        $cpd_materialtype->setAttrib('class', 'form-control');
        $cpd_materialtype->setAttrib('id', 'cpdm_content');
        $cpd_materialtype->removeDecorator("Label");
        $cpd_materialtype->addMultiOption('', '-- Select --');
        $cpd_materialtype->setAttrib('onchange', 'showContent();');
        $contentLists = $definationDB->getByCode('CPD Content');
        foreach ($contentLists as $cat) {
            $cpd_materialtype->addMultiOption($cat['idDefinition'], $cat['DefinitionDesc']);
        }


        $cpd_file = new Zend_Form_Element_File('cpdm_file');
        // $cpd_file->setLabel('File')
        // ->setDestination(DOCUMENT_PATH)
        $cpd_file->setRequired(true);
        //$this->addElement($file);

        //Provider
        $cpd_url = new Zend_Form_Element_Text('cpdm_url');
        $cpd_url->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $cpd_url->setAttrib('required', "true")
            ->setAttrib('class', 'form-control')
            ->setAttrib('cols', '100')
            ->setAttrib('class', 'form-control')
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //Provider
        $cpd_provider = new Zend_Form_Element_Text('cpdm_provider');
        $cpd_provider->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $cpd_provider->setAttrib('required', "true")
            ->setAttrib('cols', '100')
            ->setAttrib('class', 'form-control')
            ->setAttribs(array('style' => 'width: 100%;'))
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //Venue
        $cpdm_venue = new Zend_Form_Element_Text('cpdm_venue');
        $cpdm_venue->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $cpdm_venue->setAttrib('required', "true")
            ->setAttrib('cols', '100')
            ->setAttrib('class', 'form-control')
            ->setAttribs(array('style' => 'width: 100%;'))
            ->setAttrib('maxlength', '50')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //Description
        $cpd_description = new Zend_Form_Element_Textarea('cpdm_desc');
        $cpd_description->setAttrib('dojoType', "dijit.form.SimpleTextarea");
        $cpd_description->setAttrib('required', "true")
            ->setAttrib('maxlength', '150')
            ->setAttrib('class', 'form-control')
            ->setAttrib('cols', '100')
            ->setAttrib('class', 'form-control')
            ->setAttrib('rows', '4')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //Hours
        $cpd_duration = new Zend_Form_Element_Text('cpdm_hours');
        $cpd_duration->setAttrib('dojoType', "dijit.form.ValidationTextBox");
        $cpd_duration->setAttrib('required', "true")
            ->addValidator('Digits')
            ->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $date_completion = new Zend_Form_Element_Text('date_completion');

        $date_completion->setAttrib('required', "true");
        $date_completion->setAttrib('id', 'date_completion')          
          ->removeDecorator("DtDdWrapper")
          ->setAttrib('class', 'form-control')
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $date_started = new Zend_Form_Element_Text('date_started');
        $date_started->setAttrib('required', "true");
        $date_started->setAttrib('id', 'date_started')
            ->setAttrib('class', 'form-control')
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');


        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');

        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        //$Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag');

        
       
      
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
        $Back->label = $gstrtranslate->_("Close");
		$Back->setAttrib('class','uk-button')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
$this->addElements(array($Save, $UpdUser, $UpdDate, $cpd_duration, $cpd_description, $cpd_activity_id, $date_started, $date_completion, $cpdm_venue,
            $cpd_file, $cpd_url, $cpd_provider, $cpd_title, $cpd_category_id, $cpd_learning_id, $cpd_materialtype));

    
    }
}