<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
class Admin_Form_Currency extends Zend_Form
{

	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_currency');

		$this->addElement('text','cur_code',
				array(
						'label'=>'Currency Code',
						'required'=>'true',
						'class' => 'form-control'
				)
		);
		
		$this->addElement('text','cur_desc',
				array(
						'label'=>'Currency Name',
						'required'=>'true',
						'class' => 'form-control'
				)
		);
		
		$this->addElement('text','cur_desc_default_language',
				array(
						'label'=>'Default Language Name',
						'class' => 'form-control'
				)
		);
		
		$this->addElement('text','cur_symbol_prefix',
				array(
						'label'=>'Symbol Prefix',
						'class' => 'form-control'
				)
		);
		
		$this->addElement('text','cur_symbol_suffix',
				array(
						'label'=>'Symbol Suffix',
						'class' => 'form-control'
				)
		);
		
		$this->addElement('text','cur_decimal',
				array(
						'label'=>'Decimal Places',
						'required'=>'true',
						'class' => 'form-control'
				)
		);
		

		$this->addElement('checkbox','cur_status',
				array(
						'label'=>'Active'
				)
		);



		//button
		$this->addElement('submit', 'save', array(
				'label'=>'Submit',
				'decorators'=>array('ViewHelper'),
						'class' => 'btn btn-primary'
		));

		$this->addElement('submit', 'cancel', array(
				'label'=>'Cancel',
				'class' => 'btn btn-primary',
				'decorators'=>array('ViewHelper'),
				'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'studentfinance', 'controller'=>'currency','action'=>'setup'),'default',true) . "'; return false;"
		));

		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
				'decorators'=>array(
						'FormElements',
						array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
						'DtDdWrapper'
				)
		));

	}
}
?>