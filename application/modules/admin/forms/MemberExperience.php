<?php
class Admin_Form_MemberExperience extends Zend_Dojo_Form {
    public function init() {    	
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	$strSystemDate = date('Y-m-d H:i:s');
    	
       

        			
        $ExperienceName = new Zend_Form_Element_Text('experience_name');
		$ExperienceName-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
			        -> setAttrib('required',"true") 
					
					->addFilter('StripTags')
					->addFilter('StringTrim')

					->removeDecorator("Label") 
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag')
                    ->setAttrib('class','form-control');
        			
        $EName = new Zend_Form_Element_Text('professional');
        $EName-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
                    
                   
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')

                    ->removeDecorator("Label") 
                    ->removeDecorator("DtDdWrapper")
                    ->removeDecorator('HtmlTag');
                    
         $dropdown_member = $this->createElement('select','dropdown_member');
        $dropdown_member->setAttrib('required',true)
                            ->setAttrib('id','member')    
                         
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper")
                         ->setAttrib('class','form-control');
        $dropdown_member
                        ->addMultiOptions(array('' => '--Select Member--' ));
                      

           $dataDb = new Admin_Model_DbTable_Member();
        $appcategoryList = $dataDb->getListingData('member');  
            // echo "<pre>";
            // print_r($appcategoryList);

            foreach ($appcategoryList as $key => $value) {
               // echo $key;
                //echo $value['DefinitionDesc'].'<br>';
                $dropdown_member->addMultiOption($value['idmember'],$value['member_name']);
                // foreach ($value as $data) {
                //         //echo $data1.'<br>';
                //     echo $value['DefinitionDesc'].'<br>';
                // }
            }  



             $upgrade_member = $this->createElement('select','upgrade_member');
        $upgrade_member->setAttrib('required',true)
                            ->setAttrib('id','member')    
                         
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper")
                         ->setAttrib('class','form-control');
        $upgrade_member
                        ->addMultiOptions(array('' => '--Select Member--' ));
                      

           $dataDb = new Admin_Model_DbTable_Member();
        $appcategoryList = $dataDb->getListingData('member');  
            // echo "<pre>";
            // print_r($appcategoryList);

            foreach ($appcategoryList as $key => $value) {
               // echo $key;
                //echo $value['DefinitionDesc'].'<br>';
                $upgrade_member->addMultiOption($value['idmember'],$value['member_name']);
                // foreach ($value as $data) {
                //         //echo $data1.'<br>';
                //     echo $value['DefinitionDesc'].'<br>';
                // }
            }  
                        
         $dropdown_degree = $this->createElement('select','dropdown_degree');
        $dropdown_degree->setAttrib('required',true)
                            ->setAttrib('id','degree')    
                         
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper")
                         ->setAttrib('class','form-control');
        $dropdown_degree
                        ->addMultiOptions(array('' => '--Select Degree--' ));
                      


 $dataDb = new Admin_Model_DbTable_MemberExperience(); 
         $appcategoryList = $dataDb->getDefination(191);  
       
        if ($appcategoryList){
            foreach($appcategoryList as $reasonLoop){
                  $dropdown_degree->addMultiOption($reasonLoop['idDefinition'], $reasonLoop['DefinitionDesc'])
                  ->setAttrib('class','form-control');
            }
        }
                  
        			 
        			 
        
        				
        	$Sub_type = new Zend_Form_Element_Text('sub_type');
			$Sub_type-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
			       // ->setAttrib('class','txt_put')
                    ->setAttrib('id','sub')
					->setAttrib('maxlength','50')
					->addFilter('StripTags')
					->addFilter('StringTrim')					
					->removeDecorator("Label") 
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag')
                    ->setAttrib('class','form-control');

        
       
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
                    
        			
      
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
		$Save->setAttrib('class','uk-button');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag');
        
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
        $Back->label = $gstrtranslate->_("Close");
		$Back->setAttrib('class','uk-button')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        $this->addElements(array($dropdown_member,$upgrade_member,$dropdown_degree,$ExperienceName,$EName,$Sub_type,$UpdUser,$Save,$Back));

    }
}