<?php

class Admin_Form_UserGroupData extends App_Form_Base {

	public function init(){

        $translate = Zend_Registry::get('Zend_Translate');

        //name
        $this->addElement('text', 'group_name', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //description
        $this->addElement('textarea', 'group_desc', array(
            'required'   => false,
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));

        //parent
        /*
        $parent_opt = array(0 => $translate->_('None'));

        $this->addElement('select', 'parent_id', array(
            'required'    => true,
            'multiOptions'=> $parent_opt
        ));
        */

        //active
        $this->addElement('checkbox', 'active', array(
            'required'   => false,
            'class'      => 'uk-checkbox-toggle',
            'value'       => 1
        ));
	}
}
?>
