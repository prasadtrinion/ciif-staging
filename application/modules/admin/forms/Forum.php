<?php

class Admin_Form_forum extends App_Form_Base {

	public function init(){

        $translate = Zend_Registry::get('Zend_Translate');

        //name
        $this->addElement('text', 'name', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //parent
		/*
        $parent_opt = array(0 => $translate->_('None'));

        $this->addElement('select', 'parent_id', array(
            'required'    => true,
            'multiOptions'=> $parent_opt
        ));
		*/

        //description
        $this->addElement('textarea', 'description', array(
            'filters'    => array('StringTrim'),
            'rows'       => 2,
						'required'    => true
        ));

        //course
        $courseDb = new App_Model_Courses();
        $course = $courseDb->getCourses("a.code", TRUE);

        $course_list = array ('' => $translate->_('Select'));
        foreach ( $course as $key => $val ) {
            $course_list [ $val['id'] ] = $val['code'] . " - " . $val['title'];
        }

        $this->addElement('select', 'course_id', array(
            'required'    => true,
            'multiOptions'=> $course_list
        ));


        //visibility
		/*
        $visibility_opt = array('' => $translate->_('Select'));

        foreach ( Cms_Dataset::get('visibility') as $key => $value ) {
            $visibility_opt[ $key ] = $value;

        }

        $this->addElement('select', 'visibility', array(
            'required'    => true,
            'value'       => 1,
            'multiOptions'=> $visibility_opt
        ));
		*/
	}
}
