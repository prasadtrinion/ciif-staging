<?php
class Admin_Form_MembershipQualification extends Zend_Dojo_Form {
    public function init() {    	
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	$strSystemDate = date('Y-m-d H:i:s');
    	
       

        			
        $MemberName = new Zend_Form_Element_Text('member_name');
		
			       $MemberName -> setAttrib('required',"true")
                    -> setAttrib('id',"true")  
					->setAttrib('maxlength','25')
					->addFilter('StripTags')
					->addFilter('StringTrim')
                    ->setAttrib('class','form-control')
					->removeDecorator("Label") 
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');

        $sequence = new Zend_Form_Element_Text('sequence');
        
                   $MemberName -> setAttrib('required',"true")
                    -> setAttrib('id',"true")  
                    ->setAttrib('maxlength','25')
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')
                    ->setAttrib('class','form-control')
                    ->removeDecorator("Label") 
                    ->removeDecorator("DtDdWrapper")
                    ->removeDecorator('HtmlTag');
        			

      $program_route = new Zend_Form_Element_Select('program_route','');
        $program_route->setAttrib('class', 'form-control')
                        ->addValidator('NotEmpty',true)
                        ->removeDecorator("DtDdWrapper")
                        ->removeDecorator("Label");
       
        $program_route->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

        $definationDB =new Admin_Model_DbTable_Definition();
        $appcategoryList =$definationDB->getByCode('Program Route');  
            
            foreach ($appcategoryList as $key => $value) {
               // echo $key;
                //echo $value['DefinitionDesc'].'<br>';
                $program_route->addMultiOption($value['idDefinition'],$value['DefinitionDesc']);
               
            }  			 
        	

        $program_level = new Zend_Form_Element_Select('program_level','');
        $program_level->setAttrib('class', 'form-control')
                        ->addValidator('NotEmpty',true)
                        ->removeDecorator("DtDdWrapper")
                        ->removeDecorator("Label");
       
        $program_level->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

        $definationDB =new Admin_Model_DbTable_Definition();
        $appcategoryList =$definationDB->getByCode('Program Level');  
            
            foreach ($appcategoryList as $key => $value) {
               // echo $key;
                //echo $value['DefinitionDesc'].'<br>';
                $program_level->addMultiOption($value['idDefinition'],$value['DefinitionDesc']);
               
            }		 
        
        				
        	$Sub_type = new Zend_Form_Element_Text('sub_type');
			$Sub_type-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
			        ->setAttrib('class','form-control')
					->setAttrib('maxlength','50')
					->addFilter('StripTags')
					->addFilter('StringTrim')					
					->removeDecorator("Label") 
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');

        $effective_date = new Zend_Form_Element_Text('effective_date');
            $effective_date-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
                    ->setAttrib('class','form-control')
                     ->setAttrib('id','effective_date')
                    
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')                   
                    ->removeDecorator("Label") 
                    ->removeDecorator("DtDdWrapper")
                    ->removeDecorator('HtmlTag');

        $member_type = $this->createElement('select','member_type');
        $member_type->setAttrib('required',true)
                            ->setAttrib('id','member_type')    
                
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper")
                         ->setAttrib('class','form-control');
        $member_type
                        ->addMultiOptions(array('' => '--Select MemberName--' ));
                      

           $dataDb = new Admin_Model_DbTable_Member();
        $appcategoryList = $dataDb->getListingData('member');  
           
            foreach ($appcategoryList as $key => $value) {
              
                $member_type->addMultiOption($value['idmember'],$value['member_name']);
            
            } 

        
       
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
                    
        			
      
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
		$Save->setAttrib('class','uk-button');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
        ->setAttrib('class',' btn btn-primary');
        
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
        $Back->label = $gstrtranslate->_("Close");
		$Back->setAttrib('class','uk-button')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
                

                

        $this->addElements(array($MemberName,$member_type ,$program_level,$program_route,$effective_date,$Sub_type,$UpdUser,$Save,$Back));

    }
}