<?php

class Admin_Form_Quiz extends App_Form_Base {
	
	public function init(){

        $translate = Zend_Registry::get('Zend_Translate');

        //name
        $this->addElement('text', 'title', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //course
        $courseDb = new App_Model_Courses();
        $course = $courseDb->getCourses("a.code", TRUE);

        $course_list = array ('' => $translate->_('Select'));
        foreach ( $course as $key => $val ) {
            $course_list [ $val['id'] ] = $val['code'] . " - " . $val['title'];
        }

        $this->addElement('select', 'course_id', array(
            'required'    => false,
            'multiOptions'=> $course_list
        ));

        //excerpt
        $this->addElement('textarea', 'excerpt', array(
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));

        //question
        $this->addElement('textarea', 'question', array(
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));

        //description
        $this->addElement('textarea', 'description', array(
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));
		

        //visibility
        $visibility_opt = array('' => $translate->_('Select'));
        $type = array ( "0" => "Hide",
                        "1" => "Show");

        foreach ( $type as $key => $value ) {
            $visibility_opt[ $key ] = $value;

        }

        $this->addElement('select', 'visibility', array(
            'value'       => 1,
            'multiOptions'=> $visibility_opt
        ));

        //mark
        $this->addElement('text', 'mark', array(
            'required'   => true,
            'value'       => 1,
            'filters'    => array('StringTrim'),
        ));

        //answer 1
        $this->addElement('text', 'answer1', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //answer 2
        $this->addElement('text', 'answer2', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //answer 3
        $this->addElement('text', 'answer3', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //answer 4
        $this->addElement('text', 'answer4', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //correct_answer 
        $this->addElement('radio', 'correct_answer', array(
            'required'    => false,
            //'value'       => 0,
        ));

        //allow multiple attempt
        $this->addElement('checkbox', 'multiattempt', array(
            'required'    => true,
            'value'       => 0,
        ));
        // //correct_answer 2
        // $this->addElement('checkbox', 'correct_answer2', array(
        //     'required'    => false,
        //     'value'       => 0,
        // ));

        // //correct_answer 3
        // $this->addElement('checkbox', 'correct_answer3', array(
        //     'required'    => false,
        //     'value'       => 0,
        // ));

        // //correct_answer 4
        // $this->addElement('checkbox', 'correct_answer4', array(
        //     'required'    => false,
        //     'value'       => 0,
        // ));

	}
}