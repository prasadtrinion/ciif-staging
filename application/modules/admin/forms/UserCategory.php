<?php

class Admin_Form_UserCategory extends App_Form_Base {
	
	public function init(){

        $translate = Zend_Registry::get('Zend_Translate');

        //name
        $this->addElement('text', 'name', array(
            'required'   => true,
             'class' => 'form-control',
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('text', 'mreg_id', array(
            'required'   => true,
            'class' => 'form-control',
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('text', 'ic_no ', array(
            'required'   => true,
            'class' => 'form-control',
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('text', 'amount', array(
            'required'   => true,
            'class' => 'form-control',
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('text', 'code', array(
            'required'   => true,
            'class' => 'form-control',
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('text', 'start_date', array(
            'id' => 'start_date',
            'required'   => true,
            'class' => 'form-control',
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('text', 'end_date', array(
            'id' => 'end_date',
            'required'   => true,
            'class' => 'form-control',
            'filters'    => array('StringTrim'),
        ));

         $this->addElement('text', 'effective_date', array(
            'id' => 'start_date',
            'required'   => true,
            'class' => 'form-control',
            'filters'    => array('StringTrim'),
        ));

        //description
        $this->addElement('textarea', 'description', array(
            'required'   => false,
            'class' => 'form-control',
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));

        //parent
        $parent_opt = array(0 => $translate->_('None'));

        $this->addElement('select', 'parent_id', array(
            'required'    => true,
            'class' => 'form-control',
            'multiOptions'=> $parent_opt
        ));

        $program_route = array(0 => $translate->_('None'));

        $this->addElement('select', 'program_route', array(
            'required'    => true,
            'id'=> 'program_route',
            'class' => 'form-control',
            'multiOptions'=> $program_route
        ));

        $l_status = array(0 => $translate->_('None'));

        $this->addElement('select', 'l_status', array(
            'required'    => true,
            'id'=> 'l_status',
            'class' => 'form-control',
            'multiOptions'=> $l_status
        ));
        
        $amount_type = array(0 => $translate->_('None'));
        
         $this->addElement('select', 'amount_type', array(
            'required'    => true,
            'class' => 'form-control',
            'multiOptions'=> $amount_type
        ));

         
         $mode_of_program = array(0 => $translate->_('None'));

        $this->addElement('select', 'mode_of_program', array(
            'required'    => true,
            'id'=> 'mode_of_program',
            'class' => 'form-control',
            'multiOptions'=> $mode_of_program
        ));

         $effective_intake = array(0 => $translate->_('None'));

        $this->addElement('select', 'effective_intake', array(
            'required'    => true,
            'id'=> 'effective_intake',
            'class' => 'form-control',
            'multiOptions'=> $mode_of_program
        ));

        $program_level = array(0 => $translate->_('None'));

        $this->addElement('select', 'program_level', array(
            'required'    => true,
            'id'=> 'program_level',
            'class' => 'form-control',
            'multiOptions'=> $program_level
        ));

         $exam_center = array(0 => $translate->_('None'));

        $this->addElement('select', 'exam_center', array(
            'required'    => true,
            'id'=> 'program_level',
            'class' => 'form-control',
            'multiOptions'=> $program_level
        ));

        $program_landscape = array(0 => $translate->_('None'));

        $this->addElement('select', 'program_landscape', array(
            'required'    => true,
            'id'=> 'program_landscape',
            'class' => 'form-control',
            'multiOptions'=> $program_landscape
        ));

        $program_category = array(0 => $translate->_('None'));

        $this->addElement('select', 'program_category', array(
            'required'    => true,
            'id'=> 'program_category',
            'multiOptions'=> $program_category
        ));

        //active
        $this->addElement('checkbox', 'active', array(
            'required'   => false,
            'class'      => 'uk-checkbox-toggle',
            'value'       => 1
        ));

        $this->addElement('checkbox', 'cancel_app', array(
            'required'   => false,
            'class'      => 'uk-checkbox-toggle',
            'value'       => 1
        ));

         $this->addElement('checkbox', 'module_type', array(
            'required'   => false,
            'class'      => 'uk-checkbox-toggle',
            'value'       => 1
        ));
	}
}
?>