<?php
class Admin_Form_ApplicationFee extends Zend_Dojo_Form {
    public function init() {    	
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	$strSystemDate = date('Y-m-d H:i:s');
    	
       

        			
        $Name = new Zend_Form_Element_Text('name');
		$Name-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
			        -> setAttrib('required',"true") 
					->setAttrib('maxlength','25')
					->addFilter('StripTags')
					->addFilter('StringTrim')
                    ->setAttrib('class','form-control')
                    

					->removeDecorator("Label") 
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');


    $field2 = new Zend_Form_Element_Text('field2');
        $field2-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
                    -> setAttrib('required',"true") 
                    ->setAttrib('maxlength','25')
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')
                    ->setAttrib('class','form-control')
                    

                    ->removeDecorator("Label") 
                    ->removeDecorator("DtDdWrapper")
                    ->removeDecorator('HtmlTag');
        

        $amount = new Zend_Form_Element_Text('amount');
        $amount-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
                    -> setAttrib('required',"true") 
                    ->setAttrib('maxlength','25')
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')
                    ->setAttrib('class','form-control')
                    

                    ->removeDecorator("Label") 
                    ->removeDecorator("DtDdWrapper")
                    ->removeDecorator('HtmlTag');

        			 
        			 
        
        				
        	$description = new Zend_Form_Element_Text('description');
			$description-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
			        ->setAttrib('class','form-control')
					->setAttrib('maxlength','50')
					->addFilter('StripTags')
					->addFilter('StringTrim')					
					->removeDecorator("Label") 
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');


         $amount_type = $this->createElement('select','amount_type');
        $amount_type->setAttrib('required',true)
                            ->setAttrib('id','degree')    
                         ->setAttrib('class','form-control')
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper");
        $amount_type
                        ->addMultiOptions(array('' => '--Select Curency--' ));
                      


 $dataDb = new Admin_Model_DbTable_MemberExperience(); 
         $appcategoryList = $dataDb->getDefination(194);  
       
        if ($appcategoryList){
            foreach($appcategoryList as $reasonLoop){
                  $amount_type->addMultiOption($reasonLoop['idDefinition'], $reasonLoop['DefinitionDesc']);
            }
        }

        
       
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
                    
        			
      
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
		$Save->setAttrib('class','uk-button');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
        ->setAttrib('class',' btn btn-primary');
        
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
        $Back->label = $gstrtranslate->_("Close");
		$Back->setAttrib('class','uk-button')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
                

                

        $this->addElements(array($Name,$description,$amount_type,$field2,$amount,$UpdUser,$Save,$Back));

    }
}