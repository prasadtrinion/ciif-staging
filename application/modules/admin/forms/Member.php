<?php
class Admin_Form_Member extends Zend_Dojo_Form {
    public function init() {    	
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	$strSystemDate = date('Y-m-d H:i:s');
    	
       

        			
        $MemberName = new Zend_Form_Element_Text('member_name');
		$MemberName-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
			        -> setAttrib('required',"true") 
					->setAttrib('maxlength','25')
					->addFilter('StripTags')
					->addFilter('StringTrim')
                    ->setAttrib('class','form-control')
                    

					->removeDecorator("Label") 
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
        			

        			 
        			 
        
        				
        	$Sub_type = new Zend_Form_Element_Text('sub_type');
			$Sub_type-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
			        ->setAttrib('class','form-control')
					->setAttrib('maxlength','50')
					->addFilter('StripTags')
					->addFilter('StringTrim')					
					->removeDecorator("Label") 
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');

        $effective_date = new Zend_Form_Element_Text('effective_date');
            $effective_date-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
                    ->setAttrib('class','form-control')
                     ->setAttrib('id','effective_date')
                    
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')                   
                    ->removeDecorator("Label") 
                    ->removeDecorator("DtDdWrapper")
                    ->removeDecorator('HtmlTag');

        
       
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
                    
        			
      
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
		$Save->setAttrib('class','uk-button');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
        ->setAttrib('class',' btn btn-primary');
        
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
        $Back->label = $gstrtranslate->_("Close");
		$Back->setAttrib('class','uk-button')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
                

                

        $this->addElements(array($MemberName,$effective_date,$Sub_type,$UpdUser,$Save,$Back));

    }
}