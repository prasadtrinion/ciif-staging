<?php

class Admin_Form_Announcements extends App_Form_Base {
	
	public function init(){

        $translate = Zend_Registry::get('Zend_Translate');

        //name
        $this->addElement('text', 'title', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //course
        $courseDb = new App_Model_Courses();
        $course = $courseDb->getCourses("a.code", TRUE);

        $course_list = array ('' => $translate->_('Select'), '0' => $translate->_('ALL - General Announcement'));
        foreach ( $course as $key => $val ) {
            $course_list [ $val['id'] ] = $val['code'] . " - " . $val['title'];
        }

        $this->addElement('select', 'course_id', array(
            'required'    => true,
            'multiOptions'=> $course_list
        ));

        //excerpt
        $this->addElement('textarea', 'excerpt', array(
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));

        //description
        $this->addElement('textarea', 'description', array(
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));
		

        //visibility
        $visibility_opt = array('' => $translate->_('Select'));
        $type = array ( "0" => "Hide",
                        "1" => "Show");

        foreach ( $type as $key => $value ) {
            $visibility_opt[ $key ] = $value;

        }

        $this->addElement('select', 'visibility', array(
            'required'    => true,
            'value'       => 1,
            'multiOptions'=> $visibility_opt
        ));
	}
}