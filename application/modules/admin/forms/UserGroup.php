<?php

class Admin_Form_UserGroup extends App_Form_Base {
	
	public function init(){

        $translate = Zend_Registry::get('Zend_Translate');

        //name
        $this->addElement('text', 'group_name', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //description
        $this->addElement('textarea', 'group_desc', array(
            'required'   => false,
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));

        //course
        $courseDb = new App_Model_Courses();
        $course = $courseDb->getCourses("a.code", TRUE);

        $course_list = array ('0' => $translate->_('Select'));
        foreach ( $course as $key => $val ) {
            $course_list [ $val['id'] ] = $val['code'] . " - " . $val['title'];
        }

        $this->addElement('select', 'course_id', array(
            'required'    => true,
            'multiOptions'=> $course_list
        ));

        //active
        $this->addElement('checkbox', 'active', array(
            'required'   => false,
            'class'      => 'uk-checkbox-toggle',
            'value'       => 1
        ));

        //by program
        $this->addElement('checkbox', 'by_program', array(
            'required'   => true,
            'class'      => 'uk-checkbox-toggle',
            'value'      => 0,
            'onchange'   => 'displayProgramCourse()'
        ));
	}
}
?>