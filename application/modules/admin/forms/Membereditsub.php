<?php
class Admin_Form_Membereditsub extends Zend_Dojo_Form {
    public function init() {        
        $gstrtranslate =Zend_Registry::get('Zend_Translate'); 
        $strSystemDate = date('Y-m-d H:i:s');
        
       

                    
       
         $dropdown_member = $this->createElement('select','id_member');
        $dropdown_member->setAttrib('required',true)
                            ->setAttrib('id','member')    
                         ->setAttrib('class','form-control') 
                         ->setAttrib('required',true)
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper");
        $dropdown_member
                        ->addMultiOptions(array('' => '--Select Member--' ));
                      

           $dataDb = new Admin_Model_DbTable_Member();
        $appcategoryList = $dataDb->getListingData('member');  
            // echo "<pre>";
            // print_r($appcategoryList);

            foreach ($appcategoryList as $key => $value) {
               // echo $key;
                //echo $value['DefinitionDesc'].'<br>';
                $dropdown_member->addMultiOption($value['idmember'],$value['member_name']);
                // foreach ($value as $data) {
                //         //echo $data1.'<br>';
                //     echo $value['DefinitionDesc'].'<br>';
                // }
            


            }  
                        
         
                     
        
                        
        

        $sub_type = new Zend_Form_Element_Text('member_subtype_name');
            $sub_type-> setAttrib('dojoType',"dijit.form.ValidationTextBox")
                   // ->setAttrib('class','txt_put')
                    ->setAttrib('id','amt')
                    ->setAttrib('class','form-control') 
                    ->setAttrib('maxlength','50')
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')                   
                    ->removeDecorator("Label") 
                    ->removeDecorator("DtDdWrapper")
                    ->removeDecorator('HtmlTag');

        
       
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
                    
                    
      
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->setAttrib('class','uk-button');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag');
        
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
        $Back->label = $gstrtranslate->_("Close");
        $Back->setAttrib('class','uk-button')
                ->removeDecorator("Label")
                ->removeDecorator("DtDdWrapper")
                ->removeDecorator('HtmlTag');

        $this->addElements(array($dropdown_member,$sub_type,$UpdUser,$Save,$Back));

    }
}