<?php

class Admin_Form_Course extends App_Form_Base {
	
	public function init(){

        $translate = Zend_Registry::get('Zend_Translate');

        //title
        $this->addElement('text', 'title', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //code
        $this->addElement('text', 'code', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));


        //excerpt
        $this->addElement('textarea', 'excerpt', array(
            'required'   => false,
            'filters'    => array('StringTrim'),
            'rows'       => 1
        ));

        //description
        $this->addElement('textarea', 'description', array(
            'required'   => false,
            'filters'    => array('StringTrim'),
            'rows'       => 1
        ));

        //welcome
        $this->addElement('textarea', 'welcome', array(
            'required'   => false,
            'filters'    => array('StringTrim'),
            'rows'       => 1
        ));

        //category_id
        $this->addElement('select', 'category_id', array(
            'required'    => false
        ));

        //visibility
        $visibility_opt = array('' => $translate->_('Select'));

        foreach ( Cms_Dataset::get('visibility') as $key => $value ) {
            $visibility_opt[ $key ] = $value;

        }

        $this->addElement('select', 'visibility', array(
            'required'    => true,
            'value'       => 1,
            'multiOptions'=> $visibility_opt
        ));

        //use_forum
        $this->addElement('checkbox', 'use_forum', array(
            'required'    => true,
            'value'       => 1,
        ));

        //use_comments
        $this->addElement('checkbox', 'use_comments', array(
            'required'    => true,
            'value'       => 1,
        ));
	}
}
?>