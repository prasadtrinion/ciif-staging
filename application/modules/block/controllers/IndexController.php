<?php

class Block_IndexController extends  Zend_Controller_Action
{

	public function indexAction(){
		
		$id = $this->getParam('id');
		$course = $this->getParam('course');
		
		$this->view->course = $course;

		$src_link = '';

		if($course==1){
			$src_link = "/aqif/AQIFM1/M1T1/story.swf";
		}
		else if($course==2){
			$src_link = "/aqif/AQIFM2/AQIF_M2_T1/story.swf";
		}
		else if($course==3){
			$src_link = "/aqif/AQIFM3/AQIF_M3_T1/story.swf";
		}
		else if($course==4){
			$src_link = "/aqif/AQIFM4/AQIF_M4_T1/story.swf";
		}
		else if($course==5){
			$src_link = "/aqif/AQIFM5/AQIF_M5_T1/story.swf";
		}
		else if($course==6){
			$src_link = "/aqif/AQIFM6/AQIF_M6_T1/story.swf";
		}
		
		$this->view->src_link = $src_link;
	}
}

