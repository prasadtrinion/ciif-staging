<?php
class Portal_Form_MessagesCompose extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');
		
		$parent_id = new Zend_Form_Element_Hidden('parent_id');
		$parent_id	->setRequired(true)
					->setValue(0)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		$to_id = new Zend_Form_Element_Hidden('to_id');
		$to_id		->setAttrib('class', 'reqfield')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		//title
		$title = new Zend_Form_Element_Text('title');
		$title->setAttrib('class', 'inputtext reqfield')
				->setRequired(false)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//message
		$message = new Zend_Form_Element_Textarea('message');
		$message->setAttrib('class', 'textarea reqfield large')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

	
		
		//form elements
        $this->addElements(array(
			$parent_id,
			$to_id,
			$title,
			$message
			
		));
		
	}
}
?>