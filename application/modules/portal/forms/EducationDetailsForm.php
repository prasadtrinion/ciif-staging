<?php
class Portal_Form_EducationDetailsForm extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
        $this->setMethod('post');
        
        //qualification level
        $ae_qualification = new Zend_Form_Element_Select('ae_qualification');
		$ae_qualification->removeDecorator("DtDdWrapper")
						 ->removeDecorator('HtmlTag');
        $ae_qualification->setAttrib('class', 'select reqfield');
        $ae_qualification->setRequired(true);
        $ae_qualification->setAttrib('id', 'ae_qualification');
		$ae_qualification->removeDecorator("Label");
        
        $qualificationModel = new App_Model_Application_DbTable_Qualificationsetup();
        $qualificationList = $qualificationModel->fetchAll('qualification_type_id = 607');
        
        $ae_qualification->addMultiOption('', '-- Select --');
        
        if (count($qualificationList) > 0){
            foreach ($qualificationList as $qualificationLoop){
                $ae_qualification->addMultiOption($qualificationLoop['IdQualification'], $qualificationLoop['QualificationLevel']);
            }
        }
        
        //degree award
        $ae_degree_awarded = new Zend_Form_Element_Text('ae_degree_awarded');
		$ae_degree_awarded->setRequired(true);
		$ae_degree_awarded->setAttrib('class', 'inputtext')
            ->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag')
            ->removeDecorator("Label");
        
        //class degree
        $ae_class_degree = new Zend_Form_Element_Select('ae_class_degree');
        $ae_class_degree->setAttrib('class', 'select reqfield');
        $ae_class_degree->setRequired(true);
        $ae_class_degree->setAttrib('id', 'ae_class_degree');
		$ae_class_degree->removeDecorator("Label");
        $ae_class_degree->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');

        $defModel = new App_Model_General_DbTable_Definationms();
        $degreeClassList = $defModel->getDataByType(103);
        
        $ae_class_degree->addMultiOption('', '-- Select --');
        
        if (count($degreeClassList)){
            foreach ($degreeClassList as $degreeClassLoop){
                $ae_class_degree->addMultiOption($degreeClassLoop['idDefinition'], $degreeClassLoop['DefinitionDesc']);
            }
        }
        
        //result / cgpa
        $ae_result = new Zend_Form_Element_Text('ae_result');
		$ae_result->setAttrib('class', 'inputtext')
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		 //majoring
        $ae_majoring = new Zend_Form_Element_Text('ae_majoring');
		$ae_majoring->setAttrib('class', 'inputtext')
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label");
        
        //year of qraduation
        $ae_year_qraduate = new Zend_Form_Element_Text('ae_year_graduate');
		$ae_year_qraduate->setAttrib('class', 'inputtext half monthcal')
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label");
        
        //medium of instruction
		$ae_medium_instruction = new Zend_Form_Element_Text('ae_medium_instruction');
		$ae_medium_instruction->setRequired(true);
		$ae_medium_instruction->setAttrib('class', 'inputtext reqfield')
			->removeDecorator("DtDdWrapper")
			->removeDecorator("Label")
			->removeDecorator('HtmlTag');

		//ae_institution_country
		$ae_institution_country = new Zend_Form_Element_Select('ae_institution_country');
		$ae_institution_country->removeDecorator("DtDdWrapper");
		$ae_institution_country->setAttrib('class', 'select reqfield');
		$ae_institution_country->setRequired(true);
		$ae_institution_country->removeDecorator("Label")
								->removeDecorator('HtmlTag');
		$ae_institution_country->setAttrib('onchange', 'getInstitution(this.value,"ae_institution");');

        $countryModel = new App_Model_General_DbTable_Countrymaster();
		$countryList = $countryModel->fetchAll();

		$ae_institution_country->addMultiOption('','-- Select --');

		if (count($countryList) > 0){
			foreach ($countryList as $countryLoop){
				$ae_institution_country->addMultiOption($countryLoop['idCountry'], $countryLoop['CountryName']);
			}
		}

		$others = new Zend_Form_Element_Text('others');
		$others->setAttrib('class', 'inputtext')
		->setAttrib('placeholder',$this->getView()->translate('Please Specify'))
		->setAttrib('style','display:none')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

        //university
		$ae_institution = new Zend_Form_Element_Select('ae_institution');
		$ae_institution->removeDecorator("DtDdWrapper");
		$ae_institution->setAttrib('class', 'select reqfield');
		$ae_institution->setRequired(true);
		$ae_institution->setRegisterInArrayValidator(false);
		$ae_institution->setAttrib('onchange', 'checkInsOthers(this);');
		$ae_institution->removeDecorator("Label")
						->removeDecorator('HtmlTag');
        
        $schoolModel = new App_Model_General_DbTable_SchoolMaster();
        $schoolList = $schoolModel->fetchAll();
        
        $ae_institution->addMultiOption('','-- Select --');
        $ae_institution->addMultiOption(999,'Others');
        
        
        //form elements
        $this->addElements(array(
            $ae_qualification,
            $ae_degree_awarded,
			$ae_majoring,
            $ae_class_degree,
            $ae_result,
            $ae_year_qraduate,
            $ae_medium_instruction,
            $ae_institution,
			$ae_institution_country,
			$others
        ));
    }
}