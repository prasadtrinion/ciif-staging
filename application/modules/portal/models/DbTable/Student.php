<?php
class Portal_Model_DbTable_Student extends Zend_Db_Table {
	
	protected $_name = 'student_profile';


	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}
	
	public function updateData($data, $id)
	{
		$db = $this->db;
		
		$db->update($this->_name, $data, array('id'=>$id) );
	}
}