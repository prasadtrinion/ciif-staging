<?php

class Portal_HomeController extends  Zend_Controller_Action
{
	public function init() {
		
		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		
		$this->auth = Zend_Auth::getInstance();
		$this->studentinfo = $this->view->studentinfo = $this->auth->getIdentity()->info;

		$this->announceDB = new Portal_Model_DbTable_Announcements();
		
	}

	public function indexAction(){
    	
    	$this->view->title = $this->view->translate('Dashboard');
         
        $appl_id = $this->auth->getIdentity()->appl_id; 
    	$IdStudentRegistration = $this->auth->getIdentity()->registration_id;    
		$this->view->profileStatus = $this->auth->getIdentity()->info['profileStatus'];

    	$this->view->appl_id = $appl_id;
    	$this->view->IdStudentRegistration = $IdStudentRegistration;
		
		//get last login
		$this->view->lastupdated = $this->studentinfo['lastlogin'];
			
		//get news
		$this->view->news = $this->getNews();

		//get current student sem
		$semstatusDB = new App_Model_Record_DbTable_Studentsemesterstatus();
		$currentSem = $semstatusDB->getLastRegSem($IdStudentRegistration);

		//added
		$programDb = new App_Model_General_DbTable_Program();
		$semesterDb = new App_Model_Registration_DbTable_Semester();
		$studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
		$student = $studentRegDB->getStudentInfo($IdStudentRegistration);
		$program = $programDb->fngetProgramData($student['IdProgram']);
		$this->view->program = $program;
		
		//semester
		$semester = $semesterDb->getApplicantCurrentSemester(array('ap_prog_scheme' => $program['IdScheme'],'branch_id'=>$student['IdBranch']));
		$this->view->semester = $semester;

		$db = getDb();
		if($semester){
			
		  	$select = $db->select()
		                 ->from(array('a'=>'tbl_studentregsubjects'))
						 ->join(array('s'=>'tbl_subjectmaster'), 's.IdSubject=a.IdSubject', array('SubjectName', 'SubCode', 'CreditHours','CourseType'))
						 ->joinLeft(array('er' => 'exam_registration'),'er.`er_idStudentRegistration`= a.IdStudentRegistration AND er.er_idSubject = a.IdSubject AND er.er_idSemester = a.IdSemesterMain',array('er_id','er_idCity', 'er_idCityOthers'))
						 ->joinLeft(array('ec'=>'tbl_exam_center'),'ec.ec_id=er.er_ec_id',array('ec_name'))	
						 ->joinLeft(array('ecs'=>'exam_center_setup'),'ecs.ec_id = er.er_ec_id AND ecs.idSemester=a.IdSemesterMain',array('ecs_publish','idSemester'))
						 ->where('a.IdSemesterMain = ?', $semester['IdSemesterMaster'])
						 ->where('a.IdStudentRegistration = ?',  $IdStudentRegistration);		
			$registered = $db->fetchAll($select);
		}else{
			$registered = null;
		}
		
		
		$this->view->regSubjects = $registered;

		//announcement
		$idScheme = $this->announceDB->getIdSchemebasedOnReg($IdStudentRegistration);
		$this->view->announcements = $this->announceDB->getStudentAnnouncements($this->studentinfo['IdProgram'], $idScheme,'studentportal',$this->studentinfo['id'],0,0);
		$this->view->importantannouncements = $this->announceDB->getStudentAnnouncements($this->studentinfo['IdProgram'], $idScheme,'studentportal', $this->studentinfo['id']);
		
		foreach ( $this->view->importantannouncements as $i => $announcement )
		{
			$files = $this->announceDB->getFiles($announcement['id']);
			$this->view->importantannouncements[$i]['files'] = $files;
		}
		
		//events
		$eventDb = new App_Model_General_DbTable_Activity();
		//$events = $eventDb->getEventList(null, new Zend_Db_Expr('NOW()'),null,'StartDate DESC');
		$events = $eventDb->getEventList(null, null,null,'StartDate asc',null, $this->studentinfo['IdScheme']);

		//upcoming
		$upcomingevents = $eventDb->getEventList(new Zend_Db_Expr('NOW()'), null,null,'StartDate asc',3, $this->studentinfo['IdScheme']);

		$this->view->events = $upcomingevents;
		
		//events data for calendar
		$eventsdata = array();

		foreach ( $events as $event )
		{
			$duration = date('j', strtotime($event['StartDate'])).'-'.date('j', strtotime($event['EndDate'])).' '.date('M Y', strtotime($event['StartDate']));
			$eventsdata[date('m', strtotime($event['StartDate'])).'-DD-'.date('Y', strtotime($event['StartDate']))] =
				array(	'content'	=>		$duration.' - '.$event['ActivityName'],
						'startDate'	=>		date('j', strtotime($event['StartDate'])),
						'endDate'	=>		date('j', strtotime($event['EndDate']))
					);
		}

		$this->view->eventsData = json_encode($eventsdata);
	}

	public function getNews()
	{
		require_once('simplepie/simplepie.inc');
		
		if ( !is_dir(DOCUMENT_PATH.'/feedcache') )
		{
			mkdir_p(DOCUMENT_PATH.'/feedcache');
		}
		$feed = new SimplePie();
		$feed->set_feed_url('http://www.inceif.org/category/news/feed/');
		$feed->set_cache_location( DOCUMENT_PATH.'/feedcache' );
		$feed->handle_content_type();
		$feed->init();

		$news = array();

		foreach($feed->get_items() as $item)
		{
			$guid = $item->get_id(true);
			$content = $item->get_content();
			
			$news[] = array(	
								'guid'	=>	$guid,
								'title'	=> $item->get_title(),
								'date'	=> date('Y-m-d H:i:s',strtotime($item->get_date())),
								'link'	=> $item->get_permalink(),
								'content' => snippet( strip_tags($content),200)
							);
		} //feed
		
		if ( count($news) > 5 )
		{
			$news = array_slice($news, 0, 5);
		}

		$feed->__destruct(); 
		return $news;
	}
}

