<?php

class Portal_IndexController extends  Zend_Controller_Action
{
	public function init() {
		
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		
		
		$this->auth = Zend_Auth::getInstance();
		$this->studentInfo = $this->auth->getIdentity()->info;
	}

	public function indexAction()
	{
    	$this->_redirect($this->view->url(array('module'=>'portal','controller'=>'home', 'action'=>'index'),'default',true));
	}
}

