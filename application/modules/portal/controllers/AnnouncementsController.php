<?php

class Portal_AnnouncementsController extends  Zend_Controller_Action
{
	public function init() {
		
		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		
		$this->auth = Zend_Auth::getInstance();
		$this->studentinfo = $this->view->studentinfo = $this->auth->getIdentity()->info;

		$this->announceDB = new Portal_Model_DbTable_Announcements();

		$this->type = 'studentportal';
		
	}

	public function indexAction(){
    	
    	$this->view->title = $this->view->translate('Announcements');
                
		$IdStudentRegistration = $this->auth->getIdentity()->registration_id;

		//get current student sem
		$semstatusDB = new App_Model_Record_DbTable_Studentsemesterstatus();
		$currentSem = $semstatusDB->getLastRegSem($IdStudentRegistration);
                
		//announcement
                $idScheme = $this->announceDB->getIdSchemebasedOnReg($IdStudentRegistration);
		$p_data = $this->announceDB->getStudentAnnouncements($this->studentinfo['IdProgram'], $idScheme,$this->type,0,1);
                
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage(50);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
    	
    	$this->view->paginator = $paginator;
	}

	public function viewAction()
	{
		$id = $this->_getParam('id');
    	
    	$this->view->title = $this->view->translate('Announcements');
                
		$IdStudentRegistration = $this->auth->getIdentity()->registration_id;

		//get current student sem
		$semstatusDB = new App_Model_Record_DbTable_Studentsemesterstatus();
		$currentSem = $semstatusDB->getLastRegSem($IdStudentRegistration);
		
		//announcement
		$data = $this->announceDB->getData($id);

		//check permission
		if ( empty($data) )
		{
			throw new Exception('Invalid Announcement ID');
		}
		
		//checkread 
		$check = $this->announceDB->markRead($this->studentinfo['sp_id'], $id);

		$noperm = 0;
		if ( !in_array($data['program_id'], array( 0,$this->studentinfo['IdProgram'])) )
		{
			$noperm = 1;
		}

		if ( !in_array($data['semester_id'], array( 0, $currentSem['IdSemesterMaster'])) )
		{
			$noperm = 1;
		}
		
		$types = explode(',',$data['type']);
		if ( !in_array($this->type, $types) )
		{
			$noperm = 1;
		}
		$noperm = 0; // Temp solution to allow all announcement to be seen
		if ( $noperm )
		{
			throw new Exception('You cannot view this announcement');
		}
		
		$data['content'] = $data['html'] == 1 ? $data['content'] : nl2br($data['content']);

		//getfiles
		$files = $this->announceDB->getFiles($id);

		//views
		$this->view->info = $data;
		$this->view->files = $files;

		//update view counter
		$this->announceDB->updateViews($id);
	}
}

