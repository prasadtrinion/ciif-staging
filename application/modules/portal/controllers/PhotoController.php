<?php

class Portal_PhotoController extends  Zend_Controller_Action
{

    public function init()
    {
    	$this->locale = Zend_Registry::get('Zend_Locale');
    	$this->view->translate = Zend_Registry::get('Zend_Translate');
    	Zend_Form::setDefaultTranslator($this->view->translate);
    	
       Zend_Layout::getMvcInstance()->assign('navActive', 'profile');

        $this->auth = Zend_Auth::getInstance();
		$this->studentInfo = $this->auth->getIdentity()->info;

		$this->studentModel = new Portal_Model_DbTable_Student();

		$studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
		$student = $studentRegDB->getStudentInfo($this->auth->getIdentity()->IdStudentRegistration);

		if ( empty($student) )
		{
			throw new Exception('Invalid Student ID');
		}

		$this->uploadDir = $this->view->uploadDir = DOCUMENT_PATH.$student['sp_repository'];

		$this->studentinfo = $this->view->studentinfo = $student;
    }

	public function preDispatch() 
	{
        parent::preDispatch();
        $layout = new Zend_Layout();

        //jqcrop
        $layout->getView()->headScript()->appendFile($this->view->baseUrl().'/js/jquery/plugin/webcam/jquery.webcam.js');
    }

	public function indexAction()
	{
		
		$idStud = $this->studentinfo['id'];

		if ( $this->getRequest()->isPost() && $this->_request->getPost ( 'saveupload' ))
		{
			$formdata = $this->_request->getPost();
			
			try 
			{
				if ( !is_dir( $this->uploadDir ) )
				{
					if ( mkdir_p($this->uploadDir) === false )
					{
						throw new Exception('Cannot create student document folder ('.$this->uploadDir.')');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
			
				$file = $adapter->getFileInfo();
				$adapter->addValidator('NotExists', false, $this->uploadDir );
				$adapter->setDestination( $this->uploadDir );
				$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 1));
				$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

				foreach ($file as $no => $fileinfo) 
				{
					$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx,zip', 'case' => false));
	
					if ($adapter->isUploaded($no))
					{
						$ext = getext($fileinfo['name']);

						$fileName = 'photo-'.md5($fileinfo['name']).'.'.$ext;
						$fileUrl = $this->uploadDir.'/'.$fileName;

						$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
						$adapter->setOptions(array('useByteString' => false));
						
						$size = $adapter->getFileSize($no);
						
						if( !$adapter->receive($no))
						{
							$errmessage = array();
							if ( is_array($adapter->getMessages() ))
							{	
								foreach(  $adapter->getMessages() as $errtype => $errmsg )
								{
									$errmessage[] = $errmsg;
								}
								
								throw new Exception(implode('<br />',$errmessage));
							}
						}
						
					} //isuploaded
					
				} //foreach
			}
			catch (Exception $e) 
			{
				throw new Exception( $e->getMessage() );
			}
			
			//ok
			$this->deleteCurrentPhoto();

			$photo_url = $this->studentinfo['sp_repository'].'/'.$fileName;
		
			$db = getDB();
			$db->update('student_profile', array('photo' => $photo_url) , 'id='.(int)$idStud);

			//$this->studentModel->update( array('photo' => $photo_url), $idStud);

			//update session					
			$data = Zend_Auth::getInstance()->getStorage()->read();
			$data->photo = $photo_url;

			$this->auth->getStorage()->write($data);

			$this->_helper->flashMessenger->addMessage(array('success' => "Photo successfully uploaded"));

			//redirect
			$this->_redirect( 'portal/photo' );
			
		}
		
		if ( $this->getRequest()->isPost() && $this->_request->getPost ( 'savewebcam' ) )  
		{
            $larrformData = $this->_request->getPost();
            $filePath = $this->uploadDir;

			if ( !is_dir($filePath) )
			{
				mkdir_p($filePath,0777);
			}
		
			$photo = 'photo-'.md5($idStud.'-'.time()).'.png';
			$fileUrl = $filePath.'/'.$photo;
		
			if (file_put_contents($fileUrl, base64_decode($larrformData['imagedata']))) 
			{
				//ok
				$this->deleteCurrentPhoto();
				
				$photo_url = $this->studentinfo['sp_repository'].'/'.$photo;
				
				$db = getDB();
				$db->update('student_profile', array('photo' => $photo_url) , 'id='.(int)$idStud);
				
				//update session					
				$data = Zend_Auth::getInstance()->getStorage()->read();
				$data->photo = $photo_url;

				
			
				$this->auth->getStorage()->write($data);
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Photo successfully updated"));

				//redirect
				$this->_redirect( 'portal/photo' );
			}
			else
			{
				//fail
				$this->view->errMsg = $this->view->translate('Failed to save photo from webcam.');
			}
       	}
	}
	
	public function deleteCurrentPhoto()
	{
		$idStud = $this->studentinfo['IdStudentRegistration'];
		
		$photo = $this->uploadDir.'/'.$this->studentinfo['photo'];
		
		if ( file_exists( $photo) )
		{ 
			@unlink( $photo );
		}
	}
}

