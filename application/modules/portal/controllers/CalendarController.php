<?php

class Portal_CalendarController extends  Zend_Controller_Action
{
	public function init() {
		
		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		
		$this->auth = Zend_Auth::getInstance();
		$this->studentinfo = $this->view->studentinfo = $this->auth->getIdentity()->info;

		$this->announceDB = new Portal_Model_DbTable_Announcements();

		$this->type = 'studentportal';

		$IdStudentRegistration = $this->auth->getIdentity()->registration_id;

		//get current student sem
		$semstatusDB = new App_Model_Record_DbTable_Studentsemesterstatus();
		$this->currentSem = $semstatusDB->getLastRegSem($IdStudentRegistration);
	}

	public function indexAction(){
    	
    	$this->view->title = $this->view->translate('Calendar');
                
		//events
		$eventDb = new App_Model_General_DbTable_Activity();
		$events = $eventDb->getEventList(null, null,null,'StartDate ASC',null, $this->studentinfo['IdScheme']);

		$this->view->events = $events;
	}
	
	public function subscribeAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$filename = 'inceif-events.ics';

		//events
		$eventDb = new App_Model_General_DbTable_Activity();
		$events = $eventDb->getEventList(null, null,null,'StartDate ASC',null, $this->studentinfo['IdScheme']);

		header('Content-type: text/calendar; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename );
?>BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Calendar Labs//Calendar 1.0//EN
CALSCALE:GREGORIAN
X-WR-CALNAME:INCEIF Calendar
X-WR-TIMEZONE:America/New_York
METHOD:PUBLISH
<?php foreach ( $events as $row ) 
{
echo 'BEGIN:VEVENT'. "\n";
echo 'DTEND:'.$this->dateToCal($row['EndDate'])."\n";
echo 'UID:'.md5('inceif-'.$row['id'])."\n";
echo 'DTSTAMP:'.$this->dateToCal($row['StartDate'])."\n";
echo 'DESCRIPTION:'.$this->escapeString($row['ActivityName'])."\n";
echo 'DTSTART:'.$this->dateToCal($row['StartDate'])."\n";
echo 'STATUS:CONFIRMED'."\n";
echo 'SEQUENCE:0'."\n";
echo 'SUMMARY:'.$this->escapeString($row['ActivityName'])."\n";
echo 'END:VEVENT'."\n";
}
?>
END:VCALENDAR
		<?php
	}

	function dateToCal($timestamp) {
	  return date('Ymd\THis\Z', strtotime($timestamp));
	}

	// Escapes a string of characters
	function escapeString($string) {
	  return preg_replace('/([\,;])/','\\\$1', $string);
	}

	public function geteventsAction()
	{
		$this->_helper->layout->disableLayout();
		
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();
		
	
		//events
		$eventDb = new App_Model_General_DbTable_Activity();
		$events = $eventDb->getEventList(null, null,null,'StartDate ASC',null, $this->studentinfo['IdScheme']);

		//add course and venue detail
		$schedule=array();
		$i=0;
		foreach ($events as $event){
			
			$schedule[$i] = array(
						'id'=> $event['id'],
						'title'=>$event['ActivityName'],
						'allDay'=>true,
						'start'=>$event['StartDate'],
						'end'=>$event['EndDate'],
						'backgroundColor' => $event['ActivityColorCode'],
						'borderColor' => '#000000',
						'className' => 'tableCal'
						);				
			$i++;
		}			
		
		$ajaxContext->addActionContext('view', 'html')
				->addActionContext('form', 'html')
				->addActionContext('process', 'json')
				->initContext();

		echo $json = Zend_Json::encode($schedule);
	  
		$this->view->json = $json;
        exit;
	}
}

