<?php

class Portal_MessagesController extends  Zend_Controller_Action
{
	public function init() 
	{			
		$this->locale = Zend_Registry::get('Zend_Locale');
    	$this->view->translate = Zend_Registry::get('Zend_Translate');
    	Zend_Form::setDefaultTranslator($this->view->translate);
    	
        Zend_Layout::getMvcInstance()->assign('navActive', 'messages');

        $this->auth = Zend_Auth::getInstance();
		$this->studentInfo = $this->auth->getIdentity()->info;

		$this->studentModel = new Portal_Model_DbTable_Student();
		$this->msgModel = new Portal_Model_DbTable_Messages();
		$this->profileModel = new App_Model_Student_DbTable_StudentProfile();

		$studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
		$student = $studentRegDB->getStudentInfo($this->auth->getIdentity()->IdStudentRegistration);

		if ( empty($student) )
		{
			throw new Exception('Invalid Student ID');
		}

		$this->uploadDir = $this->view->uploadDir = DOCUMENT_PATH.$student['sp_repository'];

		$this->studentinfo = $this->view->studentinfo = $student;
		$this->student_id = $this->studentinfo['id'];

		//option
		$this->sendemail = 0;
	}

	public function indexAction()
	{
		$folder = $this->_getParam('folder','inbox');
		
		$this->view->folder = $folder;
		$this->view->foldername = ucfirst($folder);

		$this->view->title = $this->view->translate('Messages - '.$this->view->foldername);

		if ( $this->getRequest()->isPost() && $this->_request->getPost ( 'submit' ))
		{
			$formdata = $this->_request->getPost();
			$total = count($formdata['chk']);

			if ( $total > 0 )
			{
				$ids = implode(',', $formdata['chk']);
				
				if ( $formdata['action'] == 'read' )
				{
					$this->msgModel->updateMsgs( array('haveunread' => 0 ), $ids );
				}
				else if ( $formdata['action'] == 'unread' )
				{
					$this->msgModel->updateMsgs( array('haveunread' => 1 ), $ids );
					
				}
				elseif ( $formdata['action'] == 'inbox' )
				{
					$this->msgModel->updateMsgs( array('folder_id' => 0 ), $ids );
				}
				else if ( $formdata['action'] == 'trash' )
				{
					$this->msgModel->updateMsgs( array('folder_id' => 1 ), $ids );
				}
			}
		}

		switch ( $folder )
		{
			case "outbox":
				$p_data = $this->msgModel->getMessages(null, $this->student_id);
			break;

			case "trash":
				$p_data = $this->msgModel->getMessages(null, null, 1);
			break;
			

			case "inbox": default;
				$p_data = $this->msgModel->getMessagesInbox($this->student_id);
			break;
		}
		

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
    	$paginator->setItemCountPerPage(25);
    	$paginator->setCurrentPageNumber($this->_getParam('page',1));
	
    	
		$this->view->messages =  $paginator;
	}

	public function composeAction()
	{
		$form = new Portal_Form_MessagesCompose();
		$this->view->title = $this->view->translate('Compose');

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
		
			if ($form->isValid ( $formData )) 
			{
				//declare sender
				$from_id = $this->studentinfo['id'];
				
				$formData = clean_array($formData);
				
				$send = 1;


				
				//check
				if ( $formData['to_id'] == $this->studentinfo['id'] ) 
				{
					$send = 0 ;
					$this->_helper->flashMessenger->addMessage(array('error' => "You cannot send a message to yourself"));
				}


				//check to
				$check = $this->profileModel->getData($formData['to_id']);
				if ( empty($check) )
				{
					$send = 0;
					$this->_helper->flashMessenger->addMessage(array('error' => "Recepient account doesn't exist"));
				}

				if  ( $send ) 
				{

					//add
					$data = array(
									
									'from_id'					=> $from_id,
									'to_id'						=> $formData['to_id'],
									'parent_id'					=> $formData['parent_id'],
									'title'						=> $formData['title'],
									'message'					=> $formData['message'],
									'haveunread'				=> 1,
									'created_date'				=> new Zend_Db_Expr('NOW()')
								);

					
					$message_id = $this->msgModel->add($data);
					
					if ( $formData['parent_id'] > 0 )
					{
						$this->msgModel->unreadParent($formData['parent_id']);
					}

					$this->_helper->flashMessenger->addMessage(array('success' => "Your message has been sent to <strong>".$check['appl_fname'].' '.$check['appl_lname']."</strong>"));
					
					if ( $this->sendemail == 1 )
					{
						$message = 'Hi '.$check['appl_fname'].' '.$check['appl_lname'].','."\n";
						$message .= $this->studentinfo['appl_fname'].' '.$this->studentinfo['appl_lname'].' has sent you a new private message titled "'.$formData['title'].'"'."\n\n";
						$message .= 'You can read the message by visiting the link below:'."\n\n";
						$message .= 'http://'.APP_URL.'/portal/messages'; 

						$mail = new Cms_SendMail();
						$mail->fnSendMail($check['appl_email_personal'], $this->view->translate('New Private Message from '.$this->studentinfo['appl_fname'].' '.$this->studentinfo['appl_lname']), $message);
					}
				}
			 
				$this->_redirect($this->view->url(array('module'=>'portal','controller'=>'messages', 'action'=>'index'),'default',true));

			} // valid
			
			
		}


		//views
		$this->view->form = $form;
	}

	public function viewAction()
	{
		$form = new Portal_Form_MessagesCompose();

		$this->view->canreply = 1;
		$this->view->folder = 'inbox';

		$id = $this->_getParam('id');

		$data = $this->msgModel->getData($id);
		
		if ( empty($data) )
		{
			throw new Exception('Invalid Message ID');
		}

		//check owner
		if ( $data['to_id'] != $this->student_id && $data['from_id'] != $this->student_id )
		{
			throw new Exception('This message doesn\'t belong to you');
		}

		if ( $data['parent_id'] == 0 && $data['from_id'] == $this->student_id )
		{
			$this->view->canreply = 0;
			$this->view->folder = 'outbox';
		}

		
		$this->view->title = $data['title'];

		//haveunread 
		$this->msgModel->updateMsg(array('haveunread'=>0), $id);

		//set
		$form->parent_id->setValue($data['id']);
		$form->to_id->setValue($data['from_id']);
		$form->title->setValue('Re: '.$data['title']);
		$form->message->setAttrib("placeholder","Reply to message");
		$form->message->setAttrib("data-original",$data['message']);
		$form->message->setAttrib("onfocus","addoriginal()");

		$this->view->data = $data;

		//get reply
		$reply = $this->msgModel->getData($id,'parent_id',0);
		$this->view->replies = $reply;


		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
		
			if ($form->isValid ( $formData )) 
			{
				//declare sender
				$from_id = $this->studentinfo['id'];
				
				$formData = clean_array($formData);
				
				$send = 1;

				//check
				if ( $formData['to_id'] == $this->studentinfo['id'] ) 
				{
					$send = 0 ;
					$this->_helper->flashMessenger->addMessage(array('error' => "You cannot send a message to yourself"));
				}


				//check to
				$check = $this->profileModel->getData($formData['to_id']);
				if ( empty($check) )
				{
					$send = 0;
					$this->_helper->flashMessenger->addMessage(array('error' => "Recepient account doesn't exist"));
				}

				if  ( $send ) 
				{

					//add
					$data = array(
									
									'from_id'					=> $from_id,
									'to_id'						=> $formData['to_id'],
									'parent_id'					=> $formData['parent_id'],
									'title'						=> $formData['title'],
									'message'					=> $formData['message'],
									'haveunread'				=> 1,
									'created_date'				=> new Zend_Db_Expr('NOW()')
								);

					
					$message_id = $this->msgModel->add($data);
					
					if ( $formData['parent_id'] > 0 )
					{
						//$this->msgModel->unreadParent($formData['parent_id']);
					}

					$this->_helper->flashMessenger->addMessage(array('success' => "Your reply has been been added."));

					if ( $this->sendemail == 1 )
					{
						$message = 'Hi '.$check['appl_fname'].' '.$check['appl_lname'].','."\n";
						$message .= $this->studentinfo['appl_fname'].' '.$this->studentinfo['appl_lname'].' has sent you a new private message titled "'.$formData['title'].'"'."\n\n";
						$message .= 'You can read the message by visiting the link below:'."\n\n";
						$message .= 'http://'.APP_URL.'/portal/messages'; 


						$mail = new Cms_SendMail();
						$mail->fnSendMail($check['appl_email_personal'], $this->view->translate('New Private Message from '.$this->studentinfo['appl_fname'].' '.$this->studentinfo['appl_lname']), $message);
					}
				}
			 
				$this->_redirect($this->view->url(array('module'=>'portal','controller'=>'messages', 'action'=>'view', 'id' => $id ),'default',true));

			} // valid
			
			
		}


		//views
		$this->view->form = $form;
	}

	public function viewThreadAction()
	{
		$form = new Portal_Form_MessagesCompose();

		$this->view->canreply = 1;
		$this->view->folder = 'inbox';

		$id = $this->_getParam('id');

		$data = $this->msgModel->getData($id);
		
		if ( empty($data) )
		{
			throw new Exception('Invalid Message ID');
		}

		//check owner
		if ( $data['to_id'] != $this->student_id && $data['from_id'] != $this->student_id )
		{
			throw new Exception('This message doesn\'t belong to you');
		}
			
		//parent 
		if ( $data['parent_id'] == 0 )
		{
			throw Exception('You shouldn\'t be here');
		}

		$parent = $this->msgModel->getData($data['parent_id']);

		//
		$this->view->title = $parent['title'];
		$this->view->id = $id;

		//haveunread 
		$this->msgModel->updateMsg(array('haveunread'=>0), $data['id']);

		//set
		$form->parent_id->setValue($parent['id']);
		$form->to_id->setValue($data['from_id']);
		$form->title->setValue('Re: '.$data['title']);
		$form->message->setAttrib("placeholder","Reply to message");
		$form->message->setAttrib("data-original",$data['message']);
		$form->message->setAttrib("onfocus","addoriginal()");

		$this->view->data = $parent;

		//get reply
		$reply = $this->msgModel->getData($parent['id'],'parent_id',0);
		$this->view->replies = $reply;


		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
		
			if ($form->isValid ( $formData )) 
			{
				//declare sender
				$from_id = $this->studentinfo['id'];
				
				$formData = clean_array($formData);
				
				$send = 1;

				//check
				if ( $formData['to_id'] == $this->studentinfo['id'] ) 
				{
					$send = 0 ;
					$this->_helper->flashMessenger->addMessage(array('error' => "You cannot send a message to yourself"));
				}


				//check to
				$check = $this->profileModel->getData($formData['to_id']);
				if ( empty($check) )
				{
					$send = 0;
					$this->_helper->flashMessenger->addMessage(array('error' => "Recepient account doesn't exist"));
				}

				if  ( $send ) 
				{

					//add
					$data = array(
									
									'from_id'					=> $from_id,
									'to_id'						=> $formData['to_id'],
									'parent_id'					=> $formData['parent_id'],
									'title'						=> $formData['title'],
									'message'					=> $formData['message'],
									'haveunread'				=> 1,
									'created_date'				=> new Zend_Db_Expr('NOW()')
								);

					
					$message_id = $this->msgModel->add($data);
					
					if ( $formData['parent_id'] > 0 )
					{
						//$this->msgModel->unreadParent($formData['parent_id']);
					}

					$this->_helper->flashMessenger->addMessage(array('success' => "Your reply has been been added."));

					if ( $this->sendemail == 1 )
					{
						$message = 'Hi '.$check['appl_fname'].' '.$check['appl_lname'].','."\n";
						$message .= $this->studentinfo['appl_fname'].' '.$this->studentinfo['appl_lname'].' has sent you a new private message titled "'.$formData['title'].'"'."\n\n";
						$message .= 'You can read the message by visiting the link below:'."\n\n";
						$message .= 'http://'.APP_URL.'/portal/messages'; 


						$mail = new Cms_SendMail();
						$mail->fnSendMail($check['appl_email_personal'], $this->view->translate('New Private Message from '.$this->studentinfo['appl_fname'].' '.$this->studentinfo['appl_lname']), $message);
					}
				}
			 
				$this->_redirect($this->view->url(array('module'=>'portal','controller'=>'messages', 'action'=>'view-thread', 'id' => $message_id),'default',true));

			} // valid
			
			
		}


		//views
		$this->view->form = $form;
	}
}

