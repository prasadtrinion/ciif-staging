<?php

class Portal_ProfileController extends  Zend_Controller_Action
{
	public function init() {
		
		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		
		$this->auth = Zend_Auth::getInstance();
		$this->studentinfo = $this->view->studentinfo = $this->auth->getIdentity()->info;
		
		$this->lobjStudentprofile = new App_Model_Record_DbTable_Studentprofile();
		$studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
		$student = $studentRegDB->getStudentInfo($this->auth->getIdentity()->IdStudentRegistration);

		if ( empty($student) )
		{
			throw new Exception('Invalid Student ID');
		}

		$this->student = $student;
	}

	public function indexAction()
	{
    	$objdeftypeDB = new App_Model_Definitiontype();
		$countryDB = new App_Model_General_DbTable_Country();
		$studentprofileDb = new App_Model_Record_DbTable_Studentprofile();

    	$this->view->title = $this->view->translate('Profile');
		
		$idStud = $this->studentinfo['IdStudentRegistration'];

		$studentInfo = $this->lobjStudentprofile->getStudentInfo($idStud);
		$studentInfo['appl_passport_expiry'] = ($studentInfo['appl_passport_expiry'] == NULL ? '':date('d-m-Y', strtotime($studentInfo['appl_passport_expiry'])));
		$studentInfo['appl_dob'] = ($studentInfo['appl_dob'] == NULL ? '':date('d-m-Y', strtotime($studentInfo['appl_dob'])));
		
		$dob = date('Y-m-d', strtotime($studentInfo['appl_dob']));
		$today = date('Y-m-d');
		$age = $today-$dob;

		$studentInfo['appl_age'] = $age;
		$studentInfo['appl_password'] = '';
		$studentInfo['appl_salutation'] = $this->getRealValueByDef($studentInfo['appl_salutation'], $objdeftypeDB->fnGetDefinationsByLocale('Salutation'));
		$studentInfo['appl_idnumber_type'] = $this->getRealValueByDef($studentInfo['appl_idnumber_type'],$objdeftypeDB->fnGetDefinationsByLocale('ID Type'));
		$studentInfo['appl_marital_status'] = $this->getRealValueByDef($studentInfo['appl_marital_status'],$objdeftypeDB->fnGetDefinationsByLocale('Marital Status'));
		$studentInfo['appl_nationality'] = $this->getValueById($studentInfo['appl_nationality'],$countryDB->getData(),array('k'=>'idCountry','v'=>'CountryName')); 
		$studentInfo['appl_religion'] = $studentInfo['appl_religion']==99 ? $studentInfo['appl_religion_others'] : $this->getRealValueByDef($studentInfo['appl_religion'],$objdeftypeDB->fnGetDefinationsByLocale('Religion'));
		$studentInfo['appl_type_nationality'] = $this->getRealValueByDef($studentInfo['appl_type_nationality'],$objdeftypeDB->fnGetDefinationsByLocale('Type of Nationality'));
		$studentInfo['appl_race'] = $this->getRealValueByDef($studentInfo['appl_race'],$objdeftypeDB->fnGetDefinationsByLocale('Race'));
		$studentInfo['appl_bumiputera'] = $this->getRealValueByDef($studentInfo['appl_bumiputera'],$objdeftypeDB->fnGetDefinations('Bumiputera Eligibility'));
		
		if ($this->getRequest()->isPost()) 
		{
			$formData = $this->getRequest()->getPost();
			$formData = clean_array($formData); // clean data
			
			$data = array(		
							'appl_email_personal'	=> $formData['appl_email_personal'],
							'appl_address1'			=>	$formData['appl_address1'],
							'appl_address2'			=>	$formData['appl_address2'],
							'appl_address3'			=>	$formData['appl_address3'],
							'appl_postcode'			=>	$formData['appl_postcode'],
							'appl_country'			=>	$formData['appl_country'],
							'appl_state'			=>	$formData['appl_state'],
							'appl_state_others'		=>	$formData['appl_state_others'],
							'appl_city'				=>	$formData['appl_city'],
							'appl_city_others'		=>	$formData['appl_city_others'],
							'appl_caddress1'		=>	$formData['appl_caddress1'],
							'appl_caddress2'		=>	$formData['appl_caddress2'],
							'appl_caddress3'		=>	$formData['appl_caddress3'],
							'appl_cpostcode'		=>	$formData['appl_cpostcode'],
							'appl_ccountry'			=>	$formData['appl_ccountry'],
							'appl_cstate'			=>	$formData['appl_cstate'],
							'appl_cstate_others'	=>	$formData['appl_cstate_others'],
							'appl_ccity'			=>	$formData['appl_ccity'],
							'appl_ccity_others'		=>	$formData['appl_ccity_others'],
							'appl_phone_home'		=>	$formData['appl_phone_home'],
							'appl_phone_mobile'		=>	$formData['appl_phone_mobile'],
							'appl_phone_office'		=>	$formData['appl_phone_office'],
							'appl_fax'				=>	$formData['appl_fax'],
							'appl_cphone_home'		=>	$formData['appl_cphone_home'],
							'appl_cphone_mobile'	=>	$formData['appl_cphone_mobile'],
							'appl_cphone_office'	=>	$formData['appl_cphone_office'],
							'appl_cfax'				=>	$formData['appl_cfax']
						);

			$studentprofileDb->updateData($data, $this->student['sp_id']);
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Your information has been updated"));

			//redirect
			$this->_redirect( 'portal/profile#contact' );
		}

		$this->view->studentInfo = $studentInfo;

		$this->view->personalDetailForm = new Portal_Form_PersonalDetailsForm();
		$this->view->personalDetailForm->populate($studentInfo);

		//qualifications
		$educationDetails = $studentprofileDb->getEducationDetails($studentInfo['sp_id']);
		if(count($educationDetails)>0)
		{
			foreach($educationDetails as $index=>$q)
			{		
				 $documents = $studentprofileDb->getDocumentListByItem($studentInfo['sp_id'],$q['ae_id'],'applicant_qualification');
				 $educationDetails[$index]['documents'] = $documents;
			}
		}

		$this->view->educationDetails = $educationDetails;
		$this->view->totalqual = count($educationDetails);

		//employments
		$empDetails = $studentprofileDb->getEmploymentDetails($studentInfo['sp_id']);
		if(count($empDetails)>0)
		{
			foreach($empDetails as $index=>$q)
			{		
				 $documents = $studentprofileDb->getDocumentListByItem($studentInfo['sp_id'],$q['ae_id'],'applicant_employment');
				 $empDetails[$index]['documents'] = $documents;
			}
		}

		$this->view->empDetails = $empDetails;
		$this->view->totalemp = count($empDetails);

	}
	
	public function addEmploymentAction()
	{
		$this->view->title = 'Add Employment Details';

		$this->view->empForm = new Portal_Form_EmploymentDetailsForm();
		$studentprofileDb = new App_Model_Record_DbTable_Studentprofile();
		$defModel = new App_Model_General_DbTable_Definationms();
		$studentInfo = $this->student;
		$auth = Zend_Auth::getInstance();
		
		//qualifications
		$empDetails = $studentprofileDb->getEmploymentDetails($studentInfo['sp_id']);
		$this->view->total = count($empDetails);

		if ( $this->view->total >= 3 )
		{
			throw new Exception('You can only upload up to 3 employment details.');
		}
		
		if ($this->getRequest()->isPost()) 
		{
			$formData = $this->getRequest()->getPost();
			$formData = clean_array($formData); // clean data

			if ( $this->view->empForm->isValid($formData) )
			{
				
				$dir = DOCUMENT_PATH.$studentInfo['sp_repository'];
				
				if ( !is_dir($dir) ){
					if ( mkdir_p($dir) === false )
					{
						throw new Exception('Cannot create attachment folder ('.$dir.')');
					}
				}


				$data = array(	    		   
	    		    'sp_id' => $studentInfo['sp_id'],
		    		'ae_appl_id' => 0,
					'ae_trans_id' => 0,
	    			'ae_status'	=> $formData['ae_status'],
					'ae_comp_name' => $formData['ae_comp_name'], 
					'ae_comp_address' => $formData['ae_comp_address'],
					'ae_comp_phone' => $formData['ae_comp_phone'],
					'ae_comp_fax' => $formData['ae_comp_fax'],
					'ae_designation' => $formData['ae_designation'],
					'ae_position' => $formData['ae_position'],
					'ae_from' => '',
					'ae_to' => '',
					'emply_year_service' => $formData['emply_year_service'],
					'ae_industry' => $formData['ae_industry'],
					'ae_job_desc' => $formData['ae_job_desc'],
					'ae_income' => '',
					'ae_industry_type' => '',
					'upd_date' => date("Y-m-d H:i:s"),
					'upd_by' => $auth->getIdentity()->id,
					'ae_role' => $auth->getIdentity()->role,
	    		);
				
				$ae_id = $studentprofileDb->insertStudentProfile('student_employment', $data);


				//upload file proses
				$adapter = new Zend_File_Transfer_Adapter_Http();
				$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 10)); //maybe soon we will allow more?
				$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
				$adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx', 'case' => false));
				$adapter->addValidator('NotExists', false, $dir);
				$adapter->setDestination($dir);

				$files = $adapter->getFileInfo();


				//insert upload file in db
				if ($files)
				{
					foreach ($files as $file=>$info)
					{
						if ($adapter->isValid($file))
						{
							//get dcl id
							$definationCode = '';
							if ($file == 'employmentDetail_letter'){
								$definationCode = 'CONF';
							}

							$definationId = $defModel->getIdByDefType('Document Type', $definationCode);
							
							$fileOriName = $info['name'];
							$fileRename = date('YmdHis').'_'.$fileOriName;
							$filepath = $info['destination'].'/'.$fileRename;
							
							if ($adapter->isUploaded($file))
							{
								$adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $file);
								if ($adapter->receive($file))
								{
									
									$data2 = array(
										'sp_id' => $studentInfo['sp_id'],
										'ad_type' => '',
										'ad_section_id' => 7,
										'ad_table_name' => App_Model_Application_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = 7'),
										'ad_table_id' => $ae_id,
										'ad_filepath' => $studentInfo['sp_repository'].'/'.$fileRename,
										'ad_filename' => $fileRename,
										'ad_ori_filename' => $fileOriName,
										'ad_createddt'=> date('Y-m-d'),
										'ad_createdby'=> $auth->getIdentity()->id,
										'ad_role'=>$auth->getIdentity()->role
									);

									$studentprofileDb->insertUploadData($data2);
								}
							}
						}
					}//exit;
				}


				$this->_helper->flashMessenger->addMessage(array('success' => "Your information has been updated"));

				//redirect
				$this->_redirect( 'portal/profile#emp' );

			} // valid
		
		} //post
	}

	public function addEducationAction()
	{
		$this->view->title = 'Add Education Details';

		$this->view->eduForm = new Portal_Form_EducationDetailsForm();
		$studentprofileDb = new App_Model_Record_DbTable_Studentprofile();
		$defModel = new App_Model_General_DbTable_Definationms();
		$studentInfo = $this->student;
		$auth = Zend_Auth::getInstance();
		
		//qualifications
		$educationDetails = $studentprofileDb->getEducationDetails($studentInfo['sp_id']);
		$this->view->totalqual = count($educationDetails);

		if ( $this->view->totalqual >= 3 )
		{
			throw new Exception('You can only upload up to 3 qualification details.');
		}

		if ($this->getRequest()->isPost()) 
		{
			$formData = $this->getRequest()->getPost();
			$formData = clean_array($formData); // clean data

			if ( $this->view->eduForm->isValid($formData) )
			{
				$dir = DOCUMENT_PATH.$studentInfo['sp_repository'];
				
				if ( !is_dir($dir) ){
					if ( mkdir_p($dir) === false )
					{
						throw new Exception('Cannot create attachment folder ('.$dir.')');
					}
				}


				$data = array(	    		   
	    		    'sp_id' => $studentInfo['sp_id'],
		    		'ae_appl_id' => 0,
					'ae_transaction_id' => 0,
	    		    'ae_qualification' => $formData['ae_qualification'],
	    		    'ae_majoring' => $formData['ae_majoring'],
	    			'ae_degree_awarded' => $formData['ae_degree_awarded'],
		    		'ae_class_degree' => $formData['ae_class_degree'],
		    		'ae_result' => $formData['ae_result'],
		    		'ae_year_graduate' => $formData['ae_year_graduate'],
	    		    'ae_institution_country' => $formData['ae_institution_country'],
		    		'ae_institution' => $formData['ae_institution'],
		    		'ae_medium_instruction' => $formData['ae_medium_instruction'],	
	    		    'others' => $formData['others'],	    		  	    		
	    			'createDate' => date("Y-m-d H:i:s"),
					'createBy' => $auth->getIdentity()->id,
					'ae_role' => $auth->getIdentity()->role,
	    		);
				
				$ae_id = $studentprofileDb->insertStudentProfile('student_qualification', $data);

				//upload file proses
				$adapter = new Zend_File_Transfer_Adapter_Http();
				$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 10)); //maybe soon we will allow more?
				$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
				$adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx', 'case' => false));
				$adapter->addValidator('NotExists', false, $dir);
				$adapter->setDestination($dir);

				$files = $adapter->getFileInfo();

				//insert upload file in db
				if ($files)
				{
					foreach ($files as $file=>$info)
					{
						if ($adapter->isValid($file))
						{
							//get dcl id
							$definationCode = '';
							if ($file == 'educationDetail_certificate'){
								$definationCode = 'CEE';
							} else if ($file == 'educationDetail_transcript'){
								$definationCode = 'TRE';
							}
							
							$definationId = $defModel->getIdByDefType('Document Type', $definationCode);
							
							$fileOriName = $info['name'];
							$fileRename = date('YmdHis').'_'.$fileOriName;
							$filepath = $info['destination'].'/'.$fileRename;
							
							if ($adapter->isUploaded($file))
							{
								$adapter->addFilter('Rename', array('target' => $filepath,'overwrite' => true), $file);
								if ($adapter->receive($file))
								{
									
									$data = array(
										'sp_id' => $studentInfo['sp_id'],
										'ad_type' => $definationId['key'],
										'ad_section_id' => 3,
										'ad_table_name' => App_Model_Application_DbTable_ChecklistVerification::universalQuery('tbl_application_section', 'table_name', 'id = 3'),
										'ad_table_id' => $ae_id,
										'ad_filepath' => $studentInfo['sp_repository'].'/'.$fileRename,
										'ad_filename' => $fileRename,
										'ad_ori_filename' => $fileOriName,
										'ad_createddt'=> date('Y-m-d'),
										'ad_createdby'=> $auth->getIdentity()->id,
										'ad_role'=>$auth->getIdentity()->role
									);
									
									$studentprofileDb->insertUploadData($data);
								}
							}
						}
					}//exit;
				}

				$this->_helper->flashMessenger->addMessage(array('success' => "Your information has been updated"));

				//redirect
				$this->_redirect( 'portal/profile#qual' );
			
			} //isvalid

		}//post
	}

	public function changepasswordAction()
	{
		$this->view->title = 'Change Password';
		$this->view->errorMsg = '';
		
		$studentprofileDb = new App_Model_Record_DbTable_Studentprofile();

		$studentInfo = $this->student;
		$auth = Zend_Auth::getInstance();

		if ($this->getRequest()->isPost()) 
		{
			$formData = $this->getRequest()->getPost();
			$formData = clean_array($formData); //clean input

			$error = 0;
			if ( md5($formData['current_pass']) != $studentInfo['appl_password'] )
			{
				$error = 1;
				$this->view->errorMsg = $this->view->translate('Your current password is incorrect');
			}
		
			if ($formData['new_pass'] != $formData['new_pass2'] )
			{
				$error = 1;
				$this->view->errorMsg = $this->view->translate('New password doesn\'t match.');
			}

			if ( strlen($formData['new_pass']) < 6 )
			{
				$error = 1;
				$this->view->errorMsg = $this->view->translate('New password is too short. It must be atleast 6 characters long.');
			}

			if ( $error == 0 )
			{
				$data = array('appl_password' => md5($formData['new_pass']));
				$studentprofileDb->updateData($data, $this->student['sp_id']); 
				
				$KMC = new App_Model_Registration_DbTable_Kmc();
				$kmc_data = array(
					'username' => $studentInfo['registrationId'],
					'password' => $formData['new_pass']
				);
				
				$KMC->changePasswordKmc($kmc_data);
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Your information has been updated"));

				//redirect
				$this->_redirect( 'portal/profile/changepassword' );
			}
			
		}
	}


	public function getValueById($id, $data, $map)
	{
		if ( !empty($data) )
		{
			foreach ( $data as $row )
			{
				if ( $row[$map['k']] == $id )
				{
					return $row[$map['v']];
				}
			}
		}
	}

	public function getRealValueByDef($id, $data)
	{
		//changed to use the function above, too lazy to remove
		return $this->getValueById($id, $data, array('k'=>'idDefinition','v'=>'DefinitionDesc'));
	}
}

