<?php
class GeneralSetup_Model_DbTable_TopMenu extends Zend_Db_Table { 
		
	protected $_name = 'tbl_top_menu';
	protected $_primary = "tm_id";
		
	public function getData($id=0){
		$id = (int)$id;
		
		if($id!=0){
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('tm'=>$this->_name))
					->where('tm.tm_id = '.$id);

			$row = $db->fetchRow($select);
		}else{
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('app'=>$this->_name));
								
			$row = $db->fetchAll($select);
		}
		
		return $row;
	}
	
	public function getMenuList(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$select =  $db->select()
					->from(array('tm'=>$this->_name))					
					->order('tm.tm_seq_order');
								
		$row = $db->fetchAll($select);
		
		if($row){
			return $row;
		}else{
			return null;
		}
		
	}
	
	public function insert(array $data) {
		
        $db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('tm'=>'tbl_top_menu'), array(new Zend_Db_Expr('max(tm_seq_order) as max_seq')));
				
		$row = $db->fetchRow($select);
        
		$data['tm_seq_order'] = $row['max_seq']+1;
		
        return parent::insert($data);
    }
	
	public function addData($postData){
		
		$data = array(
		        'role_id' => $postData['role_id'],
				'label' => $postData['label'],
				'title' => $postData['title'],
				'module' => $postData['module'],
				'controller' => $postData['controller'],
				'action' => $postData['action'],
				'order' => $postData['order'],
		);
				
		$this->insert($data);
	}
	
	public function updateData($postData,$id){
		
		$data = array(
		        'role_id' => $postData['role_id'],
				'label' => $postData['label'],
				'title' => $postData['title'],
				'module' => $postData['module'],
				'controller' => $postData['controller'],
				'action' => $postData['action'],
				'order' => $postData['order'],
		);
			
		$this->update($data, 'tm_id = '. (int)$id);
	}
	
	public function deleteData($id){
		if($id!=0){
			$this->delete('tm_id = '. (int)$id);
		}
	}
}
