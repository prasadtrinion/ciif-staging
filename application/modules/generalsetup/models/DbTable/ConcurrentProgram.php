<?php 
class GeneralSetup_Model_DbTable_ConcurrentProgram extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_concurrent_program';
	protected $_primary = "cp_id";
	
	public function getDatabyId($id=0){
	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('cp'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	
	public function getDatabyProgramId($programid=0){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('cp'=>$this->_name) ) 
	                ->join(array("p" => "tbl_program"),'p.IdProgram=cp.cp_program_id',array('ProgramName','ProgramCode'))
	                ->join(array("d" => "tbl_definationms"),'d.idDefinition=p.Award',array('award'=>'DefinitionDesc'))
	                ->where('cp.IdProgram = '.$programid);	                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function addData($data){
		$this->insert($data);
	}
	
	public function updateData($data,$id){
		$this->update($data,"cp_id ='".$id."'");
	}
	
	public function deleteData($id){
		$this->delete("cp_id ='".$id."'");
	}
	
}
?>