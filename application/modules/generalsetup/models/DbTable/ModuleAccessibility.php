<?php
class GeneralSetup_Model_DbTable_ModuleAccessibility extends Zend_Db_Table { 
		
	protected $_name = 'tbl_module_accessibility';
	protected $_primary = "ma_id";
		
	public function getData($id=0){
		$id = (int)$id;
		
		if($id!=0){
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('ma'=>$this->_name))
					->where('ma.ma_id = '.$id);

			$row = $db->fetchRow($select);
		}else{
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('ma'=>$this->_name));
								
			$row = $db->fetchAll($select);
		}
		
		return $row;
	}
	
	public function searchData($formData=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select =  $db->select()
					->from(array('ma'=>$this->_name))
					->join(array('d'=>'tbl_definationms'),'d.idDefinition=ma.ma_role_id',array('RoleName'=>'DefinitionCode'))
					->joinLeft(array('f'=>'tbl_collegemaster'),'f.IdCollege=ma.ma_faculty_id',array('CollegeName'))	
					->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=ma.ma_program_id',array('ProgramName'))	
					->joinLeft(array('b'=>'tbl_branchofficevenue'),'b.IdBranch=ma.ma_branch_id',array('BranchName'))	
					->joinLeft(array('c'=>'tbl_subjectmaster'),'c.IdSubject=ma.ma_course_id',array('SubjectName'));

					if(isset($formData)){
						
						if(isset($formData['role_id']) && $formData['role_id']!=''){
							$select->where('ma.ma_role_id = ?',$formData['role_id']);
						}
						
						if(isset($formData['start_date']) && $formData['start_date']!=''){														
							if(isset($formData['end_date']) && $formData['end_date']!=''){
								$select->where('(ma.ma_start_date <= '.$formData['start_date'].' AND ma.ma_end_date >='.$formData['end_date'].')');
							}else{
								$select->where('ma.ma_start_date = ?',date('Y-m-d',strtotime($formData['start_date'])));
							}
						}
						
						if(isset($formData['faculty_id']) && $formData['faculty_id']!=''){
							$select->where('ma.ma_faculty_id = ?',$formData['faculty_id']);
						}
						
						if(isset($formData['program_id']) && $formData['program_id']!=''){
							$select->where('ma.ma_program_id = ?',$formData['program_id']);
						}
						
						if(isset($formData['branch_id']) && $formData['branch_id']!=''){
							$select->where('ma.ma_branch_id = ?',$formData['branch_id']);
						}
						
						if(isset($formData['course_id']) && $formData['course_id']!=''){
							$select->where('ma.ma_course_id = ?',$formData['course_id']);
						}
						
						
					}
					//echo $select;
		$row = $db->fetchAll($select);
		
		if($row){
			return $row;
		}else{
			return null;
		}
		
	}
	
	
	public function deleteData($id){
		if($id!=0){
			$this->delete('ma_id = '. (int)$id);
		}
	}
}


?>