<?php 
class GeneralSetup_Model_DbTable_AddressHistory extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_address_history';
	protected $_primary = "addh_id";
	
	public function getDatabyId($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('add'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getData(){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('add'=>$this->_name));		                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getDataByOrgId($org_id,$org_name){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('add'=>$this->_name))
	                ->join(array('d'=>'tbl_definationms'),'add.addh_address_type=d.idDefinition',array('addressType'=>'DefinitionDesc'))
	                ->joinLeft(array('c'=>'tbl_countries'),'add.addh_country=c.idCountry',array('CountryName'))
	                ->joinLeft(array('s'=>'tbl_state'),'add.addh_state=s.idState',array('StateName'))
	                ->joinLeft(array('u'=>'tbl_user'),'add.addh_changeby=u.iduser',array('changeby'=>'fName'))
	                ->where("add.addh_org_name=?",$org_name)
	                ->where('add.addh_org_id = ?',$org_id);		                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	
	public function addData($data){
		$this->insert($data);
	}
	
}
?>