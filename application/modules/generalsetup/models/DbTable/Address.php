<?php 
class GeneralSetup_Model_DbTable_Address extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_address';
	protected $_primary = "add_id";
	
	public function getDatabyId($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('add'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getData(){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('add'=>$this->_name));		                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function getDataByOrgId($org_id,$org_name){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('add'=>$this->_name))
	                ->join(array('d'=>'tbl_definationms'),'add.add_address_type=d.idDefinition',array('addressType'=>'DefinitionDesc'))
	                ->joinLeft(array('c'=>'tbl_countries'),'add.add_country=c.idCountry',array('CountryName'))
	                ->joinLeft(array('s'=>'tbl_state'),'add.add_state=s.idState',array('StateName'))
                        ->joinLeft(array('t'=>'tbl_city'),'add.add_city=t.idCity',array('CityName'))
	                ->where('add.add_org_id = ?',$org_id)
	                ->where('add.add_org_name = ?',$org_name);		                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function addData($data){
		$this->insert($data);
	}
	
	public function updateData($data,$id){
		$this->update($data,"add_id ='".$id."'");
	}
	
	public function deleteData($add_id){
		$this->delete("add_id ='".$add_id."'");
	}
	
	public function deleteOrgAddress($id,$org_name){
		$this->delete("add_org_name = '".$org_name."' AND add_org_id ='".$id."'");
	}
		
	
	
	
}
?>