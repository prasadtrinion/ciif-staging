<?php 

class GeneralSetup_Model_DbTable_CourseCoordinator extends Zend_Db_Table_Abstract {
	
	protected $_name = 'tbl_subjectcoordinatorlist';
	protected $_primary = "IdSubjectCoordinatorList";	
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getData($idSemester,$idSubject) { 
       $db = Zend_Db_Table::getDefaultAdapter();
       
       $select_coordinator = $db->select()
	 				 			->from(array("sc"=>"tbl_subjectcoordinatorlist"))
	 				 			->join(array("sm"=>"tbl_staffmaster"),'sm.IdStaff=sc.IdStaff')
	 				 			->where('sc.IdSemester = ?',$idSemester)
	 				 			->where('sc.IdSubject = ?',$idSubject);
       
	 	//echo $select;
		$row = $db->fetchAll($select_coordinator);
		
		return $row;
     } 
	
	
}