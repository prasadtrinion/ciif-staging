<?php
class GeneralSetup_Model_DbTable_Programscheme extends Zend_Db_Table_Abstract {
	protected $_name = 'tbl_program_scheme';
	private $lobjDbAdpt;
	protected $_locale;

	public function init() {
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
	}

	public function fnaddProgramscheme($data) { //Function for adding the University details to the table
		$id = $this->insert($data);
		if(!$id){
			 throw new Exception('Error insert Program Scheme');
		}
	}

	public function fngetprogramsscheme($programId){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_program_scheme"), array("a.*"))
		->joinLeft(array("b" => "tbl_program"),'a.IdProgram = b.IdProgram')
		->where("a.IdProgram = ?", $programId);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fndelProgramscheme($lintIdProgram){
		$db = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_program_scheme";
		$where = "IdProgram = '" . $lintIdProgram . "'";
		$db->delete($table,$where);
	}


	public function fngetprogramsschemelist($programId){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_program_scheme"), array('key' => "a.IdProgramScheme", "mode_of_program"=>"a.mode_of_program", "mode_of_study"=>"a.mode_of_study", "program_type"=>"a.program_type"))
		//->joinLeft(array("b" => "tbl_program"),'a.IdProgram = b.IdProgram')
		->where("a.IdProgram = ?", $programId);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetprogramsschemelistforfaculty($IdCollege){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("c" => "tbl_program"),array())
		->joinLeft(array("a" => "tbl_program_scheme"),'a.IdProgram = c.IdProgram' ,array(""))
		->joinLeft(array("b" => "tbl_scheme"),'a.IdScheme = b.IdScheme', array('key' => "b.IdScheme","name" => "b.EnglishDescription"))
		->where("c.IdCollege = ?", $IdCollege)
		->where("c.Active = ?",1)
		->group('b.IdScheme');
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fngetschemelist($programId){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a" => "tbl_program_scheme"), array(""))
		->joinLeft(array("b" => "tbl_scheme"),'a.IdScheme = b.IdScheme', array('key' => "b.IdScheme","value" => "b.EnglishDescription"))
		->where("a.IdProgram = ?", $programId);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		$this->update($data,"IdProgramScheme = '".$id."'");
	}
	
	public function checkDuplicate($programId,$programmode,$studymode,$programType){
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
							->from(array("a" => "tbl_program_scheme"))		
							->where("a.IdProgram = ?", $programId)
							->where("a.mode_of_program = ?", $programmode)
							->where("a.mode_of_study = ?", $studymode)
							->where("a.program_type = ?", $programType);
						
		$larrResult = $db->fetchRow($lstrSelect);
		
		return $larrResult;
		
				
	}
	
	public function deleteData($id){
		$this->delete("IdProgramScheme = '".$id."'");
	}
	
	public function deleteAllData($id){
		$this->delete("IdProgram = '".$id."'");
	}
	
	public function getProgSchemeByProgram($programId) {
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("ps" => "tbl_program_scheme"))
		->where("ps.IdProgram = ?", $programId);
		
		if($this->_locale == 'en_US'){
			$lstrSelect->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc','ProgramModeId'=>'IdDefinition'))
					   ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc','StudyModeId'=>'IdDefinition'))
			           ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc','ProgramTypeId'=>'IdDefinition'))
			           ;
		}else{
			$lstrSelect->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'Description','ProgramModeId'=>'IdDefinition'))
					   ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'Description','StudyModeId'=>'IdDefinition'))
					   ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'Description','ProgramTypeId'=>'IdDefinition'));
		}
		
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		//echo $lstrSelect;
		return $larrResult;
	}
    
    public function getSchemeById($id) {
        
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select = $db->select()
                ->from(array('ps' => $this->_name))
                ->joinLeft(array('b' => 'tbl_program'),'ps.IdProgram = b.IdProgram')
                ->where('IdProgramScheme = ?', (int)$id);
        
    	if($this->_locale == 'en_US'){
			$select->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc'))
					   ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc'))
			           ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc'))
			           ;
		}else{
			$select->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'Description'))
					   ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'Description'))
					   ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'Description'));
		}

        $row = $db->fetchRow($select);
        
        return $row;
    }

	public function getSchemeById2($id) {

		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select()
			->from(array('ps' => $this->_name))
			->joinLeft(array('b' => 'tbl_program'),'ps.IdProgram = b.IdProgram')
			->where('IdProgramScheme IN (?)', $id);

		if($this->_locale == 'en_US'){
			$select->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc'))
				->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc'))
				->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc'))
			;
		}else{
			$select->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'Description'))
				->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'Description'))
				->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'Description'));
		}

		$row = $db->fetchAll($select);

		return $row;
	}
    
    public function fnGetProgramSchemeIdWithType($program_id,$mode_of_study,$mode_of_program,$program_type = null)
    {
        $lstrSelect = $this->lobjDbAdpt->select()
                    ->from(array('a' => $this->_name))
                    ->where('a.IdProgram = ?', $program_id)
                    ->where('a.mode_of_study = ?', $mode_of_study)
                    ->where('a.mode_of_program = ?', $mode_of_program)
                    ->where('a.program_type = ?', $program_type);
        
        //echo $lstrSelect;
        $larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
        
        return $larrResult;
    }
}

?>
