<?php 

class GeneralSetup_Model_DbTable_IntakeProgram extends Zend_Db_Table_Abstract {
	
	protected $_name = 'tbl_intake_program';
	protected $_primary = "ip_id";
	
	public function addData($data){		  	
	   $this->insert($data);	
	   $db = Zend_Db_Table::getDefaultAdapter();
	   $id = $db->lastInsertId();
	   return $id;
	}
	
public function getIntakeProgram($idIntake){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sql = $db->select()
					->from(array("ip"=>$this->_name))
					->join(array('p'=>'tbl_program'),'p.IdProgram = ip.IdProgram',array('ProgramName','ProgramCode'))
					->join(array('d'=>'tbl_definationms'),'d.idDefinition = ip.IdStudentCategory',array('StudentCategory'=>'DefinitionCode','StudentCategoryDesc'=>'DefinitionDesc'))
					->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme = ip.IdProgramScheme',array('ProgramMode'=>'mode_of_program','StudyMode'=>'mode_of_study','ProgramType'=>'program_type'))					
					->where('ip.IdIntake = ?',$idIntake);
					
		$result = $db->fetchAll($sql);
		
		foreach($result as $index=>$res){
			
			//get defination scheme
			$sql_prog_mode = $db->select()
							->from(array('dp'=>'tbl_definationms'),array('ProgramMode'=>'DefinitionCode','ProgramModeDesc'=>'DefinitionDesc'))
							->where('dp.idDefinition = ?',$res['ProgramMode']);				
			$row_prog_mode = $db->fetchRow($sql_prog_mode);
			$result[$index]['ProgramMode']=$row_prog_mode['ProgramMode'];
                        $result[$index]['ProgramModeDesc']=$row_prog_mode['ProgramModeDesc'];
			
			//get defination scheme
			$sql_study_mode = $db->select()
							->from(array('dp'=>'tbl_definationms'),array('StudyMode'=>'DefinitionCode','StudyModeDesc'=>'DefinitionDesc'))
							->where('dp.idDefinition = ?',$res['StudyMode']);					
			$row_study_mode = $db->fetchRow($sql_study_mode);
			$result[$index]['StudyMode']=$row_study_mode['StudyMode'];
                        $result[$index]['StudyModeDesc']=$row_study_mode['StudyModeDesc'];
			
			//get defination type
			$sql_type = $db->select()
							->from(array('dp'=>'tbl_definationms'),array('ProgramType'=>'DefinitionCode','ProgramTypeDesc'=>'DefinitionDesc'))
							->where('dp.idDefinition = ?',$res['ProgramType']);					
			$row_type = $db->fetchRow($sql_type);
			$result[$index]['ProgramType']=$row_type['ProgramType'];
                        $result[$index]['ProgramTypeDesc']=$row_type['ProgramTypeDesc'];
			
		}
		
		return $result;
	}
	
	public function deleteData($id){		
		$this->delete("ip_id = '".$id."'");
	}
	
	public function deleteIntakeData($id_intake){		
		$this->delete("IdIntake = '".$id_intake."'");
	}
	
	public function checkDuplicate($IdProgramScheme,$idProgram,$studentcat,$intake){
		   $db = Zend_Db_Table::getDefaultAdapter();
		
	    	$select = $db->select()
						 ->from(array("ip"=>$this->_name))
						 ->where('ip.IdProgram =?',$idProgram)
						 ->where('ip.IdProgramScheme =?',$IdProgramScheme)
						  ->where('ip.IdStudentCategory =?',$studentcat)
						   ->where('ip.IdIntake  =?',$intake);											
			$row = $db->fetchRow($select);
			return $row;			
	}
        
    public function getIntakeProgramInfo($programIntakeId){
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a"=>$this->_name),
                    array(
                        "key"=>"a.ip_id",
                        "value"=>"a.*"
                    ))
            ->join(array('b'=>'tbl_program'), 'a.IdProgram = b.IdProgram')
            ->join(array('c'=>'tbl_program_scheme'), 'a.IdProgramScheme = c.IdProgramScheme')
            ->where('a.ip_id = ?',$programIntakeId);
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larrResult;
    }
    
    public function getIntakeProgramDetail($idIntake,$IdProgram,$IdProgramScheme,$IdStudentCategory) {
        $db = Zend_Db_Table::getDefaultAdapter();
		
	    	$select = $db->select()
						 ->from(array("ip"=>$this->_name))
						 ->where('ip.IdProgram =?',$IdProgram)
						 ->where('ip.IdProgramScheme =?',$IdProgramScheme)
						  ->where('ip.IdStudentCategory =?',$IdStudentCategory)
						   ->where('ip.IdIntake  =?',$idIntake);											
			$row = $db->fetchRow($select);
			return $row;			
    }
}
?>