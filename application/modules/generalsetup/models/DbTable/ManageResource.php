<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 28/6/2016
 * Time: 11:03 AM
 */
class GeneralSetup_Model_DbTable_ManageResource extends Zend_Db_Table {

    static function getTopMenu(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_top_menu'))
            ->order('a.tm_id');

        $result = $db->fetchAll($select);
        return $result;
    }

    static function getTitle($homeid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sidebar_menu'))
            ->where('a.sm_parentid = ?', 0)
            ->where('a.sm_tm_id = ?', $homeid);

        $result = $db->fetchAll($select);
        return $result;
    }

    static function getScreen($titleid){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_sidebar_menu'))
            ->where('a.sm_parentid != ?', 0)
            ->where('a.sm_parentid = ?', $titleid);

        $result = $db->fetchAll($select);
        return $result;
    }
}