<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_Model_DbTable_ErpUserSync extends Zend_Db_Table_Abstract{
    
    public function getLog($max, $min){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'erp_user_sync_log'), array('value'=>'*'))
            ->where('a.erp_log_id BETWEEN '.$min.' AND '.$max);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getLogAll(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'erp_user_sync_log'), array('value'=>'*'))
            ->order('a.erp_log_id DESC');
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getFileName(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'erp_user_sync_log'), array('erp_log_filename'))
            ->group('erp_log_filename');
        
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>