<?php

/**
 * RegistrarList
 * 
 * @author Arun
 * @version 
 */

require_once 'Zend/Db/Table/Abstract.php';

class GeneralSetup_Model_DbTable_Chiefofprogram extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_chiefofprogramlist';
	protected $_primary = "IdChiefOfProgramList";
	private $lobjDbAdpt;
	
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}	
	
	public function fninsertChiefofProgramList($larrformdata,$lintIdProgram)
	{
		$auth = Zend_Auth::getInstance();
		
    	$larrreglst['IdProgram'] = $lintIdProgram;
    	$larrreglst['IdStaff'] = $larrformdata['IdStaff'];
    	$larrreglst['FromDate'] = $larrformdata['FromDate'];    	
    	$larrreglst['Active'] = 1;
    	$larrreglst['IdCollege'] = $larrformdata['IdCollege'];
    	$larrreglst['IdDepartment'] = $larrformdata['IdDepartment'];
    	$larrreglst['UpdDate'] = date('Y-m-d H:i:s');
    	$larrreglst['UpdUser'] = $auth->getIdentity()->iduser;
    	$larrreglst['createddt'] = date('Y-m-d H:i:s');
    	$larrreglst['createdby'] = $auth->getIdentity()->iduser;
    	
		$this->insert($larrreglst);
		
	}	
	
	
	
	public function fngetChiefofProgramList($lintIdProgram)
	{
		$lstrselectsql1 = $this->lobjDbAdpt->select()
									->from(array('cop'=>'tbl_chiefofprogramlist'),array('IdChiefOfProgramList'=>'MAX(cop.IdChiefOfProgramList)'))
									->where("cop.IdProgram = $lintIdProgram");
									//->where("reglst.IdStaff = ".$larrformdata['IdStaff']);
		$larrresultset1 = $this->lobjDbAdpt->fetchRow($lstrselectsql1);			
		
		if(!empty($larrresultset1['IdChiefOfProgramList']))
		{
			$lstrselectsql = $this->lobjDbAdpt->select()
									->from(array('cop'=>'tbl_chiefofprogramlist'),array('IdStaff','FromDate','IdChiefOfProgramList'))
									->where("cop.IdChiefOfProgramList = ".$larrresultset1['IdChiefOfProgramList']);
									//->where("reglst.IdStaff = ".$larrformdata['IdStaff']);
			$larrresultset = $this->lobjDbAdpt->fetchRow($lstrselectsql);	

			return $larrresultset;
		}
		else 
			return 0;
		
	}
	
	public function deleteAllData($id){
		$this->delete("IdProgram = '".$id."'");
	}
	
	public function updateData($data,$id){
		$this->update($data, "IdChiefOfProgramList = '".$id."'");
	}
	
	public function getCurrentDean($idProgram){			
		
		$select = $this->lobjDbAdpt->select()	
		 			   ->from('tbl_chiefofprogramlist')
					   ->where('IdProgram  = ?',$idProgram)
					   ->order('IdChiefOfProgramList desc');
		return $this->lobjDbAdpt->fetchRow($select);		
	}  

	public function getHistoryDeanList($idProgram){			
		
		$select = $this->lobjDbAdpt->select()	
		 			   ->from(array('d'=>'tbl_chiefofprogramlist'))
		 			   ->join(array('s'=>'tbl_staffmaster'),'s.IdStaff=d.IdStaff',array('dean_name'=>'Fullname'))
		 			   ->join(array('c'=>'tbl_collegemaster'),'c.IdCollege=d.IdCollege',array('CollegeName'))
		 			   ->joinLeft(array("dp"=>"tbl_departmentmaster"),'dp.IdDepartment=d.IdDepartment',array('DepartmentName'))
					   ->where('IdProgram  = ?',$idProgram)
					   ->order('IdChiefOfProgramList desc');
		return $this->lobjDbAdpt->fetchAll($select);		
	}  
	
	public function isDean($IdStaff){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$auth = Zend_Auth::getInstance();
		
		$my_program  = array();
		 
		//daaptkan lecturer ni dean utk program mana?	
    	$select = $db->select()	
	 			   ->from('tbl_chiefofprogramlist',array('IdProgram','FromDate'=>'MAX(FromDate)'))
				   ->where('IdStaff  = ?',$IdStaff)
				   ->group('IdStaff')
				   ->group('IdProgram');
	 	$rows_program = $db->fetchAll($select);

 		if(count($rows_program)>0){
 	 
	 		foreach($rows_program as $program){		
	 	
		 	 //get latest dean kalo dia ok kalo bukan remove from list
		 	$select2 = $db->select()	
		 			   ->from('tbl_chiefofprogramlist')
					   ->where('IdProgram  = ?',$program['IdProgram'])
					   ->order('FromDate DESC')
					   ->order('IdChiefOfProgramList DESC');
			 $dean = $db->fetchRow($select2);
			 
		 		if($dean['IdStaff']==$auth->getIdentity()->IdStaff){
		 		array_push($my_program,$program['IdProgram']);
			 	}
	 		}
	 		
 		}//end if
 		
 		return $my_program;
	}
	
	public function isGSDean(){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$auth = Zend_Auth::getInstance();
		
		$flag = false;
		 
		//daaptkan lecturer ni dean utk program GS?	
    	$select = $db->select()	
	 			   ->from('tbl_chiefofprogramlist',array('IdProgram','FromDate'=>'MAX(FromDate)'))
				   ->where('IdStaff  = ?',$auth->getIdentity()->IdStaff)
				   ->group('IdStaff')
				   ->group('IdProgram');
	 	$rows_program = $db->fetchAll($select);
	 	
 		if(count($rows_program)>0){
 	 
	 		foreach($rows_program as $program){		
	 	
	 			if($program['IdProgram']==1 || $program['IdProgram']==3 || $program['IdProgram']==20){
	 				
	 				//get latest dean kalo dia ok kalo bukan remove from list
			 	 	$select2 = $db->select()	
			 			   ->from('tbl_chiefofprogramlist')
						   ->where('IdProgram  = ?',$program['IdProgram'])
						   ->order('FromDate DESC');
				 	$dean = $db->fetchRow($select2);
				 
			 		if($dean['IdStaff']==$auth->getIdentity()->IdStaff){
			 			$flag = true;
				 	}
	 			}
			 	 	
	 		}
 		}//end if
 		
 		return $flag;
	}
	
}