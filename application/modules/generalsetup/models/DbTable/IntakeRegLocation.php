<?php
class GeneralSetup_Model_DbTable_IntakeRegLocation extends Zend_Db_Table {
	
	protected $_name = 'intake_registration_location'; 
	protected $_primary = 'rl_id';
	
	public function addData($data){		
	   $this->insert($data);
	   $db = Zend_Db_Table::getDefaultAdapter();
	   return $db->lastInsertId();;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getDatabyId($id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
    	$select = $db->select()
					 ->from(array("rl"=>$this->_name))					
					 ->where($this->_primary.'=?',$id);											
		$row = $db->fetchRow($select);
		return $row;
	}
	
	public function getLocationList($idIntake){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
    	$select = $db->select()
					 ->from(array("rl"=>$this->_name))
					 ->join(array('b' => 'tbl_branchofficevenue'),'b.IdBranch=rl.rl_branch',array('BranchName'))
					 ->where('rl.IdIntake  =?',$idIntake);											
		$row = $db->fetchAll($select);
		return $row;
	}
	
	public function checkDuplicate($idIntake,$idBranch){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
    	$select = $db->select()
					 ->from(array("rl"=>$this->_name))
					 ->where('rl.IdIntake  =?',$idIntake)
					 ->where('rl.rl_branch  =?',$idBranch);											
		$row = $db->fetchRow($select);
		return $row;
	}
   	
}
?>