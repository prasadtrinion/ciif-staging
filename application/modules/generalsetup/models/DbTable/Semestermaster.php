<?php 
class GeneralSetup_Model_DbTable_Semestermaster extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_semestermaster';
    protected $_primary = 'IdSemesterMaster';
    
    private $lobjDbAdpt;
    protected $_locale;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
	}
    
	public function fnGetSemestermaster($semester_id){
		$lstrSelect = $this->lobjDbAdpt->select()
		->from(array("a"=>"tbl_semestermaster"))
		->where('a.IdSemesterMaster = ?',$semester_id)
		->order("a.SemesterMainName Desc");
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		
		if($larrResult){
			return $larrResult;
		}else{
			return null;
		}
	}
	
	public function fngetSemestermainDetails($IdSemesterMaster="", $Scheme='') 
	{
		if(trim($IdSemesterMaster) == "") 
		{
			
			$select = $this->select()
					->setIntegrityCheck(false)
					->from(array('a'=>$this->_name))
					->join(array('s'=>'tbl_scheme'),'s.IdScheme=a.IdScheme',array('EnglishDescription','ArabicDescription'))
					//->join(array('acy' => 'tbl_academic_year'),'acy.ay_id = a.idacadyear',array("academicYear"=>'ay_code', 'ay_id'=>'ay_id'))
					->order('a.SemesterMainName ASC');
					
			if ( $Scheme != '' )
			{
				$select->where('s.SchemeCode = ?', $Scheme);
			}
			
			$result = $this->fetchAll($select);
		}
		else
		{
			$select = $this->select()
					  ->setIntegrityCheck(false)
					  ->from(array('a'=>$this->_name))
						->join(array('s'=>'tbl_scheme'),'s.IdScheme=a.IdScheme',array('EnglishDescription','ArabicDescription'))
					 // ->join(array('acy' => 'tbl_academic_year'),'acy.ay_id = a.idacadyear',array("academicYear"=>'ay_code', 'ay_id'=>'ay_id'))
					  ->where("a.IdSemesterMaster = $IdSemesterMaster")
					  ->order('a.SemesterMainName ASC');

			$result = $this->fetchAll($select);
		}

		return $result->toArray();
	}
    
    public function fnaddSemester($data) { //Function for adding the University details to the table    	        
    	$this->insert($data);
    	$insertId = $this->lobjDbAdpt->lastInsertId('tbl_semestermaster','IdSemesterMaster');	
	    return $insertId;
	}
    
    public function fnupdateSemester($formData,$lintIdSemesterMaster) { //Function for updating the university
    	unset($formData['SemesterMainCode']);    	
    	unset($formData['Save']);
		$where = "IdSemesterMaster = '".$lintIdSemesterMaster."'";
		$this->update($formData,$where);
    }
    
	public function fnSearchSemester($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_semestermaster'),array('IdSemesterMaster'))
			   ->where('a.SemesterMainName  like "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	/**
	 * 
	 * Method to get all list of semester main to be diaplyed in a dropdown
	 */
	public function fnGetSemestermasterList(){
		$lstrSelect = $this->lobjDbAdpt->select()
 				 ->from(array("a"=>"tbl_semestermaster"),array("key"=>"a.IdSemesterMaster","value"=>"a.SemesterMainName",'value2'=>'SemesterMainDefaultLanguage','SemesterMainCode'))
                                 ->where('a.display = ?', 1)
				->joinLeft(array('s'=>'tbl_scheme'),'s.IdScheme=a.IdScheme',array('SchemeCode'))
 				 ->order("SemesterMainStartDate DESC");
				//->order("a.SemesterCountType DESC")
				//->order("a.SemesterFunctionType DESC");
		
		$results = $this->lobjDbAdpt->fetchAll($lstrSelect);
		foreach($results as $key => $result) {
			$data = $result;
			$data['value'] = $result['value'] . " - " . $result['SchemeCode'];
			$data['value2'] = $result['value2'] . " - " . $result['SchemeCode'];
			$results[$key] = $data;
		}
		return $results;
	}
	
	public function getData($id=0){
		$id = (int)$id;
		
		if($id!=0){
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from($this->_name)
					->where('IdSemesterMaster = ?', $id);
					
				$row = $db->fetchRow($select);
		}else{
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from($this->_name)
					->order('SemesterMainStartDate DESC');
								
			$row = $db->fetchAll($select);
		}
		
		if(!$row){
			throw new Exception("There is No Data");
		}
		
		return $row;
	}
	
	
	/* List Countable Semester */
	public function getCountableSemester(){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
					->from(array('a'=>'tbl_semestermaster'),array("key"=>"a.IdSemesterMaster","value"=>"a.SemesterMainName"))
						 ->where('IsCountable=1');
		//echo $select;
		
		$row = $db->fetchAll($select);
		
		return $row;
	}
	
	
	/* Regitration Semester */
	public function getSemesterCourseRegistration($id_semester){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
					     ->from(array('sm'=>'tbl_semestermaster'))						
						 ->join(array('ac'=>'tbl_activity_calender'),'ac.IdSemesterMain = sm.IdSemesterMaster')
						 ->where('CURDATE()	BETWEEN ac.StartDate AND ac.EndDate')
						 ->where('idActivity=18')	
						 ->where('IdSemesterMaster = ?',$id_semester);
		
		$row = $db->fetchRow($select);
		
		return $row;
	}
	
	public function getProgramSemester($idBranch,$sem_seq){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$semList = array();
				
	   $select = $db->select()
	   				  ->from(array('sm'=>'tbl_semestermaster'), array("key"=>"sm.IdSemesterMaster", "value"=>"sm.*"))		   				  	 			 
		 			  ->where("sm.IsCountable =?",1) 
		 			  ->where('Branch = ?',$idBranch)
		 			  ->where('sem_seq = ?',$sem_seq);               
			                                                         
		$row = $db->fetchAll($select);
		                                
		return $row; 
	}
	
	public function getCurrentSemesterScheme($idScheme) {

        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select = $db->select()
					     ->from(array('sm'=>'tbl_semestermaster'))						
						 ->where('CURDATE()	BETWEEN sm.SemesterMainStartDate AND sm.SemesterMainEndDate')
						 ->where('sm.IdScheme = ?',$idScheme);
        
        $row = $db->fetchRow($select);
        
        if($this->_locale == 'en_US'){
            $row['SemName'] = $row['SemesterMainName'];
        }
        else{
            $row['SemName'] = $row['SemesterMainDefaultLanguage'];
        }
        return $row;
    }	
    
    
    public function SearchSemester($formData=null){
		$lstrSelect = $this->lobjDbAdpt->select()
 				 ->from(array("a"=>"tbl_semestermaster"),array("key"=>"a.IdSemesterMaster","value"=>"a.SemesterMainName",'value2'=>'SemesterMainDefaultLanguage'))
 				 ->order("SemesterMainStartDate DESC");
				
		if($formData){
			if($formData['IdSemester']!=''){
				$lstrSelect->where('IdSemesterMaster = ?',$formData['IdSemester']);
			}
		}
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		
		return $larrResult;
	}

	public function getSemBySeq($year,$sem_seq){
		
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$semList = array();
				
	   $select = $db->select()
	   				  ->from(array('sm'=>'tbl_semestermaster'))
					  ->where('AcademicYear = ?',$year)
		 			  ->where('sem_seq = ?',$sem_seq);               
			                                                         
		$row = $db->fetchRow($select);
		                                
		return $row; 
	}
	
	public function fnGetSemesterList($scheme=null){
		$lstrSelect = $this->lobjDbAdpt->select()
 				 ->from(array("a"=>"tbl_semestermaster"),array("key"=>"a.IdSemesterMaster","value"=>"a.SemesterMainName",'value2'=>'SemesterMainDefaultLanguage','SemesterMainCode'))
				 ->joinLeft(array('s'=>'tbl_scheme'),'s.IdScheme=a.IdScheme',array('SchemeCode'))
				 ->where('a.SemesterMainName NOT IN ("Credit Transfer","Audit","Exemption")')
 				 ->order("SemesterMainStartDate DESC");
 				 
 				 if(isset($scheme) && $scheme!=''){
 				 	$lstrSelect->where('a.IdScheme = ?',$scheme);
 				 }
		
		$results = $this->lobjDbAdpt->fetchAll($lstrSelect);
		foreach($results as $key => $result) {
			$data = $result;
			$data['value'] = $result['value'] . " - " . $result['SchemeCode'];
			$data['value2'] = $result['value2'] . " - " . $result['SchemeCode'];
			$results[$key] = $data;
		}
		return $results;
	}
	
    public function getSemesterBasedOnIntake($year, $month){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_semestermaster'), array('value'=>'*'))
            ->where('a.AcademicYear = ?', $year)
            ->where('a.sem_seq = ?', $month);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getIntakeInfo($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_intake'), array('value'=>'*'))
            ->where('a.IdIntake = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
	public function getPreviousSemester($semester){
		$db = Zend_Db_Table::getDefaultAdapter(); 
		
		
		if($semester){
			$startDate = $semester["SemesterMainStartDate"];
			$scheme = $semester["IdScheme"];
		}else{
			$startDate =  'CURDATE()';
			$scheme = '0';
		}
		 $sql = $db->select()
			->from(array('sm'=>'tbl_semestermaster'))
			->where("SemesterMainStartDate < ?",$startDate)
			->where("IsCountable = 1");
			
			if($semester){
				$sql->where("IdScheme = ?",$scheme);
			}
			
			$sql->order('SemesterMainStartDate desc')
			->limit(1,0);
		$result = $db->fetchRow($sql);
		
		return $result;				
	}
	
	public function getSemesterOnwards()
    {
       	$db = Zend_Db_Table::getDefaultAdapter();	
        
        $sql = $db->select()
                  ->from(array('sm' => $this->_name))                 
                  //->where('(( sm.SemesterMainStartDate <= CURDATE() AND sm.SemesterMainEndDate >=  CURDATE() ) OR (sm.SemesterMainStartDate >= CURDATE() ))')
                  ->where('sm.SemesterMainName NOT IN ("Credit Transfer","Audit","Exemption")')    
                  ->where("IsCountable = 1")         
                  ->order('sm.SemesterMainStartDate DESC');
        $result = $db->fetchAll($sql); 
                
        return $result;
    	
    }

    public function getActivity($sem, $id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_activity_calender'), array('value'=>'*'))
			->where('a.IdSemesterMain = ?', $sem)
			->where('a.IdActivity = ?', $id);

		$result = $db->fetchRow($select);
		return $result;
	}
}
?>