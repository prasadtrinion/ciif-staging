<?php 

class GeneralSetup_Model_DbTable_CourseGroup extends Zend_Db_Table_Abstract {
	
	protected $_name = 'tbl_course_tagging_group';
	protected $_primary = "IdCourseTaggingGroup";
	
	
	public function addData($data){		
	   $db = Zend_Db_Table::getDefaultAdapter();	
	   $this->insert($data);
	   return $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getGroupList($idSubject,$idSemester){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					 // ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'subjectMainDefaultLanguage'))
					  ->where('IdSubject = ?',$idSubject)
					  ->where('IdSemester = ?',$idSemester);
		
		// echo $select;
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	public function getTotalGroupByCourse($idCourse,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from($this->_name)
					  ->where("IdSubject = ?",$idCourse)
					  ->where('IdSemester = ?',$idSemester);					  
		 $row = $db->fetchAll($select);	
		 
		 if($row)
		 	return count($row);
		 else
		 return 0;
	}

	public function getGroupInfoByCourse($idCourse,$idSemester){

		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db ->select()
			->from($this->_name)
			->where("IdSubject = ?",$idCourse)
			->where('IdSemester = ?',$idSemester);
		$row = $db->fetchAll($select);

		return $row;
	}
	
	public function getTotalGroupByCourseProgram($idCourse,$idSemester,$idProgram){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('ctg'=>$this->_name))
					  ->join(array('cgp'=>'course_group_program'),'cgp.group_id = ctg.IdCourseTaggingGroup',array())
					  ->join(array('d'=>'tbl_program'), 'cgp.program_id = d.IdProgram',array('ProgramCode'))
					  ->where("ctg.IdSubject = ?",$idCourse)
					  ->where('ctg.IdSemester = ?',$idSemester)
					  ->where('cgp.program_id = ?',$idProgram)
					  ->group('ctg.IdCourseTaggingGroup');
					  					  
		 $row = $db->fetchAll($select);	
		 
		 return $row;
	}
	
	public function getInfo($idGroup){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))					 
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'SubjectName','faculty_id'=>'IdFaculty','subjectMainDefaultLanguage'))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName','AcademicYear'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
                                          ->joinLeft(array('branch'=>'tbl_branchofficevenue'), 'cg.IdBranch=branch.IdBranch', array('branch.BranchName'))
					  ->where('IdCourseTaggingGroup = ?',$idGroup);	

		 $row = $db->fetchRow($select);	
		 return $row;
	}
	
	public function getInfoArray($idGroup){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))	
					  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('IdSubject'=>'GROUP_CONCAT(DISTINCT(sm.IdSubject))','subject_code'=>'GROUP_CONCAT( DISTINCT(sm.SubCode) SEPARATOR "/" )','subject_name'=>'SubjectName'))				 
//					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'SubjectName','faculty_id'=>'IdFaculty','subjectMainDefaultLanguage'))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName','AcademicYear'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->joinLeft(array('n' => 'tbl_branchofficevenue'), 'n.IdBranch=cg.IdBranch', array('branch_name' => 'GROUP_CONCAT( DISTINCT(n.BranchName) SEPARATOR "/" )'))
					  ->where("IdCourseTaggingGroup IN ($idGroup)")
					  ->group('sm.Group');

		 $row = $db->fetchRow($select);	
		 return $row;
	}
	
	public function getMarkEntryGroupList($idSubject,$idSemester){
		
		$auth = Zend_Auth::getInstance();
		
		//print_r($auth->getIdentity());
	
		$db = Zend_Db_Table::getDefaultAdapter();	

		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					 // ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'subjectMainDefaultLanguage'))
					  ->where('IdSubject = ?',$idSubject)
					  ->where('IdSemester = ?',$idSemester);	

		 $rows = $db->fetchAll($select);	
		 	
		
		 if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=298){//sekiranya bukan admin dia boleh tgk group dia sahaja
		 	
		 	
				 foreach($rows as $index=>$row){		 	
				 	
				 	//cari list lecturer dalam group tu
				 	//course group schedule
					$select_group_schedule = $db ->select()
								 				 ->from(array('cgs'=>'course_group_schedule'))
								 				 ->where("cgs.idGroup = ?",$row["IdCourseTaggingGroup"])
								 				 ->where("cgs.IdLecturer = ?",$auth->getIdentity()->iduser);
					$lecturer = $db->fetchRow($select_group_schedule);	
											
					if(!$lecturer["sc_id"]){							
						//check adakah dia coordinator
						if($row["IdLecturer"]!=$auth->getIdentity()->iduser){
							unset($rows[$index]);
						}
					}
								
				 }//end foreach
		 	
		 }//end if
		 
		
		 return $rows;
	}
	
	
	public function getMarkApprovalGroupList($subject_id,$idSemester){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'subjectMainDefaultLanguage'))
					  ->where('IdSemester = ?',$idSemester);	

		 if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=298){//sekiranya bukan admin dia boleh tgk group dia sahaja
		 	$select->where("cg.VerifyBy = ?",$auth->getIdentity()->IdStaff);
		 }else{
		 	 $select->where('cg.IdSubject = ?',$subject_id);
		 }	
		 
		
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	
	public function getCourseTaggingGroupList($idSemester){		
		
		$auth = Zend_Auth::getInstance();
		
		$id_user = $auth->getIdentity()->IdStaff;
		$idRole  = $auth->getIdentity()->IdRole;
		
	/*	$id_user = 747;
		$idRole=3;*/
		
		$db = Zend_Db_Table::getDefaultAdapter();	
		
		
		//check if user is coordinator so display all
	    $select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->join(array('cgs'=>'course_group_schedule'),'cgs.idGroup=cg.IdCourseTaggingGroup',array())
					  ->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','coordinator'=>'FullName','BackSalutation'))
					  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('IdSubject','subject_code'=>'SubCode','subject_name'=>'subjectMainDefaultLanguage'))					  
					  ->where('cg.IdSemester = ?',$idSemester)					  
					  ->order('SubCode')
					  ->order('IdCourseTaggingGroup');	
					  
					  if($idRole!=1){ //bukan admin
					  	$select->where('(cg.IdLecturer = ?',$id_user);
					  	$select->orwhere('cgs.IdLecturer = ?)',$id_user);
					  }		
		
		 $rows = $db->fetchAll($select);
		 return $rows;				
	}
	
	public function validateCodeGroup($code){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->where('GroupCode = ?',$code);
		
		// echo $select;
		 $row = $db->fetchRow($select);	
		 
		 if($row)
		 	return true;
		 else
		 	return false;
	}
	
	
	public function getCourseGroupListByProgram($formData=null){
		//dd($formData); exit;
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->join(array('gp'=>'course_group_program'),'gp.group_id=cg.IdCourseTaggingGroup',array('IdProgram'=>'program_id'))
					  ->join(array('p'=>'tbl_program'),'p.IdProgram=gp.program_id',array('ProgramName','ArabicName','ProgramCode'))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=stm.FrontSalutation',array('FrontSalutation'=>'DefinitionCode'))
	 				  ->joinLeft(array('df'=>'tbl_definationms'),'df.idDefinition=stm.BackSalutation',array('BackSalutation'=>'DefinitionCode'))
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('SubCode','subjectMainDefaultLanguage','SubjectName'))
					  ->order('sm.SubCode')
					  ->group('cg.IdCourseTaggingGroup');
					  //->group('gp.program_id'); ignore program sebab mark entry lecturer buat by group

		 if(isset($formData['IdSemesterArray']) && count($formData['IdSemesterArray']) > 0){
		 			 $select->where('cg.IdSemester IN (?)',$formData['IdSemesterArray']);
		 } 
		 
		 if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
		 			 $select->where('cg.IdSemester = ?',$formData['IdSemester']);
		 } 
		 
		 if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
		 			 $select->where('gp.program_id = ?',$formData['IdProgram']);
		 }
		 
		 if(isset($formData['IdSubject']) && $formData['IdSubject']!=''){
		 			 $select->where('cg.IdSubject = ?',$formData['IdSubject']);
		 }	
		 
		 if(isset($formData['subject_code']) && $formData['subject_code']!=''){
		 			 $select->where('sm.SubCode LIKE ?','%'.$formData['subject_code'].'%');
		 }

	 	 if(isset($formData['subject_name']) && $formData['subject_name']!=''){
		 			 $select->where('sm.SubjectName LIKE ?','%'.$formData['subject_name'].'%');
		 }

		 if(isset($formData['IdLecturer']) && $formData['IdLecturer']!=''){
		 			 $select->where('cg.IdLecturer = ?',$formData['IdLecturer']);
		 }

		if(isset($formData['IdGroup']) && $formData['IdGroup']!=''){
			$select->where('cg.IdCourseTaggingGroup = ?',$formData['IdGroup']);
		}
		 
		 if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin				 	
		 	$select->where('cg.IdLecturer = ?',$auth->getIdentity()->IdStaff);
		 }
			 
		
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	
	public function getCourseGroupDistList($formData=null){
		//NOTE:unapprove or entry status only
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->join(array('gp'=>'course_group_program'),'gp.group_id=cg.IdCourseTaggingGroup',array('IdProgram'=>'program_id'))
					  ->join(array('p'=>'tbl_program'),'p.IdProgram=gp.program_id',array('ProgramName','ArabicName'))
					  ->join(array('mdm'=>'tbl_marksdistributionmaster'),'mdm.IdCourseTaggingGroup=cg.IdCourseTaggingGroup',array())
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('SubCode','subjectMainDefaultLanguage','SubjectName'))
					  ->group('cg.IdCourseTaggingGroup');
					 // ->group('gp.program_id'); 

		 
		 if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
		 			 $select->where('cg.IdSemester = ?',$formData['IdSemester']);
		 } 
		 
	 	 if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
		 			 $select->where('gp.program_id = ?',$formData['IdProgram']);
		 }
		 
		 if(isset($formData['IdSubject']) && $formData['IdSubject']!=''){
		 			 $select->where('cg.IdSubject = ?',$formData['IdSubject']);
		 }
		 
		 if(isset($formData['Status']) && $formData['Status']!=''){
		 	$select->where('mdm.Status = ?',$formData['Status']);
		 }else{
		 	$select->where('mdm.Status IN (0,2)');//entry & Reject
		 }
		 
		// echo $select;
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	public function getGroupCopy($formData=null){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->join(array('gp'=>'course_group_program'),'gp.group_id=cg.IdCourseTaggingGroup',array('IdProgram'=>'program_id'))
					  ->join(array('p'=>'tbl_program'),'p.IdProgram=gp.program_id',array('ProgramName','ArabicName'))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('SubCode','subjectMainDefaultLanguage','SubjectName'))
					  ->where('cg.IdCourseTaggingGroup != ?',$formData['idGroupFrom'])
					  ->group('cg.IdCourseTaggingGroup')
					  ->group('gp.program_id');
					  					  
		 if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
		 			 $select->where('cg.IdSemester = ?',$formData['IdSemester']);
		 } 
		 
		 if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
		 			 $select->where('gp.program_id = ?',$formData['IdProgram']);
		 }
		 
		 if(isset($formData['IdSubject']) && $formData['IdSubject']!=''){
		 			 $select->where('cg.IdSubject = ?',$formData['IdSubject']);
		 }
		 
		 
		 if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin					 	
		 	$select->where('cg.IdLecturer = ?',$auth->getIdentity()->IdStaff);
		 }
			
		 //echo $select;
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	public function getGroup($idSemester,$idSubject,$idProgram,$idScheme){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();	
			
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->joinLeft(array('cgp'=>'course_group_program'),'cg.`IdCourseTaggingGroup` = cgp.group_id')
					  ->where('cg.IdSubject = ?',$idSubject)
					  ->where('cg.IdSemester = ?',$idSemester)
					  ->where('cgp.program_id = ?',$idProgram)
					  ->where('cgp.program_scheme_id = ?',$idScheme);
		
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	public function getGroupQuota($idSemester,$idSubject,$idProgram,$idScheme,$idBranch,$noscheme=1){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();	
			
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->join(array('cgp'=>'course_group_program'),'cg.`IdCourseTaggingGroup` = cgp.group_id')
					  ->join(array('ps'=>'tbl_program_scheme'),'ps.IdProgramScheme=cgp.program_scheme_id')
					  ->join(array('def'=>'tbl_definationms'),'def.idDefinition=ps.mode_of_study',array('progmode'=>'DefinitionDesc'))
					  ->where('cg.IdSubject = ?',$idSubject)
					  ->where('cg.IdSemester = ?',$idSemester)
					  //->where('(cg.IdBranch = 0 OR cg.IdBranch = ?)',$idBranch) // 5-2-2015 10:29pg dicommentkan sebab azila kata xyah cek branch
					  ->where('cgp.program_id = ?',$idProgram)
					  ->group('cg.IdCourseTaggingGroup')
					  ;
					  
		if($noscheme==1){
			$select = $select->where('cgp.program_scheme_id IN (?)',$idScheme);
			
		}
		//echo $select."<hr>";
		
		 $rows = $db->fetchAll($select);	
		 
		 foreach($rows as $index=>$row){
		 	//get total student tagged to this group
		 	$select2 = $db ->select()
					  	  ->from(array('srs'=>'tbl_studentregsubjects'),array('total'=>'count(IdCourseTaggingGroup)'))
					      ->where('srs.IdCourseTaggingGroup = ?',$row['IdCourseTaggingGroup']);
			//echo $select2;
			$result = $db->fetchRow($select2);
			//echo $result["total"]."-".$row['maxstud'];
			if($result['total']<$row['maxstud']){
				//allow
				//echo "True<br>";
				$rows[$index]['quota']=true;
			}else{
				//echo "False<br>";
				$rows[$index]['quota']=false;
			}
		 }
		 return $rows;
	}
	public function getLectCourseGroup($idsubject,$idsemester){
		$db = Zend_Db_Table::getDefaultAdapter();	
			
		$select = $db ->select()
					  ->distinct()
					  ->from(array('cg'=>$this->_name),array("IdLecturer"))
					  ->join(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation')) 
					  ->join(array('tu'=>'tbl_user'),'stm.IdStaff=tu.IdStaff',array('loginName'))
					  ->where("IdSemester = ?",$idsemester)	
					  ->where("stm.Active = ?",1)
					  ->where("tu.UserStatus = ?",1)
					  ->where("IdSubject = ?",$idsubject);
					  //echo $select.'<br />';
		$rows = $db->fetchAll($select);
		return $rows;
	}	
	
	public function getGroupListLect($idSubject,$idSemester,$idstaff){
		
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->joinLeft(array('u' => 'tbl_user'),'u.IdStaff = cg.IdLecturer',array('appl_username' => 'loginName'))
					 // ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'subjectMainDefaultLanguage'))
					  ->where('IdSubject = ?',$idSubject)
					  ->where('IdLecturer = ?',$idstaff)
					  ->where('IdSemester = ?',$idSemester);
		
		// echo $select;
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	public function getCourseByLectGroup($idSemester,$program_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();	
		$auth = Zend_Auth::getInstance(); 
			
		$select = $db ->select()
					  ->distinct()
					  ->from(array('cg'=>$this->_name),array("IdLecturer"))
					  ->join(array('cgp'=>'course_group_program'),'cg.`IdCourseTaggingGroup` = cgp.group_id')
					  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('IdSubject','SubCode','subjectMainDefaultLanguage','SubjectName'))
					  ->where('cg.IdSemester = ?',$idSemester)
					  ->where('cgp.program_id = ?',$program_id)
					  ->group('cg.IdSubject');	
		
	 		if($auth->getIdentity()->IdRole!=1 && $auth->getIdentity()->IdRole!=455){ //admin	atau asad admin					 	
			 	$select->where('cg.IdLecturer = ?',$auth->getIdentity()->IdStaff);
			}
		
		$rows = $db->fetchAll($select);
		return $rows;
	}	
	
	
	
	public function getGroupDistByCoordinator($formData=null){
	
		$auth = Zend_Auth::getInstance();
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
			
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->join(array('gp'=>'course_group_program'),'gp.group_id=cg.IdCourseTaggingGroup',array('IdProgram'=>'program_id'))
					  ->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=gp.program_id',array('ProgramName','ArabicName'))
					  ->join(array('mdm'=>'tbl_marksdistributionmaster'),'mdm.IdCourseTaggingGroup=cg.IdCourseTaggingGroup',array())
					  ->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('SubCode','subjectMainDefaultLanguage','SubjectName'))
					  ->where('cg.IdSubject IN (SELECT IdSubject FROM tbl_subjectcoordinatorlist WHERE IdStaff = ? GROUP BY IdSubject)',$auth->getIdentity()->IdStaff)				 
					  ->group('cg.IdCourseTaggingGroup');
					
					  
		 if(isset($formData['IdSemesterArray']) && $formData['IdSemesterArray']!=''){
		 			 $select->where('cg.IdSemester IN (?)',$formData['IdSemesterArray']);
		 } 
		 
		 if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
		 			 $select->where('cg.IdSemester = ?',$formData['IdSemester']);
		 } 
		 
	 	 if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
		 			 $select->where('gp.program_id = ?',$formData['IdProgram']);
		 }
		 
		 if(isset($formData['IdSubject']) && $formData['IdSubject']!=''){
		 			 $select->where('cg.IdSubject = ?',$formData['IdSubject']);
		 }
		 		
		if(isset($formData['Status']) && $formData['Status']!=''){
		 	$select->where('mdm.Status = ?',$formData['Status']);
		 }else{
		 	$select->where('mdm.Status IN (0,2)');//entry & Reject
		 }
		
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	
	public function getProgramGroup($IdCourseTaggingGroup){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
				  ->from(array('cg'=>$this->_name),array())
				  ->join(array('gp'=>'course_group_program'),'gp.group_id=cg.IdCourseTaggingGroup',array('IdProgram'=>'program_id'))
				  ->where('cg.IdCourseTaggingGroup = ?',$IdCourseTaggingGroup);
				  
		$row = $db->fetchAll($select);	
		return $row;
	
	}
	
	
	public function getDeanCourseGroup($formData){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$auth = Zend_Auth::getInstance();
		
		 $my_program  = array();
		 
		 if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
			 	//filter by program
			 	
		 	 		//get latest dean kalo dia ok kalo bukan remove from list
				 	$select2 = $db->select()	
				 			   ->from('tbl_chiefofprogramlist')
							   ->where('IdProgram  = ?',$formData['IdProgram'])
							   ->where('IdStaff  = ?',$auth->getIdentity()->IdStaff)
							   ->order('FromDate DESC');
					$dean = $db->fetchRow($select2);
					 
					if($dean){
			 			array_push($my_program,$formData['IdProgram']);
					}
			 	
		 } else{
		 	
			 	//daaptkan lecturer ni dean utk program mana?	
	    		$select = $db->select()	
			 			   ->from('tbl_chiefofprogramlist',array('IdProgram','FromDate'=>'MAX(FromDate)'))
						   ->where('IdStaff  = ?',$auth->getIdentity()->IdStaff)
						   ->group('IdStaff')
						   ->group('IdProgram');
			 	$rows_program = $db->fetchAll($select);
			 	
		 		if(count($rows_program)>0){
		 	 
			 		foreach($rows_program as $program){		
			 	
				 	 //get latest dean kalo dia ok kalo bukan remove from list
				 	$select2 = $db->select()	
				 			   ->from('tbl_chiefofprogramlist')
							   ->where('IdProgram  = ?',$program['IdProgram'])
							   ->order('FromDate DESC');
					 $dean = $db->fetchRow($select2);
					 
				 		if($dean['IdStaff']==$auth->getIdentity()->IdStaff){
				 		array_push($my_program,$program['IdProgram']);
					 	}
			 		}
		 		}//end if
		 }
		 
		 
		
			 	
		 if(count($my_program)>0){
		 	 
			 $select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->join(array('gp'=>'course_group_program'),'gp.group_id=cg.IdCourseTaggingGroup',array('IdProgram'=>'program_id'))
					  ->join(array('p'=>'tbl_program'),'p.IdProgram=gp.program_id',array('ProgramName','ArabicName','ProgramCode'))
					  ->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=stm.FrontSalutation',array('FrontSalutation'=>'DefinitionCode'))
	 				  ->joinLeft(array('df'=>'tbl_definationms'),'df.idDefinition=stm.BackSalutation',array('BackSalutation'=>'DefinitionCode'))
					  ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('SubCode','subjectMainDefaultLanguage','SubjectName'))
					  ->where('gp.program_id IN (?)',$my_program)
					  ->order('sm.SubCode')
					  ->group('cg.IdCourseTaggingGroup');

			 if(isset($formData['IdSemesterArray']) && $formData['IdSemesterArray']!=''){
			 			 $select->where('cg.IdSemester IN (?)',$formData['IdSemesterArray']);
			 } 
			 
			 if(isset($formData['IdSemester']) && $formData['IdSemester']!=''){
			 			 $select->where('cg.IdSemester = ?',$formData['IdSemester']);
			 } 
			 		 
			 if(isset($formData['subject_code']) && $formData['subject_code']!=''){
			 			 $select->where('sm.SubCode LIKE ?','%'.$formData['subject_code'].'%');
			 }
	
		 	 if(isset($formData['subject_name']) && $formData['subject_name']!=''){
			 			 $select->where('sm.SubjectName LIKE ?','%'.$formData['subject_name'].'%');
			 }
	
			 if(isset($formData['IdLecturer']) && $formData['IdLecturer']!=''){
			 			 $select->where('cg.IdLecturer = ?',$formData['IdLecturer']);
			 }	
			 
			
			 $row = $db->fetchAll($select);	
			 return $row;
		 }else{
		 	return array();
		 }
		 		 
		
	}
	
	
	public function getCoordinatorProfile($idSubject){

		 	 $db = Zend_Db_Table::getDefaultAdapter();
		 	
		 	 $select = $db ->select()
					  ->from(array('so'=>'tbl_subjectcoordinatorlist'),array())
					  ->join(array('u'=>'tbl_staffmaster'),'u.IdStaff=so.IdStaff')
					  ->where('so.IdSubject = ?',$idSubject)
					  ->order('so.IdSubjectCoordinatorList DESC');
			 $row = $db->fetchRow($select);	
			 return $row;
	}
	
	public function getDeanProfile($idGroup){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		 
		 $group_program = $this->getProgramGroup($idGroup);
		 
		 $select = $db->select()	
			 			   ->from(array('c'=>'tbl_chiefofprogramlist'))
			 			   ->join(array('u'=>'tbl_staffmaster'),'u.IdStaff=c.IdStaff')
						   ->where('IdProgram  IN (?)',$group_program)
						   ->order('FromDate DESC');
		$row = $db->fetchRow($select);
		return $row;
				
	}
	
	public function isMyGroup($idGroup){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$auth = Zend_Auth::getInstance();
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))	
					  ->where('cg.IdCourseTaggingGroup = ?',$idGroup)
					  ->where('cg.IdLecturer = ?',$auth->getIdentity()->iduser);	

		 $row = $db->fetchRow($select);

		 
		 return $row;
	}

	public function getSchedule($idGroup,$item=''){

		if ( !isset($idGroup) ) return ;

		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db ->select()
			->from(array("a"=>'course_group_schedule'))
			->joinLeft(array("b"=>"tbl_staffmaster"),'b.IdStaff=a.IdLecturer',array('FullName','FirstName','SecondName','ThirdName'))
			->joinLeft(array("d"=>"tbl_definationms"),'d.idDefinition=a.sc_venue',array('DefinitionDesc','BahasaIndonesia'))
			->where('idGroup = ?',$idGroup);
		if ( $item != '' )
		{
			$select->where('a.idClassType = ?', $item);
		}

		$row = $db->fetchAll($select);

		return $row;
	}
}