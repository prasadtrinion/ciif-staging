<?php
class GeneralSetup_Model_DbTable_Migrate extends Zend_Db_Table 
{
	public function insertCoursemaster($data)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->insert('tbl_subjectmaster', $data );

		$_id = $db->lastInsertId();

		return $_id;
	}
	
	public function checkSubject($subcode='')
	{
		if ( $subcode == '' )
		{
			die('Invalid Subject Code (checkSubject)');
		}
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from('tbl_subjectmaster',array('SubCode'))
		->where('SubCode = ?',trim($subcode));
		
	
		$result = $lobjDbAdpt->fetchRow($lstrSelect);
		
		//echo $lstrSelect . (!empty($result) ? '- EXISTS':'') . '<br />';
				
		return empty($result) ? 0 : 1;
	}

	public function addLog($type='', $data = array(), $userId, $filename='' )
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$logdata = array(
							'MigrateType'	=> $type,
							'LogData'		=> json_encode($data),
							'UpdUser'		=>	$userId,
							'UpdDate'		=> 	new Zend_Db_Expr('NOW()'),
							'Filename'		=> $filename
						);
		$lobjDbAdpt->insert('tbl_migratereport', $logdata );
	}
	
	public function getLogs($type='')
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array('a' => 'tbl_migratereport') );
		
		if ( $type != '' )
		{
			$lstrSelect->where('a.MigrateType = ?',$type);
		}
		
		$lstrSelect	->joinLeft(array('b' => 'tbl_user'),'a.UpdUser=b.iduser', array('b.loginName'))
					->order('a.UpdDate DESC');

		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		
		return $result;
	}

	public function getLogsById($Id)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from('tbl_migratereport')
		->where('IdMigrate = ?',$Id);
		
	
		$result = $lobjDbAdpt->fetchRow($lstrSelect);
		
		return $result;
	}
}
?>