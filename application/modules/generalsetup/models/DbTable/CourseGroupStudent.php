<?php 

class GeneralSetup_Model_DbTable_CourseGroupStudent extends Zend_Db_Table_Abstract {
	/*JGN PAKAI TABLE NI LAGI UTK CEK COURSE GROUP STUDENT GUNA tbl_studentregsubjects*/
	protected $_name = 'tbl_course_group_student_mapping';
	protected $_primary = "Id";
	
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getTotalStudent($idGroup, $check = true){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
	
		$sql1 = $db ->select()		
					  ->from(array('srs'=>'tbl_studentregsubjects'))
					  ->join(array('sr'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())						
					  ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
					  ->where('srs.Active!=3')
					  //->where('sr.profileStatus = ?',92)
					  ->where('sr.registrationId != ?',"1409999")
					  ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT","U")');

		if ($check == true){
			$sql1->where('sr.profileStatus = ?',92);
		}
					  
		 $sql2 = $db ->select()		
					  ->from(array('srs'=>'tbl_studentregsubjects'))
					  ->join(array('sr'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())						
					  ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
					  ->where('srs.audit_program IS NOT NULL')
					  ->where('srs.Active!=3')
					  //->where('sr.profileStatus = ?',92)
					  ->where('sr.registrationId != ?',"1409999")
					  ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")');

		if ($check == true){
			$sql2->where('sr.profileStatus = ?',92);
		}

		 $select = $db->select()->union(array($sql1, $sql2));	    		       
		 $row = $db->fetchAll($select);	
		 
		 if($row)
		 	return count($row);
		 else
		 return 0;
	}
	
	public function removeStudent($idGroup,$idStudent){		
		
	  $this->delete("IdCourseTaggingGroup='". $idGroup ."' AND IdStudent = '".$idStudent."'");
	}
	
	public function getStudent($idGroup){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		/*$select = $db ->select()
					->from(array('gsm'=>$this->_name))
					->join(array('sr'=>'tbl_studentregistration'), 'sr.IdStudentRegistration = gsm.IdStudent')
					->joinLeft(array('sp'=>'student_profile'), 'sp.appl_id = sr.IdApplication')
					->where('IdCourseTaggingGroup = ?',$idGroup)
					->order('sr.registrationId');
		*/
		
		$select = $db ->select()
					->from(array('srs'=>'tbl_studentregsubjects'))
					->join(array('sr'=>'tbl_studentregistration'), 'sr.IdStudentRegistration = srs.IdStudentRegistration')
					->join(array('sp'=>'student_profile'), 'sp.id = sr.sp_id')
					->where('srs.IdCourseTaggingGroup = ?',$idGroup)
					->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
					->where('srs.Active != ?',3)
					->where('sr.profileStatus = ?',92)
					->order('sr.registrationId');
					
		$row = $db->fetchAll($select);
			
		if($row)
			return $row;
		else
			return null;
	}
	
	public function getStudentbyGroup($idGroup,$student=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();	
		
	    /*$select = $db ->select()
					->from(array('gsm'=>$this->_name))
					->join(array('sr'=>'tbl_studentregistration'), 'sr.IdStudentRegistration = gsm.IdStudent')
					->join(array('sp'=>'student_profile'), 'sp.appl_id = sr.IdApplication')
					->join(array('p'=>'tbl_program'), 'p.IdProgram=sr.IdProgram',array('ProgramName'=>'ArabicName','ProgramCode'))
					->where('IdCourseTaggingGroup = ?',$idGroup)				
					->order('sr.registrationId');*/
		
		$select = $db ->select()
					->from(array('srs'=>'tbl_studentregsubjects'))
					->join(array('sr'=>'tbl_studentregistration'), 'sr.IdStudentRegistration = srs.IdStudentRegistration')
					->join(array('sp'=>'student_profile'), 'sp.id = sr.sp_id')
					->join(array('p'=>'tbl_program'), 'p.IdProgram=sr.IdProgram',array('ProgramName'=>'ArabicName','ProgramCode'))
					->where('srs.IdCourseTaggingGroup = ?',$idGroup)
					->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
					->where('srs.Active != ?',3)
					->where('sr.profileStatus = ?',92)
					->order('sr.registrationId');

					
		if(isset($student)){
			$select->where("((sp.appl_fname LIKE '%".$student."%'");
			$select->orwhere("sp.appl_mname LIKE '%".$student."%'");
			$select->orwhere("sp.appl_lname LIKE '%".$student."%')");		
			$select->orwhere("sr.registrationId LIKE '%".$student."%')");
		}
		
		$row = $db->fetchAll($select);
		
		if($row)
			return $row;
		else
			return null;
	}
	
	public function checkStudentCourseGroup($IdStudentRegistration,$idSemester,$idSubject){
		
		/*Jgn pakai dah*/
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		 $select = $db ->select()		
					  ->from(array('cgsm'=>$this->_name))
					  ->join(array('ctg'=>'tbl_course_tagging_group'),'ctg.IdCourseTaggingGroup=cgsm.IdCourseTaggingGroup')
					  ->where('cgsm.IdStudent = ?',$IdStudentRegistration)
					  ->where('ctg.IdSemester = ?',$idSemester)
					  ->where('ctg.IdSubject = ?',$idSubject);			  
		 $row = $db->fetchRow($select);	
		 
		 return $row;
	}
	
	public function getTotalStudentViaSubReg($idGroup){
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		 $select = $db ->select()		
					  ->from(array('a'=>'tbl_studentregsubjects'),array('total'=>'count(*)'))
					  ->where('a.IdCourseTaggingGroup = ?',$idGroup)
					  ->where('IFNULL(a.exam_status,"Z") NOT IN ("EX","CT")')
					  ->where('a.Active != ?',3);	
		 $row = $db->fetchRow($select);	
		 
		 return $row["total"];		
	}
	
	
	public function checkStudentMappingGroup($idGroup,$IdStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		/* $select = $db ->select()		
					  ->from(array('cgsm'=>$this->_name))					
					  ->where('cgsm.IdStudent = ?',$IdStudentRegistration)					
					  ->where('cgsm.IdCourseTaggingGroup = ?',$idGroup);					  
		 $row = $db->fetchRow($select);	*/
		
		$select = $db ->select()		
					  ->from(array('srs'=>'tbl_studentregsubjects'))					
					  ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)					
					  ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
					  ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
					  ->where('srs.Active != ?',3);					  
		 $row = $db->fetchRow($select);
		 
		 if($row)
			return $row;
		else
			return null;
	}
	
	
	public function getTotalStudentMarkApprovedOld($idGroup){
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()		
					  ->from(array('a'=>'tbl_studentregsubjects'))
					   ->where('IdCourseTaggingGroup = ?',$idGroup)
					   ->where('mark_approval_status = 2')
					   ->where('a.Active != ?',3)	
					   ->where('IFNULL(a.exam_status,"Z") NOT IN ("EX","CT","U")');					  
		 $row = $db->fetchAll($select);	
		 
		  if($row)
		 	return count($row);
		 else
		 return 0;
		 
	}
	
	public function getTotalStudentMarkApproved($idGroup, $check = true){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
	
		$sql1 = $db ->select()		
					  ->from(array('srs'=>'tbl_studentregsubjects'))
					  ->join(array('sr'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())						
					  ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
					  ->where('srs.Active!=3')
					  //->where('sr.profileStatus = ?',92)
					  ->where('sr.registrationId != ?',"1409999")
					  ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT","U")')
					   ->where('mark_approval_status = 2');

		if ($check == true){
			$sql1->where('sr.profileStatus = ?',92);
		}
					  
		 $sql2 = $db ->select()		
					  ->from(array('srs'=>'tbl_studentregsubjects'))
					  ->join(array('sr'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())						
					  ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
					  ->where('srs.audit_program IS NOT NULL')
					  ->where('srs.Active!=3')
					  //->where('sr.profileStatus = ?',92)
					  ->where('sr.registrationId != ?',"1409999")
					  ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
					   ->where('mark_approval_status = 2');

		if ($check == true){
			$sql2->where('sr.profileStatus = ?',92);
		}

		 $select = $db->select()->union(array($sql1, $sql2));	    		       
		 $row = $db->fetchAll($select);	
		 
		 if($row)
		 	return count($row);
		 else
		 return 0;
	}
	
	public function getTotalStudentMarkSubmitApprovalOld($idGroup){
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()		
					  ->from(array('a'=>'tbl_studentregsubjects'))
					   ->where('IdCourseTaggingGroup = ?',$idGroup)
					   ->where('mark_approval_status = 1')
					   ->where('a.Active != ?',3)
					   ->where('IFNULL(a.exam_status,"Z") NOT IN ("EX","CT","U")')
					   ;					  
		 $row = $db->fetchAll($select);	
		 
		  if($row)
		 	return count($row);
		 else
		 return 0;
		 
	}
	
	public function getTotalStudentMarkSubmitApproval($idGroup, $check = true){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
	
		$sql1 = $db ->select()		
					  ->from(array('srs'=>'tbl_studentregsubjects'))
					  ->join(array('sr'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())						
					  ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
					  ->where('srs.Active!=3')
					  //->where('sr.profileStatus = ?',92)
					  ->where('sr.registrationId != ?',"1409999")
					  ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT","U")')
					   ->where('mark_approval_status = 1');

		if ($check == true){
			$sql1->where('sr.profileStatus = ?',92);
		}
					  
		 $sql2 = $db ->select()		
					  ->from(array('srs'=>'tbl_studentregsubjects'))
					  ->join(array('sr'=>'tbl_studentregistration'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())						
					  ->where('srs.IdCourseTaggingGroup = ?',$idGroup)
					  ->where('srs.audit_program IS NOT NULL')
					  ->where('srs.Active!=3')
					  //->where('sr.profileStatus = ?',92)
					  ->where('sr.registrationId != ?',"1409999")
					  ->where('IFNULL(srs.exam_status,"Z") NOT IN ("EX","CT")')
					   ->where('mark_approval_status = 1');

		if ($check == true){
			$sql2->where('sr.profileStatus = ?',92);
		}

		 $select = $db->select()->union(array($sql1, $sql2));	    		       
		 $row = $db->fetchAll($select);	
		 
		 if($row)
		 	return count($row);
		 else
		 return 0;
	}
	
	
}