<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_Model_DbTable_InstructorWorkload extends Zend_Db_Table {
    
    public function getPosition(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a"=>"tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
            ->join(array("b"=>"tbl_definationtypems"),"a.idDefType = b.idDefType AND defTypeDesc='Levels'")
            ->where("a.Status = 1")
            ->order("a.DefinitionDesc");
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getDefination($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'), array('value'=>'*'))
            ->where('a.idDefType = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function insertWorkload($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_workload_instructor', $bind);
        $id = $db->lastInsertId('tbl_workload_instructor', 'twi_id');
        return $id;
    }
    
    public function insertWorkloadIndividual($bind){
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('tbl_individual_workload', $bind);
        $id = $db->lastInsertId('tbl_individual_workload', 'tiw_id');
        return $id;
    }
    
    public function getWorkload($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_workload_instructor'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.twi_position = b.idDefinition', array('positionname'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.twi_workloadtype = c.idDefinition', array('workloadtypename'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_user'), 'a.twi_updby = iduser', array('updname'=>'d.loginName'))
            ->where('a.twi_id = ?', $id);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getWorkloadIndividual($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_individual_workload'), array('value'=>'*'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.tiw_workloadtype = c.idDefinition', array('workloadtypename'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_user'), 'a.tiw_updby = iduser', array('updname'=>'d.loginName'))
            ->where('a.tiw_staffid = ?', $id);
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function checkWorkload($position, $jobtype, $workloadtype){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_workload_instructor'), array('value'=>'*'))
            ->where('a.twi_position = ?', $position)
            ->where('a.twi_jobtype = ?', $jobtype)
            ->where('a.twi_workloadtype = ?', $workloadtype);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function checkWorkloadIndividual($staffid, $workloadtype){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_individual_workload'), array('value'=>'*'))
            ->where('a.tiw_staffid = ?', $staffid)
            ->where('a.tiw_workloadtype = ?', $workloadtype);
        
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function editWorkload($bind, $id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $update = $db->update('tbl_workload_instructor', $bind, 'twi_id = '.$id);
        return $update;
    }
    
    public function getWorkloadList(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_workload_instructor'), array('value'=>'*'))
            ->joinLeft(array('b'=>'tbl_definationms'), 'a.twi_position = b.idDefinition', array('positionname'=>'b.DefinitionDesc'))
            ->joinLeft(array('c'=>'tbl_definationms'), 'a.twi_workloadtype = c.idDefinition', array('workloadtypename'=>'c.DefinitionDesc'))
            ->joinLeft(array('d'=>'tbl_user'), 'a.twi_updby = iduser', array('updname'=>'d.loginName'));
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function deleteWorkload($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_workload_instructor', 'twi_id = '.$id);
        return $delete;
    }
    
    public function deleteWorkloadIndividual($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $delete = $db->delete('tbl_individual_workload', 'tiw_id = '.$id);
        return $delete;
    }
}