<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 24/1/2017
 * Time: 12:05 PM
 */
class GeneralSetup_Model_DbTable_Report extends Zend_Db_Table_Abstract {

    public function getLoginInformation($formData = false){
        $db = Zend_Db_Table::getDefaultAdapter();
        $selectUser = $db->select()
            ->from(array('a'=>'tbl_user'), array(
                'user'=>'b.FullName',
                'login_name'=>'a.loginName',
                'last_login'=>'a.LastLogin',
                'status'=>'(CASE a.UserStatus WHEN "1" THEN "Active" ELSE "Inactive" END)',
                'type'=>new Zend_Db_Expr ('"staff"')
            ))
            ->joinLeft(array('b'=>'tbl_staffmaster'), 'a.IdStaff = b.IdStaff', array());

        if ($formData != false){
            if (isset($formData['status']) && $formData['status']!='') {
                if ($formData['status'] == 92) {
                    $statusStaff = 1;
                } else {
                    $statusStaff = $formData['status'];
                }

                $selectUser->where($db->quoteInto('a.UserStatus = ?', $statusStaff));
            }

            if (isset($formData['type']) && $formData['type']!=''){
                if ($formData['type']!=1){
                    $selectUser->where($db->quoteInto('a.iduser = ?', 0));
                }
            }

            if (isset($formData['name']) && $formData['name']!=''){
                $selectUser->where($db->quoteInto('b.FullName like ?', '%'.$formData['name'].'%'));
            }

            if (isset($formData['loginname']) && $formData['loginname']!=''){
                $selectUser->where($db->quoteInto('a.loginName like ?', '%'.$formData['loginname'].'%'));
            }
        }

        $selectStudent = $db->select()
            ->from(array('a'=>'student_profile'), array(
                'user'=>'concat(a.appl_fname, " ", a.appl_lname)',
                'login_name'=>'a.appl_username',
                'last_login'=>'a.lastlogin',
                'status'=>'c.DefinitionDesc',
                'type'=>new Zend_Db_Expr ('"student"')
            ))
            ->joinLeft(array('b'=>'tbl_studentregistration'), 'a.appl_id  = b.sp_id', array())
            ->joinLeft(array('c'=>'tbl_definationms'), 'b.profileStatus  = c.idDefinition', array());

        if ($formData != false){
            if (isset($formData['status']) && $formData['status']!='') {
                $selectStudent->where($db->quoteInto('b.profileStatus = ?', $formData['status']));
            }

            if (isset($formData['type']) && $formData['type']!=''){
                if ($formData['type']!=2){
                    $selectStudent->where($db->quoteInto('a.appl_id = ?', 0));
                }
            }

            if (isset($formData['name']) && $formData['name']!=''){
                $selectStudent->where($db->quoteInto('CONCAT_WS(" ", a.appl_fname, a.appl_lname) like ?', '%'.$formData['name'].'%'));
            }

            if (isset($formData['loginname']) && $formData['loginname']!=''){
                $selectStudent->where($db->quoteInto('a.appl_username like ?', '%'.$formData['loginname'].'%'));
            }
        }

        $select = $db->select()->union(array($selectUser, $selectStudent))
            ->order('user');
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDefination($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>'tbl_definationms'))
            ->where('a.idDefType = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }
}