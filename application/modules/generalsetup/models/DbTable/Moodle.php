<?php
class GeneralSetup_Model_DbTable_Moodle { 	
	private $moodledb;

	public function connect(){
	
		$this->moodledb = new Zend_Db_Adapter_Pdo_Mysql(array(
		        'host'     => MOODLE_DBSERVER,
		        'username' => MOODLE_DBUSER,
		        'password' => MOODLE_DBPASS,
		        'dbname'   => MOODLE_DBNAME
				/*'host'     => 'localhost',
				'username' => 'root',
				'password' => '',
				'dbname'   => 'inceif_moodle'*/
		    ));
		
		$this->msemester = 1;
		$this->mcourse = 2;
		$this->mlecturer = 3;
		$this->mcontext_system = 10;
		$this->mcontext_user =30;
		$this->mcontext_coursecat = 40;
		$this->mcontext_course = 50;
		$this->mcontext_module = 70;
		$this->mcontext_block = 80;
		    
	}
	
	public function addSemester($data) { //as category
		$this->connect();
		$this->moodledb->insert("mdl_course_categories",$data);
		$category_id = $this->moodledb->lastInsertId();
		$data["id"]=$category_id;
		$data["path"] = "/$category_id";
		$data["depth"] = substr_count($data["path"], "/");
		
		$this->moodledb->update('mdl_course_categories', $data, "id = $category_id");		
		
		$context["contextlevel"] = $this->mcontext_coursecat;
		$context["instanceid"] = $category_id;
		$context["path"] = $data["path"];
		$context["depth"] = 0;
		$this->moodledb->insert("mdl_context",$context);
		$context_id = $this->moodledb->lastInsertId();
		$parent_context_path = "/1";
		
		$context["path"] = "$parent_context_path/$context_id";
		$context["depth"] = substr_count($context["path"], "/");
		
		$this->moodledb->update('mdl_context', $context, "id = $context_id");
	
		return $data;
	}
	
	public function addSubjectSemester($data) { //as sub-category
		$this->connect();
		//print_r($data);echo "<hr>";
		$parent_category = $this->getMoodleData("mdl_course_categories","id=".$data["parent"]);
		$parent_context = $this->getMoodleData("mdl_context","contextlevel=".$this->mcontext_coursecat." AND instanceid=".$data["parent"]);
		$parent_path = $parent_category["path"];	
		$parent_context_path = $parent_context["path"];
		
		$this->moodledb->insert("mdl_course_categories",$data);
		$category_id = $this->moodledb->lastInsertId();
		$data["id"]=$category_id;
		$data["path"] = "$parent_path/$category_id";
		$data["depth"] = substr_count($data["path"], "/");
		
		$this->moodledb->update('mdl_course_categories', $data, "id = $category_id");		
		
		$context["contextlevel"] = $this->mcontext_coursecat;
		$context["instanceid"] = $category_id;
		$context["path"] = $data["path"];
		$context["depth"] = 0;
		$this->moodledb->insert("mdl_context",$context);
		$context_id = $this->moodledb->lastInsertId();
		$context["path"] = "$parent_context_path/$context_id";
		$context["depth"] = substr_count($context["path"], "/");
		
		$this->moodledb->update('mdl_context', $context, "id = $context_id");	
		return $data;
	}
	
	public function addLectSubSem($course,$category) { //Real Course in moodle
		
		$this->connect();
		$category_context = $this->getMoodleData("mdl_context", "contextlevel=".$this->mcontext_coursecat." AND instanceid='".$category["id"]."'");
		$course["sortorder"] = $this->getLastSeq("mdl_course");
		$course["groupmode"] = 2;
		//echo "<pre>";print_r($course);echo "</pre>";exit;
		
		$this->moodledb->insert("mdl_course",$course);
		$course_id = $this->moodledb->lastInsertId();
		$course["id"]= $course_id;
				
		$context["contextlevel"] = $this->mcontext_course;
		$context["instanceid"] = $course_id;
		$context["path"] = $category_context["path"];
		$context["depth"] = 5;
		
		$this->moodledb->insert("mdl_context",$context);
		$course_context_id = $this->moodledb->lastInsertId();
		
		
		$context["path"] = $category_context["path"]."/$course_context_id";
		$context["depth"] = substr_count($context["path"], "/");
		$this->moodledb->update('mdl_context', $context, "id = $course_context_id");
		$course_context = $context["path"];
			
		$enrol["enrol"]='manual';
		$enrol["courseid"]=$course_id;
		$enrol["expirythreshold"]=86400;
		$enrol["roleid"]=5;
		$enrol["timecreated"]=time();
		$enrol["timemodified"]=time();	

		$this->moodledb->insert("mdl_enrol",$enrol);
		return $course;
	}
	
	public function assignUserToSubject($user,$subject)
	{

		$this->connect();
		$context = $this->getMoodleData("mdl_context", "contextlevel=" . $this->mcontext_course . " AND instanceid=" . $subject["id"]);
		$enrol = $this->getMoodleData("mdl_enrol", "courseid=" . $subject["id"]);
		$muser = $this->getMoodleData("mdl_user", "username='" . $user["loginName"] . "'");

		$cuser = $this->isAssigned($muser["id"], $context["id"]);

		if (!$cuser) {
			$cuser["roleid"] = $user["roleid"];
			$cuser["userid"] = $muser["id"];
			$cuser["contextid"] = $context["id"];
			$cuser["timemodified"] = time();
			$cuser["modifierid"] = 2;
			$cuser["component"] = '';
			$cuser["itemid"] = 0;
			$cuser["sortorder"] = 0;

			//echo "<pre>";print_r($enrol);echo "</pre>";exit;

			$this->moodledb->insert("mdl_role_assignments", $cuser);

			$ue["status"] = '0';
			$ue["enrolid"] = $enrol["id"];
			$ue["userid"] = $muser["id"];
			$ue["timestart"] = time();
			$ue["timeend"] = '0';
			$ue["modifierid"] = '2';
			$ue["timecreated"] = time();
			$ue["timemodified"] = time();

			Cms_MoodleLog::add('Enrol User', json_encode($ue));

			$this->moodledb->insert("mdl_user_enrolments", $ue);
		} else {
			echo "User already assigned.<hr>";
		}
	}
	
	public function addGroup($grpdata) {
		//dapatkan moodle course_id for that sem
		$this->connect();
		foreach ($grpdata as $grp){
			$course = $this->isLectSubjectSemesterExist($grp["IdSemester"],$grp["IdSubject"],$grp["IdLecturer"]);
			//echo "<pre>";print_r($course);echo "</pre>";
			if($course){
				$mgrp=$this->isGroupExist($grp["IdCourseTaggingGroup"],$grp["IdSubject"]);
				//echo "<pre>masuk";print_r($mgrp);echo "</pre>";
				if(!$mgrp){
					$group["courseid"]=$course["id"];
					$group["idnumber"]=$grp["IdCourseTaggingGroup"]."/".$grp["IdSubject"];
					$group["name"]=$grp["GroupName"]."-".$grp["IdCourseTaggingGroup"];
					$group["description"]=$grp["GroupName"];
					$group["descriptionformat"]=1;
					$group["enrolmentkey"]="";
					$group["timecreated"]=time();
					$group["timemodified"]=time();
					
					Cms_MoodleLog::add('New Groups', json_encode($group));
					
					$this->moodledb->insert("mdl_groups",$group);		
					$group_id = $this->moodledb->lastInsertId();
					//exit;
					
					$grping["courseid"]=$course["id"];
					$grping["name"]=$grp["GroupName"]."-".$grp["IdCourseTaggingGroup"];
					$grping["idnumber"]=$grp["IdCourseTaggingGroup"]."/".$grp["IdSubject"];
					$grping["description"]= '';
					$grping["descriptionformat"]=1;
					$grping["timecreated"]=time();
					$grping["timemodified"]=time();
					
					$this->moodledb->insert("mdl_groupings",$grping);		
					$grping_id = $this->moodledb->lastInsertId();
					
					$grp_grping["groupingid"] = $grping_id;
					$grp_grping["groupid"] = $group_id;
					$grp_grping["timeadded"] = time();
					
					$this->moodledb->insert("mdl_groupings_groups",$grp_grping);	
					
				}else{
					//echo "<pre>";print_r($mgrp);echo "</pre>";
					echo "Group Exist<hr>";
				}
			}else{
				echo "No Course<hr>";
			}
		}
	}
	
	//utk assigned student byk byk studlist[0]["idstudentregsubject"]
	public function assignStudBatch($studlist) {
		$i=1;
		$total = array(1=>0,2=>0,3=>0,4=>0,5=>0);
		foreach ($studlist as $stud){
			//echo $i."-".$stud["IdCourseTaggingGroup"];
			$i++;
			$syncinfo = $this->assignStud($stud);
			$total[$syncinfo]++;			
		}
		return $total;
	}
	
	public function assignLectSingle($lectlist) {
		
		$total = array(1=>0,2=>0,3=>0,4=>0,5=>0);
		
		$syncinfo = $this->assignStud($lectlist);
		$total[$syncinfo]++;			
		
		return $total;
	}
	
	//utk assigned student satu satu stud["idstudentregsubject"]
	public function assignStud($stud) {
		//print_r($stud);exit;
		$this->connect();
		if($stud["IdCourseTaggingGroup"]!=0){
			$grp = $this->isGroupExist($stud["IdCourseTaggingGroup"],$stud["IdSubject"]);
			if(!$grp){
				//echo "Moodle Group not exist";
			}else{	
				$context = $this->getMoodleData("mdl_context","contextlevel=".$this->mcontext_course." AND instanceid=".$grp["courseid"]);
				$enrol = $this->getMoodleData("mdl_enrol", "courseid=".$grp["courseid"]);
				//echo "aaa:".$grp["courseid"]." ccc:".$stud["IdCourseTaggingGroup"]."/".$stud["IdSubject"];
				
				$muser = $this->getMoodleData("mdl_user", "username='".$stud["appl_username"]."'");
				
				if(!is_array($muser)){
					//echo "User moodle not found<hr>";
				}else{
					$cuser = $this->isAssigned($muser["id"],$context["id"]);
					
					if(!$cuser){
						$cuser["roleid"]=5; 
						$cuser["userid"]=$muser["id"];
						$cuser["contextid"] = $context["id"];
						$cuser["timemodified"] = time();
						$cuser["modifierid"] = 2;
						$cuser["component"] = '';
						$cuser["itemid"] = 0;
						$cuser["sortorder"] = 0;	
						
						//echo "<pre>";print_r($cuser);echo "</pre>";//exit;
						
						$this->moodledb->insert("mdl_role_assignments",$cuser);
						
						$ue["status"] = '0';
						$ue["enrolid"] = $enrol["id"];
						$ue["userid"] = $muser["id"];
						$ue["timestart"] = time();
						$ue["timeend"] = '0';
						$ue["modifierid"] = '2';
						$ue["timecreated"] = time();
						$ue["timemodified"] = time();	
						
						$this->moodledb->insert("mdl_user_enrolments",$ue);
						
						$grp_mem["groupid"] = $grp["id"];
						$grp_mem["userid"] = $muser["id"];
						$grp_mem["timeadded"] = time();
						
						$this->moodledb->insert("mdl_groups_members",$grp_mem);
						
						return 1;
					}else{
						//dapatkan current grup id
						//echo "dah assigned subject - ";
						$sqlm = "select a.id,groupid from mdl_groups_members a
								inner join mdl_groups as b on a.groupid=b.id 
								where courseid='".$grp["courseid"]."'
								AND userid = ".$muser["id"]." ";
						$curgrp = $this->moodledb->fetchrow($sqlm);
						//echo "<pre>$sqlm";
						if(!is_array($curgrp)){
							$grp_mem["groupid"] = $grp["id"];
							$grp_mem["userid"] = $muser["id"];
							$grp_mem["timeadded"] = time();
							//echo "x de grup <hr>";
							$this->moodledb->insert("mdl_groups_members",$grp_mem);									
							
							return 2;
														
						}elseif($curgrp["groupid"]!=$grp["id"]){
							//delete currgtup
							$this->moodledb->delete('mdl_groups_members',  "id = ".$curgrp["id"]);
							//Add Grup baru
							$grp_mem["groupid"] = $grp["id"];
							$grp_mem["userid"] = $muser["id"];
							$grp_mem["timeadded"] = time();	
							$this->moodledb->insert("mdl_groups_members",$grp_mem);								
							return 3;
						}else{
							//echo " No Action<hr>";
							return 4;
						}						
						//$err[2]=  $muser["id"].":".$context["id"].":User already assigned.<hr>";
					}	
				}
			}
		}else{//end kalau dah masuk group	
			//echo "tak tag kat sms<hr>";
			return 5;
		}
	}
	public function isSemesterExist($idsemester) {
		$this->connect();
		$sql = "select * from mdl_course_categories where idnumber=$idsemester";
		$rs = $this->moodledb->fetchRow($sql);
		if(is_array($rs)){
			return $rs;
		}else{
			return false;
		}
	}
	
	public function isSubjectSemesterExist($idsemester,$idsubject) {
		$this->connect();
		$sql = "select * from mdl_course_categories where idnumber='$idsemester/$idsubject'";
		$rs = $this->moodledb->fetchRow($sql);
		if(is_array($rs)){
			return $rs;
		}else{
			return false;
		}
	}	
	
	public function isLectSubjectSemesterExist($idsemester,$idsubject,$idlect) {
		$this->connect();
		$sql = "select * from mdl_course where idnumber='$idsemester/$idsubject/$idlect'";
		$rs = $this->moodledb->fetchRow($sql);
		if(is_array($rs)){
			return $rs;
		}else{
			return false;
		}
	}	
	
	public function isAssigned($userid,$contextid) {
		$this->connect();
		$sql = "select * from mdl_role_assignments where userid=$userid and contextid=$contextid";
		
		$rs = $this->moodledb->fetchRow($sql);
		if(is_array($rs)){
			return $rs;
		}else{
			return false;
		}	
	}
	
	public function isGroupExist($grpid,$idsubject) {
		$this->connect();
		$sql = "select * from mdl_groups where idnumber = '$grpid/$idsubject'";
		//echo $sql;
		$rs = $this->moodledb->fetchRow($sql);
		//exit;
		if(is_array($rs)){
			return $rs;
		}else{
			return false;
		}		
	}
	
	public function getMoodleData($table,$where) {
		$this->connect();
		$sql = "select * from $table  where $where";
		$rs = $this->moodledb->fetchRow($sql);
		if(is_array($rs)){
			return $rs;
		}else{
			return false;
		}		
	}
	
	public function getLastSeq($tblname) {
		$this->connect();
		$sql = "select sortorder from $tblname order by sortorder desc limit 0,1";
			$rs = $this->moodledb->fetchRow($sql);
		if(is_array($rs)){
			return $rs["sortorder"]+1;
		}else{
			return 1;
		}	
	}
	
	public function getAllSubject() {
		$sql="select a.idnumber, d.username, a.shortname from mdl_course a
			inner join   mdl_context as b on b.contextlevel=50 and b.instanceid=a.id
			inner join mdl_role_assignments as c on c.roleid=3 and c.contextid=b.id
			inner join mdl_user as d on d.id=c.userid";
		$this->connect();
		$rs = $this->moodledb->fetchAll($sql);
		return $rs;
	}
	
	public function getAllGroup() {
		$sql="select distinct a.id, a.idnumber, b.idnumber as cidnumber from mdl_groups a
			inner join   mdl_course as b on a.courseid=b.id";
			
		$this->connect();
		$rs = $this->moodledb->fetchAll($sql);
		return $rs;
	}	
	
	public function getAllGrouping() {
		$sql="select distinct a.id, a.idnumber, b.idnumber as cidnumber from mdl_groupings a
			inner join   mdl_course as b on a.courseid=b.id";
			
		$this->connect();
		$rs = $this->moodledb->fetchAll($sql);
		return $rs;
	}		
	
	public function update($table,$data,$where) {
		$this->connect();
		$this->moodledb->update($table,$data,$where);
		
	}
	public function dropSubject($regid,$md_userid,$contextid,$courseid,$grpregid) {
		$this->connect();
		$enrol = $this->getMoodleData("mdl_enrol", "courseid=".$courseid);
		
		$this->moodledb->delete("mdl_role_assignments","id = $regid");
		$this->moodledb->delete("mdl_user_enrolments","enrolid = ".$enrol["id"]." and userid = $md_userid");
		$this->moodledb->delete("mdl_groups_members","id = $grpregid");
	}
	public function createAssignment($idsemester, $idsubject, $idlect, $idgroup, $as_id,$idcomp, $name, $intro = '', $grade = 10.00, $maxgrade = 100.00) {
		$this->connect();
		$subject = $this->isLectSubjectSemesterExist($idsemester,$idsubject,$idlect);
		$grp = $this->isGroupExist($idgroup,$idsubject);
		//print_r($subject);exit; 
		if($subject){
			$mdl_asg=$this->checkAssignmentExist($as_id);
			//print_r($mdl_asg);exit;
			if($mdl_asg==0){
				
			//dapat section general dlm subject//
				$sqld="select * from mdl_course_sections where course=".$subject["id"]." AND section=0";
				$section = $this->moodledb->fetchRow($sqld);
							
				$assign["course"] = $subject["id"];
				$assign["name"] = $name;
				$assign["idnumber"] = $as_id;
				$assign["intro"] = $intro;
				$assign["introformat"] = 1;
				$assign["alwaysshowdescription"] = 1;
				$assign["duedate"] = time();
				$assign["allowsubmissionsfromdate"] = time();
				if(is_array($grp)){
					$assign["teamsubmission"] = 1;
					$assign["teamsubmissiongroupingid"]=$grp["id"];
				}else{
					$assign["teamsubmission"] = 0;
				}
				$assign["grade"] = $grade;
				$assign["timemodified"] = time();
				$this->moodledb->insert("mdl_assign",$assign);	
				$assign_id = $this->moodledb->lastInsertId();
				
				$module["course"]=$subject["id"];
				$module["module"]=1;
				$module["instance"]=$assign_id;
				$module["section"]=$section["id"];
				$module["added"]=time();
				if(is_array($grp)){
					$module["groupmode"] = 1;
					$module["groupingid"]=$grp["id"];
				}else{
					$module["groupmode"] = 0;
				}				
				$this->moodledb->insert("mdl_course_modules",$module);	
				$module_id = $this->moodledb->lastInsertId();
				
							
				$section["sequence"] = $section["sequence"].",".$module_id;
	
				$this->moodledb->update('mdl_course_sections', $section, "id = ".$section["id"]);	
				//update mdl_course_sections R::store($section);
				//exit;			
				//$course_context = R::findOne("mdl_context", "contextlevel=? AND instanceid=?", array(CONTEXT_COURSE, $course));
				$sqlc="select * from mdl_context where contextlevel=".$this->mcontext_course." AND instanceid=".$subject["id"];
				$ccontext = $this->moodledb->fetchRow($sqlc);
				
				//$context = R::dispense("mdl_context");
				$context["contextlevel"] = $this->mcontext_module; 
				$context["instanceid"] = $module_id; 
				$context["path"] = $ccontext["path"];
				$context["depth"] = 6; 
				
				$this->moodledb->insert("mdl_context",$context);	
				$context_id = $this->moodledb->lastInsertId();
	
				$context["path"] = $ccontext["path"]."/$context_id";
				$this->moodledb->update('mdl_context', $context, "id = ".$context_id);	
	
										
				// Update mdl_context R::store($context);
				//dapatkan dulu grade cat SMS
				$sqlgc="select * from mdl_grade_categories where courseid='".$subject["id"]."'";
				$grade_cat = $this->moodledb->fetchRow($sqlgc);
				if(!is_array($grade_cat)){
					$grade_cat["courseid"] = $subject["id"];
					$grade_cat["depth"] = 1;
					$grade_cat["path"] = "/0/";
					$grade_cat["fullname"] = "SMS";
					$grade_cat["aggregation"] = 11;
					$grade_cat["keephigh"] = 0;
					$grade_cat["droplow"] = 0;
					$grade_cat["aggregateonlygraded"] = 1;
					$grade_cat["aggregateoutcomes"] = 0;
					$grade_cat["aggregatesubcats"] = 1;
					$grade_cat["timecreated"] = time();
					$grade_cat["timemodified"] = time();
					$grade_cat["hidden"] = 1;
					$grade_cat["idnumber"] = $idcomp;
					$this->moodledb->insert("mdl_grade_categories",$grade_cat);
					$grade_catid = $this->moodledb->lastInsertId();	
					$ograde_cat["path"] = "/".$grade_catid."/";
					$this->moodledb->update('mdl_grade_categories', $ograde_cat, "id = ".$grade_catid);	
				}else{
					$grade_cat["idnumber"] = $idcomp;
					$grade_catid = $grade_cat["id"];
					
					$this->moodledb->update('mdl_grade_categories', $grade_cat, "id = ".$grade_catid);
				}
				//$grade_item = R::dispense("mdl_grade_items");
				$grade_item["courseid"] = $subject["id"];
				$grade_item["categoryid"] = $grade_catid;
				$grade_item["itemname"] = $name;
				$grade_item["itemtype"] = 'mod';
				$grade_item["itemmodule"] = 'assign';
				$grade_item["iteminstance"] = $assign_id;
				$grade_item["grademax"] = $maxgrade;
				$grade_item["timecreated"] = time();
				$grade_item["timemodified"] = time();
				$grade_item["idnumber"] = $as_id;
				
				$this->moodledb->insert("mdl_grade_items",$grade_item);	
				//print_r($grade_item);
				
				//$grade_area = R::dispense("mdl_grading_areas");
				$grade_area["contextid"] = $context_id;
				$grade_area["component"] = 'mod_assign';
				$grade_area["areaname"] = 'submissions';
	 
				$this->moodledb->insert("mdl_grading_areas",$grade_area);
				
				/*//R::store($grade_area);*/
				//$ocourse = R::load("mdl_course", $course);
				$ocourse["cacherev"] = time();
				$this->moodledb->update('mdl_course', $ocourse, "id = ".$subject["id"]);
				//R::store($ocourse);
			}else{
				$this->updateAssignment($idsemester, $idsubject, $idlect, $idgroup, $as_id, $name, $intro, $grade, $maxgrade);
				$ocourse["cacherev"] = time();
				$this->moodledb->update('mdl_course', $ocourse, "id = ".$subject["id"]);
			}
		}
	}
	
	public function updateAssignment($idsemester, $idsubject, $idlect, $idgroup, $as_id, $name, $intro = '', $grade = 10.00, $maxgrade = 100.00) {
		$this->connect();
		$assign["name"] = $name;
		$assign["intro"] = $intro;
		$assign["grade"] = $grade;
		$assign["timemodified"] = time();
		$this->moodledb->update('mdl_assign', $assign, "idnumber = ".$as_id);
		$grade_item["itemname"] = $name;
		$grade_item["grademax"] = $maxgrade;
		$this->moodledb->update('mdl_grade_items', $grade_item, "idnumber = ".$as_id);
	}
	
	public function deleteAssignment($as_id) {
		$this->connect();
		$this->moodledb->delete("mdl_assign","idnumber = $as_id");
		$this->moodledb->delete("mdl_grade_items","idnumber = $as_id");
	}

	public function checkAssignmentExist($as_id) {
		$this->connect();
		$sql="Select * from mdl_assign where idnumber='$as_id'";
		$rs = $this->moodledb->fetchRow($sql);
		if(!$rs){
			return 0;			
		}else{
			return $rs;
		}
	}
	
	public function create_quiz($idsemester, $idsubject, $idlect, $idgroup, $qz_id, $idcomp, $name, $intro = '', $grade = 10.00, $maxgrade = 100.00) {	
		$this->connect();
		$subject = $this->isLectSubjectSemesterExist($idsemester,$idsubject,$idlect);
		$grp = $this->isGroupExist($idgroup,$idsubject);
		
		if($subject){
			$mdl_asg=$this->checkQuizExist($qz_id);
			
			if($mdl_asg==0){
				echo $sqld="select * from mdl_course_sections where course=".$subject["id"]." AND section=0";
				
				$section = $this->moodledb->fetchRow($sqld);
					
				//$quiz = R::dispense("mdl_quiz");
				$quiz["course"] = $subject["id"];
				$quiz["name"] = $name;
				$quiz["intro"] = $intro;
				$quiz["idnumber"] = $qz_id;
				$quiz["introformat"] = 1;
				$quiz["preferredbehaviour"] = 'deferredfeedback';
				$quiz["reviewattempt"] = 69904;
				$quiz["reviewcorrectness"] = 4368;
				$quiz["reviewmarks"] = 4368;
				$quiz["reviewspecificfeedback"] = 4368;
				$quiz["reviewgeneralfeedback"] = 4368;
				$quiz["reviewrightanswer"] = 4368;
				$quiz["reviewoverallfeedback"] = 4368;
				$quiz["grade"] = $grade;
				$quiz["timemodified"] = time();
					
				$this->moodledb->insert("mdl_quiz",$quiz);	
				$quiz_id = $this->moodledb->lastInsertId();
				//echo $quiz_id;exit;
				$feedback["quizid"] = $quiz_id;
				$feedback["feedbacktext"] = '';
				$feedback["feedbacktextformat"] = 1;
				$feedback["mingrade"] = 0;
				$feedback["maxgrade"] = $maxgrade * 1.01;
				$this->moodledb->insert("mdl_quiz_feedback",$feedback);	
				$feedback_id = $this->moodledb->lastInsertId();	
				
				
				//$module = R::dispense("mdl_course_modules");
				$module["course"]=$subject["id"];
				$module["module"]=16;
				$module["instance"]=$quiz_id;
				$module["section"]=$section["id"];
				$module["added"]=time();
				if(is_array($grp)){
					$module["groupmode"] = 1;
					$module["groupingid"]=$grp["id"];
				}else{
					$module["groupmode"] = 0;
				}				
				$this->moodledb->insert("mdl_course_modules",$module);	
				$module_id = $this->moodledb->lastInsertId();
				
				$section["sequence"] = $section["sequence"].",".$module_id;
				$this->moodledb->update('mdl_course_sections', $section, "id = ".$section["id"]);	
		
				
				$sqlc="select * from mdl_context where contextlevel=".$this->mcontext_course." AND instanceid=".$subject["id"];
				$ccontext = $this->moodledb->fetchRow($sqlc);
				$context["contextlevel"] = $this->mcontext_module; 
				$context["instanceid"] = $module_id; 
				$context["path"] = $ccontext["path"];
				$context["depth"] = 6; 
				$this->moodledb->insert("mdl_context",$context);	
				$context_id = $this->moodledb->lastInsertId();
		
				$context["path"] = $ccontext["path"]."/$context_id";
				$this->moodledb->update('mdl_context', $context, "id = ".$context_id);	

				$sqlgc="select * from mdl_grade_categories where idnumber='$idcomp'";
				$grade_cat = $this->moodledb->fetchRow($sqlgc);
				if(!is_array($grade_cat)){
					$grade_cat["courseid"] = $subject["id"];
					$grade_cat["depth"] = 1;
					$grade_cat["path"] = "/0/";
					$grade_cat["fullname"] = "SMS";
					$grade_cat["aggregation"] = 11;
					$grade_cat["keephigh"] = 0;
					$grade_cat["droplow"] = 0;
					$grade_cat["aggregateonlygraded"] = 1;
					$grade_cat["aggregateoutcomes"] = 0;
					$grade_cat["aggregatesubcats"] = 1;
					$grade_cat["timecreated"] = time();
					$grade_cat["timemodified"] = time();
					$grade_cat["hidden"] = 1;
					$grade_cat["idnumber"] = $idcomp;
					$this->moodledb->insert("mdl_grade_categories",$grade_cat);
					$grade_catid = $this->moodledb->lastInsertId();	
					$ograde_cat["path"] = "/".$grade_catid."/";
					$this->moodledb->update('mdl_grade_categories', $ograde_cat, "id = ".$grade_catid);	
				}else{
					$grade_catid = $grade_cat["id"];
				}
								
				$grade_item["courseid"] = $subject["id"];
				$grade_item["categoryid"] = $grade_catid;
				$grade_item["itemname"] = $name;
				$grade_item["itemtype"] = 'mod';
				$grade_item["itemmodule"] = 'quiz';
				$grade_item["itemnumber"] = 0;
				$grade_item["iteminstance"] = $quiz_id;
				$grade_item["grademax"] = $maxgrade;
				$grade_item["timecreated"] = time();
				$grade_item["timemodified"] = time();
				$grade_item["idnumber"] = $qz_id;
						
				$this->moodledb->insert("mdl_grade_items",$grade_item);			
		
				$ocourse["cacherev"] = time();
				$this->moodledb->update('mdl_course', $ocourse, "id = ".$subject["id"]);
			}else{
				$this->updateQuiz($idsemester, $idsubject, $idlect, $idgroup, $qz_id, $name, $intro, $grade, $maxgrade);
				$ocourse["cacherev"] = time();
				$this->moodledb->update('mdl_course', $ocourse, "id = ".$subject["id"]);
			}
		}
	}
	
	public function updateQuiz($idsemester, $idsubject, $idlect, $idgroup, $qz_id, $name, $intro = '', $grade = 10.00, $maxgrade = 100.00) {
		$this->connect();
		$quiz["name"] = $name;
		$quiz["intro"] = $intro;
		$quiz["grade"] = $grade;
		$quiz["timemodified"] = time();	
			
		$this->moodledb->update('mdl_quiz', $quiz, "idnumber = ".$qz_id);
		$grade_item["itemname"] = $name;
		$grade_item["grademax"] = $maxgrade;
		$this->moodledb->update('mdl_grade_items', $grade_item, "idnumber = ".$qz_id);
	}	
	
	public function checkQuizExist($qz_id) {
		$this->connect();
		$sql="Select * from mdl_quiz where idnumber='$qz_id'";
		$rs = $this->moodledb->fetchRow($sql);
		if(!$rs){
			return 0;			
		}else{
			return $rs;
		}
	}
	
	public function getQuizMark($id,$user_id) {
		$this->connect();
		
		$sql="Select * from mdl_quiz_grades where quiz='$id' and userid = '$user_id'";
		$rs = $this->moodledb->fetchRow($sql);
		if(!$rs){
			return 0;			
		}else{
			return $rs;
		}
	}
	
	public function getUserId($username) {
		$this->connect();
		
		$sql="Select * from mdl_user where username='$username'";
		$rs = $this->moodledb->fetchRow($sql);
		if(!$rs){
			return 0;			
		}else{
			return $rs;
		}
	}

	public function getClass(){
		$this->connect();
		$sql = "SELECT * FROM mdl_course as a WHERE a.idnumber LIKE '%175/%' OR a.idnumber LIKE '%176/%'";
		$result = $this->moodledb->fetchAll($sql);
		return $result;
	}

	public function getCourseFormat($id){
		$this->connect();
		$sql = "SELECT * FROM mdl_course_format_options WHERE courseid = ".$id;
		$result = $this->moodledb->fetchAll($sql);
		return $result;
	}

	public function insertCourseFormat($data){
		$this->connect();
		$this->moodledb->insert(mdl_course_format_options, $data);
	}
}
?>