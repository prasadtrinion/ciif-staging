<?php
/**
 *  @author alif
 *  @date Mar 3, 2014
 *  enhance by yatie 23 9 2014
 */

class GeneralSetup_Form_CourseGroup extends Zend_Form
{
	protected $idSubject;
	protected $IdSemester;
	protected $idGroup;
	protected $subCode;
	protected $semester;
	
	
	public function setIdSubject($idSubject){
		$this->idSubject = $idSubject;
	}
	public function setIdSemester($idSemester){
		$this->IdSemester = $idSemester;
	}
	public function setIdGroup($idGroup){
		$this->idGroup = $idGroup;
	}
	public function setSubCode($subCode){
		$this->subCode = $subCode;
	}
	public function setSemester($semester){
		$this->semester = $semester;
	}
	
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','GroupForm');
		
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		
		$this->addElement('hidden','IdSubject' );
		$this->IdSubject->setValue($this->idSubject);
		
		$this->addElement('hidden','idSemester');
		$this->idSemester->setValue($this->IdSemester);
		
		
		
		$this->addHid('IdCourseTaggingGroup',$this->idGroup);
		
		$this->addElement('text','GroupCode',
				array(
						'label'=>'Section Code',
						'class'=>'input-txt',
						'required'=>'true'
				)
		);
		
		if($this->idGroup){
			$this->GroupCode->setAttrib('readonly', true);
		}
		
		$this->addElement('text','GroupName', 
			array(
				'label'=>'Section Name',
				'class'=>'input-txt',
				'required'=>'true'
			)
		);
		
		//date
		$this->addElement('text','class_start_date',array(
		    'label'=>'Class Start Date',
		    'class'=>'datepicker',
			'value'=>date('d-m-Y',strtotime($this->semester['SemesterMainStartDate'])),
                    'required'=>'true'
		));
		
		//date
		$this->addElement('text','class_end_date',array(
		    'label'=>'Class End Date',
		    'class'=>'datepicker',
			'value'=>date('d-m-Y',strtotime($this->semester['SemesterMainEndDate'])),
                    'required'=>'true'
		));
		
		$this->addElement('text','maxstud',
		    array(
		        'label'=>'Max Occupancy',
				'class'=>'input-txt small',
		        'required'=>false
		    )
		);
		$this->maxstud->addValidator('Digits');
                
                $this->addElement('text','reservation',
		    array(
		        'label'=>'Reservation',
			'class'=>'input-txt small'
		    )
		);
		$this->reservation->addValidator('Digits');
                
                $this->addElement('select','IdBranch', array(
                    'label'=>'Branch',
                    'required'=>true,
                    'id'=>'branch'
                ));
                
                $this->IdBranch->addMultiOption(null,"-- Select --");
                
                $branchModel = new GeneralSetup_Model_DbTable_Branchofficevenue();
                $branchList = $branchModel->fetchAll();
                
                if ($branchList){
                $this->IdBranch->addMultiOption(0,"All");
                    foreach ($branchList as $branchLoop){
                        $this->IdBranch->addMultiOption($branchLoop['IdBranch'],$branchLoop['BranchName']);
                    }
                }
		
		$foo = new Zend_Form_Element_Text('IdLecturerName');
		$foo->setLabel('Lecturer / Coordinator Name')
		->setDescription('<button href="#" class="btn-submit" id="assign-lecturer">Assign</button> <button href="#" class="btn-submit" onclick="$(\'#IdLecturer\').val(\'\');$(\'#IdLecturerName\').val(\'\'); return false;">Reset</button>')
		->setAttrib('readonly', true)
		->setAttrib('class','input-txt')

		->setDecorators(array(
		    'ViewHelper',
		    array('Description', array('escape' => false, 'tag' => false)),
		    array('HtmlTag', array('tag' => 'dd')),
		    array('Label', array('tag' => 'dt')),
		    'Errors',
		));
		$this->addElement($foo);
		
		$this->addHid('IdLecturer',null);
		
		
		/*$foo2 = new Zend_Form_Element_Text('VerifyByName');
		$foo2->setLabel('Mark Verificator')
		->setDescription('<button href="#" class="btn-submit" id="assign-verificator">Assign</button> <button href="#" class="btn-submit" onclick="$(\'#VerifyBy\').val(\'\'); $(\'#VerifyByName\').val(\'\'); return false;">Reset</button>')
		->setAttrib('readonly', true)
		->setAttrib('ignore', true)
		->setAttrib('class','input-txt')
		->setDecorators(array(
		    'ViewHelper',
		    array('Description', array('escape' => false, 'tag' => false)),
		    array('HtmlTag', array('tag' => 'dd')),
		    array('Label', array('tag' => 'dt')),
		    'Errors',
		));
		$this->addElement($foo2);*/
		
		
		$this->addHid('VerifyBy',null);
		
		$this->addElement('textarea','remark',
		    array(
		        'label'=>'Remark',
		    )
		);
		
		$table = '<table id="program" class="table" width="500px"><thead><tr><th>Programme</th><th>Programme Scheme</th><th width="30px">&nbsp;</th></tr></thead><tbody></tbody><tfoot><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></tfoot></table>';
		$this->addElement('hidden', 'program_list', array(
		    'label' => 'Program',
            'ignore' => true,
		    'description' => '<button href="#" class="btn-submit" id="assign-program">Add Program</button><br />'.$table
        ));
		
		$this->program_list
		->setDecorators(array(
		    'ViewHelper',
		    array('Description', array('escape' => false, 'tag' => false)),
		    array('HtmlTag', array('tag' => 'dd')),
		    array('Label', array('tag' => 'dt')),
		    'Errors',
		));
		
		
		
		//button
		$this->addElement('submit', 'save', array(
		    'label'=>'Submit',
		    'decorators'=>array('ViewHelper')
		));
		
		/*$this->addElement('submit', 'cancel', array(
		    'label'=>'Cancel',
		    'decorators'=>array('ViewHelper'),
		    'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'general-setup', 'controller'=>'highschool-subject','action'=>'index'),'default',true) . "'; return false;"
		));*/
		
		$this->addDisplayGroup(array('save'),'buttons', array(
		    'decorators'=>array(
		        'FormElements',
		        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
		        'DtDdWrapper'
		    )
		));
		
		
	}
	
	public function addHid($field, $value){
	  $hiddenIdField = new Zend_Form_Element_Hidden($field);
	  $hiddenIdField->setValue($value)
	  ->removeDecorator('label')
	  ->removeDecorator('HtmlTag');
	  $this->addElement($hiddenIdField);
	}
}
?>