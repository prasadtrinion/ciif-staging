<?php

class GeneralSetup_Form_ExamCenter extends Zend_Form
{
	
	public function init()
	{
		$gstrtranslate =Zend_Registry::get('Zend_Translate');
		
		$ec_code = new Zend_Form_Element_Text('ec_code');
		$ec_code->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ec_code->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $ec_name = new Zend_Form_Element_Text('ec_name');
		$ec_name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ec_name->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','250')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $ec_short_name= new Zend_Form_Element_Text('ec_short_name');
		$ec_short_name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ec_short_name->setAttrib('required',"true")       			 
		        		->setAttrib('maxlength','250')       
		        		->removeDecorator("DtDdWrapper")
		        	    ->removeDecorator("Label")
		        		->removeDecorator('HtmlTag');
		        		
		$ec_default_language= new Zend_Form_Element_Text('ec_default_language');
		$ec_default_language->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ec_default_language->setAttrib('required',"false")       			 
		        		->setAttrib('maxlength','50')       
		        		->removeDecorator("DtDdWrapper")
		        	    ->removeDecorator("Label")
		        		->removeDecorator('HtmlTag');
	
		$ec_start_date = new Zend_Dojo_Form_Element_DateTextBox('ec_start_date');
        $ec_start_date->setAttrib('dojoType',"dijit.form.DateTextBox");
        $ec_start_date->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$ec_start_date->setAttrib('required',"false");
        $ec_start_date->removeDecorator("DtDdWrapper");
        $ec_start_date->setAttrib('title',"dd-mm-yyyy");
        $ec_start_date->removeDecorator("Label");
        $ec_start_date->removeDecorator('HtmlTag'); 
        
        $ec_end_date = new Zend_Dojo_Form_Element_DateTextBox('ec_end_date');
        $ec_end_date->setAttrib('dojoType',"dijit.form.DateTextBox");
        $ec_end_date->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$ec_end_date->setAttrib('required',"false");
        $ec_end_date->removeDecorator("DtDdWrapper");
        $ec_end_date->setAttrib('title',"dd-mm-yyyy");
        $ec_end_date->removeDecorator("Label");
        $ec_end_date->removeDecorator('HtmlTag');  
        
        $ec_active  = new Zend_Form_Element_Checkbox('ec_active');
		$ec_active->setAttrib('dojoType',"dijit.form.CheckBox");
		$ec_active->setvalue('1');
		$ec_active->removeDecorator("DtDdWrapper");
		$ec_active->removeDecorator("Label");
		$ec_active->removeDecorator('HtmlTag');
		
		
		
		//address section below
		
		$AddressType = new Zend_Dojo_Form_Element_FilteringSelect('AddressType');
		$AddressType->removeDecorator("DtDdWrapper");
		$AddressType->setAttrib('required',"true") ;
		$AddressType->removeDecorator("Label");
		$AddressType->removeDecorator('HtmlTag');		
		$AddressType->setRegisterInArrayValidator(false);
		$AddressType->setAttrib('dojoType',"dijit.form.FilteringSelect");		
    
		$defDB = new App_Model_General_DbTable_Definationms();
		$address_type = $defDB->getDataByType(98);	//Address Type 	
		foreach ($address_type as $at){			
			$AddressType->addMultiOption($at['idDefinition'],$at['DefinitionDesc']);
		}
        
	    $ec_country = new Zend_Dojo_Form_Element_FilteringSelect('ec_country');
		$ec_country->removeDecorator("DtDdWrapper");
		$ec_country->setAttrib('required',"true") ;
		$ec_country->removeDecorator("Label");
		$ec_country->removeDecorator('HtmlTag');
		$ec_country->setAttrib('OnChange', 'fnGetCountryStateList');
		$ec_country->setRegisterInArrayValidator(false);
		$ec_country->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$countryDb = new GeneralSetup_Model_DbTable_Country();
		$country_list = $countryDb->fnGetCountryDetails();
		$ec_country->addMultiOption('','Select');
		foreach($country_list as $c){
			 $ec_country->addMultiOption($c['key'],$c['value']);
		}
			

		$ec_state = new Zend_Dojo_Form_Element_FilteringSelect('ec_state');
		$ec_state->removeDecorator("DtDdWrapper");
		$ec_state->setAttrib('required',"false") ;
		$ec_state->removeDecorator("Label");
		$ec_state->removeDecorator('HtmlTag');
		$ec_state->setAttrib('OnChange', "fnGetStateCityList(this.value); checkOthers('ec_state')");
		$ec_state->setRegisterInArrayValidator(false);
		$ec_state->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$ec_city = new Zend_Dojo_Form_Element_FilteringSelect('ec_city');
        $ec_city->removeDecorator("DtDdWrapper");
        $ec_city->removeDecorator("Label");
        $ec_city->removeDecorator('HtmlTag');
        $ec_city->setAttrib('required',"false");
        $ec_city->setRegisterInArrayValidator(false);
		$ec_city->setAttrib('onchange',"checkOthers('ec_city')");
		$ec_city->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$City_Others = new Zend_Form_Element_Text('City_Others');
        $City_Others->removeDecorator("DtDdWrapper");
        $City_Others->removeDecorator("Label");
        $City_Others->removeDecorator('HtmlTag');
        $City_Others->setAttrib('required',"false");
		$City_Others->setAttrib('dojoType',"dijit.form.ValidationTextBox");

		$State_Others = new Zend_Form_Element_Text('State_Others');
        $State_Others->removeDecorator("DtDdWrapper");
        $State_Others->setAttrib('required',"false") ;
        $State_Others->removeDecorator("Label");
        $State_Others->removeDecorator('HtmlTag');
        $State_Others->setAttrib('OnChange', '');
		$State_Others->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		
		$ec_address1 = new Zend_Form_Element_Text('ec_address1');
		$ec_address1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_address1->setAttrib('required',"true")
					->setAttrib('maxlength','500')
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');

		$ec_address2 = new Zend_Form_Element_Text('ec_address2');
		$ec_address2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_address2->setAttrib('required',"false")
					->setAttrib('maxlength','500')
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');
						
		$ec_zipcode = new Zend_Form_Element_Text('ec_zipcode');
		$ec_zipcode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_zipcode->setAttrib('maxlength','20');
		$ec_zipcode->removeDecorator("DtDdWrapper");
		$ec_zipcode->removeDecorator("Label");
		$ec_zipcode->removeDecorator('HtmlTag');
        		
		$ec_phone = new Zend_Form_Element_Text('ec_phone',array('regExp'=>"[\+0-9]+",'invalidMessage'=>"Not a valid Phone No."));
		$ec_phone->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_phone->setAttrib('maxlength','20');
		$ec_phone->removeDecorator("DtDdWrapper");
		$ec_phone->removeDecorator("Label");
		$ec_phone->removeDecorator('HtmlTag');
		
		$ec_email = new Zend_Form_Element_Text('ec_email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$ec_email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_email->setAttrib('required',"false")
				 ->setAttrib('maxlength','100')
				 ->removeDecorator("DtDdWrapper")
				 ->removeDecorator("Label")
				 ->removeDecorator('HtmlTag');
                
        $ec_coordinatorname = new Zend_Form_Element_Text('ec_coordinatorname');
		$ec_coordinatorname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_coordinatorname->setAttrib('required',"true")
		->setAttrib('maxlength','100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
        $ec_coordinatoremail = new Zend_Form_Element_Text('ec_coordinatoremail',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$ec_coordinatoremail->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_coordinatoremail->setAttrib('required',"true")
		->setAttrib('maxlength','50')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
                $ec_coordinatorphone = new Zend_Form_Element_Text('ec_coordinatorphone',array('regExp'=>"[\+0-9]+",'invalidMessage'=>"Not a valid Phone No."));
		$ec_coordinatorphone->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_coordinatorphone->setAttrib('maxlength','20');
		$ec_coordinatorphone->removeDecorator("DtDdWrapper");
		$ec_coordinatorphone->removeDecorator("Label");
		$ec_coordinatorphone->removeDecorator('HtmlTag');
                $ec_coordinatorphone->setAttrib('required',"true");
                
                $ec_coordinatormobile = new Zend_Form_Element_Text('ec_coordinatormobile',array('regExp'=>"[\+0-9]+",'invalidMessage'=>"Not a valid Mobile Phone No."));
		$ec_coordinatormobile->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_coordinatormobile->setAttrib('maxlength','20');
		$ec_coordinatormobile->removeDecorator("DtDdWrapper");
		$ec_coordinatormobile->removeDecorator("Label");
		$ec_coordinatormobile->removeDecorator('HtmlTag');
                $ec_coordinatormobile->setAttrib('required',"false");
                
        $ec_coordinatorfax = new Zend_Form_Element_Text('ec_coordinatorfax',array('regExp'=>"[0-9]+",'invalidMessage'=>"Not a valid Fax No."));
		$ec_coordinatorfax->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_coordinatorfax->setAttrib('maxlength','20');
		$ec_coordinatorfax->removeDecorator("DtDdWrapper");
		$ec_coordinatorfax->removeDecorator("Label");
		$ec_coordinatorfax->removeDecorator('HtmlTag');
        $ec_coordinatorfax->setAttrib('required',"false");

		$ec_fax = new Zend_Form_Element_Text('ec_fax',array('regExp'=>"[\+0-9]+",'invalidMessage'=>"Not a valid Fax No."));
		$ec_fax->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_fax->setAttrib('maxlength','20');
		$ec_fax->removeDecorator("DtDdWrapper");
		$ec_fax->removeDecorator("Label");
		$ec_fax->removeDecorator('HtmlTag');
        $ec_fax->setAttrib('required',"false");
		
        $save = new Zend_Form_Element_Submit('save');
        $save->dojotype="dijit.form.Button";
        $save->label = $gstrtranslate->_("Save");
        $save->removeDecorator("DtDdWrapper");
        $save->removeDecorator("Label");
        $save->removeDecorator('HtmlTag')
         	 ->class = "NormalBtn";
         	 
        $add = new Zend_Form_Element_Button('add');
        $add->dojotype="dijit.form.Button";
        $add->label = $gstrtranslate->_("Add");
        $add->setAttrib('OnClick', 'addExamCenter()');
        $add->removeDecorator("DtDdWrapper");
        $add->removeDecorator("Label");
        $add->removeDecorator('HtmlTag')
         	->class = "NormalBtn";
         	
        $search = new Zend_Form_Element_Submit('search');
        $search->dojotype="dijit.form.Button";
        $search->label = $gstrtranslate->_("Search");
        $search->removeDecorator("DtDdWrapper");
        $search->removeDecorator("Label");
        $search->removeDecorator('HtmlTag')
         	 ->class = "NormalBtn";
         
        $back = new Zend_Form_Element_Button('back');
        $back->dojotype="dijit.form.Button";
        $back->label = $gstrtranslate->_("Back");
        $back->setAttrib('OnClick', 'back()');
        $back->removeDecorator("DtDdWrapper");
        $back->removeDecorator("Label");
        $back->removeDecorator('HtmlTag')
         	 ->class = "NormalBtn";
         	 
        $clear = new Zend_Form_Element_Button('clear');
        $clear->dojotype="dijit.form.Button";
        $clear->label = $gstrtranslate->_("Clear");
        $clear->setAttrib('OnClick', 'clearSearch()');
        $clear->removeDecorator("DtDdWrapper");
        $clear->removeDecorator("Label");
        $clear->removeDecorator('HtmlTag')
         	->class = "NormalBtn";

		//$tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
		$ec_timezone = new Zend_Dojo_Form_Element_FilteringSelect('ec_timezone');
        $ec_timezone->removeDecorator("DtDdWrapper");
        $ec_timezone->removeDecorator("Label");
        $ec_timezone->removeDecorator('HtmlTag');
        $ec_timezone->setAttrib('required',"false");
        $ec_timezone->setRegisterInArrayValidator(false);
		$ec_timezone->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$ec_timezone->setValue('Asia/Kuala_Lumpur');
		foreach ($this->get_timezones() as $tz_val => $tz_name )
		{
			$ec_timezone->addMultiOption($tz_val, $tz_name);
		}
        

		$ec_capacity = new Zend_Form_Element_Text('ec_capacity',array('regExp'=>"[0-9]+",'invalidMessage'=>"Numbers only"));
		$ec_capacity->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ec_capacity->setAttrib('maxlength','10');
		$ec_capacity->removeDecorator("DtDdWrapper");
		$ec_capacity->removeDecorator("Label");
		$ec_capacity->removeDecorator('HtmlTag');
        $ec_capacity->setAttrib('required',"false");

		$ec_id = new Zend_Form_Element_Hidden('ec_id');
        $ec_id->removeDecorator("DtDdWrapper");
        $ec_id->removeDecorator("Label");
        $ec_id->removeDecorator('HtmlTag');

        $this->addElements(array(
         						$ec_code,       											
        						$ec_name,
        						$ec_short_name,
        						$ec_default_language,
        						$ec_start_date,
        						$ec_end_date,
        						$ec_active,
        						$AddressType,
        						$ec_country,
        						$ec_state,
        						$ec_city,
        						$ec_address1,
        						$ec_address2,
        						$ec_zipcode,
        						$ec_phone,
        						$ec_email,
        						$save,
        						$add,
        						$search,
        						$back,
        						$clear,
								$ec_coordinatorname,
								$ec_coordinatoremail,
								$ec_coordinatorphone,
								$ec_coordinatormobile,
								$ec_coordinatorfax,
								$City_Others,
								$State_Others,
								$ec_timezone,
								$ec_capacity,
								$ec_id,
								$ec_fax
        	));
	}

	function get_timezones()
	{
		$timezones = array(
			'Pacific/Midway'       => "(GMT-11:00) Midway Island",
			'US/Samoa'             => "(GMT-11:00) Samoa",
			'US/Hawaii'            => "(GMT-10:00) Hawaii",
			'US/Alaska'            => "(GMT-09:00) Alaska",
			'US/Pacific'           => "(GMT-08:00) Pacific Time (US &amp; Canada)",
			'America/Tijuana'      => "(GMT-08:00) Tijuana",
			'US/Arizona'           => "(GMT-07:00) Arizona",
			'US/Mountain'          => "(GMT-07:00) Mountain Time (US &amp; Canada)",
			'America/Chihuahua'    => "(GMT-07:00) Chihuahua",
			'America/Mazatlan'     => "(GMT-07:00) Mazatlan",
			'America/Mexico_City'  => "(GMT-06:00) Mexico City",
			'America/Monterrey'    => "(GMT-06:00) Monterrey",
			'Canada/Saskatchewan'  => "(GMT-06:00) Saskatchewan",
			'US/Central'           => "(GMT-06:00) Central Time (US &amp; Canada)",
			'US/Eastern'           => "(GMT-05:00) Eastern Time (US &amp; Canada)",
			'US/East-Indiana'      => "(GMT-05:00) Indiana (East)",
			'America/Bogota'       => "(GMT-05:00) Bogota",
			'America/Lima'         => "(GMT-05:00) Lima",
			'America/Caracas'      => "(GMT-04:30) Caracas",
			'Canada/Atlantic'      => "(GMT-04:00) Atlantic Time (Canada)",
			'America/La_Paz'       => "(GMT-04:00) La Paz",
			'America/Santiago'     => "(GMT-04:00) Santiago",
			'Canada/Newfoundland'  => "(GMT-03:30) Newfoundland",
			'America/Buenos_Aires' => "(GMT-03:00) Buenos Aires",
			'Greenland'            => "(GMT-03:00) Greenland",
			'Atlantic/Stanley'     => "(GMT-02:00) Stanley",
			'Atlantic/Azores'      => "(GMT-01:00) Azores",
			'Atlantic/Cape_Verde'  => "(GMT-01:00) Cape Verde Is.",
			'Africa/Casablanca'    => "(GMT) Casablanca",
			'Europe/Dublin'        => "(GMT) Dublin",
			'Europe/Lisbon'        => "(GMT) Lisbon",
			'Europe/London'        => "(GMT) London",
			'Africa/Monrovia'      => "(GMT) Monrovia",
			'Europe/Amsterdam'     => "(GMT+01:00) Amsterdam",
			'Europe/Belgrade'      => "(GMT+01:00) Belgrade",
			'Europe/Berlin'        => "(GMT+01:00) Berlin",
			'Europe/Bratislava'    => "(GMT+01:00) Bratislava",
			'Europe/Brussels'      => "(GMT+01:00) Brussels",
			'Europe/Budapest'      => "(GMT+01:00) Budapest",
			'Europe/Copenhagen'    => "(GMT+01:00) Copenhagen",
			'Europe/Ljubljana'     => "(GMT+01:00) Ljubljana",
			'Europe/Madrid'        => "(GMT+01:00) Madrid",
			'Europe/Paris'         => "(GMT+01:00) Paris",
			'Europe/Prague'        => "(GMT+01:00) Prague",
			'Europe/Rome'          => "(GMT+01:00) Rome",
			'Europe/Sarajevo'      => "(GMT+01:00) Sarajevo",
			'Europe/Skopje'        => "(GMT+01:00) Skopje",
			'Europe/Stockholm'     => "(GMT+01:00) Stockholm",
			'Europe/Vienna'        => "(GMT+01:00) Vienna",
			'Europe/Warsaw'        => "(GMT+01:00) Warsaw",
			'Africa/Algiers'	   => '(GMT+01:00) West Central Africa',
			'Europe/Zagreb'        => "(GMT+01:00) Zagreb",
			'Asia/Beirut'		   => '(GMT+02:00) Beirut',
			'Europe/Athens'        => "(GMT+02:00) Athens",
			'Europe/Bucharest'     => "(GMT+02:00) Bucharest",
			'Africa/Cairo'         => "(GMT+02:00) Cairo",
			'Africa/Harare'        => "(GMT+02:00) Harare",
			'Europe/Helsinki'      => "(GMT+02:00) Helsinki",
			'Europe/Istanbul'      => "(GMT+02:00) Istanbul",
			'Asia/Jerusalem'       => "(GMT+02:00) Jerusalem",
			'Asia/Gaza'			   => '(GMT+02:00) Gaza',
			'Africa/Blantyre'	   => '(GMT+02:00) Harare, Pretoria',
			'Europe/Kiev'          => "(GMT+02:00) Kyiv",
			'Europe/Minsk'         => "(GMT+02:00) Minsk",
			'Europe/Riga'          => "(GMT+02:00) Riga",
			'Europe/Sofia'         => "(GMT+02:00) Sofia",
			'Europe/Tallinn'       => "(GMT+02:00) Tallinn",
			'Europe/Vilnius'       => "(GMT+02:00) Vilnius",
			'Asia/Baghdad'         => "(GMT+03:00) Baghdad",
			'Asia/Kuwait'          => "(GMT+03:00) Kuwait",
			'Africa/Nairobi'       => "(GMT+03:00) Nairobi",
			'Asia/Riyadh'          => "(GMT+03:00) Riyadh",
			'Asia/Tehran'          => "(GMT+03:30) Tehran",
			'Europe/Moscow'        => "(GMT+04:00) Moscow",
			'Asia/Baku'            => "(GMT+04:00) Baku",
			'Europe/Volgograd'     => "(GMT+04:00) Volgograd",
			'Asia/Muscat'          => "(GMT+04:00) Muscat",
			'Asia/Tbilisi'         => "(GMT+04:00) Tbilisi",
			'Asia/Yerevan'         => "(GMT+04:00) Yerevan",
			'Asia/Kabul'           => "(GMT+04:30) Kabul",
			'Asia/Karachi'         => "(GMT+05:00) Karachi",
			'Asia/Tashkent'        => "(GMT+05:00) Tashkent",
			'Asia/Kolkata'         => "(GMT+05:30) Kolkata",
			'Asia/Kathmandu'       => "(GMT+05:45) Kathmandu",
			'Asia/Yekaterinburg'   => "(GMT+06:00) Ekaterinburg",
			'Asia/Almaty'          => "(GMT+06:00) Almaty",
			'Asia/Dhaka'           => "(GMT+06:00) Dhaka",
			'Asia/Novosibirsk'     => "(GMT+07:00) Novosibirsk",
			'Asia/Bangkok'         => "(GMT+07:00) Bangkok",
			'Asia/Jakarta'         => "(GMT+07:00) Jakarta",
			'Asia/Krasnoyarsk'     => "(GMT+08:00) Krasnoyarsk",
			'Asia/Chongqing'       => "(GMT+08:00) Chongqing",
			'Asia/Hong_Kong'       => "(GMT+08:00) Hong Kong",
			'Asia/Kuala_Lumpur'    => "(GMT+08:00) Kuala Lumpur",
			'Australia/Perth'      => "(GMT+08:00) Perth",
			'Asia/Singapore'       => "(GMT+08:00) Singapore",
			'Asia/Taipei'          => "(GMT+08:00) Taipei",
			'Asia/Ulaanbaatar'     => "(GMT+08:00) Ulaan Bataar",
			'Asia/Urumqi'          => "(GMT+08:00) Urumqi",
			'Asia/Irkutsk'         => "(GMT+09:00) Irkutsk",
			'Asia/Seoul'           => "(GMT+09:00) Seoul",
			'Asia/Tokyo'           => "(GMT+09:00) Tokyo",
			'Australia/Adelaide'   => "(GMT+09:30) Adelaide",
			'Australia/Darwin'     => "(GMT+09:30) Darwin",
			'Asia/Yakutsk'         => "(GMT+10:00) Yakutsk",
			'Australia/Brisbane'   => "(GMT+10:00) Brisbane",
			'Australia/Canberra'   => "(GMT+10:00) Canberra",
			'Pacific/Guam'         => "(GMT+10:00) Guam",
			'Australia/Hobart'     => "(GMT+10:00) Hobart",
			'Australia/Melbourne'  => "(GMT+10:00) Melbourne",
			'Pacific/Port_Moresby' => "(GMT+10:00) Port Moresby",
			'Australia/Sydney'     => "(GMT+10:00) Sydney",
			'Asia/Vladivostok'     => "(GMT+11:00) Vladivostok",
			'Asia/Magadan'         => "(GMT+12:00) Magadan",
			'Pacific/Auckland'     => "(GMT+12:00) Auckland",
			'Pacific/Fiji'         => "(GMT+12:00) Fiji",
		);
		return $timezones;
	} 
}
?>
