<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_Form_LearningMaterialSearchForm extends Zend_Dojo_Form { //Formclass for the user module
    
    public function init(){
        $this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $semesterid = $this->getAttrib('semesterid');
        
        //model
        $model = new GeneralSetup_Model_DbTable_LearningMaterial();
        
        //semester
        $semester = new Zend_Form_Element_Select('semester');
	$semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        $semester->setAttrib('required', true);
	$semester->removeDecorator("Label");
        $semester->setAttrib('onchange', 'getCourse(this.value);');
        
        $semester->addMultiOption('', '-- Select --');
        
        $semesterList = $model->getSemester();
        
        if ($semesterList){
            foreach ($semesterList as $semesterLoop){
                $semester->addMultiOption($semesterLoop['IdSemesterMaster'], $semesterLoop['SemesterMainName'].' - '.$semesterLoop['SemesterMainCode']);
            }
        }
        
        //course
        $course = new Zend_Form_Element_Select('course');
	$course->removeDecorator("DtDdWrapper");
        $course->setAttrib('class', 'select');
        $course->setAttrib('required', true);
	$course->removeDecorator("Label");
        
        $course->addMultiOption('', '-- Select --');
        
        if ($semesterid){
            $courseList = $model->getCourse($semesterid);
            foreach ($courseList as $courseLoop){
                $course->addMultiOption($courseLoop['IdSubject'], $courseLoop['SubCode'].' - '.$courseLoop['SubjectName']);
            }
        }
        
        $this->addElements(array(
            $semester,
            $course
        ));
    }
}