<?php 

class GeneralSetup_Form_AddLevelForm extends Zend_Form
{
	protected $programID;
	protected $landscapeId;
	protected $SemsterCount;
	protected $LevelCount;
	protected $idBlock;
	
	public function setProgramID($programID){
		$this->programID = $programID; 
	}
	
	public function setLandscapeId($landscapeId){
		$this->landscapeId = $landscapeId; 
	}
	
	public function setSemsterCount($SemsterCount){
		$this->SemsterCount = $SemsterCount; 
	}
	
	public function setLevelCount($LevelCount){
		$this->LevelCount = $LevelCount; 
	}
	
	public function setIdBlock($idBlock){
		$this->idBlock = $idBlock; 
	}
	
	
	
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','detailForm');
		
		if($this->idBlock){
			$this->setAction('/generalsetup/landscape/edit-level/');
		}else{
			$this->setAction('/generalsetup/landscape/add-level/');
		}
						
		$this->addElement('hidden', 'id',array('value'=>$this->programID));
		$this->addElement('hidden', 'idlandscape',array('value'=>$this->landscapeId));
		$this->addElement('hidden', 'idblock',array('value'=>$this->idBlock));
		  
	
    	
		//Block Count
		$this->addElement('select','block', array(
			'label'=>$this->getView()->translate('Level')
		));	
		
		if($this->idBlock){
			$this->block->setAttrib('disabled','disabled');
		}else{
			$this->block->setAttrib('required',true);
		}
		
		$this->block->addMultiOption(null,"-- Please Select --");		
		for($b=1; $b<=$this->LevelCount; $b++){
			$this->block->addMultiOption($b,$b);
		}
		
		
        $this->addElement('text','blockname', array(
			'label'=>$this->getView()->translate('Level Name'),
			'class'=>'input-txt',
			'required'=>true));
        
        //Prerequisite
		$this->addElement('select','prerequisite', array(
			'label'=>$this->getView()->translate('Prerequisite')
		));	
		
		if($this->idBlock){
			$this->prerequisite->setAttrib('disabled','disabled');
		}else{
			$this->prerequisite->setAttrib('required',true);
		}

		$blockDb = new GeneralSetup_Model_DbTable_Landscapeblock();
		$blocks = $blockDb->getBlocksByLandscape($this->landscapeId);
		$this->prerequisite->addMultiOption(0,'N/A');
		foreach( $blocks as $block )
		{
			$this->prerequisite->addMultiOption($block['idblock'],$block['blockname']);	
		}
        		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Save',
          'decorators'=>array('ViewHelper')
        ));       
       
        
        $this->addDisplayGroup(array('save'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        
       
        
	}
	
	
}
?>