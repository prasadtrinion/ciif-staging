<?php 

class GeneralSetup_Form_CourseSearch extends Zend_Form
{		
	protected $_locale;
	
	public function setLocale($value) {
		$this->_locale = $value;
	}
	
	public function init()
	{
						
		$this->setMethod('post');
		$this->setAttrib('id','myform');
                
                $courseGroup = $this->getAttrib('course_group');

                //Intake
                if ($courseGroup == true){
                    $this->addElement('select','IdSemester', array(
                        'label'=>$this->getView()->translate('Semester'),
                        'required'=>true,
                        'onchange'=>'checkOpenRegister(this.value)'
                    ));
                } else {
                    $this->addElement('select','IdSemester', array(
                        'label'=>$this->getView()->translate('Semester'),
                        'required'=>true,
                        'disable'=>true,
                        'onchange'=>'checkOpenRegister(this.value)'

                    ));
                }
                    
		$semesterDB = new GeneralSetup_Model_DbTable_Semestermaster();
		
		$this->IdSemester->addMultiOption(null,"-- Please Select --");		
		foreach($semesterDB->fnGetSemestermasterList() as $semester){
			$this->IdSemester->addMultiOption($semester["key"],$semester["SemesterMainCode"].' - '.$semester["value"]);
		}

		//Program
		$this->addElement('select','IdCollege', array(
                        'label'=>$this->getView()->translate('Department'), 
                        'required'=>true
                ));
		
		$collegeDb = new GeneralSetup_Model_DbTable_Collegemaster();

		$this->IdCollege->addMultiOption(null,"-- Please Select --");		

		foreach($collegeDb->getCollege()  as $college){
                    $this->IdCollege->addMultiOption($college["IdCollege"],$college["CollegeCode"].' - '.$college["CollegeName"]);
		}

		$this->addElement('text','SubjectName', array(
			'label'=>'Subject Name'
		));
		
		//name
		$this->addElement('text','SubjectCode', array(
			'label'=>'Course Code'
		));
		
		//button
		$this->addElement('submit', 'Search', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'Clear', array(
          'label'=>$this->getView()->translate('Clear'),
           'class'=>'btn-submit',
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('Search','Clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        	    
		
        		
	}
	
	
}
?>