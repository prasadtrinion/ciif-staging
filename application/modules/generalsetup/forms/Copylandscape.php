<?php
class GeneralSetup_Form_Copylandscape extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
	public function init() {

		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		$IdLandscape = new Zend_Form_Element_Hidden('IdLandscape');
		$IdLandscape->removeDecorator("DtDdWrapper");
		$IdLandscape->removeDecorator("Label");
		$IdLandscape->removeDecorator('HtmlTag');

		$IdProgram = new Zend_Form_Element_Hidden('IdProgram');
		$IdProgram->removeDecorator("DtDdWrapper");
		$IdProgram->setAttrib('required',"true") ;
		$IdProgram->removeDecorator("Label");
		$IdProgram->removeDecorator('HtmlTag');

		$ProgramDescription = new Zend_Form_Element_Textarea('ProgramDescription');
		$ProgramDescription->setAttrib('required',"true");
		$ProgramDescription->setAttrib('size', '20');
		$ProgramDescription->setAttrib('cols', '40');
		$ProgramDescription->setAttrib('rows', '4');
		$ProgramDescription->setAttrib('maxlength','150');
		$ProgramDescription->removeDecorator("DtDdWrapper");
		$ProgramDescription->removeDecorator("Label");
		$ProgramDescription->removeDecorator('HtmlTag');
		$ProgramDescription->setAttrib('dojoType',"dijit.form.Textarea");

		$landscapeDefaultLanguage = new Zend_Form_Element_Textarea('landscapeDefaultLanguage');
		$landscapeDefaultLanguage->setAttrib('required',"true");
		$landscapeDefaultLanguage->setAttrib('size', '20');
		$landscapeDefaultLanguage->setAttrib('cols', '40');
		$landscapeDefaultLanguage->setAttrib('rows', '4');
		$landscapeDefaultLanguage->setAttrib('maxlength','150');
		$landscapeDefaultLanguage->removeDecorator("DtDdWrapper");
		$landscapeDefaultLanguage->removeDecorator("Label");
		$landscapeDefaultLanguage->removeDecorator('HtmlTag');
		$landscapeDefaultLanguage->setAttrib('dojoType',"dijit.form.Textarea");
		
		$IdStartSemester = new Zend_Dojo_Form_Element_FilteringSelect('IdStartSemester');
		$IdStartSemester->removeDecorator("DtDdWrapper");
		$IdStartSemester->setAttrib('required',"true") ;
		$IdStartSemester->removeDecorator("Label");
		$IdStartSemester->removeDecorator('HtmlTag');
		$IdStartSemester->setRegisterInArrayValidator(false);
		$IdStartSemester->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Save = new Zend_Form_Element_Submit('Save');
		$Save->label = $gstrtranslate->_("Continue");
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator('HtmlTag');
		
		
		//form elements
		$this->addElements(array(
				$IdLandscape,
				$IdProgram,
				$ProgramDescription,
				$landscapeDefaultLanguage,
				$IdStartSemester,
				$Save
		));

	}
}
