<?php
/**
 * Created by Izham.
 * User: Izham
 * Date: 4/8/2016
 * Time: 10:35 AM
 */
class GeneralSetup_Form_ChecklistReport extends Zend_Dojo_Form {
    public function init() {
        $this->setMethod('post');

        $model = new GeneralSetup_Model_DbTable_ChecklistReport();

        $semester = new Zend_Form_Element_Select('semester');
        $semester->removeDecorator("DtDdWrapper");
        $semester->setAttrib('class', 'select');
        $semester->setAttrib('required', true);
        $semester->removeDecorator("Label");

        $semester->addMultiOption('', '-- Select --');

        $semList = $model->getSemester();

        if ($semList){
            foreach ($semList as $semLoop){
                $semester->addMultiOption($semLoop['IdSemesterMaster'], $semLoop['SemesterMainName'].' - '.$semLoop['SemesterMainCode']);
            }
        }

        $this->addElements(array(
            $semester
        ));
    }
}