<?php
class GeneralSetup_Form_Departmentmaster extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$joiningdate = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		
    	$IdDepartment = new Zend_Form_Element_Hidden('IdDepartment');
        $IdDepartment->removeDecorator("DtDdWrapper");
        $IdDepartment->removeDecorator("Label");
        $IdDepartment->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
   		
        $DepartmentName = new Zend_Form_Element_Text('DepartmentName');
	$DepartmentName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $DepartmentName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')     
        		->setAttrib('propercase','true')    
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
      	$ShortName = new Zend_Form_Element_Text('ShortName');
		$ShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ShortName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $ArabicName = new Zend_Form_Element_Text('ArabicName');
        $ArabicName->setAttrib('dojoType',"dijit.form.TextBox");
        $ArabicName//->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');		
        		
        $DeptCode = new Zend_Form_Element_Text('DeptCode',array('regExp'=>"^[a-zA-Z0-9]+$",'invalidMessage'=>""));
		$DeptCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $DeptCode->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','25')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');	

        $DepartmentType  = new Zend_Form_Element_Radio('DepartmentType');
		$DepartmentType->setAttrib('dojoType',"dijit.form.RadioButton");
        $DepartmentType->addMultiOptions(array('0' => 'Admin','1' => 'School'))
        			->setvalue('0')
        			->setSeparator('&nbsp;')
        			->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag')
        			->setAttrib('onclick', 'fnToggleCollegeDetails(this.value)');	

        $IdCollege = new Zend_Dojo_Form_Element_FilteringSelect('IdCollege');
        $IdCollege->removeDecorator("DtDdWrapper");
        $IdCollege->setAttrib('required',"true") ;
        $IdCollege->removeDecorator("Label");
        $IdCollege->removeDecorator('HtmlTag');
        //$IdCollege->setAttrib('OnChange', 'fnGetBrancheList');
        $IdCollege->setRegisterInArrayValidator(false);
		$IdCollege->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		
		$FromDate = new Zend_Form_Element_Text('FromDate');
		$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $FromDate->setAttrib('required',"true")
			     ->setAttrib('constraints', "$joiningdate")	 
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

		$ToDate = new Zend_Form_Element_Hidden('ToDate');
		$ToDate	->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');        		

        $IdStaff = new Zend_Dojo_Form_Element_FilteringSelect('IdStaff');
        $IdStaff->setAttrib('OnChange', "fnGetStaffList(this)");
        $IdStaff->removeDecorator("DtDdWrapper");
        $IdStaff->setAttrib('required',"true") ;
        $IdStaff->removeDecorator("Label");
        $IdStaff->removeDecorator('HtmlTag');
        $IdStaff->setRegisterInArrayValidator(false);
		$IdStaff->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		
		$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
	
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
	
				
		//New added 8/5/2014
		$StartDate = new Zend_Dojo_Form_Element_DateTextBox('StartDate');
        $StartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $StartDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$StartDate->setAttrib('required',"false");
        $StartDate->removeDecorator("DtDdWrapper");
        $StartDate->setAttrib('title',"dd-mm-yyyy");
        $StartDate->removeDecorator("Label");
        $StartDate->removeDecorator('HtmlTag'); 
        
        $EndDate = new Zend_Dojo_Form_Element_DateTextBox('EndDate');
        $EndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $EndDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$EndDate->setAttrib('required',"false");
        $EndDate->removeDecorator("DtDdWrapper");
        $EndDate->setAttrib('title',"dd-mm-yyyy");
        $EndDate->removeDecorator("Label");
        $EndDate->removeDecorator('HtmlTag'); 
		
		$defDB = new App_Model_General_DbTable_Definationms();
		$address_type = $defDB->getDataByType(98);	//Address Type 	
		
		$AddressType = new Zend_Dojo_Form_Element_FilteringSelect('AddressType');
		$AddressType->removeDecorator("DtDdWrapper");
		$AddressType->setAttrib('required',"true") ;
		$AddressType->removeDecorator("Label");
		$AddressType->removeDecorator('HtmlTag');		
		$AddressType->setRegisterInArrayValidator(false);
		$AddressType->setAttrib('dojoType',"dijit.form.FilteringSelect");		
    
		foreach ($address_type as $at){			
			$AddressType->addMultiOption($at['idDefinition'],$at['DefinitionDesc']);
		}
		
		//Correspondance Adderrss
		$Addr1 = new Zend_Form_Element_Text('Addr1');
		$Addr1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Addr1->setAttrib('required',"true")
		->setAttrib('maxlength','200')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Addr2 = new Zend_Form_Element_Text('Addr2');
		$Addr2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Addr2->setAttrib('required',"false")
		->setAttrib('maxlength','200')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$idCountry = new Zend_Dojo_Form_Element_FilteringSelect('idCountry');
		$idCountry->removeDecorator("DtDdWrapper");
		$idCountry->setAttrib('required',"true") ;
		$idCountry->removeDecorator("Label");
		$idCountry->removeDecorator('HtmlTag');
		$idCountry->setAttrib('OnChange', 'fnGetCountryStateList');
		$idCountry->setRegisterInArrayValidator(false);
		$idCountry->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$idState = new Zend_Dojo_Form_Element_FilteringSelect('idState');
		$idState->removeDecorator("DtDdWrapper");
		$idState->setAttrib('required',"true") ;
		$idState->removeDecorator("Label");
		$idState->removeDecorator('HtmlTag');
		$idState->setRegisterInArrayValidator(false);
		$idState->setAttrib('OnChange', "fnGetStateCityList(this.value); checkOthers('idState')");
		$idState->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$State_Others = new Zend_Form_Element_Text('State_Others');
        $State_Others->removeDecorator("DtDdWrapper");
        $State_Others->setAttrib('required',"false") ;
        $State_Others->removeDecorator("Label");
        $State_Others->removeDecorator('HtmlTag');
        $State_Others->setAttrib('OnChange', '');
		$State_Others->setAttrib('dojoType',"dijit.form.ValidationTextBox");

		$City = new Zend_Dojo_Form_Element_FilteringSelect('City');
        $City->removeDecorator("DtDdWrapper");
        $City->removeDecorator("Label");
        $City->removeDecorator('HtmlTag');
        $City->setAttrib('required',"false");
		$City->setAttrib('OnChange',"checkOthers('City')");
        $City->setRegisterInArrayValidator(false);
		$City->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$City_Others = new Zend_Form_Element_Text('City_Others');
        $City_Others->removeDecorator("DtDdWrapper");
        $City_Others->removeDecorator("Label");
        $City_Others->removeDecorator('HtmlTag');
        $City_Others->setAttrib('required',"false");
		$City_Others->setAttrib('dojoType',"dijit.form.ValidationTextBox");

		$Email = new Zend_Form_Element_Text('Email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$Email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Email->setAttrib('required',"true")
		->setAttrib('maxlength','50')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

	
		$Phone = new Zend_Form_Element_Text('Phone'); ///*,array('regExp'=>"[0-9]+",'invalidMessage'=>"Not a valid Home Phone No.")*/
		$Phone->setAttrib('dojoType',"dijit.form.ValidationTextBox");			
		$Phone->removeDecorator("DtDdWrapper");
		$Phone->removeDecorator("Label");
		$Phone->removeDecorator('HtmlTag');


		$zipCode = new Zend_Form_Element_Text('zipCode');
		$zipCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$zipCode->setAttrib('maxlength','20');
		$zipCode->removeDecorator("DtDdWrapper");
		$zipCode->removeDecorator("Label");
		$zipCode->removeDecorator('HtmlTag');
		
		$AffiliatedTo = new Zend_Dojo_Form_Element_FilteringSelect('AffiliatedTo');
		$AffiliatedTo->removeDecorator("DtDdWrapper");
		$AffiliatedTo->setAttrib('required',"true") ;
		$AffiliatedTo->removeDecorator("Label");
		$AffiliatedTo->removeDecorator('HtmlTag');
		$AffiliatedTo->setRegisterInArrayValidator(false);
		$AffiliatedTo->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		
        //form elements
        $this->addElements(array($IdDepartment,$UpdDate,$UpdUser,$DepartmentName,$ArabicName,$DeptCode,$DepartmentType,
        						$IdCollege,$Active,$ShortName,
        						 $Save,$Clear,$Add,$FromDate,$ToDate,$IdStaff,
        						 $StartDate,$EndDate,
        						  $Addr1,
                                 $Addr2,                                
                                 $idState,
                                 $idCountry,
                                 $zipCode,
                                 $AffiliatedTo,
                                 $Email,
                                $Phone,                                
								$AddressType,
								$State_Others,
								$City,
								$City_Others
                                 ));

    }
}