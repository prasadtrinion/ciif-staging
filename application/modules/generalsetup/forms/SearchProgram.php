<?php 

class GeneralSetup_Form_SearchProgram extends Zend_Form
{
	
	public function init()
	{
				
		$this->setMethod('post');
		$this->setAttrib('id','Search');
						
		
		$this->addElement('text','program_name', array(
			'label'=>$this->getView()->translate('Programme Name'),
			'class'=>'input-txt'
		));

		$this->addElement('text','program_code', array(
			'label'=>$this->getView()->translate('Programme Code'),
			'class'=>'input-txt'
		));
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('Search'),
          'decorators'=>array('ViewHelper')
        ));
        
        
        $this->addDisplayGroup(array('save'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	    
	}
		
}

?>