<?php 
class GeneralSetup_Form_Resources extends Zend_Form{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_add');
		
				
		//top menu
		$this->addElement('select','r_menu_id', array(
			'label'=>'Top Menu (Top Menu)',
		    'required'=>true,
		    'onChange'=>'getTitle()'
		));
				
		$this->r_menu_id->addMultiOption(null,"-- Please Select --");		
	
		$topMenuDb = new GeneralSetup_Model_DbTable_TopMenu();
		$menu_list = $topMenuDb->getMenuList();
		
		if(count($menu_list)>0){
			foreach ($menu_list as $list){		
				$this->r_menu_id->addMultiOption($list['tm_id'],$list['tm_name']);
			}
		}

		
	   //title sidebar
		$this->addElement('select','r_title_id', array(
			'label'=>'Title',
		    'required'=>true,
		    'onChange'=>'getScreen()'
		));	
		$this->r_title_id->setRegisterInArrayValidator(false);			
		$this->r_title_id->addMultiOption(null,"-- Please Select --");		
	    
		
		 //screen sidebar
		$this->addElement('select','r_screen_id', array(
			'label'=>'Screen',
		    'required'=>true
		));			
		$this->r_screen_id->setRegisterInArrayValidator(false);		
		$this->r_screen_id->addMultiOption(null,"-- Please Select --");		
	
		
		$this->addElement('text','r_module', array(
				'label'=>'Module',
				'required'=>true
		));
		
								
		$this->addElement('text','r_controller', array(
			'label'=>'Controller',
			'required'=>true
		));

		$this->addElement('text','r_action', array(
				'label'=>'Action',
				'required'=>true
		));
		
	
		
		//visibility
		$this->addElement('radio','r_group', array(
			'label'=>'Group',
			'required'=>true
		));
		
		$this->r_group->addMultiOptions(array(
			'1' => 'View',
			'2' => 'Add',
			'3' => 'Edit',
			'4' => 'Delete',
			'5' => 'Approve',
		))->setSeparator('<br>')->setValue("1");
		

			
		
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>'Cancel',
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'generalsetup', 'controller'=>'manage-resources','action'=>'view-resources'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>