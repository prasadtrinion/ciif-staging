<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GeneralSetup_Form_InstructorWorkload extends Zend_Dojo_Form {
    
    public function init() {
        $this->setMethod('post');
        $gstrtranslate =Zend_Registry::get('Zend_Translate');
        
        $model = new GeneralSetup_Model_DbTable_InstructorWorkload();
        
        //position
        $position = new Zend_Form_Element_Select('position');
	$position->removeDecorator("DtDdWrapper");
        $position->setAttrib('class', 'select');
        $position->setAttrib('required', true);
	$position->removeDecorator("Label");
        
        $position->addMultiOption('', '-- Select --');
        
        $positionList = $model->getPosition();
        
        if ($positionList){
            foreach ($positionList as $positionLoop){
                $position->addMultiOption($positionLoop['key'], $positionLoop['value']);
            }
        }
        
        //staff job type
        $jobtype = new Zend_Form_Element_Select('jobtype');
	$jobtype->removeDecorator("DtDdWrapper");
        $jobtype->setAttrib('class', 'select');
        $jobtype->setAttrib('required', true);
	$jobtype->removeDecorator("Label");
        
        $jobtype->addMultiOption('', '-- Select --');
        
        $jobtype->addMultiOptions(array(
            '1' => 'Full time',
            '0' => 'Part Time'
        ));
        
        //workload type
        $workloadtype = new Zend_Form_Element_Select('workloadtype');
	$workloadtype->removeDecorator("DtDdWrapper");
        $workloadtype->setAttrib('class', 'select');
        $workloadtype->setAttrib('required', true);
        $workloadtype->setAttrib('onchange', 'defineWorkloadType(this.value);');
	$workloadtype->removeDecorator("Label");
        
        $workloadtype->addMultiOption('', '-- Select --');
        
        $workloadList = $model->getDefination(182);
        
        if ($workloadList){
            foreach ($workloadList as $workloadLoop){
                $workloadtype->addMultiOption($workloadLoop['idDefinition'], $workloadLoop['DefinitionDesc']);
            }
        }
        
        //minimum credit hours
        $min = new Zend_Form_Element_Text('min');
	$min->setAttrib('class', 'input-txt')
            ->setAttrib('onkeyup', 'comparenumbermin(this.value);')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //maximum credit hours
        $max = new Zend_Form_Element_Text('max');
	$max->setAttrib('class', 'input-txt')
            ->setAttrib('onkeyup', 'comparenumbermax(this.value);')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        //no of student
        $nostudent = new Zend_Form_Element_Text('nostudent');
	$nostudent->setAttrib('class', 'input-txt')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
        $this->addElements(array(
            $position,
            $jobtype,
            $workloadtype,
            $min,
            $max,
            $nostudent
        ));
    }
}