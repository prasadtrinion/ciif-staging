<?php
class GeneralSetup_Form_Branchofficevenue extends Zend_Dojo_Form {
	public function init(){
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		$IdBranch = new Zend_Form_Element_Hidden('IdBranch');
		$IdBranch->setAttrib('id','IdBranch')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');
			
		$IdRegistration = new Zend_Form_Element_Hidden('IdRegistration');
		$IdRegistration	->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');
			
		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
		$UpdDate->removeDecorator("DtDdWrapper");
		$UpdDate->removeDecorator("Label");
		$UpdDate->removeDecorator('HtmlTag');
		 
		$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->setAttrib('id','UpdUser')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');


		$BranchName = new Zend_Form_Element_Text('BranchName');
		$BranchName  ->setAttrib('dojoType',"dijit.form.ValidationTextBox")
		-> setAttrib('required',"true")
		->setAttrib('maxlength','50')
		->addFilter('StripTags')
		->addFilter('StringTrim')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');
		
		
		$AddressType = new Zend_Dojo_Form_Element_FilteringSelect('AddressType');
		$AddressType->removeDecorator("DtDdWrapper");
		$AddressType->setAttrib('required',"true") ;
		$AddressType->removeDecorator("Label");
		$AddressType->removeDecorator('HtmlTag');		
		$AddressType->setRegisterInArrayValidator(false);
		$AddressType->setAttrib('dojoType',"dijit.form.FilteringSelect");		
    
		$defDB = new App_Model_General_DbTable_Definationms();
		$address_type = $defDB->getDataByType(98);	//Address Type 	
		foreach ($address_type as $at){			
			$AddressType->addMultiOption($at['idDefinition'],$at['DefinitionDesc']);
		}
		
		$Addr1 = new Zend_Form_Element_Text('Addr1');
		$Addr1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Addr1->setAttrib('required',"true")
		->setAttrib('maxlength','500')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Addr2 = new Zend_Form_Element_Text('Addr2');
		$Addr2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Addr2->setAttrib('required',"false")
		->setAttrib('maxlength','500')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$idCountry = new Zend_Dojo_Form_Element_FilteringSelect('idCountry');
		$idCountry->removeDecorator("DtDdWrapper");
		$idCountry->setAttrib('required',"true") ;
		$idCountry->removeDecorator("Label");
		$idCountry->removeDecorator('HtmlTag');
		$idCountry->setAttrib('OnChange', 'fnGetCountryStateList');
		$idCountry->setRegisterInArrayValidator(false);
		$idCountry->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$idState = new Zend_Dojo_Form_Element_FilteringSelect('idState');
		$idState->removeDecorator("DtDdWrapper");
		$idState->setAttrib('required',"true") ;
		$idState->removeDecorator("Label");
		$idState->removeDecorator('HtmlTag');
		$idState->setRegisterInArrayValidator(false);
		$idState->setAttrib('OnChange', "fnGetStateCityList(this.value); checkOthers('idState')");
		$idState->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		
		$State_Others = new Zend_Form_Element_Text('State_Others');
        $State_Others->removeDecorator("DtDdWrapper");
        $State_Others->setAttrib('required',"false") ;
        $State_Others->removeDecorator("Label");
        $State_Others->removeDecorator('HtmlTag');
        $State_Others->setAttrib('OnChange', '');
		$State_Others->setAttrib('dojoType',"dijit.form.ValidationTextBox");

		$City = new Zend_Dojo_Form_Element_FilteringSelect('City');
        $City->removeDecorator("DtDdWrapper");
        $City->removeDecorator("Label");
        $City->removeDecorator('HtmlTag');
        $City->setAttrib('required',"false");
		$City->setAttrib('OnChange',"checkOthers('City')");
        $City->setRegisterInArrayValidator(false);
		$City->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$City_Others = new Zend_Form_Element_Text('City_Others');
        $City_Others->removeDecorator("DtDdWrapper");
        $City_Others->removeDecorator("Label");
        $City_Others->removeDecorator('HtmlTag');
        $City_Others->setAttrib('required',"false");
		$City_Others->setAttrib('dojoType',"dijit.form.ValidationTextBox");

		$Email = new Zend_Form_Element_Text('Email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$Email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Email->setAttrib('required',"false")
		->setAttrib('maxlength','50')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$countrycode = new Zend_Form_Element_Text('countrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$countrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$countrycode->setAttrib('maxlength','3');
		$countrycode->setAttrib('style','width:30px');
		$countrycode->removeDecorator("DtDdWrapper");
		$countrycode->removeDecorator("Label");
		$countrycode->removeDecorator('HtmlTag');

		$statecode = new Zend_Form_Element_Text('statecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$statecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$statecode->setAttrib('maxlength','2');
		$statecode->setAttrib('style','width:30px');
		$statecode->removeDecorator("DtDdWrapper");
		$statecode->removeDecorator("Label");
		$statecode->removeDecorator('HtmlTag');


		$Phone = new Zend_Form_Element_Text('Phone',array('regExp'=>"[0-9]+",'invalidMessage'=>"Not a valid Home Phone No."));
		$Phone->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Phone->setAttrib('maxlength','10');
		$Phone->setAttrib('style','width:93px');
		$Phone->removeDecorator("DtDdWrapper");
		$Phone->removeDecorator("Label");
		$Phone->removeDecorator('HtmlTag');


		$zipCode = new Zend_Form_Element_Text('zipCode');
		$zipCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$zipCode->setAttrib('maxlength','20');
		$zipCode->removeDecorator("DtDdWrapper");
		$zipCode->removeDecorator("Label");
		$zipCode->removeDecorator('HtmlTag');

		$IdType = new Zend_Dojo_Form_Element_FilteringSelect('IdType');
		$IdType->addMultiOptions(array('1' => 'Internal',									  
        							   '2' => 'External'));
		$IdType->setAttrib('required',"true");
		$IdType->removeDecorator("DtDdWrapper");
		$IdType->removeDecorator("Label");
		$IdType->removeDecorator('HtmlTag');
		$IdType->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$IdLink = new Zend_Dojo_Form_Element_FilteringSelect('IdLink');
		$IdLink->addMultiOptions(array('1' => 'Faculty',
									    '2' => 'Department',
        								'3' => 'Others'));
		$IdLink->setAttrib('required',"false");
		$IdLink->removeDecorator("DtDdWrapper");
		$IdLink->removeDecorator("Label");
		$IdLink->removeDecorator('HtmlTag');
		$IdLink->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$IdLinkType = new Zend_Dojo_Form_Element_FilteringSelect('IdLinkType');
		$IdLinkType->addMultiOptions(array('1' => 'Faculty',
									    '2' => 'Department',
        								'3' => 'Others'));
		$IdLinkType->setAttrib('required',"false");
		$IdLinkType->removeDecorator("DtDdWrapper");
		$IdLinkType->removeDecorator("Label");
		$IdLinkType->removeDecorator('HtmlTag');
		$IdLinkType->setAttrib('dojoType',"dijit.form.FilteringSelect");




		$Active  = new Zend_Form_Element_Checkbox('Active');
		$Active->setAttrib('dojoType',"dijit.form.CheckBox");
		$Active->setvalue('1');
		$Active->removeDecorator("DtDdWrapper");
		$Active->removeDecorator("Label");
		$Active->removeDecorator('HtmlTag');

		$AffiliatedTo = new Zend_Dojo_Form_Element_FilteringSelect('AffiliatedTo');
		$AffiliatedTo->removeDecorator("DtDdWrapper");
		$AffiliatedTo->setAttrib('required',"true") ;
		$AffiliatedTo->removeDecorator("Label");
		$AffiliatedTo->removeDecorator('HtmlTag');
		$AffiliatedTo->setRegisterInArrayValidator(false);
		$AffiliatedTo->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$RegistrationLoc = new Zend_Dojo_Form_Element_FilteringSelect('RegistrationLoc');
		$RegistrationLoc->removeDecorator("DtDdWrapper");
		$RegistrationLoc->setAttrib('required',"false") ;
		$RegistrationLoc->removeDecorator("Label");
		$RegistrationLoc->removeDecorator('HtmlTag');
		$RegistrationLoc->setRegisterInArrayValidator(false);
		$RegistrationLoc->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$Programme = new Zend_Dojo_Form_Element_FilteringSelect('Programme');
		$Programme->removeDecorator("DtDdWrapper");
		$Programme->setAttrib('required',"false") ;
		$Programme->removeDecorator("Label");
		$Programme->removeDecorator('HtmlTag');
		$Programme->setRegisterInArrayValidator(false);
		$Programme->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$Branch = new Zend_Dojo_Form_Element_FilteringSelect('Branch');
		$Branch->removeDecorator("DtDdWrapper");
		$Branch->setAttrib('required',"true") ;
		$Branch->removeDecorator("Label");
		$Branch->removeDecorator('HtmlTag');
		$Branch->setRegisterInArrayValidator(false);
		$Branch->setAttrib('dojoType',"dijit.form.FilteringSelect");


		$Add = new Zend_Form_Element_Button('Add');
		$Add->setAttrib('class', 'NormalBtn');
		$Add->setAttrib('dojoType',"dijit.form.Button");
		$Add->setAttrib('OnClick', 'registrationLocInsert()')
		->removeDecorator("Label")
		->removeDecorator("DtDdWrapper")
		->removeDecorator('HtmlTag');

		$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype="dijit.form.Button";
		$Save->label = $gstrtranslate->_("Save");
		$Save->setAttrib('class', 'NormalBtn');
		$Save->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$Arabic = new Zend_Form_Element_Text('Arabic');
		$Arabic->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Arabic->setAttrib('required',"false")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$ShortName = new Zend_Form_Element_Text('ShortName');
		$ShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ShortName->setAttrib('required',"true")
		->setAttrib('maxlength','50')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$BranchCode = new Zend_Form_Element_Text('BranchCode');
		$BranchCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$BranchCode->setAttrib('required',"true")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		 
		$StartDate = new Zend_Dojo_Form_Element_DateTextBox('StartDate');
        $StartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $StartDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$StartDate->setAttrib('required',"false");
        $StartDate->removeDecorator("DtDdWrapper");
        $StartDate->setAttrib('title',"dd-mm-yyyy");
        $StartDate->removeDecorator("Label");
        $StartDate->removeDecorator('HtmlTag'); 
        
        $EndDate = new Zend_Dojo_Form_Element_DateTextBox('EndDate');
        $EndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $EndDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$EndDate->setAttrib('required',"false");
        $EndDate->removeDecorator("DtDdWrapper");
        $EndDate->setAttrib('title',"dd-mm-yyyy");
        $EndDate->removeDecorator("Label");
        $EndDate->removeDecorator('HtmlTag');  
        
				
		//Permanent Adderrss
		$PermanentAddr1 = new Zend_Form_Element_Text('PermanentAddr1');
		$PermanentAddr1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PermanentAddr1->setAttrib('required',"true")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$PermanentAddr2 = new Zend_Form_Element_Text('PermanentAddr2');
		$PermanentAddr2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PermanentAddr2->setAttrib('required',"false")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$PermanentidCountry = new Zend_Dojo_Form_Element_FilteringSelect('PermanentidCountry');
		$PermanentidCountry->removeDecorator("DtDdWrapper");
		$PermanentidCountry->setAttrib('required',"true") ;
		$PermanentidCountry->removeDecorator("Label");
		$PermanentidCountry->removeDecorator('HtmlTag');
		$PermanentidCountry->setAttrib('OnChange', 'PermanentfnGetCountryStateList');
		$PermanentidCountry->setRegisterInArrayValidator(false);
		$PermanentidCountry->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$PermanentidState = new Zend_Dojo_Form_Element_FilteringSelect('PermanentidState');
		$PermanentidState->removeDecorator("DtDdWrapper");
		$PermanentidState->setAttrib('required',"true");
		$PermanentidState->removeDecorator("Label");
		$PermanentidState->removeDecorator('HtmlTag');
		$PermanentidState->setRegisterInArrayValidator(false);
		$PermanentidState->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$PermanentEmail = new Zend_Form_Element_Text('PermanentEmail',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$PermanentEmail->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PermanentEmail->setAttrib('required',"false")
		->setAttrib('maxlength','50')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

	
		$PermanentPhone = new Zend_Form_Element_Text('PermanentPhone',array('regExp'=>"[0-9]+",'invalidMessage'=>"Not a valid Home Phone No."));
		$PermanentPhone->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PermanentPhone->setAttrib('maxlength','9');
		$PermanentPhone->setAttrib('style','width:93px');
		$PermanentPhone->removeDecorator("DtDdWrapper");
		$PermanentPhone->removeDecorator("Label");
		$PermanentPhone->removeDecorator('HtmlTag');


		$PermanentzipCode = new Zend_Form_Element_Text('PermanentzipCode');
		$PermanentzipCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PermanentzipCode->setAttrib('maxlength','20');
		$PermanentzipCode->removeDecorator("DtDdWrapper");
		$PermanentzipCode->removeDecorator("Label");
		$PermanentzipCode->removeDecorator('HtmlTag');
		
		$PermanentAffiliatedTo = new Zend_Dojo_Form_Element_FilteringSelect('PermanentAffiliatedTo');
		$PermanentAffiliatedTo->removeDecorator("DtDdWrapper");
		$PermanentAffiliatedTo->setAttrib('required',"true") ;
		$PermanentAffiliatedTo->removeDecorator("Label");
		$PermanentAffiliatedTo->removeDecorator('HtmlTag');
		$PermanentAffiliatedTo->setRegisterInArrayValidator(false);
		$PermanentAffiliatedTo->setAttrib('dojoType',"dijit.form.FilteringSelect");
                
                $InstitutionEmail = new Zend_Form_Element_Text('InstitutionEmail',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$InstitutionEmail->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$InstitutionEmail->setAttrib('required',"false")
		->setAttrib('maxlength','50')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
                $Phone2 = new Zend_Form_Element_Text('Phone2',array('regExp'=>"[0-9]+",'invalidMessage'=>"Not a valid Home Phone No."));
		$Phone2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Phone2->setAttrib('maxlength','10');
		$Phone2->setAttrib('style','width:93px');
		$Phone2->removeDecorator("DtDdWrapper");
		$Phone2->removeDecorator("Label");
		$Phone2->removeDecorator('HtmlTag');
                $Phone2->setAttrib('required',"true");
                
                $Currency = new Zend_Dojo_Form_Element_FilteringSelect('Currency');
		$Currency->removeDecorator("DtDdWrapper");
		$Currency->setAttrib('required',"false");
		$Currency->removeDecorator("Label");
		$Currency->removeDecorator('HtmlTag');
		$Currency->setRegisterInArrayValidator(false);
		$Currency->setAttrib('dojoType',"dijit.form.FilteringSelect");
                
                $ExemptionCriteria = new Zend_Form_Element_Text('ExemptionCriteria');
		$ExemptionCriteria->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ExemptionCriteria->setAttrib('required',"false")
		->setAttrib('maxlength','100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
        $Remark = new Zend_Form_Element_Text('Remark');
		$Remark->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Remark->setAttrib('required',"false")
		->setAttrib('maxlength','100')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		$registration_url = new Zend_Form_Element_Text('registration_url');
		$registration_url->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$registration_url->setAttrib('required',"false")
		->setAttrib('maxlength','100')
		->setAttrib('id', 'registration_url')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
                $cp = new Zend_Form_Element_Text('cp');
		$cp->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$cp->setAttrib('required',"false")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
                $cph = new Zend_Form_Element_Text('cph');
		$cph->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$cph->setAttrib('required',"false")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
                $cpemail = new Zend_Form_Element_Text('cpemail',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$cpemail->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$cpemail->setAttrib('required',"false")
		->setAttrib('maxlength','50')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
                $cphemail = new Zend_Form_Element_Text('cphemail',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$cphemail->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$cphemail->setAttrib('required',"false")
		->setAttrib('maxlength','50')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
                $mou_name = new Zend_Form_Element_Text('mou_name');
		$mou_name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$mou_name->setAttrib('required',"true")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
                $mou_desc = new Zend_Form_Element_Text('mou_desc');
		$mou_desc->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$mou_desc->setAttrib('required',"true")
		->setAttrib('maxlength','20')
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');
                
                $month= date("m"); // Month value
                $day= date("d"); //today's date
                $year= date("Y"); // Year value
                $yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
                $dateformat = "{datePattern:'dd-MM-yyyy'}";

                $mou_startDate  = new Zend_Form_Element_Text('mou_startDate');
		$mou_startDate->setAttrib('dojoType',"dijit.form.DateTextBox");
                $mou_startDate->setAttrib('onChange',"dijit.byId('mou_endDate').constraints.min = arguments[0];") ;
                $mou_startDate->setAttrib('required',"true");
                $mou_startDate->removeDecorator("DtDdWrapper");
                $mou_startDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
                $mou_startDate->removeDecorator("Label");
                $mou_startDate->removeDecorator('HtmlTag');

		$mou_endDate  = new Zend_Form_Element_Text('mou_endDate');
		$mou_endDate->setAttrib('dojoType',"dijit.form.DateTextBox");
                $mou_endDate->setAttrib('onChange',"dijit.byId('mou_startDate').constraints.max = arguments[0];") ;
                $mou_endDate->setAttrib('required',"true");
                $mou_endDate->removeDecorator("DtDdWrapper");
                $mou_endDate->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
                $mou_endDate->removeDecorator("Label");
                $mou_endDate->removeDecorator('HtmlTag');
                
		$this->addElements(
		array(
			$Branch,
			$IdRegistration,
			$RegistrationLoc,
			$AffiliatedTo,
			$Remark,
			$Programme,
			$BranchCode,
			$Add,
			$ShortName,
			$Arabic,
			$Currency,
			$IdBranch,
			$UpdDate,
			$UpdUser,
			$BranchName,
			$Addr1,
			$Addr2,
			$idCountry,
			$ExemptionCriteria,
			$idState,
			$Email,
			$InstitutionEmail,
			$Phone2,
			$countrycode,
			$statecode,
			$Phone,
			$zipCode,
			$IdType,
			$IdLink,
			$IdLinkType,
			$Save,
			$Active,
			$StartDate,
			$EndDate,		
			$AddressType,
			$cp,
			$cph,
			$cpemail,
			$cphemail,
			$mou_name,
			$mou_desc,
			$mou_startDate,
			$mou_endDate,
			$registration_url,
			$State_Others,$City,$City_Others
		)
		);
	}
}
?>