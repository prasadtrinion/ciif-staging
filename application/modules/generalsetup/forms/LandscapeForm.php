<?php 

class GeneralSetup_Form_LandscapeForm extends Zend_Form
{
	protected $programID;
	protected $landscapeId;
	protected $landscapeType;
	
	public function setProgramID($programID){
		$this->programID = $programID; 
	}
	
	public function setLandscapeId($landscapeId){
		$this->landscapeId = $landscapeId; 
	}
	
	public function setLandscapeType($landscapeType){
		$this->landscapeType = $landscapeType; 
	}
		
	
	public function init()
	{
						
		//$this->setMethod('post');
		//$this->setAttrib('id','detailForm');
		//$this->setAction('/generalsetup/landscape/add-landscape/');
						
		$this->addElement('hidden', 'IdProgram',array('value'=>$this->programID));
		$this->addElement('hidden', 'IdLandscape',array('value'=>$this->landscapeId));
		       
	    $this->addElement('select','LandscapeType', array(
			'label'=>$this->getView()->translate('Landscape Type'),
	        'onchange'=>'showAddDrop();',
			'class'=>'select'
		));
		$this->LandscapeType->removeDecorator("Label");
		$this->LandscapeType->removeDecorator("DtDdWrapper");
		$this->LandscapeType->removeDecorator('HtmlTag');
		
		if($this->landscapeId){
			$this->LandscapeType->setAttrib('disabled',true);
		}
		
		//get landscape type
		$definationDB = new App_Model_General_DbTable_Definationms();
		
		$this->LandscapeType->addMultiOption(null,$this->getView()->translate('-- Please Select --'));
		foreach ($definationDB->getDataByType(6) as $list){
			$this->LandscapeType->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
		}
		
		
		
		//get list intake Efective Intake		
		$this->addElement('select','IdStartSemester', array(
			'label'=>$this->getView()->translate('Efective Intake'),
		    'onchange'=>'getProgramScheme();',
			'class'=>'select'		
		));
		$this->IdStartSemester->removeDecorator("Label");
		$this->IdStartSemester->removeDecorator("DtDdWrapper");
		$this->IdStartSemester->removeDecorator('HtmlTag');
		
		$intakeDB = new App_Model_Record_DbTable_Intake();
		
		$this->IdStartSemester->addMultiOption(null,$this->getView()->translate('-- Please Select --'));
		foreach ($intakeDB->getData() as $list){
			$this->IdStartSemester->addMultiOption($list['IdIntake'],$list['IntakeDesc']);
		}	

		//Scheme (Program Scheme)		
		$this->addElement('select','IdProgramScheme', array(
			'label'=>$this->getView()->translate('Program Scheme'),
			'class'=>'select'
		));
		$this->IdProgramScheme->removeDecorator("Label");
		$this->IdProgramScheme->removeDecorator("DtDdWrapper");
		$this->IdProgramScheme->removeDecorator('HtmlTag');
		$this->IdProgramScheme->setRegisterInArrayValidator(false);
		
		
        $this->addElement('text','TotalCreditHours', array(
			'label'=>$this->getView()->translate('Min Total Credit Hours'),
			'required'=>true,
			'class'=>'input-txt small'));
        $this->TotalCreditHours->addValidator('Float');
        $this->TotalCreditHours->removeDecorator("Label");
		$this->TotalCreditHours->removeDecorator("DtDdWrapper");
		$this->TotalCreditHours->removeDecorator('HtmlTag');
		
        
        $this->addElement('text','MaxRepeatCourse', array(
			'label'=>$this->getView()->translate('Max Repeat Course '),
			'required'=>false,
			'class'=>'input-txt small'));
        $this->MaxRepeatCourse->addValidator('Digits');
        $this->MaxRepeatCourse->removeDecorator("Label");
		$this->MaxRepeatCourse->removeDecorator("DtDdWrapper");
		$this->MaxRepeatCourse->removeDecorator('HtmlTag');
		
        
        $this->addElement('text','MaxRepeatExam', array(
			'label'=>$this->getView()->translate('Max Repeat Exam'),
			'required'=>false,
			'class'=>'input-txt small'));
        $this->MaxRepeatExam->addValidator('Digits');
        $this->MaxRepeatExam->removeDecorator("Label");
		$this->MaxRepeatExam->removeDecorator("DtDdWrapper");
		$this->MaxRepeatExam->removeDecorator('HtmlTag');
		
		
        
         $this->addElement('text','MinRequiredPass', array(
			'label'=>$this->getView()->translate('Min Required Passed Course'),
			'required'=>false,
			'class'=>'input-txt small'));
        $this->MinRequiredPass->addValidator('Digits');
        $this->MinRequiredPass->removeDecorator("Label");
		$this->MinRequiredPass->removeDecorator("DtDdWrapper");
		$this->MinRequiredPass->removeDecorator('HtmlTag');
		
        
        $this->addElement('text','SemsterCount', array(
			'label'=>$this->getView()->translate('Total Semester'),
            'value'=>'8',
			'required'=>true,
			'class'=>'input-txt small'));
        $this->SemsterCount->addValidator('Digits');
        $this->SemsterCount->removeDecorator("Label");
		$this->SemsterCount->removeDecorator("DtDdWrapper");
		$this->SemsterCount->removeDecorator('HtmlTag');
		
        
        
        $this->addElement('text','Blockcount', array(
			'label'=>$this->getView()->translate('Total Block'),
            'value'=>'8',
			'class'=>'input-txt small'));
        $this->Blockcount->addValidator('Digits');
        $this->Blockcount->removeDecorator("Label");
		$this->Blockcount->removeDecorator("DtDdWrapper");
		$this->Blockcount->removeDecorator('HtmlTag');

		$this->addElement('text','Levelcount', array(
			'label'=>$this->getView()->translate('Total Level'),
            'value'=>'0',
			'class'=>'input-txt small'));
        $this->Levelcount->addValidator('Digits');
        $this->Levelcount->removeDecorator("Label");
		$this->Levelcount->removeDecorator("DtDdWrapper");
		$this->Levelcount->removeDecorator('HtmlTag');
		
        
       
         $this->addElement('textarea','ProgramDescription', array(
			'label'=>$this->getView()->translate('Description'),
			'class'=>'textarea'
        			 
			));
		$this->ProgramDescription->removeDecorator("Label");
		$this->ProgramDescription->removeDecorator("DtDdWrapper");
		$this->ProgramDescription->removeDecorator('HtmlTag');
		$this->ProgramDescription->setAttrib('cols', 30);
        $this->ProgramDescription->setAttrib('rows', 5);

		$this->addElement('select','AssessmentMethod', array(
			'label'=>$this->getView()->translate('Assessment Method'),
			'required'=>true,
			'class'=>'select'		
		));
		$this->AssessmentMethod->removeDecorator("Label");
		$this->AssessmentMethod->removeDecorator("DtDdWrapper");
		$this->AssessmentMethod->removeDecorator('HtmlTag');
		$this->AssessmentMethod->addMultiOption(null,$this->getView()->translate('-- Please Select --'));
		$this->AssessmentMethod->addMultiOptions(array('point' => 'Point', 'grade' => 'Grade')); //spec said hardcoded
       
		//Add/Drop
		$this->addElement('checkbox','AddDrop', array(
			'label'=>$this->getView()->translate('Add/Drop')
		));
		$this->AddDrop->removeDecorator("Label");
		$this->AddDrop->removeDecorator("DtDdWrapper");
		$this->AddDrop->removeDecorator('HtmlTag');
		
	
		if($this->landscapeType==44){
			$this->AddDrop->setAttrib('disabled', 'disabled');			
		}	
		
		if($this->landscapeType==43){			
			$this->Blockcount->setAttrib('disabled', 'disabled');
		}
		
		//Semester Type
	 	$this->addElement('select','SemesterType', array(
			'label'=>$this->getView()->translate('Semester Type'),
			'class'=>'select'
		));
		$this->SemesterType->removeDecorator("Label");
		$this->SemesterType->removeDecorator("DtDdWrapper");
		$this->SemesterType->removeDecorator('HtmlTag');		
		
		
		$this->SemesterType->addMultiOption(null,$this->getView()->translate('-- Please Select --'));
		foreach ($definationDB->getDataByType(51) as $list){
			$this->SemesterType->addMultiOption($list['idDefinition'],$list['DefinitionDesc']);
		}
		
		$this->addElement('text','MinRegCourse', array('label'=>$this->getView()->translate('Minimum No Registered Courses'),
			'class'=>'input-txt small'));			
        $this->MinRegCourse->addValidator('Digits');
        $this->MinRegCourse->setAttrib('size', 5);
        $this->MinRegCourse->removeDecorator("Label");
		$this->MinRegCourse->removeDecorator("DtDdWrapper");
		$this->MinRegCourse->removeDecorator('HtmlTag');
		
		$this->addElement('text','MaxRegCourse', array('label'=>$this->getView()->translate('Maximum No Registered Courses'),
			'class'=>'input-txt small'));			
        $this->MaxRegCourse->addValidator('Digits');
        $this->MaxRegCourse->setAttrib('size', 5);
        $this->MaxRegCourse->removeDecorator("Label");
		$this->MaxRegCourse->removeDecorator("DtDdWrapper");
		$this->MaxRegCourse->removeDecorator('HtmlTag');
		
		$this->addElement('text','MinReqPassCourse', array('label'=>$this->getView()->translate('Minimum No Required Passed Courses'),
			'class'=>'input-txt small'));			
        $this->MinReqPassCourse->addValidator('Digits');
        $this->MinReqPassCourse->setAttrib('size', 5);
        $this->MinReqPassCourse->removeDecorator("Label");
		$this->MinReqPassCourse->removeDecorator("DtDdWrapper");
		$this->MinReqPassCourse->removeDecorator('HtmlTag');
		
		
		//Semester Type
	 	$this->addElement('select','Type', array(
			'label'=>$this->getView()->translate('Type'),
			'class'=>'select'
		));
		$this->Type->removeDecorator("Label");
		$this->Type->removeDecorator("DtDdWrapper");
		$this->Type->removeDecorator('HtmlTag');		
		
		
		$this->Type->addMultiOption(null,$this->getView()->translate('-- Please Select --'));
		$this->Type->addMultiOption(1,$this->getView()->translate('No of Courses'));
		$this->Type->addMultiOption(2,$this->getView()->translate('No of Credit Hours'));
		
		//Initial
		$this->addElement('text','Initial', array('label'=>$this->getView()->translate('Initial'),
			'class'=>'input-txt small'));			
        $this->Initial->addValidator('Digits');
        $this->Initial->setAttrib('size', 5);
        $this->Initial->removeDecorator("Label");
		$this->Initial->removeDecorator("DtDdWrapper");
		$this->Initial->removeDecorator('HtmlTag');

		//Initial Type
	 	$this->addElement('select','InitialType', array(
			'label'=>$this->getView()->translate('Initial Type'),
			'class'=>'select'
		));
		$this->InitialType->removeDecorator("Label");
		$this->InitialType->removeDecorator("DtDdWrapper");
		$this->InitialType->removeDecorator('HtmlTag');		
		
		
		$this->InitialType->addMultiOption(null,$this->getView()->translate('-- Please Select --'));
		$this->InitialType->addMultiOption(1,$this->getView()->translate('No of Courses'));
		$this->InitialType->addMultiOption(2,$this->getView()->translate('No of Credit Hours'));


		
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Save',
		  'class'=>'btn-submit',
          'decorators'=>array('ViewHelper')
        ));
        
       
        
        $this->addDisplayGroup(array('save'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
        
       
        
	}
	
	
}
?>
