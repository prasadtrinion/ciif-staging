<?php
class GeneralSetup_Form_SearchAnnouncement extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');		
		$this->setAttrib('id','search_form');
		
		$defModel = new App_Model_General_DbTable_Definationms();

		$types = $defModel->getByCode('Announcement Types');

		foreach ( $types as $type )
		{
			$valuetypes[] = $type['DefinitionCode'];
		}

		//title
		$this->addElement('text','title', array(
			'label'=>'Title',
			'class'=>'input-txt'
		));

		//anntypes
		$this->addElement ( 
			'multiCheckbox', 'types', array(
			'label' => 'Types',
			'value' => $valuetypes
		));
		foreach ( $types as $type )
		{
			$this->types->addMultiOption($type['DefinitionCode'],$type['DefinitionDesc']);
		}
	
		//Intake
		$this->addElement('select','intake_id', array(
			'label'=>'Intake'
		));
            
		$intakeDb = new GeneralSetup_Model_DbTable_Intake();
		$listData = $intakeDb->fngetIntakeList();
		$this->intake_id->addMultiOption('', '--All--');
		foreach ($listData as $list){
			$this->intake_id->addMultiOption($list['IdIntake'],$list['IntakeDesc']);
		}

		//program

		$progModel = new Application_Model_DbTable_ProgramScheme();
        $progList = $progModel->fnGetProgramList();
		$this->addElement('select','program_id', array(
			'label'=>'Programme'
		));
		$this->program_id->addMultiOption('', '--All--');

		foreach ( $progList as $row )
		{
			$this->program_id->addMultiOption($row['IdProgram'], $row['ProgramName']);
		}
		
		//semester_id
		$semDB = new GeneralSetup_Model_DbTable_Semestermaster ();
		$semList = $semDB->fngetSemestermainDetails();
		
		$this->addElement('select','semester_id', array(
			'label'=>'Semester'
		));
		$this->semester_id->addMultiOption('', '--All--');
		foreach ( $semList as $row )
		{
			$this->semester_id->addMultiOption($row['IdSemesterMaster'], $row['SemesterMainName'].' - '.$row['SemesterMainCode']);
		}

		//save
		$this->addElement('submit', 'save', array(
          'label'=>'Search',
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('reset', 'clear', array(
          'label'=>'Clear',
		  'onclick'=>"window.location=window.location;",
          'decorators'=>array('ViewHelper'),
         ));
        
        $this->addDisplayGroup(array('save','clear'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}	
?>