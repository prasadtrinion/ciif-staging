<?php 
class GeneralSetup_Form_SearchResource extends Zend_Form{
	
	public function init()
	{
		$this->setMethod('post');
		$this->setAttrib('id','form_search');
		
				
		//top menu
		$this->addElement('select','r_menu_id', array(
			'label'=>'Top Menu (Top Menu)',		   
		    'onChange'=>'getTitle()'
		));
				
		$this->r_menu_id->addMultiOption(null,"-- All --");		
	
		$topMenuDb = new GeneralSetup_Model_DbTable_TopMenu();
		$menu_list = $topMenuDb->getMenuList();
		
		if(count($menu_list)>0){
			foreach ($menu_list as $list){		
				$this->r_menu_id->addMultiOption($list['tm_id'],$list['tm_name']);
			}
		}

		
	   //title sidebar
		$this->addElement('select','r_title_id', array(
			'label'=>'Title',		   
		    'onChange'=>'getScreen()'
		));	
		$this->r_title_id->setRegisterInArrayValidator(false);			
		$this->r_title_id->addMultiOption(null,"-- All --");		
	    
		
		 //screen sidebar
		$this->addElement('select','r_screen_id', array(
			'label'=>'Screen'
		));			
		$this->r_screen_id->setRegisterInArrayValidator(false);		
		$this->r_screen_id->addMultiOption(null,"-- All --");		
	
		$this->addElement('text','r_module', array(
				'label'=>'Module'
		));
								
		$this->addElement('text','r_controller', array(
			'label'=>'Controller'
		));

		$this->addElement('text','r_action', array(
				'label'=>'Action'
		));
			
		
		//visibility
		$this->addElement('select','r_group', array(
			'label'=>'Group'
		));
		$this->r_group->addMultiOption(null,'-- All --');
		$this->r_group->addMultiOption(1,'View');
		$this->r_group->addMultiOption(2,'Add');
		$this->r_group->addMultiOption(3,'Edit');
		$this->r_group->addMultiOption(4,'Delete');
		$this->r_group->addMultiOption(5,'Approve');
		

		
		
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>'Submit',
          'decorators'=>array('ViewHelper')
        ));
        
               
        $this->addDisplayGroup(array('save'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
		
	}
}
?>