<?php

class App_Form_SearchExamSchedule extends Zend_Form {
	
	public function init(){
		$this->setName('search');
		$this->setMethod('post');
		$this->setAttrib('id','search_form');
		$this->setMethod('post');
		
		$ec_id	= new Zend_Form_Element_Select('ec_id');
		$ec_id ->removeDecorator("DtDdWrapper")
            		->removeDecorator("Label")
            		->removeDecorator("HtmlTag");	
            		            
		$this->addElement('select', 'ec_id', array(
			              'required'    => false
        ));
        
        $exam_center_db = new App_Model_ExamSchedule();
	 	$ec_list = $exam_center_db->getExamCenterList();
        
        $ec_id->addMultiOption('', '-- All --');
        
        if (count($ec_list) > 0){
            foreach ($ec_list as $ec){
                $ec_id->addMultiOption($ec['ec_id'], strtoupper($ec['ec_name']));
            }
        }
        
		$date_from	= new Zend_Form_Element_Text('date_from');
		$date_from	->setLabel('Exam Center')
					->setAttrib('class', 'datepicker')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
            		->removeDecorator("Label")
            		->removeDecorator("HtmlTag");
					
        $date_to = new Zend_Form_Element_Text('date_to');
        $date_to	->setAttrib('class', 'datepicker')
        		    ->setRequired(true)
		            ->removeDecorator("DtDdWrapper")
		            ->removeDecorator("Label")
		            ->removeDecorator("HtmlTag");

		            
		$days	= new Zend_Form_Element_Select('days');	 
		$days ->removeDecorator("DtDdWrapper")
            		->removeDecorator("Label")
            		->removeDecorator("HtmlTag");	           
		$this->addElement('select', 'days', array(
			              'required'    => false
        ));
        
        $days->addMultiOption('', '-- All --');
		$timestamp = strtotime('next Sunday');		
		for ($i = 0; $i < 7; $i++) {
		    $days->addMultiOption($i, strftime('%A', $timestamp));
		    $timestamp = strtotime('+1 day', $timestamp);
		}
		$this->addElements(array($ec_id,$date_from,$date_to,$days));
		
        		
	}
}
?>

<script>
$(function () {
    $("#date_from").datepicker({
        minDate: "dateToday",
        changeMonth: true,
        dateFormat: 'yy-mm-dd',
        onClose: function (selectedDate, instance) {
            if (selectedDate != '') { //added this to fix the issue
                $("#date_to").datepicker("option", "minDate", selectedDate);
                var date = $.datepicker.parseDate(instance.settings.dateFormat, selectedDate, instance.settings);
                date.setMonth(date.getMonth() + 1);
                console.log(selectedDate, date);
                $("#date_to").datepicker("option", "minDate", selectedDate);
                $("#date_to").datepicker("option", "maxDate", date);
            }
        }
    });
    $("#date_to").datepicker({
        minDate: "dateToday",
        changeMonth: true,
        dateFormat: 'yy-mm-dd',
        onClose: function (selectedDate) {
            $("#date_from").datepicker("option", "maxDate", selectedDate);
        }
    });
});
</script>