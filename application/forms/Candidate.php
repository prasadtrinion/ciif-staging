<?php

class App_Form_Candidate extends App_Form_Base {

    public function init(){

        //username
        $icno = new Zend_Form_Element_Text('icno');
        $icno->setAttrib('class', '')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator("HtmlTag");

        $this->addElements(array(
            $icno,

        ));

    }
}
?>