<?php

class App_Form_Roleregister extends App_Form_Base {
    
    public function init(){




        $link_id = new Zend_Form_Element_Select('link_id','');
        $link_id->setAttrib('class', 'form-control')
                    
                    ->setAttrib('get_required_files()', true)
                    ->addValidator('NotEmpty',true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $link_id->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

        $dataDb = new App_Model_Link();
        $appcategoryList4 = $dataDb->fetchAll();  
            // echo "<pre>";
            // print_r($appcategoryList);

            foreach ($appcategoryList4 as $key4 => $value4) {
               // echo $key;
                //echo $value['DefinitionDesc'].'<br>';
                $link_id->addMultiOption($value4['id'],$value4['name']);
            }
            


        //      $menu = new Zend_Form_Element_Select('menu','');
        // $menu->setAttrib('class', 'form-control')
                    
        //             ->setAttrib('get_required_files()', true)
        //             ->addValidator('NotEmpty',true)
        //     ->removeDecorator("DtDdWrapper")
        //     ->removeDecorator("Label");

        // $menu->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

        // $dataDb = new App_Model_Menu();
        // $appcategoryList4 = $dataDb->get_menu_name();  
         
        //     foreach ($appcategoryList4 as $key4 => $value4) {
        //        // echo $key;
        //         //echo $value['DefinitionDesc'].'<br>';
        //         $menu->addMultiOption($value4['id'],$value4['menu']);
        //     }
            




        //first_name
        $menu = new Zend_Form_Element_Text('menu');
        $menu->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $name = new Zend_Form_Element_Text('name');
        $name->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $module = new Zend_Form_Element_Text('module');
        $module->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        $controller = new Zend_Form_Element_Text('controller');
        $controller->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $action = new Zend_Form_Element_Text('action');
        $action->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        $sub_menu = new Zend_Form_Element_Text('sub_menu');
        $sub_menu->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $description = new Zend_Form_Element_Text('description');
        $description->setAttrib('class', 'form-control')
           // ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //last_name
        $lastname = new Zend_Form_Element_Text('lastname');
        $lastname->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //email
        $email = new Zend_Form_Element_Text('email');
        $email->setAttrib('class', 'form-control')
                ->setAttrib('autocomplete', 'off')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        //$contactno
        $contactno = new Zend_Form_Element_Text('contactno');
        $contactno->setAttrib('class', 'form-control')
                         ->setAttrib('style','width:18.5em !important')

            ->setAttrib('maxlength',15)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        $role_id = new Zend_Form_Element_Select('role_id','');
        $role_id->setAttrib('class', 'form-control')
                    
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $role_id->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

           $definationDB =new Admin_Model_DbTable_Definition();

            $learningLists = $definationDB->getByCode('Role');

       // $country = $countryDb->fetchAll();  


            foreach ($learningLists as $key8 => $value8)
            {
               $role_id->addMultiOption($value8['idDefinition'],$value8['DefinitionDesc']);
            }


        //password
        $password = new Zend_Form_Element_Password('password');
        $password->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //password
        $newpassword = new Zend_Form_Element_Password('newpassword');
        $newpassword->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //timezone
        $timezone = new Zend_Form_Element_Select('timezone');
        $timezone->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $timezone->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('Select'));
        foreach ( Cms_Common::getTimezones() as $tz => $tz_name )
        {
            $timezone->addMultiOption($tz, $tz_name);
        }
        
      $radio_Industrialexp_type = $this->createElement('radio', 'Experience_type', array(
            'multiOptions'=>array(
                '1' => 'Yes',
                '0' => 'No',
            ),
        ));

        
       $radio_Industrialexp_type->setAttrib('id','Experience_type')
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->setAttrib('class', 'check-radio')
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper")
                         ->setSeparator('');



        $radio_Lectureshipexp_type = $this->createElement('radio', 'lectureshipRadio', array(
            'multiOptions'=>array(
                '1' => 'Yes',
                '0' => 'No',
            ),
        ));

             //first_name
        $firstname = new Zend_Form_Element_Text('firstname');
        $firstname->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

          //first_name
        $username = new Zend_Form_Element_Text('username');
        $username->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
       $radio_Lectureshipexp_type->setAttrib('id','lectureshipRadio')
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper")
                         ->setSeparator('');                 
         //username
        $admin_notes = new Zend_Form_Element_Text('admin_notes');
        $admin_notes->setAttrib('class', 'form-control')
           // ->setAttrib('maxlength', '12')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator("HtmlTag");

     //banking_notes
        $banking_notes = new Zend_Form_Element_Text('banking_notes');
        $banking_notes->setAttrib('class', 'form-control')
           // ->setAttrib('maxlength', '12')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator("HtmlTag");



        $this->addElements(array(
                                $username,
                                $firstname,
                                $lastname,
                                $link_id,
                                $role_id,
                                $menu,
                                 $name,
                                $module,
                                $controller,
                                $action,
                                $description,
                                 $admin_notes,
                                  $banking_notes,
                              
                                 $sub_menu,
                                $email,
                                $contactno,
                                $password,
                                $newpassword,
                                $timezone,
                                $contactno,                                                 
                             
                               
        ));
        
    }
}
?>
<script>

$('#program').change(function()
{
    loadrequirement();

});

$('#appcategory').change(function() {
    var appcategory = $('#appcategory option:selected').val();

    if(appcategory != ""){
        if (appcategory!=1){//if qua only n qua+membership
            $('#divprog').css({'display': ''});
            $('#divreq').css({'display': ''});

            if (appcategory==3) {
                $('#divmember').css({'display': ''});
            }else{
                $('#divmember').css({'display': 'none'});
            }
        }else{//if membership only =1
            $('#membership').empty();
            $('#divmember').css({'display': ''});
            $('#divprog').css({'display': 'none'});
            $('#divreq').css({'display': 'none'});
            getmembership();
        }
    }

});

$('#nationality').change(function() {

    var nationality = $('#nationality option:selected').val();
    //alert(nationality);
    $('#username').val('');
//
//    $('#username').attr("value", "");
    if (nationality=='MY'){
        integerOnly();
    }
});

function getmembership(){
    
    $.ajax({
        type: "post",
        async: false,
        url: "/index/get-membership-ajax",
        dataType:"json",
        data: '',
        success: function(data) {

            response = eval(data);
         
            $('#membership').empty();
         
            if(response.length > 0){
                    $('#membership').append( '<option value=""> -- Please Select -- </option>' );
                    for (var i=0; i<response.length; i++) {
                        $('#membership').append( '<option value="'+response[i].m_id+'">'+response[i].m_name +'</option>' );
                    }            
            }
        }
    }); 
}

function loadmembership () {//alert('loadmember');
    var appcategory = $('#appcategory option:selected').val();
    var programid = $('#program option:selected').val();
    //$('#membership').empty();
    if (programid!="" || appcategory == 1) {
       // alert('progid='+programid);
      //  /**/ $("#divloader").ajaxStart(function () {
    //        $(this).show();
    //    });


        var my_data = {"programid":programid};
        $.ajax({
            type: "post",
            async: false,
            url: "/index/loadmembership-ajax",
            dataType:"json",
            data: my_data,
            success: function(data) {

                /* */setTimeout(function(){
                    $("#divloader").fadeOut();

                }, 1000);

                response = eval(data);
//alert('length-' +response.length);
                if(response.length > 0){
                    if(response.length == 1){
                        $('#membership').empty();
                        var progMembershipName = response[0].ProgMembership;
                        var progMembershipId = response[0].ProgMembershipId;
                        $('#membership').append( '<option value="'+progMembershipId+'">'+progMembershipName +'</option>' );
                    }else{
                        $('#membership').empty();
                        for (var i=0; i<response.length; i++) {
                            var progMembershipName = response[i].ProgMembership;
                            var progMembershipId = response[i].ProgMembershipId;
                            $('#membership').append( '<option value="'+progMembershipId+'">'+progMembershipName +'</option>' );

                        }
                    }

                }else{
                    $('#membership').empty();
                }
            }
        });


        return returnVal;

   }//no program selected
   else{
        $('#membership').empty();

    }
}

function loadrequirement () {
//    loadmembership();
    var programid = $('#program option:selected').val();
    if (programid!="") {

        /**/ $("#divloader").ajaxStart(function () {
         $(this).show();
         });

        $('#tblqua tbody').empty();
        var my_data = {"programid":programid};
        $.ajax({
            type: "post",
            async: false,
            url: "/index/loadrequirement-ajax",
            dataType:"json",
            data: my_data,
            success: function(data) {

                /* */setTimeout(function(){
                 $("#divloader").fadeOut();

                 }, 1000);

                response = eval(data);

                if(response.length > 0){
                    for (var i=0; i<response.length; i++) {

                        var entrylevelname = 'Passed ' + response[i].EntryLevelname;
                        if(response[i].Itemname != null){
                            response[i].Itemname = response[i].Itemname;
                        }else{
                            response[i].Itemname = '';
                        }

                        if(response[i].Conditionname != null){
                            response[i].Conditionname = 'WITH ' +response[i].Conditionname;
                        }else{
                            response[i].Conditionname = '';
                        }

                        var itemname = response[i].Itemname;
                        var conditionname =  response[i].Conditionname;
//                        var idProgramEntryReq = response[i].IdProgramEntryReq;
                        var idProgramEntryReq = response[i].EntryLevel;

                        $get_lastID(entrylevelname,idProgramEntryReq,itemname,conditionname);
                        $('#tblqua tbody').append($newRow);
                    }
                }else{
                    $get_lastID('No Data',0,'','');
                    $('#tblqua tbody').append($newRow);
                }
            }
        });


        return returnVal;

    }

    loadmembership();
}

function addRowQua(){
    $get_lastID(entrylevelname,idProgramEntryReq);
    $('#tblqua tbody').append($newRow);
}


$get_lastID = function(entrylevelname,idProgramEntryReq,itemname,conditionname){

    var id = $('#tblqua tr:last-child td:first-child input').attr("name");
    var qualification = entrylevelname + itemname + conditionname ;
    if(idProgramEntryReq != '0'){
        $newRow = "<tr> \
                    <td width='2%'><input type='radio' name='progEntry' id='' value='"+idProgramEntryReq+"' /></td> \
                    <td align='left'>"+qualification+"</td> \
                   </tr>";
    }else{
        $newRow = "<tr> \
                   <td colspan='2'><label class='uk-alert-warning'><b>"+entrylevelname+"</b></label></td> \
                   </tr>";
    }

   return $newRow;


}


$(document).ready(function () {
//    var nationality = $('#nationality option:selected').val();
//    if (nationality != "" && nationality=='MY'){alert(nationality);
//        integerOnly();
//    }
});

function integerOnly(){
    // $("#username").keypress(function (e) {
    //     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    //         //display error message
    //        // $("#errmsg").html("Digits Only").show().fadeOut("slow");
    //         return false;
    //     }
    // });
}
</script>