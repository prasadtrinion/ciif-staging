<?php

class App_Form_Register extends App_Form_Base {
    
    public function init(){

        //nationality
        $nationality = new Zend_Form_Element_Select('nationality');
        $nationality->setAttrib('class', 'form-control')
                   
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $nationality->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));


        foreach( Cms_Common::getCountryList() as $code => $name ) {
            $nationality->addMultiOption($code, $name);
        }

        // Membership type dropdown start


        
        $membershiptype = new Zend_Form_Element_Select('membershiptype','');
        $membershiptype->setAttrib('class', 'form-control')
                       
                        ->addValidator('NotEmpty',true)
                        
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
       
        $membershiptype->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

        $dataDb = new Admin_Model_DbTable_Member();
        $appcategoryList = $dataDb->getmembertype();  
            
            foreach ($appcategoryList as $key => $value) {
               // echo $key;
                //echo $value['DefinitionDesc'].'<br>';
                $membershiptype->addMultiOption($value['idmember'],$value['member_name']);
               
            }




         $subclassifaication = new Zend_Form_Element_Select('subclassifaication','');
         $subclassifaication->setRegisterInArrayValidator(false);
         $subclassifaication->setAttrib('class', 'form-control')
                            ->setAttrib('id', 'subclassifaication')
                            ->addValidator('NotEmpty',true)
                            
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
         
        $subclassifaication->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

        //     $dataDb = new Admin_Model_DbTable_ExternalData();
        // $appcategoryList1 = $dataDb->getListingData('Vw_subclass');  
        //     // echo "<pre>";
        //     // print_r($appcategoryList);

        //     foreach ($appcategoryList1 as $key1 => $value1) {
        //        // echo $key;
        //         //echo $value['DefinitionDesc'].'<br>';
        //         $subclassifaication->addMultiOption($value1['idDefinition'],$value1['DefinitionDesc']);
        //         // foreach ($value as $data) {
        //         //         //echo $data1.'<br>';
        //         //     echo $value['DefinitionDesc'].'<br>';
        //         // }
        //     }
       

        $workexperience = new Zend_Form_Element_Select('workexperience','');

        $workexperience->setRegisterInArrayValidator(false);
        $workexperience->setAttrib('class', 'form-control')
                       
                        ->addValidator('NotEmpty',true)
                        
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

            $workexperience->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

      


        $pd = new Zend_Form_Element_Select('ddlQualification','');
        $pd->setAttrib('class', 'form-control')
            ->addValidator('NotEmpty',true)       
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $pd->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));


      //  App_Model_MemberIndustrialExperience
        $dataDb = new Admin_Model_DbTable_Definition();
        $appcategoryList3 = $dataDb->getdefCode('Industrial Experience');  


            foreach ($appcategoryList3 as $key3 => $value3)
            {

                $pd->addMultiOption($value3['idDefinition'],$value3['DefinitionDesc']);

            }


            $prof_des = new Zend_Form_Element_Select('prof_des','');
        $prof_des->setAttrib('class', 'form-control')
                 ->addValidator('NotEmpty',true)       
                 ->removeDecorator("DtDdWrapper")
                 ->removeDecorator("Label");

        $prof_des->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));


      //  App_Model_MemberIndustrialExperience
        $dataDb = new App_Model_MemberIndustrialExperience();
        $appcategoryList3 = $dataDb->get_prof_des(211);  


            foreach ($appcategoryList3 as $key3 => $value3)
            {

                $prof_des->addMultiOption($value3['idDefinition'],$value3['DefinitionDesc']);

            }


        $Salutation = new Zend_Form_Element_Select('Salutation','');
        $Salutation->setAttrib('class', 'form-control')
                ->setAttrib('required', true)
                 ->addValidator('NotEmpty',true)       
                 ->removeDecorator("DtDdWrapper")
                 ->removeDecorator("Label");

        $Salutation->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));


      //  App_Model_MemberIndustrialExperience
       $definationDB =new Admin_Model_DbTable_Definition();

            $learningLists = $definationDB->getByCode('Salutation');


            foreach ($learningLists as $key3 => $value3)
            {

                $Salutation->addMultiOption($value3['idDefinition'],$value3['DefinitionDesc']);

            }







        $industrialExp = new Zend_Form_Element_Select('industrialExp','');
        $industrialExp->setAttrib('class', 'form-control')
                    
                    ->setAttrib('get_required_files()', true)
                    ->addValidator('NotEmpty',true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $industrialExp->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

        $dataDb = new App_Model_MemberIndustrialExperience();
        $appcategoryList4 = $dataDb->getIndustrialExp();  
            // echo "<pre>";
            // print_r($appcategoryList);

            foreach ($appcategoryList4 as $key4 => $value4) {
               // echo $key;
                //echo $value['DefinitionDesc'].'<br>';
                $industrialExp->addMultiOption($value4['idDefinition'],$value4['DefinitionCode']);
            }
            




        $lectureship = new Zend_Form_Element_Select('lectureship','');
        $lectureship->setAttrib('class', 'form-control')
                
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $lectureship->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

            $dataDb = new App_Model_MemberIndustrialExperience();
        $appcategoryList7 = $dataDb->getLectureshipExp();  

            foreach ($appcategoryList7 as $key7 => $value7)
            {
               $lectureship->addMultiOption($value7['idDefinition'],$value7['DefinitionCode']);
            }


             $countryList = new Zend_Form_Element_Select('countryList','');
        $countryList->setAttrib('class', 'form-control')
           
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $countryList->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

            $countryDb = new App_Model_Country();

        $country = $countryDb->fetchAll();  


            foreach ($country as $key8 => $value8)
            {
               $countryList->addMultiOption($value8['idCountry'],$value8['CountryName']);
            }


             $role_level = new Zend_Form_Element_Select('role_level','');
        $role_level->setAttrib('class', 'form-control')
                    
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $role_level->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

           $definationDB =new Admin_Model_DbTable_Definition();

            $learningLists = $definationDB->getByCode('Role');

       // $country = $countryDb->fetchAll();  


            foreach ($learningLists as $key8 => $value8)
            {
               $role_level->addMultiOption($value8['idDefinition'],$value8['DefinitionDesc']);
            }


      


        //username
        $username = new Zend_Form_Element_Text('username');
        $username->setAttrib('class', 'form-control')
           // ->setAttrib('maxlength', '12')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator("HtmlTag");

        //first_name
        $firstname = new Zend_Form_Element_Text('firstname');
        $firstname->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $total_fee_amount = new Zend_Form_Element_Text('total_fee_amount');
        $total_fee_amount->setAttrib('class', 'form-control')
           // ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //last_name
        $lastname = new Zend_Form_Element_Text('lastname');
        $lastname->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //email
        $email = new Zend_Form_Element_Text('email');
        $email->setAttrib('class', 'form-control')
                ->setAttrib('autocomplete', 'off')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        //$contactno
        $contactno = new Zend_Form_Element_Text('contactno');
        $contactno->setAttrib('class', 'form-control')
                         ->setAttrib('style','width:18.5em !important')

            ->setAttrib('maxlength',15)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //password
        $password = new Zend_Form_Element_Password('password');
        $password->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //password
        $newpassword = new Zend_Form_Element_Password('newpassword');
        $newpassword->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //timezone
        $timezone = new Zend_Form_Element_Select('timezone');
        $timezone->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $timezone->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('Select'));
        foreach ( Cms_Common::getTimezones() as $tz => $tz_name )
        {
            $timezone->addMultiOption($tz, $tz_name);
        }
        
      $radio_Industrialexp_type = $this->createElement('radio', 'Experience_type', array(
            'multiOptions'=>array(
                '1' => 'Yes',
                '0' => 'No',
            ),
        ));

        
       $radio_Industrialexp_type->setAttrib('id','Experience_type')
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->setAttrib('class', 'check-radio')
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper")
                         ->setSeparator('');



        $radio_Lectureshipexp_type = $this->createElement('radio', 'lectureshipRadio', array(
            'multiOptions'=>array(
                '1' => 'Yes',
                '0' => 'No',
            ),
        ));

        
       $radio_Lectureshipexp_type->setAttrib('id','lectureshipRadio')
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper")
                         ->setSeparator('');                 
         //username
        $admin_notes = new Zend_Form_Element_Text('admin_notes');
        $admin_notes->setAttrib('class', 'form-control')
           // ->setAttrib('maxlength', '12')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator("HtmlTag");

     //banking_notes
        $banking_notes = new Zend_Form_Element_Text('banking_notes');
        $banking_notes->setAttrib('class', 'form-control')
           // ->setAttrib('maxlength', '12')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator("HtmlTag");



        $this->addElements(array($nationality,
                                $username,
                                $firstname,
                                $lastname,
                                 $admin_notes,
                                  $banking_notes,
                                  $role_level,
                                $email,
                                $contactno,
                                $password,
                                $newpassword,
                                $timezone,
                                $contactno,                                                 
                                $membershiptype,
                                $subclassifaication,
                                $workexperience,
                                $pd,
                                $prof_des,
                                $industrialExp,
                                $countryList,
                                $Salutation,
                                $lectureship,
                               // $previous,
                                $radio_Industrialexp_type,
                                $radio_Lectureshipexp_type,
                                $total_fee_amount
        ));
        
    }
}
?>
<script>

$('#program').change(function()
{
    loadrequirement();

});

$('#appcategory').change(function() {
    var appcategory = $('#appcategory option:selected').val();

    if(appcategory != ""){
        if (appcategory!=1){//if qua only n qua+membership
            $('#divprog').css({'display': ''});
            $('#divreq').css({'display': ''});

            if (appcategory==3) {
                $('#divmember').css({'display': ''});
            }else{
                $('#divmember').css({'display': 'none'});
            }
        }else{//if membership only =1
            $('#membership').empty();
            $('#divmember').css({'display': ''});
            $('#divprog').css({'display': 'none'});
            $('#divreq').css({'display': 'none'});
            getmembership();
        }
    }

});

$('#nationality').change(function() {

    var nationality = $('#nationality option:selected').val();
    //alert(nationality);
    $('#username').val('');
//
//    $('#username').attr("value", "");
    if (nationality=='MY'){
        integerOnly();
    }
});

function getmembership(){
    
    $.ajax({
        type: "post",
        async: false,
        url: "/index/get-membership-ajax",
        dataType:"json",
        data: '',
        success: function(data) {

            response = eval(data);
         
            $('#membership').empty();
         
            if(response.length > 0){
                    $('#membership').append( '<option value=""> -- Please Select -- </option>' );
                    for (var i=0; i<response.length; i++) {
                        $('#membership').append( '<option value="'+response[i].m_id+'">'+response[i].m_name +'</option>' );
                    }            
            }
        }
    }); 
}

function loadmembership () {//alert('loadmember');
    var appcategory = $('#appcategory option:selected').val();
    var programid = $('#program option:selected').val();
    //$('#membership').empty();
    if (programid!="" || appcategory == 1) {
       // alert('progid='+programid);
      //  /**/ $("#divloader").ajaxStart(function () {
    //        $(this).show();
    //    });


        var my_data = {"programid":programid};
        $.ajax({
            type: "post",
            async: false,
            url: "/index/loadmembership-ajax",
            dataType:"json",
            data: my_data,
            success: function(data) {

                /* */setTimeout(function(){
                    $("#divloader").fadeOut();

                }, 1000);

                response = eval(data);
//alert('length-' +response.length);
                if(response.length > 0){
                    if(response.length == 1){
                        $('#membership').empty();
                        var progMembershipName = response[0].ProgMembership;
                        var progMembershipId = response[0].ProgMembershipId;
                        $('#membership').append( '<option value="'+progMembershipId+'">'+progMembershipName +'</option>' );
                    }else{
                        $('#membership').empty();
                        for (var i=0; i<response.length; i++) {
                            var progMembershipName = response[i].ProgMembership;
                            var progMembershipId = response[i].ProgMembershipId;
                            $('#membership').append( '<option value="'+progMembershipId+'">'+progMembershipName +'</option>' );

                        }
                    }

                }else{
                    $('#membership').empty();
                }
            }
        });


        return returnVal;

   }//no program selected
   else{
        $('#membership').empty();

    }
}

function loadrequirement () {
//    loadmembership();
    var programid = $('#program option:selected').val();
    if (programid!="") {

        /**/ $("#divloader").ajaxStart(function () {
         $(this).show();
         });

        $('#tblqua tbody').empty();
        var my_data = {"programid":programid};
        $.ajax({
            type: "post",
            async: false,
            url: "/index/loadrequirement-ajax",
            dataType:"json",
            data: my_data,
            success: function(data) {

                /* */setTimeout(function(){
                 $("#divloader").fadeOut();

                 }, 1000);

                response = eval(data);

                if(response.length > 0){
                    for (var i=0; i<response.length; i++) {

                        var entrylevelname = 'Passed ' + response[i].EntryLevelname;
                        if(response[i].Itemname != null){
                            response[i].Itemname = response[i].Itemname;
                        }else{
                            response[i].Itemname = '';
                        }

                        if(response[i].Conditionname != null){
                            response[i].Conditionname = 'WITH ' +response[i].Conditionname;
                        }else{
                            response[i].Conditionname = '';
                        }

                        var itemname = response[i].Itemname;
                        var conditionname =  response[i].Conditionname;
//                        var idProgramEntryReq = response[i].IdProgramEntryReq;
                        var idProgramEntryReq = response[i].EntryLevel;

                        $get_lastID(entrylevelname,idProgramEntryReq,itemname,conditionname);
                        $('#tblqua tbody').append($newRow);
                    }
                }else{
                    $get_lastID('No Data',0,'','');
                    $('#tblqua tbody').append($newRow);
                }
            }
        });


        return returnVal;

    }

    loadmembership();
}

function addRowQua(){
    $get_lastID(entrylevelname,idProgramEntryReq);
    $('#tblqua tbody').append($newRow);
}


$get_lastID = function(entrylevelname,idProgramEntryReq,itemname,conditionname){

    var id = $('#tblqua tr:last-child td:first-child input').attr("name");
    var qualification = entrylevelname + itemname + conditionname ;
    if(idProgramEntryReq != '0'){
        $newRow = "<tr> \
                    <td width='2%'><input type='radio' name='progEntry' id='' value='"+idProgramEntryReq+"' /></td> \
                    <td align='left'>"+qualification+"</td> \
                   </tr>";
    }else{
        $newRow = "<tr> \
                   <td colspan='2'><label class='uk-alert-warning'><b>"+entrylevelname+"</b></label></td> \
                   </tr>";
    }

   return $newRow;


}


$(document).ready(function () {
//    var nationality = $('#nationality option:selected').val();
//    if (nationality != "" && nationality=='MY'){alert(nationality);
//        integerOnly();
//    }
});

function integerOnly(){
    // $("#username").keypress(function (e) {
    //     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    //         //display error message
    //        // $("#errmsg").html("Digits Only").show().fadeOut("slow");
    //         return false;
    //     }
    // });
}
</script>