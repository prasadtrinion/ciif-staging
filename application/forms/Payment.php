<?php

class App_Form_Payment extends App_Form_Base {
    
    public function init(){




        $link_id = new Zend_Form_Element_Select('link_id','');
        $link_id->setAttrib('class', 'form-control')
                    
                    ->setAttrib('get_required_files()', true)
                    ->addValidator('NotEmpty',true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $link_id->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

        $dataDb = new App_Model_Link();
        $appcategoryList4 = $dataDb->fetchAll();  
            // echo "<pre>";
            // print_r($appcategoryList);

            foreach ($appcategoryList4 as $key4 => $value4) {
               // echo $key;
                //echo $value['DefinitionDesc'].'<br>';
                $link_id->addMultiOption($value4['id'],$value4['name']);
            }
            


        //      $menu = new Zend_Form_Element_Select('menu','');
        // $menu->setAttrib('class', 'form-control')
                    
        //             ->setAttrib('get_required_files()', true)
        //             ->addValidator('NotEmpty',true)
        //     ->removeDecorator("DtDdWrapper")
        //     ->removeDecorator("Label");

        // $menu->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

        // $dataDb = new App_Model_Menu();
        // $appcategoryList4 = $dataDb->get_menu_name();  
         
        //     foreach ($appcategoryList4 as $key4 => $value4) {
        //        // echo $key;
        //         //echo $value['DefinitionDesc'].'<br>';
        //         $menu->addMultiOption($value4['id'],$value4['menu']);
        //     }
            




        //first_name
        $card_name = new Zend_Form_Element_Text('card_name');
        $card_name->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $card_no = new Zend_Form_Element_Text('card_no');
        $card_no->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $card_expiry_date = new Zend_Form_Element_Text('card_expiry_date');
        $card_expiry_date->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        $card_holder_name = new Zend_Form_Element_Text('card_holder_name');
        $card_holder_name->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $action = new Zend_Form_Element_Text('action');
        $action->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        $cvv = new Zend_Form_Element_Password('cvv');
        $cvv->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $description = new Zend_Form_Element_Text('description');
        $description->setAttrib('class', 'form-control')
           // ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //last_name
        $lastname = new Zend_Form_Element_Text('lastname');
        $lastname->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //email
        $email = new Zend_Form_Element_Text('email');
        $email->setAttrib('class', 'form-control')
                ->setAttrib('autocomplete', 'off')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        //$contactno
        $contactno = new Zend_Form_Element_Text('contactno');
        $contactno->setAttrib('class', 'form-control')
                         ->setAttrib('style','width:18.5em !important')

            ->setAttrib('maxlength',15)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        $role_id = new Zend_Form_Element_Select('role_id','');
        $role_id->setAttrib('class', 'form-control')
                    
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $role_id->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

           $definationDB =new Admin_Model_DbTable_Definition();

            $learningLists = $definationDB->getByCode('Role');

       // $country = $countryDb->fetchAll();  


            foreach ($learningLists as $key8 => $value8)
            {
               $role_id->addMultiOption($value8['idDefinition'],$value8['DefinitionDesc']);
            }


        //password
        $password = new Zend_Form_Element_Password('password');
        $password->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //password
        $amount = new Zend_Form_Element_Text('amount');
        $amount->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //timezone
        $timezone = new Zend_Form_Element_Select('timezone');
        $timezone->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $timezone->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('Select'));
        foreach ( Cms_Common::getTimezones() as $tz => $tz_name )
        {
            $timezone->addMultiOption($tz, $tz_name);
        }
        
      $radio_Industrialexp_type = $this->createElement('radio', 'Experience_type', array(
            'multiOptions'=>array(
                '1' => 'Yes',
                '0' => 'No',
            ),
        ));

        
       $radio_Industrialexp_type->setAttrib('id','Experience_type')
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->setAttrib('class', 'check-radio')
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper")
                         ->setSeparator('');



        $radio_Lectureshipexp_type = $this->createElement('radio', 'lectureshipRadio', array(
            'multiOptions'=>array(
                '1' => 'Yes',
                '0' => 'No',
            ),
        ));

             //first_name
        $firstname = new Zend_Form_Element_Text('firstname');
        $firstname->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

          //first_name
        $username = new Zend_Form_Element_Text('username');
        $username->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
        
       $radio_Lectureshipexp_type->setAttrib('id','lectureshipRadio')
                         ->removeDecorator("Label")
                         ->removeDecorator("HtmlTag")
                         ->removeDecorator("Errors")
                         ->removeDecorator("DtDdWrapper")
                         ->setSeparator('');                 
         //username
        $admin_notes = new Zend_Form_Element_Text('admin_notes');
        $admin_notes->setAttrib('class', 'form-control')
           // ->setAttrib('maxlength', '12')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator("HtmlTag");

     //banking_notes
        $banking_notes = new Zend_Form_Element_Text('banking_notes');
        $banking_notes->setAttrib('class', 'form-control')
           // ->setAttrib('maxlength', '12')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator("HtmlTag");



        $this->addElements(array(
                                $card_name,
                                $card_no,
                                $card_expiry_date,
                                $card_holder_name,
                                $role_id,
                                
                                $action,
                                $description,
                                 $admin_notes,
                                  $banking_notes,
                              
                                 $cvv,
                                $email,
                                $contactno,
                                $password,
                                $amount,
                                $timezone,
                                $contactno,                                                 
                             
                               
        ));
        
    }
}
?>
