<?php

class App_Form_SearchRegisterExam extends Zend_Form {
	
	protected $studentregistrationid;
	
 	public function setStudentregistrationid($studentregistrationid)
     {
         $this->studentregistrationid = $studentregistrationid;
         return $this;
     }
	
	public function init(){
		$this->setName('search');
		$this->setMethod('post');
		$this->setAttrib('id','search_form');
		$this->setMethod('post');
		
		$program_id	= new Zend_Form_Element_Select('program_id');
		$program_id ->removeDecorator("DtDdWrapper")
            		->removeDecorator("Label")
            		->removeDecorator("HtmlTag");	
            		            
		$this->addElement('select', 'program_id', array(
			              'required'    => false
        ));
        
        $registrationdb = new App_Model_Qualification_StudentRegistration();
	 	$program_list = $registrationdb->getDatabyStudentId(157622);
        
        $program_id->addMultiOption('', '-- Please Select --');
        
        /*if (count($program_list) > 0){
            foreach ($program_list as $p){
                $program_id->addMultiOption($p['IdProgram'], strtoupper($p['ProgramName']));
            }
        }*/
        
		$date_from	= new Zend_Form_Element_Text('date_from');
		$date_from	->setLabel('Exam Center')
					->setAttrib('class', 'datepicker')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
            		->removeDecorator("Label")
            		->removeDecorator("HtmlTag");
					
        $date_to = new Zend_Form_Element_Text('date_to');
        $date_to	->setAttrib('class', 'datepicker')
        		    ->setRequired(true)
		            ->removeDecorator("DtDdWrapper")
		            ->removeDecorator("Label")
		            ->removeDecorator("HtmlTag");

		            
		
		$this->addElements(array($program_id,$date_from,$date_to));
		
        		
	}
}
?>

<script>
$(function () {
    $("#date_from").datepicker({
        minDate: "dateToday",
        changeMonth: true,
        dateFormat: 'yy-mm-dd',
        onClose: function (selectedDate, instance) {
            if (selectedDate != '') { //added this to fix the issue
                $("#date_to").datepicker("option", "minDate", selectedDate);
                var date = $.datepicker.parseDate(instance.settings.dateFormat, selectedDate, instance.settings);
                date.setMonth(date.getMonth() + 1);
                console.log(selectedDate, date);
                $("#date_to").datepicker("option", "minDate", selectedDate);
                $("#date_to").datepicker("option", "maxDate", date);
            }
        }
    });
    $("#date_to").datepicker({
        minDate: "dateToday",
        changeMonth: true,
        dateFormat: 'yy-mm-dd',
        onClose: function (selectedDate) {
            $("#date_from").datepicker("option", "maxDate", selectedDate);
        }
    });
});
</script>