<?php

class App_Form_Bookmark extends App_Form_Base {
	
	public function init(){

        $translate = Zend_Registry::get('Zend_Translate');

        //name
        $this->addElement('text', 'bookmark_name', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        //excerpt
        $this->addElement('textarea', 'bookmark_description', array(
            'filters'    => array('StringTrim'),
            'rows'       => 2
        ));

        //name
        $this->addElement('text', 'bookmark_link', array(
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));
	}
}