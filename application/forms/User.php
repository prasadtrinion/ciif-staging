<?php

class App_Form_User extends App_Form_Base {
	
	public function init(){

        //username
        $username = new Zend_Form_Element_Text('username');
        $username->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator("HtmlTag");

        //first_name
        $firstname = new Zend_Form_Element_Text('firstname');
        $firstname->setAttrib('class', 'form-control')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
         //first_name
        $sec_mobile = new Zend_Form_Element_Text('sec_mobile');
        $sec_mobile->setAttrib('class', 'form-control')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

         //first_name
        $tel_office = new Zend_Form_Element_Text('tel_office');
        $tel_office->setAttrib('class', 'form-control')
            
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

         //first_name
        $firstname = new Zend_Form_Element_Text('firstname');
        $firstname->setAttrib('class', 'form-control')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //last_name
        $lastname = new Zend_Form_Element_Text('lastname');
        $lastname->setAttrib('class', 'form-control')
            //->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //email
        $email = new Zend_Form_Element_Text('email');
        $email->setAttrib('class', 'form-control')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //gender
        $gender = new Zend_Form_Element_Text('gender');
        $gender->setAttrib('class', 'form-control')
        ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //estatus
        $estatus = new Zend_Form_Element_Text('estatus');
        $estatus->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $city = new Zend_Form_Element_Text('city');
        $city->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $estatus = new Zend_Form_Element_Text('estatus');
        $estatus->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");


        //$contactno
        $contactno = new Zend_Form_Element_Text('contactno');
        $contactno->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

         //$contactno
        $company_others = new Zend_Form_Element_Text('company_others');
        $company_others->setAttrib('class', 'form-control')
            ->setAttrib('id', 'company_others')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //password
        $password = new Zend_Form_Element_Password('password');
        $password->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //password
        $newpassword = new Zend_Form_Element_Password('newpassword');
        $newpassword->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        //timezone
        $timezone = new Zend_Form_Element_Select('timezone');
        $timezone->setAttrib('class', 'form-control')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $timezone->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('Select'));
        foreach ( Cms_Common::getTimezones() as $tz => $tz_name )
        {
            $timezone->addMultiOption($tz, $tz_name);
        }



             $country = new Zend_Form_Element_Select('country','');
        $country->setAttrib('class', 'form-control')
                ->setAttrib('id', 'addr-country')
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $country->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

            $countryDb = new App_Model_Country();

        $countrylist = $countryDb->fetchAll();  


            foreach ($countrylist as $key8 => $value8)
            {
               $country->addMultiOption($value8['idCountry'],$value8['CountryName']);
            }

        $state = new Zend_Form_Element_Select('state','');
        $state->setAttrib('class', 'form-control')
                 ->setAttrib('id', 'addr-state')
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $state->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

            $stateDb = new App_Model_State();

        $getState = $stateDb->fetchAll();  


            foreach ($getState as $key9 => $value9)
            {
               $state->addMultiOption($value9['idState'],$value9['StateName']);
            }

            $cor_country = new Zend_Form_Element_Select('cor_country','');
        $cor_country->setAttrib('class', 'form-control')
                ->setAttrib('id', 'addr-cor-country')
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $cor_country->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

            $countryDb = new App_Model_Country();

        $countrylist = $countryDb->fetchAll();  


            foreach ($countrylist as $key8 => $value8)
            {
               $cor_country->addMultiOption($value8['idCountry'],$value8['CountryName']);
            }

        $cor_state = new Zend_Form_Element_Select('cor_state','');
        $cor_state->setAttrib('class', 'form-control')
                 ->setAttrib('id', 'addr-cor-state')
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $cor_state->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

            $stateDb = new App_Model_State();

        $getState = $stateDb->fetchAll();  


            foreach ($getState as $key9 => $value9)
            {
               $cor_state->addMultiOption($value9['idState'],$value9['StateName']);
            }

 
          $c_country = new Zend_Form_Element_Select('c_country','');
        $c_country->setAttrib('class', 'form-control')
                ->setAttrib('id', 'addr-c_country')
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $c_country->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

            $countryDb = new App_Model_Country();

        $countrylist = $countryDb->fetchAll();  


            foreach ($countrylist as $key8 => $value8)
            {
               $c_country->addMultiOption($value8['idCountry'],$value8['CountryName']);
            }

        $c_state = new Zend_Form_Element_Select('c_state','');
        $c_state->setAttrib('class', 'form-control')
                 ->setAttrib('id', 'addr-c_state')
                 ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $c_state->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

            $stateDb = new App_Model_State();

        $getState = $stateDb->fetchAll();  


            foreach ($getState as $key9 => $value9)
            {
               $c_state->addMultiOption($value9['idState'],$value9['StateName']);
            }


        $gender = new Zend_Form_Element_Select('gender','');
        $gender->setAttrib('class', 'form-control')
                 ->addValidator('NotEmpty',true) 
                  ->setAttrib('required', true)      
                 ->removeDecorator("DtDdWrapper")
                 ->removeDecorator("Label");

        $gender->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));


      //  App_Model_MemberIndustrialExperience
        $dataDb = new Admin_Model_DbTable_Definition();
        $appcategoryList3 = $dataDb->getByCode('Gender');  


            foreach ($appcategoryList3 as $key3 => $value3)
            {

                $gender->addMultiOption($value3['idDefinition'],$value3['DefinitionDesc']);

            }

        $industrialExp = new Zend_Form_Element_Select('industrial_exp','');
        $industrialExp->setAttrib('class', 'form-control')
                    
                    ->setAttrib('get_required_files()', true)
                    ->addValidator('NotEmpty',true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $industrialExp->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

        $dataDb = new App_Model_MemberIndustrialExperience();
        $appcategoryList4 = $dataDb->getIndustrialExp();  
            // echo "<pre>";
            // print_r($appcategoryList);

            foreach ($appcategoryList4 as $key4 => $value4) {
               // echo $key;
                //echo $value['DefinitionDesc'].'<br>';
                $industrialExp->addMultiOption($value4['idDefinition'],$value4['DefinitionCode']);
            }


             $qualification_level = new Zend_Form_Element_Select('qualification_level','');
        $qualification_level->setAttrib('class', 'form-control')
           
                 ->setAttrib('required', true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

        $qualification_level->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));

           $definationDB =new Admin_Model_DbTable_Definition();

            $learningLists = $definationDB->getByCode('LOQ');



            foreach ($learningLists as $key8 => $value8)
            {
               $qualification_level->addMultiOption($value8['idDefinition'],$value8['DefinitionDesc']);
            }

    $salutation = new Zend_Form_Element_Select('salutation','');
      $salutation->setAttrib('class', 'form-control')
                 ->addValidator('NotEmpty',true)       
                 ->removeDecorator("DtDdWrapper")
                 ->removeDecorator("Label");

        $salutation->addMultiOption('',Zend_Registry::get('Zend_Translate')->translate('--- Please Select ---'));


      //  App_Model_MemberIndustrialExperience
       $definationDB =new Admin_Model_DbTable_Definition();

            $learningLists = $definationDB->getByCode('Salutation');


            foreach ($learningLists as $key3 => $value3)
            {

                $salutation->addMultiOption($value3['idDefinition'],$value3['DefinitionDesc']);

            }

        $this->addElements(array(
                                $username,
                                $firstname,
                                $lastname,
                                $email,
                                $contactno,
                                $password,
                                $newpassword,
                                $timezone,
                                $contactno,
                                $salutation,
                                $gender,
                                $company_others,
                                $estatus,
                                $city,
                                $country,
                                $state,
                                $c_country,
                                $c_state,
                                $cor_country,
                                $tel_office,
                                $sec_mobile,
                                $cor_state,
                                $qualification_level,
                                $industrialExp
        ));
		
	}
}
?>