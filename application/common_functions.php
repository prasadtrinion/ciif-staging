<?php
/**
* collection of common functions
*
**/


//require_once('gitbranch.php');
/**
* get database connection
*
**/
function getDB() {
	$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');
	$params = array('host'=>$config->resources->db->params->host,
					'username' => $config->resources->db->params->username,
					'password'=>$config->resources->db->params->password,
					'dbname'=>$config->resources->db->params->dbname,
					'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8')
				);
	if(isset($config->resources->db->params->unix_socket)) {
		$params['unix_socket'] = $config->resources->db->params->unix_socket;
	}
	$db= Zend_Db::factory('Pdo_Mysql', $params);
	return($db);
}

function getDB2() {
	$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');
	$params = array('host'=>$config->resources->multidb->db2->host,
					'username' => $config->resources->multidb->db2->username,
					'password'=>$config->resources->multidb->db2->password,
					'dbname'=>$config->resources->multidb->db2->dbname,
					'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8')
				);
	if(isset($config->resources->multidb->db2->unix_socket)) {
		$params['unix_socket'] = $config->resources->multidb->db2->unix_socket;
	}
	$db= Zend_Db::factory('Pdo_Mysql', $params);
	return($db);
}

function pr($variale) {
    echo '<pre>';
    print_r($variale);
    echo '</pre>';
}

function clean($value='',$type='normal')
{
    $value = trim(strip_tags($value));

    if ( $type == 'alpha' )
    {
        $value = preg_replace('/[^\da-z]/i', '', $value);
    }

    return $value;
}

function clean_array($data=array(), $keys=array())
{
    if (is_array($data))
    {
        foreach( $data as $key => $val)
        {
            if ( !empty($keys) )
            {
                if ( is_string($val) && in_array($key, $keys) )
                {
                    $data[$key] = clean($val);
                }
                else
                {
                    $data[$key] = clean_array($val,$keys);
                }
            }
            else
            {

                if(is_string($val))
                {
                    $data[$key] = clean($val);
                }
                elseif(is_array($val))
                {
                    $data[$key] = clean_array($val,$keys);
                }
            }
        }

    }
    return $data;
}


function encrypt($text,$enc = true)
{
    $salt = $key = '1nc31f';
	if ( $enc == true )
    {
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key.$key), $text, MCRYPT_MODE_ECB, $iv);

        // encode data so that $_GET won't urldecode it and mess up some characters
        $data = base64_encode($crypttext);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return trim($data);
    }
    else
    {
        $base64 = $text;
        $data = str_replace(array('-','_'),array('+','/'),$base64); // manual de-hack url formatting
        $mod4 = strlen($data) % 4; // base64 length must be evenly divisible by 4
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        $crypttext = base64_decode($data);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key.$key), $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
}

/*
 * Moved a lot of the codes to Cms_Common
 */

/* External Libs */
require_once('External/JSPacker/Packer.php');
require_once('External/carbon/carbon.php');
