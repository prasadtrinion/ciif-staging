<?php

class App_Model_Courses extends Zend_Db_Table
{

    protected $_name = 'courses';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getCourse(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'courses'))
            ->joinLeft(array('c' => 'courses_category'), 'c.id=a.category_id', array('c.name as category_name'))
            ->joinLeft(array('d' => 'curriculum_courses'), 'd.course_id=a.id', array('d.curriculum_id'))
            ->order('d.curriculum_id desc');

        foreach ($where as $what => $value) {
            $what = $what == 'id = ?' ? 'a.id = ?' : $what;
            $select->where($what, $value);
        }
//echo $select;
        $result = $db->fetchRow($select);

        return $result;
    }

    public function getCourses($order = 'a.created_date DESC',$results=false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'courses'))
            ->joinLeft(array('c' => 'courses_category'), 'c.id=a.category_id', array('c.name as category_name'))
            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }

    public function getCoursesByExternalId($id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'courses'))
            ->where('a.external_id = ?', $id);
        $result = $db->fetchRow($select);
        return $result;
    }


    public function getCoursesTitle($id)

    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()

            ->from(array('a' => 'courses'))
            ->where('a.id = ?', $id);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getProgramGroup()
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('cur' => 'curriculum'))
            ->where('cur.parent_id =?', 0)
            ->order('cur.name asc');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProgramByGroup($idprogram)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('cur' => 'curriculum'))
            ->where('cur.parent_id =?', $idprogram);
        $result = $db->fetchAll($select);
        return $result;

    }

    public function getCoursesByProgram($idprogram)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'courses'), array('id', 'title', 'code', 'code_safe'))
            ->join(array('b' => 'curriculum_courses'), 'a.id = b.course_id', array(''))
            ->where('b.curriculum_id =?', $idprogram);
        $result = $db->fetchAll($select);

        return $result;

    }

    public function getAllCourseByDesc($idProgram, $groupDescription)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'user_group'), array('*'))
            ->where('a.group_desc =?', $groupDescription)
            ->where('a.by_program =?', $idProgram);
        $result = $db->fetchAll($select);

        return $result;

    }

}
