<?php

class App_Model_Options extends Zend_Db_Table
{

    protected $_name = 'options';
    protected $_primary = 'id';
    protected $db;
    protected $locale;

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
        $this->db = $this->getDefaultAdapter();
    }

    public function updateOption($name, $value)
    {
        $db = $this->db;

        $check = $this->fetchRow(array('opt_name = ?' => $name));

        if ( empty($check) )
        {
            $this->insert(array('opt_name' => $name));
        }

        $data = array(
                        'modified_by'   => Zend_Auth::getInstance()->getIdentity()->id,
                        'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'opt_value'     => $value
        );

        $this->update($data, array('opt_name = ?' => $name));
    }
}
