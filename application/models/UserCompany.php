<?php

class App_Model_UserCompany extends Zend_Db_Table
{

    protected $_name = 'tbl_user_company';
    protected $_primary = 'id_cmp';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
}
