<?php
class App_Model_PerformaInvoice extends Zend_Db_Table_Abstract
{

	protected $_name = 'tbl_performa_invoice';
	protected $_PRIMARY='id_invoice';


                //addmembershipdocdetail($membership_id,$doc_name,$doc_type)    


    public function get_invoice($id)
{
    $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            
            ->where('user_id ='.$id);
           
           $result = $db->fetchAll($select);  
        return $result;
}


  public function get_amount($id)
{

    $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            
            ->where('u.user_id ='.$id)
            ->order('u.id_invoice DESC');
           
           $result = $db->fetchRow($select);  
        return $result;
}


}
?>