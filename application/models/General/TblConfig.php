<?php 
class App_Model_General_TblConfig extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_config';
	protected $_primary = "idConfig";
	
	public function getData($id=0){
		$id = (int)$id;
		
		if($id!=0){
			$row = $this->fetchAll($this->_primary.' = ' .$id);
		}else{
			$row = $this->fetchAll();
		}
		
		if(!$row){
			return null;
		}else{
			return $row->toArray();	
		}
	}
	
	public function getConfig($idUniversity=1){
		
		 $db     = getDB2();
		
		$select = $db->select()
					 ->from(array('c'=>$this->_name))
				     ->where("idUniversity = ?",$idUniversity);
		$row = $db->fetchRow($select);
		return $row;		
	}
	
	
public function updateData($data,$id){		
		 $this->update($data,"idConfig =".(int)$id); 		 
	}
}

?>