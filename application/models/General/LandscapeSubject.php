<?php
class App_Model_General_LandscapeSubject extends Zend_Db_Table_Abstract
{
	protected $_name = 'tbl_landscapesubject';
	
	public function getLandscapeSubjectInfo($idLandscape,$idPathway,$idSubject){
               $db     = getDB2();

        $select = $db->select()
                   ->from(array('ls' => $this->_name))
                   ->where('ls.IdLandscape=?',$idLandscape)
                   ->where('ls.IdSubject=?',$idSubject);

        $row = $db->fetchRow($select);
        return $row;

    }
}
?>