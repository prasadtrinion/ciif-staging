<?php

class App_Model_Document extends Zend_Db_Table
{
    protected $_name = "tbl_graduation_documents";

    public function getDocument($type=0){

        $db = getDB2();
        $selectData = $db->select()
            ->from(array('tgd'=>$this->_name))
            ->join(array('gd'=>'general_documents'),'gd.gd_table_id=tgd.gd_id AND gd.gd_table_name="tbl_graduation_documents" ',array('gd_filepath','gd_ori_filename'))
            ->where('gd.gd_type = ?', $type);
        $row = $db->fetchAll($selectData);

        return $row;
    }
}
