<?php

class App_Model_General_StudentRegistrationSeqNo extends Zend_Db_Table_Abstract
{

    /**
     * The default table name
     */

    protected $_name = 'student_registrationid_seqno';
    protected $_primary = "srs_id";


    public function addData($data)
    {
        $db = getDB2();
        $db->insert($this->_name, $data);
        return $id = $db->lastInsertId($this->_name);
    }

    public function updateData($data, $id)
    {
        $db = getDB2();
        $db->update('student_registrationid_seqno',$data, $this->_primary . ' = ' . (int)$id);
    }

    public function deleteData($id)
    {
        $db = getDB2();
        $db->delete('student_registrationid_seqno',$this->_primary . ' =' . (int)$id);
    }

    public function getData($year)
    {

        $auth = Zend_Auth::getInstance();

        $db = getDB2();

        $select = $db->select()
            ->from(array('srs' => $this->_name))
            ->where("srs_year = ?", $year);
        $row = $db->fetchRow($select);

        if (!$row) {
            $data = array();
            $data['srs_year'] = $year;
            $data['srs_seqno'] = 1;
            $data['srs_updateby'] = $auth->getIdentity()->id;
            $data['srs_updatedt'] = date("Y-m-d H:i:s");
            $this->addData($data);

            $this->getData($year);
        }
        return $row;
    }

}