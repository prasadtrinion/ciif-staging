
<?php

class App_Model_SubMenu extends Zend_Db_Table
{

    protected $_name = 'tbl_sub_menu';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function get_role(){
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id');
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function get_role_menu(){
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id');
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function get_menu($id,$menu_id){
     
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id')
            ->where('u.role_id ='.$id)
            ->where('u.access = 1')
             ->where('u.menu_id ='.$menu_id);

           $result = $db->fetchAll($select);  
           
            
        
        return $result;

    }

    public function get_menu_access($id,$menu_id){
     
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id')
            ->where('u.role_id ='.$id)
             ->where('u.menu_id ='.$menu_id);

           $result = $db->fetchAll($select);  
            
        
        return $result;

    }
  
}


 