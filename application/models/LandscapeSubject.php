<?php


class App_Model_LandscapeSubject extends Zend_Db_Table_Abstract {
    /**
     * The default table name
     */
    protected $_name = 'tbl_landscapesubject';
    protected $_primary = 'IdLandscapeSub';


    public function getData($appl_id=0) {

        if($appl_id!=0){
            $db = getDB2();
            $select = $db->select()
                ->from(array('sr'=>$this->_name))
                ->where('sr.IdApplication = ?',$appl_id)
                ->where('sr.profileStatus = IN (92,248)'); //Active

            $stmt = $db->query($select);
            $row = $stmt->fetchRow();

        }else{
            $row = $this->fetchAll();
            $row=$row->toArray();
        }

        if(!$row){
            throw new Exception("There is No Information Found");
        }
        return $row;
    }

    public function getDataFromProfile($sp_id) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('sr'=>$this->_name))
            ->where('sr.sp_id = ?',$sp_id)
            ->where('sr.profileStatus IN (92,248,96)'); //Active

        $result = $db->fetchAll($select);
        return $result;
        //return $row;
    }

    public function getDataStudentRegistration($sp_id) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('sr'=>$this->_name))
            ->joinLeft(array("ts" => "tbl_definationms"),'ts.idDefinition=sr.TransactionStatus', array('StatusName'=>'DefinitionDesc'))
            ->where('sr.sp_id = ?',$sp_id)
        ->where('sr.IdRegistrationType IN (2,3)');
         //   ->where('sr.profileStatus IN (92,248,96)'
          //  //); //Active
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getLandscapeCourseByProgram($programId,$programMajor,$SubjectType=""){

        $db = getDB2();

        $select = $db->select()
            ->from(array("ls"=>$this->_name))
            ->join(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject',array('SubjectName','SubCode','CreditHours'))
            ->join(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array('SubjectType'=>'DefinitionDesc'))
            ->join(array("e"=>"tbl_programmajoring"),'e.idProgram=ls.IdProgram AND e.IDProgramMajoring=ls.IDProgramMajoring',array('IDProgramMajoring', 'MajoringDesc' => 'EnglishDescription'))
            ->where("ls.IdProgram = ?",$programId)
            ->where("ls.IDProgramMajoring = ?",$programMajor)
            ->order("s.SubCode");

        if ($SubjectType != "") {
            $select->where("ls.SubjectType = ? ", $SubjectType);
        }

        $larrResult = $db->fetchAll($select);
        return $larrResult;
    }

    public function getRequirementCourseByProgram($programId,$programMajor,$subjectType){

        $db = getDB2();

        $select = $db->select()
            ->from(array("pr"=>"tbl_programrequirement"))
            ->join(array("d"=>"tbl_definationms"),'d.idDefinition=pr.SubjectType',array('SubjectType'=>'DefinitionDesc'))
            ->join(array("e"=>"tbl_programmajoring"),'e.idProgram=pr.IdProgram AND e.IDProgramMajoring=pr.IDProgramMajoring',array('IDProgramMajoring', 'MajoringDesc' => 'EnglishDescription'))
            ->where("pr.IdProgram = ?",$programId)
            ->where("pr.IDProgramMajoring = ?",$programMajor)
            ->where("pr.SubjectType = ?",$subjectType);
        $larrResult = $db->fetchAll($select);
        return $larrResult;
    }


}

?>
