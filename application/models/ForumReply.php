<?php

class App_Model_ForumReply extends Zend_Db_Table
{

    protected $_name = 'forum_reply';
    protected $_primary = 'id';
    private static $tree = array();
    private static $html = '';

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

    public static function printNav($tree, $r = 0, $p = null )
    {
        foreach ($tree as $i => $t)
        {

            if ($t['parent_id'] == $p) {
                // reset $r
                $r = 0;
            }

            self::$html .= '<li '.(isset($t['_children'])?' class="uk-parent"':'').'><a href="#">'.$t['name'].'</a>';

            if (isset($t['_children'])) {
                self::$html .= '<ul class="uk-nav-sub">';
                self::printNav($t['_children'], ++$r, $t['parent_id']);
                self::$html .= '</ul>'."\n";
            }

            self::$html .= '</li>'."\n";
        }

        return self::$html;
    }

    public static function printTree($tree, $r = 0, $p = 0, $printdash = true) {


        foreach ($tree as $i => $t) {

            $dash = $printdash == true ? (($t['parent_id'] == 0) ? '' : str_repeat('-', $r) .' ') : '';

            if ($t['parent_id'] == $p) {
                // reset $r
                $r = 0;
            }

            self::$tree[] = array_merge( $t, array('name' => $dash.$t['name'], '_children' => '', 'depth' => $r) );


            if (isset($t['_children']) && !empty($t['_children'])) {
                self::printTree($t['_children'], ++$r, $t['parent_id'], $printdash);
            }
        }

        return self::$tree;
    }

    public static function buildTree(Array $data, $parent = 0) {
        $tree = array();
        foreach ($data as $d) {
            if ($d['parent_id'] == $parent) {
                $children = self::buildTree($data, $d['id']);
                // set a trivial key
                if (!empty($children)) {
                    $d['_children'] = $children;
                }
                $tree[] = $d;
            }
        }
        return $tree;
    }
	
	    public function getThreadsreply($order = 'a.created_date ASC',$results=false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'forum_reply'),array('a.id as reply_id','message','created_date','reply_content'))
			->joinLeft(array('d' => 'forum_thread'), 'd.id=a.thread_id',array('d.id as thread_id'))
			->joinLeft(array('b' => 'forum'), 'b.id=d.forum_id',array('b.id as forum_id'))			
            ->joinLeft(array('c' => 'courses'), 'c.id=b.course_id',array('c.id as course_id'))
			->joinLeft(array('e' => 'user'), 'a.created_by=e.id',array('e.id as user_id','username'))

            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }
		
		//print_r($results);exit;
        return $select;
    }

}
