
<?php

class App_Model_User extends Zend_Db_Table
{

    protected $_name = 'user';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getreguser($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))

            ->where('id ='.$id);
           
           $result = $db->fetchRow($select);  
        return $result;

    }

     public function getsalutation($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.salutation')
            ->where('id ='.$id);
           
           $result = $db->fetchRow($select);  
        return $result;

    }

     public function get_role_search($id){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('u' => 'user'))
      ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id')
    ->where('u.role_id = ?', $id);

       $result = $db->fetchAll($select);
    
    return $result;
    
    }

     public function get_admin_role(){
        $role = 'administrator';
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id')
              ->where('u.role like ?',"%".$role."%");
           
           $result = $db->fetchAll($select);  
        return $result;

    }


    public function get_admin_role_list(){
        $role = 'administrator';
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            
             ->from(array('a' => 'tbl_definationms'))
              ->where('a.idDefType = 1');
           
           
           $result = $db->fetchAll($select);  
        return $result;

    }


     public function get_role_access(){
        $role = 'administrator';
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id')
              ->where('u.role like ?',"%".$role."%");
           
           $result = $db->fetchAll($select);  
        return $result;

    }


     public function get_by_name($name){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
              ->where('u.firstname like ?',"%".$name."%");
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function getcurrency(){

                $id = 2;
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('tbl_currency_rate'))
            ->order('cr_id DESC')
            ->where('cr_cur_id ='.$id);
           
            $result = $db->fetchRow($select);  
        return $result;

    }

    public function get_approve_date($from,$to){
          
            $num = 1;
            $db = $this->getDefaultAdapter();

         $select = $db->select()
            ->from(array('u' => 'user'))
            ->join(array('m' => 'member'), 'm.idmember =u.id_member')
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus')
            ->joinLeft(array('b' => 'tbl_user_qualification'),'b.user_id = u.id')
            ->joinLeft(array('c' => 'tbl_user_company'),'c.user_id = u.id')
            
            ->where('u.approve_status1 = 1')
           ->where('u.approve_status = 0')
           ->group('u.id')
            ->where("u.amc_approve_date between CAST('".$from."' AS DATE) AND CAST('".$to."' AS DATE)");
          
         
        $result = $db->fetchAll($select); 
        return $result;

}
     public function getSubtype($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('tbl_member_subtype'))

            ->where('id_member ='.$id);
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function getCpdList($id)
    {
        $db = getDB();
        $select = $db->select()
            ->from(array("a" => "tbl_cpd_details"), array("a.*"))
             ->join(array("b" => "tbl_definationms"), 'a.cpd_learning_id = b.idDefinition',array("learning" => "b.DefinitionDesc"))
             ->join(array("c" => "tbl_definationms"), 'a.cpd_category_id = c.idDefinition',array("category" => "c.DefinitionDesc"))
             ->join(array("d" => "tbl_definationms"), 'a.cpd_activity_id = d.idDefinition',array("activity" => "d.DefinitionDesc"))
            ->join(array('u' => 'user'), 'u.id=a.spid')
            
            ->where('u.id ='.$id);;

        //echo $select;exit;

        $row = $db->fetchAll($select);

        return $row;
    }

    public function getMember($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->joinLeft(array('a' => 'member'),'a.idmember = u.id_member')
            ->joinLeft(array('b' => 'tbl_member_subtype'),'b.id_subtype = u.id_member_subtype')
            ->joinLeft(array('c' => 'tbl_definationms'),'c.idDefinition = u.industrial_exp',array("indus" => "c.DefinitionDesc"))

            // ->join(array('d' => 'tbl_member_experience_master'),'d.id_exp = u.id_experience')
              ->joinLeft(array('e' => 'tbl_countries'),'e.idCountry = u.country')
             ->joinLeft(array("f" => "tbl_definationms"), 'u.gender = f.idDefinition',array("gender" => "f.DefinitionDesc"))
              ->joinLeft(array("m" => "tbl_definationms"), 'u.prof_des = m.idDefinition',array("prof_des" => "m.DefinitionDesc"))
             // ->join(array("g" => "tbl_definationms"), 'u.id_qualification = g.idDefinition',array("Qualificaion" => "g.DefinitionDesc"))
            
            
               ->joinLeft(array('s' => 'tbl_state'),'s.idState = u.state')
            //    ->join(array('g' => 'tbl_city'),'g.idCity = u.city')
          
            ->where('u.id ='.$id);
           
           $result = $db->fetchRow($select); 
         
           
         
        return $result;

    }

     public function getExpiry($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'membership_registration'),'a.mr_membershipId = u.id')
            ->where('u.id ='.$id);
           
           $result = $db->fetchRow($select); 
         
           
         
        return $result;

    }


    public function get_userqualification($id){

         $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_user_qualification'),'a.user_id = u.id')
             ->where('u.id ='.$id);
           
           $result = $db->fetchAll($select); 

           
         
        return $result;
    }

    public function get_usercompany($id){

         $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_user_company'),'a.user_id = u.id')
            //->join(array('e' => 'tbl_company'),'e.id_cmp = a.u_company')
             ->where('u.id ='.$id);
           
           $result = $db->fetchAll($select); 

           
         
        return $result;
    }

    public function Details($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             // ->join(array('a' => 'member'),'a.idmember = u.id_member')
             // ->join(array('b' => 'tbl_member_subtype'),'b.id_subtype = u.id_member_subtype')
           ->join(array('c' => 'tbl_definationms'),'c.idDefinition = u.industrial_exp',array("indus" => "c.DefinitionDesc"))

            // ->join(array('d' => 'tbl_member_experience_master'),'d.id_exp = u.id_experience')
             ->join(array('e' => 'tbl_countries'),'e.idCountry = u.country')
             ->join(array("f" => "tbl_definationms"), 'u.gender = f.idDefinition',array("gender" => "f.DefinitionCode"))
            //  ->join(array("e" => "tbl_definationms"), 'u.prof_des = e.idDefinition',array("prof_des" => "e.DefinitionDesc"))
             // ->join(array("g" => "tbl_definationms"), 'u.id_qualification = g.idDefinition',array("Qualificaion" => "g.DefinitionDesc"))
            
            
              ->join(array('s' => 'tbl_state'),'s.idState = u.state')
            //    ->join(array('g' => 'tbl_city'),'g.idCity = u.city')
            ->where('u.id ='.$id);
           
           $result = $db->fetchRow($select); 
           
         
        return $result;

    }

     public function MemberDetails($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
           
            ->where('u.id ='.$id);
           
           $result = $db->fetchRow($select); 
           
         
        return $result;

    }

    public function getLecExp($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            
               ->join(array('f' => 'tbl_definationms'),'f.idDefinition = u.lecturship_exp',array('lec'=> 'DefinitionCode'))
            ->where('id ='.$id);
           
           $result = $db->fetchRow($select);  
        return $result;

    } 

    public function getIndExp($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            
               ->join(array('f' => 'tbl_definationms'),'f.idDefinition = u.industrial_exp',array('indus'=> 'DefinitionCode'))
            ->where('id ='.$id);
           
           $result = $db->fetchRow($select);  
        return $result;

    } 

     public function getMemberDetails($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => 'membership_registration'))
           
            ->where('mr_membershipId ='.$id);
           
           $result = $db->fetchRow($select);  
        return $result;

    }

    public function get_details($id){
        $db = $this->getDefaultAdapter();
        $select = $db->select()
                     ->from(array('u' => $this->_name))
                     ->where('u.id ='.$id);

        $result = $db->fetchRow($select);

    }
    public function get_app_status(){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus')
          
            ->where('approve_status1 = 0')
            ->where('approve_status = 0' )
             
             ->where('update_profile = 1' );
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function get_member_status(){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name));
           
           $result = $db->fetchAll($select);  
        return $result;

    }


    public function get_applicant(){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus')
            ->join(array('c' => 'member'),'c.idmember = u.id_member')
            ->join(array('b' => 'tbl_member_subtype'),'b.id_subtype = u.id_member_subtype')
            ->where('payment_status = 0')
            ->where('approve_status1 = 0')
            ->where('approve_status = 0' );
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function get_memberlist_search($username){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
             ->where('u.firstname like ?',"%".$username."%");
           
           $result = $db->fetchAll($select);  
        return $result;

    }
     public function get_memberlist(){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            //->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus');
            ->joinLeft(array('c' => 'member'),'c.idmember = u.id_member')
            ->joinLeft(array('b' => 'tbl_member_subtype'),'b.id_subtype = u.id_member_subtype');
            // ->where('payment_status = 1')
            // ->where('approve_status1 = 1')
            // ->where('approve_status = 1' );
           
           $result = $db->fetchAll($select); 
        
        return $result;

    }

    public function get_role(){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.role');
           
           $result = $db->fetchAll($select);  
        return $result;

    }


    public function get_userdoc($id){
        

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->joinLeft(array('a' => 'tbl_userdoc'),'a. membership_id = u.id')
              ->where('u.id ='.$id);
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function get_role_list()
    {
      
            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('m' => 'member'), 'm.idmember =u.id_member')
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus')
            ->joinLeft(array('b' => 'tbl_user_qualification'),'b.user_id = u.id')
            ->joinLeft(array('c' => 'tbl_user_company'),'c.user_id = u.id')
            ->group('u.id')
            ->where('approve_status1 = 1')
            ->where('payment_status = 1')
            ->where('approve_status = 0');
           
           $result = $db->fetchAll($select);  
           // echo "<pre>";
           // print_r($result);
           // die();
          
        return $result;

    }


     public function get_payed_list()
    {
            $num = 1;
            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('m' => 'member'), 'm.idmember =u.id_member')
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus')
            ->joinLeft(array('b' => 'tbl_user_qualification'),'b.user_id = u.id')
            ->joinLeft(array('c' => 'tbl_user_company'),'c.user_id = u.id')
            ->group('u.id')
            ->where('approve_status1 ='.$num)
            ->where('approve_status = 0')
               ->where('payment_status = 2');
           
           $result = $db->fetchAll($select);  
           // echo "<pre>";
           // print_r($result);
           // die();
          
        return $result;

    }

    public function get_barring_list()
    {
            $num = 1;
            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('m' => 'member'), 'm.idmember =u.id_member')
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus');
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function get_barred_member()
    {
            $num = 1;
            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('m' => 'member'), 'm.idmember =u.id_member')
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus')
            ->join(array('bm' => 'tbl_barred_member'),'bm.bmem_id =u.id')
            ->join(array('b' => 'tbl_barring'),'b.id_barring =bm.case_id');
           
           $result = $db->fetchAll($select);  
        return $result;

    }


     public function get_member_details(){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus');
           
           $result = $db->fetchAll($select);  
        return $result;

    }
    

    public function get_upgrade_status(){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus')
            ->where('upgrade_member =?',1648);
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    
    public function get_renewal_status(){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus')
            ->where('renew_member =?',0);
           
           $result = $db->fetchAll($select);  
        return $result;

    }

     public function get_program_status(){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus')
            ->where('idprogram =?',2);
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function get_exemption_status(){

            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = u.estatus')
            ->where('exemption_status =?',2);
           
           $result = $db->fetchAll($select);  
        return $result;
}

    


    public function get_email_status(){
            $date = date("Y-m-d H:i:s");
             $date2 = date("Y-m-d H:i:s", strtotime("60 day"));
            
            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => 'membership_registration'))
            ->join(array('b' => 'user'),'b.id = u.mr_membershipId')
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = b.estatus')
            ->where('u.mr_activation_date <= ?',$date)
            ->where('u.mr_expiry_date <= ?',$date2);
            
        $result = $db->fetchAll($select);  
        return $result;

    }

     public function getQualificaion($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_program'),'a.IdProgram = u.program')
            ->where('id ='.$id);
           
           $result = $db->fetchRow($select);  
            return $result;
 
    }

    public function getUser($id,$by='id')
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->joinLeft(array('a' => 'tbl_user_company'),'a.user_id = u.id')
            ->joinLeft(array('b' => 'tbl_user_qualification'),'b.user_id=u.id')
            
            ->where('u.id = ?', $id);

        return $db->fetchRow($select);
    }


    public function checkPassword($formData, $tab_password)
    {
        $oldpass = $formData['oldpass'];
        $password = $tab_password;
        if (password_verify($oldpass, $password)) 
        {
            return 1;
        } 
        else 
        {
            return 0;
        }
    }


    public function getUserInfo($id)
    {

        
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            
            ->where('u.id = ?', $id);

        return $db->fetchRow($select);
    }


     public function getFee($sub_type,$member_type)
    {

        
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'tbl_member_renew_fee_structure'))
            ->where('a.id_member = '.$sub_type)
            // ->where('a.id_degree = '.$degree_type)
            ->where('a.id_member_subtype = '.$member_type);

        $result = $db->fetchRow($select);
       
        return $result;
    }

    public function getUserfirstname($username)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'user'))
            ->where('a.firstname like ?',"%".$username."%")
            ->join(array('b' => 'tbl_definationms'),'b. idDefinition = a.estatus');

        return $db->fetchAll($select);
    }

    public function getUpgradefirstname($username)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'user'))
            ->where('a.firstname like ?',"%".$username."%")
            ->join(array('b' => 'tbl_definationms'),'b. idDefinition = a.estatus')
             ->where('upgrade_member =?',1648);

        return $db->fetchAll($select);
    }

public function getRenewfirstname($username)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'user'))
            ->where('a.firstname like ?',"%".$username."%")
            ->join(array('b' => 'tbl_definationms'),'b. idDefinition = a.estatus')
            
            ->where('renew_member =?',0);

        return $db->fetchAll($select);
    }

public function getprogramfirstname($username)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'user'))
            ->where('a.firstname like ?',"%".$username."%")
            ->join(array('b' => 'tbl_definationms'),'b. idDefinition = a.estatus')
            
            ->where('idprogram =?',0);

        return $db->fetchAll($select);
    }
    public function getname($username)
    {

       $date = date("Y-m-d H:i:s");
             $date2 = date("Y-m-d H:i:s", strtotime("-300 day"));
            
            $db = $this->getDefaultAdapter();

        $select = $db->select('*')
            ->from(array('u' => 'membership_registration'))
            ->join(array('b' => 'user'),'b.id = u.mr_membershipId')
            ->join(array('a' => 'tbl_definationms'),'a. idDefinition = b.estatus')
            ->where('u.mr_activation_date <= ?',$date)
            ->where('u.mr_expiry_date >= ?',$date2)
            ->where('a.firstname like ?',"%".$username."%");

        return $db->fetchAll($select);
    }

    public function getUserByUsername($username)
    {
        $db = $this->getDefaultAdapter();

        $select = $this->select()->where('username = ?', $username);

        return $db->fetchRow($select);
    }

    public function getRace($id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'external_data'),array ('a.name as r_name'))
            ->join(array('b' => 'external_datatype'),'b.id=a.type_id')
            ->where('a.value = ?', $id);

            return $db->fetchRow($select);
    }

    public function getReligion($id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'external_data'),array ('a.name as re_name'))
            ->join(array('b' => 'external_datatype'),'b.id=a.type_id')
            ->where('a.value = ?', $id);

            return $db->fetchRow($select);
    }

    public function getUserByEnrollment($course_id, $group_id=0)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('c' => 'enrol'), 'c.user_id = a.id', array('c.user_id', 'c.learningmode'))
            ->where('c.course_id = ?', $course_id)
            ->where('a.id NOT IN (SELECT user_id FROM user_group_data WHERE course_id = '.$course_id. ' AND group_id = '.$group_id.')')
            ->where('a.active = ?', 1)
            ->group('a.id')
            ->order('a.firstname');

        return $select;
    }

     public function exemption($user_id)
    
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update user set exemption_status='0' where id='$user_id'";
        $result = $db->query($sql);
        return $result;
          
       
    }

      public function exemptionpayment($user_id)
    
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update user set exemption_status='2' where id='$user_id'";
        $result = $db->query($sql);
        return $result;
          
       
    }

    public function approveUser($applicant_id)
    
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update user set estatus='1384' , approve_status1='1' where id='$applicant_id'";
        $result = $db->query($sql);
        return $result;
          
       
    }




     public function approveUserlist($applicant_id)
    
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update user set estatus='1384' , approve_status='1' where id='$applicant_id'";
        $result = $db->query($sql);
        return $result;
          
       
    }

    public function approveProgram($applicant_id)
    
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update user set idprogram='1' where id='$applicant_id'";
        $result = $db->query($sql);

        $sql1 = " tbl_member_register_program set payment='1657' where mr_id='$applicant_id'";
        $result1 = $db->query($sql);
        return $result;
          
       
    }

    public function renewUser($applicant_id)
    
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update user set renew_member='1' where id='$applicant_id'";
        $result = $db->query($sql);
        return $result;
          
       
    }
    public function rejectUser($applicant_id)
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update user set estatus='594' , approve_status1='2' where id='$applicant_id'";
        $result = $db->query($sql);
        return $sql;
          
       
    }

    public function rejectProgramUser($applicant_id)
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update user set idprogram='3' where id='$applicant_id'";
        
        $result = $db->query($sql);
        return $sql;
          
       
    }

    public function upgradeUser($applicant_id)
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update user set upgrade_member ='1649' , approve_status='1' where id='$applicant_id'";
        $result = $db->query($sql);
        return $sql;
          }

    public function updateexemption($applicant_id)
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update user set exemption_status='1' where id='$applicant_id'";
        $result = $db->query($sql);
        return $sql;
          }


    public function upgrade($applicant_id)
    { 

         $db = $this->getDefaultAdapter();  
        
        $sql = "update membership_application set ma_approve_date ='1384' , ma_application_status='1384' where id='$applicant_id'";
        $result = $db->query($sql);
        return $sql;
          
       
    }
}


 