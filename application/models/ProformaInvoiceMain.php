<?php

class App_Model_ProformaInvoiceMain extends Zend_Db_Table_Abstract
{
    /**
     * The default table name
     */
    protected $_name = 'tbl_performa_invoice_details';
    protected $_primary = "id";

    public function getData($id)
    {

        $db = getDB2();

        $selectData = $db->select()
            ->from(array('im' => $this->_name))
            ->where("im.id = ?", (int)$id);

        $row = $db->fetchRow($selectData);
        return $row;
    }

public function get_invoice($id)
{
    $db = $this->getDefaultAdapter();

        $select = $db->select()
            //->from(array('u' => $this->_name))
            ->from(array('pf' => 'tbl_performa_invoice'))
            ->where('pf.user_id ='.$id);
           
           $result = $db->fetchAll($select);  
        return $result;
}


public function get_performainvoice_app($fee,$id,$pid)
{
    $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('pf' => 'tbl_performa_invoice'), 'pf.id_invoice = u.performa_id', array('fee_type'=>'fee_type'))
            ->where('u.performa_id ='.$pid)
            ->where('pf.fee_type ='.$fee)
            ->where('u.user_id ='.$id);
           
           $result = $db->fetchAll($select);  
        return $result;
}

public function get_invoice_app($fee,$id,$pid)
{
    $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' =>'tbl_invoice_main_details'))
            ->join(array('pf' => 'tbl_invoice_main'), 'pf.id_invoice = u.invoice_id', array('fee_type'=>'fee_type'))
            ->where('u.invoice_id ='.$pid)
            ->where('pf.fee_type ='.$fee)
            ->where('u.user_id ='.$id);
           
           $result = $db->fetchAll($select);  
        return $result;
}

   public function get_invoice_print($fee,$id)
{
    $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name), array('program_amount'=>'amount'))
             ->joinLeft(array('pf' => 'tbl_performa_invoice'), 'pf.id_invoice = u.performa_id')
             ->joinLeft(array('pc' => 'tbl_program_category'), 'pc.id = u.id_program')
              ->where('pf.fee_type ='.$fee)
            ->where('u.user_id ='.$id);
           
           $result = $db->fetchAll($select);  
        return $result;
}

}