<?php

class App_Model_Guest extends Zend_Db_Table
{
    protected $_name = "tbl_graduation_guests";

    function deleteGuests($student_id) {

        $db = getDB2();
        $db->delete($this->_name,array(
            'IdStudentRegistration = ?' => $student_id
        ));

        return;
    }

    function getguests($student_id,$type=1) { //default set guest = 1

        $db = getDB2();

        $select = $db->select()
            ->from(array('g'=>'tbl_graduation_guests'))
            ->joinLeft(array('im'=>'invoice_main'),'im.id=g.invoice_id',array('bill_amount'))
            ->joinLeft(array('cur'=>'tbl_currency'),'im.currency_id=cur.cur_id',array('cur_symbol_prefix'))
            //->joinLeft(array('p'=>'pregraduate_list'),'p.idStudentRegistration =g.IdStudentRegistration ',array('c_id'))
            ->where('g.IdStudentRegistration = ?', $student_id);

        if(isset($type) && $type!=null){
            $select->where('type = ?',$type);
        }

        $checklists = $db->fetchRow($select);

        return($checklists);
    }

    function save($data) {
        $this->clear($data['IdStudentRegistration']);

        $this->insert($data);
        return(true);
    }



}
