<?php

class App_Model_UserQualification extends Zend_Db_Table
{

    protected $_name = 'tbl_user_qualification';
    protected $_primary = 'id_qualify';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
}
