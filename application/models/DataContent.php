<?php

class App_Model_DataContent extends Zend_Db_Table
{

    protected $_name = 'data_content';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
}
