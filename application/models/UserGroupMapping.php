<?php

class App_Model_UserGroupMapping extends Zend_Db_Table
{

    protected $_name = 'user_group_mapping';
    protected $_primary = 'map_id';

    public function init()
    {
        $this->db = $this->getDefaultAdapter();
        $this->_locale = Zend_Registry::get('Zend_Locale');
        $this->auth = Zend_Auth::getInstance();
    }

    public function deleteMappingByDataId($data_id, $course_id, $type='content')
    {
        $this->db->delete($this->_name, 'course_id = "' . $course_id . '" AND mapped_id = "' . $data_id . '" AND map_type = "' . $type . '"');
    }

    public function deleteMappingByType($type, $course_id)
    {
        $this->db->delete($this->_name, 'course_id = "' . $course_id . '" AND map_type = "' . $type . '"');
    }

    public function addMappingData($data) 
    {
        //$data['created_by'] = ($data['created_by']) ? $data['created_by'] : $this->auth->getIdentity()->id;
        //$data['created_date'] = ($data['created_date']) ? $data['created_date'] : new Zend_Db_Expr('UTC_TIMESTAMP()');

        $id = $this->db->insert($this->_name, $data);

        return $id;
    }

    public function checkContentPermission($data_id, $course_id, $type) // true = all, false = specific group only
    {
        $select = $this->db->select()
                    ->from(array('a' => $this->_name))
                    ->where('course_id = ?', $course_id)
                    ->where('mapped_id = ?', $data_id)
                    ->where('map_type = ?', $type);

        $result = $this->db->fetchAll($select);

        if (count($result) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getContentGroup($data_id, $course_id, $group_id, $type = 'content')
    {
        $select = $this->db->select()
                    ->from(array('a' => $this->_name))
                    ->joinLeft(array('c' => 'user_group'), 'a.group_id = c.group_id', 'c.group_name')
                    ->where('a.map_type = ?', $type)
                    ->where('a.group_id = ?', $group_id)
                    ->where('a.course_id = ?', $course_id)
                    ->where('a.mapped_id = ?', $data_id);

        $groups = $this->db->fetchRow($select);

        return $groups;
    }

    public function getContentGrouping($course_id)
    {
        $db = $this->getDefaultAdapter();
        $groupdata = $db->select()
                ->from(array('a' => 'user_group'))
                ->where('a.course_id = ?', $course_id)
                ->group('a.group_id');

        $groups = $db->fetchAll($groupdata);

        return $groups;
    }
}
