<?php
class App_Model_FeeItem extends Zend_Db_Table_Abstract {
    /**
     * The default table name
     */
    protected $_name = 'fee_item';
    protected $_primary = "fi_id";

    public function getData($id=0){
        $db     = getDB2();
        $selectData = $db->select()
            ->from(array('fi'=>$this->_name))
            ->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id = fi.fi_fc_id')
            ->joinLeft(array('ac'=>'tbl_account_code'),'ac.ac_id = fi.fi_ac_id')
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
            ->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'));

        if($id!=0){
            $selectData->where("fi.fi_id = '".$id."'");

            $row = $db->fetchRow($selectData);

        }else{

            $row = $db->fetchAll($selectData);
        }

        if(!$row){
            return null;
        }else{
            return $row;
        }

    }

    public function getActiveFeeItem(){
        $db     = getDB2();
        $selectData = $db->select()
            ->from(array('fi'=>$this->_name))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
            ->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'))
            ->where('fi.fi_active = 1')
            ->order('fi.fi_id');

        $row = $db->fetchAll($selectData);

        if(!$row){
            return null;
        }else{
            return $row;
        }
    }

    public function getActiveNonInvoiceFeeItem(){
        $db     = getDB2();
        $selectData = $db->select()
            ->from(array('fi'=>$this->_name))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
            ->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'))
            ->where('fi.fi_active = 1')
            ->where('fi.fi_non_invoice = 1')
            ->order('fi.fi_id');

        $row = $db->fetchAll($selectData);

        if(!$row){
            return null;
        }else{
            return $row;
        }
    }

    public function getPaginateData($search=null){
        $db     = getDB2();

        if($search){
            $selectData = $db->select()
                ->from(array('fi'=>$this->_name))
                ->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id = fi.fi_fc_id')
                ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
                ->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'))
                ->where("fi.fi_name LIKE '%".$search['fi_name']."%'")
                ->where("fi.fi_name_bahasa LIKE '%".$search['fi_name_bahasa']."%'")
                ->where("fi.fi_name_short LIKE '%".$search['fi_name_short']."%'")
                ->where("fi.fi_code LIKE '%".$search['fi_code']."%'")
                ->where("fi.fi_amount_calculation_type LIKE '%".$search['fi_amount_calculation_type']."%'")
                ->where("fi.fi_frequency_mode LIKE '%".$search['fi_frequency_mode']."%'")
                ->where("fi.fi_active = 1")
                ->order("fi.fi_name asc");
        }else{
            $selectData = $db->select()
                ->from(array('fi'=>$this->_name))
                ->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id = fi.fi_fc_id')
                ->joinLeft(array('ac'=>'tbl_account_code'),'ac.ac_id = fi.fi_ac_id')
                ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
                ->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'))
                ->where("fi.fi_active = 1")
                ->order("fi.fi_name asc");

        }

        return $selectData;
    }


    public function addData($postData){

        $data = array(
            'fi_name' => $postData['fi_name'],
            'fi_name_bahasa' => $postData['fi_name_bahasa'],
            'fi_code' => $postData['fi_code'],
            'fi_amount_calculation_type' => $postData['fi_amount_calculation_type'],
            'fi_frequency_mode' => $postData['fi_frequency_mode'],
            'fi_fc_id' => $postData['fi_fc_id'],
            'fi_ac_id' => $postData['fi_ac_id'],
            'fi_refundable' => $postData['fi_refundable'],
            'fi_non_invoice' => $postData['fi_non_invoice'],
            'fi_active' => $postData['fi_active'],
            'fi_gst' => $postData['fi_gst'],
            'fi_gst_tax' => $postData['fi_gst_tax'],
            'fi_effective_date' => $postData['fi_effective_date'],
        );

        $id = parent::insert($data);
        return $id;
    }


    public function updateData($postData,$id){

        $data = array(
            'fi_name' => $postData['fi_name'],
            'fi_name_bahasa' => $postData['fi_name_bahasa'],
            'fi_code' => $postData['fi_code'],
            'fi_amount_calculation_type' => $postData['fi_amount_calculation_type'],
            'fi_frequency_mode' => $postData['fi_frequency_mode'],
            'fi_fc_id' => $postData['fi_fc_id'],
            'fi_ac_id' => $postData['fi_ac_id'],
            'fi_refundable' => $postData['fi_refundable'],
            'fi_non_invoice' => $postData['fi_non_invoice'],
            'fi_active' => $postData['fi_active'],
            'fi_gst' => $postData['fi_gst'],
            'fi_gst_tax' => $postData['fi_gst_tax'],
            'fi_effective_date' => $postData['fi_effective_date'],
        );

        $this->update($data, "fi_id = '".$id."'");
    }

    public function deleteData($id=null){
        if($id!=null){
            $data = array(
                'fi_active' => 0
            );

            $this->update($data, "fi_id = '".$id."'");
        }
    }

    public static function getListByCodes($code) {
        $db     = getDB2();
        $where = $db->select()
            ->from(array('fi'=> 'fee_item'))
            ->joinLeft(array('fc'=>'tbl_fee_category'),'fc.fc_id = fi.fi_fc_id')
            ->joinLeft(array('ac'=>'tbl_account_code'),'ac.ac_id = fi.fi_ac_id')
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
            ->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'))
            ->where('fc.fc_code LIKE ?', $code . "%")
        ;

        $rows = $db->fetchAll($where);

        return($rows);
    }

    public static function getFeeItemIdByCode($code) {
        $db     = getDB2();
        $where = $db->select()
            ->from(array('fi'=> 'fee_item'))
            ->where('fi.fi_code = ?', $code)
        ;
        $fee_item = $db->fetchRow($where);
        if(!empty($fee_item)) {
            return($fee_item['fi_id']);
        } else {
            return(false);
        }
    }

    public static function getListByProgram($programid) {
        $db     = getDB2();
        $select = $db->select()
            ->from(array('a'=> 'invoice_detail'))
            ->joinLeft(array('b'=>'invoice_main'),'a.invoice_main_id = b.id',array('IdStudentRegistration'))
            ->joinLeft(array('c'=>'fee_item'),'c.fi_id = a.fi_id')
            ->joinLeft(array('d'=>'tbl_studentregistration'),'d.IdStudentRegistration = b.IdStudentRegistration AND (b.IdStudentRegistration IS NOT NULL OR b.IdStudentRegistration != 0)',array('IdProgram'))
            ->where('d.IdProgram = ?', $programid)
            ->where('a.fi_id != 0')
//					->where("b.not_include = 0")
            ->where("b.status IN ('A','W')")
            ->group('a.fi_id')
            ->order('c.fi_name')
        ;

        $rows = $db->fetchAll($select);

        return($rows);
    }

    public function addHistory($postData,$id){

        $db     = getDB2();

        $auth = Zend_Auth::getInstance();
        $data = array(
            'fi_id' => $id,
            'fi_name' => $postData['fi_name'],
            'fi_name_bahasa' => $postData['fi_name_bahasa'],
            'fi_code' => $postData['fi_code'],
            'fi_amount_calculation_type' => $postData['fi_amount_calculation_type'],
            'fi_frequency_mode' => $postData['fi_frequency_mode'],
            'fi_fc_id' => $postData['fi_fc_id'],
            'fi_ac_id' => $postData['fi_ac_id'],
            'fi_refundable' => $postData['fi_refundable'],
            'fi_non_invoice' => $postData['fi_non_invoice'],
            'fi_active' => $postData['fi_active'],
            'fi_gst' => $postData['fi_gst'],
            'fi_gst_tax' => $postData['fi_gst_tax'],
            'fi_effective_date' => $postData['fi_effective_date'],
            'created_by' =>  $auth->getIdentity()->iduser,
            'created_date' => date('Y-m-d H:i:s'),
        );

        $db->insert('fee_item_history',$data);
    }


    public function getDataConvoFee($convoId,$feeId){
        $db     = getDB2();
        $selectData = $db->select()
            ->from(array('fe'=>'fee_item_convo'))
            ->join(array('fi'=>'fee_item'),'fi.fi_id = fe.fv_fi_id')
            ->join(array('cv'=>'convocation'),'cv.c_id = fe.fv_convo')
            ->join(array('c'=>'tbl_currency'),'c.cur_id = fe.fv_currency_id')
            ->where("fe.fv_convo =?",$convoId)
            ->where("fe.fv_fi_id =?",$feeId)
        ;

        $row = $db->fetchRow($selectData);
        if(!$row){
            return null;
        }else{
            return $row;
        }

    }
}