
<?php

class App_Model_Menu extends Zend_Db_Table
{

    protected $_name = 'tbl_menu';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function get_role(){
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id');
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function get_role_menu(){
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id')
             ->group('u.role_id');
           
           $result = $db->fetchAll($select);  
        return $result;

    }


      public function get_menu_name(){
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id')
             ->group('u.menu');
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function get_menu($id){
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id')
              // ->joinLeft(array('b' => 'user'),'b. role_id = u.role_id')
            ->where("u.role_id =".$id)
            ->where('u.access = 1');
            
           $result = $db->fetchAll($select);  
          
        
        return $result;

    }


    public function get_access_menu($id){
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id')
             //  ->joinLeft(array('b' => 'user'),'b. role_id = u.role_id')
            ->where('u.role_id ='.$id);

           $result = $db->fetchAll($select); 
           // echo "<pre>";
           // print_r($result);
           // die(); 
          
        
        return $result;

    }
  
}


 