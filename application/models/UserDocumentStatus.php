<?php

class App_Model_UserDocumentStatus extends Zend_Db_Table
{

    protected $_name = 'tbl_userdoc_status';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function addData($data)
    {
        $db = $this->getDefaultAdapter();

        $result = $db->insert($this->_name, $data);

        return $result;
    }

    public function getUserDoc($id)
    {
        $doc = 'Acad';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = ?', $id)
            ->where('a.doc_name like "%" ? "%"',$doc);

        $result = $db->fetchAll($select);

        // echo "<pre>";
        // print_r($result);
        // die();

        return $result;
    }

    public function getProgramDoc($id)
    {
        $doc = 'ProgrammePayme';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = ?', $id)
            ->where('a.doc_name like "%" ? "%"',$doc);

        $result = $db->fetchAll($select);

        // echo "<pre>";
        // print_r($result);
        // die();

        return $result;
    }

    public function getRenewDoc($id)
    {
        $doc = 'RenewalDoc';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = ?', $id)
            ->where('a.doc_name like "%" ? "%"',$doc);

        $result = $db->fetchAll($select);

        // echo "<pre>";
        // print_r($result);
        // die();

        return $result;
    }

    public function getExemptionDoc($id)
    {
        $doc = 'Exemption';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = ?', $id)
            ->where('a.doc_name like "%" ? "%"',$doc);

        $result = $db->fetchAll($select);

        // echo "<pre>";
        // print_r($result);
        // die();

        return $result;
    }

    public function getExpDoc($id)
    {
        $doc = 'Work';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = ?', $id)
            ->where('a.doc_name like "%" ? "%"',$doc);

        $result = $db->fetchAll($select);
        return $result;
    }

     public function get_doc_status($id)
    {   
        $doc = '1668';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = '. $id)
            ->where('a.status = '.$doc);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getProDoc($id)
    {
        $doc = 'Prog';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = ?', $id)
            ->where('a.doc_name like "%" ? "%"',$doc);

        $result = $db->fetchAll($select);
        return $result;
    }


    public function getPreDoc($id)
    {
        $doc = 'Previ';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = ?', $id)
            ->where('a.doc_name like "%" ? "%"',$doc);

        $result = $db->fetchAll($select);
        return $result;
    }

   public function getNominDoc($id)
    {
        $doc = 'Nomin';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = ?', $id)
            ->where('a.doc_name like "%" ? "%"',$doc);

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getOtherDoc($id)
    {
        $doc = 'Other';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = ?', $id)
            ->where('a.doc_name like "%" ? "%"',$doc);

        $result = $db->fetchAll($select);
        return $result;
    } 


    public function getUpgradeDoc($id)
    {
        $doc = 'Upgrade';
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.membership_id = ?', $id)
            ->where('a.doc_name like "%" ? "%"',$doc);

        $result = $db->fetchAll($select);
        return $result;
    } 

    public function getDoc($doc_id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.doc_id = ?', $doc_id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function deleteDoc($doc_id)
    {
        $db = $this->getDefaultAdapter();

        $delete = $db->delete($this->_name, 'doc_id = ' . $doc_id);

        return $delete;
    }
}

?>