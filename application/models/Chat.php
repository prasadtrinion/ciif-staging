<?php

class App_Model_Chat extends Zend_Db_Table
{

    protected $_name = 'chat';
    
    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getChat($user_id, $type='', $id='')
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()
                  ->from(array("a" => $this->_name), array('a.user_id', 'a.message', 'a.date_time')) 
                  ->join(array('u'=>'user'),'u.id = a.user_id',array('u.firstname','u.lastname'))
                  ->order('a.chat_id ASC')
                  ->limit('30');

        if ($type == 'course')
        {
            $sql->where('type = ?', 'course')
                ->where('course_id = ?', $id);
        }
        else if ($type == 'personal')
        {
            $sql->where('type = ?', 'personal')
                ->where('user_id = ?', $id);
        }
        else if ($type == 'global' || $type == '')
        {
            $sql->where('type = ?', 'global');
        }
        
        $results = $db->fetchAll($sql);

        return $results;
    }

    public function insertChat($data)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $id = $db->insert($this->_name, $data);
        return $id;
    }

    public function countChat($type='global')
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()
                ->from(array('a' => $this->_name), array(new Zend_Db_Expr('COUNT(message) AS total_message')))
                ->where('type = ?', $type);

        $results = $db->fetchRow($sql);

        return $results;
    }
}
