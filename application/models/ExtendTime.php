<?php

class App_Model_ExtendTime extends Zend_Db_Table
{

    protected $_name = 'tbl_studentextendtime';
    protected $_primary = 'IdExtend';

    public function getData($id, $fetchAll = true, $join = false){
        $db = getDB();
        $selectData = $db->select()
            ->from(array('a'=>$this->_name))
            ->where("a.id = ?", (int)$id);

        if($join) {
            $selectData->join(array('b'=>'tbl_currency'), 'b.cur_id = a.currency_id', array('cur_symbol_prefix', 'cur_code'));
        }

        if($fetchAll) {
            $row = $db->fetchAll($selectData);
            return $row;
        }

        $row = $db->fetchRow($selectData);
        return $row;

    }

    public function getDataStudReg($IdStudentRegistration){
        $db = getDB();
        $selectData = $db->select()
            ->from(array('a'=>$this->_name))
            ->where("a.IdStudentRegistration = ?", (int)$IdStudentRegistration);

        $row = $db->fetchAll($selectData);
        return $row;
    }
}
