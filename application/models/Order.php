<?php

class App_Model_Order extends Zend_Db_Table
{

    protected $_name = 'order';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getOrders($where='', $order='id DESC')
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name));
        $select->joinLeft(array('c'=>'curriculum'),'(c.id=a.item_id OR c.id = a.curriculum_id)AND (a.regtype="curriculum" OR a.regtype="exam")', array('c.name as curriculum_name','c.external_id as IdProgram'));
        $select->joinLeft(array('b'=>'courses'),'b.id=a.item_id AND a.regtype="course"', array('b.title as course_name'));

        if ( !empty($where) ) {
            foreach ($where as $what => $value) {
                $select->where($what, $value);
            }
        }

        $select->group('a.id');
        $select->order($order);

        $result = $db->fetchAll( $select );


        return $result;

    }

    public function getOrder(array $where, $join=0)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name));

        if ( $join )
        {
            //$select->joinLeft(array('c'=>'curriculum'),'(c.id=a.item_id AND a.regtype="curriculum") OR (c.id=a.curriculum_id AND a.regtype="exam") ', array('c.name as curriculum_name','c.external_id as IdProgram'));
            $select->joinLeft(array('c'=>'curriculum'),'(c.id=a.item_id AND a.regtype="curriculum") OR (c.id=a.curriculum_id AND a.regtype="exam") OR (c.id=a.curriculum_id AND a.regtype="course") ', array('c.name as curriculum_name','c.external_id as IdProgram'));
            $select->joinLeft(array('b'=>'courses'),'b.id=a.item_id AND (a.regtype="course" OR a.regtype="exemption")', array('b.title as course_name'));
            $select->group('a.id');
        }

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }

        $result = $db->fetchRow( $select );

        return $result;
    }

    public function getOrderData($user_id)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name));
        $select->join(array('e'=>'enrol'),'e.order_id = a.id', array('*'));
//        $select->joinLeft(array('b'=>'courses'),'b.id=a.item_id AND a.regtype="course"', array('b.title as course_name'));

        if ( !empty($user_id) ) {
            $select->where('a.user_id=?',$user_id);
        }

        $select->group('a.id');

        $result = $db->fetchAll( $select );

        return $result;

    }

    public function getOrderExam($user_id, $curriculum_id = 0)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name))
            ->where("a.regtype = 'exam'")
            ->where("a.external_id = ?", $user_id);

        if ($curriculum_id)  {
            $select->where('a.curriculum_id = ?',$curriculum_id);
        }

        $select->group('a.id');
        $select->order('a.id desc');

        $result = $db->fetchRow( $select );

        return $result;

    }

    public function checkOrderID($invoice_id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name))
            ->where('a.status="PENDING"')
            ->where('a.invoice_id =?', $invoice_id);

        $result = $db->fetchRow( $select );

        return $result;
    }

    public function getInvoiceID ($id)
    {
        $db = getDB2();
        $select = $db->select()
            ->from(array('i'=>'invoice_main'))
            ->where('i.id = ?' , $id);

        $result = $db->fetchRow($select);

        return($result);
    }

    public function getInvoiceInfo ($idstudregistration=0)
    {
        $db = getDB2();
        $select = $db->select()
            ->from(array('i'=>'invoice_main'))
            ->where('i.IdStudentRegistration = ?' , $idstudregistration);

        $result = $db->fetchRow($select);

        return($result);
    }

    public function addNewOrder($table, $data){

        $db = $this->getDefaultAdapter();

        $db->insert($this->_name,$data);
        $paymentId = $db->lastInsertId();
        return $paymentId;
    }

    public function addInvoiceNew($table, $data){
        $db = getDB2();
        $id = $db->insert($table, $data);
        return $id;
    }
}
