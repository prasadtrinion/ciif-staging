<?php

class App_Model_UserDoc extends Zend_Db_Table
{

    protected $_name = 'user_doc';
    protected $_primary = 'doc_id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function addData($data)
    {
        $db = $this->getDefaultAdapter();

        $result = $db->insert($this->_name, $data);

        return $result;
    }

    

    public function getDoc($doc_id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.doc_id = ?', $doc_id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function deleteDoc($doc_id)
    {
        $db = $this->getDefaultAdapter();

        $delete = $db->delete($this->_name, 'doc_id = ' . $doc_id);

        return $delete;
    }
}

?>