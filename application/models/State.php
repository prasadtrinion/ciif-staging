<?php
class App_Model_State extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_state';
    protected $_primary = "idState";

    public function getState($name){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
          
            ->where('StateName like ?',"%".$name."%");
           
           $result = $db->fetchRow($select); 
          
            return $result;
 
    }
   

   
}
?>