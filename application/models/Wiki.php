<?php

class App_Model_Wiki extends Zend_Db_Table
{

    protected $_name = 'wiki';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getWiki(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'wiki'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'));

        foreach ($where as $what => $value) {
            $what = $what == 'id = ?' ? 'a.id = ?' : $what;
            $select->where($what, $value);
        }

        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getWikis($order = 'a.created_date ASC',$results=false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'wiki'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'))
            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }

}
