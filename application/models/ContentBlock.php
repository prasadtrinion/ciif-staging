<?php

class App_Model_ContentBlock extends Zend_Db_Table
{

    protected $_name = 'content_block';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getBlocks(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }

        $result = $db->fetchAll( $select );

        return $result;
    }
}
