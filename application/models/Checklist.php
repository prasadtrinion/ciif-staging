<?php
class App_Model_Checklist extends Zend_Db_Table
{
    protected $_name = "pregraduate_student_checklist";

    private $db;

    public function init() {
        $this->db = Zend_Db_Table::getDefaultAdapter();
    }

    function get_checklist($convo_id,$program_id)
    {
        $db = getDB2();
        $select = $this->db->select()
            ->from(array('cc'=>'convocation_checklist'),array())
            ->join(array('cpc'=>'convocation_program_checklist'),'cpc.cc_id=cc.cc_id',array())
            ->join(array('gc'=>'tbl_graduation_checklist'),'gc.id=cpc.idChecklist')
            ->where('cc.c_id = ?',$convo_id)
            ->where('cc.idProgram = ?',$program_id);

        $select = $this->db->select()
                ->from(array('a'=>'tbl_graduation_checklist'));

        //$results = $this->db->fetchAll($select);
        $results = $db->fetchAll($select);

        return $results;

    }

    function isStudentChecklist($student_id,$id_checklist)
    {
        $db = getDB2();
        $select = $db->select()
            ->from(array('psc'=>$this->_name))
            ->joinLeft(array("gc"=>"tbl_graduation_checklist"),'gc.id=psc.idChecklist',array('ChecklistName'=>'name'))
            ->where('psc.IdStudentRegistration = ?',  $student_id)
            ->where('psc.idChecklist = ?',$id_checklist);

        $results = $db->fetchRow($select);

        return $results;

    }

    function studentChecklist($student_id)
    {
        $db = getDB2();
        $select = $this->db->select()
            ->from(array('psc'=>$this->_name))
            ->joinLeft(array("gc"=>"tbl_graduation_checklist"),'gc.id=psc.idChecklist',array('ChecklistName'=>'name'))
            ->where('psc.IdStudentRegistration = ?',  $student_id);

        $results = $db->fetchAll($select);

        return $results;

    }

    public function deleteData($IdStudentRegistration){
        $this->db->delete('pregraduate_student_checklist','IdStudentRegistration = '.(int)$IdStudentRegistration);
    }


}
