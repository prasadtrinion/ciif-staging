<?php

class App_Model_Bookmark extends Zend_Db_Table
{

    protected $_name = 'bookmark';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function saveBookmark($data) {
        $db = $this->getDefaultAdapter();

        $bookmark_id = $db->insert($this->_name, $data);
    }

    public function editBookmark($data, $id) {
        $db = $this->getDefaultAdapter();

        $bookmark_id = $db->update($this->_name, $data, 'id='.(int)$id);
    }

    public function deleteBookmark($data_id, $user_id) {
        $db = $this->getDefaultAdapter();
        
        $db->delete($this->_name, array(
                'id = ?' => $data_id,
                'user_id = ?' => $user_id
            ));

        return;
    }

    public function getBookmark($id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.id = ?', $id)
            ->order('a.bookmark_name');

        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getBookmarkByUser($user_id, $course_id, $results = true)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.user_id = ?', $user_id)
            ->where('course_id = ?', $course_id)
            ->order('a.bookmark_name');

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }

    public function getBookmarkByDataId($data_id, $user_id) 
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from($this->_name)
            ->where('user_id = ?', $user_id)
            ->where('data_id = ?', $data_id);

        return $db->fetchRow($select);
    }

}
