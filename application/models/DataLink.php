<?php

class App_Model_DataLink extends Zend_Db_Table
{

    protected $_name = 'data_link';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
}
