<?php
/*
 * model to track revision of data around the system
 *
 */
class App_Model_SystemLog extends Zend_Db_Table
{

    protected $_name = 'system_log';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
}
