<?php

class App_Model_ContentBlockData extends Zend_Db_Table
{

    protected $_name = 'content_block_data';
    protected $_primary = 'data_id';
    protected $uploadDir;

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
        $this->uploadDir = Cms_System_Links::dataFile();
    }

    public function deleteContent( array $data)
    {
        try {
            switch ($data['data_type']) {
                case "content":
				case "wiki":
                case "link":

                    break;

                case "video":

                    $filesDb = new App_Model_DataVideo();

                    $files = $filesDb->getDataAll(array('data_id = ?' => $data['data_id'], 'video_type' => 'upload'));
                    foreach ($files as $video) {
                        @unlink(DOCUMENT_PATH . $video['video_url']);
                    }

                    break;

                case "scorm":

                    $scormDb = new App_Model_DataScorm();
                    $scorm = $scormDb->getData(array('data_id = ?' => $data['data_id']));
                    
                    if (file_exists(DOCUMENT_PATH . $scorm['zipfile']) && $scorm['zipfile'] != '') {

                        if ($scorm['url'] != '' && $scorm['url'] != null)
                        {
                            Cms_Common::removeDir(DOCUMENT_PATH . $scorm['url']);
                        }
                        
                        unlink(DOCUMENT_PATH . $scorm['zipfile']);
                    }

                    break;

                case "support":
				case "assignment":

                    $filesDb = new App_Model_DataFiles();
                    $files   = $filesDb->getDataAll(array('parent_id = ?' => $data['data_id'], 'file_type = ?' => 'blockfile'));

                    foreach ($files as $file) {
                        $file_path = Cms_System_Links::dataFile($file['file_url']);

                        if (file_exists($file_path))
                        {
                            Cms_System_Links::dataFile($file['file_url']);
                        }
                    }

                    break;
					
            }
        }
        catch ( Exception $e )
        {
            Cms_SystemLog::add('error','ContentBlockData.php Failure to Delete. Data: '.json_encode($data));
        }

        $this->delete(array('data_id = ?'=>$data['data_id']));
    }

    public function addContent( array $formdata )
    {
        $data = array(
                        'course_id'     => $formdata['course_id'],
                        'block_id'      => $formdata['block_id'],
                        'data_type'     => $formdata['type'],
                        'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                        'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
        );

        $data_id = $this->insert($data);

        $datacontent = array('data_id' => $data_id );

        switch( $formdata['type'] )
        {
            case "content":

                $datacontent['title']   = $formdata['title'];
                $datacontent['content'] = $formdata['content'];

                $datacontentDb = new App_Model_DataContent();
                $datacontentDb->insert($datacontent);

                //we want the title
                $this->update(array('title' => $formdata['title']), array('data_id = ?' => $data_id));

            break;

            case "quiz":

                $datacontent['title']   = $formdata['title'];
                $datacontent['content'] = $formdata['description'];

                $datacontentDb = new App_Model_DataContent();
                $datacontentDb->insert($datacontent);

                //we want the title
                $this->update(array(
                    'title'          => $formdata['title'],
                    'description'    => $formdata['description'], 
                    'date_start'     => $formdata['date_start'],
                    'date_end'       => $formdata['date_end'],
                    'time_start'     => $formdata['time_start'], 
                    'time_end'       => $formdata['time_end'],
                    'gradepoint'     => $formdata['gradepoint'], 
                    'gradepass'      => $formdata['gradepass'], 
                    'multiattempt'   => $formdata['multiattempt'],
                    'grading_method' => $formdata['grading_method']
                ), array('data_id = ?' => $data_id));

            break;
			
			case "wiki":

                $datacontent['title']   = $formdata['title'];
                $datacontent['content'] = $formdata['content'];

                $datawikiDb = new App_Model_DataWiki();
                $datawikiDb->insert($datacontent);

                //we want the title
                $this->update(array('title' => $formdata['title']), array('data_id = ?' => $data_id));

            break;

            case "link":

                //$datacontent['title']   = $formdata['title'];
                //$datacontent['description'] = $formdata['description'];
                $datacontent['url'] = $formdata['url'];

                $linkDb = new App_Model_DataLink();
                $linkDb->insert($datacontent);

                //we want the title
                $this->update(array('title' => $formdata['title'], 'description' => $formdata['description']), array('data_id = ?' => $data_id));

            break;

            case "video":

                $filesDb = new App_Model_DataVideo();

                if ( $formdata['video-type'] == 'upload' )
                {
                    //upload file
                    $files = Cms_Common::uploadFiles($this->uploadDir, 'mp4', 20, 1, 'files');
					$formdata['thumb'] = '';
					
                    if ( !empty($files) )
                    {
                        foreach ( $files as $file )
                        {
                            $fileData = array(
                                'data_id'       => $data_id,
                                'video_type'    => 'upload',
                                'video_name'    => $file['filename'],
                                'video_url'     => Cms_System_Links::dataFile($file['fileurl'], false),
                                'video_size'    => $file['filesize']
                            );
							$filesDb->insert($fileData);

                            //we want the title
                            $this->update(array('title' => $formdata['title'], 'description' => $formdata['description']), array('data_id = ?' => $data_id));
                        }
                    }
					//upload thumb
                    
					if ( Cms_Common::hasUploadFiles('thumb') )
                    {
                        $thumb = Cms_Common::uploadFiles($this->uploadDir, 'jpg,jpeg,png',5,1,'thumb');

                        if ( isset($thumb) && !empty($thumb) )
                        {
                            $formdata['thumb'] = isset($thumb[0]['fileurl']) ? Cms_System_Links::dataFile($thumb[0]['fileurl'], false) : '';
							
                            $filesDb->update(array('video_thumb' => $formdata['thumb']), array('data_id = ?' => $data_id));
                        }

                    }
					
                    else
                    {
                        $this->delete(array('data_id = ?'=>$data_id));
                        Cms_Common::notify('error','No files uploaded');
                        $this->redirect('/admin/courses/view/id/'.$formdata['course_id']);
                    }
					
                }
                else
                {
                    $fileData = array(
                        'data_id'       => $data_id,
                        'video_type'    => 'youtube',
                        'video_name'    => $formdata['title'],
                        'video_url'     => $formdata['url'],
                        'video_size'    => null,
                        'video_thumb'   => $formdata['thumb']
                    );

                    $filesDb->insert($fileData);

                    //we want the title
                    $this->update(array('title' => $formdata['title'], 'description' => $formdata['description']), array('data_id = ?' => $data_id));

                }

            break;

            case "support":

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');


                $filesDb = new App_Model_DataFiles();

                if ( !empty($files) )
                {
                    foreach ( $files as $file )
                    {
                        $fileData = array(
                                            'parent_id' => $data_id,
                                            'file_type' => 'blockfile',
                                            'file_name' => $file['filename'],
                                            'file_url'  => Cms_System_Links::dataFile($file['fileurl'], false),
                                            'file_size' => $file['filesize'],
                                            'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
                        );

                        $filesDb->insert($fileData);

                        //we want the title
                        $this->update(array('title' => $formdata['title'], 'description' => $formdata['description']), array('data_id = ?' => $data_id));
                    }
                }
                else
                {
                    $this->delete(array('data_id = ?'=>$data_id));
                    Cms_Common::notify('error','No files uploaded');
                    $this->redirect('/admin/courses/view/id/'.$formdata['course_id']);
                }

            break;
			
			  case "assignment":
              
                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');


                $filesDb = new App_Model_DataFiles();

                if ( !empty($files) )
                {
                    foreach ( $files as $file )
                    {
                        $fileData = array(
                                            'parent_id' => $data_id,
                                            'file_type' => 'blockfile',
                                            'file_name' => $file['filename'],
                                            'file_url'  => Cms_System_Links::dataFile($file['fileurl'], false),
                                            'file_size' => $file['filesize'],
                                            'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
                        );

                        $filesDb->insert($fileData);

                        //we want the title
                        $this->update(array('title' => $formdata['title'], 'description' => $formdata['description'],
                                    'date_start'     => $formdata['date_start'],'date_end'      => $formdata['date_end'],
                                    'time_start'     => $formdata['time_start'], 'time_end'    => $formdata['time_end'],
                                    'gradepoint'    => $formdata['gradepoint']), 
                            array('data_id = ?' => $data_id));
                    }
                }
                else
                {
                    $this->delete(array('data_id = ?'=>$data_id));
                    Cms_Common::notify('error','No files uploaded');
                    $this->redirect('/admin/courses/view/id/'.$formdata['course_id']);
                }


            break;

            case "audio":

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'mp3');


                $filesDb = new App_Model_DataFiles();

                if ( !empty($files) )
                {
                    foreach ( $files as $file )
                    {
                        $fileData = array(
                                            'parent_id' => $data_id,
                                            'file_type' => 'audiofile',
                                            'file_name' => $file['filename'],
                                            'file_url'  => Cms_System_Links::dataFile($file['fileurl'], false),
                                            'file_size' => $file['filesize'],
                                            'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
                        );

                        $filesDb->insert($fileData);

                        //we want the title
                        $this->update(array('title' => $formdata['title'], 'description' => $formdata['description']), array('data_id = ?' => $data_id));
                    }
                }
                else
                {
                    $this->delete(array('data_id = ?'=>$data_id));
                    Cms_Common::notify('error','No files uploaded');
                    $this->redirect('/admin/courses/view/id/'.$formdata['course_id']);
                }
                $data = array(
                        'date_start'     => $formdata['date_start'],
                        'date_end'      => $formdata['date_end'],
                        'time_start'     => $formdata['time_start'],
                        'time_end'    => $formdata['time_end'],
                );

                $data_id = $this->insert($data);

            break;
        }
        return $data_id;
    }

    public function updateContent( array $formdata, $id )
    {
        $data = array(
            'title' => $formdata['title'],
            'description' => $formdata['description'],
            'modified_by'    => Zend_Auth::getInstance()->getIdentity()->id,
            'modified_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
        );

        $this->update($data, array('data_id = ?' => $id));

        $datacontent = array();

        switch( $formdata['type'] )
        {
            case "content":

                $datacontent['title']   = $formdata['title'];
                $datacontent['content'] = $formdata['content'];

                $datacontentDb = new App_Model_DataContent();

                $datacontentDb->update($datacontent, array('data_id = ?' => $id));

                //we want the title
                $this->update(array('title' => $formdata['title']), array('data_id = ?' => $id));

            break;
			
			case "wiki":

                $datacontent['title']   = $formdata['title'];
                $datacontent['content'] = $formdata['content'];

                $datawikiDb = new App_Model_DataWiki();

                $datawikiDb->update($datacontent, array('data_id = ?' => $id));

                //we want the title
                $this->update(array('title' => $formdata['title']), array('data_id = ?' => $id));

            break;
			
            case "quiz":
                $this->update(array(
                    'title'          => $formdata['title'], 
                    'description'    => $formdata['description'],
                    'date_start'     => $formdata['date_start'],
                    'date_end'       => $formdata['date_end'],
                    'time_start'     => $formdata['time_start'],
                    'time_end'       => $formdata['time_end'],
                    'gradepoint'     => $formdata['gradepoint'],
                    'gradepass'      => $formdata['gradepass'], 
                    'multiattempt'   => $formdata['multiattempt'],
                    'grading_method' => $formdata['grading_method']
                ), array('data_id = ?' => $id));
            break;
			
			case "assignment":

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');

                $filesDb = new App_Model_DataFiles();

                if ( !empty($files) )
                {
                    foreach ( $files as $file )
                    {
                        $fileData = array(
                            'parent_id' => $id,
                            'file_type' => 'blockfile',
                            'file_name' => $file['filename'],
                            'file_url'  => Cms_System_Links::dataFile($file['fileurl'], false),
                            'file_size' => $file['filesize'],
                            'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
                        );

                        $filesDb->insert($fileData);

                        
                    }
                }
                
                //we want the title
                        $this->update(array('title' => $formdata['title'], 'description' => $formdata['description'],
                        'date_start'     => $formdata['date_start'],
                        'date_end'      => $formdata['date_end'],
                        'time_start'     => $formdata['time_start'],
                        'time_end'    => $formdata['time_end'],
                        'gradepoint'    => $formdata['gradepoint'],
                        ), array('data_id = ?' => $id));

            break;

            case "scorm":

                //we want the title
                $this->update(array('title' => $formdata['title']), array('data_id = ?' => $id));


            break;

            case "support":

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');

                $filesDb = new App_Model_DataFiles();

                if ( !empty($files) )
                {
                    foreach ( $files as $file )
                    {
                        $fileData = array(
                            'parent_id' => $id,
                            'file_type' => 'blockfile',
                            'file_name' => $file['filename'],
                            'file_url'  => Cms_System_Links::dataFile($file['fileurl'], false),
                            'file_size' => $file['filesize'],
                            'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
                        );

                        $filesDb->insert($fileData);

                        //we want the title
                        $this->update(array('title' => $formdata['title'], 'description' => $formdata['description']), array('data_id = ?' => $id));
                    }
                }

            break;

            case "link":


                //$datacontent['title']   = $formdata['title'];
                //$datacontent['description'] = $formdata['description'];
                $datacontent['url'] = $formdata['url'];

                $linkDb = new App_Model_DataLink();
                $linkDb->update($datacontent, array('data_id = ?' => $id));

                //we want the title
                $this->update(array('title' => $formdata['title'], 'description' => $formdata['description']), array('data_id = ?' => $id));

            break;

            case "video":

                $videoDb = new App_Model_DataVideo();

                if ( $formdata['video-type'] == 'upload' )
                {
                   //upload file
                    if  ( Cms_Common::hasUploadFiles('files') ) {
                        $files = Cms_Common::uploadFiles($this->uploadDir, 'mp4', 20, 1, 'files');
						//$files = Cms_Common::uploadFiles($this->uploadDir, 'jpg,jpeg,png',5,1,'thumb');
                    }

                    $video = $videoDb->fetchRow(array('data_id = ?' => $id));
                    $video = $video->toArray();

                    $formdata['thumb'] = '';


                    if ( isset($files) && !empty($files) )
                    {
                        //delete
                        unlink(DOCUMENT_PATH . $video['video_url']);

                        //add
                        $fileData = array(
                                'video_name'    => $files[0]['filename'],
                                'video_url'     => Cms_System_Links::dataFile($files[0]['fileurl'], false),
                                'video_size'    => $files[0]['filesize']
                            );

                        $videoDb->update($fileData, array('data_id = ?' => $id));
                    }

                    //upload thumb
                    
					if ( Cms_Common::hasUploadFiles('thumb') )
                    {
                        $thumb = Cms_Common::uploadFiles($this->uploadDir, 'jpg,jpeg,png',5,1,'thumb');

                        if ( isset($thumb) && !empty($thumb) )
                        {
                            //delete
                            unlink(DOCUMENT_PATH . $video['video_thumb']);

                            $formdata['thumb'] = isset($thumb[0]['fileurl']) ? Cms_System_Links::dataFile($thumb[0]['fileurl'], false) : '';

                            $videoDb->update(array('video_thumb' => $formdata['thumb']), array('data_id = ?' => $id));
                        }

                    }
					
                }
                else
                {
                    $datacontent['video_url'] = $formdata['url'];
                    $datacontent['video_thumb'] = $formdata['thumb'];
                    $datacontent['video_name'] = $formdata['title'];
                }

                //we want the title
                $this->update(array('title' => $formdata['title'], 'description' => $formdata['description']), array('data_id = ?' => $id));

                break;
        }
    }

    /*
     * get block data and group by block
     */
    public function getByBlock( $course_id, $sort = array())
    {
        $select = $this->select()
                    ->from(array('a' => $this->_name))
                    ->where('course_id = ?', $course_id)
                    ->order('arrangement');


        foreach($sort as $row) {
            $select->order($row);
        }

        $results = $this->fetchAll($select);
        //$results = $this->fetchAll(array('course_id = ?' => $course_id))->toArray();

        $out = array();

        foreach ( $results as $result )
        {
            $out[ $result['block_id'] ][ $result['data_id'] ] = $result;
        }

        return $out;
    }

    /*
    * save position of sorted contents in a block
    */
    public function updateArrangement( $data, $where )
    {
        $result = $this->update($data, $where);
    }
}
