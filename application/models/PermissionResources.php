<?php

class App_Model_PermissionResources extends Zend_Db_Table
{

    protected $_name = 'permission_resources';
    protected $_primary = 'id';
    protected $db;
    protected $locale;

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
        $this->db = $this->getDefaultAdapter();
    }
}
