<?php
class App_Model_Exam_ExamRoom extends Zend_Db_Table_Abstract {
    protected $_name    = 'tbl_examroom';
    protected $_primary = 'id';

    public function getRoom($es_id, $examcenter_id) {
        //$db = Zend_Db_Table::getDefaultAdapter();
        $db     = getDB2();
        
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'exam_schedule'), 'b.es_exam_center = a.examcenter_id', array())
            ->where('a.examcenter_id = ?', $examcenter_id)
            ->where('b.es_id = ?', $es_id)
            ->where('a.status = 1')
            ->order('a.order ASC');

        $rooms = $db->fetchAll($select);

        $room_id = -1;

        foreach($rooms as $room) {
            $select = $db->select()
                ->from(array('a' => 'exam_registration'), array('count' => 'COUNT(*)'))
                ->join(array('b' => 'exam_schedule'), 'b.es_id = a.examschedule_id', array())
                ->join(array('c' => $this->_name), 'c.id = a.examroom_id', array())
                ->where('b.es_id = ?', $es_id)
                ->where('b.es_exam_center = ?', $examcenter_id)
                ->where('c.id = ?', $room['id'])
                ->where("a.er_registration_type in (0, 1, 4, 9)");

            $count = $db->fetchRow($select);
            $count = $count['count'];
            
            if($count < $room['quota']) {
                $room_id = $room['id'];
                break;
            }
        }

        return $room_id;
    }

    /*public function getQuota($es_id, $examcenter_id) {
        //$db = Zend_Db_Table::getDefaultAdapter();
        $db     = getDB2();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('quota' => 'SUM(QUOTA)'))
            ->join(array('b' => 'exam_schedule'), 'b.es_exam_center = a.examcenter_id', array())
            ->where('a.examcenter_id = ?', $examcenter_id)
            ->where('b.es_id = ?', $es_id)
            ->where('a.status = 1')
            ->order('a.order ASC');

        $quota = $db->fetchRow($select);

        $select = $db->select()
            ->from(array('a' => 'exam_registration'), array('registered' => 'COUNT(*)'))
            ->join(array('b' => 'exam_schedule'), 'b.es_id = a.examschedule_id', array())
            ->join(array('c' => 'tbl_exam_center'), 'c.ec_id = b.es_exam_center', array())
            ->join(array('d' => $this->_name), 'd.examcenter_id = c.ec_id', array())
            ->where('b.es_id = ?', $es_id)
            ->where('b.es_exam_center = ?', $examcenter_id)
            ->where("a.er_registration_type in (0, 1, 4, 9)")
            ->group('a.er_id');

        $registered = $db->fetchRow($select);

        $result = array(
            'quota'      => $quota['quota'],
            'registered' => $registered['registered'],
            'available'  => $quota['quota'] - $registered['registered']
        );
        
        return $result;
    }*/

       public function getQuota($es_id, $examcenter_id, $room_id = 0) {
         $db     = getDB2();
        $select = $db->select();

        if($room_id) {
            $select->from(array('a' => $this->_name))
                ->where('a.id = ?', $room_id);
        }
        else {
            $select->from(array('a' => $this->_name), array('quota' => 'SUM(QUOTA)'));
        }
            
        $select->join(array('b' => 'exam_schedule'), 'b.es_exam_center = a.examcenter_id', array())
            ->where('a.examcenter_id = ?', $examcenter_id)
            ->where('b.es_id = ?', $es_id)
            ->where('a.status = 1')
            ->order('a.order ASC');

        $quota = $db->fetchRow($select);

        $select = $db->select()
            ->from(array('a' => 'exam_registration'), array('registered' => 'COUNT(*)'))
            ->join(array('b' => 'exam_schedule'), 'b.es_id = a.examschedule_id', array())
            ->where("a.er_registration_type in (1, 4, 9)")
            ->where('a.examschedule_id = ?', $es_id)
            ->where('b.es_exam_center = ?', $examcenter_id);

        if($room_id) {
            $select->where('a.examroom_id = ?', $room_id);
        }

        $registered = $db->fetchRow($select);

        $result = array(
            'quota'      => $quota['quota'],
            'registered' => $registered['registered'],
            'available'  => $quota['quota'] - $registered['registered']
        );

        foreach($result as $key => $value) {
            if(!$value) {
                $result[$key] = 0;
            }
        }
        
        return $result;
    }

    public function checkCapacity($es_id, $examcenter_id, $head_count = 1) {
        $quota = $this->getQuota($es_id, $examcenter_id);
        // $head_count = 99999;
        if($quota['available'] >= $head_count) {
            return 1;
        }

        return 0;
    }

}
?>