<?php 
class App_Model_Exam_ExamSchedule extends Zend_Db_Table_Abstract
{
    protected $_name    = 'exam_schedule';
    protected $_primary = "es_id";
    protected $_room    = 'tbl_examroom';
	
	public function getDatabyId($id=0){
		$id = (int)$id;
		
		 $db = getDB2();
		
		$select = $db->select()
	                ->from(array('es'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getData(){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('es'=>$this->_name));		                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	public function addData($data){
		$this->insert($data);
		$db = Zend_Db_Table::getDefaultAdapter();
		return $db->lastInsertId();
	}
	
	public function updateData($data,$id){
		$this->update($data,"es_id ='".$id."'");
	}
	
	public function deleteData($add_id){
		$this->delete("es_id ='".$add_id."'");
	}
	
	
	public function getScheduleByExamCenter($formData){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('es'=>$this->_name))
	                ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=es.es_course',array('IdSubject','subjectMainDefaultLanguage','SubCode','SubjectName'))
	                ->join(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=es.es_semester',array('IdSemesterMaster','SemesterMainName','SemesterMainDefaultLanguage'))
	                ->order('es.es_date desc');
	                

	                if(isset($formData['IdSemester']) && $formData['IdSemester']!= ''){
	    				$select->where('es_semester = ?',$formData['IdSemester']);
	                }	

	                if(isset($formData['IdSubject']) && $formData['IdSubject']!= ''){
	    				$select->where('es_course = ?',$formData['IdSubject']);
	                }	
        
        $row = $db->fetchAll($select);
		return $row;
		
	}
	
	
	public function getScheduleBySubject($idSemester,$idSubject,$ec_id=0){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('es'=>$this->_name))	              
	                ->where('es.es_semester = ?',$idSemester)
	                ->where('es.es_course = ?',$idSubject);
	    
	    if(isset($ec_id) && $ec_id!=0){           
	   	 	$select->where('es.es_exam_center = ?',$ec_id);
	    }
	     
        $row = $db->fetchRow($select);
        
        if(!$row){
        	
        	$select2 = $db->select()
	                ->from(array('es'=>$this->_name))	              
	                ->where('es.es_semester = ?',$idSemester)
	                ->where('es.es_course = ?',$idSubject);
	     
       		$row2 = $db->fetchRow($select2);
       		$row = $row2;
        }
        
		return $row;
		
	}

     public function getScheduleCalendar($IdProgram, $IdLandscape, $params = array()) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'exam_setup'), 'b.es_id = a.es_examsetup_id', array('examsetup_id'=>'es_id', 'examsetup_name'=>'es_name', 'IdLandscape' => 'es_idLandscape'))
            ->join(array('c' => 'exam_setup_detail'), 'c.esd_id = a.es_esd_id', array('esd_id'))
            ->join(array('d' => 'tbl_markdistribution_setup'), 'd.mds_id = c.esd_mds_id', array('mds_id', 'mds_name'))
            ->join(array('e' => 'tbl_exam_center'), 'e.ec_id = a.es_exam_center', array('ec_id', 'ec_name'))
            ->join(array('f' => 'programme_exam'), 'f.pe_id = d.mds_programme_exam', array('pe_id', 'pe_name'))
            ->join(array('g' => 'tbl_program'), 'g.IdProgram = f.IdProgram', array('exam_closing_date'))
            // ->where('a.es_date > ?', date('Y-m-d', strtotime('+14 days')))
            ->where('a.es_course      = ?', $IdProgram)
            ->where('b.es_idLandscape = ?', $IdLandscape)
            ->order('a.es_date ASC');

        if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
            $select->where('a.es_date > DATE_SUB(a.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
        }
        else {
            $select->where('a.es_date > DATE_SUB(a.es_date, INTERVAL g.exam_closing_date DAY)');
        }

        $result   = $db->fetchAll($select);
        $schedule = array();

        for($i=1; $i<=12; $i++) {
            $month = str_pad($i, 2, "00", STR_PAD_LEFT);
            $schedule[$month] = array();
        }

        foreach($result as $row) {
            $month = date('m', strtotime($row['es_date']));
            $month = str_pad($month, 2, "00", STR_PAD_LEFT);
            $date  = $row['es_date'];

            $row['es_date_dmy']        = date('d-m-Y', strtotime($row['es_date']));
            $row['es_start_time_ampm'] = date('H:iA', strtotime($row['es_start_time']));
            $row['es_end_time_ampm']   = date('H:iA', strtotime($row['es_end_time']));
            $schedule[$month][$date][] = $row;
        }
        
        return $schedule;
    }

    public function getExamDate($esd_id, $ec_id, $params = array()) {
        $ecrDB  = new Examination_Model_DbTable_ExamRoom();
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'exam_setup'), array())
            ->join(array('b' => 'exam_setup_detail'), 'b.es_id = a.es_id', array())
            ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array('examschedule_id' => 'es_id', '*'))
            ->join(array('d' => 'tbl_markdistribution_setup'), 'd.mds_id = esd_mds_id', array())
            ->join(array('e' => 'programme_exam'), 'e.pe_id = b.esd_pe_id AND e.pe_id = d.mds_programme_exam', array('pe_name'))
            ->joinLeft(array('f' => 'tbl_definationms'), 'f.idDefinition = c.es_session', array('es_session_desc' => 'DefinitionDesc'))
            ->join(array('g' => 'tbl_program'), 'g.IdProgram = e.IdProgram', array('exam_closing_date'))
            ->where('b.esd_id = ?', $esd_id)
            ->where('c.es_exam_center = ?', $ec_id)
            ->where('c.es_date > ?', date('Y-m-d', strtotime('+14 days')))
            ->order('c.es_date ASC')
            ->order('c.es_start_time');

        if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
        }
        else {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL g.exam_closing_date DAY)');
        }

        $result = $db->fetchAll($select);

        $schedule = array();

        for($i=1; $i<=12; $i++) {
            $month = str_pad($i, 2, "00", STR_PAD_LEFT);
            $schedule[$month] = array();
        }

        foreach($result as $row) {
            $month = date('m', strtotime($row['es_date']));
            $month = str_pad($month, 2, "00", STR_PAD_LEFT);
            $date  = $row['es_date'];

            $select = $db->select()
                ->from(array('a' => 'exam_registration'), array('count' => 'COUNT(*)'))
                ->where('a.examschedule_id = ?', $row['es_id'])
                ->where('a.er_registration_type in (0, 1, 4, 9)');
            $registered = $db->fetchRow($select);

            $row['total_seat']     = 30; // TODO: tukar benda ni jadi field. sape yg nak buat??
            $row['available_seat'] = $row['total_seat'] - $registered['count'];

            $row['es_date_dmy']        = date('d-m-Y', strtotime($row['es_date']));
            $row['es_start_time_ampm'] = date('H:iA', strtotime($row['es_start_time']));
            $row['es_end_time_ampm']   = date('H:iA', strtotime($row['es_end_time']));
            $row['seat_quota']         = $ecrDB->getQuota($row['examschedule_id'], $row['es_exam_center']);

            $schedule[$month][$date][] = $row;
        }

        return $schedule;
    }


	
}

?>