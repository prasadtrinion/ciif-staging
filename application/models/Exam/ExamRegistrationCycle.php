<?php 
class App_Model_Exam_ExamRegistrationCycle extends Zend_Db_Table_Abstract
{
    protected $_name    = 'exam_registration_cycle';
    protected $_primary = "erc_id";

    public function addData($data) {
        $db = getDB2();
        $db->insert($this->_name, $data);
        $id = $db->lastInsertId();
        return $id;
    }

	public function updateData($data,$where)
    {
    	$db = getDB2();
    	$db->update($this->_name,$data,$where);
    }
    
	public function getData($erc_id) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('erc'=>$this->_name))
            ->where('erc.erc_id = ?',$erc_id);

        $result = $db->fetchRow($select);
        return $row;
    }
    
	public function getExamCycle($IdStudentRegistration) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('erc'=>$this->_name))
            ->where('erc.IdStudentRegistration = ?',$IdStudentRegistration)
            ->where('erc.erc_id')
            ->order('erc.erc_id DESC');

        $result = $db->fetchRow($select);
        return $row;
    }
    
 	public function getCountExamCycle($IdStudentRegistration) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('erc'=>$this->_name))
            ->where('erc.IdStudentRegistration = ?',$IdStudentRegistration);

        $result = $db->fetchAll($select);
        return count($result);
        //return $row;
    }
    
	public function getListExamCycle($IdStudentRegistration) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('erc'=>$this->_name))
            ->where('erc.IdStudentRegistration = ?',$IdStudentRegistration)
            ->order('erc_createddt ASC');

        $result = $db->fetchAll($select);
        return $result;
    }
}