<?php 
class App_Model_Exam_ProgrammeExamPrerequisite extends Zend_Db_Table_Abstract
{
    protected $_name             = 'programme_exam_prerequisite';
    protected $_primary          = "pep_id";
    protected $_pep_based_on     = [1=>'Programme', 2=>'Exam', 3=>'Module'];
    protected $_pep_grade_status = [0=>'Fail', 1=>'Pass'];

    public function init() {
        $this->auth = Zend_Auth::getInstance();
    }

    public function getDataByPeId($pe_id) {
        $db     = getDB2();
        $query = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'tbl_program'), 'a.IdProgram = b.IdProgram', array('ProgramName', 'ArabicName'))
            ->join(array('c' => 'programme_exam'), 'a.pe_id = c.pe_id', array('pe_name'))
            ->join(array('d' => 'tbl_definationms'),'d.idDefinition = a.pep_type', array("DefinitionCode as pep_type_text"))
            ->joinLeft(array('e' => 'tbl_program'), 'e.IdProgram = a.pep_idprogram', array('required_IdProgram' => 'IdProgram', 'required_IdProgram_desc' => 'ProgramName', 'required_ArabicName' => 'ArabicName'))
            ->joinLeft(array('f' => 'tbl_landscape'), 'f.IdLandscape = a.pep_idlandscape', array('required_IdLandscape' => 'IdLandscape'))
            ->joinLeft(array('g' => 'tbl_program_scheme'), 'g.IdProgramScheme = f.IdProgramScheme', array())
            ->joinLeft(array('h' => 'tbl_definationms'), 'h.idDefinition = g.mode_of_program', array('required_IdLandscape_desc' => 'DefinitionDesc'))
            ->joinLeft(array('i' => 'tbl_subjectmaster'), 'i.IdSubject = a.pep_idsubject', array('required_IdSubject' => 'IdSubject', 'required_IdSubject_desc' => 'SubjectName'))
            ->joinLeft(array('j' => 'programme_exam'), 'j.pe_id = a.pep_programmeexam_id', array('required_pe_id' => 'pe_id', 'required_pe_desc' => 'pe_name'))
            ->where("a.pe_id = $pe_id");
        $result = $db->fetchAll($query);

        foreach($result as $index => $pep) {
            $result[$index]['pep_based_on_text']         = $this->_pep_based_on[$pep['pep_based_on']];
            $result[$index]['pep_grade_status_text']     = $this->_pep_grade_status[$pep['pep_grade_status']];
        }

        return $result;
    }

    public function getData($pep_id, $join = true) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = $db->select()
            ->from(array('a'=>$this->_name))
            ->where("a.pep_id=$pep_id");

        if($join) {
            $query->join(array('b'=>'tbl_program'), 'a.IdProgram=b.IdProgram', array('ProgramName', 'ArabicName'))
                ->join(array('c' => 'programme_exam'), 'a.pe_id=c.pe_id', array('pe_name'))
                ->join(array('d' => 'tbl_definationms'),'d.idDefinition=a.pep_type', array("DefinitionCode as pep_type_text"))
                ->joinLeft(array('e' => 'tbl_program'), 'e.IdProgram = a.pep_idprogram', array('required_IdProgram' => 'IdProgram', 'required_IdProgram_desc' => 'ProgramName', 'required_ArabicName' => 'ArabicName'))
                ->joinLeft(array('f' => 'tbl_landscape'), 'f.IdLandscape = a.pep_idlandscape', array('required_IdLandscape' => 'IdLandscape'))
                ->joinLeft(array('g' => 'tbl_program_scheme'), 'g.IdProgramScheme = f.IdProgramScheme', array())
                ->joinLeft(array('h' => 'tbl_definationms'), 'h.idDefinition = g.mode_of_program', array('required_IdLandscape_desc' => 'DefinitionDesc'))
                ->joinLeft(array('i' => 'tbl_subjectmaster'), 'i.IdSubject = a.pep_idsubject', array('required_IdSubject' => 'IdSubject', 'required_IdSubject_desc' => 'SubjectName'))
                ->joinLeft(array('j' => 'programme_exam'), 'j.pe_id = a.pep_programmeexam_id', array('required_pe_id' => 'pe_id', 'required_pe_desc' => 'pe_name'));
        }

        $pep = $db->fetchRow($query);

        if($join) {
            $pep['pep_based_on_text']         = $this->_pep_based_on[$pep['pep_based_on']];
            $pep['pep_grade_status_text']     = $this->_pep_grade_status[$pep['pep_grade_status']];
        }

        return $pep;
    }

    public function deleteData($pep_id) {
        return $this->delete("pep_id = $pep_id");
    }

    public function flushPrerequisites($pe_id) {
        return $this->delete("pe_id= $pe_id");
    }

    public function checkPrerequisite($pep_id, $std_id, $IdStudentRegistration) {
        $db     = getDB2();
      echo  $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'tbl_definationms'), 'b.idDefinition = a.pep_type', array('pep_type_desc' => 'DefinitionCode'))
            ->where('a.pep_id = ?', $pep_id);
        $pep = $db->fetchRow($select);

        $result   = null;
        $result2  = null;
        $response = array(
            'result' => 0,
            'value'  => 0
        );

        switch ($pep['pep_based_on']) {
            case 1:
            case 3:

                if($pep['pep_type_desc'] == 'Attendance') {
                    $result  = $this->_checkForProgram($pep, $std_id, $IdStudentRegistration, 395);
                    $result2 = $this->_checkForProgram($pep, $std_id, $IdStudentRegistration);
                }
                else {
                    $result = $this->_checkForProgram($pep, $std_id, $IdStudentRegistration);
                }
                break;
            case 2:
                $result = $this->_checkForExam($pep, $std_id, $IdStudentRegistration);
                break;
        }

        if($pep['pep_type_desc'] == 'Attendance') {
            if($result2 == 0) {
                return $response;
            }

            $precentange =  $result / $result2 * 100;

            $response = array(
                'result' => ($precentange >= $pep['pep_attendance'] ? 1 : 0),
                'value'  => $precentange
            );
        }
        else {
            $response = array(
                'result' => ($result > 0 ? 1 : 0),
                'value'  => $result
            );
        }

        return $response;
    }

    public function _checkForProgram($pep, $std_id, $IdStudentRegistration, $status = 0) {
        $db     = getDB2();
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregistration'), array('count' => 'COUNT(*)'))
            // ->from(array('a' => 'tbl_studentregistration'), array('IdStudentRegistration'))
            ->join(array('c' => 'tbl_studentregsubjects'), 'c.IdStudentRegistration = a.IdStudentRegistration', array('IdStudentRegSubjects'))
            ->where('a.IdStudentRegistration = ?', $IdStudentRegistration);

        if($pep['pep_idprogram']) {
            $select->where('a.IdProgram = ?', $pep['pep_idprogram']);
        }

        if($pep['pep_idlandscape']) {
            $select->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.IdLandscape', array())->where('b.IdLandscape = ?', $pep['pep_idlandscape']);
        }

        if($pep['pep_idsubject']) {
            $select->where('c.IdSubject = ?', $pep['pep_idsubject']);
        }

        if($pep['pep_type_desc'] == 'Attendance') {
            $select->join(array('e' => 'course_group_schedule'), 'e.sc_id = c.IdCourseTaggingGroup', array('cgs_id' => 'sc_id'));
            $select->join(array('f' => 'course_group_schedule_detail'), 'f.scDetId = e.sc_id', array('cgsd_id' => 'id', 'sc_date'));
            $select->join(array('g' => 'course_group_attendance'), 'g.IdSchedule = f.id AND g.IdStudentRegistration = a.IdStudentRegistration', array('cga_id' => 'id', 'attendance_status' => 'status'));
            if($status) {
                $select->where('g.status = ?', 395);
            }    
        }
        $result = $db->fetchRow($select);

        return $result['count'];
    }

    public function _checkForExam($pep, $std_id, $IdStudentRegistration) {
        $db     = getDB2();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'), array('count' => 'COUNT(*)'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array())
            ->where("a.er_registration_type in (0, 1, 4, 9)")
            ->where('b.sp_id = ?', $std_id);
        
        if($pep['pep_type_desc'] == 'Grade Status') {
            $select->where('a.pass = ?', $pep['pep_grade_status']);
        }

        $result = $db->fetchRow($select);

        return $result['count'];
    }

}
?>