<?php
class App_Model_Exam_ExamRegistration extends Zend_Db_Table_Abstract
{
    protected $_name = 'exam_registration';
	protected $_primary = "er_id";
    protected $_history = 'exam_registration_history';


 public function getEnrolexam(array $where)
    {
        //$db = $this->getDefaultAdapter();
        $db     = getDB2();

        $select = $db->select()->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }


        $result = $db->fetchRow( $select );

        return $result;
    }


	public function updateDataSc($data,$id, $remarks = null) {
        $auth = Zend_Auth::getInstance();
        $db     = getDB2();

        if($auth->getIdentity()->login_as){//if admin
            $updateby = 1;//1 from tbl_user
        }else{
            $updateby = $auth->getIdentity()->external_id;//$auth->getIdentity()->sp_id;
        }

        $data['modifieddt'] = date('Y-m-d H:i:s');
        $data['modifiedby'] = $updateby;

        $result = $db->update($this->_name,($data),$this->_primary . ' = ' . (int)$id);

        $db     = getDB2();
        $query = $db->select()
            ->from(array('er' => $this->_name))
            ->where('er.er_id = ?', $id);
        $data = $db->fetchRow($query);
        $data['remarks'] = $remarks;
        $data['payment_status'] = (int) $data['payment_status'];
        $db->insert($this->_history, $data);

        return $result;

    }

    public function getDatabyId($id=0){
        $id = (int)$id;

         $db     = getDB2();

        $select = $db->select()
                    ->from(array('es'=>'exam_schedule') )
                    ->where('es_id'.' = ' .$id);

        $row = $db->fetchRow($select);
        return $row;

    }

    public function geterDatabyId($id=0){
        $id = (int)$id;

         $db     = getDB2();

        $select = $db->select()
                   ->from(array('er' => $this->_name))
                    ->where($this->_primary.' = ' .$id);

        $row = $db->fetchRow($select);
        //pr($row);exit;
        return $row;

    }

    public function getDataByErId($er_id, $IdStudentRegistration = 0) {
        $db = getDB2();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array())
            ->join(array('c' => 'student_profile'), 'c.std_id = b.sp_id', array('std_fullname', 'std_idnumber'))
            ->join(array('d' => 'exam_schedule'), 'd.es_id = a.examschedule_id', array('examschedule_id' => 'es_id', 'es_date', 'result_publish_date' => "CONCAT_WS(' ', es_publish_start_date, es_publish_start_time)"))
            ->join(array('f' => 'exam_setup_detail'), 'f.esd_id = a.examsetupdetail_id', array('esd_id'))
            ->join(array('g' => 'exam_setup'), 'f.es_id = g.es_id', array('examsetup_id' => 'es_id', 'es_name', 'es_idProgram'))
            ->join(array('h' => 'tbl_program'), 'h.IdProgram = g.es_idProgram', array('ProgramName', 'ProgramCode'))
            ->join(array('i' => 'programme_exam'), 'f.esd_pe_id = i.pe_id', array('pe_id', 'pe_name'))

            ->join(array('j' => 'exam_scheduletaggingslot'), 'j.sts_id = a.examtaggingslot_id', array('schedule_tagging_slot_id' => 'sts_id'))
            ->join(array('k' => 'exam_slot'), 'k.sl_id = j.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->join(array('l' => 'exam_taggingslotcenterroom'), 'l.tsc_slotid = tsc_examcenterid', array('tagging_slot_center_room_id' => 'tsc_id'))
            ->join(array('m' => 'tbl_exam_center'), 'm.ec_id =  l.tsc_examcenterid', array('exam_center_id' => 'ec_id', 'ec_name'))
            ->joinLeft(array('n' => 'tbl_examroom'), 'n.id = l.tsc_room_id', array('examroom_name' => 'name'))

            ->joinLeft(array('t' => 'tbl_subjectmaster'), 'f.esd_idSubject = t.IdSubject', array('IdSubject', 'SubjectName', 'SubCode'))
            ->join(array('v' => 'tbl_landscape'), 'v.IdLandscape = a.landscape_id', array())
            ->join(array('w' => 'tbl_program_scheme'), 'w.IdProgramScheme = v.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('x' => 'tbl_definationms'), 'x.idDefinition = w.mode_of_program', array('mop' => 'DefinitionDesc'))
            ->joinLeft(array('y' => 'tbl_definationms'),'a.attendance_status = y.idDefinition', array('attendance_status_desc' => 'DefinitionDesc'))

            ->where('a.er_id = ?', $er_id)
            ->order('c.std_fullname ASC')
            ->order('d.es_date ASC')
            ->order('k.sl_starttime ASC')
            ->group('a.er_id');

        if($IdStudentRegistration) {
            echo 
            //$select->join(array('l' => 'exam_registration'), 'l.bes_id = a.bes_id', array('er_id'));
            //$select->where("l.er_registration_type in (0, 1, 4, 9)");
            $select->where('a.student_id = ?', $IdStudentRegistration);
        }
        /*
        else {
            $select->where('a.individual = ?', 0);
        }*/

        $result = $db->fetchAll($select);

        return $result;
    }

          /*public function updateData($data, $er_id) {
            $auth  = Zend_Auth::getInstance();
            $data['modifieddt']  = date('Y-m-d H:i:s');
            $data['modifiedby']  = $auth->getIdentity()->iduser;

            $result = $this->update($data, "er_id = $er_id");

            $db    = Zend_Db_Table::getDefaultAdapter();
            $query = $db->select()
                ->from(array('er' => $this->_name))
                ->where('er.er_id = ?', $er_id);
            $data = $db->fetchRow($query);
            $db->insert($this->_history, $data);

            return $result;
        }*/


    public function getCourseRegExambyStudent($idSemester,$idStudentRegistration){

		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('cm'=>'tbl_subjectmaster'),'cm.IdSubject=er.er_idSubject',array('SubjectName','subjectMainDefaultLanguage','SubCode'))
	                ->joinLeft(array('ec'=>'tbl_exam_center'),'ec.ec_id=er.er_ec_id',array('ec_name'))
	                ->where('er_idSemester = ?',$idSemester)
	                ->where('er_idStudentRegistration = ?',$idStudentRegistration);

        $row = $db->fetchAll($select);
		return $row;
	}

     public function getExamReg($id) {
         $db = getDB2();

       $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('f'=>'exam_setup'),'a.examsetup_id=f.es_id')
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = a.student_id',array('IdProgram','IdProgramScheme'))
            ->join(array('be'=>'exam_setup_detail'),'be.esd_id = a.examsetupdetail_id',array())
            ->join(array('p'=>'programme_exam'),'p.pe_id = be.esd_pe_id',array('examname'=>'pe_name','pe_createddt' => 'createddt'))
            ->join(array('g'=>'exam_schedule'),'a.examschedule_id=g.es_id', array(
                '*',
                'allow_publish_result' => "DATEDIFF(CURDATE(), es_publish_start_date)"
            ))
            ->join(array('e'=>'tbl_program'),'a.IdProgram=e.IdProgram')
            ->joinLeft(array('td'=>'tbl_programclosingdate'),'e.IdProgram=td.IdProgram AND td.category_id = 1 AND td.registration_type = 1',array('pcd_createddt' => 'createddt'))
            //->join(array('d'=>'tbl_address'),'add.add_org_id=ec.ec_id') 
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.landscape_id', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array('IdProgramScheme','ps_createddt' => 'createddt'))
            ->join(array('d' => 'tbl_definationms'), 'd.idDefinition = c.mode_of_program', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->joinLeft(array('a2' => 'tbl_definationms'), 'a2.idDefinition = a.attendance_status', array('attendance_status_desc' => 'DefinitionDesc'))
            //->join(array('e' => 'tbl_program'), 'e.IdProgram = a.IdProgram', array('IdProgram', 'ProgramCode', 'ProgramName', 'exam_only', 'exam_closing_date'))
            //->join(array('f' => 'exam_setup'), 'f.es_id = a.examsetup_id', array('examsetup_id' => 'es_id', 'es_name', 'es_online', 'es_awardname'))
            ->joinLeft(array('is' => 'invoice_student'), 'is.examregistration_main_id = a.er_main_id', array('invoice_id'))
            ->where("((a.ber_id = 0 OR a.ber_id is null) AND a.er_registration_type in (0, 1, 4, 9, 10)) OR ((a.ber_id != 0 OR a.ber_id is not null) AND a.er_registration_type in (1,4, 10))")
            ->order('a.createddt DESC')
            ->where('sr.sp_id = ?', $id);
            // ->where('td.category_id = 1')
            // ->where('td.registration_type = 1');

        $row = $db->fetchAll($select);
        return $row;
    }

    public function getExamregid($id) {
         $db = getDB2();

        $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('f'=>'exam_setup'),'a.examsetup_id=f.es_id')
            ->join(array('e'=>'tbl_program'),'a.IdProgram=e.IdProgram')
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.landscape_id', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array('IdProgramScheme','ps_createddt' => 'createddt'))
            ->join(array('d' => 'tbl_definationms'), 'd.idDefinition = c.mode_of_program', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->join(array('g'=>'exam_schedule'),'a.examschedule_id=g.es_id')
            // ->join(array('h'=>'tbl_exam_center'),'h.ec_id=g.es_exam_center', array('ec_createddt' => 'createddt','ec_name'))
            ->join(array('i'=>'exam_setup_detail'),'g.es_esd_id=i.esd_id')
            ->join(array('j'=>'programme_exam'),'i.esd_pe_id=j.pe_id',array('examname'=>'pe_name','pe_createddt' => 'createddt','pe_name' => 'pe_name' ))
            ->joinLeft(array('k' => 'tbl_subjectmaster'), 'k.IdSubject = i.esd_idSubject', array('IdSubject', 'SubjectName', 'SubCode'))

            ->join(array('l' => 'exam_scheduletaggingslot'), 'l.sts_id = a.examtaggingslot_id', array('schedule_tagging_slot_id' => 'sts_id'))
            ->join(array('m' => 'exam_slot'), 'm.sl_id = l.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->join(array('n' => 'exam_taggingslotcenterroom'), 'n.tsc_slotid = sl_id AND n.tsc_examcenterid = a.examcenter_id', array('tagging_slot_center_room_id' => 'tsc_id'))
            ->joinLeft(array('o' => 'tbl_exam_center'), 'o.ec_id = n.tsc_examcenterid AND o.ec_id = a.examcenter_id', array('exam_center_id' => 'ec_id', 'ec_name'))
            ->join(array('p' => 'tbl_studentregistration'), 'p.IdStudentRegistration = a.student_id', array('std_id' => 'sp_id'))
            ->joinLeft(array('q'=>'tbl_programclosingdate'),'e.IdProgram=q.IdProgram AND q.category_id = 1 AND q.registration_type = 1', array('days' => 'IFNULL(days, 14)'))
            ->where('a.er_id = ?', $id);
            //->where('g.es_active = 1');

        $row = $db->fetchRow($select);
        return $row;
    }

         public function getProgramClo($IdProgram,$cat=1,$regtype=1) {
         $db = getDB2();

        $select = $db->select()
            ->from(array('a'=>'tbl_program'),array('IdProgram'))
            ->join(array('b'=>'tbl_programclosingdate'),'a.IdProgram=b.IdProgram')

            ->where('a.IdProgram = ?', $IdProgram)
            ->where('b.category_id = ?', $cat)
            ->where('b.registration_type = ?', $regtype);

        $row = $db->fetchAll($select);
        return $row;
    }

     public function getErorder($order_id) {
         $db = getDB2();

        $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('f'=>'exam_setup'),'a.examsetup_id=f.es_id')
            ->join(array('e'=>'tbl_program'),'a.IdProgram=e.IdProgram')
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.landscape_id', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('d' => 'tbl_definationms'), 'd.idDefinition = c.mode_of_program', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->join(array('g'=>'exam_schedule'),'a.examschedule_id=g.es_id')
            ->join(array('h'=>'tbl_exam_center'),'h.ec_id=g.es_exam_center')
            ->join(array('i'=>'exam_setup_detail'),'g.es_esd_id=i.esd_id')
            ->join(array('j'=>'programme_exam'),'i.esd_pe_id=j.pe_id')
            ->joinLeft(array('k' => 'tbl_subjectmaster'), 'k.IdSubject = i.esd_idSubject', array('IdSubject', 'SubjectName', 'SubCode'))
           ->joinLeft(array('l' => 'tbl_studentregistration'), 'l.IdStudentRegistration = a.student_id', array('IdStudentRegistration', 'sp_id'))
           ->joinLeft(array('m' => 'student_profile'), 'm.std_id = l.sp_id', array('std_fullname','spt_id'))
            ->where('a.order_id = ?', $order_id);

        $row = $db->fetchAll($select);
        return $row;
    }

    public function getDataOrder($orderId) {
        $db = $this->getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => 'order'))
            ->where('a.id = ?', $orderId);

        $result = $db->fetchAll($query);
        return $result;
    }

     public function getDataEr($er_id) {
         $db = getDB2();

        $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('f'=>'exam_setup'),'a.examsetup_id=f.es_id')
            ->join(array('e'=>'tbl_program'),'a.IdProgram=e.IdProgram')
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.landscape_id', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('d' => 'tbl_definationms'), 'd.idDefinition = c.mode_of_program', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->join(array('g'=>'exam_schedule'),'a.examschedule_id=g.es_id')
            ->join(array('h'=>'tbl_exam_center'),'h.ec_id=g.es_exam_center')
            ->join(array('i'=>'exam_setup_detail'),'g.es_esd_id=i.esd_id')
            ->join(array('j'=>'programme_exam'),'i.esd_pe_id=j.pe_id')
            ->joinLeft(array('k' => 'tbl_subjectmaster'), 'k.IdSubject = i.esd_idSubject', array('IdSubject', 'SubjectName', 'SubCode'))
           ->joinLeft(array('l' => 'tbl_studentregistration'), 'l.IdStudentRegistration = a.student_id', array('IdStudentRegistration', 'sp_id'))
           ->joinLeft(array('m' => 'student_profile'), 'm.std_id = l.sp_id', array('std_fullname','spt_id'))
            ->where('a.er_id = ?', $er_id);

        $row = $db->fetchAll($select);
        return $row;
    }

     public function getInvoiceMain($orderId) {
        $db = getDB2();
        $query = $db->select()
            ->from(array('a' => 'invoice_main'))
            ->joinLeft(array('b'=>'tbl_currency'),'a.currency_id = b.cur_id',array('cur_code','cur_symbol_prefix'))
            ->where('a.order_id = ?', $orderId);

        $result = $db->fetchRow($query);
        return $result;
    }

    public function getExamstudentid($external_id,$IdProgram=0) {
         $db = getDB2();

         $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('b'=>'tbl_studentregistration'),'a.student_id = b.IdStudentRegistration')
           /*->join(array('f'=>'exam_setup'),'a.examsetup_id=f.es_id')
            ->join(array('e'=>'tbl_program'),'a.IdProgram=e.IdProgram')
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.landscape_id', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('d' => 'tbl_definationms'), 'd.idDefinition = c.mode_of_program', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->join(array('g'=>'exam_schedule'),'a.examschedule_id=g.es_id')
            ->join(array('h'=>'tbl_exam_center'),'h.ec_id=g.es_exam_center')
            ->join(array('i'=>'exam_setup_detail'),'g.es_esd_id=i.esd_id')
            ->join(array('j'=>'programme_exam'),'i.esd_pe_id=j.pe_id')
            ->joinLeft(array('k' => 'tbl_subjectmaster'), 'k.IdSubject = i.esd_idSubject', array('IdSubject', 'SubjectName', 'SubCode'))*/

            ->where('b.sp_id = ?', $external_id)
            ->where('b.IdProgram = ?', $IdProgram);

        $row = $db->fetchRow($select);
        return $row;
    }
    /*
	 * get exam center address by exam registered by student
	 */
	public function getExamCenterbyStudent($idSemester,$idStudentRegistration){

		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select()
	                ->from(array('er'=>$this->_name))
	                ->join(array('ec'=>'tbl_exam_center'),'ec.ec_id=er.er_ec_id',array('ec_name'))
	                ->join(array('add'=>'tbl_address'),'add.add_org_id=ec.ec_id')
	                ->join(array('c'=>'tbl_countries'),'c.idCountry=add.add_country',array('CountryName'))
	                ->join(array('s'=>'tbl_state'),'s.idState=add.add_state',array('StateName'))
	                ->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=add.add_city',array('CityName'))
	                ->where('add.add_org_name = "tbl_exam_center"')
                    ->where("er.er_registration_type in (0, 1, 4, 9)")
	                ->where('add.add_address_type = ?',615)   //permenant address
	                ->where('er_idSemester = ?',$idSemester)
	                ->where('er_idStudentRegistration = ?',$idStudentRegistration)
	                ->group('er.er_ec_id');

        $row = $db->fetchAll($select);
		return $row;
	}

    public function getDataByBerId($ber_id, $group = true, $join = true) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('er_id', 'exam_registrationID' => 'registrationId', 'ber_id', 'bes_id', 'examsetupdetail_id', 'er_registration_type'))
            ->where('a.ber_id = ?', $ber_id)
            ->where("a.er_registration_type in (0, 1, 4, 9)");

        if($join) {
            $select->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array('IdStudentRegistration', 'student_registrationID' => 'registrationId'));
            $select->join(array('c' => 'student_profile'),'c.std_id = b.sp_id');
            $select->joinLeft(array('d' => 'tbl_countries'), 'd.idCountry = c.std_nationality', array('CountryName'));
            $select->joinLeft(array('e' => 'tbl_qualificationmaster'), 'e.IdQualification = c.std_qualification', array('QualificationLevel'));
            $select->join(array('f' => 'exam_schedule'), 'a.examschedule_id = f.es_id', array('es_id', 'es_date','es_start_time', 'es_end_time'));
            $select->join(array('g' => 'batch_exam_schedule'), 'g.bes_id = a.bes_id', array('individual_schedule' => 'MAX(individual)'));
            $select->order('g.individual DESC');
            $select->order('c.std_fullname ASC');
        }


        if($group) {
            $select->group('a.student_id');
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDataByStudentId($student_id, $join = true) {
        //$db     = getDB2();
        $db     = getDB2();
        $query = $db->select()
            ->from(array('er'=>$this->_name))
            ->where('er.student_id = ?', $student_id)
            ->where("er.er_registration_type in (0, 1, 4, 9)")
            ->group("er.examsetupdetail_id");

        $result = $db->fetchAll($query);

        $exam_registration = array();

        foreach($result as $row) {
            $examsetupdetail_id = $row['examsetupdetail_id'];
            $query = $db->select()
                ->from(array('er'=>$this->_name))
                ->where('er.student_id = ?', $student_id)
                ->where('er.examsetupdetail_id = ?', $examsetupdetail_id)
                ->where("er.er_registration_type in (0, 1, 4, 9)")
                ->where("er.pass is null")
                ->order("er.createddt DESC");

            if($join) {
                $query->join(array('esd' => 'exam_setup_detail'), 'esd.esd_id=er.examsetupdetail_id', array());
                $query->join(array('es'  => 'exam_setup'), 'esd.es_id=es.es_id', array('es_name', 'es_idProgram'));
                $query->join(array('l'   => 'tbl_landscape'),'l.IdLandscape=es.es_idLandscape',array('IdStartSemester'));
                $query->join(array('i'   => 'tbl_intake'),'i.IdIntake = l.IdStartSemester',array('IntakeDesc'));
                $query->join(array('p'   => 'tbl_program'), 'p.IdProgram=es.es_idProgram', array('ProgramName', 'ProgramCode'));
                $query->join(array('pe'  => 'programme_exam'), 'esd.esd_pe_id=pe.pe_id', array('pe_id', 'pe_name'));
                $query->join(array('scd' => 'exam_schedule'), 'scd.es_id=er.examschedule_id', array('es_exam_center', 'es_date', 'es_session', 'es_start_time', 'es_end_time'));
                $query->join(array('ec'  => 'tbl_exam_center'), 'scd.es_exam_center=ec.ec_id', array('ec_name'));
                $query->join(array('def' => 'tbl_definationms'), 'scd.es_session=def.idDefinition', array('DefinitionCode as es_session_text'));
                $query->joinLeft(array('sm' => 'tbl_subjectmaster'), 'esd.esd_idSubject=sm.IdSubject', array('SubjectName','BahasaIndonesia'));
                $query->order('es_date asc');
                $query->order('es_start_time asc');
            }

            $result = $db->fetchRow($query);

            if($result) {
                $exam_registration[$examsetupdetail_id] = $result;
            }
        }

        return $exam_registration;
    }

    /*public function addData($data) {
        $auth  = Zend_Auth::getInstance();
        $data['createddt'] = new Zend_Db_Expr('UTC_TIMESTAMP()');
        $data['createdby'] = $auth->getIdentity()->id;
        //$data['createddt']  = date('Y-m-d H:i:s');
        //$data['createdby']  = $auth->getIdentity()->iduser;
        $db     = getDB2();
        $data['er_id'] = $db = $this->insert($data);

        //$db     = getDB2();
        $db     = getDB2();
        $db->insert($this->_history, $data);

        return $data['er_id'];
    }*/
    public function addData($data) {
        $db                = getDB2();
        $auth              = Zend_Auth::getInstance();
        $data['createddt'] = date('Y-m-d H:i:s');
        $data['createdby'] = $auth->getIdentity()->external_id;
        
        $db->insert($this->_name,$data);

        $erID = $db->lastInsertId();

        $query = $db->select()
            ->from(array('er' => $this->_name))
            ->where('er.er_id = ?', $erID);
        $data = $db->fetchRow($query);
        $data['remarks'] = 'New registration (LMS)';
        $data['payment_status'] = (int) $data['payment_status'];
        
        $db->insert($this->_history, $data);
        
        return $erID;

    }

    public function deleteData($id)
    {
        $db = getDB2();
        $db->delete($this->_name, $this->_primary . ' =' . (int)$id);
    }


    public function addDataHistory($data) {

        $db     = getDB2();
        unset($data['tagging_slot_id']);
        $data['payment_status'] = (int) $data['payment_status'];
        $db->insert($this->_history,$data);

        $erID = $db->lastInsertId();

        return $erID;

    }

     public function getStud($id) {
         $db = getDB2();

        $select = $db->select()
            ->from(array('a' => 'student_profile'))
            ->where('a.std_id = ?', $id);

        $row = $db->fetchRow($select);
        return $row;
    }

    public function updateData($data, $er_id, $remarks = null) {
        $db     = getDB2();
        $auth  = Zend_Auth::getInstance();

        if($auth->getIdentity()->login_as){//if admin
            $modifiedby = 1; //1= tbl_user
        }else{
            $modifiedby = $auth->getIdentity()->external_id;
        }

        $data['modifieddt']  = date('Y-m-d H:i:s');
        $data['modifiedby']  = $modifiedby;
//        $data['modifiedby']  = $auth->getIdentity()->external_id;

        $result = $db->update($this->_name, $data, "er_id = $er_id");

        $query = $db->select()
            ->from(array('er' => $this->_name))
            ->where('er.er_id = ?', $er_id);
        $data = $db->fetchRow($query);
        $data['remarks']        = $remarks;
        $data['payment_status'] = (int) $data['payment_status'];
        $db->insert($this->_history, $data);

        return $result;
    }

    public function isRegisteredbyBerId($IdStudentRegistration, $ber_id) {
        $db     = getDB2();
        $query = $db->select()
            ->from(array('a' => $this->_name), array('count' => 'COUNT(*)'))
            ->where('a.student_id = ?', $IdStudentRegistration)
            ->where('a.ber_id = ?', $ber_id)
            ->where("a.er_registration_type in (0, 1, 4, 9)")
            ->order('a.createddt DESC');

        $result = $db->fetchRow($query);

        if(!$result['count']) {
            return 0;
        }

        return 1;
    }

    public function getData($er_id, $join = true) {
        //$db     = getDB2();
        $db     = getDB2();
        $query = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.er_id = ?', $er_id);


        $result = $db->fetchRow($query);
        //pr($result);exit;
        return $result;
    }

    public function dropStudent($ber_id, $IdStudentRegistration) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('er_id'))
            ->where('a.ber_id = ?', $ber_id)
            ->where('a.student_id = ?', $IdStudentRegistration)
            ->where("a.er_registration_type in (0, 1, 4, 9)");

        $er = $db->fetchAll($select);

        foreach($er as $row) {
            $row['er_registration_type'] = 2;
            $this->updateData($row, $row['er_id']);
        }
    }

    public function getDataByParams($params = array(), $fetchAll = true) {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where("a.er_registration_type in (0, 1, 4, 9)");

        if(isset($params['ber_id']) && $params['ber_id']) {
            $select->where('a.ber_id = ?', $params['ber_id']);
        }

        if(isset($params['esd_id']) && $params['esd_id']) {
            $select->where('a.examsetupdetail_id = ?', $params['esd_id']);
        }

        if(isset($params['not-bes_id']) && $params['not-bes_id']) {
            $select->where('a.bes_id != ?', $params['not-bes_id']);
        }

        if($fetchAll) {
            return $db->fetchAll($select);
        }

        return $db->fetchRow($select);
    }

    public function getRegistration($IdStudentRegistration) {
        $db     = getDB2();
        $query = $db->select()
            ->from(array('a' => 'tbl_studentregistration'))
            ->join(array('b'=>'tbl_program_scheme'),'b.IdProgramScheme = a.IdProgramScheme',array())
            ->join(array('c'=>'tbl_definationms'),'c.idDefinition = b.mode_of_program',array('learningmode'=>'DefinitionCode'))
            ->where('a.IdStudentRegistration = ?', $IdStudentRegistration);
        $result = $db->fetchRow($query);
        return $result;
    }

    public function getDataByExamSchedule($se_id) {
        $db     = getDB2();
        $select = $db->select()
            ->from(array('a' => 'exam_schedule'))
            ->where('a.es_id = ?', $se_id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getExamRegistration($IdStudentRegistration, $IdProgram, $join = true) {
        
        $db     = getDB2();
        $query = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->where('a.student_id = ?', $IdStudentRegistration)
            ->where('a.IdProgram = ?', $IdProgram)
            ->where('a.ber_id = ?', 0); // from lms

        if($join) {
            $query->join(array('f'=>'exam_setup'),'a.examsetup_id=f.es_id');
            $query->join(array('be'=>'exam_setup_detail'),'be.esd_id = a.examsetupdetail_id',array());
        }

        $result = $db->fetchAll($query);
        return $result;
    }

    public function getExamRegistrationByStdId($std_id, $IdProgram, $join = true) {
        $db     = getDB2();
        $query = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array('IdStudentRegistration'))
            ->join(array('c' => 'student_profile'), 'c.std_id = b.sp_id', array('std_id'))
            ->where('c.std_id = ?', $std_id)
            ->where('a.IdProgram = ?', $IdProgram)
            ->where('a.ber_id = ?', 0); // from lms

        if($join) {
            $query->join(array('f'=>'exam_setup'),'a.examsetup_id=f.es_id');
            $query->join(array('be'=>'exam_setup_detail'),'be.esd_id = a.examsetupdetail_id',array());
        }

        $result = $db->fetchAll($query);
        return $result;
    }



    public function getProgram($IdProgram){

        $db     = getDB2();

        $select = $db->select()
            ->from(array('a'=>'tbl_program'))
            ->where('a.IdProgram = ?',$IdProgram);

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getDataByOrder($orderId) {
        $db     = getDB2();
        $query = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.order_id = ?', $orderId);

        $result = $db->fetchAll($query);
        return $result;
    }

    public function getDataRegistration($studentId, $IdProgram, $IdProgramScheme=0, $status = 0) {
        $db     = getDB2();
        $query = $db->select()
            ->from(array('a' => 'tbl_studentregistration'))
            ->where('a.sp_id = ?', $studentId)
            ->where('a.IdProgram = ?', $IdProgram);

        if($IdProgramScheme) {
            $query->where('a.IdProgramScheme = ?', $IdProgramScheme);
        }

        if($status) {
            $query->where('a.profileStatus = ?', $status);
        }

        $result = $db->fetchRow($query);
        return $result;
    }

    public function getProfile($externalid){ //get data tbl_studentregistration

        $db     = getDB2();

        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'))
            ->where('a.IdStudentRegistration = ?',$externalid);

        $row = $db->fetchRow($select);
        return $row;
    }

    public function addDataStudentRegistration($data) {

        $db     = getDB2();
        $db->insert('tbl_studentregistration',$data);

        $iD = $db->lastInsertId();

        $query = $db->select()
            ->from(array('a' => 'tbl_studentregistration'))
            ->where('a.IdStudentRegistration = ?', $iD);
        $data = $db->fetchRow($query);

        $db->insert('tbl_studentregistration_history', $data);

        return $iD;

    }

        public function getstudid($hash) {
         $db = getDB2();

         $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->where('a.er_id = ?', $hash);

        $row = $db->fetchRow($select);
        return $row;
    }
    
    public function getDataByErMainId($er_main_id) {
        $db = getDB2();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array())
            ->join(array('c' => 'student_profile'), 'c.std_id = b.sp_id', array('std_fullname', 'std_idnumber'))
            ->join(array('d' => 'exam_schedule'), 'd.es_id = a.examschedule_id', array('examschedule_id' => 'es_id', 'es_date', 'result_publish_date' => "CONCAT_WS(' ', es_publish_start_date, es_publish_start_time)"))
            ->join(array('f' => 'exam_setup_detail'), 'f.esd_id = a.examsetupdetail_id', array('esd_id'))
            ->join(array('g' => 'exam_setup'), 'f.es_id = g.es_id', array('examsetup_id' => 'es_id', 'es_name', 'es_idProgram'))
            ->join(array('h' => 'tbl_program'), 'h.IdProgram = g.es_idProgram', array('ProgramName', 'ProgramCode'))
            ->join(array('i' => 'programme_exam'), 'f.esd_pe_id = i.pe_id', array('pe_id', 'pe_name'))
            
            ->join(array('j' => 'exam_scheduletaggingslot'), 'j.sts_schedule_id = d.es_id', array('schedule_tagging_slot_id' => 'sts_id'))
            ->join(array('k' => 'exam_slot'), 'k.sl_id = j.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->joinLeft(array('l' => 'exam_taggingslotcenterroom'), 'l.tsc_slotid = tsc_examcenterid', array('tagging_slot_center_room_id' => 'tsc_id'))
            ->join(array('m' => 'tbl_exam_center'), 'm.ec_id = a.examcenter_id', array('exam_center_id' => 'ec_id', 'ec_name'))
            ->joinLeft(array('n' => 'tbl_examroom'), 'n.id = l.tsc_room_id', array('examroom_name' => 'name'))
            
            ->joinLeft(array('t' => 'tbl_subjectmaster'), 'f.esd_idSubject = t.IdSubject', array('IdSubject', 'SubjectName', 'SubCode'))
            ->join(array('v' => 'tbl_landscape'), 'v.IdLandscape = a.landscape_id', array())
            ->join(array('w' => 'tbl_program_scheme'), 'w.IdProgramScheme = v.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('x' => 'tbl_definationms'), 'x.idDefinition = w.mode_of_program', array('mop' => 'DefinitionDesc'))
            ->joinLeft(array('y' => 'tbl_definationms'),'a.attendance_status = y.idDefinition', array('attendance_status_desc' => 'DefinitionDesc'))
            
            ->where('a.er_main_id = ?', $er_main_id)
            ->order('c.std_fullname ASC')
            ->order('d.es_date ASC')
            ->order('k.sl_starttime ASC')
            ->group('a.er_id');
        
        
        return $db->fetchAll($select);
    }

    public function getRegistrationByStudent($icno) {
        $db = getDB2();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array())
            ->join(array('c' => 'student_profile'), 'c.std_id = b.sp_id', array('std_fullname', 'std_idnumber'))
            ->join(array('d' => 'exam_schedule'), 'd.es_id = a.examschedule_id', array('examschedule_id' => 'es_id', 'es_date', 'result_publish_date' => "CONCAT_WS(' ', es_publish_start_date, es_publish_start_time)"))
            ->join(array('f' => 'exam_setup_detail'), 'f.esd_id = a.examsetupdetail_id', array('esd_id'))
            ->join(array('g' => 'exam_setup'), 'f.es_id = g.es_id', array('examsetup_id' => 'es_id', 'es_name', 'es_idProgram'))
            ->join(array('h' => 'tbl_program'), 'h.IdProgram = g.es_idProgram', array('ProgramName', 'ProgramCode'))
            ->join(array('i' => 'programme_exam'), 'f.esd_pe_id = i.pe_id', array('pe_id', 'pe_name'))

            ->join(array('j' => 'exam_scheduletaggingslot'), 'j.sts_id = a.examtaggingslot_id', array('schedule_tagging_slot_id' => 'sts_id'))
            ->join(array('k' => 'exam_slot'), 'k.sl_id = j.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->joinLeft(array('l' => 'exam_taggingslotcenterroom'), 'l.tsc_slotid = tsc_examcenterid', array('tagging_slot_center_room_id' => 'tsc_id'))
            ->join(array('m' => 'tbl_exam_center'), 'm.ec_id = a.examcenter_id', array('exam_center_id' => 'ec_id', 'ec_name'))
            ->joinLeft(array('n' => 'tbl_examroom'), 'n.id = l.tsc_room_id', array('examroom_name' => 'name'))

            ->joinLeft(array('t' => 'tbl_subjectmaster'), 'f.esd_idSubject = t.IdSubject', array('IdSubject', 'SubjectName', 'SubCode'))
            ->join(array('v' => 'tbl_landscape'), 'v.IdLandscape = a.landscape_id', array())
            ->join(array('w' => 'tbl_program_scheme'), 'w.IdProgramScheme = v.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('x' => 'tbl_definationms'), 'x.idDefinition = w.mode_of_program', array('mop' => 'DefinitionDesc'))
            ->joinLeft(array('y' => 'tbl_definationms'),'a.attendance_status = y.idDefinition', array('attendance_status_desc' => 'DefinitionDesc'))

            ->where('c.std_idnumber = ?', $icno)
            ->group('a.er_id')
            ->order('d.es_date desc');

        return $db->fetchAll($select);
    }

    public function courseStatus($val)
    {
        switch($val)
        {
            case 0: $newval = 'Pre-Registered'; break;
            case 1: $newval = 'Registered';     break;
            case 2: $newval = 'Drop';           break;
            case 3: $newval = 'Withdraw';       break;
            case 4: $newval = 'Repeat';         break;
            case 5: $newval = 'Refer';          break;
            case 9: $newval = 'Pre-Repeat';         break;
            case 10: $newval = 'Completed';         break;
        }

        return $newval;
    }
    
 	public function getRegistrationInfo($IdStudentRegSubjects) {
        $db     = getDB2();
        $query = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.IdStudentRegSubjects = ?', $IdStudentRegSubjects)
            ->where('a.er_registration_type =?',1);

        $result = $db->fetchAll($query);
        return $result;
    }
    
 	public function getCurrentExamRegByProgram($idStudentRegistration,$program_id) {
         $db = getDB2();

       $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('f'=>'exam_setup'),'a.examsetup_id=f.es_id',array())
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = a.student_id',array('IdProgram','IdProgramScheme'))
            ->join(array('be'=>'exam_setup_detail'),'be.esd_id = a.examsetupdetail_id',array())
            ->join(array('p'=>'programme_exam'),'p.pe_id = be.esd_pe_id',array('examname'=>'pe_name','pe_createddt' => 'createddt'))
            ->join(array('g'=>'exam_schedule'),'a.examschedule_id=g.es_id', array(
                'es_date',
            	'es_publish_start_date',
                'allow_publish_result' => "DATEDIFF(CURDATE(), es_publish_start_date)"
            ))
            ->join(array('e'=>'tbl_program'),'a.IdProgram=e.IdProgram',array('ProgramName'))
            ->joinLeft(array('td'=>'tbl_programclosingdate'),'e.IdProgram=td.IdProgram AND td.category_id = 1 AND td.registration_type = 1',array('pcd_createddt' => 'createddt'))
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.landscape_id', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array('IdProgramScheme','ps_createddt' => 'createddt'))
            ->join(array('d' => 'tbl_definationms'), 'd.idDefinition = c.mode_of_program', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->joinLeft(array('a2' => 'tbl_definationms'), 'a2.idDefinition = a.attendance_status', array('attendance_status_desc' => 'DefinitionDesc'))
            ->joinLeft(array('is' => 'invoice_student'), 'is.examregistration_main_id = a.er_main_id', array('invoice_id'))
            ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegSubjects=a.IdStudentRegSubjects',array())
            ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','SubCode'))
            ->join(array('ec'=>'tbl_exam_center'),'ec.ec_id=a.examcenter_id',array('ec_name'))
            ->joinLeft(array('erm'=>'exam_registration_main'),'erm.id=a.er_main_id',array())
            ->joinLeft(array('est'=>'exam_sitting'),'erm.exam_sitting_id=est.est_id',array('est_name'))
            ->where("((a.ber_id = 0 OR a.ber_id is null) AND a.er_registration_type in (0, 1, 4, 9, 10)) OR ((a.ber_id != 0 OR a.ber_id is not null) AND a.er_registration_type in (1,4, 10))")
            
            ->where('sr.IdStudentRegistration = ?', $idStudentRegistration)
            ->where('sr.IdProgram=?',$program_id)
            //->where('g.es_date >= CURDATE()') //actual
            ->where('a.er_registration_type !=?',10) //temporary
            ->order('g.es_date ASC');
            
        $row = $db->fetchAll($select);
        return $row;
    }
    
    public function isRegistered($IdStudentRegSubjects,$est_id=null){
    	
    	 $db = getDB2();
    	 
    	 $select = $db->select()
            		  ->from(array('er' => 'exam_registration'))
            		  ->join(array('erm'=>'exam_registration_main'),'erm.id=er.er_main_id',array())
            		  ->where('er.IdStudentRegSubjects=?',$IdStudentRegSubjects);
            		  if(isset($est_id)){
            		  	$select->where('erm.exam_sitting_id=?',$est_id);
            		  }
        $row = $db->fetchRow($select);
        return $row;
    }
    
    
	public function getExamRegistrationHistory($idStudentRegistration,$program_id) {
         $db = getDB2();

       $select = $db->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('f'=>'exam_setup'),'a.examsetup_id=f.es_id')
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = a.student_id',array('IdProgram','IdProgramScheme'))
            ->join(array('be'=>'exam_setup_detail'),'be.esd_id = a.examsetupdetail_id',array())
            ->join(array('p'=>'programme_exam'),'p.pe_id = be.esd_pe_id',array('examname'=>'pe_name','pe_createddt' => 'createddt'))
            ->join(array('g'=>'exam_schedule'),'a.examschedule_id=g.es_id', array(
                '*',
                'allow_publish_result' => "DATEDIFF(CURDATE(), es_publish_start_date)"
            ))
            ->join(array('e'=>'tbl_program'),'a.IdProgram=e.IdProgram')
            ->joinLeft(array('td'=>'tbl_programclosingdate'),'e.IdProgram=td.IdProgram AND td.category_id = 1 AND td.registration_type = 1',array('pcd_createddt' => 'createddt'))
            //->join(array('d'=>'tbl_address'),'add.add_org_id=ec.ec_id') 
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.landscape_id', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array('IdProgramScheme','ps_createddt' => 'createddt'))
            ->join(array('d' => 'tbl_definationms'), 'd.idDefinition = c.mode_of_program', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->joinLeft(array('a2' => 'tbl_definationms'), 'a2.idDefinition = a.attendance_status', array('attendance_status_desc' => 'DefinitionDesc'))
            ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegSubjects=a.IdStudentRegSubjects',array())
            ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','SubCode'))
            ->join(array('ec'=>'tbl_exam_center'),'ec.ec_id=a.examcenter_id',array('ec_name'))
            ->joinLeft(array('erm'=>'exam_registration_main'),'erm.id=a.er_main_id',array())
            ->joinLeft(array('est'=>'exam_sitting'),'erm.exam_sitting_id=est.est_id',array('est_name'))
           // ->where("((a.ber_id = 0 OR a.ber_id is null) AND a.er_registration_type in (0, 1, 4, 9, 10)) OR ((a.ber_id != 0 OR a.ber_id is not null) AND a.er_registration_type in (1,4, 10))")
            
            ->where('sr.IdStudentRegistration = ?', $idStudentRegistration)
            ->where('sr.IdProgram=?',$program_id)
            //->where('g.es_date < CURDATE()') //original
            ->where('a.grade_name IS NOT NULL') //temporary for demo
            ->order('g.es_date DESC');
            
        $row = $db->fetchAll($select);
        return $row;
    }
    
    public function isPass($IdStudentRegSubjects){
    	
    	 $db = getDB2();
    	 
    	 $select = $db->select()
            		  ->from(array('er' => 'exam_registration'))
            		  ->join(array('erm'=>'exam_registration_main'),'erm.id=er.er_main_id',array())
            		  ->where('er.IdStudentRegSubjects=?',$IdStudentRegSubjects)
            		  ->where('er.pass=?',1)
            		  ->where('result_status=?',2)
            		  ->where('exam_status=?',"C");    //approved        		 
        $row = $db->fetchRow($select);
        return $row;
    }
    
    public function updateExamStatus($idStudentRegistration){
    	
    	 $db = getDB2();
    	 $db->update($this->_name,array('exam_status'=>'N'),'student_id = ' . (int)$idStudentRegistration);
    }
    
}
?>