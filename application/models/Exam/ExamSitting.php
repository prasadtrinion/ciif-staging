<?php 
class App_Model_Exam_ExamSitting extends Zend_Db_Table_Abstract
{
    protected $_name    = 'exam_sitting';
    protected $_primary = "est_id";

    public function addData($data) {
        $db = getDB2();
        $db->insert($this->_name, $data);
        $id = $db->lastInsertId();
        return $id;
    }

	public function updateData($data,$where)
    {
    	$db = getDB2();
    	$db->update($this->_name,$data,$where);
    }
    
 	public function getexamsittingonwards() {

        $db = getDB2();

        $select = $db->select()
            		 ->from(array('est'=>$this->_name))
            		 ->where('est.est_year >= ?',date('Y'))
            		 ->where('est.est_month >= ?',date('m'));

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getDataById($est_id) {

        $db = getDB2();

        $select = $db->select()
            		 ->from(array('est'=>$this->_name))
            		 ->where('est.est_id=?',$est_id);

        $result = $db->fetchRow($select);
        return $result;
    }
    
	public function getOpenExamSitting() {

        $db = getDB2();

       /* $select = $db->select()
            		 ->from(array('est'=>$this->_name))
            		 ->where('est.est_start_date <= CURDATE()')
            		 ->where('est.est_end_date <= CURDATE()');ORIGINAL*/
        //DEMO
        $select = $db->select()
            		 ->from(array('est'=>$this->_name))
            		 ->where('est.est_open =1');

        $result = $db->fetchRow($select);
        return $result;
    }
    
	public function getExamSittingByCycleId($erc_id) {

        $db = getDB2();

        $select = $db->select()
            		 ->from(array('est'=>$this->_name))
            		 ->where('est.erc_id=?',$est_id);

        $result = $db->fetchRow($select);
        return $result;
    }
    
	
}