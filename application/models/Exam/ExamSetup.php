<?php 
class App_Model_Exam_ExamSetup extends Zend_Db_Table_Abstract {
    protected $_name           = 'exam_setup';
    protected $_primary        = "es_id";
    protected $_detail_name    = 'exam_setup_detail';
    protected $_detail_primary = "esd_id";


    public function getExamOption($IdLandscape, $params = array()){
        $db     = Zend_Db_Table::getDefaultAdapter();       
        $select = $db->select()
            ->from(array('a' => $this->_name), array('examsetup_id' => 'es_id', 'es_id', 'es_name', 'es_online'))
            ->join(array('b' => $this->_detail_name), 'b.es_id = a.es_id', array('esd_id', 'esd_type', 'esd_idSubject'))
            ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array())
            ->join(array('d' => 'tbl_program'), 'd.IdProgram = a.es_idProgram', array('exam_closing_date'))
            ->where('a.es_idLandscape = ?',$IdLandscape)
            ->order('a.es_name ASC')
            ->group('a.es_id');

        if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
        }
        else {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL d.exam_closing_date DAY)');
        }

        $result = $db->fetchAll($select);

        foreach($result as $index => $row) {
            // pr($row);
            $examsetup_id = $row['examsetup_id'];

            $select = $db->select()
                ->from(array('b' => $this->_detail_name), array('esd_id'))
                ->join(array('d' => 'programme_exam'), 'b.esd_pe_id = d.pe_id', array('pe_id', 'pe_name'))
                ->join(array('e' => 'tbl_markdistribution_setup'), 'e.mds_id = b.esd_mds_id', array('mds_id', 'mds_name'))
                ->where('b.es_id = ?',$examsetup_id)
                ->where('d.IdLandscape = ?', $IdLandscape)
                ->group('b.esd_id');
            $count_papers = $db->fetchAll($select);
            $count_papers = count($count_papers);
            // pr("count_papers = $count_papers");

            $select = $db->select()
                ->from(array('b' => $this->_detail_name), array('esd_id'))
                ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array('es_id'))
                ->join(array('d' => 'programme_exam'), 'b.esd_pe_id = d.pe_id', array('pe_id', 'pe_name'))
                ->join(array('e' => 'tbl_markdistribution_setup'), 'e.mds_id = b.esd_mds_id', array('mds_id'))
                ->join(array('f' => 'tbl_program'), 'f.IdProgram = d.IdProgram', array('exam_closing_date'))
                ->where('b.es_id = ?',$examsetup_id)
                // ->where('c.es_date > ?', date('Y-m-d', strtotime('+14 days')))
                ->where('d.IdLandscape = ?', $IdLandscape)
                ->group('b.esd_id');

                if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
                    $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
                }
                else {
                    $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL f.exam_closing_date DAY)');
                }

            $count_schedule = $db->fetchAll($select);
            $count_schedule = count($count_schedule);
            // pr("count_schedule = $count_schedule");

            if($count_schedule < $count_papers) {
                unset($result[$index]);
                continue;
            }

            if($row['es_online']) {
                $result[$index]['es_online_desc'] = 'Online Examination';
            }
            else {
                $result[$index]['es_online_desc'] = 'Manual Examination';
            }

            $result[$index]['count_papers']   = $count_papers;
            $result[$index]['count_schedule'] = $count_schedule;
        }
        return $result;
    }

    public function getExamDetail($es_id, $examschedule_id = 0, $params = array()) {
        //$db = Zend_Db_Table::getDefaultAdapter();
        $db = getDB2();
        $select = $db->select()
            ->from(array('b' => $this->_detail_name), array('esd_id', 'esd_type', 'esd_idSubject'))
            ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array('examschedule_id' => 'es_id', 'es_id', 'es_exam_center','es_active'))
            ->join(array('d' => 'tbl_markdistribution_setup'), 'd.mds_id = esd_mds_id', array('mds_id', 'mds_name'))
            ->join(array('e' => 'programme_exam'), 'e.pe_id = b.esd_pe_id AND e.pe_id = d.mds_programme_exam', array('pe_id', 'pe_name'))
            ->joinLeft(array('f' => 'tbl_subjectmaster'), 'f.IdSubject = b.esd_idSubject', array('IdSubject', 'SubjectName', 'SubCode'))
            ->join(array('g' => 'tbl_program'), 'g.IdProgram = e.IdProgram', array('exam_closing_date'))
            ->where('b.es_id = ?', $es_id)
            ->where('c.es_active = 1')
            // ->where('c.es_date > ?', date('Y-m-d', strtotime('+14 days')))
            ->group('b.esd_id');

        if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
        }
        else {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL g.exam_closing_date DAY)');
        }

        if($examschedule_id) {
            $select->where('c.es_id = ?', $examschedule_id);   
        }

        $result = $db->fetchAll($select);

        foreach($result as $index => $row) {
            $select = $db->select()
                ->from(array('b' => $this->_detail_name), array())
                ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array('examschedule_id' => 'es_id', 'es_id', 'es_esd_id'))
                ->join(array('f' => 'tbl_exam_center'), 'f.ec_id = c.es_exam_center', array('ec_id', 'ec_name'))
                ->join(array('a' => $this->_name), 'a.es_id = b.es_id', array())
                ->join(array('g' => 'tbl_program'), 'g.IdProgram = a.es_idProgram', array('exam_closing_date'))
                ->where('b.es_id = ?', $es_id)
                ->where('b.esd_id = ?', $row['esd_id'])
                ->where('c.es_active = 1')
                // ->where('c.es_date > ?', date('Y-m-d', strtotime('+14 days')))
                ->group('f.ec_id');

            if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
                $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
            }
            else {
                $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL g.exam_closing_date DAY)');
            }

            $exam_centers = $db->fetchAll($select);
            $result[$index]['exam_centers']  = $exam_centers;
            $result[$index]['esd_type_desc'] = '';
            $result[$index]['module_desc']   = '';
            
            if($row['IdSubject']) {
                $result[$index]['module_desc'] = $row['SubCode'] .' — '. $row['SubjectName'];
            }

            if($row['esd_type'] == 1) {
                $result[$index]['esd_type_desc'] = 'Programme Based';
            }
            elseif($row['esd_type'] == 2) {
                $result[$index]['esd_type_desc'] = 'Modular Based';
            }
        }

        if($examschedule_id && count($result)) {
            $result = $result[0];
        }

        return $result;
    }

    public function getDataByBerId($er_id, $sp) {
        $db = getDB2();
        $select = $db->select()
            ->from(array('a' => 'exam_registration'), array('er_id','student_id','examsetup_id'))
            ->join(array('b' => 'exam_schedule'), 'b.es_id = a.examschedule_id', array('examschedule_id' =>'es_id', 'es_id', 'es_date', 'es_start_time', 'es_end_time'))
            ->join(array('c' => 'tbl_exam_center'), 'c.ec_id = b.es_exam_center', array('ec_id', 'ec_name'))
            ->join(array('f' => 'tbl_definationms'), 'f.idDefinition = b.es_session', array('es_session_desc' => 'DefinitionDesc'))
            ->join(array('g' => 'exam_setup'), 'g.es_id = b.es_examsetup_id', array('examsetup_id'=>'es_id', 'examsetup_name'=>'es_name', 'IdLandscape' => 'es_idLandscape'))
            ->join(array('h' => 'exam_setup_detail'), 'h.esd_id = b.es_esd_id', array('esd_id'))
            ->join(array('i' => 'tbl_markdistribution_setup'), 'i.mds_id = h.esd_mds_id', array('mds_id', 'mds_name'))
            ->join(array('j' => 'programme_exam'), 'j.pe_id = i.mds_programme_exam', array('pe_id', 'pe_name'))
            ->joinLeft(array('k' => 'tbl_subjectmaster'), 'k.IdSubject = h.esd_idSubject', array('IdSubject', 'SubjectName', 'SubCode'))
            ->where('a.er_id = ?', $er_id)
            ->where('a.student_id = ?', $sp)
            // 
            //->where('b.es_active = 1')
            ->order('b.es_date ASC')
            ->order('b.es_start_time ASC');

        /*if($IdStudentRegistration) {
            $select->join(array('l' => 'exam_registration'), 'l.bes_id = a.bes_id', array('er_id'));
            $select->where("l.er_registration_type in (0, 1, 4, 9)");
            $select->where('l.student_id = ?', $IdStudentRegistration);
        }
        else {
            $select->where('a.individual = ?', 0);
        }*/

        $result = $db->fetchAll($select);
        //pr($result);exit;
        return $result;
    }
}
?>