<?php 
class App_Model_Exam_ExamRegistrationMain extends Zend_Db_Table_Abstract
{
    protected $_name    = 'exam_registration_main';
    protected $_primary = "id";

    public function addData($data) {
        $db = getDB2();
        $db->insert($this->_name, $data);
        $id = $db->lastInsertId();
        return $id;
    }

    public function getData($params = array()) {
        $db     = getDB2();
        $select = $db->select()->from(array('a' => $this->_name));

        if(isset($params['id']) && $params['id']) {
            $select->where('a.id = ?', $params['id']);
        }
    
        if(isset($params['er_main_id']) && $params['er_main_id']) {
            //$select->where('a.er_main_id = ?', $params['er_main_id']);
        }

        if(isset($params['IdStudentRegistration']) && $params['IdStudentRegistration']) {
            $select->where('a.IdStudentRegistration = ?', $params['IdStudentRegistration']);
        }

        if(isset($params['IdProgram']) && $params['IdProgram']) {
            $select->where('a.IdProgram = ?', $params['IdProgram']);
        }

        if(isset($params['ber_id']) && $params['ber_id']) {
            $select->where('a.ber_id = ?', $params['ber_id']);
        }

        if(isset($params['join_studentregistration']) && $params['join_studentregistration']) {
            $select->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.IdStudentRegistration', array('std_id' => 'sp_id'));
        }

        $result = $db->fetchRow($select);
        return $result;
    }

    public function setActive($id) {
        $db   = getDB2();
        $data = array('active' => 1);

        return $db->update($this->_name, $data, "id = $id");
    }
    
 	public function getDataByStudentReg($IdStudentRegistration) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('erm'=>$this->_name))
            ->where('sr.sp_id = ?',$IdStudentRegistration);

        $result = $db->fetchRow($select);
        return $result;
        //return $row;
    }
    
	public function getCountExamSitting($IdStudentRegistration,$erc_cycle_id) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('erm'=>$this->_name))
            ->where('erm.IdStudentRegistration = ?',$IdStudentRegistration)
            ->where('erm.erc_id = ?',$erc_cycle_id);

        $result = $db->fetchAll($select);
        return $result;
    }
    
	public function getMyExamRegistration($IdStudentRegistration,$erc_id=null) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('erm'=>$this->_name))
            ->join(array('exs'=>'exam_sitting'),'erm.exam_sitting_id=exs.est_id',array('est_name'))
            ->where('erm.IdStudentRegistration = ?',$IdStudentRegistration);
            
            if($erc_id){
            	$select->where('erm.erc_id=?',$erc_id);
            }

        $result = $db->fetchAll($select);
        return $result;
    }
}