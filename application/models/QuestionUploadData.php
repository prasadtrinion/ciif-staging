<?php

class App_Model_QuestionUploadData extends Zend_Db_Table
{
    protected $_name = "quizqbs_question_upload_data";

    public function addData($data)
    {
        $id = $this->insert($data);
        return $id;
    }

    public function updateData($data, $id)
    {
        $this->_db->update($this->_name, $data, 'id = ' . (int)$id);
    }

    public function deleteData($id)
    {
        $this->_db->delete($this->_name, " id = '" . (int)$id . "'");
    }

    public function getData($id)
    {

        $select = $this->_db->select()
            ->from(array('qp' => $this->_name))
            ->where("qp.id = ?", $id);

        $result = $this->_db->fetchRow($select);

        return $result;

    }

}

?>
