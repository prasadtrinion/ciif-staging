<?php


class App_Model_StudentExemption extends Zend_Db_Table_Abstract {
    /**
     * The default table name
     */
    protected $_name = 'tbl_studentexemption';
    protected $_primary = 'IdExemption';

    public function addData($data){
        $id = $this->insert($data);
        return $id;
    }

    public function updateData($data,$id){
        $this->update($data, $this->_primary .' = '. (int)$id);
    }

    public function deleteData($id){
        $this->delete($this->_primary .' =' . (int)$id);
    }

    public function getData($id){
        $db = getDB2();

        $select = $db ->select()
            ->from(array('p'=>'tbl_studentexemption'))
            ->where("p.IdExemption = ? ",$id);

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getDataByStudRegID($registrationId, $IdProgram, $IdMajoringPathway) {
        $db = getDB2();

       $sql = $db->select()
            ->from(array('se' => 'tbl_studentexemption', array('se.IdExemption', 'se.IdSubject', 'sr.ExemptionStatus')))
            ->join(array('p'=>'tbl_program'),'p.IdProgram=se.IdProgram',array('ProgramName'))
            ->where('se.IdStudentRegistration = ?', $registrationId)
            ->where('se.IdProgram = ?', $IdProgram)
            ->where('se.IdMajoringPathway = ?', $IdMajoringPathway);
        $result = $db->fetchAll($sql);
        return $result;
    }

    public function getDataStud($registrationId, $IdProgram, $IdMajoringPathway, $IdSubject) {
        $db = getDB2();

         $sql = $db->select()
            ->from(array('se' => 'tbl_studentexemption', array('se.IdExemption', 'se.IdSubject', 'sr.ExemptionStatus')))
            ->join(array("ls"=>"tbl_landscapesubject"),'ls.IdSubject=se.IdSubject',array('SubjectType'))
            ->join(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject',array('SubjectName','SubCode','CreditHours'))
            ->join(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array('SubjectType'=>'DefinitionDesc'))
            ->join(array("e"=>"tbl_programmajoring"),'e.idProgram=ls.IdProgram AND e.IDProgramMajoring=ls.IDProgramMajoring AND se.IdMajoringPathway=e.IDProgramMajoring',array('IDProgramMajoring', 'MajoringDesc' => 'EnglishDescription'))
            ->where('se.IdStudentRegistration = ?', $registrationId)
            ->where('se.IdProgram = ?', $IdProgram)
            ->where('se.IdSubject = ?', $IdSubject)
            ->where('se.IdMajoringPathway = ?', $IdMajoringPathway);



        /*$select = $db->select()
            ->from(array("ls"=>"tbl_landscapesubject"))
            ->join(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject',array('SubjectName','SubCode','CreditHours'))
            ->join(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array('SubjectType'=>'DefinitionDesc'))
            ->join(array("e"=>"tbl_programmajoring"),'e.idProgram=ls.IdProgram AND e.IDProgramMajoring=ls.IDProgramMajoring',array('IDProgramMajoring', 'MajoringDesc' => 'EnglishDescription'))
            ->where("ls.IdProgram = ?",$programId)
            ->where("ls.IDProgramMajoring = ?",$programMajor)
            ->order("s.SubCode");*/

        $result = $db->fetchRow($sql);
        return $result;
    }

    public function getDataStudSubject($registrationId, $IdProgram, $IdMajoringPathway, $IdSubject) {
        $db = getDB2();

        $sql = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('sp_id','IdTransaction','IdMajoringPathway'))
            ->join(array("ls"=>"tbl_landscapesubject"),'ls.IdProgram=sr.IdProgram AND ls.IDProgramMajoring=sr.IdMajoringPathway',array('SubjectType'))
            ->join(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject',array('SubjectName','SubCode','CreditHours'))
            ->join(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array('SubjectType'=>'DefinitionDesc'))
            ->join(array("e"=>"tbl_programmajoring"),'e.idProgram=ls.IdProgram AND e.IDProgramMajoring=ls.IDProgramMajoring',array('IDProgramMajoring', 'MajoringDesc' => 'EnglishDescription'))
            ->where('sr.IdStudentRegistration = ?', $registrationId)
            ->where('ls.IdProgram = ?', $IdProgram)
            ->where('ls.IdSubject = ?', $IdSubject)
            ->where('ls.IDProgramMajoring = ?', $IdMajoringPathway);

        $result = $db->fetchRow($sql);
        return $result;
    }
}

?>
