<?php

class App_Model_UserGroupData extends Zend_Db_Table
{

    protected $_name = 'user_group_data';
    protected $_primary = 'gdata_id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function insertGroupData($data)
    {
        $db = $this->getDefaultAdapter();

        $group_data_id = $db->insert($this->_name, $data);

        return $db->lastInsertId();
    }

    public function getAllUser($group_id, $return_result = true)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'user_group'), 'c.group_id=a.group_id', array('*'))
            ->joinLeft(array('d' => 'user'), 'd.id=a.user_id', array('*'))
            ->where('a.group_id = ?', $group_id);

        $result = $db->fetchAll($select);

        if ($return_result) {
            return $result;
        } else {
            return $select;
        }
    }

    public function getUser($id, $group_id) {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.user_id = ?', $id)
            ->where('a.group_id = ?', $group_id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getUserGroup($user_id, $course_id) {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinInner(array('c' => 'user_group'), 'c.group_id = a.group_id', array('c.group_id', 'c.group_name'))
            ->where('a.user_id = ?', $user_id)
            ->where('c.course_id = ?', $course_id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function removeUser($id, $group_id) {
        $db = $this->getDefaultAdapter();

        $result = $db->delete($this->_name, 'user_id = '. $id .' AND group_id = ' . $group_id);
        return $result;
    }
}
