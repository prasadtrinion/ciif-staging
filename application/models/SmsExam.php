<?php

class App_Model_SmsExam extends Zend_Db_Table
{


    public function addData($data)
    {
        $db = getDB();
        $db->insert($data);
        return $db->lastInsertId();
    }

    public function updateData($data, $id)
    {
        $db = getDB();
        $db->update($data, $this->_primary . ' = ' . (int)$id);
    }

    public function deleteData($id)
    {
        $db = getDB();
        $db->delete($this->_primary . ' =' . (int)$id);
    }

    public function getExamByProgram($IdProgram, $scheme)
    {

        $db = getDB();

        $select = $db->select()
            ->from(array('a' => 'exam_setup'))
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.es_idLandscape', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array())
            ->join(array('p' => 'tbl_programclosingdate'), 'a.es_idProgram = p.IdProgram', array('ProgramName', 'ProgramCode'))
            ->join(array('s' => 'exam_schedule'), 's.es_examsetup_id = a.es_id', array())
            ->where('a.es_idProgram = ?', $IdProgram)
            ->where('c.mode_of_program = ?', $scheme);

        $row = $db->fetchAll($select);
        return $row;
    }

    public function getAvailableExam()
    {

        $db = getDB();

        $selectEnrolled = $db->select()
            ->from(array('a' => 'exam_setup'),
                array(
                    'id' => 'a.es_id',
                    'IdProgram' => 'p.IdProgram',
                    'ProgramName' => 'p.ProgramName',
                    'ProgramCode' => 'p.ProgramCode',
                    'pe_id' => 'GROUP_CONCAT(DISTINCT esd_pe_id)',

                )
            )
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.es_idLandscape', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array())
            ->join(array('p' => 'tbl_program'), 'a.es_idProgram = p.IdProgram AND p.exam_only = 0', array())
            ->join(array('s' => 'exam_schedule'), 's.es_examsetup_id = a.es_id', array())
            ->join(array('ab' => 'exam_setup_detail'), 'ab.es_id = a.es_id', array())
            ->where('p.exam_offering = ?', 1)
            ->group('a.es_idProgram');

        $selectExamOnly = $db->select()
            ->from(array('p' => 'tbl_program'),
                array(
                    'id' => 'a.es_id',
                    'IdProgram' => 'p.IdProgram',
                    'ProgramName' => 'p.ProgramName',
                    'ProgramCode' => 'p.ProgramCode',
                    'pe_id' => "IFNULL(GROUP_CONCAT(DISTINCT esd_pe_id),'')",
                )
            )
            ->join(array('a' => 'exam_setup'), 'a.es_idProgram = p.IdProgram', array())
            ->join(array('ab' => 'exam_setup_detail'), 'ab.es_id = a.es_id', array())
            ->where('p.exam_offering = ?', 1)
            ->where('p.exam_only = 1')
           // ->where('p.Active = 1')//br tmbh
            ->group('p.IdProgram');


        $select = $db->select()
            ->union(array($selectEnrolled, $selectExamOnly), Zend_Db_Select::SQL_UNION_ALL);

        $row = $db->fetchAll($select);
        return $row;
    }

    public function getAvailableExamByUser($sp)
    {

        $db = getDB();

        $selectRegis = $db->select()
            ->from(array('st' => 'tbl_studentregistration'), array('IdProgram'))
            ->where('st.sp_id = ?', $sp);
        $rsRegis = $db->fetchRow($selectRegis);

        $selectEnrolled = $db->select()
            ->from(array('a' => 'exam_setup'),
                array(
                    'id' => 'a.es_id',
                    'IdProgram' => 'p.IdProgram',
                    'ProgramName' => 'p.ProgramName',
                    'ProgramCode' => 'p.ProgramCode',
                    'pe_id' => 'GROUP_CONCAT(DISTINCT esd_pe_id)',

                )
            )
            ->join(array('b' => 'tbl_landscape'), 'b.IdLandscape = a.es_idLandscape', array())
            ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = b.IdProgramScheme', array())
            ->join(array('p' => 'tbl_program'), 'a.es_idProgram = p.IdProgram AND p.exam_only = 0', array())
            ->join(array('s' => 'exam_schedule'), 's.es_examsetup_id = a.es_id', array())
            ->join(array('ab' => 'exam_setup_detail'), 'ab.es_id = a.es_id', array())
            ->where('p.exam_offering = ?', 1)
            ->where('p.IdProgram = ?', $rsRegis)
            ->group('a.es_idProgram');
$rowssd = $db->fetchAll($selectEnrolled);

        $selectExamOnly = $db->select()
            ->from(array('p' => 'tbl_program'),
                array(
                    'id' => 'a.es_id',
                    'IdProgram' => 'p.IdProgram',
                    'ProgramName' => 'p.ProgramName',
                    'ProgramCode' => 'p.ProgramCode',
                    'pe_id' => "IFNULL(GROUP_CONCAT(DISTINCT esd_pe_id),'')",
                )
            )
            ->join(array('a' => 'exam_setup'), 'a.es_idProgram = p.IdProgram', array())
            ->join(array('ab' => 'exam_setup_detail'), 'ab.es_id = a.es_id', array())
            ->where('p.exam_offering = ?', 1)
            ->where('p.exam_only = 1')
            ->where('p.IdProgram = ?', $rsRegis)
            // ->where('p.Active = 1')//br tmbh
            ->group('p.IdProgram');


        $select = $db->select()
            ->union(array($selectEnrolled, $selectExamOnly), Zend_Db_Select::SQL_UNION_ALL);

        $row = $db->fetchAll($select);
        return $row;
    }

    public function getTransactionMigs($orderId)
    {

        $db = getDB();

        $select = $db->select()
            ->from(array('a' => 'transaction_migs'))
            ->where('a.mt_external_id = ?', $orderId);

        $row = $db->fetchRow($select);
        return $row;
    }

     public function getTransMigs($id) {
        //FOR DEVELOPMENT USE ONLY
        $db = getDB();

        $select = $db->select()
            ->from(array('a' => 'transaction_migs'))
            ->where('a.mt_id = ?', $id);

        $row = $db->fetchRow($select);
        return $row;
    }

      public function updateDataOrder($data, $order_id) {
        //FOR DEVELOPMENT USE ONLY
        $db = $this->getDefaultAdapter();
        $data['status']  = 'ACTIVE';
        $result = $db->update('order', $data, "id = $order_id");

        $db2 = getDB();

        $select = $db2->select()
            ->from(array('a' => 'exam_registration'))
            ->join(array('b' => 'exam_schedule'), 'a.examsetup_id = b.es_examsetup_id',array('es_examsetup_id','es_exam_center'))
            ->where('a.order_id = ?', $order_id);

        $row = $db2->fetchAll($select);
        //pr($row);exit;
        $exid = ['er_id'];
        //$db2     = getDB2();
        foreach ($row as $er){
                $temp['er_registration_type'] = 1;

                    if($er['examroom_id'] == 0) {
                    //get room id
                    $examRoomDB = new App_Model_Exam_ExamRoom;
                    $examroom_id = $examRoomDB->getRoom($er['examschedule_id'], $er['es_exam_center']);
                    }
                    if($examroom_id == -1){
                        throw new Exception("Slots are fully booked. Please select other schedule."); 
                    }

                $temp['examroom_id'] = $examroom_id;

                $result2 = $db2->update('exam_registration', $temp, 'er_id = ' . $er["er_id"]);
        }
            
        return $result2;
    }

    public function getExamSetup($examsetupid, $IdProgram) {
        $db = getDB();

        $select = $db->select()
            ->from(array('a' => 'exam_schedule'), array('es_date'=>'es_date'))
           ->join(array('b' => 'tbl_programclosingdate'), 'b.IdProgram = a.es_course', array('days' => 'IFNULL(days, 14)'))
            ->where('a.es_id = ?', $examsetupid)
            ->where('a.es_course= ?', $IdProgram)
            ->where('b.category_id= ?', 1)
            ->where('b.registration_type= ?', 1);

        $row = $db->fetchRow($select);
        return $row;

    }
    
	public function addLogData($table,$data)
    {
        $db = getDB();
        $db->insert($table,$data);
    }

    public function getExamList($sp_id) {
        $db = getDB();

        $select = $db->select()
            ->from(array('a' => 'tbl_studentregistration'))
            ->join(array('b' => 'tbl_studentregsubjects'),'a.IdStudentRegistration = b.IdStudentRegistration', array('IdStudentRegSubjects','IdStudentRegistration'))
            ->join(array('c' => 'tbl_subjectmaster'),'b.IdSubject = c.IdSubject',array('IdSubject','SubjectName','SubCode'))
            ->where('a.sp_id = ?', $sp_id)
            ->group('b.IdStudentRegSubjects')
        ;

        $row = $db->fetchAll($select);
        return $row;
    }

    public function getRegisteredExamList($sp_id, $arrayCourse) {
        $db = getDB();

        $select = $db->select()
            ->from(array('a' => 'tbl_studentregistration'), array('a.IdStudentRegistration'))
            ->join(array('b' => 'tbl_studentregsubjects'),'a.IdStudentRegistration = b.IdStudentRegistration', array('IdStudentRegSubjects'))
            ->join(array('c' => 'tbl_subjectmaster'),'b.IdSubject = c.IdSubject',array('IdSubject','SubjectName','SubCode'))
            ->where('a.sp_id = ?', $sp_id)
            ->where('b.ExemptionStatus = ?', 0)
            ->where('b.IdSubject IN (?)', $arrayCourse)
            ->group('b.IdStudentRegSubjects')
        ;

        $row = $db->fetchAll($select);
        return $row;
    }
}