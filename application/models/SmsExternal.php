<?php

class App_Model_SmsExternal extends Zend_Db_Table
{


    /*public function addData($data)
    {
        $db = getDB2();
        $db->insert($data);
        return $db->lastInsertId();
    }

    public function updateData($data, $id)
    {
        $db = getDB2();
        $db->update($data, $this->_primary . ' = ' . (int)$id);
    }

    public function deleteData($id)
    {
        $db = getDB2();
        $db->delete($this->_primary . ' =' . (int)$id);
    }*/

    public function getProgramInfo($id)
    {

        $db = getDB2();
        $id = (int) $id;

        if($id!=0){
            $select = $db->select()
                ->from(array('a'=> 'tbl_program'))
                ->where( 'a.IdProgram = ?', $id);

            $row = $db->fetchRow($select);
            return($row);
        }else{
            $select = $db ->select()
                ->from(array('a'=> 'tbl_program'))
                ->order('a.ArabicName ASC');

            $stmt = $db->query($select);
            $rows = $stmt->fetchAll();
            return($rows);
        }

        return true;
    }
    public function getCourseInfo($id)
    {

        $db = getDB2();
        $id = (int) $id;

        if($id!=0){
            $select = $db->select()
                ->from(array('a'=>  'tbl_subjectmaster'))
                ->where( 'a.IdSubject = ?', $id);

            $row = $db->fetchRow($select);
            return($row);
        }else{
            $select = $db ->select()
                ->from(array('a'=> 'tbl_subjectmaster'));

            $stmt = $db->query($select);
            $rows = $stmt->fetchAll();
            return($rows);
        }

        return true;
    }
}