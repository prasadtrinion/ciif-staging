<?php

class App_Model_Quizattempt extends Zend_Db_Table
{

    protected $_name = 'quiz_attempt';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
    
       public function getCourses($id,$cid,$order = 'a.created_date DESC',$results=false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_attempt'))
            ->joinLeft(array('b' => 'user'), 'b.id=a.user_id',array('b.id as u_id','firstname','email'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('title','code'))
            ->joinLeft(array('d' => 'content_block_data'), 'd.data_id=a.data_id', array('gradepoint','title'))
            ->where('a.course_id = ?', $id)
            ->where('d.data_id = ?', $cid)
            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }

         public function getTitle($cid)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'content_block_data'))
            ->where('a.data_id = ?', $cid)
            ->order('data_id');
            
        $result = $db->fetchRow($select);

            return $result;
    }

    public function getAttempt($id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.id = ?', $id)
            ->order('id');
            
        $result = $db->fetchRow($select);

        return $result;
    }

    public function getuAttempt($user,$cid)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.user_id = ?', $user)
            ->where('a.course_id = ?', $cid)
            //->where('a.id = ?', $atid)
            ->order('id DESC');
            
        $result = $db->fetchRow($select);

        return $result;
    }

    public function gettotalmark($atid)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_answer'),array('id','attempt_id','sum(mark) as totalmark'))
            // ->where('a.data_id = ?',$id)
            ->where('a.attempt_id = ?',$atid);
        $result = $db->fetchRow($select);

        //update total mark
        $data['total_mark'] =  $result['totalmark'] ;
        $result2 = $db->update('quiz_attempt', $data, 'id = ' . $atid);
        return $result2;

    }

   public function updatestat($atid)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_attempt'))
            ->where('a.id = ?',$atid);
        $result = $db->fetchAll($select);
       
        //update total mark
        $data['status'] = 1 ;
        $data['date_completed'] = new Zend_Db_Expr('UTC_TIMESTAMP()');
        $result2 = $db->update('quiz_attempt', $data, 'id = ' . $atid);
         //pr($result);exit;
        return $result2;
    }

    public function getallAttempt($user,$bid)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->where('a.user_id = ?', $user)
            ->where('a.data_id = ?', $bid)
            ->order('id ASC');
            
        $result = $db->fetchAll($select);

        return $result;
    }

      public function updategrade($atid,$point,$percent)
    {
        $db = $this->getDefaultAdapter();

        //update total mark
        $data['total_grade'] = $point ;
        $data['percent'] = $percent ;
        $result2 = $db->update('quiz_attempt', $data, 'id = ' . $atid);
        return $result2;
    }
   
}
