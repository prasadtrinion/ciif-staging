<?php

class App_Model_UserScorm extends Zend_Db_Table
{

    protected $_name = 'user_scorm';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getData(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }
        $result = $db->fetchRow( $select );

        return $result;
    }

    public function getDataAll(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }

        $result = $db->fetchAll( $select );

        return $result;
    }

    public function getUserData($scorm_id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>$this->_name))
            ->join(array('c'=>'user'),'c.id=a.user_id',array('c.firstname','c.lastname','c.email'))
            ->where('data_id = ?', $scorm_id);

        $result = $db->fetchAll( $select );

        return $result;
    }

    public function checkForCompleted($user_id, $scorm_id) {
        $db     = $this->getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'data_scorm'), 'b.id = a.data_id', array())
            ->where('a.user_id = ?', $user_id)
            ->where('b.data_id = ?', $scorm_id);
            // ->where('status = ?', 'completed');
        $result = $db->fetchRow($select);

        if ($result && $result['status'] == 'completed')
        {
            return 1;
        }
        if ($result && $result['status'] != 'completed')
        {
            return 0;
        }
        else 
        {
            // $user_scorm = array(
            //     'user_id'      => $user_id,
            //     'data_id'      => $scorm_id,
            //     'status'       => 'incomplete',
            //     'duration'     => 0,
            //     'attempts'     => 0,
            //     'score'        => null,
            //     'lastaccessed' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
            //     'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()')
            // );
            // $this->insert($user_scorm);
            return 0;
        }
    }

    public function checkForCompletedUser($scorm_id, $params = array())
    {
        $db     = $this->getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'data_scorm'), 'b.id = a.data_id', array('scorm_id' => 'data_id'))
            ->join(array('c' => 'user'), 'c.id = a.user_id', array('user_id'=>'id', 'firstname', 'lastname', 'nationality'))
            ->where('b.data_id = ?', $scorm_id);

        if (isset($params['nationality']) && $params['nationality'])
        {
            $select->where('c.nationality = ?', $params['nationality']);
        }

        $result = $db->fetchAll($select);

        $data = array();
        foreach ($result as $row)
        {
            $user_id = $row['user_id'];
            $data[$user_id] = $row;
        }

        return $data;
    }
}
