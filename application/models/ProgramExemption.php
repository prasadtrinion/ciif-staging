<?php

class App_Model_ProgramExemption extends Zend_Db_Table
{

    protected $_name = 'tbl_exempted_program';
    protected $_primary = 'id_ep';
    private static $tree = array();
    private static $html = '';

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

   public function get_exemption_fee(){
    $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
        ->from(array('pf' => 'tbl_program_category'))
       
        ->join(array('c' => 'tbl_member_register_program'), 'c.program_route = pf.program_route and c.program_level = pf.program_level')
        ->join(array('d' => 'tbl_program_fee'), 'c.program_route = pf.program_route and c.program_level = pf.program_level')
        ->join(array('d' => 'tbl_exempted_program'), 'c.program_route = pf.program_route and c.program_level = pf.program_level')

       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = c.program_level', array('level'=>'DefinitionDesc'))
    
    ->where('c.mr_id = ?', $userid);


    $result = $db->fetchAll($select);
   
    return $result;
   }

   
public function get_amount($id){
             $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
        ->from(array('pf' => 'tbl_program_exemptionfee'))
        ->where('program_category ='.$id);
    $result = $db->fetchRow($select);
    return $result;   
}

public function get_exem_qualification($id){
             $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
        ->from(array('pf' => 'tbl_exempted_program'))
        ->join(array('pc' => 'tbl_program_category'), 'pc.id = pf.program_id ')
        ->where('pf.user_id ='.$id);
    $result = $db->fetchAll($select);
    return $result;   
}
    
}
