<?php

class App_Model_MemberFeeStructure extends Zend_Db_Table
{

    protected $_name = 'tbl_member_fee_structure';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

     
    public function getFeeStructure($memberid,$subtypeid,$nationality,$degree)
    {
        


        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT amount AS `totalamount`, `d`.`DefinitionDesc` AS `Feetype`, `e`.`DefinitionDesc` AS `Currency` FROM `tbl_member_fee_structure` AS `a` INNER JOIN `vw_feetypes` AS `d` ON d.idDefinition=a.id_fees INNER JOIN `vw_currencylist` AS `e` ON e.idDefinition=a.id_currency AS `e` ON z.idDefinition=a.id_degree WHERE (a.id_member = '$memberid') AND (a.id_member_subtype = '$subtypeid')AND(a.id_degree = '$degree') AND (a.nationality = '$nationality') GROUP BY `a`.`nationality`
                 ";
        $result = $db->fetchAll($sql);
        return $result; 

    }


    public function getTotalamount($memberid,$subtypeid,$nationality)
    {
        // echo $memberid."=>".$subtypeid."=>".$nationality;
        //     exit();
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ),array('calculated_amount as totalamount'))
    
    ->join(array("d"=>"tbl_definationms"),'d.idDefinition=a.id_currency',array('Currency'=>'DefinitionDesc'))
            
            ->where('a.id_member = ?', $memberid)
            ->where('a.id_member_subtype = ?', $subtypeid)
            ->where('a.nationality = ?', $nationality);

        $result = $db->fetchAll($select);


        return $result;
    }


    public function getMemberamount($memberid,$subtypeid,$nationality)
    {
        // echo $memberid."=>".$subtypeid."=>".$nationality;
        //     exit();
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
    
    ->join(array("d"=>"tbl_definationms"),'d.idDefinition=a.id_currency',array('Currency'=>'DefinitionDesc'))
            
            ->where('a.id_member = ?', $memberid)
            ->where('a.id_member_subtype = ?', $subtypeid)
            ->where('a.nationality = ?', $nationality);
        $result = $db->fetchRow($select);


        return $result;
    }

    public function getCurrencyRate()
    {
        
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array("a"=>"tbl_currency_rate"))
            ->where('a.cr_effective_date < ?', date('Y-m-d'))
            ->order('a.cr_effective_date DESC');

        $result = $db->fetchRow($select);
        


        return $result;
    }

    public function getamount($memberid,$nationality,$degree)
    {
        // echo $memberid."=>".$subtypeid."=>".$nationality;
        //     exit();
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ),array('calculate_fee as totalamount'))
    
    ->join(array("d"=>"tbl_definationms"),'d.idDefinition=a.id_currency',array('Currency'=>'DefinitionDesc'))
            
            ->where('a.id_member = ?', $memberid)
            ->where('a.id_member_subtype = 0')
            ->where('a.nationality = ?', $nationality);

        $result = $db->fetchAll($select);


        return $result;
    }

    
    
}

?>