<?php
/*
 * model to track revision of data around the system
 *
 */
class App_Model_History extends Zend_Db_Table
{

    protected $_name = 'data_history';
    protected $_primary = 'history_id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
}
