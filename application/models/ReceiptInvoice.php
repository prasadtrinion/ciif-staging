<?php
/**
 * @copyright Copyright (c) 2015, MTCSB
 */
class App_Model_ReceiptInvoice extends Zend_Db_Table_Abstract {
    /**
     * The default table name
     */
    protected $_name = 'receipt_invoice';
    protected $_primary = "rcp_inv_id";


    public function getDataFromReceipt($receipt_id, $invoice_id=null){
        $db = getDB2();

        $selectData = $db->select()
            ->from(array('ri'=>$this->_name))
            ->where('ri.rcp_inv_rcp_id = ?', $receipt_id)
            ->order('ri.rcp_inv_rcp_no DESC');

        if($invoice_id!=null){
            $selectData->where('ri.rcp_inv_invoice_id = ?', $invoice_id);
        }

        $row = $db->fetchAll($selectData);

        return $row;
    }

    public function insert(array $data){

        $auth = Zend_Auth::getInstance();

        if(!isset($data['rcp_inv_create_by'])){
            $data['rcp_inv_create_by'] = 665;
        }

        $data['rcp_inv_create_date'] = date('Y-m-d H:i:s');

        return parent::insert($data);
    }
    
    public function getDataByInvoice($invoice_id)
    {
        $db = getDB2();
        $select = $db->select()
            ->from(array('a' => 'receipt_invoice'), array())
            ->join(array('b' => 'receipt'), 'b.rcp_id = a.rcp_inv_rcp_id')
            ->join(array('c' => 'tbl_currency'), 'c.cur_id = b.rcp_cur_id')
            ->where('a.rcp_inv_invoice_id = ?', $invoice_id)
            ->where('b.rcp_status = ?', 'APPROVE');
        $result = $db->fetchAll($select);
        return $result;
    }
}