<?php

class App_Model_DataScorm extends Zend_Db_Table
{

    protected $_name = 'data_scorm';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getData(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }
        $result = $db->fetchRow( $select );

        return $result;
    }

    public function getDataAll(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }

        $result = $db->fetchAll( $select );

        return $result;
    }

    public function getDataByCourse($course_id, $grouping = true)
    {
        $db     = $this->getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('a.id', 'b.course_id', 'b.data_id'))
            ->join(array('b' => 'content_block_data'), "b.data_id = a.data_id", array('data_type', 'a.title'))
            ->join(array('c' => 'content_block'), "c.id = b.block_id", array())
            ->joinLeft(array('d' => 'user_group_mapping'), "d.mapped_id = b.data_id AND d.course_id = b.course_id AND d.map_type = 'content'", array())
            ->joinLeft(array('e' => 'user_group'), "e.group_id = d.group_id AND e.course_id = b. course_id", array("group_id" => "coalesce(e.group_id, 0)", 'e.group_name'))
            ->where('b.course_id = ?', $course_id)
            ->where('b.data_type = ?', 'scorm')
            ->order('e.group_id');
        $result = $db->fetchAll($select);

        $scorms = array();

        if ($grouping)
        {
            foreach ($result as $row)
            {
                $group_id = $row['group_id'];
                $data_id  = $row['data_id'];

                if (!isset($scorms[$data_id]))
                {
                    unset($row['group_id'], $row['group_name']);
                    $row['groups'] = array();
                    $scorms[$data_id] = $row;
                }

                if ($group_id)
                {
                    $scorms[$data_id]['groups'][] = $group_id;
                }
            }

            return $scorms;
        }

        return $result;

    }
}
