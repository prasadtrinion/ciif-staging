<?php

class App_Model_ProgramCategory extends Zend_Db_Table
{

    protected $_name = 'tbl_program_category';
    protected $_primary = 'id';
    private static $tree = array();
    private static $html = '';

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

   
    public function getbyMember($id,$memberId)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'membership_qualification'))
    
         ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pc.program_route', array('route'=>'DefinitionDesc'))
        ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pc.program_level', array('level'=>'DefinitionDesc'))

        ->join(array('pr' => 'tbl_member_register_program'), 'pr.program_level != pc.program_level and pr.program_level != pc.program_level')

         ->where('pr. mr_id = ?',$id)
       ->where('pc. member_type = ?',$memberId);
       $result = $db->fetchAll($select);
    
    return $result;
    }

   
    public function get_program_details($id)
    
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pr' => 'tbl_member_register_program'))
    ->join(array('pc' => 'tbl_program_category'), 'pc.program_route = pr.program_route')
    // ->join(array('pc1' => 'tbl_program_category'), 'pc.name = pr.program')
    ->join(array('pc2' => 'tbl_program_category'), 'pc2.program_level = pr.program_level')
    ->where('pr.mr_id = ?', $id);
    $result = $db->fetchAll($select);
        return $result;
    }

    public function getProgram(){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_category'))
    
      ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pc.program_route', array('route'=>'DefinitionDesc'))
       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pc.program_level', array('level'=>'DefinitionDesc'))
       ->join(array('defm' => 'tbl_definationms'), 'defm.idDefinition = pc.mode_of_program', array('mode'=>'DefinitionDesc'));
       $result = $db->fetchAll($select);
    
    return $result;
    }


     public function getprogramdata($route,$level){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_category'))
     ->where('pc.program_route = ?', $route)
    ->where('pc.program_level = ?', $level);
    
      
       $result = $db->fetchAll($select);
   
    return $result;
    }


    public function getValidProgram($route,$level,$name){
       $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_category'))
     ->where('pc.program_route = ?', $route)
     ->where('pc.name = ?', $name)
    ->where('pc.program_level = ?', $level);
    
      
       $result = $db->fetchAll($select);
   
    return $result;
    }

    public function getByProgram($route,$level,$id){
       $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
      ->from(array('pc' => 'tbl_member_register_program'))
      ->where('pc.program_route = ?', $route)
      ->where('pc.program_level = ?', $level)
      ->where('pc.mr_id = ?',$id);
    
      
       $result = $db->fetchAll($select);
   
    return $result;
    }

    public function programvaliadiate($route,$level,$category){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_fee'))
     ->where('pc.program_route = ?', $route)
     ->where('pc.program_category = ?', $category)
    ->where('pc.program_level = ?', $level);
    
      
       $result = $db->fetchAll($select);
   
    return $result;
    }

    public function getProgramSearch($formdata){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_category'))
    
      ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pc.program_route', array('route'=>'DefinitionDesc'))
       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pc.program_level', array('level'=>'DefinitionDesc'))
       ->join(array('defm' => 'tbl_definationms'), 'defm.idDefinition = pc.mode_of_program', array('mode'=>'DefinitionDesc'));


       if (isset($formdata['program_route']) && $formdata['program_route']!=0){
       
              
            $select->where(('pc.program_route ='.$formdata['program_route']));
                
            }
            if (isset($formdata['program_level']) && $formdata['program_level']!=0){
 
            $select->where(('pc.program_level ='.$formdata['program_level']));
                
            }
            if (isset($formdata['mode_of_program']) && $formdata['mode_of_program']!=0){
                
            $select->where(('pc.mode_of_program ='.$formdata['mode_of_program']));
                
            }

       $result = $db->fetchAll($select);
   
    return $result;
    }

    public function get_program_fee($route,$level)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

    $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_category'))
    ->join(array('pf' => 'tbl_program_fee'), 'pf.program_category = pc.id')
     ->join(array('d' => 'tbl_program_exemptionfee'), 'd.program_route = pc.program_route and pc.program_level = pc.program_level and .d.program_category = pc.id', array('exemption_amt'=>'calculated_amount'))
      ->join(array('def' => 'tbl_definationms'), 'def.idDefinition = pf.amount_type')
     ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pf.program_route', array('route'=>'DefinitionDesc'))
  ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pf.program_level', array('level'=>'DefinitionDesc'))
    ->where('pf.program_route = ?', $route)
    ->where('pf.program_level = ?', $level);

    $result = $db->fetchAll($select);
 
    return $result;
    }

     public function modules_exemption($route,$level)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
        ->from(array('pf' => 'tbl_program_category'))
       
       // ->join(array('c' => 'tbl_member_register_program'), 'c.program_route = pf.program_route and c.program_level = pf.program_level and c.program = pf.id')
        ->join(array('d' => 'tbl_program_exemptionfee'), 'd.program_route = pf.program_route and d.program_level = pf.program_level and d.program_category = pf.id')

         ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = c.program_level', array('level'=>'DefinitionDesc'))
      
    ->where('pf.program_route = ?', $route)
    ->where('pf.program_level = ?', $level);


    $result = $db->fetchAll($select);
   
   
    return $result;
    }

public function get_amount($route,$level)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_category'), array('sum(amount) as sum'))
    ->join(array('pf' => 'tbl_program_fee'), 'pf.program_category = pc.id')
     ->join(array('def' => 'tbl_definationms'), 'def.idDefinition = pf.amount_type')
      ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pf.program_route', array('route'=>'DefinitionDesc'))
       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pf.program_level', array('level'=>'DefinitionDesc'))
    ->where('pf.program_route = ?', $route)
    ->where('pf.program_level = ?', $level)
    ->where('pc.active = 1');

    $result = $db->fetchAll($select);
   
    return $result;
    }




    public function get_program_code($route)
    {

         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_definationms'))

    ->where('pc.idDefinition = ?', $route);

    $result = $db->fetchRow($select);
    return $result;
    }
}
