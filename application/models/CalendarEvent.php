<?php

class App_Model_CalendarEvent extends Zend_Db_Table
{

    protected $_name = 'calendar_event';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
    public function get_cpd_event(){
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'))
            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }
    

    public function getWiki(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'));

        foreach ($where as $what => $value) {
            $what = $what == 'id = ?' ? 'a.id = ?' : $what;
            $select->where($what, $value);
        }

        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getWikis($order = 'a.created_date ASC',$results=false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'))
            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }

    public function getEvents($user=false, $user_id = 0)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'))
            ->order('a.date_start DESC');

        if ($user) {
            $select->where('a.user_id = ?', $user_id);
        } else {
            $select->where('a.user_id = 0');
        }

        return $select;
    }

    public function getEvent($date)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'))
            ->where('a.date_start IN (?)', $listData2)
            ->order('a.date_start DESC');

        $result = $db->fetchAll($select);
    }

    public function getEventsByDuration($date_start, $date_end, $user=false, $user_id=0, $course_id=0)
    {
        $db = $this->getDefaultAdapter();
        $duration = CMS_Common::getDuration($date_start, $date_end);

        $dates = array();
        for ($i=0; $i<sizeof($duration); $i++)
        {
            $dates[] = $duration[$i];
        }

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'))
            ->where('a.date_start IN (?) OR a.date_end IN (?)', $duration)
            ->order('a.date_start DESC');

        if ($user) {
            $select->where('a.user_id = ?', $user_id);
        } else {
            $select->where('a.user_id = 0');
            $select->where('a.course_id = ?', $course_id);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

}
