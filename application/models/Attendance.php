<?php
class App_Model_Attendance extends Zend_Db_Table_Abstract
{
    protected $_name = 'pregraduate_list';

    private $db;

    public function getPregrad($student_id)
    {
        $db = getDB2();
        $select = $db->select()
            ->from(array('a'=>$this->_name))
            ->where('idStudentRegistration = ?',  $student_id);

        $results = $db->fetchRow($select);

        return $results;
    }
}