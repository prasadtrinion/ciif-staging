<?php
class App_Model_Invitation extends Zend_Db_Table_Abstract {
    /**
     * The default table name
     */
    protected $_name = 'pregraduate_list';
    protected $_primary = "id";

    public function getInfo($idStudentRegistration){

        $db = getDB2();

        $selectData = $db->select()
            ->from(array('pgl'=>$this->_name))
            ->join(array('sr' => 'tbl_studentregistration'),'sr.IdStudentRegistration=pgl.idStudentRegistration')
            ->join(array('p' => 'tbl_program'), "p.IdProgram = sr.IdProgram", array("IdScheme"))
            ->where('pgl.idStudentRegistration = ?',$idStudentRegistration);

        $row = $db->fetchRow($selectData);
        return $row;
    }


    public function getGraduateInfo($idStudent) {

        $db = getDB2();

        $selectData = $db->select()
            ->from(array('pgl'=>$this->_name))
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.IdStudentRegistration', array('registrationId', 'IdProgram'))
            ->join(array('sp'=>'student_profile'),'sp.std_id = sr.sp_id', array('student_name'=>'sp.std_fullname'))
            ->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName','ProgramCode'))
            ->where('sp.std_id = ?', $idStudent);
            //->where('pgl.status=3');

        $row = $db->fetchAll($selectData);

        return $row;
    }

    public function getGraduateProfile($idStudentRegistration)
    {
        $db = getDB2();
        $select = $db->select()
            ->from(array('a'=>$this->_name))
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=a.idStudentRegistration')
            //->join(array('sp'=>'student_profile'),'sp.std_id=sr.sp_id',array('student_name'=>'CONCAT_WS(" ",appl_fname,appl_lname)','appl_idnumber','appl_phone_mobile','appl_address1','appl_address2','appl_address3','appl_postcode','appl_email_personal','appl_city','appl_state','appl_city_others','appl_state_others'))
            ->join(array('sp'=>'student_profile'),'sp.std_id=sr.sp_id',array('student_name'=>'sp.std_fullname','std_idnumber','std_contact_number','std_region','std_remark','std_location', 'std_email'))
            //->join(array('c'=>'tbl_countries'),'c.idCountry=sp.appl_country',array('CountryName'))
            //->joinLeft(array('s'=>'tbl_state'),'s.idState=sp.appl_state',array('StateName'))
            //->joinLeft(array('ct'=>'tbl_city'),'ct.idCity=sp.appl_city',array('CityName'))
            ->where('sp.std_id = ?', $idStudentRegistration)
            ->where('sr.grad_status = 0');
        //exit;
        $results = $db->fetchRow($select);


        return $results;
    }

    public function getData($formData=0, $search=false){

        $db = getDB2();

        $selectData = $db->select()
            ->from(array('pgl'=>$this->_name))
            ->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = pgl.shortlisted_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
            ->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = pgl.approve_by', array('add_by_name2'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
            ->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = pgl.graduate_by', array('add_by_name3'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"))
            ->joinLeft(array('c'=>'convocation'), 'pgl.c_id=c.c_id', array('c.c_year', 'c.c_date_from', 'c.c_date_to', 'c.c_session'))
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.idStudentRegistration')
            ->join(array('sp'=>'student_profile'),'sp.std_id = sr.sp_id', array('student_name'=>'std_fullname'))
            ->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName','ProgramCode'))
            //->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDefaultLanguage'))
            //->where('pgl.status = 2')
            ->order('sp.std_fullname')
            ->order('sr.registrationId');

        if(isset($formData) && $formData!=null){

            if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
                $selectData->where('sr.IdProgram = ?',$formData['IdProgram']);
            }

            /*if(isset($formData['IdIntake']) && $formData['IdIntake']!=''){
                 $selectData->where('sr.IdIntake = ?',$formData['IdIntake']);
             }*/

            if(isset($formData['studentId']) && $formData['studentId']!=''){
                $selectData->where('sr.registrationId LIKE ?','%'.$formData['studentId'].'%');
            }

            if(isset($formData['c_id']) && $formData['c_id']!=''){
                $selectData->where('pgl.c_id = ?',$formData['c_id']);
            }

            $selectData->where('pgl.status = ?',$formData['Status']);

        }else{
            $selectData->where('pgl.status = 2'); //Default
        }

        if ($search != false){
            if (isset($search['program']) && $search['program']!=''){
                $selectData->where('sr.IdProgram = ?', $search['program']);
            }
        }

        $row = $db->fetchAll($selectData);
        return $row;

    }

    public function getApprovedGraduate($formData=0){

        $db = getDB2();

        $selectData = $db->select()
            ->from(array('pgl'=>$this->_name))
            ->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = pgl.shortlisted_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
            ->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = pgl.approve_by', array('add_by_name2'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
            ->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = pgl.graduate_by', array('add_by_name3'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"))
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.idStudentRegistration')
            ->join(array('sp'=>'student_profile'),'sp.std_id = sr.sp_id', array('student_name'=>'std_fullname'))
            ->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName','ProgramCode'))
            //->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDesc'))
            ->where ('pgl.status= ?', $formData['status'])
            ->order('sp.std_fullname')
            ->order('sr.registrationId');

        if(isset($formData) && $formData!=null){

            if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
                $selectData->where('sr.IdProgram = ?',$formData['IdProgram']);
            }
            /*
           if(isset($formData['IdIntake']) && $formData['IdIntake']!=''){
                $selectData->where('sr.IdIntake = ?',$formData['IdIntake']);
            }*/

            if(isset($formData['studentId']) && $formData['studentId']!=''){
                $selectData->where('sr.registrationId LIKE ?','%'.$formData['studentId'].'%');
            }

            /*if(isset($formData['profileStatus']) && $formData['profileStatus']!=''){
                 $selectData->where('sr.profileStatus= ?',$formData['profileStatus']);
             }

            if(isset($formData['profileStatusArray']) && $formData['profileStatusArray']!=''){
                 $selectData->where('sr.profileStatus IN ( ? )',$formData['profileStatusArray']);
             }*/

            if(isset($formData['student_name']) && $formData['student_name']!=''){
                $selectData->where('( sp.appl_fname LIKE ?','%'.$formData['student_name'].'%');
                $selectData->orwhere('sp.appl_lname LIKE ? )','%'.$formData['student_name'].'%');
            }

            if(isset($formData['status']) && $formData['status']!=''){
                $selectData->where('pgl.status = ?',$formData['status']);
            }else{
                $selectData->where('pgl.status IN (1,2)');
            }

        }
        //echo $selectData;
        $row = $db->fetchAll($selectData);
        return $row;

    }


    public function getClearanceGraduate($formData=0,$type=null){

        $db = getDB2();

        $selectData = $db->select()
            ->from(array('pgl'=>$this->_name))
            ->joinLeft(array('tu'=>'tbl_user'),'tu.iduser = pgl.shortlisted_by', array('add_by_name'=>"concat_ws(' ',tu.fname,tu.mname,tu.lname)"))
            ->joinLeft(array('tu2'=>'tbl_user'),'tu2.iduser = pgl.approve_by', array('add_by_name2'=>"concat_ws(' ',tu2.fname,tu2.mname,tu2.lname)"))
            ->joinLeft(array('tu3'=>'tbl_user'),'tu3.iduser = pgl.graduate_by', array('add_by_name3'=>"concat_ws(' ',tu3.fname,tu3.mname,tu3.lname)"))
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration = pgl.idStudentRegistration')
            ->join(array('sp'=>'student_profile'),'sp.std_id = sr.sp_id', array('student_name'=>'std_fullname'))
            ->join(array('pr'=>'tbl_program'), 'pr.IdProgram = sr.IdProgram', array('program_name'=>'ProgramName','ProgramCode'))
            ->join(array('itk' => 'tbl_intake'), 'itk.IdIntake = sr.idIntake', array('intake_name'=>'IntakeDefaultLanguage'))
            ->where('pgl.status = ?',1)
            ->order('sp.std_fullname')
            ->order('sr.registrationId');

        if(isset($type) && $type!=''){
            if($type=='kmc'){
                $selectData->joinLeft(array('dc'=>'department_clearance_kmc'),'dc.IdStudentRegistration=pgl.idStudentRegistration',array('dc_status'=>'kmc_status'));
            }else
                if($type=='fin'){
                    $selectData->joinLeft(array('dc'=>'department_clearance_finance'),'dc.IdStudentRegistration=pgl.idStudentRegistration',array('dc_status'=>'dcf_status','dcf_createddt'))
                        ->joinLeft(array('u'=>'tbl_user'),'u.iduser = dc.dcf_createdby',array('fin_approved_by'=>'concat_ws(" ",u.fname,u.mname,u.lname)'));
                }else
                    if($type=='ict'){
                        $selectData->joinLeft(array('dc'=>'department_clearance_ict'),'dc.IdStudentRegistration=pgl.idStudentRegistration',array('dc_status'=>'ict_status','ict_createddt'))
                            ->joinLeft(array('u'=>'tbl_user'),'u.iduser = dc.ict_createdby',array('ict_approved_by'=>'concat_ws(" ",u.fname,u.mname,u.lname)'));
                    }
            if($type=='logistic'){
                $selectData->joinLeft(array('dc'=>'department_clearance_logistic'),'dc.IdStudentRegistration=pgl.idStudentRegistration',array('dc_status'=>'log_status','log_createddt'))
                    ->joinLeft(array('u'=>'tbl_user'),'u.iduser = dc.log_createdby',array('log_approved_by'=>'concat_ws(" ",u.fname,u.mname,u.lname)'));
            }
        }else{
            $selectData->joinLeft(array('dcs'=>'department_clearance_summary'),'dcs.IdStudentRegistration=pgl.idStudentRegistration')
                ->joinLeft(array('u'=>'tbl_user'),'u.iduser = dcs.dcs_createdby',array('summary_approved_by'=>'concat_ws(" ",u.fname,u.mname,u.lname)'));

            if(isset($formData['status']) && $formData['status']!='All'){
                if($formData['status']==1){
                    $selectData->where('dcs.dcs_status = ?',1);
                }
                if($formData['status']==0){
                    $selectData->where('dcs.dcs_status IS NULL');
                }
            }
        }

        if(isset($formData) && $formData!=null){

            if(isset($formData['IdProgram']) && $formData['IdProgram']!=''){
                $selectData->where('sr.IdProgram = ?',$formData['IdProgram']);
            }

            if(isset($formData['IdIntake']) && $formData['IdIntake']!=''){
                $selectData->where('sr.IdIntake = ?',$formData['IdIntake']);
            }


            if(isset($formData['studentId']) && $formData['studentId']!=''){
                $selectData->where('sr.registrationId LIKE ?','%'.$formData['studentId'].'%');
            }
        }
        //echo $selectData;
        $row = $db->fetchAll($selectData);
        return $row;

    }

    public function getTemplate($tpl_id,$locale='en_US'){
        $db = getDB2();
        $select = $db->select()
            ->from(array('ct'=>'comm_template'))
            ->join(array('ctc'=>'comm_template_content'),'ctc.tpl_id=ct.tpl_id')
            ->where('ct.tpl_id  = ?',$tpl_id)
            ->where('ctc.locale = ?',$locale);

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getTemplateTag($tplId){
        $db = getDB2();
        $lstrSelect = $db->select()
            ->from(array("a"=>"comm_template_tags"),array("value"=>"a.*"))
            ->where('a.tpl_id = '.$tplId.' or a.tpl_id = 0')
            ->order("a.tpl_id");
        $larrResult = $db->fetchAll($lstrSelect);
        return $larrResult;
    }


    public function addData($data){
        $id = $this->insert($data);
        return $id;
    }

    public function updateStatus($data,$idStudentRegistration){
        $this->update($data, 'idStudentRegistration = '. (int)$idStudentRegistration);
    }



}

?>