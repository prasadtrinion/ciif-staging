<?php

class App_Model_Quiz extends Zend_Db_Table
{

    protected $_name = 'quiz_qbank';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
    
    public function getQuizques($id,$bid,$order = 'a.created_date ASC',$results=false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_qbank'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'))
            ->where('a.course_id = ?', $id)
            ->where('a.data_id = ?', $bid)
            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }

    public function getquescount($id)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_qbank'))
            //->join(array('b' => 'quiz_answer'), 'b.id_question = a.id', array())
            // ->where('a.data_id = ?',$id)
            ->where('a.data_id = ?',$id);
        

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getranquest($cid,$bid)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_qbank'))
            //->join(array('b' => 'quiz_answer'), 'b.id_question = a.id', array())
            // ->where('a.data_id = ?',$id)
            ->where('a.data_id = ?',$bid)
            ->where('a.course_id = ?',$cid) 
            ->order(new Zend_Db_Expr("RAND()"));    
        $result = $db->fetchAll($select);
        return $result;

    }
    

    public function getmarksum($id)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_qbank'),array('data_id','sum(mark) as sum','count(id) as totalquestion'))
            // ->where('a.data_id = ?',$id)
            ->where('a.data_id = ?',$id);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getpointsum($id)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'content_block_data'),array('data_id','sum(gradepoint) as point'))
            // ->where('a.data_id = ?',$id)
            ->where('a.data_id = ?',$id);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function addData($data)
        {
            // $db = $this->getDefaultAdapter();
            // $id = $this->insert($data);
            // $db->insert($this->_name, $data);
            $id = $this->insert($data);
            return $id;
        }

    public function updateData($data, $id)
    {
        $this->_db->update($this->_name, $data, 'id = ' . (int)$id);
    }

    // public function getData($data)
    //     {
    //        $db = $this->getDefaultAdapter();

    //             $select = $db->select()
    //                 ->from(array('a' => 'quizqbs_question_upload_data'))
    //                 // ->where('a.data_id = ?',$id)
    //                 ->where('a.upl_id = ?',$data);
    //             $result = $db->fetchAll($select);
    //             pr($result);exit;

    //             // $data = array(
    //             //     'question' => $result['question'],           
    //             // );
                
    //             // $this->insert($data);
    //             return $result;
    //     }

}
