<?php

class App_Model_PaymentHistory extends Zend_Db_Table
{

    protected $_name = 'payment_history';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getData(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }


        $result = $db->fetchRow( $select );

        return $result;
    }


}
