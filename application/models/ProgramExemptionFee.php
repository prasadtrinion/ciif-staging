<?php

class App_Model_ProgramExemptionFee extends Zend_Db_Table
{

    protected $_name = 'tbl_program_exemptionfee';
    protected $_primary = 'id_program';
    private static $tree = array();
    private static $html = '';

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
    }
 
    public function getfee(){
          $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_program_category'),'a.id = u.program_category')
            ->join(array('b' => 'tbl_definationms'),'b.idDefinition = u.program_route', array('level'=>'DefinitionDesc'))
            ->join(array('c' => 'tbl_definationms'),'c.idDefinition = u.program_level', array('route'=>'DefinitionDesc'));
            // ->where('id ='.$id);
           
           $result = $db->fetchAll($select);
           // echo  '<pre>';
           // print_r($result);
           // die();  
            return $result;
    }
    public function get_fee_search($formdata){
        
           
          $db = $this->getDefaultAdapter();
       
        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_program_category'),'a.id = u.program_category')
            ->join(array('b' => 'tbl_definationms'),'b.idDefinition = u.program_route', array('level'=>'DefinitionDesc'))
            ->join(array('c' => 'tbl_definationms'),'c.idDefinition = u.program_level', array('route'=>'DefinitionDesc'));
         


            if (isset($formdata['program_route']) && $formdata['program_route']!=0){
              
            $select->where(('u.program_route ='.$formdata['program_route']));
                
            }
            if (isset($formdata['program_category']) && $formdata['program_category']!=0){
 
            $select->where(('u.program_category ='.$formdata['program_category']));
                
            }
            if (isset($formdata['program_level']) && $formdata['program_level']!=0){

            $select->where(('u.program_level ='.$formdata['program_level']));
                
            }
            
           
           $result = $db->fetchAll($select);
           // echo  '<pre>';
           // print_r($result);
           // die();  
            return $result;
    }

    public function programvaliadiate($route,$level,$category){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_exemptionfee'))
     ->where('pc.program_route = ?', $route)
     ->where('pc.program_category = ?', $category)
    ->where('pc.program_level = ?', $level);
    
      
       $result = $db->fetchAll($select);
   
    return $result;
    }
  
}