<?php
class App_Model_Ciifpdoc extends Zend_Db_Table_Abstract
{

	protected $_name = 'tbl_userdoc';
	protected $_PRIMARY='id';


                //addmembershipdocdetail($membership_id,$doc_name,$doc_type)    
 public function addmembershipdocdetail($membership_id,$doc_name)
    {
        $data=array(
        		'membership_id'=>$membership_id,
        		'doc_name'=>$doc_name,
            );
       
     
       $result = $this->insert($data);
       
    }

    public function get_doc($id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))

            ->where('membership_id ='.$id);
           
           $result = $db->fetchRow($select);  
        return $result;
       
    }


      public function update_doc($doc_id,$doc_name)
    {
       
           
        $db = $this->getDefaultAdapter();
           $result = $db->update($doc_name, array('id = ?'. $doc_id));  
        return $result;
       
    }


}