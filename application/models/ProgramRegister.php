<?php

class App_Model_ProgramRegister extends Zend_Db_Table
{

    protected $_name = 'tbl_member_register_program';
    protected $_primary = 'id_mp';
    private static $tree = array();
    private static $html = '';

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

   

    public function get_qualification($userid)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

 $select = $db->select()
   
    ->from(array('pf' => 'tbl_program_category'))
    ->join(array('pc' => 'tbl_member_register_program'), 'pc.program = pf.id ')
 
       ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pf.program_route', array('route'=>'DefinitionDesc'))
      // ->join(array('r' => 'tbl_definationms'), 'r.idDefinition = pc.payment_status', array('payment'=>'DefinitionDesc'))
        ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pf.program_level', array('level'=>'DefinitionDesc'))
      //  ->join(array('defms' => 'tbl_definationms'), 'defms.idDefinition = pf.program_level')
       ->join(array('e' => 'user'), 'e.id = pc.mr_id')
      //  ->join(array('l' => 'tbl_definationms'), 'l.idDefinition = e.estatus', array('status'=>'DefinitionDesc'))
       ->where('exemption_program = 1')
        ->where('pc.mr_id = ?', $userid);

    $result = $db->fetchAll($select);
   
    
    return $result;
    }

     public function get_exem_qualification($userid)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

 $select = $db->select()
   
    ->from(array('pf' => 'tbl_program_category'))
    ->join(array('pc' => 'tbl_exempted_program'), 'pc.program_id = pf.id ')
 
       ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pf.program_route', array('route'=>'DefinitionDesc'))
      // ->join(array('r' => 'tbl_definationms'), 'r.idDefinition = pc.payment_status', array('payment'=>'DefinitionDesc'))
        ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pf.program_level', array('level'=>'DefinitionDesc'))
      //  ->join(array('defms' => 'tbl_definationms'), 'defms.idDefinition = pf.program_level')
       ->join(array('e' => 'user'), 'e.id = pc.user_id')
      //  ->join(array('l' => 'tbl_definationms'), 'l.idDefinition = e.estatus', array('status'=>'DefinitionDesc'))

        ->where('pc.user_id = ?', $userid);

    $result = $db->fetchAll($select);
   
    
    return $result;
    }

    public function get_exemption_qualification($userid)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pf' => 'tbl_program_category'))
    ->join(array('pc' => 'tbl_member_register_program'), 'pc.program_route = pf.program_route and pc.program_level = pf.program_level')
    
      ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pf.program_route', array('route'=>'DefinitionDesc'))
      ->join(array('r' => 'tbl_definationms'), 'r.idDefinition = pc.payment_status', array('payment'=>'DefinitionDesc'))
       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pf.program_level', array('level'=>'DefinitionDesc'))
       ->join(array('defms' => 'tbl_definationms'), 'defms.idDefinition = pf.program_level')
       ->join(array('e' => 'user'), 'e.id = pc.mr_id')
       ->join(array('l' => 'tbl_definationms'), 'l.idDefinition = e.estatus', array('status'=>'DefinitionDesc'))
       ->join(array('pe' => 'tbl_exempted_program'), 'pe.program_route = pc.program_route and pe.program_level = pc.program_level and pf.id = pe.program_id')
    ->where('pc.mr_id = ?', $userid);

    $result = $db->fetchAll($select);
    
   
    
    return $result;
    }

    public function getexemption($user_id){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
        ->from(array('pr'=>'tbl_member_register_program'))
        ->join(array('pc' => 'tbl_program_category'), 'pc.program_route = pr.program_route and pc.program_level = pr.program_level')
        ->join(array('pf' => 'tbl_program_fee'), 'pf.program_category = pc.id and pc.program_route = pr.program_route and pc.program_level = pr.program_level')

         ->where('mr_id = '.$user_id);
         $result = $db->fetchAll($select);
         // echo '<pre>';
         // print_r($result);
         // die();
         return $result;

    }

     public function get_modules($userid)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
        ->from(array('pf' => 'tbl_program_category'))
    ->join(array('c' => 'tbl_member_register_program'), 'c.program_route = pf.program_route')
        ->join(array('d' => 'tbl_member_register_program'), 'd.program_level = pf.program_level')

       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pf.program_level', array('level'=>'DefinitionDesc'))
    
       ->join(array('e' => 'user'), 'e.id = c.mr_id')
       ->join(array('l' => 'tbl_definationms'), 'l.idDefinition = e.estatus', array('status'=>'DefinitionDesc'))
    ->where('c.mr_id = ?', $userid);

    $result = $db->fetchAll($select);
   
    return $result;
    }



    

     public function modules_type($userid)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
        ->from(array('pf' => 'tbl_program_category'))
       ->join(array('tp' => 'tbl_exempted_program'),'tp.program_route = pf.program_route and tp.program_level = pf.program_level and tp.program_id = pf.id')
       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = tp.program_level', array('level'=>'DefinitionDesc'))
      ->where('tp.user_id = ?', $userid);

    $result = $db->fetchAll($select);
   
    return $result;
    }

    public function modules_exemption($userid)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
        ->from(array('pf' => 'tbl_program_category'))
       
        ->join(array('c' => 'tbl_member_register_program'), 'c.program_route = pf.program_route and c.program_level = pf.program_level and c.program = pf.id')
        ->join(array('d' => 'tbl_program_exemptionfee'), 'd.program_route = pf.program_route and d.program_level = pf.program_level and d.program_category = pf.id')

         ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = c.program_level', array('level'=>'DefinitionDesc'))
      
     // ->where('c.exemption_program = 1')
    ->where('c.mr_id = ?', $userid);


    $result = $db->fetchAll($select);
   
   
    return $result;
    }

    public function modules_exemptionfee($userid)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
        ->from(array('pf' => 'tbl_program_category'))
       
        ->join(array('c' => 'tbl_member_register_program'), 'c.program_route = pf.program_route and c.program_level = pf.program_level')
        ->join(array('d' => 'tbl_program_exemptionfee'), 'd.program_route = pf.program_route and d.program_level = pf.program_level and d.program_category = pf.id')
        ->join(array('e' => 'tbl_exempted_program'),'e.program_id = pf.id and e.program_route = pf.program_route and e.program_level = pf.program_level')

         

       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = c.program_level', array('level'=>'DefinitionDesc'))
       ->where('e.exempted_status != 0')
    
    ->where('c.mr_id = ?', $userid);
    


    $result = $db->fetchAll($select);
    
    return $result;
    }

    public function exemption_total($userid){
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   ->from(array('pf' => 'tbl_program_category'))
       
        // ->join(array('c' => 'tbl_member_register_program'), 'c.program_route = pf.program_route and c.program_level = pf.program_level')
        ->join(array('d' => 'tbl_program_exemptionfee'), 'd.program_route = pf.program_route and d.program_level = pf.program_level and d.program_category = pf.id')
        ->join(array('e' => 'tbl_exempted_program'),'e.program_id = pf.id and e.program_route = pf.program_route and e.program_level = pf.program_level')
       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = e.program_level', array('level'=>'DefinitionDesc'))
      
       ->where('e.exempted_status != 0')
    
    ->where('e.user_id = ?', $userid);

    $result = $db->fetchAll($select);
    

    return $result;
    }

    public function get_member_id($userid){
$db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
       ->from(array('pc' => 'membership_registration'))
       
    
    ->where('pc.mr_membershipId = ?', $userid);

    $result = $db->fetchAll($select);
    

    return $result;

    }

    public function total_amount($userid){
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
       ->from(array('pc' => 'tbl_exempted_program'),array('sum(pc.f_amount) as sum'))
       ->where('pc.exempted_status != 0')
    
    ->where('pc.user_id = ?', $userid);

    $result = $db->fetchAll($select);
    

    return $result;
    }

    public function get_account($userid)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    
    ->from(array('e' => 'user'))
      
    ->where('e.id = ?', $userid);

    $result = $db->fetchAll($select);

   
    return $result;
    }

    public function get_amount($route,$level)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_category'), array('sum(amount) as sum'))
    ->join(array('pf' => 'tbl_program_fee'), 'pf.program_category = pc.id')
     ->join(array('def' => 'tbl_definationms'), 'def.idDefinition = pf.amount_type')
      ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pf.program_route', array('route'=>'DefinitionDesc'))
       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pf.program_level', array('level'=>'DefinitionDesc'))
    ->where('pf.program_route = ?', $route)
    ->where('pf.program_level = ?', $level)
    ->where('pc.active = 1');

    $result = $db->fetchAll($select);
  
    return $result;
    }

     public function get_program_amount($id)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   

    ->from(array('pf' => 'tbl_program_fee'))
    // ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pf.program_route', array('route'=>'DefinitionDesc'))
    //    ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pf.program_level', array('level'=>'DefinitionDesc'))
   ->where('pf.program_category = ?', $id);

    $result = $db->fetchAll($select);
 
  
    return $result;
    }


     public function get_exemption_amount($id)
    {
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   

    ->from(array('pf' => 'tbl_program_exemptionfee'))
    // ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pf.program_route', array('route'=>'DefinitionDesc'))
    //    ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pf.program_level', array('level'=>'DefinitionDesc'))
   ->where('pf.program_category = ?', $id);

    $result = $db->fetchAll($select);
 
  
    return $result;
    }





    public function get_program_code($route)
    {

         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_definationms'))

    ->where('pc.idDefinition = ?', $route);

    $result = $db->fetchRow($select);
    return $result;
    }

    public function get_user_regprogram($id)
    {

         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
     ->from(array('pr'=>'tbl_member_register_program'))

    ->where('pr.mr_id = ?', $id);

    $result = $db->fetchRow($select);
    return $result;
    }

    public function get_regamount($id)
    {

         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
     ->from(array('pr'=>'tbl_member_register_program'),array('sum(program_amount) as sum' ))
     ->where('pr.exemption_program = 1')

    ->where('pr.mr_id = ?', $id);

    $result = $db->fetchRow($select);
    return $result;
    }

    public function get_exemamount($id)
    {

         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
     ->from(array('pr'=>'tbl_exempted_program'),array('sum(f_amount) as suma' ))
     // ->where('pr.exempted_status = 1')
     ->group('pr.user_id')
    ->where('pr.user_id = ?', $id);

    $result = $db->fetchRow($select);
   
    return $result;
    }    

     public function get_regisamount($id)
    {

         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
     ->from(array('pr'=>'tbl_member_register_program'),array('sum(program_amount) as sum' ))
    
     ->order('pr.id_mp DESC')
    ->where('pr.mr_id = ?', $id);

    $result = $db->fetchRow($select);
    return $result;
    }

    
}
