<?php

class App_Model_UserContentTracking extends Zend_Db_Table
{

    protected $_name = 'user_content_tracking';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getTrackingByUser($user_id, $course_id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'content_block_data'), 'c.data_id = a.data_id', 'course_id')
            ->joinLeft(array('d' => 'courses'), 'd.id = a.course_id', array('code','title'))
            ->where('a.user_id = ?', $user_id)
            ->where('d.id = ?', $course_id);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getTrackingAllUser($course_id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'content_block_data'), 'c.data_id = a.data_id', 'course_id')
            ->joinLeft(array('d' => 'courses'), 'd.id = c.course_id', array('code','title'))
            ->where('d.id = ?', $course_id);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getTrackingByDataId($user_id, $data_id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name), new Zend_Db_Expr('COUNT(a.id) AS total_access'))
            ->where('a.user_id = ?', $user_id)
            ->where('a.data_id = ?', $data_id);

        $result = $db->fetchRow($select);

        return $result;
    }
}
