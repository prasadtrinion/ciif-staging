<?php

class App_Model_UserOnline extends Zend_Db_Table
{

    protected $_name = 'user_online';
    
    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

	public function getOnlineInt($limit='',$role='')
	{
		return count($this->getOnline($limit, $role));
	}

	public function getOnline($limit='',$role='')
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		    
		$sql = $db->select()
				  ->from(array("a" => $this->_name)) 
				  ->join(array('u'=>'user'),'u.id = a.user_id',array('u.firstname','u.lastname'));

		if ( $role )
		{
			$sql->where('a.user_role = ?', $role);
		}

		if ( $limit )
		{
			$sql->limit($limit);
		}

		$results = $db->fetchAll($sql);


		return $results;
	}

	public function getOnlineIds($limit='', $role='')
	{
		$get = $this->getOnline($limit, $role);
		$ids = array();

		foreach ( $get as $user )
		{
			$ids[] = $user['user_id'];
		}

		return $ids;
	}

	public function getOnlineCourseMatesInt($course_id, $limit='',$role='')
	{
		return count($this->getOnlineCourseMates($course_id, $limit, $role));
	}

	public function getOnlineCourseMates($course_id, $limit='', $role='')
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$sql = $db->select()
					->from(array('a' => $this->_name))
					->join(array('c' => 'user'), 'c.id = a.user_id', array('c.firstname', 'c.lastname'))
					->join(array('d' => 'enrol'), 'd.user_id = a.user_id')
					// ->where('d.course_id = ?', $course_id)
					->where('d.active = ?', 1);

		if ( $role )
		{
			$sql->where('a.user_role = ?', $role);
		}

		if ( $limit )
		{
			$sql->limit($limit);
		}

		$results = $db->fetchAll($sql);

		return $results;
	}

    public function getTotalUsers()
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()
            ->from(array('a' => 'user'), array('*'))
            ->where('a.role != ?', "administrator")
//            ->where('a.role != "administrator"')
            ->limit('50');
        $results = $db->fetchAll($sql);
        return $results;
    }
}
