<?php

class App_Model_UserGroup extends Zend_Db_Table
{

    protected $_name = 'user_group';
    protected $_primary = 'group_id';

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

    public function getAllGroup($active_only = false) {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'courses'), 'c.id = a.course_id', array('c.code as course_code', 'c.title AS course_title'));

        if ($active_only) {
            $select->where('a.active = 1');
        }

        $select->group('a.group_id')
                ->order('a.course_id')
                ->order('a.group_name');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getAllGroupQuery($active_only = false, $where = array()) {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'courses'), 'c.id = a.course_id', array('c.code as course_code', 'c.title AS course_title'))
            ->joinLeft(array('d' => 'user_group_data'), 'd.group_id=a.group_id', array(new Zend_Db_Expr("COUNT(`user_id`) AS total_user")));


        if ($active_only) {
            $select->where('a.active = 1');
        }

        if (!empty($where))
        {
            foreach($where as $k=>$v)
            {
                $select->where($k . ' LIKE ?', '%' . $v . '%');
            }
        }

        $select->group ('a.group_desc')
                ->order('a.course_id')
                ->order('a.group_name');
        return $select;
    }

    public function getUserGroup($id, $active_only = false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'courses'), 'c.id = a.course_id', array('c.code as course_code', 'c.title as course_title'))
            ->where('a.group_id = ?', $id);

        if ($active_only) {
            $select->where('a.active = 1');
        }
        $result = $db->fetchRow($select);

        return $result;
    }

    public function getGroupByName($group_name)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->joinLeft(array('c' => 'courses'), 'c.id = a.course_id', array('c.code as course_code', 'c.title as course_title'))
            ->where('a.group_name = ?', $group_name);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function insertGroup($data)
    {
        $db = $this->getDefaultAdapter();

        $id = $db->insert($this->_name, $data);

        return $db->lastInsertId();
    }
    
    public function getDataByUserAndGroup($user_id, $group_id)
    {
        $db     = $this->getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => 'user_group_data'))
            ->where('a.user_id = ?', $user_id)
            ->where('a.group_id = ?', $group_id);
        return $db->fetchRow($select);
    }
}
