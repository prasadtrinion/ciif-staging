
<?php

class App_Model_Link extends Zend_Db_Table
{

    protected $_name = 'tbl_link';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function get_role($id){
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->where('u.id ='.$id);
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    public function get_role_menu(){
        
            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
             ->joinLeft(array('a' => 'tbl_definationms'),'a. idDefinition = u.role_id');
           
           $result = $db->fetchAll($select);  
        return $result;

    }

    // public function get_menu($id){
       
        
    //         $db = $this->getDefaultAdapter();

    //     $select = $db->select()
    //         ->from(array('u' => $this->_name))
            
    //         ->where('u.id ='.$id);

    //        $result = $db->fetchAll($select);  
         
    //     return $result;

    // }
  
}


 