<?php
class App_Model_PaymentInfo{

    /*
       *  Get current receipt seq no
       */
    public function getReceiptNoSeq()
    {

        $seq_data = array(
            5,
            date('Y'),
            0,
            0,
            0
        );

        $db = getDB2();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS receipt_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['receipt_no'];
    }

}
?>