<?php

class App_Model_MemberExperience extends Zend_Db_Table
{

    protected $_name = 'tbl_member_experience_master';
    protected $_primary = 'id_exp';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getDegreeExp($memberid,$subtypeid,$degreeid)
    {
        
       $db = $this->getDefaultAdapter();

       $select = $db->select()
                    ->from(array('a' => $this->_name ))
                    ->where('a.id_member = ?', $memberid)
                    ->where('a.id_member_subtype = ?', $subtypeid)
                    ->where('a.id_degree = ?', $degreeid);

        $result = $db->fetchAll($select);


        return $result;
    }


    
    
}

?>