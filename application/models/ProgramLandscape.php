<?php

class App_Model_ProgramLandscape extends Zend_Db_Table
{

    protected $_name = 'tbl_program_landscape';
    protected $_primary = 'id';
    private static $tree = array();
    private static $html = '';

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

   

    public function get_details(){
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_landscape'))
    
      ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pc.program_route', array('route'=>'DefinitionDesc'))
       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pc.program_level', array('level'=>'DefinitionDesc'));
       $result = $db->fetchAll($select);
    // echo '<pre>';
    // print_r($result);
    // die();
    return $result;
    }

    public function get_landscape($search){
         $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
   
    ->from(array('pc' => 'tbl_program_landscape'))
    
      ->join(array('defr' => 'tbl_definationms'), 'defr.idDefinition = pc.program_route', array('route'=>'DefinitionDesc'))
       ->join(array('defl' => 'tbl_definationms'), 'defl.idDefinition = pc.program_level', array('level'=>'DefinitionDesc'))
       ->where('name LIKE ?', '%'.$search.'%');

       if (isset($formData['group_name']) && $formData['group_name']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto('a.group_name LIKE ?', "%".$formData['group_name']."%"));
            }
            if (isset($formData['dropdown_intake']) && $formData['dropdown_intake']!=''){
                $lstrSelect->where($lobjDbAdpt->quoteInto('a.Intake LIKE ?', "%".$formData['dropdown_intake']."%"));
            }
       $result = $db->fetchAll($select);
    
    return $result;
    }

   
    

    
}
