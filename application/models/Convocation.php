<?php

class App_Model_Convocation extends Zend_Db_Table
{
    protected $_name = "";

    function clear($student_id) {
        $select = 'IdStudentRegistration = '. $student_id;
        $this->delete($select);
        return(true);
    }



    function save($data) {
        $this->clear($data['IdStudentRegistration']);

        $this->insert($data);
        return(true);
    }


    public function getCollectionInfo($IdStudentRegistration) {
        //$db = Zend_Db_Table::getDefaultAdapter();

        $db = getDB2();
        $selectData = $db->select()
            ->from(array('a'=>'tbl_graduation_collection'))
            ->where('a.IdStudentRegistration = ?', $IdStudentRegistration);
        $row = $db->fetchRow($selectData);
        //echo $selectData;
        return $row;
    }

    public function getCollectionInfoCertificate($IdStudentRegistration) {
        //$db = Zend_Db_Table::getDefaultAdapter();

        $db = getDB2();
        $selectData = $db->select()
            ->from(array('gc'=>'tbl_graduation_collectioncertificate'))
            ->where('gc.IdStudentRegistration = ?', $IdStudentRegistration);
        $row = $db->fetchRow($selectData);
        //echo $selectData;
        return $row;
    }


    public function getConvocationGraduate($IdStudentRegistration){

        $db = getDB2();
        //$db = Zend_Db_Table::getDefaultAdapter();

        /*$selectData = $db->select()
                         ->from(array('cg'=>'convocation_graduate'))
                         ->join(array('c'=>'convocation'),'c.c_id = cg.convocation_id')
                         ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=cg.IdStudentRegistration',array('IdProgran'))
                         ->where('cg.IdStudentRegistration = ?', $IdStudentRegistration);
        $row = $db->fetchRow($selectData);*/

        $selectData = $db->select()
            ->from(array('pl'=>'pregraduate_list'))
            ->join(array('c'=>'convocation'),'c.c_id = pl.c_id')
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=pl.idStudentRegistration',array('IdProgram'))
            ->join(array('sp'=>'student_profile'),'sp.std_id=sr.sp_id')
            ->where('pl.idStudentRegistration = ?', $IdStudentRegistration)
            ->where('sr.grad_status = 0');
        $row = $db->fetchRow($selectData);

        return $row;
    }


    public function saveInvoiceHistory($data) {
        $db = getDB2();
        $db->insert('tbl_graduation_invoice_history',$data);
        return(true);

    }

    public function getGraduateConvoInfo($IdStudentRegistration){
        $db = getDB2();

        $selectData = $db->select()
            ->from(array('pl'=>'pregraduate_list'))
            ->join(array('c'=>'convocation'),'c.c_id = pl.c_id')
            ->join(array('sr'=>'tbl_studentregistration'),'sr.IdStudentRegistration=cg.IdStudentRegistration',array('IdProgram'))
            ->where('pl.idStudentRegistration = ?', $IdStudentRegistration)
            ->where('pl.status = ?',1);
        $row = $db->fetchRow($selectData);

        return $row;
    }

    public function getProfile($externalid){

        $db     = getDB2();

        $select = $db->select()
            ->from(array('a'=>'tbl_studentregistration'))
            ->where('a.IdStudentRegistration = ?',$externalid);

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getConvoFee($convo_id,$fee_id){
        $db = getDB2();

        $selectData = $db->select()
            ->from(array('fic'=>'fee_item_convo'))
            ->where('fic.fv_convo = ?',$convo_id)
            ->where('fic.fv_fi_id = ?',$fee_id)
            ->where('fic.fv_currency_id = ?',1);

        $row = $db->fetchRow($selectData);

        return $row;
    }

    public function getDocumentDownload($tpl_id,$locale='en_US'){

        $db = getDB2();
        $select = $db->select()
            ->from(array('ct'=>'comm_template'))
            ->join(array('ctc'=>'comm_template_content'),'ctc.tpl_id = ct.tpl_id')
            ->where('ct.tpl_id = ?',$tpl_id)
            ->where('ctc.locale =?',$locale);

        $row = $db->fetchRow($select);

        return $row;
    }

    public function getDataConvoFee($convoId,$feeId){
        $db = getDB2();
        $selectData = $db->select()
            ->from(array('fe'=>'fee_item_convo'))
            ->join(array('fi'=>'fee_item'),'fi.fi_id = fe.fv_fi_id')
            ->join(array('cv'=>'convocation'),'cv.c_id = fe.fv_convo')
            ->join(array('c'=>'tbl_currency'),'c.cur_id = fe.fv_currency_id')
            ->where("fe.fv_convo =?",$convoId)
            ->where("fe.fv_fi_id =?",$feeId)
        ;

        $row = $db->fetchRow($selectData);
        if(!$row){
            return null;
        }else{
            return $row;
        }

    }

}
