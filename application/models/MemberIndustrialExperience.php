<?php

class App_Model_MemberIndustrialExperience extends Zend_Db_Table
{

    protected $_name = 'tbl_definationms';
    protected $_primary = 'idDefinition';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    

    public function getIndustrialExp()
    {
        $id = 193;
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.idDefType = ?', $id);

        $result = $db->fetchAll($select);

        // echo "<pre>";
        // print_r($result);
        // die();

        return $result;
    }


    public function getLectureshipExp()
    {
        $id = 195;
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.idDefType = ?', $id);

        $result = $db->fetchAll($select);

        // echo "<pre>";
        // print_r($result);
        // die();

        return $result;
    }
public function get_prof_des($id)
    {
        
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name ))
            ->where('a.idDefType = ?', $id);

        $result = $db->fetchAll($select);

        // echo "<pre>";
        // print_r($result);
        // die();

        return $result;
    }

    public function getDegreeExp()
    {
       // SELECT * FROM `tbl_definationtypems` WHERE `idDefType` in (196,197,198)

        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "select * from tbl_definationtypems  
                where idDefType in (196,197,198)
                 ";
        $result = $db->fetchAll($sql);

        // echo "<pre>";
        // print_r($result);
        // die();


        return $result;
        // $id = 195;
        // $db = $this->getDefaultAdapter();

        // $select = $db->select()
        //     ->from(array('a' => $this->_name ))
        //     ->where('a.idDefType = ?', $id);

        // $result = $db->fetchAll($select);

        // echo "<pre>";
        // print_r($result);
        // die();

       // return $result;
    }




    public function getDegreeExp1()
    {
       // SELECT * FROM `tbl_definationtypems` WHERE `idDefType` in (196,197,198)

        $db = Zend_Db_Table::getDefaultAdapter();
       $sql = "select * from tbl_definationms as a  inner join  tbl_definationtypems as b on  a.idDefType = b.idDefType
                where defTypeDesc = 'Degree Type'
                 ";
        $result = $db->fetchAll($sql);
        return $result;
      
    }

    
}

?>