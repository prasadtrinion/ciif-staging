<?php
class App_Model_Finance_FeeStructure extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'fee_structure';
	protected $_primary = "fs_id";
		
	public function getData($id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fs'=>$this->_name))
					->joinLeft(array('i'=>'tbl_intake'),'i.IdIntake = fs.fs_intake_start', array('s_IntakeId'=>'','s_Intake'=>'IntakeDesc', 's_intake_bahasa'=>'IntakeDefaultLanguage', 'start_date'=>'ApplicationStartDate', 'end_date'=>'ApplicationEndDate'))
					->joinLeft(array('ii'=>'tbl_intake'),'ii.IdIntake = fs.fs_intake_end', array('s_IntakeId'=>'','e_Intake'=>'IntakeDesc', 'e_intake_bahasa'=>'IntakeDefaultLanguage', 'e_start_date'=>'ApplicationStartDate', 'e_end_date'=>'ApplicationEndDate' ))
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fs.fs_student_category')
					->joinLeft(array('ac'=>'tbl_account_code'), 'ac.ac_id = fs.fs_ac_id')
					->joinLeft(array('acpre'=>'tbl_account_code'), 'acpre.ac_id = fs.fs_prepayment_ac', array('prepayment_code'=>'acpre.ac_desc'))
					->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = fs.fs_cur_id')
					->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster=fs.fs_semester_start', array('s.SemesterMainCode', 's.SemesterMainName as semester_name'))
					->joinLeft(array('cp'=>'tbl_branchofficevenue'),'cp.IdBranch=fs.fs_cp', array('cp.BranchName'))
					->where("fs.fs_id = '".$id."'");
			
		$row = $db->fetchRow($selectData);				
		return $row;
	}
	
	public function getPaginateData($search=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		if($search){
			$selectData = $db->select()
					->from(array('fs'=>$this->_name))
					//->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fi.fi_amount_calculation_type', array('calType'=>'d.DefinitionDesc','calTypeBahasa'=>'d.Description'))
					//->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition = fi.fi_frequency_mode',array('freqMode'=>'e.DefinitionDesc','freqModeBahasa'=>'e.Description'))
					->where("fs.fs_name LIKE '%".$search['fs_name']."%'")
					->where("fs.fs_description LIKE '%".$search['fs_description']."%'")
					->where("fs.fs_intake_start = ? ",$search['fs_intake_start'])
					->where("fs.fs_semester_start = ? ",$search['fs_semester_start'])
					->where("fs.fs_student_category = ? ",$search['fs_student_category'])
					->where("fs.fs_effective_date LIKE '%".$search['fs_effective_date']."%'")
					->where("fs.fs_ac_id = ? ",$search['fs_ac_id'])
					->where("fs.fs_prepayment_ac = ? ",$search['fs_prepayment_ac'])
					->where("fs.fs_cur_id = ? ",$search['fs_cur_id'])
					->where("fs.fs_estimated_total_fee LIKE '%".$search['fs_estimated_total_fee']."%'")
					->where("fs.fs_cp = ? ",$search['fs_cp']);
		}else{
			$selectData = $db->select()
					->from(array('fs'=>$this->_name))
					->joinLeft(array('i'=>'tbl_intake'),'i.IdIntake = fs.fs_intake_start', array('s_IntakeId'=>'','s_Intake'=>'IntakeDesc', 's_intake_bahasa'=>'IntakeDefaultLanguage', 'start_date'=>'ApplicationStartDate', 'end_date'=>'ApplicationEndDate'))
					->joinLeft(array('ii'=>'tbl_intake'),'ii.IdIntake = fs.fs_intake_end', array('s_IntakeId'=>'','e_Intake'=>'IntakeDesc', 'e_intake_bahasa'=>'IntakeDefaultLanguage'))
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fs.fs_student_category');
						
		}
			
//		echo $selectData;exit;
		return $selectData;
	}
	
	public function getPaginateDataByCategory($category=314, $intake_id=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
					->from(array('fs'=>$this->_name))
					->joinLeft(array('i'=>'tbl_intake'),'i.IdIntake = fs.fs_intake_start', array('s_IntakeId'=>'','s_Intake'=>'IntakeDesc', 's_intake_bahasa'=>'IntakeDefaultLanguage', 'start_date'=>'ApplicationStartDate', 'end_date'=>'ApplicationEndDate'))
					->joinLeft(array('ii'=>'tbl_intake'),'ii.IdIntake = fs.fs_intake_end', array('s_IntakeId'=>'','e_Intake'=>'IntakeDesc', 'e_intake_bahasa'=>'IntakeDefaultLanguage'))
					->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fs.fs_student_category')
					->where('fs.fs_student_category = ?',$category);
		
		if($intake_id){
		  $selectData->where('fs.fs_intake_start = ?',$intake_id);
		}
		return $selectData;
	}
	
	public function getFeeStructureByCategory($category){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
		->from(array('fs'=>$this->_name))
		->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fs.fs_student_category')
		->joinLeft(array('ac'=>'tbl_account_code'), 'ac.ac_id = fs.fs_ac_id')
		->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = fs.fs_cur_id')
		->joinLeft(array('itk'=>'tbl_intake'), 'itk.IdIntake = fs.fs_intake_start', array('IntakeDesc', 'IntakeDefaultLanguage'))
		->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster=fs.fs_semester_start', array('s.SemesterMainCode', 's.SemesterMainName as semester_name'))
		->where('fs.fs_student_category = ?',$category);
		
		$row = $db->fetchAll($selectData);
		
		return $row;
	}
	
	public function getFeeStructureByProgramCategory($category, $program_id=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		 $selectData = $db->select()
		->from(array('p'=>'fee_structure_program'))
		->joinLeft(array('fs'=>$this->_name), 'fs.fs_id=p.fsp_fs_id', array('*'))
		->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fs.fs_student_category')
		->joinLeft(array('ac'=>'tbl_account_code'), 'ac.ac_id = fs.fs_ac_id')
		->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = fs.fs_cur_id')
		->joinLeft(array('itk'=>'tbl_intake'), 'itk.IdIntake = fs.fs_intake_start', array('IntakeDesc', 'IntakeDefaultLanguage','sem_year','sem_seq'))
		->joinLeft(array('s'=>'tbl_semestermaster'), 's.IdSemesterMaster=fs.fs_semester_start', array('s.SemesterMainCode', 's.SemesterMainName as semester_name','sem_seq'))
		->where('fs.fs_student_category = ?',$category)
		->where('p.fsp_program_id = ?',$program_id)
		->group('fs.fs_id');
			
		$selectData .= "order by itk.sem_year desc, CASE itk.sem_seq
		      WHEN 'SEP' THEN 1
		      WHEN 'JUN' THEN 2
		      WHEN 'JAN' THEN 3
		   END";
		
   
	
		
		$row = $db->fetchAll($selectData);
		
		return $row;
	}
		
	public function addData($postData){
		
		
		$db = Zend_Db_Table::getDefaultAdapter();
		//add fs data
	   	$data = array(
	        'fs_name' => $postData['fs_name'],
			'fs_description' => $postData['fs_description'],
			'fs_intake_start' => $postData['fs_intake_start'],
			//'fs_intake_end' => $postData['fs_intake_end'],
			'fs_student_category' => $postData['fs_student_category']				
		);
		
		$this->insert($data);
			
		/*
		$db->beginTransaction();
		try{
			//set end intake for fee structure with same student category
			$data = array(
						'fs_intake_end' => $postData['fs_intake_start']
					);

			$where = array(
						'fs_student_category = ?' => $postData['fs_student_category'],
						'fs_intake_end is null'
					);

			$this->update($data,$where);		
			
			//add fs data
		   	$data = array(
		        'fs_name' => $postData['fs_name'],
				'fs_description' => $postData['fs_description'],
				'fs_intake_start' => $postData['fs_intake_start'],
				//'fs_intake_end' => $postData['fs_intake_end'],
				'fs_student_category' => $postData['fs_student_category']				
			);
			
			$this->insert($data);
		   
		   	$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
			echo $e->getMessage();
		}*/
		
		
	}		
		

	public function updateData($postData,$id){
		
		$data = array(
		        'fs_name' => $postData['fs_name'],
				'fs_description' => $postData['fs_description'],
				'fs_intake_start' => $postData['fs_intake_start'],
				'fs_intake_end' => $postData['fs_intake_end']!=null?$postData['fs_intake_end']:null,
				'fs_student_category' => $postData['fs_student_category']				
			);
			
		$this->update($data, "fs_id = '".$id."'");
	}
	
	public function deleteData($id=null){
		if($id!=null){	
			$this->delete("fs_id = '".$id."'");
		}
	}	
	
	public function getApplicantFeeStructure( $program_id, $scheme, $intake_id, $student_category=579,$throw=0){
		
		$db = getDB2();
		$selectData = $db->select()
					->from(array('fs'=>$this->_name))
					->join( array('fsp'=>'fee_structure_program'), 'fsp.fsp_fs_id = fs.fs_id and fsp.fsp_program_id = '.$program_id.' and fsp.fsp_idProgramScheme = '.$scheme)
					->where("fs.fs_intake_start = '".$intake_id."'")
					->where("fs.fs_student_category = '".$student_category."'");
		
		$row = $db->fetchRow($selectData);
		
		if(!$row){
		  
		    $ex_msg = "No Fee Structure setup for program(".$program_id."), scheme(".$scheme."), intake(".$intake_id."), category(".$student_category.")";
		        
			if ( $throw ) 
			{
				throw new Exception($ex_msg);
			}
			else
			{
				return false;
			}

		}else{
			return $row;
		}
	}
	
	public function getProgramFeeStructureList($program_id){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$selectData = $db->select()
			->from(array('fs'=>$this->_name))
			->join( array('fsp'=>'fee_structure_program'), 'fsp.fsp_fs_id = fs.fs_id and fsp.fsp_program_id = '.$program_id)
			->joinLeft(array('i'=>'tbl_intake'),'i.IdIntake = fs.fs_intake_start', array('s_IntakeId'=>'','s_Intake'=>'IntakeDesc', 's_intake_bahasa'=>'IntakeDefaultLanguage', 'start_date'=>'ApplicationStartDate', 'end_date'=>'ApplicationEndDate'))
			->joinLeft(array('ii'=>'tbl_intake'),'ii.IdIntake = fs.fs_intake_end', array('s_IntakeId'=>'','e_Intake'=>'IntakeDesc', 'e_intake_bahasa'=>'IntakeDefaultLanguage', 'e_start_date'=>'ApplicationStartDate', 'e_end_date'=>'ApplicationEndDate' ))
			->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = fs.fs_student_category')
			->order('i.ApplicationStartDate DESC')
			->order('fs.fs_student_category ASC');
			
		$row = $db->fetchAll($selectData);
		
		return $row;
	}
	
	public function getAppFeeStructure( $program_id, $scheme, $intake_id, $student_category=579,$semester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db->select()
					->from(array('fs'=>$this->_name))
					->join( array('fsp'=>'fee_structure_program'), 'fsp.fsp_fs_id = fs.fs_id and fsp.fsp_program_id = '.$program_id.' and fsp.fsp_idProgramScheme = '.$scheme)
					->where("fs.fs_intake_start = '".$intake_id."'")
					->where("fs.fs_semester_start = '".$semester."'")
					->where("fs.fs_student_category = '".$student_category."'");
		
		$row = $db->fetchRow($selectData);
		
		if(!$row){
		  
		    $ex_msg = "No Fee Structure setup for program(".$program_id."), scheme(".$scheme."), intake(".$intake_id."), category(".$student_category.")";
		        
			throw new Exception($ex_msg);
		}else{
			return $row;
		}
	}

}

