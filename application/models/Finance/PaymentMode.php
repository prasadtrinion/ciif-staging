<?php
/**
 * @copyright Copyright (c) 2015, MTCSB
 */

class App_Model_Finance_PaymentMode extends Zend_Db_Table_Abstract {
	
	/**
	 * The default table name
	 */
	protected $_name = 'payment_mode';
	protected $_primary = "id";
	protected $_locale;
	
	public function init() {
		$registry = Zend_Registry :: getInstance();
		$this->_locale = $registry->get('Zend_Locale');
	}
	
	
	public function getData($id=0){

        $db = getDB2();
		$selectData = $db->select()
						->from(array('pm'=>$this->_name))
						->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition = pm.payment_group', array('payment_category_name'=>'d.DefinitionDesc','payment_category_name2'=>'d.Description'));
	
		if($id!=0){
			$selectData->where("pm.id = '".$id."'");
				
			$row = $db->fetchRow($selectData);
				
		}else{
				
			$row = $db->fetchAll($selectData);
		}
			
		if(!$row){
			return null;
		}else{
			return $row;
		}
	
	}

    public function getPaymentMode(){
        $db     = getDB2();
        $selectData = $db->select()
            ->from(array('pm'=>'tbl_payment_mode_category'))
            ->join(array('d'=>'payment_mode'),'d.id = pm.IdPaymentMode')
            ->where('d.display_portal = 1')
            ->order('d.description');
        $row = $db->fetchAll($selectData);
        return $row;
    }
	
	public function getPaymentModeList($corporateId,$Id=0){
		
		$db     = getDB2();
		$selectData = $db->select()
			->from(array('pm'=>'tbl_payment_mode_category'))
			->join(array('d'=>'payment_mode'),'d.id = pm.IdPaymentMode')
			->where('pm.IdCategory = ?',$corporateId)
            ->where('d.display_portal = 1')
            ->where('d.active = 1')
            ->order('d.description');

        $row = $db->fetchAll($selectData);

        foreach($row as $n=>$data) {

            //check company payment mode
            $selectData2 = $db->select()
                ->from(array('pc' => 'tbl_payment_mode_company'),array('IdCompany'))
                ->where('pc.IdPaymentMode = ?',$data['id']);

            $row2 = $db->fetchAll($selectData2);

            if($row2) {

                $CompanyList = array();

                foreach ($row2 as $data2) {
                    $CompanyList[] = $data2['IdCompany'];
                }

                if ($Id) {
                    $exist = in_array($Id, $CompanyList);

                    if($exist == false){
                        unset($row[$n]);
                    }
                }
            }
        }

		return $row;
		
	}

    public function getEpf($programId){

        $db = getDB2();
        $selectData = $db->select()
            ->from(array('pm'=>'tbl_payment_mode_program'))
            ->join(array('d'=>'payment_mode'),'d.id = pm.IdPaymentMode')
            ->where('pm.IdProgram = ?',$programId)
            ->where('d.active = 1')
            ->where('d.payment_group = 1023'); //payment group epf

        $row = $db->fetchRow($selectData);

        return $row;

    }
	
/*
	 * Get the list of payment mode of the respected payment group and current user's university
	*/
	public function fnGetPaymentMode($idGroupPayment){
        $db = getDB2();
		$lstrSelect = $db->select()
		->from(array("a" => "payment_mode"), array("key" => "a.id" , "name"=> 'a.payment_mode'))
		->where('a.payment_group =?',$idGroupPayment);
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}

    public function insertPaymentModeProgram($paymentmodeprogram) {
        $db = getDB2();
        return $db->insert('tbl_payment_mode_program', $paymentmodeprogram);
    }

    public function insertPaymentModeCompany($paymentmodecompany) {
        $db = getDB2();
        return $db->insert('tbl_payment_mode_company', $paymentmodecompany);
    }

    public function insertPaymentModeCategory($paymentmodecategory) {
        $db = getDB2();
        return $db->insert('tbl_payment_mode_category', $paymentmodecategory);
    }

    public function flushPaymentModeProgram($IdPaymentMode) {
        $db = getDB2();
        return $db->delete('tbl_payment_mode_program', 'IdPaymentMode = '. $IdPaymentMode);
    }

    public function flushPaymentModeCompany($IdPaymentMode) {
        $db = getDB2();
        return $db->delete('tbl_payment_mode_company', 'IdPaymentMode = '. $IdPaymentMode);
    }

    public function flushPaymentModeCategory($IdPaymentMode) {
        $db = getDB2();
        return $db->delete('tbl_payment_mode_category', 'IdPaymentMode = '. $IdPaymentMode);
    }

    public function deletePaymentMode($IdPaymentMode) {
        return $this->delete($this->_primary .' = '.$IdPaymentMode);
    }

    public function getSelectCategory($IdPaymentMode) {
        $db = getDB2();
        $query = $db->select()
            ->from(array('pmc'=>'tbl_payment_mode_category'))
            ->where("pmc.IdPaymentMode = $IdPaymentMode");
        $result = $db->fetchAll($query);
       
        $data = array();

        foreach($result as $category) {
            $data[] = $category['IdCategory'];
        }
        return $data;
    }

    public function getSelectCompany($IdPaymentMode) {
        $db = getDB2();
        $query = $db->select()
            ->from(array('pmc'=>'tbl_payment_mode_company'))
            ->where("pmc.IdPaymentMode = $IdPaymentMode");
        $result = $db->fetchAll($query);
       
        $data = array();

        foreach($result as $company) {
            $data[] = $company['IdCompany'];
        }
        return $data;
    }

    public function getSelectProgram($IdPaymentMode) {
        $db = getDB2();
        $query = $db->select()
            ->from(array('pmg'=>'tbl_payment_mode_program'))
            ->where("pmg.IdPaymentMode = $IdPaymentMode");
        $result = $db->fetchAll($query);
       
        $data = array();

        foreach($result as $program) {
            $data[] = $program['IdProgram'];
        }
        return $data;
    }
	
	
}