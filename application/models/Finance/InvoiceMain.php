<?php
/**
 * @copyright Copyright (c) 2015, MTCSB
 */

class App_Model_Finance_InvoiceMain extends Zend_Db_Table_Abstract {
	
	/**
	 * The default table name
	 */
	protected $_name = 'invoice_main';
	protected $_primary = "id";

    public function updateData($data, $id)
    {
        $db = getDB2();
        $db->update($this->_name ,$data, $this->_primary . ' = ' . (int)$id);
    }


}