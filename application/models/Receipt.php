<?php

/**
 * @copyright Copyright (c) 2015, MTCSB
 */
class App_Model_Receipt extends Zend_Db_Table_Abstract
{
    /**
     * The default table name
     */
    protected $_name = 'receipt';
    protected $_primary = "rcp_id";

    public function getData($id = null)
    {
        $db = getDB2();

        $selectData = $db->select()
            ->from(array('r' => $this->_name), array('*', 'rcp_UpdDate' => 'UpdDate'))
//					->joinLeft(array('adv'=>'advance_payment'), 'adv.advpy_rcp_id = r.rcp_id',array('advpy_cur_id','advpy_amount'))
//					->joinLeft(array('ca'=>'tbl_currency'), 'ca.cur_id = adv.advpy_cur_id',array('advpy_cur_code'=>'ca.cur_code'))
            ->joinLeft(array('c' => 'tbl_currency'), 'c.cur_id = r.rcp_cur_id')
//					->joinLeft(array('ap'=>'applicant_profile'), 'ap.appl_id = r.rcp_appl_id',array("CONCAT_WS(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) as appl_fullname"))
//					->joinLeft(array('st'=>'tbl_state'), 'st.idState = ap.appl_state')
//					->joinLeft(array('cn'=>'tbl_countries'), 'cn.idCountry = ap.appl_country')
//					->joinLeft(array('at'=>'applicant_transaction'), 'at.at_trans_id = r.rcp_trans_id')
//					->joinLeft(array('app'=>'applicant_program'), 'at.at_trans_id=app.ap_at_trans_id', array())
//					->joinLeft(array('pa'=>'tbl_program'),'app.ap_prog_id=pa.IdProgram',array('applicant_program' => 'pa.ProgramCode','applicant_program_name' => 'pa.ProgramName'))
//					->joinLeft(array('std'=>'tbl_studentregistration'), 'std.IdStudentRegistration = r.rcp_idStudentRegistration')
//					->joinLeft(array('stdf'=>'student_profile'), 'stdf.id = std.sp_id',array("CONCAT_WS(' ',stdf.appl_fname,stdf.appl_mname,stdf.appl_lname) as student_fullname"))
//					->joinLeft(array('ps'=>'tbl_program'), 'ps.IdProgram=std.IdProgram',array('student_program' => 'ps.ProgramCode','student_program_name' => 'ps.ProgramName'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = r.rcp_create_by', array())
            ->joinLeft(array('ts' => 'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name' => 'FullName'))
            ->joinLeft(array('ua' => 'tbl_user'), 'ua.iduser = r.cancel_by', array())
            ->joinLeft(array('tsa' => 'tbl_staffmaster'), 'tsa.IdStaff = ua.IdStaff', array('cancel_name' => 'tsa.FullName'))
            ->joinLeft(array('ub' => 'tbl_user'), 'ub.iduser = r.UpdUser', array())
            ->joinLeft(array('tsb' => 'tbl_staffmaster'), 'tsb.IdStaff = ub.IdStaff', array('update_name' => 'tsb.FullName'))
            ->joinLeft(array('rci' => 'receipt_invoice'), 'rci.rcp_inv_rcp_id=r.rcp_id', array())
            ->joinLeft(array('iv' => 'invoice_main'), 'rci.rcp_inv_invoice_id=iv.id', array('GROUP_CONCAT(iv.bill_number) as invoice_no'))
            ->joinLeft(array('ac' => 'tbl_account_code'), 'ac.ac_id = r.rcp_account_code')
            ->group('r.rcp_id')
            ->order('r.rcp_date desc');

        if ($id != null) {
            $selectData->where("r.rcp_id = ?", (int)$id);
            //$row = $db->fetchRow($selectData);
        }
        /*else{
            $row = $db->fetchAll($selectData);

        }*/


        $row = $db->fetchAll($selectData);

        if ($row) {
            $m = 0;
            foreach ($row as &$receipt) {
                $db = Zend_Db_Table::getDefaultAdapter();

                $selectData = $db->select()
                    ->from(array('p' => 'payment'))
                    ->joinLeft(array('c' => 'tbl_currency'), 'c.cur_id = p.p_cur_id')
                    ->where("p.p_rcp_id = ?", (int)$receipt['rcp_id']);

                $payment = $db->fetchAll($selectData);

                if ($payment) {
                    $row[$m]['payment'] = $payment;
                }

                //receipt invoice

                $selectDataReceipt = $db->select()
                    ->from(array('a' => 'receipt_invoice'))
                    ->joinLeft(array('iv' => 'invoice_main'), 'iv.id = a.rcp_inv_invoice_id', array('*'))
//									->joinLeft(array('ivd'=>'invoice_detail'), 'ivd.id = a.rcp_inv_invoice_dtl_id',array('idDet'=>'ivd.id','*'))
                    ->where("a.rcp_inv_rcp_id = ?", (int)$receipt['rcp_id']);

                $receiptInvoice = $db->fetchAll($selectDataReceipt);

                if ($receiptInvoice) {
                    $row[$m]['receipt_invoice'] = $receiptInvoice;

                    foreach ($receiptInvoice as $n => $rci) {

                        //invoice student
                        $selectStudent = $db->select()
                            ->from(array('a' => 'invoice_student'))
                            ->where("a.invoice_id = ?", (int)$rci['id']);

                        $receiptStudent = $db->fetchAll($selectStudent);

                        if ($receiptStudent) {
                            $row[$m]['receipt_invoice'][$n]['student'] = $receiptStudent;

                            foreach ($receiptStudent as $b => $std) {

                                $selectDetail = $db->select()
                                    ->from(array('a' => 'invoice_detail'))
                                    ->where("a.invoice_main_id = ?", (int)$rci['id'])
                                    ->where("a.invoice_student_id = ?", (int)$std['id']);

                                $receiptDetail = $db->fetchAll($selectDetail);

                                if ($receiptDetail) {
                                    $row[$m]['receipt_invoice'][$n]['student'][$b]['detail'] = $receiptDetail;

                                    foreach ($receiptDetail as $v => $ivd) {
                                        $selectSubject = $db->select()
                                            ->from(array('a' => 'invoice_subject'))
                                            ->where("a.invoice_main_id = ?", (int)$rci['id'])
                                            ->where("a.invoice_detail_id = ?", (int)$ivd['id'])
                                            ->where("a.invoice_student_id = ?", (int)$std['id']);

                                        $receiptSubject = $db->fetchAll($selectSubject);

                                        $row[$m]['receipt_invoice'][$n]['student'][$b]['detail'][$v]['subject'] = $receiptSubject;
                                    }

                                }
                            }
                        }

                    }

                } else {
                    $row[$m]['receipt_invoice'] = null;
                }
                $m++;

            }
        }


//		echo "<pre>";
//		print_r($row);
//		exit;
        return $row[0];
    }

    public function getSearchData($name = '', $id = '', $type = 645)
    {
        $db = Zend_Db_Table::getDefaultAdapter();

        $selectData = $db->select()
            ->from(array('r' => $this->_name))
            ->joinLeft(array('ap' => 'applicant_profile'), 'ap.appl_id = r.rcp_appl_id')
            ->joinLeft(array('at' => 'applicant_transaction'), 'at.at_trans_id = r.rcp_trans_id')
            ->joinLeft(array('std' => 'tbl_studentregistration'), 'std.IdStudentRegistration = r.rcp_idStudentRegistration')
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = r.rcp_create_by', array())
            ->joinLeft(array('ts' => 'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('name' => 'FullName'))
            ->join(array('ac' => 'tbl_account_code'), 'ac.ac_id = r.rcp_account_code')
            ->where('r.rcp_payee_type = ?', $type)
            ->order('r.rcp_no DESC');

        if ($name != '') {
            $selectData->where("concat_ws(' ',ap.appl_fname,ap.appl_mname,ap.appl_lname) like ?", '%' . $name . '%');
        }

        if ($id != '' && $type == 645) {
            $selectData->where("at.at_pes_id like '%" . $id . "%'");
        }

        if ($id != '' && $type == 646) {
            $selectData->where("std.registrationId like '%" . $id . "%'");
        }


        $row = $db->fetchAll($selectData);

        if ($row) {
            foreach ($row as &$receipt) {
                $db = Zend_Db_Table::getDefaultAdapter();

                $selectData = $db->select()
                    ->from(array('p' => 'payment'))
                    ->joinLeft(array('c' => 'tbl_currency'), 'c.cur_id = p.p_cur_id')
                    ->where("p.p_rcp_id = ?", (int)$receipt['rcp_id']);

                $payment = $db->fetchAll($selectData);

                if ($payment) {
                    $receipt['payment'] = $payment;
                }
            }
        }


        return $row;
    }

    public function insert(array $data)
    {

        $auth = Zend_Auth::getInstance();

        if (!isset($data['rcp_create_by'])) {
            $data['rcp_create_by'] = 0;
        }

        $data['rcp_date'] = date('Y-m-d H:i:s');

        return parent::insert($data);
    }

    public function getDataByBerId($ber_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'receipt_invoice'), 'b.rcp_inv_rcp_id = a.rcp_id', array())
            ->join(array('c' => 'invoice_main'), 'c.id = b.rcp_inv_invoice_id', array())
            ->where('c.ber_id = ?', $ber_id);
        return $db->fetchAll($select);
    }
}