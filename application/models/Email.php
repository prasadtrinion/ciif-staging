<?php

class App_Model_Email extends Zend_Db_Table
{

    protected $_name = 'email_que';
    protected $_primary = 'id';



    public function getData($id=0){

        $db = getDB2();

        $id = (int)$id;

        if($id!=0){

            $select = $db->select()
                ->from($this->_name)
                ->where($this->_primary.' = ' .$id);

            $stmt = $db->query($select);
            $row = $stmt->fetch();

            if(!$row){
                throw new Exception("There is No Data");
            }

        }else{
            $select = $db->select()
                ->from($this->_name);

            $stmt = $db->query($select);
            $row = $stmt->fetchAll();

            if(!$row){
                $row =  $row->toArray();
            }
        }

        return $row;
    }

    public function addData($data){

        $db = getDB2();
        $data['date_que'] = date("Y-m-d H:i:s");

        // $queue = array(
        // 	'recepient_email' => $data['recepient_email'],
        // 	'subject' => $data['subject'],
        // 	'content' => $data['content'],
        //     'attachment_path' => $data['attachment_path'],
        //     'attachment_filename' => $data['attachment_filename'],
        // 	'date_que' => date("Y-m-d H:i:s")
        // );

        return $db->insert('email_que',$data);
    }
}