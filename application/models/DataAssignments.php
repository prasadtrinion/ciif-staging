<?php

class App_Model_DataAssignments extends Zend_Db_Table
{

    protected $_name = 'data_assignments';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function updateData($data,$id) {

        $db = $this->getDefaultAdapter();
        //$db->update($this->_name,($data),$this->_primary . ' = ' . (int)$id);
        $db->update($this->_name,($data),$this->_primary . ' = ' . (int)$id);

        /*$db     = getDB2();
            $query = $db->select()
                ->from(array('er' => $this->_name))
                ->where('er.er_id = ?', $id);
            $data = $db->fetchRow($query);
            $db->insert($this->_history, $data);

        return $result;
        */

    }

    public function getCourses($id,$cid,$order = 'a.created_date DESC',$results=false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'data_assignments'))
            ->joinLeft(array('b' => 'user'), 'b.id=a.user_id',array('b.id as u_id','firstname','email'))

            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('title','code'))
            ->joinLeft(array('d' => 'content_block_data'), 'd.data_id=a.parent_id', array('gradepoint','title'))
            ->joinLeft(array('e' => 'data_files'), 'e.id=a.data_id', array('id as data_id','file_url'))
            ->where('a.course_id = ?', $id)
            ->where('d.data_id = ?', $cid)
            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }

    public function getUser($id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'users'))
            ->joinLeft(array('b' => 'data_assignments'), 'a.id=b.grade_by',array('firstname','email'))
            ->order($id);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }

     public function getTitle($cid)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'content_block_data'))
            ->where('a.data_id = ?', $cid)
            ->order('data_id');
            
        $result = $db->fetchRow($select);

            return $result;
    }


    public function getGradepoint($parentid)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'content_block_data'))
            ->where('a.data_id = ?', $parentid);
            //->order('data_id');
            
        $result = $db->fetchRow($select);

            return $result;
    }


    
}
