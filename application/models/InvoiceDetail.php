<?php
class App_Model_InvoiceDetail extends Zend_Db_Table_Abstract {
    /**
     * The default table name
     */
    protected $_name = 'invoice_detail';
    protected $_primary = "id";

    public function getData($id){
        $db = getDB2();
        $selectData = $db->select()
            ->from(array('idtl'=>$this->_name))
            ->where("idtl.id = ?", (int)$id);

        $row = $db->fetchRow($selectData);
        return $row;
    }

    public function getDetail($invoice_main_id,$balcheck=0){

        $db = getDB2();

        $selectData = $db->select()
            ->from(array('id'=>$this->_name))
            ->joinLeft(array('i'=>'invoice_main'), 'i.id = id.invoice_main_id')
            ->joinLeft(array('fi'=>'fee_item'), 'fi.fi_id = id.fi_id')
            ->joinLeft(array('fc'=>'tbl_fee_category'), 'fc.fc_id = fi.fi_fc_id')
            ->joinLeft(array('c'=>'tbl_currency'), 'c.cur_id = id.cur_id')
            ->joinLeft(array('ivs'=>'invoice_subject'), 'ivs.invoice_main_id = id.invoice_main_id and ivs.invoice_detail_id = id.id',array('amount_subject'=>'ivs.amount'))
            ->joinLeft(array('s'=>'tbl_subjectmaster'), 's.IdSubject = ivs.subject_id')
            //->joinLeft(array('sm'=>'tbl_semestermaster'), 'sm.IdSemesterMaster = i.semester')
            //->joinLeft(array('fsi'=>'fee_structure_item'), 'fsi.fsi_item_id = fi.fi_id and fsi.fsi_structure_id = i.fs_id',array())
            ->joinLeft(array('fsi'=>'fee_structure_item'), 'fsi.fsi_id = (SELECT fsifsi.fsi_id FROM fee_structure_item as fsifsi WHERE id.fi_id = fsifsi.fsi_item_id AND fsifsi.fsi_structure_id = i.fs_id LIMIT 1)',array())
            ->joinLeft(array('ic'=>'tbl_definationms'), 'fsi.fsi_registration_item_id = ic.idDefinition', array('item_name'=>'ic.DefinitionDesc','item_code'=>'ic.DefinitionCode'))
            ->joinLeft(array('dsct'=>'discount_detail'), 'dsct.dcnt_invoice_id = i.id and dsct.dcnt_invoice_det_id = id.id',array('dcnt_amount'))
            ->where("i.id = ?", (int)$invoice_main_id);
        //->group("id.id");

        if ( $balcheck )
        {
            $selectData->where("id.balance != '0.00'");
        }

        $row = $db->fetchAll($selectData);
        return $row;
    }

    public function getFeeId($invoice_main_id,$balcheck=0){

        $db = getDB2();

        $selectData = $db->select()
            ->from(array('id'=>$this->_name))
            ->joinLeft(array('i'=>'invoice_main'), 'i.id = id.invoice_main_id')
            ->joinLeft(array('fi'=>'fee_item'), 'fi.fi_id = id.fi_id')
            ->where("i.id = ?", (int)$invoice_main_id);
        //->group("id.id");

        if ( $balcheck )
        {
            $selectData->where("id.balance != '0.00'");
        }

        $row = $db->fetchRow($selectData);
        return $row;
    }

    public function getInvoiceDetailItem($billing_id){
        $db = getDB2();
        $selectData = $db->select()
            ->from(array('im'=>'invoice_main'))
            ->join(array('idtl'=>'invoice_Detail'), 'idtl.invoice_main_id = im.id')
            ->where("im.bill_number ?", $billing_id);

        $row = $db->fetchRow($selectData);

        if(!$row){
            return null;
        }else{
            return $row;
        }
    }

    public function getInvoiceDetail($invoice_id){
        $db = getDB2();
        $selectData = $db->select()
            ->from(array('idtl'=>$this->_name))
            ->join(array('fi'=>'fee_item'), 'fi.fi_id = idtl.fi_id')
            ->where("idtl.invoice_main_id = ?", $invoice_id);

        $row = $db->fetchAll($selectData);

        if(!$row){
            return null;
        }else{
            return $row;
        }
    }
}
?>