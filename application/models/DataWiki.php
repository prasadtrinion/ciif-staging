<?php

class App_Model_DataWiki extends Zend_Db_Table
{

    protected $_name = 'data_wiki';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
}
