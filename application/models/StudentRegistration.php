<?php


class App_Model_StudentRegistration extends Zend_Db_Table_Abstract {
    /**
     * The default table name
     */
    protected $_name = 'tbl_studentregistration';
    protected $_primary = "IdStudentRegistration";


    public function getData($appl_id=0) {

        if($appl_id!=0){
            $db = getDB();
            $select = $db->select()
                ->from(array('sr'=>$this->_name))
                ->where('sr.IdApplication = ?',$appl_id)
                ->where('sr.profileStatus = IN (92,248)'); //Active

            $stmt = $db->query($select);
            $row = $stmt->fetchRow();

        }else{
            $row = $this->fetchAll();
            $row=$row->toArray();
        }

        if(!$row){
            throw new Exception("There is No Information Found");
        }
        return $row;
    }

    public function getDataFromProfile($sp_id) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('sr'=>$this->_name))
            ->where('sr.sp_id = ?',$sp_id)
            ->where('sr.profileStatus IN (92,248,96)'); //Active

        $result = $db->fetchAll($select);
        return $result;
        //return $row;
    }

    public function getDataStudentRegistration($sp_id) {

        $db = getDB();

        $select = $db->select()
            ->from(array('sr'=>$this->_name))
            ->joinLeft(array("ts" => "tbl_definationms"),'ts.idDefinition=sr.TransactionStatus', array('StatusName'=>'DefinitionDesc'))
            ->where('sr.sp_id = ?',$sp_id)
        ->where('sr.IdRegistrationType IN (2,3)');
         //   ->where('sr.profileStatus IN (92,248,96)'
          //  //); //Active
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getTheStudentRegistrationDetail($id) {
        $db = getDB2();
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregistration'), array('a.registrationId', 'a.IdProgram','at_pes_id'=>'a.registrationId','*'))
            ->joinLeft(array('StudentProfile' => 'student_profile'), "a.sp_id = StudentProfile.std_id")
            ->joinLeft(array('b' => 'tbl_program'), "a.IdProgram = b.IdProgram", array("b.ProgramName", "b.IdScheme"))
            //->joinLeft(array('c' => 'tbl_intake'), "a.IdIntake = c.IdIntake", array("c.IntakeDesc"))
            ->joinLeft(array('d' => 'tbl_definationms'), "a.profileStatus = d.idDefinition", array("d.DefinitionDesc"))
            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme=a.IdProgramScheme', array())
            ->joinLeft(array("dp" => "tbl_definationms"),'dp.idDefinition=ps.mode_of_program', array('ProgramMode'=>'DefinitionDesc','ProgramModeMy'=>'BahasaIndonesia'))
            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=ps.mode_of_study', array('StudyMode'=>'DefinitionDesc','StudyModeMy'=>'BahasaIndonesia'))
            ->joinLeft(array("dt" => "tbl_definationms"),'dt.idDefinition=ps.program_type', array('ProgramType'=>'DefinitionDesc','ProgramTypeMy'=>'BahasaIndonesia'))
            ->joinLeft(array("fs" => "fee_structure"),'fs.fs_id=a.fs_id', array('fs_name'))
            ->joinLeft(array('o'=>'student_financial'), 'a.IdStudentRegistration = o.sp_id')
            ->joinLeft(array('u'=>'tbl_definationms'), 'o.af_method = u.idDefinition', array('afMethodName'=>'u.DefinitionDesc'))
            ->joinLeft(array('v'=>'tbl_definationms'), 'o.af_type_scholarship = v.idDefinition', array('typeScholarshipName'=>'v.DefinitionDesc'))
            ->joinLeft(array('w'=>'tbl_scholarship_sch'), 'o.af_scholarship_apply = w.sch_Id', array('applScholarshipName'=>'w.sch_name'))
            //->joinLeft(array("dc" => "tbl_definationms"),'dc.idDefinition=StudentProfile.appl_category', array('StudentCategory'=>'DefinitionDesc'))
            ->joinLeft(array("st" => "tbl_definationms"),'st.idDefinition=a.profileStatus', array('StatusName'=>'DefinitionDesc'))
            ->where("a.IdStudentRegistration =?", $id);
        $row = $db->fetchRow($select);
        return $row;
    }

    public function getStudentInfo($id=0,$by='sr.IdStudentRegistration') {

        $db = getDB2();
        $select = $db->select()
            ->from(array('sr'=>$this->_name))
            ->join(array('ap'=>'student_profile'),'ap.std_id=sr.sp_id')
            ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ArabicName','ProgramName','ProgramCode','IdProgram','IdScheme'))
            ->join(array('s'=>'tbl_scheme'),'p.IdScheme=s.IdScheme', array('SchemeCode','EnglishDescription as SchemeName'))
            //->join(array('i'=>'tbl_intake'),'i.IdIntake=sr.IdIntake',array('intake'=>'IntakeDefaultLanguage','IntakeId', 'sem_seq'))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=sr.profileStatus',array('StudentStatus'=>'DefinitionDesc'))
            ->joinLeft(array('pm'=>'tbl_programmajoring'),'pm.IDProgramMajoring=sr.IDProgramMajoring',array('majoring'=>'BahasaDescription'))
            ->joinLeft(array('b'=>'tbl_branchofficevenue'),'b.IdBranch=sr.IdBranch',array('BranchName','invoice','BranchCode'))
            ->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege=p.IdCollege',array('CollegeName'=>'ArabicName','c.IdCollege'))
            ->joinLeft(array('sm'=>'tbl_staffmaster'),'sm.IdStaff=sr.AcademicAdvisor',array('AcademicAdvisor'=>'FullName',"FrontSalutation","BackSalutation", 'AdvisorEmail'=>'sm.Email'))
            ->join(array("aps" => "tbl_program_scheme"),'aps.IdProgramScheme=sr.IdProgramScheme', array('*'))
            ->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=aps.mode_of_study', array('StudyMode'=>'ds.DefinitionDesc'))
            ->joinLeft(array("dss" => "tbl_definationms"),'dss.idDefinition=aps.mode_of_program', array('ProgramMode'=>'dss.DefinitionDesc'))
            //->joinLeft(array('city'=>'tbl_city'),'city.idCity=ap.appl_city',array('CityName'))
            //->joinLeft(array('state'=>'tbl_state'),'state.IdState=ap.appl_state',array('StateName'))
            //->joinLeft(array('ctrs'=>'tbl_countries'),'ctrs.idCountry=ap.appl_country',array('CountryName'))
            //->joinLeft(array('city2'=>'tbl_city'),'city2.idCity=ap.appl_ccity',array('CCityName'=>'CityName'))
            //->joinLeft(array('state2'=>'tbl_state'),'state2.IdState=ap.appl_cstate',array('CStateName'=>'StateName'))
            //->joinLeft(array('ctrs2'=>'tbl_countries'),'ctrs2.idCountry=ap.appl_ccountry',array('CCountryName'=>'CountryName'))

            ->where($by.' = ?',$id)
            ->where('sr.profileStatus IN (92,248,96)'); //Active


        $row = $db->fetchRow($select);

        return $row;
    }


    /*
    * This function to get course registered by semester.
    */
    public function getCourseRegisteredBySemester($registrationId,$idSemesterMain,$idBlock=null){

        $db = getDB2();

        $sql = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))
            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
            ->joinLeft(array('cg'=>'tbl_course_tagging_group'),'cg.IdCourseTaggingGroup = srs.IdCourseTaggingGroup',array())
            ->joinLeft(array('u'=>'tbl_staffmaster'),'u.IdStaff = cg.IdLecturer ',array('lecturername'=>'FullName','IdStaff'))
            ->where('sr.IdStudentRegistration = ?', $registrationId)
            //->where("srs.subjectlandscapetype != 2")
            ->where('srs.IdSemesterMain = ?',$idSemesterMain);

        if(isset($idBlock) && $idBlock != ''){ //Block
            $sql->where("srs.IdBlock = ?",$idBlock);
            $sql->order("srs.BlockLevel");
        }


//        echo $sql;
        $result = $db->fetchAll($sql);
        return $result;
    }


    public function getCourseRegisteredBySemesterEvaluation($registrationId,$idSemesterMain,$idBlock=null){

        $db = getDB2();

        $sql = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))
            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
            ->joinLeft(array('cg'=>'tbl_course_tagging_group'),'cg.IdCourseTaggingGroup = srs.IdCourseTaggingGroup',array())
            ->joinLeft(array('u'=>'tbl_staffmaster'),'u.IdStaff = cg.IdLecturer ',array('lecturername'=>'FullName','IdStaff'))
            ->where('sr.IdStudentRegistration = ?', $registrationId)
            //->where("srs.subjectlandscapetype != 2")
            ->where('srs.IdSemesterMain = ?',$idSemesterMain)
            ->where('srs.Active NOT IN (3)')
//			->where('sm.CourseType NOT IN (3,19,2)') // coursetype GD & PPP & articleship
            ->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
            ->where("IFNULL(srs.exam_status,'x') NOT IN ('CT','EX','U')");

        if(isset($idBlock) && $idBlock != ''){ //Block
            $sql->where("srs.IdBlock = ?",$idBlock);
            $sql->order("srs.BlockLevel");
        }

        $sql2 = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))
            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
            ->joinLeft(array('cg'=>'tbl_course_tagging_group'),'cg.IdCourseTaggingGroup = srs.IdCourseTaggingGroup',array())
            ->joinLeft(array('u'=>'tbl_staffmaster'),'u.IdStaff = cg.IdLecturer ',array('lecturername'=>'FullName','IdStaff'))
            ->where('sr.IdStudentRegistration = ?', $registrationId)
            //->where("srs.subjectlandscapetype != 2")
            ->where('srs.audit_program IS NOT NULL')
            ->where('srs.IdSemesterMain = ?',$idSemesterMain)
            ->where('srs.Active NOT IN (3)')
//			->where('sm.CourseType NOT IN (3,19,2)') // coursetype GD & PPP & articleship
            ->where('sm.CourseType IN (1)') // coursetype GD & PPP & articleship
            ->where("IFNULL(srs.exam_status,'x') IN ('U')");

        if(isset($idBlock) && $idBlock != ''){ //Block
            $sql2->where("srs.IdBlock = ?",$idBlock);
            $sql2->order("srs.BlockLevel");
        }

        $select = $db->select()
            ->union(array($sql, $sql2));

        $result = $db->fetchAll($select);
        return $result;
    }

    /*
     * Get semester regular for particular semester
     */
    function getSemesterRegular($idsemmain,$idstudreg){
        $db = getDB2();
        $sql = "SELECT * FROM `tbl_semestermaster` sm
		inner join tbl_studentsemesterstatus as sss on sss.IdSemesterMain=sm.IdSemesterMaster
		inner join(
		select idacadyear,semestercounttype from tbl_semestermaster where idsemestermaster=".$idsemmain." )semmaster
			on sm.idacadyear=semmaster.idacadyear and sm.semestercounttype=semmaster.semestercounttype
			WHERE
			sss.idstudentregistration = $idstudreg and
			semesterfunctiontype = 0
			";
        $row = $db->fetchRow($sql);
        return $row;
    }


    /*
     * This function to get course registered by semester.
     */
    public function getCourseRegisteredByBlock($registrationId,$idBlock=null){

        $db = getDB2();

        $sql = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))
            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
            ->where('sr.IdStudentRegistration  = ?', $registrationId)
            ->where("srs.IdBlock = ?",$idBlock)
            ->order("srs.BlockLevel");

        $result = $db->fetchAll($sql);
        return $result;
    }


    public function updateData($data,$id){
       // $this->update($data, $this->_primary .' = '. (int)$id);

        $db = getDB2();
        $db->update($this->_name ,$data, $this->_primary . ' = ' . (int)$id);
    }


    public function getCourseRegisteredBySemesterBlock($registrationId,$idSemester,$idBlock=null){

        $db = getDB2();

        $sql = $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))
            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
            ->where('sr.IdStudentRegistration  = ?', $registrationId)
            ->where("srs.IdSemesterMain = ?",$idSemester)
            ->where("srs.IdBlock = ?",$idBlock)
            ->where("srs.subjectlandscapetype != 2")
            ->order("srs.BlockLevel");

        $result = $db->fetchAll($sql);
        return $result;
    }

    public function getCourseRegisteredBySemesterId($registrationId,$idSemesterMain,$idBlock=null){

        $db = getDB2();

        /*$sql = $db->select('SUM CreditHours AS Total')
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
                        ->where('srs.IdSemesterMain IN ('.$idSemesterMain.')')
                        ->where('srs.subjectlandscapetype != 2');*/
        $sql = $db->select()
            ->distinct()
            ->from(array('sr' => 'tbl_studentregistration'), array())
            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode'))
            ->where('sr.IdStudentRegistration = ?', $registrationId)
            ->where('srs.IdSemesterMain IN ('.$idSemesterMain.')');
        //->where('srs.subjectlandscapetype != 2');

        if(isset($idBlock) && $idBlock != ''){ //Block
            $sql->where("srs.IdBlock = ?",$idBlock);
            $sql->order("srs.BlockLevel");
        }

        //echo $sql;

        $result = $db->fetchAll($sql);
        return $result;
    }

    public function getSubjectSchedule($id,$idGroup)
    {
        $db = getDB2();

        $sql = $db->select()
            ->from(array('a' => 'tbl_course_tagging_group'))
            ->joinRight(array('b' => 'course_group_schedule'),'b.idGroup = a.IdCourseTaggingGroup')
            ->where('a.idSubject = ?',(int)$id)
            ->where('a.IdCourseTaggingGroup = ?',(int)$idGroup);
        //echo $sql;
        $row = $db->fetchAll($sql);

        return $row;
    }

    public function getInfo($idGroup){

        $db = getDB2();

        $select = $db ->select()
            ->from(array('cg'=>'tbl_course_tagging_group'))
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'subjectMainDefaultLanguage','faculty_id'=>'IdFaculty'))
            ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
            ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
            ->where('IdCourseTaggingGroup = ?',$idGroup);
        $row = $db->fetchRow($select);
        return $row;
    }

    public function getTotalStudentRegistered($idSubject,$idSemester,$idGroup)
    {
        $db = getDB2();
        $sql =   $db->select()
            ->from('tbl_studentregsubjects',array('Total' => ('COUNT(*)')))
            //->columns()
            ->where('IdSemesterMain = ?',(int)$idSemester)
            ->where('IdCourseTaggingGroup = ?',(int)$idGroup)
            ->where('IdSubject = ?',(int)$idSubject);

        $row =  $db->fetchRow($sql);
        //echo $sql;
        return $row;
    }

    public function getAcademicAdvisor($id){
        $db = getDB2();
        $select = $db->select()
            ->from(array('a'=>'tbl_staffmaster'), array('FullName'))
            ->where('a.IdStaff = ?', $id);

        $result = $db->fetchOne($select);
        return $result;
    }

    public function getConvocationData($id){
        $db = getDB2();
        $select = $db->select()
            ->from(array('a'=>'pregraduate_list'), array('value'=>'*'))
            ->joinLeft(array('b'=>'convocation'), 'a.c_id = b.c_id')
            ->where('a.idStudentRegistration = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStudentSubjectsInfo($idstudentregistration)
    {

        $db = getDB2();
        $sql =   $db->select()
            ->from('tbl_studentregsubjects',array('IdStudentRegSubjects' => 'IdStudentRegSubjects'))
            //->columns()
            ->where('IdStudentRegistration = ?',(int)$idstudentregistration)
           // ->where('IdCourseTaggingGroup = ?',(int)$idGroup)
           // ->where('IdSubject = ?',(int)$idSubject)
        ;

        $row =  $db->fetchRow($sql);
        return $row;
    }


    public function addRegSubjectsData($table, $data){
        $db = getDB2();
        $id = $db->insert($table, $data);
        return $id;
    }

    public function checkStudentRegSubjects($idstudreg=0,$idsubject=0) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('sr'=>'tbl_studentregsubjects'))
            ->where('sr.IdStudentRegistration = ?',$idstudreg)
            ->where('sr.IdSubject = ?',$idsubject);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getInfoStudentRegistration($txnid) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('sr'=>$this->_name))
            ->where('sr.IdTransaction = ?',$txnid);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStudentSubjectsDetail($idstudentregistration)
    {

        $db = getDB();
        $sql =   $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('IdProgram'))
            ->join(array('p' => 'tbl_program'), 'p.IdProgram=sr.IdProgram', array('ProgramName'))
            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('ExemptionStatusSubject' => 'ExemptionStatus','IdStudentRegSubjects'))
            ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode','IdSubject'))
            //->from('tbl_studentregsubjects',array('IdStudentRegSubjects' => 'IdStudentRegSubjects'))
            //->columns()
            ->where('sr.sp_id = ?',(int)$idstudentregistration)
            // ->where('IdCourseTaggingGroup = ?',(int)$idGroup)
            // ->where('IdSubject = ?',(int)$idSubject)
        ;
//echo $sql;exit;
        $row =  $db->fetchAll($sql);
        return $row;
    }

    public function getInfoStudentRegistrationSp($txnid) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('sr'=>$this->_name))
            ->join(array('sp'=>'student_profile'), 'sp.std_id = sr.sp_id',array('std_corporate_id'))
            ->where('sr.IdTransaction = ?',$txnid);
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getScheduleExam($idProgram,$idSubject,$selected_exam_sitting=null)
    {
		//ini cara nak mongok sebab cepat kalau betul2 develpment please tukar yea
        $db = getDB2();
        $select ="SELECT sts_id as exam_tagging_slot,sl.sl_id,sl.sl_name,s.es_date,sl.sl_starttime,sl.sl_endtime,sm.SubjectName,sm.IdSubject,esd.esd_id,sts.sts_slot_id, es.es_id,s.es_id as schedule_id
						FROM `exam_setup` as es
						join `exam_setup_detail` as esd ON es.es_id = esd.es_id
						join exam_schedule as s ON s.es_esd_id = esd.esd_id
						join exam_scheduletaggingslot as sts ON sts.sts_schedule_id = s.es_id
						join exam_slot as sl ON sl.sl_id = sts.sts_slot_id
						join tbl_subjectmaster as sm ON sm.IdSubject=esd.esd_idSubject
						WHERE es.es_idProgram = '".$idProgram."' 
						and esd_idSubject='".$idSubject."'
						group by es_date,esd_idSubject
						order by es_date,sl_starttime,sl.sl_endtime";
        
        if($selected_exam_sitting){        
            $select = "
            			SELECT sts_id as exam_tagging_slot,sl.sl_id,sl.sl_name,s.es_date,sl.sl_starttime,sl.sl_endtime,sm.SubjectName,sm.IdSubject,esd.esd_id,sts.sts_slot_id, es.es_id,s.es_id as schedule_id
						FROM `exam_setup` as es
						join `exam_setup_detail` as esd ON es.es_id = esd.es_id
						join exam_schedule as s ON s.es_esd_id = esd.esd_id
						join exam_scheduletaggingslot as sts ON sts.sts_schedule_id = s.es_id
						join exam_slot as sl ON sl.sl_id = sts.sts_slot_id
						join tbl_subjectmaster as sm ON sm.IdSubject=esd.esd_idSubject
						WHERE es.es_idProgram = '".$idProgram."' 						
						and esd_idSubject='".$idSubject."'
						AND YEAR(s.es_date)='".$selected_exam_sitting['est_year']."' AND MONTH(s.es_date) = '".$selected_exam_sitting['est_month']."'
						group by es_date,esd_idSubject
						order by es_date,sl_starttime,sl.sl_endtime
            			";
        }
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getExamCenter($sl_id){
    	
    	$db = getDB2();
    	$select = $db->select()
		            ->from(array('c'=>'exam_taggingslotcenterroom'))
		            ->join(array('ec'=>'tbl_exam_center'),'ec.ec_id=c.tsc_examcenterid',array('ec_name'))
		            ->where('c.tsc_slotid = ?',$sl_id);
        $result = $db->fetchAll($select);
        return $result;
    }
    
 	public function getRegistrationDetails($IdStudentRegistration) {

        $db = getDB2();

        $select = $db->select()
            ->from(array('sr'=>$this->_name))
            ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ProgramName'))           
            ->where('sr.IdStudentRegistration = ?',$IdStudentRegistration);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getStudentSubjects($idstudentregistration,$IdProgram) {

        $db = getDB2();
        $sql =   $db->select()
            ->from(array('sr' => 'tbl_studentregistration'), array('IdProgram'))
            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('ExemptionStatusSubject' => 'ExemptionStatus','IdStudentRegSubjects'))
            ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode','IdSubject'))
            ->where('sr.sp_id = ?',(int)$idstudentregistration)
            ->where('sr.IdProgram = ?',(int)$IdProgram)
        ;
        $row =  $db->fetchAll($sql);
        return $row;
    }
    
	public function getRegistrationLandscapeDetails($IdStudentRegistration) {
            $IdStudentRegistration = 205;
        $db = getDB();

        $select = $db->select()
            ->from(array('sr'=>$this->_name))
            ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ProgramName'))    
            ->join(array('l'=>'tbl_landscape'),'l.IdLandscape=sr.IdLandscape',array('MaxExamSitting'))       
            ->where('sr.IdStudentRegistration = ?',$IdStudentRegistration);
        $result = $db->fetchRow($select);
        return $result;
    }

}

?>
