<?php
/**
 * @copyright Copyright (c) 2015, MTCSB
 */
class App_Model_TransactionMigsDetail extends Zend_Db_Table_Abstract {

    /**
     * The default table name
     */
    protected $_name = 'transaction_migs_detail';
    protected $_primary = "tmd_id";


    public function getData($migsId, $invoiceId)
    {
        $db = getDB2();
        $selectData = $db->select()
            ->from(array('im' => $this->_name))
            ->where("im.tmd_tm_id = ?", (int)$migsId)
            ->where("im.tmd_invoice_id = ?", (int)$invoiceId);

        $row = $db->fetchRow($selectData);
        return $row;
    }


}