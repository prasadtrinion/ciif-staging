<?php

class App_Model_OrderGroup extends Zend_Db_Table
{

    protected $_name = 'order_group';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getGroup(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }

        $result = $db->fetchRow( $select );

        return $result;
    }

    public function getData(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }

        $result = $db->fetchAll( $select );

        return $result;
    }
}
