<?php

class App_Model_Enrol extends Zend_Db_Table
{

    protected $_name = 'enrol';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getEnrol(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }
        
        $select->order('a.id DESC');

        $result = $db->fetchRow( $select );

        return $result;
    }

    public function getEnrols(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }

        $result = $db->fetchAll( $select );

        return $result;
    }

    public function getEnrolledCourses($user_id, $cur_id = 0, $all = true)
    {
        $db = $this->getDefaultAdapter();
        $dbSms = getDB();

        $select = $db->select()

            ->from(array('a'=>$this->_name), array('a.created_date as enrolled_date','a.expiry_date','a.active', 'a.learningmode', 'a.activity_progress','a.curriculum_id','a.schedule','IdEnrol'=>'a.id'))

            ->join(array('c'=>'courses'),'c.id=a.course_id')
            ->join(array('u'=>'user'),'a.user_id = u.id', array('sp_id'=>'external_id'))
            ->where('a.user_id = ?', $user_id)
            ->where('a.regtype = ?','course')
            ->order('a.course_id ASC');

        if (!$all) {
//            $select->where('a.active = ?', 1)->where('a.completed = ?', 0);
            $select->where('a.completed = ?', 0);
        }

//        if($cur_id){
            $select->where('a.curriculum_id = ?',$cur_id);
//        }
//        echo $select;
        $dataArray = array();
        $result = $db->fetchAll( $select );

        $dataArray = $result;
        if($result){
            foreach($result as $a=>$data){
                $mop = ($data['learningmode'] == 'FTF' ? 578 : 577);

                //get IdStudentRegistration from sms
                $selectSms = $dbSms->select()
                    ->from(array('a' => 'tbl_studentregistration'))
                    ->join(array('b' => 'tbl_studentregsubjects'),'a.IdStudentRegistration = b.IdStudentRegistration')
                    ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = a.IdProgramScheme', array('mode_of_program'))
                    ->where('a.sp_id = ?', $data['sp_id'])
                    ->where('b.IdSubject = ?', $data['external_id'])
                    ->where('c.mode_of_program = ?', $mop)
                    ->group('b.IdStudentRegSubjects');

                $resultReg = $dbSms->fetchRow( $selectSms );

                if($resultReg) {
                    $dataArray[$a]['IdStudentRegistration'] = $resultReg['IdStudentRegistration'];

                    //update
                    $this->update(array('external_id' => $resultReg['IdStudentRegSubjects']), array('id = ?' => $data['IdEnrol']));

                }else{
                    //insert into db sms//

                    if($data['curriculum_id']
                       // && ($data['expiry_date'] == NULL || $data['expiry_date'] == '')
                        ) {

                        //get curriculum info
                        $currDb = new App_Model_Curriculum();
                        $courseDb = new App_Model_Courses();

                        $cur = $currDb->getCurriculum(array('id = ?' => $data['curriculum_id']));
                        $course = $courseDb->getCourse(array('a.id = ?'=>$data['id']));

                        if ($cur) {

                            //scheme & landscape
                            $params = array(
                                'IdProgram' => $cur['external_id'],
                                'IdMode' => ($data['learningmode'] == 'OL') ? 577 : 578,
                            );

                            $externalData = Cms_Curl::__(SMS_API . '/get/Registration/do/externalData', $params);

                            if (empty($externalData)) {
                                return Cms_Render::error('Error Fetching Data. #extd');
                            }

                            $params = array(
                                'sp_id' => $data['sp_id'],
                                'IdProgram' => $cur['external_id'],
                                'IdProgramScheme' => $externalData['scheme']['IdProgramScheme'],
                                'IdLandscape' => $externalData['landscape']['IdLandscape']
                            );

                            $regStudent = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewReg', $params);

                            $dataArray[$a]['IdStudentRegistration'] = $regStudent['IdStudentRegistration'];

                            //register
                            $register_external = array();

                            $register_external[] = array(
                                'lms_course_id'             =>  $course['external_id'],
                                'course_id'                 => $course['id']
                            );

                            $paramsRegSubject = array('courses' => $register_external, 'check' => 1, 'student_id' => $regStudent['IdStudentRegistration'],'Active'=>1);

                            $schedule = array();

                            $course_schedule = $schedule[$data['external_id']] = isset($data['schedule']) ? $data['schedule'] : array();

                            $paramsRegSubject = $paramsRegSubject + array('schedule' => $course_schedule);

                            $registered_external = Cms_Curl::__(SMS_API . '/get/Registration/do/addSubject', $paramsRegSubject);

                            //update
                            $this->update(array('external_id' => $registered_external['registered'][$data['id']]), array('id = ?' => $data['IdEnrol']));

                        }
                    }
                }
            }
        }

        return $dataArray;
    }

    public function getCompletedCourses($user_id, $cur_id = 0)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()

            ->from(array('a'=>$this->_name), array('a.created_date as enrolled_date','a.completed_date','a.expiry_date','a.active','a.learningmode', 'a.schedule', 'a.activity_progress'))
            ->join(array('c'=>'courses'),'c.id=a.course_id')
            ->where('a.user_id = ?', $user_id)
            ->where('a.regtype = ?','course')
            //->where('a.curriculum_id = ?',$cur_id)
//            ->where('a.active = ?', 0)
            ->where('a.completed = ?', 1)

            ->order('a.created_date DESC');

        $result = $db->fetchAll( $select );

        return $result;
    }

    public function getEnrolledProgramme($user_id, $all = true)
    {
        // echo $user_id;
        // exit;

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>$this->_name), array('a.created_date as enrolled_date','a.expiry_date','a.active','a.order_id','a.learningmode'))
            ->joinLeft(array('c'=>'curriculum'),'c.id=a.curriculum_id')
            ->where('a.user_id = ?', $user_id)
            ->where('a.regtype = ?','curriculum')//ni ori
           // ->where('a.regtype  IN ("curriculum","course")')
            ->order('a.created_date DESC');

        if (!$all) {
            // $select->where('a.active = ?', 1)->where('a.completed = ?', 0);
            $select->where('a.completed = ?', 0);
        }
       // echo $select;
        $result = $db->fetchAll( $select );


        return $result;
    }

    public function getCompletedProgramme($user_id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>$this->_name), array('a.created_date as enrolled_date','a.completed_date','a.expiry_date','a.active','a.order_id','a.learningmode'))
            ->join(array('c'=>'curriculum'),'c.id=a.curriculum_id')
            ->where('a.user_id = ?', $user_id)
            ->where('a.regtype = ?','curriculum')
//            ->where('a.active = ?', 0)
            ->where('a.completed = ?', 1)
            ->order('a.created_date DESC');

        $result = $db->fetchAll( $select );

        return $result;
    }

    public function getEnrolledExam($user_id, $cur_id = 0, $all = true)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>$this->_name), array('a.created_date as enrolled_date','a.expiry_date','a.active'))
            ->join(array('c'=>'courses'),'c.id=a.course_id')
            ->where('a.user_id = ?', $user_id)
            ->where('a.regtype = ?','exam')
            ->order('a.created_date DESC');

        if (!$all) {
//            $select->where('a.active = ?', 1) ->where('a.completed = ?', 0);
            $select->where('a.completed = ?', 0);
        }

//        if($cur_id){
            $select->where('a.curriculum_id = ?',$cur_id);
//        }

        $result = $db->fetchAll( $select );

        return $result;
    }

    public function getActiveStudentCount($course_id)
    {
        $db = $this->getDefaultAdapter();

        $enroldata = $db->select()
                ->from(array('a' => 'enrol'))
                ->where('a.course_id = ?', $course_id)
                ->where('active = ?', 1)
                ->group('a.user_id');

        $enrol = $db->fetchAll($enroldata);

        return $enrol;
    }


    public function updateProgress($id, $percentage = 0, $external_id = 0) {
        if(!$percentage || !$external_id) {
            return false;
        }

        $data = array(
            'activity_progress' => $percentage,
            'completed'         => 0,
            'completed_date'    => null
        );

        if ($percentage >= 100) {
            $data['completed']      = 1;
            $data['completed_date'] = new Zend_Db_Expr('UTC_TIMESTAMP()');
        }

        $this->update($data, array('id = ?' => $id));

        $params = array (
            'IdStudentRegSubjects' => $external_id,
            'lms'                  => 1,
            'set_completed'        => $data['completed']
        );

        $response = Cms_Curl::__(SMS_API . '/get/Registration/do/updateModuleStatus', $params);
    }

    public function getEnrolledProgrammeSimple($user_id)
    {
        $db     = $this->getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>$this->_name), array())
            ->join(array('c'=>'curriculum'),'c.id=a.curriculum_id', array('id', 'name', 'code'))
            ->where('a.user_id = ?', $user_id)
            ->where('a.regtype = ?','curriculum')
            ->order('c.name ASC');
        $result = $db->fetchAll( $select );

        return $result;
    }

    public function getEnrolledCoursesSimple($user_id, $cur_id)
    {
        $db     = $this->getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>$this->_name), array())
            ->join(array('c'=>'courses'),'c.id=a.course_id', array('id', 'title', 'code'))
            ->where('a.user_id = ?', $user_id)
            ->where('a.curriculum_id = ?',$cur_id)
            ->where('a.regtype = ?','course')
            ->order('c.code ASC');
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getEnrolData($enrol_id)
    {
        $db     = $this->getDefaultAdapter();
        $select = $db->select()
            ->from(array('a'=>$this->_name), array())
            ->join(array('cr'=>'curriculum'),'cr.id = a.curriculum_id', array('curriculum_name'=>'name','curriculum_code'=>'code'))
            ->join(array('c'=>'courses'),'c.id=a.course_id', array('course_name'=>'name','curriculum_code'=>'code'))
            ->joinLeft(array('o'=>'order'),'o.id = a.order_id', array('invoice_no','invoice_id','paymentmethod','amount'))
            ->where('a.user_id = ?', $enrol_id);
        $result = $db->fetchRow($select);

        return $result;
    }

    public function saveEnrol($data) {
        $db = $this->getDefaultAdapter();

        $bookmark_id = $db->insert($this->_name, $data);
    }
}
