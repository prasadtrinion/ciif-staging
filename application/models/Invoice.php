<?php
class App_Model_Invoice{

    public function generateInvoiceConvocation($studentID, $convoID, $feeID, $guestNo = 1)
    {

        /**
         * function to generate convocation fee
         */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = getDB2();

        //checking CP
        $studentRegDB = new App_Model_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $checkingInvoice = $studentInfo['invoice'];
        $invoice_id = null;
        if ($checkingInvoice == 1) {

            //get fee info
            $feeConvoDB = new App_Model_Convocation();
            $feeConvoData = $feeConvoDB->getDataConvoFee($convoID, $feeID);

            if ($feeConvoData) {
                $amountAdd = $feeConvoData['fv_amount'];
                if ($guestNo > 1) {
                    $amountAdd = $amountAdd * $guestNo;
                }

                $curID = $feeConvoData['fv_currency_id'];
                $feeDesc = $feeConvoData['fi_name'] . ' for Session ' . $feeConvoData['c_session'] . '/' . $feeConvoData['c_year'];
                $feeID = $feeConvoData['fv_fi_id'];

                $currencyDb = new App_Model_CurrencyRate();
                $currency = $currencyDb->getCurrentExchangeRate($curID);

                //get current semester
               /* $profile = array('ap_prog_scheme' => $studentInfo['IdScheme'], 'branch_id' => $studentInfo['IdBranch']);

                $semesterDB = new App_Model_General_DbTable_Semestermaster();
                $semester = $semesterDB->getApplicantCurrentSemester($profile);
                $semesterID = $semester['IdSemesterMaster'];*/

                $db->beginTransaction();
                try {

                    //insert into invoice_main
                    $invoiceMainDb = new App_Model_InvoiceMain();

                    $bill_no = $this->getBillSeq(2, date('Y'));
                    $data_invoice = array(
                        'bill_number' => $bill_no,
                        'appl_id' => isset($studentInfo['appl_id']) ? $studentInfo['appl_id'] : 0,
                        'trans_id' => isset($studentInfo['transaction_id']) ? $studentInfo['transaction_id'] : 0,
                        'IdStudentRegistration' => isset($studentInfo['IdStudentRegistration']) ? $studentInfo['IdStudentRegistration'] : 0,
                        'academic_year' => '',
                        'bill_amount' => $amountAdd,
                        'bill_paid' => 0,
                        'bill_balance' => $amountAdd,
                        'bill_description' => $feeDesc,
                        'program_id' => $studentInfo['IdProgram'],
                        'fs_id' => $studentInfo['fs_id'],
                        'currency_id' => $curID,
                        'exchange_rate' => $currency['cr_exchange_rate'],
                        'invoice_date' => date('Y-m-d'),
                        'date_create' => date('Y-m-d H:i:s'),
                        'creator' => $creator//by admin
                    );


                    $invoice_id = $invoiceMainDb->insert($data_invoice);

                    //insert invoice detail
                    $invoiceDetailDb = new App_Model_InvoiceDetail();

                    $invoiceDetailData = array(
                        'invoice_main_id' => $invoice_id,
                        'fi_id' => $feeID,
                        'fee_item_description' => $feeConvoData['fi_name'],
                        'cur_id' => $curID,
                        'amount' => $amountAdd,
                        'balance' => $amountAdd,
                        'exchange_rate' => $currency['cr_exchange_rate']
                    );

                    $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                    $db->commit();

                } catch (Exception $e) {

                    $db->rollBack();

                    //save error message
                    $sysErDb = new App_Model_SystemError();
                    $msg['se_IdStudentRegistration'] = $studentID;
                    $msg['se_title'] = 'Error generate invoice';
                    $msg['se_message'] = $e->getMessage();
                    $msg['se_createdby'] = $creator;
                    $msg['se_createddt'] = date("Y-m-d H:i:s");
                    $sysErDb->addData($msg);

                    throw $e;
                }
            }
        }
        return $invoice_id;
    }

    /*
         * Get Bill no from mysql function
         */
    public function getBillSeq($type, $year){

        $seq_data = array(
            $type,
            $year,
            0,
            0,
            0
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->prepare("SELECT seq(?,?,?,?,?) AS proforma_invoice_no");
        $stmt->execute($seq_data);
        $seq = $stmt->fetch();

        return $seq['proforma_invoice_no'];
    }



    public function generateInvoicePostage($studentID,$feeID,$countryId,$stateId=0){

        /**
         * function to generate postage fee
         */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = getDB2();

        //checking CP
        $studentRegDB = new App_Model_StudentRegistration();
        $studentInfo = $studentRegDB->getStudentInfo($studentID);

        $checkingInvoice = $studentInfo['invoice'];
        $invoice_id = null;

//		if($checkingInvoice == 1){

        $curID = 1;//myr only

        $currencyDb = new App_Model_CurrencyRate();
        $currency = $currencyDb->getCurrentExchangeRate($curID);

        /*get fee info
         * 1. check state
         * 2. if state amount null, check country
         */

        $amountAdd = 0;

        if($stateId){
            $feeStateDB = new App_Model_FeeItemState();
            $feeStateData = $feeStateDB->getItemData($feeID,$stateId,$curID);

            if($feeStateData){
                $amountAdd = $feeStateData['fic_amount'];
                $feeDesc = $feeStateData['fi_name'];
            }else{
                $feeCountryDB = new App_Model_FeeCountry();
                $feeCountryData = $feeCountryDB->getItemData($feeID,$countryId,$curID);
                if($feeCountryData){
                    $amountAdd = $feeCountryData['fic_amount'] * $feeCountryData['fic_year'];
                    $feeDesc = $feeCountryData['fi_name'];
                }
            }
        }else{
            $feeCountryDB = new App_Model_FeeCountry();
            $feeCountryData = $feeCountryDB->getItemData($feeID,$countryId,$curID);
            if($feeCountryData){
                $amountAdd = $feeCountryData['fic_amount'] * $feeCountryData['fic_year'];
                $feeDesc = $feeCountryData['fi_name'];
            }
        }

        if($amountAdd != 0){

            //get current semester
            $profile = array('ap_prog_scheme'=>$studentInfo['IdScheme'],'branch_id'=>$studentInfo['IdBranch']);
            //$semesterDB = new App_Model_Registration_DbTable_Semester();
            //$semester = $semesterDB->getApplicantCurrentSemester($profile);
            //$semesterID = $semester['IdSemesterMaster'];

            $db->beginTransaction();
            try{

                //insert into invoice_main
                $invoiceMainDb = new App_Model_InvoiceMain();

                $bill_no =  $this->getBillSeq(2, date('Y'));
                $data_invoice = array(
                    'bill_number' => $bill_no,
                    'appl_id' => isset($studentInfo['appl_id'])?$studentInfo['appl_id']:0,
                    'trans_id' => isset($studentInfo['transaction_id'])?$studentInfo['transaction_id']:0,
                    'IdStudentRegistration' => isset($studentInfo['IdStudentRegistration'])?$studentInfo['IdStudentRegistration']:0,
                    'academic_year' => '',
                    'bill_amount' => $amountAdd,
                    'bill_paid' => 0,
                    'bill_balance' => $amountAdd,
                    'bill_description' =>$feeDesc,
                    'program_id' => $studentInfo['IdProgram'],
                    'fs_id' => $studentInfo['fs_id'],
                    'currency_id' => $curID,
                    'exchange_rate' => $currency['cr_exchange_rate'],
                    'invoice_date'=>date('Y-m-d'),
                    'date_create'=>date('Y-m-d H:i:s'),
                    'creator'=>$creator//by admin
                );

//							echo "<pre>";
//							print_r($data_invoice);

                $invoice_id = $invoiceMainDb->insert($data_invoice);

                //insert invoice detail
                $invoiceDetailDb = new App_Model_InvoiceDetail();

                $invoiceDetailData = array(
                    'invoice_main_id' => $invoice_id,
                    'fi_id' =>  $feeID,
                    'fee_item_description' =>  $feeDesc,
                    'cur_id' =>  $curID,
                    'amount' => $amountAdd,
                    'balance' => $amountAdd,
                    'exchange_rate' => $currency['cr_exchange_rate']
                );

                $invoice_detail_id = $invoiceDetailDb->insert($invoiceDetailData);

                $db->commit();

            }catch(Exception $e){

                $db->rollBack();

                //save error message
                $sysErDb = new App_Model_SystemError();
                $msg['se_IdStudentRegistration'] = $studentID;
                $msg['se_title'] = 'Error generate invoice';
                $msg['se_message'] = $e->getMessage();
                $msg['se_createdby'] = $creator;
                $msg['se_createddt'] = date("Y-m-d H:i:s");
                $sysErDb->addData($msg);

                throw $e;
            }
        }
//		}
        return $invoice_id;
    }


    public function generateCreditNoteEntryMain($studentID,$invoiceID,$percentageDefined=0,$amount=0,$commit=0,$dateCreated=0){

        /* Generate function for generate credit note
         * Parameter  in : Student ID,  Subject, $percentageDefined, amount)
         * $invoiceID  - id invoice_detail
         */

        $auth = Zend_Auth::getInstance();
        $getUserIdentity = $auth->getIdentity();
        $creator = $getUserIdentity->id;

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'invoice_main'),array('*','cn_amount_main'=>'a.cn_amount','idMain'=>'a.id'))
            ->join(array('c' => 'invoice_detail'), 'c.invoice_main_id = a.id',array('*','id_detail'=>'c.id'))
            ->joinLeft(array('b' => 'invoice_subject'), 'b.invoice_main_id = a.id and b.invoice_detail_id = c.id',array('subject_id'))
            ->joinLeft(array('e' => 'tbl_subjectmaster'), 'e.IdSubject = b.subject_id')
            ->joinLeft(array('d' => 'fee_structure_item'), 'd.fsi_item_id = c.fi_id and d.fsi_structure_id = a.fs_id')
            ->joinLeft(array('fi'=>'fee_item'),'fi.fi_id=c.fi_id', array(''))
            ->joinLeft(array('fsi'=>'fee_structure_item'),'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=a.fs_id', array(''))
            ->joinLeft(array('i'=>'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name'=>'i.DefinitionDesc','item_code'=>'i.DefinitionCode'))
            ->where('a.id = ?', $invoiceID);
//		->where('a.IdStudentRegistration = ?', $studentID);
//		->orWhere('a.trans_id = ?', $studentID);
//		exit;
        $txnData = $db->fetchAll($select);

        $invoiceID = $txnData[0]['idMain'];

        if($txnData==null){
            return null;
//			throw new Exception('No data to issue credit note');
        }else{

            $balanceNew = 0;
            $fee_item_to_charge = array();
            $totalAmount = 0;
            foreach($txnData as $key=>$data){
                $fee_item_to_charge[$data['invoice_main_id']]['fee_item'][$key] = $data;
                $totalAmount += $data['amount'];

            }

            $amountCN = $totalAmount;
            $typeAmount = 0;
            if($percentageDefined){
                $amountCN = ($totalAmount * ($percentageDefined/100));
                $typeAmount = $percentageDefined;
            }
            if($amount){
                $amountCN = $amount;
            }

            $fee_item_to_charge[$data['invoice_main_id']]['amount'] = $amountCN;

            /*echo "<pre>";
            print_r($fee_item_to_charge);
    //		exit;*/

            if($commit == 1){
                $db->beginTransaction();
            }

            try{

                //insert into invoice_main
                $creditMainDb = new App_Model_DbTable_CreditNote();

                $bill_no =  $this->getBillSeq(4, date('Y'));

                $feeItem = $fee_item_to_charge[$invoiceID]['fee_item'][0];

                if($feeItem['SubCode']){
                    if($feeItem['item_name']){
                        $cn_desc = $feeItem['SubCode'].' - '.$feeItem['SubjectName'].', '.$feeItem['item_name'];
                    }else{
                        $cn_desc = $feeItem['SubCode'].' - '.$feeItem['bill_description'];
                    }

                }else{
                    $cn_desc =$feeItem['bill_description'];
                }

                $amountCN = $fee_item_to_charge[$invoiceID]['amount'];

                if($dateCreated){
                    $dateCreated = date('Y-m-d',strtotime($dateCreated));
                }else{
                    $dateCreated = date('Y-m-d H:i:s');
                }

                if($amountCN > 0){
                    $data_cn = array(
                        'cn_billing_no' => $bill_no,
                        'cn_appl_id' => $feeItem['appl_id'],
                        'cn_trans_id' => $feeItem['trans_id'],
                        'cn_IdStudentRegistration' => $feeItem['IdStudentRegistration'],
                        'cn_amount' => $amountCN,
                        'cn_description' => $cn_desc,
                        'cn_cur_id' => $feeItem['currency_id'],
                        'cn_invoice_id' => $invoiceID,
                        'cn_create_date'=>$dateCreated,
                        'type_amount' => $typeAmount,
                        'cn_creator'=>$creator,//by system
                        'cn_source'=>1,//by student portal
                        'cn_status' => 'A',//active/approve
                        'cn_approve_date'=>date('Y-m-d H:i:s'),
                        'cn_approver'=>1,//by admin
                    );

                    $cn_id = $creditMainDb->insert($data_cn);

                    //insert invoice detail
                    $creditDetailDb = new Studentfinance_Model_DbTable_CreditNoteDetail();

                    foreach($fee_item_to_charge[$invoiceID]['fee_item'] as $data){

                        $cnDetailData = array(
                            'cn_id' =>  $cn_id,
                            'invoice_main_id' =>  $invoiceID,
                            'invoice_detail_id' =>  $data['id_detail'],
                            'amount' => $amountCN,
                            'cur_id' => $data['currency_id']
                        );

                        $cn_detail_id = $creditDetailDb->insert($cnDetailData);

                        //update amount cn at invoice_detail
                        $balanceNew1 = $data['balance'] - $amountCN;
                        $amountCNNew1 = $amountCN;
//						$balanceNew1 = $data['balance'] - $amountCN;
//						$amountCNNew1 = $data['cn_amount'] + $amountCN;
                        $dataUpdDetail = array('cn_amount'=>$amountCNNew1,'balance'=>$balanceNew1);
                        $db->update('invoice_detail', $dataUpdDetail,$db->quoteInto("id = ?",$data['id_detail']));

                        $invoiceMainDB = new Studentfinance_Model_DbTable_InvoiceMain();
                        $invMain = $invoiceMainDB->getData($invoiceID);

                        //update amount cn at invoice_main
                        $balanceNew = $invMain['bill_balance'] - $amountCNNew1;
                        $amountCNNew = $invMain['cn_amount'] + $amountCNNew1;
                        $dataUpd = array('cn_amount'=>$amountCNNew,'bill_balance'=>$balanceNew);
                        $db->update('invoice_main', $dataUpd,$db->quoteInto("id = ?",$invoiceID));

                    }


                }

                if($commit == 1){
                    $db->commit();
                }

                return $cn_id;

            }catch(Exception $e){

                if($commit == 1){
                    $db->rollBack();
                }

                //save error message
                $sysErDb = new App_Model_SystemError();
                $msg['se_IdStudentRegistration'] = $studentID;
                $msg['se_title'] = 'Error generate credit note entry';
                $msg['se_message'] = $e->getMessage();
                $msg['se_createdby'] = $creator;
                $msg['se_createddt'] = date("Y-m-d H:i:s");
                $sysErDb->addData($msg);

                throw $e;
                return null;
            }

        }

    }
    }?>