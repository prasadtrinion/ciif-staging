<?php


class App_Model_Smsconnect {
    private $smsdb;
    private $lobjDbAdpt;
    protected $_name           = 'exam_setup';
    protected $_primary        = "es_id";
    protected $_detail_name    = 'exam_setup_detail';
    protected $_detail_primary = "esd_id";

public function init() {
    $this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
}

    public function connect(){

        $this->smsdb = new Zend_Db_Adapter_Pdo_Mysql(array(
                'host'     => 'localhost',
                'username' => 'root',
                'password' => '',
                'dbname'   => 'meteor_ibfim'
                /*'host'     => DBSERVER,
                'username' => DBUSER,
                'password' => DBPASS,
                'dbname'   => DBNAME*/
            ));

    }

    public function getUserId($username) {
        $this->connect();

        $sql="Select * from tbl_user where iduser='$username'";
        $rs = $this->smsdb->fetchRow($sql);
        if(!$rs){
            return 0;
        }else{
            return $rs;
        }
    }


    public function getProgramGroup() {
        $this->connect();
        $sql="SELECT `a`.* FROM `tbl_program_group` AS `a` WHERE (a.active = 1) ORDER BY `a`.`name` asc";
        $rs = $this->smsdb->fetchAll($sql);
        return $rs;
        /*$lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_program_group"))
            ->where("a.active = 1")
            ->order('a.name asc')
           ;
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;*/
    }

    public function getProgramByGroup($idGroup) {
        $this->connect();
        $sql="SELECT `a`.* FROM `tbl_program` AS `a`
        WHERE (a.group_id = '$idGroup') AND (a.Active = 1)";
        $rs = $this->smsdb->fetchAll($sql);
        return $rs;
        /*$lstrSelect = $this->lobjDbAdpt->select()
            ->from(array("a" => "tbl_program"))
            ->where("a.group_id = ?",$idGroup)
            ->where("a.Active = 1")
        ;
        $larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
        */
    }

        public function getExamLandscape($IdProgram, $params = array()) {
        $this->connect();
        $sql="SELECT `a`.`IdLandscape`, `a`.`IdProgram`, `a`.`ProgramDescription`, `b`.`IdProgramScheme`,
        `c`.`DefinitionDesc` AS `mode_of_program_desc`, `g`.`exam_closing_date`, `core`.`CreditHours`
        AS `CoreCH`, `elc`.`CreditHours` AS `ElcCH` FROM `tbl_landscape` AS `a` INNER JOIN `tbl_program_scheme`
        AS `b` ON b.IdProgramScheme = a.IdProgramScheme INNER JOIN `tbl_definationms`
        AS `c` ON b.mode_of_program = c.idDefinition INNER JOIN `tbl_markdistribution_setup`
        AS `d` ON d.mds_landscape = a.IdLandscape INNER JOIN `exam_setup_detail` AS
        `e` ON e.esd_mds_id = d.mds_id INNER JOIN `exam_schedule` AS `f` ON f.es_esd_id = e.esd_id
        INNER JOIN `tbl_program` AS `g` ON g.IdProgram = a.IdProgram LEFT JOIN `tbl_programrequirement`
        AS `core` ON a.IdLandscape = core.IdLandscape AND core.SubjectType = 394
        LEFT JOIN `tbl_programrequirement` AS `elc` ON a.IdLandscape = elc.IdLandscape
        AND elc.SubjectType = 275 WHERE (a.IdProgram = '$IdProgram') AND (a.Active = 1) GROUP BY `a`.`IdLandscape`";
        $rs = $this->smsdb->fetchAll($sql);
        return $rs ;
    }

        public function getExamOption($IdLandscape, $params = array()){
            $this->connect();
            $sql="SELECT `a`.`es_id` AS `examsetup_id`, `a`.`es_id`, `a`.`es_name`, `a`.`es_online`, `b`.`esd_id`,
            `b`.`esd_type`, `b`.`esd_idSubject`, `d`.`exam_closing_date` FROM `exam_setup` AS `a` INNER JOIN
            `exam_setup_detail` AS `b` ON b.es_id = a.es_id INNER JOIN `exam_schedule` AS `c` ON c.es_esd_id =
            b.esd_id INNER JOIN `tbl_program` AS `d` ON d.IdProgram = a.es_idProgram WHERE (a.es_idLandscape = '2')
            GROUP BY `a`.`es_id` ORDER BY `a`.`es_name`";
            $rs = $this->smsdb->fetchAll($sql);
            //print_r($rs);exit;
            return $rs ;
            }
        /*$db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array('a' => $this->_name), array('IdLandscape', 'IdProgram', 'ProgramDescription'))
            ->join(array('b' => 'tbl_program_scheme'), 'b.IdProgramScheme = a.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('c' => 'tbl_definationms'), 'b.mode_of_program = c.idDefinition', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->join(array('d' => 'tbl_markdistribution_setup'), 'd.mds_landscape = a.IdLandscape', array())
            ->join(array('e' => 'exam_setup_detail'), 'e.esd_mds_id = d.mds_id', array())
            ->join(array('f' => 'exam_schedule'), 'f.es_esd_id = e.esd_id', array())
            ->join(array('g' => 'tbl_program'), 'g.IdProgram = a.IdProgram', array('exam_closing_date'))
            ->joinLeft(array('core' => 'tbl_programrequirement'), 'a.IdLandscape = core.IdLandscape AND core.SubjectType = 394', array('core.CreditHours as CoreCH'))
            ->joinLeft(array('elc'  => 'tbl_programrequirement'), 'a.IdLandscape = elc.IdLandscape  AND elc.SubjectType  = 275', array('elc.CreditHours as ElcCH'))
            ->where('a.IdProgram = ?', $IdProgram)
            ->where('a.Active = ?', 1)
            ->group('a.IdLandscape');

        if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
            $select->where('f.es_date > DATE_SUB(f.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
        }
        else {
            $select->where('f.es_date > DATE_SUB(f.es_date, INTERVAL g.exam_closing_date DAY)');
        }

        $result = $db->fetchAll($select);
        return $result;
    }*/
/*
    protected $_name = 'announcements';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getWiki(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'announcements'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'));

        foreach ($where as $what => $value) {
            $what = $what == 'id = ?' ? 'a.id = ?' : $what;
            $select->where($what, $value);
        }

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getWikis($order = 'a.created_date ASC',$results=false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'announcements'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'))
            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }
*/


}
