<?php

class App_Model_Curriculum extends Zend_Db_Table
{

    protected $_name = 'curriculum';
    protected $_primary = 'id';

    private static $tree = array();
    private static $html = '';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getCurriculum(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a'=>$this->_name));

        foreach ( $where as $what => $value )
        {
            $select->where( $what, $value);
        }

        $result = $db->fetchRow( $select );

        return $result;
    }

    public static function printNav($tree, $r = 0, $p = null )
    {
        foreach ($tree as $i => $t)
        {

            if ($t['parent_id'] == $p) {
                // reset $r
                $r = 0;
            }

            self::$html .= '<li '.(isset($t['_children'])?' class="uk-parent"':'').'><a href="#">'.$t['name'].'</a>';

            if (isset($t['_children'])) {
                self::$html .= '<ul class="uk-nav-sub">';
                self::printNav($t['_children'], ++$r, $t['parent_id']);
                self::$html .= '</ul>'."\n";
            }

            self::$html .= '</li>'."\n";
        }

        return self::$html;
    }

    public static function printTree($tree, $r = 0, $p = 0, $printdash = true, $maxdepth=-1) {

        foreach ($tree as $i => $t) {

            $dash = $printdash == true ? (($t['parent_id'] == 0) ? '' : str_repeat('-', $r) .' ') : '';

            if ($t['parent_id'] == $p) {
                // reset $r
                $r = 0;
            }

            self::$tree[] = array_merge( $t, array('name' => $dash.$t['name'], '_children' => '', 'depth' => $r) );

            if (isset($t['_children']) && !empty($t['_children']) && ($r > ($maxdepth-1))) {
                if ( $maxdepth > -1 && ($maxdepth-1) == $r ) return self::$tree;
                self::printTree($t['_children'], ++$r, $t['parent_id'], $printdash, $maxdepth);
            }
        }

        return self::$tree;
    }

    public static function buildTree(Array $data, $parent = 0) {
        $tree = array();
        foreach ($data as $d) {
            if ($d['parent_id'] == $parent) {
                $children = self::buildTree($data, $d['id']);
                // set a trivial key
                if (!empty($children)) {
                    $d['_children'] = $children;
                }
                $tree[] = $d;
            }
        }
        return $tree;
    }

    public function getDataByExternal($programId)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'curriculum'), array('*'))
            ->where('a.external_id = ?', $programId);

        $result = $db->fetchRow($select);
        return $result;

    }
}
