<?php
class App_Model_Membership_Membership extends Zend_Db_Table_Abstract
{
    protected $_name = 'membership_registration';
    protected $_primary = "mr_id";





    public function get_member($id)
    {   
        $db     = getDB();
         $select = $db->select()
            ->from(array('mr' => $this->_name))
             ->join(array('u' => 'user'),'mr.mr_membershipId = u.id')
             ->join(array('a' => 'member'),'a.idmember = u.id_member')
             ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status',array('mr_status'=>'DefinitionDesc','idDefinition'))
            

            ->where('mr_membershipId ='.$id);
            $result = $db->fetchRow($select);
            return $result;
    }


    public function get_status_member($id)
    {   
        $db     = getDB();
         $select = $db->select()
            ->from(array('mr' => $this->_name))
             ->join(array('u' => 'user'),'mr.mr_membershipId = u.id')
             ->join(array('a' => 'member'),'a.idmember = u.id_member')
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status')
            

            ->where('mr_membershipId ='.$id);
            $result = $db->fetchRow($select);
            return $result;
    }

    public function getDataById($id="", $studregid=""){
        $id = (int)$id;

        $db     = getDB();

        $select = $db->select()
            ->from(array('mr' => $this->_name) )
            ->joinLeft(array('p' => 'tbl_program'), 'mr.mr_idProgram=p.IdProgram', array('MembershipName'=>'ProgramName', 'ProgramCode'))
            ;

            if ($id != "") {
                $select->where('mr_id'.' = ' .$id);
            }

            if ($studregid != "") {
                $select->where('mr_student_id'.' = ' .$studregid);
            }

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getStudReg ($studregid="", $studprofileid="", $registype=""){//`sp_id` = 194 AND `IdRegistrationType` IN (1,3)
        $studregid = (int)$studregid;

        $db     = getDB2();

        $select = $db->select()
            ->from(array('mr' => $this->_studentregistration) )
            ->join(array('rt' => 'tbl_registration_type'), 'rt.rt_id = mr.IdRegistrationType', array('typename' => 'rt_activity'))
            ->joinLeft(array('p' => 'tbl_program'), 'mr.IdProgram=p.IdProgram', array('ProgramName', 'ProgramCode'))
            ;

            if ($studregid != "") {
                $select->where('registrationId'.' = ' .$studregid);
            }

            if ($studprofileid != "") {
                $select->where('sp_id'.' = ' .$studprofileid);
            }

            if ($registype != "") {
                $select->where('IdRegistrationType'.' IN  (' .$registype .')' );
            }

        $row = $db->fetchRow($select);
        return $row;
    }

    public function get_status($id){
     
        $db     = getDB();

        $select = $db->select()
            ->from(array('fp' => $this->_name))
            ->join(array('fs' => 'tbl_definationms'), 'fp.mr_status=fs.idDefinition')
            ->where('fs.idDefinition ='.$id);
        $row = $db->fetchRow($select);
        return $row;

    }


    public function get_doc($id){
         $db     = getDB();

        $select = $db->select()
            ->from(array('fp' => 'tbl_userdoc'))
            ->where('membership_id ='.$id);
        $row = $db->fetchAll($select);
        return $row;
    }
    public function get_event(){
         $db     = getDB();

        $select = $db->select()
            ->from(array('fp' => 'calendar_event'))
            ->where('date_start >='.date("Y-m-d"));
        $row = $db->fetchAll($select);
        return $row;
    }

     public function get_qualification($id){
        $db     = getDB();
       
        $select = $db->select()
            ->from(array('fp' => 'user'))
            ->join(array('f' => 'tbl_program'), 'fp.idprogram =f.IdProgram')
            ->join(array('e' => 'membership_registration'), 'fp.id =e.mr_membershipId')
            ->join(array('fs' => 'tbl_definationms'),'fs.idDefinition=e.mr_status')
            ->where('e.mr_membershipId ='.$id);
        $row = $db->fetchRow($select);
        
        return $row;

    }

    public function getMemberProgram ($memberProgram=""){
        $memberProgram = (int)$memberProgram;

        $db     = getDB();

        $select = $db->select()
            ->from(array('p' => 'tbl_program'));

            if ($memberProgram != "") {
                $select->where('p.IdProgram'.' = ' .$memberProgram);
            }

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getFeeProgram ($memberProgram=""){
        $memberProgram = (int)$memberProgram;

        $db     = getDB();

        $select = $db->select()
            ->from(array('fp' => 'fee_structure_program'))
            ->join(array('fs' => 'fee_structure'), 'fp.fsp_fs_id=fs.fs_id')
            ->join(array('fsi' => 'fee_structure_item'), 'fsi.fsi_structure_id=fs.fs_id')
            ->join(array('c' => 'tbl_currency'), 'c.cur_id=fsi.fsi_cur_id', array('cur_symbol_prefix'))
            ;

            if ($memberProgram != "") {
                $select->where('fp.fsp_program_id'.' = ' .$memberProgram);
            }

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getDataOrder($orderId) {
        $db = $this->getDefaultAdapter();
        $query = $db->select()
            ->from(array('a' => 'order'))
            ->where('a.id = ?', $orderId);

        $result = $db->fetchAll($query);
        return $result;
    }

    public function addData($data) {

        $db = getDB2();
        
        $db->insert($this->_name,$data);

        $id = $db->lastInsertId();
        
        return $id;

    }

    public function deleteData($id)
    {
        $db = getDB2();
        $db->delete($this->_name, $this->_primary . ' =' . (int)$id);
    }

    public function getStud($id) {
         $db = getDB2();

        $select = $db->select()
            ->from(array('a' => 'student_profile'))
            ->where('a.std_id = ?', $id);

        $row = $db->fetchRow($select);
        return $row;
    }

    public function updateData($data, $er_id, $remarks = null) {
        $db     = getDB2();
        $auth  = Zend_Auth::getInstance();

        if($auth->getIdentity()->login_as){//if admin
            $modifiedby = 1; //1= tbl_user
        }else{
            $modifiedby = $auth->getIdentity()->external_id;
        }

        $data['modifieddt']  = date('Y-m-d H:i:s');
        $data['modifiedby']  = $modifiedby;
//        $data['modifiedby']  = $auth->getIdentity()->external_id;

        $result = $db->update($this->_name, $data, "er_id = $er_id");

        $query = $db->select()
            ->from(array('er' => $this->_name))
            ->where('er.er_id = ?', $er_id);
        $data = $db->fetchRow($query);
        $data['remarks']        = $remarks;
        $data['payment_status'] = (int) $data['payment_status'];
        $db->insert($this->_history, $data);

        return $result;
    }

    public function getProgram ($memberProgram="", $IsMembership = ""){
        $memberProgram = (int)$memberProgram;

        $db     = getDB2();

        $select = $db->select()
            ->from(array('p' => 'tbl_program'))
        ;

        if ($memberProgram != "") {
            $select->where('p.IdProgram'.' = ' .$memberProgram);
        }
        if ($IsMembership != "") {
            $select->where('p.IsMembership'.' = "' .$IsMembership.'"');
        }
        $row = $db->fetchAll($select);
        return $row;
    }

    public function updateDataMember($data, $mr_id)
    {
        $db = getDB2();
        $auth  = Zend_Auth::getInstance();

        if($auth->getIdentity()->login_as){//if admin
            $modifiedby = 1; //1= tbl_user
        }else{
            $modifiedby = $auth->getIdentity()->external_id;
        }

        $expdate = date('Y-m-d H:i:s');
        $expireDate = date("Y-m-d H:i:s",strtotime($expdate."+7 day"));

        $data['mr_modifieddt'] = date('Y-m-d H:i:s');
        $data['mr_modifiedby'] = $modifiedby;

        $db->update($this->_name, $data, "mr_id = $mr_id");
    }
    
     public function getMembership($id){
     
        $db     = getDB2();

        $select = $db->select()
            ->from(array('mr' => $this->_name))
            ->join(array('m'=>'tbl_membership'),'m.m_id = mr.mr_membershipId',array('MembershipName'=>'m_name'))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status',array('mr_status'=>'DefinitionDesc','idDefinition','mr_status_id'=>'idDefinition'))
            ->joinLeft(array('sponsor'=>'tbl_sponsor_registration'),'sponsor.sponsor_member_id=mr.mr_id',array('SponsorStatus'=>'sponsor.status'))
            ->where('mr.mr_sp_id=?',$id);
         
        $row = $db->fetchAll($select);
        return $row;
    }
    
    
    public function getMembershipById($mr_id){
     
        $db     = getDB2();

        $select = $db->select()
            ->from(array('mr' => $this->_name))
            ->join(array('m'=>'tbl_membership'),'m.m_id = mr.mr_membershipId',array('MembershipName'=>'m_name','m_id'))            
            ->join(array('at'=>'applicant_transaction'),'at.at_trans_id=mr.mr_transaction_id',array('at_appl_type'))
            ->join(array('sp'=>'student_profile'),'sp.std_id=mr.mr_sp_id',array('std_fullname'))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status',array('mr_status'=>'DefinitionDesc','idDefinition'))
            ->where('mr.mr_id=?',$mr_id);
         
        $row = $db->fetchRow($select);
        return $row;
    }
    
    public function getMembershipList($IdProgram = 0)
    {
        $db     = getDB2();
        $select = $db->select()
            ->from(array('a' => 'tbl_program'),array('a.ProgramName as ProgMembership','a.IdProgram as ProgMembershipId'))
            ->join( array('b' =>'tbl_program'), 'b.ProgramMembership = a.IdProgram', array( 'b.*' ) );

            if($IdProgram != 0){
                 $select->where( 'b.IdProgram = ?', $IdProgram );
            }else{
                $select->where( 1);
            }
//echo $select;
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function updateApplication($data,$where)
    {
        $db = getDB2();
        $db->update($this->_name,$data,$where);
    }
    
    
    public function getActiveMembership($id){
     
        $db     = getDB2();

        $select = $db->select()
            ->from(array('mr' => $this->_name))
            ->join(array('m'=>'tbl_membership'),'m.m_id = mr.mr_membershipId',array('MembershipName'=>'m_name'))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status',array('mr_status'=>'DefinitionDesc','idDefinition','mr_status_id'=>'idDefinition'))
            ->where('mr.mr_sp_id=?',$id)
            ->where('mr.mr_status=?',1384);
         
        $row = $db->fetchRow($select);
        return $row;
    }
    
    
    public function getMembershipInfo($id){
             
        $db     = getDB();

        $select = $db->select()
            ->from(array('mr' => $this->_name))
            ->join(array('sp'=>'user'),'sp.id=mr.mr_membershipId')

            ->join(array('m'=>'member'),'m.idmember = sp.id_member',array('MembershipName'=>'m.member_name'))
            //->join(array('sp'=>'student_profile'),'sp.std_id=mr.mr_sp_id',array('std_fullname'))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status')
            ->where('mr.mr_sp_id=?',$id);
         
        $row = $db->fetchRow($select);
        return $row;
    }

    public function addRegData($table, $data){
        $db = getDB2();
        $id = $db->insert($table, $data);
        return $id;
    }
    
    public function getData($mr_id){
             
        $db     = getDB2();

        $select = $db->select()
            ->from(array('mr' => $this->_name))
            ->where('mr.mr_id=?',$mr_id);
         
        $row = $db->fetchRow($select);
        return $row;
    }

     public function getFee($sub_type,$degree_type,$member_type)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'tbl_member_renew_fee_structure'))
            ->where('a.id_member_subtype = '.$sub_type)
            // ->where('a.id_degree = '.$degree_type)
            ->where('a.id_member = '.$member_type);

        $result = $db->fetchRow($select);
        
       
        return $result;
    }

    
public function getMembershipInfoById($id){
             
      $db     = getDB();

        $select = $db->select()
            ->from(array('mr' => $this->_name))
            ->join(array('sp'=>'user'),'sp.id=mr.mr_membershipId')

            ->join(array('m'=>'member'),'m.idmember = sp.id_member',array('MembershipName'=>'m.member_name'))
            //->join(array('sp'=>'student_profile'),'sp.std_id=mr.mr_sp_id',array('std_fullname'))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status')
            ->where('mr.mr_id=?',$id);
         
        
         
        $row = $db->fetchRow($select);
        return $row;
    }


    public function getLastSequenceID()
    {
        $db     = getDB2();
        $query = $db->select()
            ->from(array('mr'=>$this->_name))
            ->where('1')
            ->order("mr_registrationId DESC");

        $result = $db->fetchRow($query);
        return $result["mr_registrationId"];

//        if(!$result["mr_registrationId"]){echo "here";
//            $n='100001';
//
//        }else{
//            $n = $result["mr_registrationId"];
//            $n = $n + 1;
//        }
//        $membershipRegID = str_pad($n, 5, 0, STR_PAD_LEFT);
//        return $membershipRegID;
    }

    public function getDataByIdHistory($id=""){
        $id = (int)$id;

        $db     = getDB2();

        $select = $db->select()
            ->from(array('mr' => $this->_name) )
        ;

        if ($id != "") {
            $select->where('mr_id'.' = ' .$id);
        }

        $row = $db->fetchRow($select);
        return $row;
    }


    public function getMembershipInfoByIdHistory($id){

        $db     = getDB2();

        $select = $db->select()
            ->from(array('mr' => $this->_name))
            ->join(array('m'=>'tbl_membership'),'m.m_id = mr.mr_membershipId',array('MembershipName'=>'m_name'))
            ->joinleft(array('ma'=>'membership_application'),'ma.ma_prev_mrid = mr.mr_id',array('type'=>new Zend_Db_Expr('CASE WHEN ma_type = 1 THEN "Upgrade" WHEN ma_type=2 THEN "Renew" WHEN ma_type=3 THEN "Reinstatement" ELSE "-" END')))
            ->join(array('sp'=>'student_profile'),'sp.std_id=mr.mr_sp_id',array('std_fullname'))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status',array('mr_status'=>'DefinitionDesc','idDefinition','mr_status_id'=>'idDefinition'))
            ->where('mr.mr_id=?',$id);

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getMembershipByTxnId($txnid){

        $db     = getDB2();

        $select = $db->select()
            ->from(array('mr' => $this->_name))
            ->join(array('m'=>'tbl_membership'),'m.m_id = mr.mr_membershipId',array('MembershipName'=>'m_name'))
            ->join(array('sp'=>'student_profile'),'sp.std_id=mr.mr_sp_id',array('std_fullname'))
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status',array('mr_status'=>'DefinitionDesc','idDefinition','mr_status_id'=>'idDefinition'))
            ->where('mr.mr_transaction_id=?',$txnid);

        $row = $db->fetchRow($select);
        return $row;
    }
    
}
?>



