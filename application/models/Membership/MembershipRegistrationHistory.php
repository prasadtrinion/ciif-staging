<?php
class App_Model_Membership_MembershipRegistrationHistory extends Zend_Db_Table_Abstract
{
    protected $_name = 'membership_registration_history';
	protected $_primary = "mrh_id";
	
public function addData($data) {
        $db  = getDB();       
        $db->insert($this->_name,$data);
		$id = $db->lastInsertId();        
        return $id; 
    }

    public function deleteData($id)
    {
        $db = getDB();
        $db->delete($this->_name, $this->_primary . ' =' . (int)$id);
    }
    
 	public function updateData($data,$where)
    {
    	$db = getDB();
    	$db->update($this->_name,$data,$where);
    }

    public function getDataByIdHistory($id=""){
       // $id = (int)$id;

        $db     = getDB();

        $select = $db->select()
            ->from(array('mr' => $this->_name) )
            ->join(array('m'=>'tbl_membership'),'m.m_id = mr.mr_membershipId',array('MembershipName'=>'m_name'))
            ->join(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status',array('prevStatus'=>'DefinitionDesc'))

        ;

        if ($id != "") {
            $select->where('mr_sp_id'.' = ' .$id);
        }

        //echo $select;
        $row = $db->fetchAll($select);
        return $row;
    }
}
?>