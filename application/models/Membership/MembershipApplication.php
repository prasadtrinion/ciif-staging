<?php
class App_Model_Membership_MembershipApplication extends Zend_Db_Table_Abstract
{
    protected $_name = 'membership_application';
	protected $_primary = "ma_id";

    public function getListData($m_id){
       
        $db     = getDB();

         $select = $db->select()
            				   ->from(array('ma' => $this->_name));
        $result = $db->fetchAll($select);
        return $result;
    }


    //  public function getListMember($id){
       
    //     $db     = getDB();

    //      $select = $db->select()
    //                 ->from(array('ma' => $this->_name));
    //                  ->where('membershipId ='.$id);
    //     $result = $db->fetchAll($select);
    //     return $result;
    // }


    public function get_prev_member($id)
    {   
        $db     = getDB();
         $select = $db->select()
            ->from(array('mr' => $this->_name))
             ->join(array('u' => 'user'),'mr.membershipId = u.id')
             ->join(array('a' => 'member'),'a.idmember = mr.prev_member_id')
            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=mr.mr_status',array('mr_status'=>'DefinitionDesc','idDefinition'))
            

            ->where('membershipId ='.$id);
            $result = $db->fetchRow($select);
            return $result;
    }

    public function get_details($id){
        $db = getDB();

        $select = $db->select()
                     ->from(array('user'))
                     ->where('id ='.$id);

        $result = $db->fetchRow($select);

    }
    
 	public function addData($data) {

        $db  = getDB();       
        $db->insert($this->_name,$data);
		$id = $db->lastInsertId();        
        return $id; 
    }

    public function deleteData($id)
    {
        $db = getDB();
        $db->delete($this->_name, $this->_primary . ' =' . (int)$id);
    }
    
 	public function updateData($data,$where)
    {
    	$db = getDB();
    	$db->update($this->_name,$data,$where);
    }

    public function updateDataMaid($data, $id) {

        $db = getDB();
        $db->update($this->_name,$data, $this->_primary . ' =' . (int)$id);

    }
    public function updateDataM($data, $id, $type) {

        $db = getDB();
        $db->update($this->_name,$data, 'sp_id =' . $id and 'ma_type =' . $type );

    }

	public function getDataDoc($mrc_id,$mr_id){
       
       $db = getDB();

         $select = $db->select()
            				   ->from(array('mrd' => $this->_name))
            				   ->where('mrd.mr_id=?',$mr_id)
            				   ->where('mrd.mrc_id=?',$mrc_id);
        $result = $db->fetchRow($select);
        return $result;
    }
    
	public function getDetailsInfoByType($sp_id,$type){
       
        $db     = getDB();

        $select = $db->select()
            	     ->from(array('ma' => $this->_name))
            	     ->join(array('sp'=>'user'),'sp.id=ma.membershipId')

            ->join(array('m'=>'member'),'m.idmember = ma.prev_member_id',array('MembershipName'=>'m.member_name'))
            	     ->join(array('d'=>'tbl_definationms'),'d.idDefinition=ma.ma_status',array('ma_status'=>'DefinitionDesc','idDefinition','ma_status_id'=>'idDefinition'))
                     ->joinLeft(array('e'=>'tbl_definationms'),'e.idDefinition=ma.prev_status',array('status'=>'DefinitionDesc'))
            	     ->where('ma.membershipId= '.$sp_id)
            	     ->where('ma.ma_type ='.$type);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDetailsInfo($sp_id,$type){
       
        $db     = getDB();

        $select = $db->select()
                     ->from(array('ma' => $this->_name))
                     
                     ->join(array('e'=>'user'),'e.id = ma.membershipId')
                     ->join(array('m'=>'member'),'m.idmember = ma.prev_member_id',array('MembershipName'=>'member_name'))
                     ->join(array('d'=>'tbl_definationms'),'d.idDefinition=e.upgrade_member',array('ma_status'=>'DefinitionDesc','idDefinition','ma_status_id'=>'idDefinition'))
                     ->joinLeft(array('c'=>'tbl_definationms'),'c.idDefinition=ma.prev_status',array('status'=>'DefinitionDesc'))
                     ->join(array('mr'=>'membership_registration'),'mr.mr_id = ma.ma_prev_mrid')
                     ->where('ma.membershipId= '.$sp_id)
                     ->where('ma.ma_type ='.$type);
        $result = $db->fetchAll($select);
        
        
        return $result;
    }
    
	public function getApplicationById($ma_id){
     
        $db     = getDB();

        $select = $db->select()
            ->from(array('mr' => $this->_name))
            ->join(array('m'=>'tbl_membership'),'m.m_id = mr.membershipId',array('MembershipName'=>'m_name','m_id'))   
            ->join(array('sp'=>'student_profile'),'sp.std_id=mr.sp_id',array('std_fullname'))
            ->join(array('d'=>'tbl_definationms'),'d.idDefinition=mr.ma_status',array('ma_status'=>'DefinitionDesc','idDefinition'))
            ->where('mr.ma_id=?',$ma_id);
         
        $row = $db->fetchRow($select);
        return $row;
    }
 	
	public function getData($ma_id){
     
        $db     = getDB();

        $select = $db->select()
            ->from(array('ma' => $this->_name))
            ->where('ma.ma_id=?',$ma_id);
         
        $row = $db->fetchRow($select);
        return $row;
    }

    public function get_record($ma_id){
     
        $db     = getDB();

        $select = $db->select()
        
            ->from(array('ma' => $this->_name))
            ->where('ma.membershipId=?',$ma_id);
         
        $row = $db->fetchAll($select);
        return $row;
    }

public function get_renew_fee($ma_id){
     
        $db     = getDB();

        $select = $db->select()
        
            ->from(array('ma' => $this->_name))
            ->where('ma.membershipId=?',$ma_id)
            ->order('ma.ma_id DESC');
         
        $row = $db->fetchRow($select);
        return $row;
    }

    public function getCheckExist($sp_id,$ma_type){

        $db     = getDB();

        $select = $db->select()
            ->from(array('ma' => $this->_name))
            ->where('ma.sp_id=?',$sp_id)
            ->where('ma.ma_type=?',$ma_type);

        $row = $db->fetchRow($select);
        return $row;
    }


    

    public function getDetailsInfoByTypeRow($sp_id,$type){

        $db     = getDB();

        $select = $db->select()
            ->from(array('ma' => $this->_name))
            ->join(array('m'=>'tbl_membership'),'m.m_id = ma.membershipId',array('MembershipName'=>'m_name'))
            ->join(array('d'=>'tbl_definationms'),'d.idDefinition=ma.ma_status',array('ma_status'=>'DefinitionDesc','idDefinition','ma_status_id'=>'idDefinition'))
            ->where('ma.sp_id=?',$sp_id)
            ->where('ma.ma_type=?',$type);
        $result = $db->fetchRow($select);
        return $result;
    }
}
?>