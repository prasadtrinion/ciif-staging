<?php
class App_Model_Membership_MemberRegistration extends Zend_Db_Table_Abstract
{
    protected $_name = 'membership_registration';
	protected $_primary = "mr_id";

    public function getListData($m_id,$type=1){
       
        $db     = getDB2();

        $select = $db->select()
            				   ->from(array('mrc' => $this->_name))
            				   ->where('mrc_status=?',1)
            				   ->where('mrc.m_id=?',$m_id);
            				   
        if(isset($type)&&$type!=''){
        	$select->where('mrc_type=?',$type);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getListMember($id){
       
        $db     = getDB();

         $select = $db->select()
                    ->from(array('ma' => $this->_name))
                     ->where('ma.mr_membershipId ='.$id);
        $result = $db->fetchRow($select);
        return $result;
    }
    
 	public function getDataByType($type){
       
        $db     = getDB2();

        $select = $db->select()
            				   ->from(array('mrc' => $this->_name))
            				   ->where('mrc_status=?',1)
            				   ->where('mrc.mrc_type=?',$type);
        $result = $db->fetchAll($select);
        return $result;
    }
    
 	
}
?>