<?php
class App_Model_Membership_MembershipRegistrationChecklist extends Zend_Db_Table_Abstract
{
    protected $_name = 'membership_registration_checklist';
	protected $_primary = "mrc_id";

    public function getListData($m_id,$type=1){
       
        $db     = getDB2();

        $select = $db->select()
            				   ->from(array('mrc' => $this->_name))
            				   ->where('mrc_status=?',1)
            				   ->where('mrc.m_id=?',$m_id);
            				   
        if(isset($type)&&$type!=''){
        	$select->where('mrc_type=?',$type);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
 	public function getDataByType($type){
       
        $db     = getDB2();

        $select = $db->select()
            				   ->from(array('mrc' => $this->_name))
            				   ->where('mrc_status=?',1)
            				   ->where('mrc.mrc_type=?',$type);
        $result = $db->fetchAll($select);
        return $result;
    }
    
 	
}
?>