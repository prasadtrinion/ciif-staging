<?php
class App_Model_Membership_MembershipRegistrationDocuments extends Zend_Db_Table_Abstract
{
    protected $_name = 'membership_registration_documents';
	protected $_primary = "mrd_id";

    public function getListData($m_id){
       
        $db     = getDB2();

         $select = $db->select()
            				   ->from(array('mrd' => $this->_name));
        $result = $db->fetchAll($select);
        return $result;
    }
    
 	public function addData($data) {
        $db  = getDB2();       
        $db->insert($this->_name,$data);

    }

    public function deleteData($id)
    {
        $db = getDB2();
        $db->delete($this->_name, $this->_primary . ' =' . (int)$id);
    }
    
    
	public function getDataDoc($mrc_id,$mr_id){
       
       $db = getDB2();

         $select = $db->select()
            				   ->from(array('mrd' => $this->_name))
            				   ->where('mrd.mr_id=?',$mr_id)
            				   ->where('mrd.mrc_id=?',$mrc_id);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getDataDocUser($mrc_id,$mr_id,$createby){

        $db = getDB2();

        $select = $db->select()
            ->from(array('mrd' => $this->_name))
            ->where('mrd.mr_id=?',$mr_id)
            ->where('mrd.mrc_id=?',$mrc_id)
            ->where('mrd.mrd_createdby=?',$createby);
        $result = $db->fetchRow($select);
        return $result;
    }
 	
}
?>