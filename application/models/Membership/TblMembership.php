<?php
class App_Model_Membership_TblMembership extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_membership';
	protected $_primary = "m_id";

    public function getListMembership($register=""){
       
        $db     = getDB2();

         $select = $db->select()
            				   ->from(array('m' => 'tbl_membership'))
            				   ->where('m.m_active=?',1);
        if ($register!="") {
            $select->where('m.m_register=?',1);
        }
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>