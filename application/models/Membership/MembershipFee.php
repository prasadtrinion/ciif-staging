
<?php
class App_Model_Membership_MembershipFee extends Zend_Db_Table_Abstract
{
    protected $_name = 'membership_fee';
    protected $_primary = "mf_id";

    public function getFeeByMemberId($m_id){
       
        $db     = getDB2();

         $select = $db->select()
                               ->from(array('mf' => $this->_name))
                               ->where('mf.member_id=?',$m_id)
                               ->where('mf.item_id=?',1)
                               ->where('mf_effective_date <= CURDATE()');
        $result = $db->fetchRow($select);
        return $result;
    }       
    
    public function getRenewalFeeByMemberId($m_id,$expiry_date){
       
        $db     = getDB2();

         $select = $db->select()
                               ->from(array('mf' => ''))
                               ->where('mf.member_id=?',$m_id)
                               ->where('mf.item_id=?',2)
                               ->where('mf_effective_date <= CURDATE()')
                               ->where('mf_effective_date >= ?',$expiry_date)
                               ->order('mf_effective_date desc');
        $result = $db->fetchRow($select);
        return $result;
    }
    
 public function getFee($m_id){
  
  $db     = getDB2();
          $select = $db->select()
                               ->from(array('mf' => 'user'))
                               ->where('mf.id ='.$m_id);

            $result = $db->fetchRow($select);
        return $result;

 }

    public function getUpgradeFeeByMemberId($m_id){
        $db     = getDB2();

         $select = $db->select()
                               ->from(array('mf' => $this->_name))
                               ->where('mf.member_id=?',$m_id)
                               ->where('mf.item_id=?',3)
                               ->where('mf_effective_date <= CURDATE()');
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getReinstatementFee($m_id,$type,$start,$end){

        $db     = getDB2();

        $select = $db->select()
            ->from(array('mf' => $this->_name))
            ->where('mf.member_id=?',$m_id)
            ->where('mf.item_id=?',$type)
            ->where("'".$start."'".' >= mf.mf_start_date')
            ->where("'".$end."'".' <= mf.mf_end_date')
            ->order('mf_start_date desc');

        //echo $select;
        $result = $db->fetchRow($select);
        return $result;
    }
}

?>