<?php

class App_Model_Member extends Zend_Db_Table
{

    protected $_name = 'membership_registration';
    protected $_primary = 'mr_id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    


    public function getMember($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'member'),'a.idmember = u.id_member')
            ->where('id ='.$id);
           
           $result = $db->fetchRow($select);  
        return $result;

    }

    

     public function getQualificaion($id){

            $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->join(array('a' => 'tbl_program'),'a.IdProgram = u.program')
            ->where('id ='.$id);
           
           $result = $db->fetchRow($select);  
            return $result;
 
    }

    public function getUser($id,$by='id')
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            ->joinLeft(array('a' => 'external_data'),'a.value = u.race',array ('a.name as r_name'))
            ->joinLeft(array('b' => 'external_datatype'),'b.id=a.type_id', array())
            ->joinLeft(array('d' => 'external_data'),'d.value = u.religion',array ('d.name as re_name'))
            ->joinLeft(array('e' => 'external_datatype'),'e.id=d.type_id', array())
            ->where('u.'.$by.' = ?', $id);

        return $db->fetchRow($select);
    }


    public function getUserInfo($id)
    {

        
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('u' => $this->_name))
            
            ->where('u.id = ?', $id);

        return $db->fetchRow($select);
    }

    public function getUserByUsername($username)
    {
        $db = $this->getDefaultAdapter();

        $select = $this->select()->where('username = ?', $username);

        return $db->fetchRow($select);
    }

    public function getRace($id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'external_data'),array ('a.name as r_name'))
            ->join(array('b' => 'external_datatype'),'b.id=a.type_id')
            ->where('a.value = ?', $id);

            return $db->fetchRow($select);
    }

    public function getReligion($id)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'external_data'),array ('a.name as re_name'))
            ->join(array('b' => 'external_datatype'),'b.id=a.type_id')
            ->where('a.value = ?', $id);

            return $db->fetchRow($select);
    }

    public function getUserByEnrollment($course_id, $group_id=0)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('c' => 'enrol'), 'c.user_id = a.id', array('c.user_id', 'c.learningmode'))
            ->where('c.course_id = ?', $course_id)
            ->where('a.id NOT IN (SELECT user_id FROM user_group_data WHERE course_id = '.$course_id. ' AND group_id = '.$group_id.')')
            ->where('a.active = ?', 1)
            ->group('a.id')
            ->order('a.firstname');

        return $select;
    }

    public function approveUser($applicant_id)
    {
        $db = $this->getDefaultAdapter();  

            
        $result = $db->update('approve_status = 1')
                        ->where('id ='.$applicant_id);
        // echo $result;
        // die();
        if($result)
        {
            return $result;
            //exit();
        }   
        else
        {
            return false;
        }
    
    }
}
 