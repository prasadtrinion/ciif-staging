<?php

class App_Model_Quizanswer extends Zend_Db_Table
{

    protected $_name = 'quiz_answer';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }
    
    public function getQuizques($id,$bid,$order = 'a.created_date ASC',$results=false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_qbank'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'))
            ->where('a.course_id = ?', $id)
            ->where('a.data_id = ?', $bid)
            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }

    public function getqbankid($qid,$aid)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_qbank'))
            ->join(array('b' => 'quiz_answer'), 'b.id_question = a.id', array('answer','qaid'=>'id'))
            ->where('a.id = ?',$qid)
            ->where('b.attempt_id = ?',$aid);
        

        $row = $db->fetchRow($select);
        return $row;
    }

    public function getquizresult($atid)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_qbank'))
            ->join(array('b' => 'quiz_answer'), 'b.id_question = a.id', array('answer','correct_answer','quesnum','amark'=>'mark'))
            ->where('b.attempt_id = ?',$atid);
        

        $result = $db->fetchAll($select);
        return $result;
    }


    public function updateanswer($data,$quesid,$gid,$aid)
    {

        $db = $this->getDefaultAdapter();

        // $select = $db->select()
        //     ->from(array('a' => 'quiz_answer'))
        //     ->join(array('b' => 'quiz_qbank'), 'a.id_question = b.id', array('qmark'=>'mark','c_answer'=>'correct_answer','answer1','answer2','answer3','answer4'))
        //     ->where('a.attempt_id = ?',$atid);
        

        // $result = $db->fetchRow($select);
        // pr($result);exit;
                
        //     foreach ($result as $at){

        //             $at['answer'] = $data['answer'];

        //         //pr($result);exit;

        //         $result = $db->update('quiz_answer', $data, 'id = ' . $at["id"]);
        //     }
       //public function updateDataOrder($data, $order_id) {
        //FOR DEVELOPMENT USE ONLY
        //pr($data);pr($atid);pr($data);pr($quesid);exit;
        $data2 =  array(
                        'answer'   => $data['answer'],
                        //'id_question' => substr($key,7,1),
                        'id_question' => $data['id_question'],
                        'correct_answer' => $data['correct_answer'],
                        //'quesnum' => substr($key,2,1),
                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'   => $gid,
                        //'attempt_id'   => $aid,

        );
                if($data['answer'] == $data['correct_answer']){
                    $data2['mark'] = $data['mark'];
                } else if($data['answer'] != $data['correct_answer']) {
                    $data2['mark'] = 0 ;
                } 

        //pr($data2);exit;
        $result = $db->update('quiz_answer', $data2, "id = $aid");

        //return $result;

    }
    
    public function getlistanswer($atid)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'quiz_answer'))
            ->join(array('b' => 'quiz_qbank'), 'a.id_question = b.id', array('qmark'=>'mark','c_answer'=>'correct_answer','answer1','answer2','answer3','answer4'))
            ->where('a.attempt_id = ?',$atid);
        

        $result = $db->fetchAll($select);
                
            foreach ($result as $at){

                if($at['answer'] == $at['correct_answer']){
                    $data['mark'] = $at['qmark'];
                    //$result = $db->update('quiz_answer', $data, 'id = ' . $at["id"]);
                } else if($at['answer'] != $at['correct_answer']) {
                    $data['mark'] = 0 ;
                    //$result = $db->update('quiz_answer', $data, 'id = ' . $at["id"]);
                } 

                //pr($result);exit;

                $result = $db->update('quiz_answer', $data, 'id = ' . $at["id"]);
            }

        return $result;

    }

    public function gettotalcorrect($atid)
    {

        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('b' => 'quiz_answer'), array('id','answer','correct_answer','quesnum','amark'=>'mark','attempt_id'))
            ->where('b.attempt_id = ?',$atid)
            ->where('b.mark != 0');

        $result = $db->fetchAll($select);

         //update total mark
        $data['total_correct'] = count($result) ;
        $result2 = $db->update('quiz_attempt', $data, 'id = ' . $atid);

        return $result;
    }


}
