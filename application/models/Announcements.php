<?php

class App_Model_Announcements extends Zend_Db_Table
{

    protected $_name = 'announcements';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getWiki(array $where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'announcements'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'));

        foreach ($where as $what => $value) {
            $what = $what == 'id = ?' ? 'a.id = ?' : $what;
            $select->where($what, $value);
        }

        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getAnnouncements($order = 'a.created_date DESC',$results=false)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'announcements'))
            ->joinLeft(array('c' => 'courses'), 'c.id=a.course_id', array('c.code as course_name', 'c.title as course_title'))
            ->order($order);

        if ( $results )
        {
            return $db->fetchAll($select);
        }

        return $select;
    }

    public function getGeneralAnnouncementQuery($where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'announcements'))
            ->joinLeft(array('c' => 'user'), 'a.created_by=c.id', array('c.firstname', 'c.lastname'))
            ->order('a.created_date DESC');

        foreach ($where as $what => $value) {
            $what = $what == 'id = ?' ? 'a.id = ?' : $what;
            $select->where($what, $value);
        }

        return $select;
    }

    public function getCourseAnnouncement($where)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'announcements'))
            ->joinLeft(array('c' => 'user'), 'a.created_by=c.id', array('c.firstname', 'c.lastname'))
            ->order('a.created_date DESC');

        foreach ($where as $what => $value) {
            $what = $what == 'id = ?' ? 'a.id = ?' : $what;
            $select->where($what, $value);
        }

        return $db->fetchAll($select);
    }

    public function getCourseAnnouncementCount($course_id)
    {
        $db = $this->getDefaultAdapter();
        $announcementdata = $db->select()
                ->from(array('a' => 'announcements'))
                ->where('a.course_id = ?', $course_id)
                ->where('visibility = ?', 1);

        $announcements = $db->fetchAll($announcementdata);

        return $announcements;
    }

}
