<?php
class App_Model_Qualification_CourseGroup extends Zend_Db_Table_Abstract
{
    protected $_name = 'course_group';
	protected $_primary = "id";

    public function getData($IdSubject){
        $db     = getDB2();

         $select = $db->select()
                   ->from(array('cg' =>$this->_name),array('IdCourseGroup'=>'id','IdSubject'))
                   ->join(array('cgs'=>'course_group_schedule'),'cgs.IdSchedule=cg.id',array('sc_id','sc_date_start','sc_date_end'))
                   ->where('cg.IdSubject = ? ',$IdSubject);
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>
