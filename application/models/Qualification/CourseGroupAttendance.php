<?php
class App_Model_Qualification_CourseGroupAttendance extends Zend_Db_Table_Abstract
{
    protected $_name = 'course_group_attendance';
	protected $_primary = "id";

    public function getAttendanceStatus($sc_id,$IdStudentRegistration){

        $db     = getDB2();

         $select = $db->select()
            				   ->from(array('cgs' =>'course_group_schedule'),array(''))
                               ->join(array('cgd'=>'course_group_schedule_detail'),'cgd.scDetId=cgs.sc_id',array(''))
                               ->join(array('cga'=>'course_group_attendance'),'cga.IdSchedule = cgd.id',array('attendance_status'=>'status'))
                               ->where('sc_id=?',$sc_id)//course_group_schedule
                               ->where('cga.IdStudentRegistration=?',$IdStudentRegistration);
        $result = $db->fetchRow($select);
        return $result;
    }
    
    
	
    
}
?>
