<?php
class App_Model_Qualification_StudentRegistration extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_studentregistration';
	protected $_primary = "IdStudentRegistration";

    public function getDatabyStudentId($sp_id){
       
        $db     = getDB();

         $select = $db->select()
            				   ->from(array('sr' => $this->_name))
                               ->join(array('p'=>'tbl_program'),'p.IdProgram = sr.IdProgram',array('ProgramName'))
                               ->where('sp_id=?',$sp_id);
        $result = $db->fetchAll($select);
        return $result;
    }
    
    
	public function getDataByTxnId($txn_id){
       
        $db     = getDB();

         $select = $db->select()
            				   ->from(array('sr' => $this->_name))
                               ->where('IdTransaction=?',$txn_id);
        $result = $db->fetchRow($select);
        return $result;
    }
    
     public function updateData($data, $id)
    {
        $db = getDB();
        $auth  = Zend_Auth::getInstance();

        if($auth->getIdentity()->login_as){//if admin
            $modifiedby = 1; //1= tbl_user
        }else{
            $modifiedby = $auth->getIdentity()->external_id;
        }

        $expdate = date('Y-m-d H:i:s');
        $expireDate = date("Y-m-d H:i:s",strtotime($expdate."+7 day"));

        $data['updated_role'] = $auth->getIdentity()->RoleId;
        $data['updated_iduser'] = $modifiedby;

        $db->update($this->_name, $data, "IdStudentRegistration = $id");
    }
    
    
}
?>
