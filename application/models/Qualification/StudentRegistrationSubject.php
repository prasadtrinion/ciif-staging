<?php
class App_Model_Qualification_StudentRegistrationSubject extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_studentregsubjects';
	protected $_primary = "IdStudentRegSubjects";

    public function getDatabyStudentId($IdStudentRegistration){
       
        $db     = getDB();

        $sql =   $db->select()
		            ->from(array('sr' => 'tbl_studentregistration'), array('IdProgram'))
		            ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('ExemptionStatusSubject' => 'ExemptionStatus','IdStudentRegSubjects','IdCourseTaggingGroup'))
		            ->joinLeft(array('cgs'=>'course_group_schedule'),'cgs.sc_id = srs.IdCourseTaggingGroup',array('sc_date_start','sc_date_end','sc_start_time','sc_end_time'))
		            ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode','IdSubject'))
		            ->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ProgramName'))
		            ->joinLeft(array('ls'=>'tbl_landscapesubject'),'ls.IdSubject=srs.IdSubject AND ls.IdProgram=sr.IdProgram AND ls.IDProgramMajoring=sr.IdMajoringPathway',array('SubjectType'))
		            ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=ls.SubjectType',array('SubjectTypeDesc'=>'DefinitionDesc'))
		            ->where('sr.IdStudentRegistration = ?',(int)$IdStudentRegistration);
		$row =  $db->fetchAll($sql);
		return $row;
       
    }
    
    
	public function getCourseExamRegistration($IdStudentRegistration,$est_id){
		
		$db     = getDB2();
		 
		$sql =   $db->select()
		            ->from(array('sr' => 'tbl_studentregistration'), array('IdProgram'))
		            ->join(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array('ExemptionStatusSubject' => 'ExemptionStatus','IdStudentRegSubjects'))
		            ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode','IdSubject'))
		            ->joinLeft(array('er'=>'exam_registration'),'er.IdStudentRegSubjects = srs.IdStudentRegSubjects',array('er_registration_type','er_id'))
		            ->joinLeft(array('erm'=>'exam_registration_main'),'erm.id=er.er_main_id',array(''))
		            ->where('sr.IdStudentRegistration = ?',(int)$IdStudentRegistration)
		            ->where('erm.exam_sitting_id=?',$est_id);
		 $row =  $db->fetchAll($sql);
		return $row;
                  
	} 
    
}
?>
