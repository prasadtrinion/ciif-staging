<?php
class App_Model_ExamSchedule extends Zend_Db_Table {
	
	public function init()
	{
		$this->locale = Zend_Registry::get('Zend_Locale');
		
	}
	
	
	public function getExamCenterList($formdata=null)
	{		
		$dbsms = getDB2();
		$select = $dbsms->select()
            	 		->from(array('ec'=>'tbl_exam_center'))
            	 		->where('ec.is_local=?',1)
            	 		->order('ec.ec_name');
        
        if(isset($formdata)){
        	if(isset($formdata['ec_id']) && $formdata['ec_id']!=''){
        		$select->where('ec.ec_id=?',$formdata['ec_id']);
        	}
        }
        $result = $dbsms->fetchAll( $select );

        return $result;
	}
	
	public function getExamCenterInfo($formdata=null)
	{		
		$dbsms = getDB2();
		$select = $dbsms->select()
            	 		->from(array('ec'=>'tbl_exam_center'))
            	 		->joinLeft(array('r'=>'tbl_examroom'),'r.examcenter_id=ec.ec_id',array('total_quota'=>'SUM(quota)'))
            	 		->where('r.status=?',1)
            	 		->where('ec.is_local=?',1)
            	 		->group('ec.ec_id')
            	 		->order('ec.ec_name');
        
        if(isset($formdata)){
        	if(isset($formdata['ec_id']) && $formdata['ec_id']!=''){
        		$select->where('ec.ec_id=?',$formdata['ec_id']);
        	}
        }
        $result = $dbsms->fetchAll( $select );

        return $result;
	}
	
	public function getExamDate($formdata=null)
	{
		
		$dbsms = getDB2();
		$select = $dbsms->select()
            	 		->from(array('es'=>'exam_schedule'),array('es_date'))
            	 		->join(array('sts'=>'exam_scheduletaggingslot'),'sts.sts_schedule_id = es.es_id',array())
            	 		->join(array('sl'=>'exam_slot'),'sl.sl_id = sts.sts_slot_id',array())
            	 		->join(array('tsc'=>'exam_taggingslotcenterroom'),'tsc.tsc_slotid = sl.sl_id',array())  
            	 		->where('es.es_date between "'.$formdata['date_from'].'" and "'.$formdata['date_to'].'"')  
            	 		->group('es.es_date')         	 		          	 		
            	 		->order('es.es_date');
            	 		
		if($formdata!=null){
			
        	if(isset($formdata['ec_id']) && $formdata['ec_id']!=''){
        		$select->where('tsc.tsc_examcenterid=?',$formdata['ec_id']);
        	}
        	
        }     
		$result = $dbsms->fetchAll( $select );

        return $result;
	}
	
	public function getExamSchedule($ec_id,$es_date)
	{
		
		$dbsms = getDB2();	  
		
		$select = $dbsms->select()
            	 		->from(array('es'=>'exam_schedule'),array('es_date','es_id_list'=>'GROUP_CONCAT(DISTINCT(es_id))'))
            	 		->join(array('sts'=>'exam_scheduletaggingslot'),'sts.sts_schedule_id = es.es_id',array())
            	 		->join(array('sl'=>'exam_slot'),'sl.sl_id = sts.sts_slot_id',array())
            	 		->join(array('tsc'=>'exam_taggingslotcenterroom'),'tsc.tsc_slotid = sl.sl_id',array())  
            	 		->where('tsc.tsc_examcenterid=?',$ec_id)
            	 		->where('es.es_date = ?',$es_date);
            	 		
		$result = $dbsms->fetchRow( $select );

        return $result;
	}
	
	public function getExamSlot($ec_id,$es_date)
	{		
		$dbsms = getDB2();
		
		$select = $dbsms->select()
            	 		->from(array('es'=>'exam_schedule'),array('es_id','es_date'))
            	 		->join(array('sts'=>'exam_scheduletaggingslot'),'sts.sts_schedule_id = es.es_id',array('sts_id'))
            	 		->join(array('sl'=>'exam_slot'),'sl.sl_id = sts.sts_slot_id',array('sl_id','sl_name','sl_starttime'))
            	 		->join(array('tsc'=>'exam_taggingslotcenterroom'),'tsc.tsc_slotid = sl.sl_id',array())  
            	 		->where('es.es_date = ?',$es_date) 
            	 		->where('tsc.tsc_examcenterid=?',$ec_id) 
            	 		->group('sts.sts_slot_id')     	 		
            	 		->order('es.es_date');

        $result = $dbsms->fetchAll( $select );

        return $result;
	}
	
	
	public function getRegisteredCandidate($ec_id,$es_id_list,$slot_id){
		
		$dbsms = getDB2();
		
		$es_id_list = explode(",", $es_id_list);
		
		$select = $dbsms->select()
            	 		->from(array('es'=>'exam_schedule'),array(''))
            	 		->join(array('sts'=>'exam_scheduletaggingslot'),'sts.sts_schedule_id = es.es_id',array())
            	 		->join(array('sl'=>'exam_slot'),'sl.sl_id = sts.sts_slot_id',array())
            	 		->join(array('tsc'=>'exam_taggingslotcenterroom'),'tsc.tsc_slotid = sl.sl_id',array()) 
            	 		->join(array('er'=>'exam_registration'),'er.examcenter_id = tsc.tsc_examcenterid and er.examsetup_id = es.es_examsetup_id and er.examsetupdetail_id = es.es_esd_id and er.examschedule_id = es.es_id and er.examtaggingslot_id = sts.sts_id',array('total_candidate'=>'count(er_id)'))  
            	 		->where('er.er_registration_type IN (1,4,10)')            	 		 
            	 		->where('tsc.tsc_examcenterid=?',$ec_id)
            	 		->where('es.es_id IN (?)',$es_id_list)
            	 		->where('sl.sl_id=?',$slot_id);
            	 		
		$result = $dbsms->fetchRow( $select );

        return $result;
	}
	
	public function getExamException($ec_id,$es_date){
		
		$dbsms = getDB2();
		
		$select_exception = $dbsms->select()
								  ->from(array('ese'=>'exam_schedule_exception'),array('ese_exam_date'))
								  ->where('ese.ese_exam_date = ?',$es_date)
								  ->where('ese.ec_id=?',$ec_id)
								  ->group('ese.ese_exam_date');
		$result = $dbsms->fetchRow( $select_exception );

        return $result;
	}
	
}
?>