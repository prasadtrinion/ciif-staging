<?php

class App_Model_CurriculumCourses extends Zend_Db_Table
{

    protected $_name = 'curriculum_courses';
    protected $_primary = 'id';

    public function init()
    {
        $this->_locale = Zend_Registry::get('Zend_Locale');
    }

    public function getData($curriculum_id, $results = true)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'curriculum_courses'), array())
            ->join(array('c' => 'courses'), 'c.id=a.course_id')
            ->where('a.curriculum_id = ?', $curriculum_id);

        if ($results) {

            $result = $db->fetchAll($select);
            return $result;
        } else {
            return $select;
        }
    }

    public function getDataByExternal($programId)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'curriculum_courses'), array('curriculum_id'))
            ->joinLeft(array('b' => 'curriculum'), 'a.curriculum_id = b.id', array('name','code'))
            ->join(array('c' => 'courses'), 'c.id=a.course_id')
            ->where('c.external_id = ?', $programId);

        $result = $db->fetchAll($select);
        return $result;

    }

    public function getData2($curriculum_id, $arraySubject)
    {
        $db = $this->getDefaultAdapter();

        $select = $db->select()
            ->from(array('a' => 'curriculum_courses'), array())
            ->join(array('c' => 'courses'), 'c.id=a.course_id')
            ->where('a.curriculum_id = ?', $curriculum_id)
            ->where('c.external_id IN (?)', $arraySubject);

        $result = $db->fetchAll($select);
        return $result;

    }
}
