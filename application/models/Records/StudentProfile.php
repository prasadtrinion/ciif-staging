<?php

class App_Model_Records_StudentProfile extends Zend_Db_Table_Abstract
{
    
    protected $_name = 'student_profile';
    protected $_primary = "sp_id";
    
    public function init(){
    	
    	 $this->db = getDB2();
    }
    
  	public function addData($data){
        $id = $this->db->insert($data);
        $db2 = getDB2();
	    return $id = $db2->lastInsertId();
    }  
    
    
 	public function deleteData($id){
        $this->db->delete($this->_primary .' =' . (int)$id);
    }
	
}

?>