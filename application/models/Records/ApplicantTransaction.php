<?php

class App_Model_Records_ApplicantTransaction extends Zend_Db_Table_Abstract
{
    
    protected $_name = 'applicant_transaction';
    protected $_primary = "at_trans_id";
    
    public function init(){
    	
    	 $this->db = getDB();
    }
    
  	public function addData($data){
  		//$db = Zend_Db_Table::getDefaultAdapter();
        $this->db->insert($this->_name,$data);
        return $this->db->lastInsertId();
    }
    
    public function updateData($data,$id){
    	

        if ( $id == null || $id == 0 )
        {
            throw Exception('Invalid Transaction ID');
        }
        
        $this->db->update($this->_name, $data,$this->db->quoteInto("at_trans_id = ?",$id));
    }
    
    
 	public function deleteData($id){
        $this->db->delete($this->_primary .' =' . (int)$id);
    }

	public function getData($sp_id){

         $select = $this->db->select()
           			  ->from(array('at'=>$this->_name))
           			  ->join(array('rt'=>'tbl_registration_type'),'rt.rt_id=at.at_appl_type',array('registration_type'=>'rt_activity'))
           			  ->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=at.at_status',array('status'=>'DefinitionDesc','idDefinition','transaction_status'=>'idDefinition'))
           			  
           			  ->joinLeft(array('mr'=>'membership_registration'),'mr.mr_transaction_id=at.at_trans_id',array('mr_id','mr_status'))
           			  ->joinLeft(array('m'=>'tbl_membership'),'m.m_id = mr.mr_membershipId',array('m_name'))
           			  ->joinLeft(array('d2'=>'tbl_definationms'),'d2.idDefinition=mr.mr_status',array('membership_status'=>'DefinitionDesc','membership_status_id'=>'idDefinition'))  
           			
           			  ->joinLeft(array('sr'=>'tbl_studentregistration'),'sr.IdTransaction=at.at_trans_id',array('IdProgram'))
           			  ->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ProgramName','ProgramCode','require_membership'))           			  
           			  ->joinLeft(array('d3'=>'tbl_definationms'),'d3.idDefinition=sr.TransactionStatus',array('qualification_status'=>'DefinitionDesc','qualification_status_id'=>'idDefinition'))
           			           			  
           			  ->where('at_appl_id=?',$sp_id)
           			  ->group('at_appl_id'); //at_appl_id==student_profile
            
        $rows = $this->db->fetchAll($select);
      
       
        return $rows;
    }
    
	public function getActiveTransaction($sp_id){

         $select = $this->db->select()
           			  ->from(array('at'=>$this->_name))
           			  ->where('at.at_status=?',1607)          			  
           			  ->where('at_appl_id=?',$sp_id);
            
        $rows = $this->db->fetchRow($select);
       
        return $rows;
    }
    
	public function getTransaction($sp_id){

         $select = $this->db->select()
           			  ->from(array('at'=>$this->_name))       			  
           			  ->where('at_appl_id=?',$sp_id);
            
        $rows = $this->db->fetchRow($select);
       
        return $rows;
    }

	public function getTransactionById($txnid){

         $select = $this->db->select()
           			  ->from(array('at'=>$this->_name))
           			  ->where('at_trans_id=?',$txnid);

        $rows = $this->db->fetchRow($select);

        return $rows;
    }
}

?>