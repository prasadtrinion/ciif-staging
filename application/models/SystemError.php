<?php
class App_Model_SystemError extends Zend_Db_Table_Abstract
{
    protected $_name = 'system_error';
    protected $_primary = "se_id";

    public function addData($data){
        $db = getDB2();
        $id = $db->insert($this->_name,$data);
        return $id;
    }

    public function updateData($data,$id){
        $db = getDB2();
        $db->update($this->_name,$data,"ad_id =".(int)$id);
    }

    public function deleteData($id){
        $db = getDB2();
        $db->delete($this->_name,$this->_primary .' =' . (int)$id);
    }

    public function getDatabyId ($id){

        $db = getDB2();

        $select = $db ->select()
            ->from(array('q'=>$this->_name))
            ->where($this->_primary." = ?",$id);

        $row = $db->fetchRow($select);
        return $row;
    }
}

?>