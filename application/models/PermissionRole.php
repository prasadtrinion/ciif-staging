<?php

class App_Model_PermissionRole extends Zend_Db_Table
{

    protected $_name = 'permission_role';
    protected $_primary = 'id';
    protected $db;
    protected $locale;

    public function init()
    {
        $this->locale = Zend_Registry::get('Zend_Locale');
        $this->db = $this->getDefaultAdapter();
    }

    public function getAll($force=false)
    {
        $cache = Zend_Registry::get('cache');

        $result = $cache->load('permission');

        if(  !$result || $force == true) {
            $roles = $this->fetchAll()->toArray();

            $data = array();
            foreach ($roles as $role) {
                $data[$role['role']] = json_decode($role['permission'], true);
            }

            $cache->save($data, 'permission');

            return $data;
        }
        else
        {
            return $result;
        }
    }

    public function updateAll(array $data)
    {

        foreach ( $data['role'] as $role => $roledata ) {

            $this->updatePerm($role, $roledata);

        }

    }

    public function updatePerm( $role, array $roledata)
    {
        $db = $this->db;

        $check = $this->fetchRow(array('role = ?' => $role ));

       if ( empty($check) )
       {
           $this->insert(array('role' => $role));
       }

        $data = array(
                        'modified_by'   => Zend_Auth::getInstance()->getIdentity()->id,
                        'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'permission'    => json_encode($roledata)
        );

        $this->update($data, array('role = ?' => $role));
    }
}
