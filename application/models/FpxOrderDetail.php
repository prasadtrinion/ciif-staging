<?php
/**
 * @copyright Copyright (c) 2016, MTCSB
 */
class App_Model_FpxOrderDetail extends Zend_Db_Table_Abstract {

    /**
     * The default table name
     */
    protected $_name = 'fpx_order_detail';
    protected $_primary = "id";


    public function getData($fpx_id, $invoiceId)
    {
        $db = getDB2();
        $selectData = $db->select()
            ->from(array('a' => $this->_name))
            ->join(array('b' => 'fpx_transaction'), 'b.order_id = a.order_id')
            ->where("b.id = ?", (int) $fpx_id)
            ->where("a.invoice_id = ?", (int) $invoiceId);
        $row = $db->fetchRow($selectData);
        return $row;
    }


}