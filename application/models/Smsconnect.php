<?php


class App_Model_Smsconnect {
    private $db;

    public function connect(){
        $this->db = getDB2();
    }

    public function scheduleexam ($data,$id){
    $this->connect();
        //$this->db->delete("exam_registration","er_id = '49'");
         $this->db->update("exam_registration","er_id = '49'");
    }

    public function update($table,$data,$where) {
        $this->connect();
        $this->db->update($table,$data,$where);

    }

    public function getExamLandscape($IdProgram, $params = array()) {
        $db = getDB2();
        $select = $db->select()
            ->from(array('a' => 'tbl_landscape'), array('IdLandscape', 'IdProgram', 'ProgramDescription'))
            ->join(array('b' => 'tbl_program_scheme'), 'b.IdProgramScheme = a.IdProgramScheme', array('IdProgramScheme'))
            ->join(array('c' => 'tbl_definationms'), 'b.mode_of_program = c.idDefinition', array('mode_of_program_desc' => 'DefinitionDesc'))
            ->join(array('d' => 'tbl_markdistribution_setup'), 'd.mds_landscape = a.IdLandscape', array())
            ->join(array('e' => 'exam_setup_detail'), 'e.esd_mds_id = d.mds_id', array())
            ->join(array('f' => 'exam_schedule'), 'f.es_esd_id = e.esd_id', array())
            ->join(array('g' => 'tbl_program'), 'g.IdProgram = a.IdProgram', array('exam_closing_date'))
            ->joinLeft(array('core' => 'tbl_programrequirement'), 'a.IdLandscape = core.IdLandscape AND core.SubjectType = 394', array('core.CreditHours as CoreCH'))
            ->joinLeft(array('elc'  => 'tbl_programrequirement'), 'a.IdLandscape = elc.IdLandscape  AND elc.SubjectType  = 275', array('elc.CreditHours as ElcCH'))
            ->where('a.IdProgram = ?', $IdProgram)
            ->where('a.Active = ?', 1)
            ->group('a.IdLandscape');

        if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
            $select->where('f.es_date > DATE_SUB(f.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
        }
        else {
            $select->where('f.es_date > DATE_SUB(f.es_date, INTERVAL g.exam_closing_date DAY)');
        }

        $result = $db->fetchAll($select);
        return $result;
    }


    public function getProgramGroup() {
        $db = getDB2();
        $lstrSelect = $db->select()
            ->from(array("a" => "tbl_program_group"))
            ->where("a.active = 1")
            ->order('a.name asc')
           ;
        $larrResult = $db->fetchAll($lstrSelect);
        //print_r($larrResult);exit;
        return $larrResult;
    }

    public function getProgramByGroup($idGroup) {
        $db = getDB2();
        $lstrSelect = $db->select()
            ->from(array("a" => "tbl_program"))
            ->where("a.group_id = ?",$idGroup)
            ->where("a.Active = 1")
        ;
        $larrResult = $db->fetchAll($lstrSelect);
        return $larrResult;
    }

    public function getExamOption($IdLandscape, $params = array()){
        $db = getDB2();
        //$IdLandscape = '1';
        $select = $db->select()
            ->from(array('a' => 'exam_setup'), array('examsetup_id' => 'es_id', 'es_id', 'es_name', 'es_online','es_idLandscape'))
            ->join(array('b' => 'exam_setup_detail'), 'b.es_id = a.es_id', array('esd_id', 'esd_type', 'esd_idSubject'))
            ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array())
            ->join(array('d' => 'tbl_program'), 'd.IdProgram = a.es_idProgram', array('exam_closing_date'))
            ->where('a.es_idLandscape = ?',$IdLandscape)
            ->where("c.es_active = 1")
            ->order('a.es_name ASC')
            ->group('a.es_id');

        if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
        }
        else {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL d.exam_closing_date DAY)');
        }

        $result = $db->fetchAll($select);

        foreach($result as $index => $row) {
            // pr($row);
            $examsetup_id = $row['examsetup_id'];

            $select = $db->select()
                ->from(array('b' => 'exam_setup_detail'), array('esd_id'))
                ->join(array('d' => 'programme_exam'), 'b.esd_pe_id = d.pe_id', array('pe_id', 'pe_name'))
                ->join(array('e' => 'tbl_markdistribution_setup'), 'e.mds_id = b.esd_mds_id', array('mds_id', 'mds_name'))
                ->where('b.es_id = ?',$examsetup_id)
                ->where('d.IdLandscape = ?', $IdLandscape)
                ->group('b.esd_id');
            $count_papers = $db->fetchAll($select);
            $count_papers = count($count_papers);
            // pr("count_papers = $count_papers");

            $select = $db->select()
                ->from(array('b' => 'exam_setup_detail'), array('esd_id'))
                ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array('es_id'))
                ->join(array('d' => 'programme_exam'), 'b.esd_pe_id = d.pe_id', array('pe_id', 'pe_name'))
                ->join(array('e' => 'tbl_markdistribution_setup'), 'e.mds_id = b.esd_mds_id', array('mds_id'))
                ->join(array('f' => 'tbl_program'), 'f.IdProgram = d.IdProgram', array('exam_closing_date'))
                ->where('b.es_id = ?',$examsetup_id)
                // ->where('c.es_date > ?', date('Y-m-d', strtotime('+14 days')))
                ->where('d.IdLandscape = ?', $IdLandscape)
                ->group('b.esd_id');

                if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
                    $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
                }
                else {
                    $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL f.exam_closing_date DAY)');
                }

            $count_schedule = $db->fetchAll($select);
            $count_schedule = count($count_schedule);
            // pr("count_schedule = $count_schedule");

            if($count_schedule < $count_papers) {
                unset($result[$index]);
                continue;
            }

            if($row['es_online']) {
                $result[$index]['es_online_desc'] = 'Online Examination';
            }
            else {
                $result[$index]['es_online_desc'] = 'Manual Examination';
            }

            $result[$index]['count_papers']   = $count_papers;
            $result[$index]['count_schedule'] = $count_schedule;
        }
        sort($result);
        return $result;
    }

    public function getExamDetail($es_id, $examschedule_id = 0, $params = array()) {
        $db = getDB2();
        $select = $db->select()
            ->from(array('b' => 'exam_setup_detail'), array('esd_id', 'esd_type', 'esd_idSubject'))
            ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array('examschedule_id' => 'es_id', 'es_id', 'es_exam_center'))
            ->join(array('d' => 'tbl_markdistribution_setup'), 'd.mds_id = esd_mds_id', array('mds_id', 'mds_name'))
            ->join(array('e' => 'programme_exam'), 'e.pe_id = b.esd_pe_id AND e.pe_id = d.mds_programme_exam', array('pe_id', 'pe_name'))
            ->joinLeft(array('f' => 'tbl_subjectmaster'), 'f.IdSubject = b.esd_idSubject', array('IdSubject', 'SubjectName', 'SubCode'))
            ->join(array('g' => 'tbl_program'), 'g.IdProgram = e.IdProgram', array('exam_closing_date'))
            ->join(array('l' => 'tbl_landscape'), 'l.IdLandscape = e.IdLandscape', array('IdProgramScheme'))
            ->join(array('ps' => 'tbl_program_scheme'), 'ps.IdProgramScheme = l.IdProgramScheme', array('IdMode'=>'mode_of_program'))
            ->where('b.es_id = ?', $es_id)
            ->where("c.es_active = 1")
            // ->where('c.es_date > ?', date('Y-m-d', strtotime('+14 days')))
            ->group('b.esd_id');

        if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
        }
        else {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL g.exam_closing_date DAY)');
        }

        if($examschedule_id) {
            $select->where('c.es_id = ?', $examschedule_id);
        }

        $result = $db->fetchAll($select);

        foreach($result as $index => $row) {
            $select = $db->select()
                ->from(array('b' => 'exam_setup_detail'), array())
                ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array('examschedule_id' => 'es_id', 'es_id', 'es_esd_id'))
                ->join(array('f' => 'tbl_exam_center'), 'f.ec_id = c.es_exam_center', array('ec_id', 'ec_name'))
                ->join(array('a' => 'exam_setup'), 'a.es_id = b.es_id', array())
                ->join(array('g' => 'tbl_program'), 'g.IdProgram = a.es_idProgram', array('exam_closing_date'))
                ->where('b.es_id = ?', $es_id)
                ->where('b.esd_id = ?', $row['esd_id'])
                ->where("c.es_active = 1")
                // ->where('c.es_date > ?', date('Y-m-d', strtotime('+14 days')))
                ->group('f.ec_id');

            if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
                $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
            }
            else {
                $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL g.exam_closing_date DAY)');
            }

            $exam_centers = $db->fetchAll($select);
            $result[$index]['exam_centers']  = $exam_centers;
            $result[$index]['esd_type_desc'] = '';
            $result[$index]['module_desc']   = '';

            if($row['IdSubject']) {
                $result[$index]['module_desc'] = $row['SubCode'] .' — '. $row['SubjectName'];
            }

            if($row['esd_type'] == 1) {
                $result[$index]['esd_type_desc'] = 'Programme Based';
            }
            elseif($row['esd_type'] == 2) {
                $result[$index]['esd_type_desc'] = 'Modular Based';
            }
        }

        if($examschedule_id && count($result)) {
            $result = $result[0];
        }

        return $result;
    }

    public function getExamDate($esd_id, $ec_id, $params = array()) {
        $ecrDB  = new App_Model_Exam_ExamRoom();
        $db = getDB2();
        $select = $db->select()
            ->from(array('a' => 'exam_setup'), array())
            ->join(array('b' => 'exam_setup_detail'), 'b.es_id = a.es_id', array())
            ->join(array('c' => 'exam_schedule'), 'c.es_esd_id = b.esd_id', array('examschedule_id' => 'es_id', '*'))
            ->join(array('d' => 'tbl_markdistribution_setup'), 'd.mds_id = esd_mds_id', array())
            ->join(array('e' => 'programme_exam'), 'e.pe_id = b.esd_pe_id AND e.pe_id = d.mds_programme_exam', array('pe_name'))
            ->joinLeft(array('f' => 'tbl_definationms'), 'f.idDefinition = c.es_session', array('es_session_desc' => 'DefinitionDesc'))
            ->join(array('g' => 'tbl_program'), 'g.IdProgram = e.IdProgram', array('exam_closing_date'))
            ->where('b.esd_id = ?', $esd_id)
            ->where('c.es_exam_center = ?', $ec_id)
            ->where('c.es_date > ?', date('Y-m-d', strtotime('+14 days')))
            ->where("c.es_active = 1")
            ->order('c.es_date ASC')
            ->order('c.es_start_time');

        if(isset($params['exam_closing_date']) && $params['exam_closing_date']) {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL ' . $params['exam_closing_date'] . ' DAY)');
        }
        else {
            $select->where('c.es_date > DATE_SUB(c.es_date, INTERVAL g.exam_closing_date DAY)');
        }

        $result = $db->fetchAll($select);

        $schedule = array();

        for($i=1; $i<=12; $i++) {
            $month = str_pad($i, 2, "00", STR_PAD_LEFT);
            $schedule[$month] = array();
        }

        foreach($result as $row) {
            $month = date('m', strtotime($row['es_date']));
            $month = str_pad($month, 2, "00", STR_PAD_LEFT);
            $date  = $row['es_date'];

            $select = $db->select()
                ->from(array('a' => 'exam_registration'), array('count' => 'COUNT(*)'))
                ->where('a.examschedule_id = ?', $row['es_id'])
                ->where('a.er_registration_type in (0, 1, 4, 9)');
            $registered = $db->fetchRow($select);

            $row['total_seat']     = 30; // TODO: tukar benda ni jadi field. sape yg nak buat??
            $row['available_seat'] = $row['total_seat'] - $registered['count'];

            $row['es_date_dmy']        = date('d-m-Y', strtotime($row['es_date']));
            $row['es_start_time_ampm'] = date('H:iA', strtotime($row['es_start_time']));
            $row['es_end_time_ampm']   = date('H:iA', strtotime($row['es_end_time']));
            $row['seat_quota']         = $ecrDB->getQuota($row['examschedule_id'], $row['es_exam_center']);

            $schedule[$month][$date][] = $row;
        }



        return $schedule;
    }



}
