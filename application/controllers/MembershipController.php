

<?php

ini_set('display_errors', 'on');

class MembershipController extends Zend_Controller_Action
{

    public function init()
    {   

        $this->smsDb = getDB();

        $auth = Zend_Auth::getInstance();
        $this->auth = $auth;
         $this->userDb = new App_Model_User();

        if (isset($this->auth->getIdentity()->login_as)) {
            $this->sp_id = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        } else {
            $this->sp_id = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }

        Zend_Layout::getMvcInstance()->assign('nowrap', 1);

        $this->uploadDir = Cms_System_Links::dataFile();

         $MembershipDB = new App_Model_Membership_Membership();
         $this->definationDB =new Admin_Model_DbTable_Definition();
    }

    public static function userSidebar($active = 'dashboard')
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view->active = $active;
        $view->setBasePath(APPLICATION_PATH . '/views/');
        return $view->render('plugins/usersidebar.phtml');
    }


public function paymentproofAction()
    {

 $auth = Zend_Auth::getInstance()->getIdentity();

        
        $user_id = $auth->id;
        $this->view->mr_id = $this->getParam('mr_id');

        $member_app = new App_Model_Membership_MembershipApplication();
         $member_renewal= $member_app->get_renew_fee($user_id);
           $this->view->member_renewal = $member_renewal;
         
       // Cms_Hooks::push('user.sidebar', self::userSidebar('dashboard'));
        if ($this->getRequest()->isPost()) {
         
        $this->view->user_id = $user_id;
        $member_id = $this->getParam('id');


              
        $formData = $this->getRequest()->getPost();
        $this->view->formData = $formData;
       
        $this->view->member_id =  $member_id;

        $sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());
        $dbciifpdoc= new App_Model_Ciifpdoc;
         $dbUser = new App_Model_User;
         $get_info      = $dbUser->getUserInfo($user_id);
         
         $sub_type      = $get_info['id_member'];

         $member_type   = $get_info['id_member_subtype'];
         $degree_type   = $get_info['id_qualification'];




        
         $get_memberfee  = $dbUser->getFee($sub_type,$member_type,$degree_type);
// $member_payment = $this->definationDB->getdefCode('Paid');
         // $member_status = $this->definationDB->getByCode('Status');

         $member_status = $this->definationDB->getdefCode('Paid');
                  $upload = new Zend_File_Transfer();
                    
                   // $id= $userid1['id'];
                            $membership_id= $user_id;
                              $files  = $_FILES;
                                   $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
                                 $target_file1 = 'RenewalDoc'.$_FILES['file1']['name'][$i];
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH."\\".$target_file1)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
                                           } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }

     if($user_id)
         {
          $invoice = new App_Model_PerformaInvoice();
               
                
            $data_invioce = array('user_id' => $user_id,
                                   'description' => 'Renewal Payment',
                              'amount' => $get_memberfee['amount'],
                    'invoice_no' => 'CIIF'.  date('YHims').$user_id,
                    'created_user' => $user_id,
                    'created_date' => date("Y-m-d")
                    // 'payment_status'=>$member_status[0]['idDefinition']
                         );
                    $invoice_no = $invoice->insert($data_invioce);
                    }
        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }

        if ($sp) {
            //get membership info
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);
            $this->view->membership = $membership;

            //get qualification info
            $applicationDb = new App_Model_SmsApplication();
            $currDb = new App_Model_Curriculum();
            $smsStudentRegistrationDB = new App_Model_StudentRegistration();

            $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
            $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
            if ($myApplication) {
                foreach ($myApplication as $index => $app) {
                    $smsprogramid = $app["IdProgram"];
                    if ($smsprogramid) {
                        $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                        $myApplication[$index]['cid'] = $curiculum['id'];

                        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjects($sp,$smsprogramid);
                        $myApplication[$index]['modules'] = $StudentSubjectsDetail;
                    }
                }
            }
            $this->view->myApplication = $myApplication;
            //pr($myApplication);

            //enrolment matrix
            $matrix = array(
                'active_flag'                => true,
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );


            //check active
            if (isset($membership[0]["mr_status_id"]) && $membership[0]["mr_status_id"] != 1607) { //active
                $matrix['active_flag'] = false;
            } else {
                if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                }
            }

            //check expired
            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired
                    $matrix['expired_flag'] = true;
                }
            }
            // pr($matrix);
            $this->view->matrix = $matrix;

        }//end sp

}
    }

    public function qualificationpaymentAction()
    {
          $auth = Zend_Auth::getInstance()->getIdentity();
        $userid = $this->auth->getIdentity()->id; 

         $programRegister = new App_Model_ProgramRegister();
             $get_amount = $programRegister->get_regamount($userid);

              $get_exem_amount = $programRegister->get_exemamount($userid);
              // echo '<pre>';
              // print_r($get_amount);
              // die();
               $this->view->get_exem_amount = $get_exem_amount;
            
             $this->view->get_amount = $get_amount;
              $user = $this->userDb->fetchRow(array('id = ?' => $userid))->toArray();


        if($user['nationality'] = 'MY')
        {
            $rate = 1;
            $this->view->rate = $rate;
        }else
        {

                $feeModel =  new App_Model_MemberFeeStructure();
                $rate = $feeModel->getCurrencyRate();
                $this->view->rate = $rate['cr_exchange_rate'];
        }
            
        
        if ($this->getRequest()->isPost()) {
         $auth = Zend_Auth::getInstance()->getIdentity();

        
        $user_id = $auth->id;
        $this->view->user_id = $user_id;
        $member_id = $this->getParam('id');

              
            $formData = $this->getRequest()->getPost();
            $this->view->formData = $formData;
       
        $this->view->member_id =  $member_id;

        $sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }

        if ($sp) {
            //get membership info
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);
            $this->view->membership = $membership;

            //get qualification info
            $applicationDb = new App_Model_SmsApplication();
            $currDb = new App_Model_Curriculum();
            $smsStudentRegistrationDB = new App_Model_StudentRegistration();

            $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
            $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
            if ($myApplication) {
                foreach ($myApplication as $index => $app) {
                    $smsprogramid = $app["IdProgram"];
                    if ($smsprogramid) {
                        $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                        $myApplication[$index]['cid'] = $curiculum['id'];

                        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjects($sp,$smsprogramid);
                        $myApplication[$index]['modules'] = $StudentSubjectsDetail;
                    }
                }
            }
            $this->view->myApplication = $myApplication;
            //pr($myApplication);

            //enrolment matrix
            $matrix = array(
                'active_flag'                => true,
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );


            //check active
            if (isset($membership[0]["mr_status_id"]) && $membership[0]["mr_status_id"] != 1607) { //active
                $matrix['active_flag'] = false;
            } else {
                if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                }
            }

            //check expired
            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired
                    $matrix['expired_flag'] = true;
                }
            }
            // pr($matrix);
            $this->view->matrix = $matrix;

        }//end sp

}
    }
    

           
          
    public function indexAction()
    {


        $auth = Zend_Auth::getInstance()->getIdentity();
        $this->view->auth = $auth;
        $dbUser = new App_Model_User();

        $MembershipDB = new App_Model_Membership_Membership();
        $userid = $this->auth->getIdentity()->id;

        $user_upgrade = $this->auth->getIdentity()->upgrade_member;
       $MembershipAppDB = new App_Model_Membership_MembershipApplication();

        $getMember= $dbUser->getMember($userid);

        
        $this->view->getMember = $getMember;

        $get_membership = $MembershipDB->get_member($userid);
        
        
    $this->view->user_upgrade == $get_membership['upgrade_member'];
        $this->view->application = $get_membership;
       
        $tab = $this->_getParam('tab', 'upgrade');
        $this->view->tab = $tab;

        // $get_status = $MembershipDB->get_status($get_membership['mr_status']);
        // $this->view->get_status = $get_status;
       
        Cms_Hooks::push('user.sidebar', self::userSidebar('membership'));

        $tab = $this->_getParam('tab', 'upgrade');
        $this->view->tab = $tab;

 
        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        } else {
            $sp = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }


        // $MembershipDB = new App_Model_Membership_Membership();
        // $membership = $MembershipDB->getMembershipInfo($sp);
        // $this->view->application = $membership;

        $MembershipDB = new App_Model_Membership_Membership();
        $membership = $MembershipDB->getMembershipInfo($userid);
        
        $upgrade_user = $dbUser->getUserInfo($userid);
        
    $this->view->upgrade = $upgrade_user;
    
    

       
 
        //check for upgade application
        $MembershipAppDB = new App_Model_Membership_MembershipApplication();
        $MembershipHistoryDB = new App_Model_Membership_MembershipRegistrationHistory();

        $upgrade_list = $MembershipAppDB->getDetailsInfo($userid, 1);
       
        
        $this->view->upgrade_list = $upgrade_list;

        $renew_list = $MembershipAppDB->getDetailsInfoByType($userid, 2);
      
        $this->view->renew_list = $renew_list;

        // $this->view->upgrade_list = $MembershipAppDB->getDetailsInfoByType($userid, 1);
         // $this->view->renew_list = $MembershipAppDB->getDetailsInfoByType($sp, 2);

        $this->view->reinstate_list = $MembershipAppDB->getDetailsInfoByType($sp, 3);
        $this->view->history = $MembershipHistoryDB->getDataByIdHistory($sp);

    }

    public function applyAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('apply'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        $userDb = new App_Model_User();
        $user = $userDb->getUser($this->auth->getIdentity()->id);

        $userid = $this->auth->getIdentity()->id;
        $mr_id = $this->getParam('mr_id');

        $memberDB = new App_Model_Membership_TblMembership();
        $this->view->membership_list = $memberDB->getListMembership();

        $MembershipDB = new App_Model_Membership_Membership();
        $membership = $MembershipDB->getMembershipById($mr_id);
        $this->view->membership = $membership;

        $memberFeeDb = new App_Model_Membership_MembershipFee();
        $this->view->member_fee = $memberFeeDb->getFeeByMemberId($membership['m_id']);

        $checklistDb = new App_Model_Membership_MembershipRegistrationChecklist();
        $document_checklist = $checklistDb->getListData($membership['m_id'], 1);

        $memberRegDoc = new App_Model_Membership_MembershipRegistrationDocuments();

        foreach ($document_checklist as $index => $doc) {
            $member_doc = $memberRegDoc->getDataDoc($doc['mrc_id'], $mr_id);
            if ($member_doc) {
                $document_checklist[$index]['mrd_id'] = $member_doc['mrd_id'];
                $document_checklist[$index]['mrd_name'] = $member_doc['mrd_name'];
            }

        }

        $this->view->document_checklist = $document_checklist;

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if (isset($this->auth->getIdentity()->login_as)) {//if admin
                $formData['mr_modifiedby'] = $this->auth->getIdentity()->login_as;
            } else {
                $formData['mr_modifiedby'] = $this->auth->getIdentity()->id;
            }

            unset($formData['title']);
            unset($formData['save']);

            //update data
            $formData['mr_modifieddt'] = date('Y-m-d H:i:s');
            $MembershipDB->updateApplication($formData, array('mr_id = ?' => $formData['mr_id']));

            //add documents info
            if (count($_FILES) > 0) {

                foreach ($_FILES as $fieldname => $doc) {

                    if ($doc['name'] != '') {
                        $filedata['mr_id'] = $formData['mr_id'];
                        $filedata['mrc_id'] = substr($fieldname, 6);
                        $filedata['mrd_name'] = $doc['name'];
                        $filedata['mrd_createddt'] = date('Y-m-d H:i:s');
                        $filedata['mrd_createdby'] = $this->auth->getIdentity()->id;
                        $memberRegDoc->addData($filedata);
                    }
                }
            }


            Cms_Common::notify('success', 'Information Updated');
            $this->redirect('/membership/declaration/mr_id/' . $formData['mr_id']);
        }

    }


    public function newApplyAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('apply'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        $userDb = new App_Model_User();
        $user = $userDb->getUser($this->auth->getIdentity()->id);

        $userid = $this->auth->getIdentity()->id;

        $memberDB = new App_Model_Membership_TblMembership();
        $this->view->membership_list = $memberDB->getListMembership();
    }

    public function addqualificaionAction(){

        

        Cms_Hooks::push('user.sidebar', self::userSidebar('pay'));

        $auth = Zend_Auth::getInstance()->getIdentity();
 $userModel = new App_Model_User();

    $programDb = new App_Model_ProgramCategory();

      $qualification = $this->definationDB->getByCode('Program Route');
        
        $this->view->application = $qualification;

         $userDb = new App_Model_User();
        $user = $userDb->getUser($this->auth->getIdentity()->id);
      $sub_qualification = $this->definationDB->getByCode('Program Level');
        
        $this->view->sub_qualification = $sub_qualification;
}

     public function qualificaionregisterAction(){

        

        Cms_Hooks::push('user.sidebar', self::userSidebar('pay'));

        $program_route = $this->getParam('program_route');
        $program_level = $this->getParam('program_level');
       
        $auth = Zend_Auth::getInstance()->getIdentity();
        $userModel = new App_Model_User();

        $programDb = new App_Model_ProgramCategory();

            $qualification = $programDb->get_program_fee($program_route,$program_level);

            $exemption = $programDb->get_program_fee($program_route,$program_level);

       
        $this->view->application = $qualification;


    $get_program_route= $programDb->get_program_code($program_route);
        $this->view->route = $get_program_route;
        
        $amount= $programDb->get_amount($program_route,$program_level);
         $this->view->amount = $amount;
        
        $get_program_level= $programDb->get_program_code($program_level);
        $this->view->level = $get_program_level;

 

        $sub_qualification = $this->definationDB->getByCode('Program Level');


        $this->view->sub_qualification = $sub_qualification;
}

public function programregisterAction(){
 $userid = $this->auth->getIdentity()->id;
     if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            
             $programRegister = new App_Model_ProgramRegister();
          $auth = Zend_Auth::getInstance()->getIdentity();
        $userid = $this->auth->getIdentity()->id; 
        $uniqueId = 'CIIF'.date('Y').date('mhsi');
        $membershipId = $uniqueId.$userid;

        $payment_status = $this->definationDB->getByCode('Status');


        foreach ($formData['program'] as $program) {
              $data['mr_id']       = $userid;
              $data['program_route'] = $formData['program_route'];
              $data['program_level'] = $formData['program_level'];
               $data['program'] = $program;
              $data['payment_status'] = 1658;
            
             $data['created_date']    = date('y-m-d H:i:s');
                $data['created_by']  = $userid;
                       $query = $programRegister->insert($data);

           if(isset($formData['exemption_program'])){

            foreach ($formData['exemption_program'] as $value) {
            
               
            $update['exemption_program'] = 1;
            $query = $programRegister->update($update,array('program = ?' =>$value));
    
          

           
            $get_amount = $programRegister->get_program_amount($program);

            $reg_amount =  $programRegister->get_regamount($userid);
          
           
    
            foreach ($get_amount as $amount) {
            $updateamt['program_amount'] =$amount['calculated_amount'];
                
                      $exem_program =   $programRegister->update($updateamt,array('program = ?' =>$program));

            
        }
                      $updateamt['total_amount'] = $reg_amount['sum'];

      $exem_program =   $programRegister->update($updateamt,array('program = ?' =>$program));

           }
}

        
    }

    $programExemption = new App_Model_ProgramExemption();

    foreach ($formData['exemption'] as $value) {
            
            $data1['user_id']       = $userid;
              $data1['program_route'] = $formData['program_route'];
              $data1['program_level'] = $formData['program_level'];
               $data1['program_id'] = $value;
    
             $data1['created_date']    = date('y-m-d H:i:s');
                $data1['created_by']  = $userid;
            $query = $programExemption->insert($data1);

            $exem_amount = $programRegister->get_exemption_amount($program);
          
           
    
            foreach ($exem_amount as $amount) {
            $updateamt1['f_amount'] =$amount['calculated_amount'];
                
                      $exem_program =   $programExemption->update($updateamt1,array('id_ep = ?' =>$query));

            
        }
        }
        

       

             

              $invoice = new App_Model_PerformaInvoice();
               
                
               
            $data_invioce = array('user_id' => $userid,
                                   'description' => 'Programme Payment',
                                   'fee_type' => 1,
                              'amount' => $updateamt['total_amount'],
                    'invoice_no' => 'CIIF'.  date('YHims').$userid,
                    'created_user' => $userid,
                    'created_date' => date("Y-m-d"),
                         );
           
                    $invoice_no = $invoice->insert($data_invioce);

                     $invoice_data = new App_Model_PerformaInvoiceDetails();
               
                // echo $userid;
                // die();
                 $get_qualifiacion = $programRegister->get_qualification($userid);

                foreach ($get_qualifiacion as  $value) {
                    # code...
              
            $data_invioce_details = array('user_id' => $userid,
                                    'description' => 'Programme Payment',
                                    'performa_id' => $invoice_no,
                                    'amount' => $value['program_amount'],
                                    'id_program' => $value['program'],
                                    'invoice_no' => 'CIIF'.  date('YHims').$userid,
                                    'created_user' => $userid,
                                    'created_date' => date("Y-m-d"),
                       
                         );
                    $invoice_performa = $invoice_data->insert($data_invioce_details);
                   
                      }     
                        
            
                    
             $userModel = new App_Model_User();

             $userdata = array('idprogram' =>2 );
                    
                    $user_id1 =  $userModel->update($userdata, array('id = ?' => $userid));
              

             // Cms_Common::notify('success', 'Your Successfully applied for the Programme');

             $this->redirect('/qualification/index');

}
        
}
    public function declarationAction()
    
    {
    
        Cms_Hooks::push('user.sidebar', self::userSidebar('declaration'));

        $userDb = new App_Model_User();
        $user = $userDb->getUser($this->auth->getIdentity()->id);

        $mr_id = $this->getParam('mr_id');
        $this->view->mr_id = $mr_id;

        $memberDB = new App_Model_Membership_TblMembership();
        $this->view->membership_list = $memberDB->getListMembership();

        $MembershipDB = new App_Model_Membership_Membership();
        $membership = $MembershipDB->getMembershipById($mr_id);
        $this->view->membership = $membership;


        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if (isset($this->auth->getIdentity()->login_as)) {//if admin
                $formData['d_createdby'] = $this->auth->getIdentity()->login_as;
            } else {
                $formData['d_createdby'] = $this->auth->getIdentity()->id;
            }

            $formData['d_createdddt'] = date('Y-m-d H:i:s');

            unset($formData['submit-btn']);

            //update txn
            $applicantTransactionDB = new App_Model_Records_ApplicantTransaction();
            $txnData['at_status'] = 1603; //pending approval
            $applicantTransactionDB->updateData($txnData, $formData['at_trans_id']);

            //add data declaration


            //update membership submission
            $memberData['mr_status'] = 1603;//Pending Approval
            $memberData['mr_submit_date'] = date('Y-m-d H:i:s');
            $MembershipDB->updateDataMember($memberData, $formData['mr_id']);


            if ($membership['at_appl_type'] == 3) {

                $template_id = 1;

                //update tbl_studntregistration
                $studentRegDb = new App_Model_Qualification_StudentRegistration();
                $studentRegistration = $studentRegDb->getDataByTxnId($formData['at_trans_id']);

                $regData['TransactionStatus'] = 1603;
                $regData['updated_iduser'] = $this->auth->getIdentity()->id;
                $regData['updated_role'] = $this->auth->getIdentity()->role;

                $studentRegDb->updateData($regData, $studentRegistration['IdStudentRegistration']);

            } else if ($membership['at_appl_type'] == 1) {

                $template_id = 5;
            }

            //email
            $info = array(
                'template_id'     => $template_id,
                'name'            => $membership['std_fullname'],
                'program_name'    => '',
                'membership_name' => $membership['MembershipName']
            );

            $email = new icampus_Function_Email_Email();
            $email->sendEmail($info);


            Cms_Common::notify('success', 'Your application has been submitted.');

            $this->redirect('/dashboard');


        }

    }

    public function viewMembershipAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('view'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        $userid = $this->auth->getIdentity()->id;
        $mr_id = $this->getParam('mr_id');

        $this->view->userid = $userid;
        $this->view->mr_id = $mr_id;

        $MembershipDB = new App_Model_Membership_Membership();
        $membership = $MembershipDB->getMembershipById($mr_id);
        $this->view->application = $membership;

        $memberFeeDb = new App_Model_Membership_MembershipFee();
        $this->view->member_fee = $memberFeeDb->getFeeByMemberId($membership['m_id']);

    }

    public function payMembershipAction()
    {

        Cms_Hooks::push('user.sidebar', self::userSidebar('pay'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        $userid = $this->auth->getIdentity()->id;

        $userDb = new App_Model_User();
        $user = $userDb->getUser($userid);

        $mr_id = $this->getParam('mr_id');

        $this->view->userid = $userid;
        $this->view->mr_id = $mr_id;

        $this->view->order_id = 1;

        $MembershipDB = new App_Model_Membership_Membership();
        $membership = $MembershipDB->getMembershipById($mr_id);
        $this->view->application = $membership;

        //check transaction
        $applicantTransactionDB = new App_Model_Records_ApplicantTransaction();
        $transaction = $applicantTransactionDB->getTransaction($user['external_id']);

        $memberFeeDb = new App_Model_Membership_MembershipFee();
        $this->view->member_fee = $memberFeeDb->getFeeByMemberId($membership['m_id']);

        $paymentModeDB = new App_Model_Finance_PaymentMode();
        $this->view->paymentmodelist = $paymentModeDB->getPaymentModeList(1);
        $MembershipDB = new App_Model_Membership_Membership();

        $seq = $MembershipDB->getLastSequenceID();
        if(!$seq){
            $n='100001';

        }else{
            $n=$seq;
            $n = $n + 1;
        }
       $membershipRegID = str_pad($n, 5, 0, STR_PAD_LEFT);

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if (isset($formData['payment_mode'])) {
                if ($formData['payment_mode'] == '10') {

                   /* $data['mr_status'] = 1603; //pending sponsor
                    $data['mr_payment_status'] = 0; //pending
                    $MembershipDB->updateApplication($data, $where = 'mr_id=' . $formData['mr_id']);*/

                    if ($membership['std_corporate_id']) {
                        $corpId = $membership['std_corporate_id'];
                    } else {
                        $corpId = 35;
                    }
                    $timestamp = new Zend_Db_Expr('NOW()');
                    $dataSponsor = array(
                        'corporate_id'      => $corpId,
                        'external_id'       => $user['external_id'],
                        'sponsor_type'      => 1,
                        'sponsor_member_id' => $formData['mr_id'],
                        'created_date'      => $timestamp,
                        'created_by'        => $user['external_id'],
                        'sponsor_amount'    => $formData["payment_amount"],
                        'sponsor_balance'   => $formData["payment_amount"],
                        'payment_mode'      => 10,
                        'payment_mode_date' => $timestamp,
                        'status'            => 1 //applied
                    );
                    $MembershipDB->addRegData('tbl_sponsor_registration', $dataSponsor);

                    Cms_Common::notify('success', 'Bank Sponsor have been submitted. Your membership bank sponsor will be approve soon.');
                    $this->redirect('/membership');

                } else {

                   // $membershipRegID = $MembershipDB->getLastSequenceID();

                    $data['mr_status'] = 1607; //active
                    $data['mr_payment_status'] = 1; //paid
                    $data['mr_activation_date'] = date('Y-m-d H:i:s');
                    $data['mr_expiry_date'] = date('Y-m-d H:i:s', strtotime((date('Y')) . '-12-31'));
                    //$data['mr_expiry_date'] = date('Y-m-d H:i:s', strtotime(date("Y-m-d", time()) . " + 1 year"));
                    $data['mr_registrationId']= $membershipRegID;


                    $MembershipDB->updateApplication($data, $where = 'mr_id=' . $formData['mr_id']);

                    if ($transaction['at_appl_type'] == 3) {
                        Cms_Common::notify('success', 'Payment Successfull. Your membership is activated. You may proceed for qualification enrollment once qualification application approved.');
                        $this->redirect('/membership');
                    } else if ($transaction['at_appl_type'] == 1) {
                        Cms_Common::notify('success', 'Payment Successfull. Your membership is activated!');
                        $this->redirect('/membership');
                    }
                }
            }
        }

    }


    public function renewMembershipAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('pay'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        $userid = $this->auth->getIdentity()->id;
        $mr_id = $this->getParam('mr_id');

        $this->view->userid = $userid;
        $this->view->mr_id = $mr_id;

        $this->view->order_id = 1;

        $MembershipDB = new App_Model_Membership_Membership();
        $membership = $MembershipDB->getMembershipInfoById($mr_id);
        $this->view->application = $membership;

        $memberFeeDb = new App_Model_Membership_MembershipFee();
        $this->view->renew_fee = $memberFeeDb->getRenewalFeeByMemberId($membership['mr_membershipId'], $membership['mr_expiry_date']);

        $checklistDb = new App_Model_Membership_MembershipRegistrationChecklist();
        $document_checklist = $checklistDb->getDataByType(3); //3: Renew
        $this->view->document_checklist = $document_checklist;

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($formData['mr_renew_period'] == 1) {
                $period = "+1 years";
            } else {
                $period = "+2 years";
            }

            $data['mr_expiry_date'] = date("Y-m-d H:i:s", strtotime($period, strtotime($membership['mr_expiry_date'])));
            $data['mr_status'] = 1607; //active

            $MembershipDB = new App_Model_Membership_Membership();
            $MembershipDB->updateApplication($data, $where = 'mr_id=' . $formData['mr_id']);

            Cms_Common::notify('success', 'Membership Renewal Successfull!');
            $this->redirect('/membership');
        }
    }


    public function payRenewalAction()
    {

        error_reporting(E_ERROR | E_WARNING | E_PARSE);

        //Cms_Hooks::push('user.sidebar', self::userSidebar('pay'));
        $Members = new App_Model_Membership_MemberRegistration();
        $auth = Zend_Auth::getInstance()->getIdentity();
        $userid = $this->auth->getIdentity()->id;
        $dbUser = new App_Model_User;
        $MembershipAppDB = new App_Model_Membership_MembershipApplication();

        $renewal_period = $MembershipAppDB->get_record($userid);

         
        $getMember= $dbUser->getMember($userid);
        // echo "<pre>";
       // . // print_r($getMember);
        // die();
        $this->view->getMember = $getMember;
        $getExpiry= $dbUser->getExpiry($userid);
                $this->view->getExpiry = $getExpiry;

         $MembershipDB = new App_Model_Membership_Membership();
       
        $get_membership = $MembershipDB->get_member($userid);

        $member_status = $this->definationDB->getByCode('Status');


        $this->view->application = $get_membership;
       
        
        // $get_status = $MembershipDB->get_status($get_membership['mr_status']);
        // $this->view->get_status = $get_status;
         
         $get_info      = $dbUser->getUserInfo($userid);
         
         $sub_type      = $get_info['id_member'];

         $member_type   = $get_info['id_member_subtype'];
         $degree_type   = $get_info['id_qualification'];


        
         $get_memberfee  = $dbUser->getFee($sub_type,$member_type);
        
         $renew_fee = $get_memberfee;

         $this->view->renew_fee = $renew_fee;
       

        Cms_Hooks::push('user.sidebar', self::userSidebar('pay'));

        $auth = Zend_Auth::getInstance()->getIdentity();


        $userid = $this->auth->getIdentity()->id;
        $qualification = $this->auth->getIdentity()->id_qualification;
        $member_type = $this->auth->getIdentity()->id_member;
        $member_subtype = $this->auth->getIdentity()->id_member_subtype;
    

        $mr_id = $this->getParam('mr_id');


        $this->view->userid = $userid1;
        $this->view->mr_id = $mr_id;

        $this->view->order_id = 1;
 $definationDB = new Admin_Model_DbTable_Definition();
        $yearLists = $this->definationDB->getByCode('Renewal Year');
        
        $this->view->yearLists = $yearLists;

        
         $dbciifp= new App_Model_Ciifpmb;
                $dbciifpdoc= new App_Model_Ciifpdoc;

        $MembershipDB = new App_Model_Membership_Membership();
        
       
        // $renew_fee = $MembershipDB->getFee($member_subtype,$qualification,$member_type);
       
        $this->view->renew_fee = $renew_fee;

        if (date('m') <= 10) {
            //1 year only
            $this->view->renewal_period = 1;
        } else {
            //2 years end of next year
            $this->view->renewal_period = 2;
        }
         $value = count($renewal_period);
                  
       
        if ($this->getRequest()->isPost()) {
              $member = $this->getParam('mr_renew_period');

            $formData = $this->getRequest()->getPost();

             $MembershipDB = new App_Model_Membership_Membership();

            $dbUser = new App_Model_User;
                                  $data = array('estatus'=> 592,'renew_member' =>0 );
                    
                    $dbUser->update($data, array('id = ?' =>$userid));

            $get_status = $this->definationDB->getdefCode('InActive');
           

            
            $member_reg = new App_Model_Membership_MemberRegistration();

                  

                            $upload = new Zend_File_Transfer();
                    $userid1 = $formData['member_id'];
                   // $id= $userid1['id'];
                            $membership_id= $userid;
                              $files  = $_FILES;
                                   $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
                                 $target_file1 = 'RenewalDoc'.date('YmdHis').$_FILES['file1']['name'][$i];
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH."\\".$target_file1)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
                                           } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }
           $member_payment = $this->definationDB->getdefCode('Paid');
           // $member_payment = $this->definationDB->getdefCode('Paid');
         $member_status = $this->definationDB->getByCode('Status');
$member_app = new App_Model_Membership_MembershipApplication();
  $membership= $member_app->get_renew_fee($userid);
      
     if($userid)
         {

          $invoice = new App_Model_InvoiceMain();
               
                
            $data_invioce = array('user_id' => $userid,
                                   'description' => 'Renewal Payment',
                              'amount' => $formData['renew_fee'],
                    'invoice_no' => 'CIIF'.  date('YHims').$userid,
                    'created_user' => $userid,
                    'created_date' => date("Y-m-d"),
                    'payment_status'=>$member_payment[0]['idDefinition']
                         );
                    $invoice_no = $invoice->insert($data_invioce);
                    }
            if ($formData['renewal_year'] == 1683) {
                $period = "+1 years";
                 $data['mr_expiry_date'] = date('Y')+ 1 . date('-m-d');
            } else {
                $period = "+2 years";
                $data['mr_expiry_date'] = (date('Y') + 2) . date('-m-d');
            }

            
            $renewData['sp_id'] = $this->sp_id;
            $renewData['ma_type'] = 2; //renew
            $renewData['ma_createddt'] = date('Y-m-d H:i:s');
            $renewData['ma_createdby'] = $userid;
            $renewData['ma_submit_date'] = date('Y-m-d H:i:s');
        $renewData['ma_status'] = $member_status[14]['idDefinition'];
            $renewData['ma_payment_status'] = 1;
            $renewData['mf_id'] = 1;
            $renewData['ma_renewal_amount'] = $formData['renew_fee'];
            $renewData['membershipId'] = $userid;
            $renewData['renewal_year'] = $formData['renewal_year'] ;
            $renewData['ma_application_status'] = 1;
            $renewData['ma_expiry_date'] = $data['mr_expiry_date'];
            $renewData['ma_activation_date'] = date('Y-m-d H:i:s');
            $renewData['prev_reg_date'] = $get_membership['mr_activation_date'];
            $renewData['prev_member_id'] = $getMember['id_member'];
        $renewData['prev_member_sub_id'] = $getMember['id_member_subtype'];
$renewData['prev_expiry_date'] = $get_membership['mr_expiry_date'];
    $renewData['prev_status'] = 596;
               



            $MembershipAppDB = new App_Model_Membership_MembershipApplication();
            $MembershipAppDB->addData($renewData);
             
            $renew['mr_activation_date'] = date('Y-m-d H:i:s');
            $renew['mr_expiry_date'] = $data['mr_expiry_date'];
            $renew['mr_status'] = 1641;
            
        $Members->update($renew,array('mr_id = ?' => $mr_id));
            Cms_Common::notify('success', 'Successfully applied for  membership renewal please wait for admin to approval');
            $this->redirect('/Membership/paymentproof/mr_id/'.$mr_id);
        }
    }

public function paymentAction(){
         $member_id = $this->getParam('id');
        
        $this->view->member_id =  $member_id;
         $this->_helper->layout()->setLayout('/auth/register');
         $dbUser = new App_Model_User;
        $form = new App_Form_Register();

         $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;

        $redirect = $this->getParam('r');
$programRegister = new App_Model_ProgramRegister;
  $reg_program =$programRegister->get_user_regprogram($user_id);



        if ($this->getRequest()->isPost())
        {
            Cms_Common::expire();

            $formData = $this->getRequest()->getPost();
           // echo "<pre>";
           // print_r($formData);
           // die();

       
            if ($form->isValid($formData))
            {

                //$dbPayment = new App_Model_Paymentupload;
                $dbciifp= new App_Model_Ciifpmb;
                $dbciifpdoc= new App_Model_Ciifpdoc;

        
        
          
              
                   
                                  $data = array(
                                    'payment_status' => 1648 );
                    
                    $user_id1 =  $programRegister->update($data, array('mr_id = ?' => $user_id));
                    // echo $user_id1;
                    // die();
    

                            $upload = new Zend_File_Transfer();
                 // print_r($formData);
                 // die();
                 //    $userid1 = $formData['member_id'];
                   // $id= $userid1['id'];
                            $membership_id= $user_id;
                              $files  = $_FILES;

                            // FileUpload Academic
                               $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
                                 $target_file1 = 'ProgrammePayment'.date('YmdHis').$_FILES['file1']['name'][$i];
                                 // echo UPLOAD_PATH.$target_file1;
                                 // die();
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH.$target_file1)) {



                         $userDoc = $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
                         
                                           } 
                                        else {
                                             // print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }



        if($user_id)
             {

           // $get_amount = $this->programReg->total_amount($user_id);
        $member_payment = $this->definationDB->getdefCode('Paid');

          $invoice = new App_Model_InvoiceMain();
               $get_pay =  $invoice->get_ramount($user_id);
// print_r($get_pay);
// die();
            $data_invioce = array('user_id' => $user_id,
                                   'description' => 'Programme Payment',
                              'amount' =>  (int)$get_pay['amount'],

                              'fee_type'=> 1,
                    'invoice_no' => 'CIIF'.  date('YHims').$user_id,
                    'created_user' => $user_id,
                    'created_date' => date("Y-m-d"),
                    'payment_status'=>$member_payment[0]['idDefinition']
                         );
                    $invoice_no = $invoice->insert($data_invioce);



                     $invoice_data = new App_Model_InvoiceMainDetails();
                $get_qualifiacion = $programRegister->get_qualification($user_id);

                foreach ($get_qualifiacion as  $value) {
                    # code...
              
            $data_invioce_details = array('user_id' => $user_id,
                                   'description' => 'Programme Payment',
                              'amount' => $value['program_amount'],
                              'id_program' => $value['program'],
                              'invoice_id' => $invoice_no,
                           
                         'invoice_no' => 'CIIF'.  date('YHims').$user_id,
                    'created_user' => $user_id,
                    'created_date' => date("Y-m-d"),
                    'payment_status'=>$member_payment[0]['idDefinition']
                         );
                    $invoice_main = $invoice_data->insert($data_invioce_details);
                   
                      }
                
                    }  





                            Cms_Common::notify('success', 'Payment is Successfull!');
                             $this->redirect('/Qualification');
          
                
                
            }
            
        }

        //view
        $this->view->redirect = $redirect;
        $this->view->form = $form;
    }
    public function upgradeMembershipAction()
    {
       
       $Members = new App_Model_Membership_MemberRegistration();

        $auth = Zend_Auth::getInstance()->getIdentity();
        $dbUser = new App_Model_User;

        $userid = $this->auth->getIdentity()->id;
      
        $getMember= $dbUser->getreguser($userid);

    $getSubMember= $dbUser->getSubtype($getMember['id_member']);

    $this->view->get_sub_type = $getSubMember;

       

                $this->view->getMember = $getMember;
       
        $elevationDb = new Admin_Model_DbTable_MemberElevation();  

        $elevation = $elevationDb->getelevation($getMember['id_member'],$getMember['id_member_subtype']);
        $this->view->elevation = $elevation;

         $member_status = $this->definationDB->getByCode('Status');

        
         $MembershipDB = new App_Model_Membership_Membership();
       
        $get_membership = $MembershipDB->get_member($userid);

        $this->view->application = $get_membership;
       
    Cms_Hooks::push('user.sidebar', self::userSidebar('membership'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        $userid = $this->auth->getIdentity()->id;

        $mr_id = $this->getParam('mr_id');

        $this->view->userid = $userid;
        $this->view->mr_id = $mr_id;

        $memberDB = new App_Model_Membership_TblMembership();
        $this->view->membership_list = $memberDB->getListMembership();

        $member_upgrade = new App_Model_Membership_MembershipApplication();

    
        $checklistDb = new App_Model_Membership_MembershipRegistrationChecklist();
        $document_checklist = $checklistDb->getDataByType(2); //2: Upgrade
        $this->view->document_checklist = $document_checklist;

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            


             $renew['mr_activation_date'] = date('Y-m-d H:i:s');
            $renew['mr_expiry_date'] = date('Y-m-d H:i:s', strtotime(date("Y-m-d", time()) . " + 1 year"));
            $renew['mr_status'] = 1384;
        $Members->update($renew,array('mr_id = ?' => $mr_id));


            if (isset($this->auth->getIdentity()->login_as)) {//if admin
                $formData['ma_createdby'] = $this->auth->getIdentity()->login_as;
            } else {
                $formData['ma_createdby'] = $this->auth->getIdentity()->id;
            }

            //membership application
            $formData['ma_status'] = $member_status[4]['idDefinition'];
            $formData['sp_id'] = $this->sp_id;
            $formData['ma_type'] = 1; //upgrade
        $formData['membershipId'] =  $this->auth->getIdentity()->id;          
            $formData['ma_createddt'] = date('Y-m-d H:i:s');
            $formData['ma_submit_date'] = date('Y-m-d H:i:s');
            $formData['ma_prev_mrid'] = $mr_id;
        $formData['prev_reg_date'] = $get_membership['mr_activation_date'];
        $formData['prev_expiry_date'] = $get_membership['mr_expiry_date'];
        $formData['prev_status'] = 596;
        $formData['prev_member_id'] = $getMember['id_member'];
        $formData['prev_member_sub_id'] = $getMember['id_member_subtype'];

            unset($formData['mr_prev_mrid']);
            unset($formData['Submit']);

            //add new application
            $MembershipAppDB = new App_Model_Membership_MembershipApplication();
            $dbciifpdoc= new App_Model_Ciifpdoc;
                
            $ma_id = $MembershipAppDB->addData($formData);
            $data = array('id_member' =>$formData['upgrade_id'],
                          'id_member_subtype' => $formData['upgrade_sub'],
                          
                'upgrade_member' => 1648 );
                    
                   $user= $dbUser->update($data, array('id = ?' =>$userid));
            $files  = $_FILES;
      
                              // FileUpload Academic
                               $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
                                 $target_file1 = 'UpgradeDoc'.date('YmdHis').$_FILES['file1']['name'][$i];
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH."\\".$target_file1)) {


                            $dbciifpdoc->addmembershipdocdetail($userid,$target_file1);
                                           } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }



                             for($i=0;$i<count($_FILES['file2']['name']);$i++) 
                              {
                                   if($i==0) {
                                      $k = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $k = $j;
                                   
                                   }
                                 $target_file2 = 'Upgrade'.date('YmdHis').$_FILES['file2']['name'][$k];
   if (move_uploaded_file($_FILES["file2"]["tmp_name"][$k], UPLOAD_PATH."\\".$target_file2)) {



                            $dbciifpdoc->addmembershipdocdetail($userid,$target_file2);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              } 


                              for($i=0;$i<count($_FILES['file3']['name']);$i++) 
                              {

                                if($i==0) {
                                      $l = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $l = $j;
                                   
                                   }

                                        // move upload file
                                 $target_file3 = 'UpgradeMembershipdoc'.date('YmdHis').$_FILES['file3']['name'][$l];
   if (move_uploaded_file($_FILES["file3"]["tmp_name"][$l], UPLOAD_PATH."\\".$target_file3)) {


                            $dbciifpdoc->addmembershipdocdetail($userid,$target_file3);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }

            //add documents info
            if (count($_FILES) > 0) {
                $memberRegDoc = new App_Model_Membership_MembershipRegistrationDocuments();


                foreach ($_FILES as $fieldname => $doc) {


   //                  $target_file1 = 'RenewalDoc'.date('YmdHis').$_FILES['file1']['name'][$i];
   move_uploaded_file(UPLOAD_PATH."\\".$doc['name']);

   //                          $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
   //                                         } 
   //                                      else {
   //                                           print_r(error_get_last());
   //                                          echo "Sorry, there was an error uploading your file.";
   //                                      }
                    // if ($doc['name'] != '') {
                    //     $filedata['ma_id'] = $ma_id;
                    //     $filedata['mrd_application_type'] = 2;
                    //     $filedata['mrc_id'] = substr($fieldname, 6);
                    //     $filedata['mrd_name'] = $doc['name'];
                    //     $filedata['mrd_createddt'] = date('Y-m-d H:i:s');
                    //     $filedata['mrd_createdby'] = $this->auth->getIdentity()->id;
                    //     $memberRegDoc->addData($filedata);
                    // }
                }
            }

            //email
            $info = array(
                'template_id'     => 18,
                'name'            => $membership['std_fullname'],
                'program_name'    => '',
                'membership_name' => $membership['MembershipName']
            );

             $get_status = $this->definationDB->getdefCode('InActive');
           

            
            $member_reg = new App_Model_Membership_MemberRegistration();

                   $data = array('mr_status' => $get_status[0]['idDefinition'] );
                    
        $member_reg= $member_reg->update($data, array('mr_membershipId = ?' =>$userid));
                   

            $email = new icampus_Function_Email_Email();
            $email->sendEmail($info);

            Cms_Common::notify('success', 'Membership upgrade application submitted!');
            $this->redirect('/membership');
        }
    }

    public function payUpgradeAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('pay'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        $userid = $this->auth->getIdentity()->id;
        $ma_id = $this->getParam('id');

        $this->view->userid = $userid;
        $this->view->ma_id = $ma_id;

        $paymentModeDB = new App_Model_Finance_PaymentMode();
        $this->view->paymentmodelist = $paymentModeDB->getPaymentModeList(1);

        $MembershipAppDB = new App_Model_Membership_MembershipApplication();
        $membership = $MembershipAppDB->getApplicationById($ma_id);
        $this->view->application = $membership;

        $memberFeeDb = new App_Model_Membership_MembershipFee();
        $this->view->fee = $memberFeeDb->getUpgradeFeeByMemberId($membership['m_id']);

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        } else {
            $sp = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            //1:update application completed
            $upgrade['ma_status'] = 586; //Completed

            // $upgrade['ma_status'] = 1617; //Completed
            $upgrade['ma_payment_status'] = 1;
            $upgrade['ma_activation_date'] = date('Y-m-d H:i:s');

            $MembershipAppDB = new App_Model_Membership_MembershipApplication();
            $MembershipAppDB->updateData($upgrade, $where = 'ma_id=' . $formData['ma_id']);

            //get info
            $upgradeData = $MembershipAppDB->getData($formData['ma_id']);

            //2:add current membership registration to history
            $MembershipDB = new App_Model_Membership_Membership();
            $prev_member_info = $MembershipDB->getData($formData['ma_prev_mrid']);

            
            $prev_member_info['mrh_createddt'] = date('Y-m-d H:i:s');
            $prev_member_info['mrh_createdby'] = $this->auth->getIdentity()->id;

            $regHistoryDB = new App_Model_Membership_MembershipRegistrationHistory();
            $regHistoryDB->addData($prev_member_info);

            //3:delete current 
            $MembershipDB->deleteData($formData['ma_prev_mrid']);

            //4 add new application membership registration


            $data['mr_type'] = 0;
            $data['mr_sp_id'] = $sp;
            $data['mr_activation_date'] = date('Y-m-d H:i:s');
            $data['mr_expiry_date'] = date('Y-m-d H:i:s', strtotime(date("Y-m-d", time()) . " + 1 year"));
            $data['mr_status'] = 1384; //active
            $data['mr_payment_status'] = 1; //paid
            $data['mr_approve_date'] = $upgradeData['ma_approve_date'];
            $data['mr_approve_by'] = $upgradeData['ma_approve_by'];
            $data['mr_membershipId'] = $upgradeData['membershipId'];
            $data['mr_prev_mrid'] = $upgradeData['ma_prev_mrid'];
            $MembershipDB->addData($data);

            Cms_Common::notify('success', 'Membership upgrade was successfull! Please proceed on CPD registration!');
            $this->redirect('/membership');
        }
    }

    public function getDirection()
    {

        $auth = Zend_Auth::getInstance()->getIdentity();

        $applicantTransactionDB = new App_Model_Records_ApplicantTransaction();
        $transaction = $applicantTransactionDB->getTransaction($auth->getIdentity()->id);

    }

    public function checkoutModeAction()
    {

        //Cms_Hooks::push('user.sidebar', self::userSidebar('pay'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        $hash = $this->_getParam('i', null);
        $data = Cms_Common::encrypt($hash, false);
        $data = json_decode($data, true);

        $m_id = $data['m_id'];
        $mr_id = $data['mr_id'];

        if (!isset($data['token'])) {
            return Cms_Render::error('Invalid Session. Please try again.');
        }

        if (Cms_Common::tokenCheck($data['token']) === false) {
            return Cms_Render::error('Invalid Session. Please try again.');
        }

        //generate performa
        $bill_no = 'PIN' . date('Y') . '-' . rand();//PIN2015-00116
        $invoice_desc = 'Membership - New application';

        $data = array(
            'bill_number'           => $bill_no,
            'IdStudentRegistration' => $IdStudentRegistration,
            'bill_amount'           => $total_invoice_amount,
            'bill_description'      => $invoice_desc,
            'creator'               => $creator,
            'from'                  => 1,
            'category_id'           => $category_id,
            'totalStudent'          => $totalStudent,
            'registration_item'     => isset($registrationItem) ? $registrationItem : 0,
            'batchId'               => 0,
            'fs_id'                 => $fs_id,
            'status'                => 'A',
            'currency_id'           => $currency_id,
            'invoice_type'          => 'PROCESSING',
        );
        $main_id = $proformaInvoiceMainDb->insert($data);


        //get fee amount
        $memberFeeDb = new App_Model_Membership_MembershipFee();
        $member_fee = $memberFeeDb->getFeeByMemberId($m_id);

        //create Invoice
        $gst = 0.06;
        $amountMainBeforeGst = $member_fee['mf_amount'];
        $gstAmount = $amountMainBeforeGst * $gst;
        $nettAmount = $amountMainBeforeGst + $gstAmount; //amount after discount + gst
        $nettAmountMain = $amountMainBeforeGst + $gstAmount; // amount before discount +gst
        $amountBalance = $nettAmountMain;


        $bill_no = 'IN' . date('Y') . '-' . rand();//PIN2015-00116
        $data_invoice = array(
            'bill_number'           => $bill_no,
            'corporate_id'          => '',//$proforma_invoice['corporate_id'],
            'IdStudentRegistration' => '',
            'mr_id'                 => $mr_id,
            'batchId'               => '', //$proforma_invoice['batchId'],
            'ber_id'                => '',  //$proforma_invoice['ber_id'],
            'bill_amount'           => $nettAmountMain, //nett amount
            'bill_amount_gst'       => $amountMainBeforeGst, //amount before gst
            'gst_amount'            => $gstAmount, //gst amount
            // 'dn_amount'          => $discAmount, //disc amount
            'payment_mode'          => '',
            'bill_paid'             => 0,
            'bill_balance'          => $amountBalance,
            'category_id'           => 1, //tbl-category 1:individual
            'totalStudent'          => 1,
            'bill_description'      => 'membership new application',
            'registration_item'     => '',
            'program_id'            => 0,
            'fs_id'                 => '',
            'currency_id'           => 1,
            'exchange_rate'         => isset($proforma_invoice['exchange_rate']) ? $proforma_invoice['exchange_rate'] : 0,
            'proforma_invoice_id'   => $proforma_invoice['id'],
            'from'                  => isset($from) ? $from : 0,
            'invoice_date'          => date('Y-m-d'),
            'date_create'           => date('Y-m-d H:i:s'),
            'coupon_id'             => ($coupon_id) ? $coupon_id : 0
        );

        //create order ID        
        $order = array(
            'user_id'          => $auth->getIdentity()->id,
            'created_date'     => new Zend_Db_Expr('UTC_TIMESTAMP()'),
            'created_by'       => $auth->getIdentity()->id,
            'amount'           => 60,
            'paymentmethod'    => '',
            'status'           => 'PENDING',
            'invoice_id'       => $invoiceId,
            'invoice_no'       => $invoiceNo,
            'invoice_external' => 1,
            'regtype'          => $regtype,
            'curriculum_id'    => '',
            'item_id'          => '',
            'learningmode'     => '',
            'schedule'         => '',
            'external_id'      => ''
        );

        $order_id = $this->orderDb->insert($order);
        $order['id'] = $order_id;


    }

    public function reinstatementMembershipAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('membership'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        $userid = $this->auth->getIdentity()->id;
        $mr_id = $this->getParam('mr_id');

        $this->view->userid = $userid;
        $this->view->mr_id = $mr_id;

        $memberDB = new App_Model_Membership_TblMembership();
        $this->view->membership_list = $memberDB->getListMembership();

        /* $MembershipDB = new App_Model_Membership_Membership();
         $membership = $MembershipDB->getMembershipById($mr_id);
         $this->view->application = $membership;*/

        $MembershipDB = new App_Model_Membership_Membership();
        $membership = $MembershipDB->getMembershipInfo($this->sp_id);
        $this->view->application = $membership;

        // $memberFeeDb = new App_Model_Membership_MembershipFee();
        // $this->view->renew_fee = $memberFeeDb->getUpgradeFeeByMemberId($membership['m_id']);

        $checklistDb = new App_Model_Membership_MembershipRegistrationChecklist();
        $document_checklist = $checklistDb->getDataByType(3); //3: Reinstatement
        $this->view->document_checklist = $document_checklist;

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            //pr($formData);exit;


            if (isset($this->auth->getIdentity()->login_as)) {//if admin
                $formData['ma_createdby'] = $this->auth->getIdentity()->login_as;
            } else {
                $formData['ma_createdby'] = $this->auth->getIdentity()->id;
            }

            //membership application
            $formData['ma_status'] = 1614;
            $formData['ma_type'] = 3; //reinstatement
            //$formData['ma_prev_mrid'] = $formData['ma_prev_mrid'];

           // unset($formData['mr_prev_mrid']);
            unset($formData['Submit']);

            //add new application
            $MembershipAppDB = new App_Model_Membership_MembershipApplication();
            $checkExist = $MembershipAppDB->getCheckExist($this->sp_id, '3');


            if ($checkExist) {

                $formData['ma_modifieddt'] = date('Y-m-d H:i:s');
                $formData['ma_modifiedby'] = $this->sp_id;

                $update = $MembershipAppDB->updateDataMaid($formData, $checkExist['ma_id']);
                $ma_id = $checkExist['ma_id'];

            } else {

                $formData['sp_id'] = $this->sp_id;
                $formData['ma_createddt'] = date('Y-m-d H:i:s');
                $formData['ma_submit_date'] = date('Y-m-d H:i:s');


                $ma_id = $MembershipAppDB->addData($formData);

                //email
                $info = array(
                    'template_id'     => 13,
                    'name'            => $membership['std_fullname'],
                    'program_name'    => '',
                    'membership_name' => $membership['MembershipName']
                );

                $email = new icampus_Function_Email_Email();
                $email->sendEmail($info);
            }

            //add documents info
            if (count($_FILES) > 0) {
                $memberRegDoc = new App_Model_Membership_MembershipRegistrationDocuments();
                foreach ($_FILES as $fieldname => $doc) {
                    if ($doc['name'] != '') {
                        $filedata['ma_id'] = $ma_id;
                        $filedata['mrd_application_type'] = 3;
                        $filedata['mrc_id'] = substr($fieldname, 6);
                        $filedata['mrd_name'] = $doc['name'];
                        $filedata['mrd_createddt'] = date('Y-m-d H:i:s');
                        $filedata['mrd_createdby'] = $this->auth->getIdentity()->id;
                        $memberRegDoc->addData($filedata);
                    }
                }
            }


            Cms_Common::notify('success', 'Membership reinstatement application submitted!');
            $this->redirect('/membership');
        }
    }

    public function payReinstatementAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('pay'));

        $auth = Zend_Auth::getInstance()->getIdentity();


        $userid = $this->auth->getIdentity()->id;
        $mr_id = $this->getParam('mr_id');

        //exit;

        $this->view->userid = $userid;
        $this->view->mr_id = $mr_id;

        $this->view->order_id = 1;
        $dbUser = new App_Model_User;
        $MembershipDB = new App_Model_Membership_Membership();
        $membership = $MembershipDB->getMembershipInfoById($mr_id);
        $membershipInfo = $MembershipDB->getDataByIdHistory($mr_id);
        $this->view->application = $membership;

        $memberFeeDb = new App_Model_Membership_MembershipFee();
        $renew_fee = $memberFeeDb->getRenewalFeeByMemberId($membership['mr_membershipId'], $membership['mr_expiry_date']);
        $this->view->renew_fee = $renew_fee;

        $membership['mr_expiry_date'];
        $expired_year = date('Y', strtotime( $membership['mr_expiry_date']));
        $current_year = date('Y');
        $renewal_month = date('m');
        //echo $this->sp_id;

        $sum_arrears = 0;
        for ($x = $expired_year+1; $x < $current_year; $x++) {

            $start_loop = $x . '-01-01';
            $end_loop = $x . '-12-31';
            $arrears_fee = $memberFeeDb->getReinstatementFee($membership['mr_membershipId'],2, $start_loop,$end_loop);
            $arrears_amount = $arrears_fee['mf_amount'];
            // echo "The number is: $arrears_amount<br>";
            $sum_arrears+= $arrears_amount;
            //echo $arrears_amount++;
        }
        $this->view->arrears_fee = $sum_arrears;

        if (date('m') <= 10) {
            //1 year only
            $this->view->renewal_period = 1;
        } else {
            //2 years end of next year
            $this->view->renewal_period = 2;
        }

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            $data = array('approve_status' => 0 );
                    
                    $dbUser->update($data, array('id = ?' => $userid));


                            $upload = new Zend_File_Transfer();
                    $userid1 = $formData['member_id'];
                   // $id= $userid1['id'];
                            $membership_id= $userid1;
                              $files  = $_FILES;
           //     echo $userid1;
           //          echo "<pre>";
           // print_r($files);
           // die();            // FileUpload Academic
                               $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
                                 $target_file1 = 'Doc'.date('YmdHis').$_FILES['file1']['name'][$i];
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH."\\".$target_file1)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
                                           } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }

            if ($formData['mr_renew_period'] == 1) {
                $period = "+1 years";
                $new_expiry = $data['mr_expiry_date'] = date('Y') . '-12-31';

            } else {
                $period = "+2 years";
                $new_expiry =$data['mr_expiry_date'] = (date('Y') + 1) . '-12-31';

            }

            $data['mr_activation_date'] = date('Y-m-d H:i:s');
            $data['mr_expiry_date'] = $new_expiry;
            $data['mr_status'] = 1384; //active

            $MembershipDB = new App_Model_Membership_Membership();
            $MembershipDB->updateApplication($data, $where = 'mr_id=' . $formData['mr_id']);

            //save renewal data
            $renewData['sp_id'] = $this->sp_id;
            $renewData['ma_type'] = 3; //renew
            $renewData['ma_createddt'] = date('Y-m-d H:i:s');
            $renewData['ma_createdby'] = $userid;
            $renewData['ma_submit_date'] = date('Y-m-d H:i:s');
            $renewData['ma_status'] = 1617;
            $renewData['ma_payment_status'] = 1;
            $renewData['mf_id'] = 1;
            $renewData['membershipId'] = $formData['m_id'];
            $renewData['ma_application_status'] = 1;
            $renewData['ma_expiry_date'] = $new_expiry;
            $renewData['ma_activation_date'] = date('Y-m-d H:i:s');



            $MembershipAppDB = new App_Model_Membership_MembershipApplication();
            $info = $MembershipAppDB->getDetailsInfoByTypeRow($this->sp_id, 3);
            $MembershipAppDB->updateData($renewData, $where = 'ma_id=' . $info['ma_id']);

            $membershipInfo['mrh_createddt'] = date('Y-m-d H:i:s');
            $membershipInfo['mrh_createdby'] = $userid;
            $membershipInfo['mrh_amount_fee'] = $formData['mr_renew_fee'];
            $MembershipHistoryDB = new App_Model_Membership_MembershipRegistrationHistory();
            $MembershipHistoryDB->addData($membershipInfo);

            Cms_Common::notify('success', 'Membership Reinstatement Successfull!');
            $this->redirect('/membership');
        }
    }


}

?>
