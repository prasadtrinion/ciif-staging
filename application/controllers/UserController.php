<?php
//error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
require_once('External/recaptcha/autoload.php');
header('Access-Control-Allow-Origin: *');
class UserController extends Zend_Controller_Action
{
    protected $orderDb;
    protected $uploadDir;

    public function init()
    {
       // error_reporting(E_ERROR | E_WARNING | E_NOTICE);

        $this->currDb = new App_Model_Curriculum();
        $this->orderDb = new App_Model_Order();
        $auth = Zend_Auth::getInstance();
        $this->auth = $auth;
         $this->programDb = new App_Model_ProgramCategory();

        if (!$auth->hasIdentity()) {
            $this->redirect('/index/login');
        }
        $this->programReg = new App_Model_ProgramRegister();

        Zend_Layout::getMvcInstance()->assign('nowrap', 1);
        $this->programReg = new App_Model_ProgramRegister();

        $this->uploadDir = DOCUMENT_PATH;
       
        $this->view->uploadDir = $this->uploadDir;
        $this->definationDB =new Admin_Model_DbTable_Definition();

        $this->userDb = new App_Model_User();
         



    }

    public function mainAction()
    {

        {
            $tab = $this->_getParam('tab', 'register');
            $this->view->tab = $tab;

            Cms_Hooks::push('user.sidebar', self::userSidebar('dashboard'));

            $enrolDb = new App_Model_Enrol();
            $auth = Zend_Auth::getInstance()->getIdentity();
            $smsExamDB = new App_Model_SmsExam();
            $ExamregDB = new App_Model_Exam_ExamRegistration();
            $currDb = new App_Model_Curriculum();
            $pepDB = new App_Model_Exam_ProgrammeExamPrerequisite();
            $erDB = new App_Model_Exam_ExamRegistration();
            $coursesDb = new App_Model_Courses();
            $userGroupDb = new App_Model_UserGroup();
            $userGroupDataDb = new App_Model_UserGroupData();

            $welcome = isset($auth->new) && $auth->new == 1 ? 1 : 0;

            $myprograms = $enrolDb->getEnrolledProgramme($auth->id, false);
            $myexams = array();
            $learningMode_ = array("OL" => "Online", "FTF" => "Face to Face");
            $user_id = $auth->id;
            $totalenrolledprogram = count($myprograms);
            $totalenrolled = 0;
            $a = 0;

            if ($myprograms) {
                foreach ($myprograms as $a => $mypg) {

                    $curriculumId = $mypg['id'];
                    $IdProgram = $mypg['external_id'];
                    $learningmode = ($mypg['learningmode'] == 'OL') ? 577 : 578;

                    $mycourses = $enrolDb->getEnrolledCourses($auth->id, $curriculumId, false);
                    $totalenrolled += count($mycourses);
                    $myprograms[$a]['courses'] = $mycourses;

                    foreach ($myprograms[$a]['courses'] as $course) {
                        $learningmode = $course['learningmode'];
                        $group_ = $userGroupDb->getGroupByName($course['code'] . ' - ' . $learningMode_[$learningmode]);

                        if (isset($group_) && !empty($group_)) {
                            $dataGroup = $userGroupDb->getDataByUserAndGroup($user_id, $group_['group_id']);

                            if (!$dataGroup) {
                                $dataGroup = array(
                                    'group_id'     => $group_['group_id'],
                                    'user_id'      => $user_id,
                                    'active'       => 1,
                                    'created_date' => new Zend_Db_Expr('NOW()'),
                                    'created_by'   => 1
                                );
                                //$groupData = $userGroupDataDb->insertGroupData($dataGroup);
                            }
                        }
                    }

                    $examSetup = $smsExamDB->getExamByProgram($IdProgram, $learningmode);

                    if ($examSetup) {
                        $myprograms[$a]['exam'] = $examSetup;
                    }
                }
            }


            // enrolled
            $a = $a + 1;

            $myprograms[$a]['courses'] = $total = $enrolDb->getEnrolledCourses($auth->id, 0, false);
            $totalenrolled += @count($total);

            // completed
            $c = 0;

            $completed[$c]['courses'] = $totalcomplete = $enrolDb->getCompletedCourses($auth->id, 0);
            $totalcompleted = @count($totalcomplete);

            //exam
            $sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

            if ($this->auth->getIdentity()->login_as) {
                $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
            } else {
                $sp = isset($auth->external_id) ? $auth->external_id : 0;
            }
            
            if ($sp) {
                // login admin
                /*if ($auth->role =='administrator' || empty($sp)) {
                    $profile = $sp;
                } else {
                    $profile = $erDB->getProfile($sp);
                }

                $sp = $profile['sp_id'];*/

                if ($auth->role == 'administrator') {

                } else {
                    $totalexam = $ExamregDB->getexamReg($sp);
                    $totalexam = @count($totalexam);
                    $myexams = $ExamregDB->getexamReg($sp);

                    foreach ($myexams as $n => $ex) {
                        $myexams[$n]['examStatus'] = $ExamregDB->courseStatus($ex['er_registration_type']);

                    }
                }
            } else {
                $totalexam = 0;
                $myexams = array();
            }

            //orders pending
            $param = array('user_id = ?' => $auth->id, 'status = ?' => 'PENDING');
            $pendingorders = $this->orderDb->getOrders($param);

            $userDb = new App_Model_User();

            $user = $userDb->getUser($auth->id);

            //get all available exam setup
            $examList = $smsExamDB->getAvailableExam();

            $arrayExamList = array();

            //get exam fee
            foreach ($examList as $a => $exam) {

                $programmeExamId = explode(',', $exam['pe_id']);

                $examParam = array(
                    // 'IdMode'=> 578, //by default FTF
                    'IdProgram'       => $exam['IdProgram'],
                    'IdProgramScheme' => 0,
                    'Category'        => ($auth->nationality == 'MY') ? 579 : 580,
                    'ExamOption'      => json_encode($programmeExamId),
                    'view'            => 1
                );

                $get = Cms_Curl::__(SMS_API . '/get/Finance/do/getAmountExam', $examParam);

                $examList[$a]['amount'] = '-';
                if (isset($get['totalFee'])) {
                    $examList[$a]['amount'] = new Zend_Currency(array('value' => $get['totalFee'], 'symbol' => $get['fee']['cur_code']));
                    //get lms id
                    $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
                    $examList[$a]['external_id'] = $curriculumData['id'];

                    $arrayExamList[$a] = $examList[$a];

                }

            }


            // populate registered exams in an array
            $registeredExam = array();
            if (is_array($myexams)) {
                foreach ($myexams as $exam) {
                    if (!in_array($exam['er_registration_type'], array(0, 1, 4, 9, 10))) {
                        continue;
                    }
                    $registeredExam[] = $exam['IdProgram'];
                }
            }

            //views
            $this->view->registeredExam = $registeredExam;

            $this->view->user = $user;
            $this->view->pendingorders = $pendingorders;
            $this->view->welcome = $welcome;

            $this->view->totalenrolled = $totalenrolled;
            $this->view->myprograms = $myprograms;
            $this->view->myexams = @$myexams;

            $this->view->totalcompleted = $totalcompleted;
            $this->view->totalexam = @$totalexam;
            $this->view->completed = $completed;

            $this->view->exam = $arrayExamList;

        }


    }

    public function dashboardAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('dashboard'));

        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;


        $dbUser = new App_Model_User;
        $get_info = $dbUser->getUserInfo($user_id);
        $this->view->get_info = $get_info;

         $barredMember = new Admin_Model_DbTable_MemberBarring();

         if($get_info['registered_flag'] != '1'){
                $this->redirect('/user/profile');
         }

         $get_barred = $barredMember->getBarMem($user_id);
    if($get_info['payment_status'] == 0 && $get_info['approve_status'] == 0 && $get_info['approve_status1'] == 1)
             {
                $this->redirect('/user/paymentproof/id/'.$user_id);
             }elseif ($get_info['barring_status']==1) {
                 $this->redirect('/user/barringpayment/id/'.$user_id);
             }

             $MembershipDB = new App_Model_Membership_Membership();
        $get_member = $MembershipDB->get_member($user_id);
  // if($get_member['mr_status'] == 'InActive' || date('Y-m-d', strtotime($get_member['mr_expiry_date'])) > date('Y-m-d') )
                if($get_member['mr_status'] == 'InActive' )
                {

                 $this->redirect('/membership');
                }

        $getMember= $dbUser->getMember($user_id);
        $this->view->getMember = $getMember;
$userDocDb = new App_Model_UserDocuments();
                     $get_doc_status = $userDocDb->get_doc_status($user_id);

                     $this->view->doc_status = $get_doc_status;

//             if($get_doc_statuss)
//              {
//              $newDocArray = [];
//              $newDocArray1 = [];

//              for($i=0;$i<count($get_doc_statuss);$i++) {
//                  switch ($get_doc_statuss[$i]['doc_type']) {
//                      case '1':
//                      $newDocArray['1'] = $get_doc_statuss[$i];
//                          break;
//                      case '2':
//                      $newDocArray['2'] = $get_doc_statuss[$i];
//                          break;
//                      case '3':
//                      $newDocArray['3'] = $get_doc_statuss[$i];
//                          break;
//                      case '4':
//                      $newDocArray['4'] = $get_doc_statuss[$i];
//                          break;
//                      case '5':
//                      $newDocArray['5'] = $get_doc_statuss[$i];
//                          break;
                        
//                      default:
                      
//                          break;
//                  }
//              }
             
//              $get_doc_status = $newDocArray;

//              if($get_doc_status['1']['status'] == 1668 || $get_doc_status['2']['status'] == 1668 || $get_doc_status['3']['status'] == 1668 || $get_doc_status['4']['status'] == 1668 || $get_doc_status['5']['status'] == 1668 )
//              {
//                     $this->redirect('/user/uploaddocument');

//              }
             

//                 
            
// }

         $getMemberDetails= $dbUser->getMemberDetails($user_id);
         if($getMemberDetails['mr_expiry_date'] < date("Y-m-d H:i:s"))
         {
                $memberReg =new App_Model_Membership_MemberRegistration();
                $userdata = array('mr_status' =>1641 );
    $user_id1 =  $memberReg->update($userdata, array('mr_membershipId = ?' => $user_id));

     }
        $this->view->getMemberDetails = $getMemberDetails;

       

       $qualify = $get_info['id_qualification'];

       $MembershipDB = new App_Model_Membership_Membership();
        $user_id       = $auth->id;
        $this->view->user_id = $user_id;
        
        $get_membership = $MembershipDB->get_member($user_id);
       
        $qualification = $auth->id_qualification;
        
         $get_qualifiacion = $this->programReg->get_qualification($user_id);
         $get_exem_qualifiacion = $this->programReg->get_exemption_qualification($user_id);
        
         if(!empty($get_exem_qualifiacion))
         {
             $this->view->get_qualifiacion = $get_exem_qualifiacion;
         }else{
            
             $this->view->get_qualifiacion = $get_qualifiacion;

         }
        
        $this->view->get_qualifiacion = $get_qualifiacion;
 


        $this->view->get_event = $MembershipDB->get_event();

        
        $announcements = $this->getannouncement();
        $this->view->announcements = $announcements;

        

        $get_module = $this->programReg->modules_type($user_id);
        
       
        $this->view->get_module = $get_module;
        

        $event = $this->getcalendar();
        $this->view->event = $event;

        $user_id = $auth->id;
        $this->view->user_id = $user_id;

        $sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }

        if ($sp) {
            //get membership info
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);
            $this->view->membership = $membership;

            //get qualification info
            $applicationDb = new App_Model_SmsApplication();
            $currDb = new App_Model_Curriculum();
            $smsStudentRegistrationDB = new App_Model_StudentRegistration();

            $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
            $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
            if ($myApplication) {
                foreach ($myApplication as $index => $app) {
                    $smsprogramid = $app["IdProgram"];
                    if ($smsprogramid) {
                        $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                        $myApplication[$index]['cid'] = $curiculum['id'];

                        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjects($sp,$smsprogramid);
                        $myApplication[$index]['modules'] = $StudentSubjectsDetail;
                    }
                }
            }
            $this->view->myApplication = $myApplication;
            //pr($myApplication);

            //enrolment matrix
            $matrix = array(
                'active_flag'                => true,
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );


            //check active
            if (isset($membership[0]["mr_status_id"]) && $membership[0]["mr_status_id"] != 1607) { //active
                $matrix['active_flag'] = false;
            } else {
                if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                }
            }

            //check expired
            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired
                    $matrix['expired_flag'] = true;
                }
            }
            // pr($matrix);
            $this->view->matrix = $matrix;

        }//end sp


    }


    public function renewdashboardAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('dashboard'));

        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;


        $dbUser = new App_Model_User;
        $MembershipDB = new App_Model_Membership_Membership();

        $get_info = $dbUser->getUserInfo($user_id);
        $this->view->get_info = $get_info;
$get_membership = $MembershipDB->get_member($user_id);

        $this->view->application = $get_membership;

    if($get_info['payment_status'] == 0)
             {
                $this->redirect('/user/paymentproof/id/'.$user_id);
             }
        $getMember= $dbUser->getMember($user_id);
        $this->view->getMember = $getMember;

         $getMemberDetails= $dbUser->getMemberDetails($user_id);
         if($getMemberDetails['mr_expiry_date'] < date("Y-m-d H:i:s"))
         {
                $memberReg =new App_Model_Membership_MemberRegistration();
                $userdata = array(
                                    'mr_status' =>1641 );
                    
                    $user_id1 =  $memberReg->update($userdata, array('mr_membershipId = ?' => $user_id));
     }
        $this->view->getMemberDetails = $getMemberDetails;

       

       $qualify = $get_info['id_qualification'];

       $MembershipDB = new App_Model_Membership_Membership();
        $user_id       = $auth->id;
        $this->view->user_id = $user_id;
        
        $get_membership = $MembershipDB->get_member($user_id);
        // print_r($get_membership);
        // die();

        $qualification = $auth->id_qualification;
        
         $get_qualifiacion = $this->programReg->get_qualification($user_id);
        // echo '<pre>';
        // print_r($get_qualifiacion);
        // die();
        $this->view->get_qualifiacion = $get_qualifiacion;
 


        $this->view->get_event = $MembershipDB->get_event();

        // $get_status = $MembershipDB->get_status($get_membership['mr_status']);

        // $this->view->get_status = $get_status;
    
        
        $announcements = $this->getannouncement();
        $this->view->announcements = $announcements;

        $event = $this->getcalendar();
        $this->view->event = $event;

        $user_id = $auth->id;
        $this->view->user_id = $user_id;

        $sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }

        if ($sp) {
            //get membership info
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);
            $this->view->membership = $membership;

            //get qualification info
            $applicationDb = new App_Model_SmsApplication();
            $currDb = new App_Model_Curriculum();
            $smsStudentRegistrationDB = new App_Model_StudentRegistration();

            $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
            $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
            if ($myApplication) {
                foreach ($myApplication as $index => $app) {
                    $smsprogramid = $app["IdProgram"];
                    if ($smsprogramid) {
                        $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                        $myApplication[$index]['cid'] = $curiculum['id'];

                        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjects($sp,$smsprogramid);
                        $myApplication[$index]['modules'] = $StudentSubjectsDetail;
                    }
                }
            }
            $this->view->myApplication = $myApplication;
            //pr($myApplication);

            //enrolment matrix
            $matrix = array(
                'active_flag'                => true,
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );


            //check active
            if (isset($membership[0]["mr_status_id"]) && $membership[0]["mr_status_id"] != 1607) { //active
                $matrix['active_flag'] = false;
            } else {
                if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                }
            }

            //check expired
            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired
                    $matrix['expired_flag'] = true;
                }
            }
            // pr($matrix);
            $this->view->matrix = $matrix;

        }//end sp


    }


    public function paymentproofAction()
    {
       // Cms_Hooks::push('user.sidebar', self::userSidebar('dashboard'));

        $auth = Zend_Auth::getInstance()->getIdentity();

       $applicant_fee = new Admin_Model_DbTable_ApplicationFee();

       

        $user_id = $auth->id;
        $this->view->user_id = $user_id;
        $member_id = $this->getParam('id');
        
        $this->view->member_id =  $member_id;

        $sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

        $user = $this->userDb->fetchRow(array('id = ?' => $user_id))->toArray();
            $this->view->user = $user;

        if($user['nationality'] = 'MY')
        {
           
            $fee = $applicant_fee->local();
            $rate = 1;
            $this->view->rate = $rate;
            $this->view->fee = $fee;
        }else
        {

            $feeModel =  new App_Model_MemberFeeStructure();
                $rate = $feeModel->getCurrencyRate();

                 $this->view->rate = $rate['cr_exchange_rate'];
            $fee = $applicant_fee->international();
                  $this->view->fee = $fee;
        }

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }

        if ($sp) {
            //get membership info
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);
            $this->view->membership = $membership;

            //get qualification info
            $applicationDb = new App_Model_SmsApplication();
            $currDb = new App_Model_Curriculum();
            $smsStudentRegistrationDB = new App_Model_StudentRegistration();

            $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
            $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
            if ($myApplication) {
                foreach ($myApplication as $index => $app) {
                    $smsprogramid = $app["IdProgram"];
                    if ($smsprogramid) {
                        $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                        $myApplication[$index]['cid'] = $curiculum['id'];

                        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjects($sp,$smsprogramid);
                        $myApplication[$index]['modules'] = $StudentSubjectsDetail;
                    }
                }
            }
            $this->view->myApplication = $myApplication;
            //pr($myApplication);

            //enrolment matrix
            $matrix = array(
                'active_flag'                => true,
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );


            //check active
            if (isset($membership[0]["mr_status_id"]) && $membership[0]["mr_status_id"] != 1607) { //active
                $matrix['active_flag'] = false;
            } else {
                if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                }
            }

            //check expired
            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired
                    $matrix['expired_flag'] = true;
                }
            }
         
            $this->view->matrix = $matrix;

        }

    }
    public function userpaymentAction(){
         $member_id = $this->getParam('id');
        
        $this->view->member_id =  $member_id;
         $this->_helper->layout()->setLayout('/auth/register');
         $dbUser = new App_Model_User;
         $User_register = new App_Model_User;
        $form = new App_Form_Register();

        $redirect = $this->getParam('r');

         $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;

        $get_payment =  $dbUser->getUserInfo($user_id);

      

        if ($this->getRequest()->isPost())
        {
            Cms_Common::expire();

            $formData = $this->getRequest()->getPost();
           // echo "<pre>";
           // print_r($formData);
           // die();

           
            if ($form->isValid($formData))
            {

                //$dbPayment = new App_Model_Paymentupload;
                $dbciifp= new App_Model_Ciifpmb;
                $dbciifpdoc= new App_Model_Ciifpdoc;

         $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;
        
                
                   
                                  $data = array('estatus'=> 592,
                                    'total_amount' => $formData['total_amount'],
                                    'payment_status' => 2 );
                    
                    $user_id1 =  $dbUser->update($data, array('id = ?' => $user_id));

             //        $data = array('payment_status' =>1657 );
                    
             // $member_register = $User_register->update($data, array('mr_id = ?' => $user_id));
    

                            $upload = new Zend_File_Transfer();
                   // $userid1 = $formData['member_id'];
                   // $id= $userid1['id'];
                            $membership_id= $user_id;
                              $files  = $_FILES;

                            // FileUpload Academic
                               $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
                                 $target_file1 = 'Payment'.$_FILES['file1']['name'][$i];
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH.$target_file1)) {



                         $userDoc = $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
                         
                                           } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }
 $feeModel =  new App_Model_MemberFeeStructure();

$result = $feeModel->getTotalamount($get_payment['id_member'],$get_payment['id_member_subtype'] ,$get_payment['nationality']);



      
                              
$member_payment = $this->definationDB->getdefCode('Paid');
          
     if($user_id)
         {
          $invoice = new App_Model_InvoiceMain();
               
                
            $data_invioce = array('user_id' => $user_id,
                                   'description' => 'Registration Payment',
                                   'fee_type' => 0,
                              'amount' => $get_payment['total_amount'],
                    'invoice_no' => 'CIIF'.  date('YHims').$user_id,
                    'created_user' => $user_id,
                    'created_date' => date("Y-m-d"),
                    'payment_status'=>$member_payment[0]['idDefinition']
                         );
                    $invoice_no = $invoice->insert($data_invioce);
                  

        $invoice_data = new App_Model_InvoiceMainDetails();
               
                
            $data_invioce_details = array('user_id' => $user_id,
                                   'description' => 'Registration Payment',
                              'amount' => $get_payment['total_amount'],
                              'invoice_id' => $invoice_no,
                            'id_program' => $result['id'],  
                         'invoice_no' => 'CIIF'.  date('YHims').$user_id,
                    'created_user' => $user_id,
                    'created_date' => date("Y-m-d"),
                    'payment_status'=>$member_payment[0]['idDefinition']
                         );
                    $invoice_no = $invoice_data->insert($data_invioce_details);

                    }    

//Cms_Common::notify('success', 'Payment is successful please wait for CIIF approval');
                            
                            $this->redirect('/login');
          
                
                
            }
            
        }

        //view
        $this->view->redirect = $redirect;
        $this->view->form = $form;
    }


    public function barringpaymentAction(){
         $auth = Zend_Auth::getInstance()->getIdentity();

       

        $user_id = $auth->id;
        $this->view->user_id = $user_id;
        $member_id = $this->getParam('id');
        
        $this->view->member_id =  $member_id;
        $this->view->auth = $this->auth->getIdentity();
            $this->view->matrix = $matrix;

    }

     public function paybarredAction(){
         $member_id = $this->getParam('id');
        
        $this->view->member_id =  $member_id;
         $this->_helper->layout()->setLayout('/auth/register');
         $dbUser = new App_Model_User;
         $User_register = new App_Model_User;
        $form = new App_Form_Register();

        $redirect = $this->getParam('r');

         $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;


        $get_payment =  $dbUser->getUserInfo($user_id);


        $barredMember = new Admin_Model_DbTable_MemberBarring();

         $get_barred = $barredMember->getBarMem($user_id);
        
         $member_payment = $this->definationDB->getdefCode('Paid');

          $get_barred = $barredMember->getBarMem($user_id);

        if ($this->getRequest()->isPost())
        {
            Cms_Common::expire();

            $formData = $this->getRequest()->getPost();
           
         

            if ($form->isValid($formData))
            {

                //$dbPayment = new App_Model_Paymentupload;
                $dbciifp= new App_Model_Ciifpmb;
                $dbciifpdoc= new App_Model_Ciifpdoc;

         $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;
        
                
                   
        $data = array('barring_status' => 1   );


                    
                    $user_id1 =  $dbUser->update($data, array('id = ?' => $user_id));



                    $data = array( 'payment_status'=>$member_payment[0]['idDefinition'] );
        $update_member = $barredMember->update($data,array('bmem_id= ?' => $user_id));


                            $upload = new Zend_File_Transfer();
                       $membership_id= $user_id;
                              $files  = $_FILES;

                            
                               $j=0;
                  for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                
                                $j= $j+$i;                 
$target_file1 = 'Barring'.$_FILES['file1']['name'][$i];
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH."\\".$target_file1)) {

$userDoc = $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
                         
 } 
    else {
         print_r(error_get_last());
     echo "Sorry, there was an error uploading your file.";
                     }

                              }

                              
$member_payment = $this->definationDB->getdefCode('Paid');      
     if($user_id)
         {
          $invoice = new App_Model_InvoiceMain();


            
            $data_invioce = array('user_id' => $user_id,
                                   'description' => 'Barring Payment',
                     'amount' => $get_barred[0]['amount'],
                    'invoice_no' => 'CIIF'.  date('YHims').$user_id,
                    'created_user' => $user_id,
                    'created_date' => date("Y-m-d"),
                    'payment_status'=>$member_payment[0]['idDefinition']
                         );
                    $invoice_no = $invoice->insert($data_invioce);
                    } 

Cms_Common::notify('success', 'Payment is successful please wait for CIIF approval');
                            
                            $this->redirect('/login');
          
                
                
            }
            
        }

        //view
        $this->view->redirect = $redirect;
        $this->view->form = $form;
    }

    public function dashboardOldAction()
    {

        /*$email = new icampus_Function_Email_Email();

        $info = array(
            'template_id' => 1,
            'name' => 'name',
            'program_name' => 'name',
            'membership_name' => 'name',
            'title' => 'title'
        );

        $email->sendEmail($info);*/

        //error_reporting(E_ALL);
        //ini_set('display_errors', '1');
        $tab = $this->_getParam('tab', 'application');
        $this->view->tab = $tab;

        Cms_Hooks::push('user.sidebar', self::userSidebar('dashboard'));

        $enrolDb = new App_Model_Enrol();
        $auth = Zend_Auth::getInstance()->getIdentity();

        $smsExamDB = new App_Model_SmsExam();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $currDb = new App_Model_Curriculum();
        $pepDB = new App_Model_Exam_ProgrammeExamPrerequisite();
        $erDB = new App_Model_Exam_ExamRegistration();
        $coursesDb = new App_Model_Courses();
        $userGroupDb = new App_Model_UserGroup();
        $userGroupDataDb = new App_Model_UserGroupData();
        $online = new App_Model_UserOnline();
        $smsExtendTimeDB = new App_Model_ExtendTime();

        $count = $online->getTotalUsers();//pr($count);
        $this->view->totalusers = $count;

        $welcome = isset($auth->new) && $auth->new == 1 ? 1 : 0;

        $myprograms = $enrolDb->getEnrolledProgramme($auth->id, false);
        $myexams = array();
        $learningMode_ = array("OL" => "Online", "FTF" => "Face to Face");
        $user_id = $auth->id;

        $totalenrolledprogram = count($myprograms);

        $totalenrolled = 0;
        $a = 0;

        if ($myprograms) {

            foreach ($myprograms as $a => $mypg) {

                $curriculumId = $mypg['id'];
                $IdProgram = $mypg['external_id'];
                $learningmode = ($mypg['learningmode'] == 'OL') ? 577 : 578;

                $mycourses = $enrolDb->getEnrolledCourses($auth->id, $curriculumId, false);
                $totalenrolled += count($mycourses);
                $myprograms[$a]['courses'] = $mycourses;

                foreach ($myprograms[$a]['courses'] as $course) {

                    $learningmode = $course['learningmode'];
                    $group_ = $userGroupDb->getGroupByName($course['code'] . ' - ' . $learningMode_[$learningmode]);

                    if (isset($group_) && !empty($group_)) {
                        $dataGroup = $userGroupDb->getDataByUserAndGroup($user_id, $group_['group_id']);

                        if (!$dataGroup) {
                            $dataGroup = array(
                                'group_id'     => $group_['group_id'],
                                'user_id'      => $user_id,
                                'active'       => 1,
                                'created_date' => new Zend_Db_Expr('NOW()'),
                                'created_by'   => 1
                            );
                            //$groupData = $userGroupDataDb->insertGroupData($dataGroup);
                        }
                    }
                }

                $examSetup = $smsExamDB->getExamByProgram($IdProgram, $learningmode);
                if ($examSetup) {
                    $myprograms[$a]['exam'] = $examSetup;
                }
            }
        }


        // enrolled
        $a = $a + 1;

        $myprograms[$a]['courses'] = $total = $enrolDb->getEnrolledCourses($auth->id, 0, false);
        $totalenrolled += @count($total);

        // completed
        $c = 0;

        $completed[$c]['courses'] = $totalcomplete = $enrolDb->getCompletedCourses($auth->id, 0);
        $totalcompleted = @count($totalcomplete);

        //exam
        $sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }

        if ($sp) {
            // login admin
            /*if ($auth->role =='administrator' || empty($sp)) {
                $profile = $sp;
            } else {
                $profile = $erDB->getProfile($sp);
            }

            $sp = $profile['sp_id'];*/

            //get membership info
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);
            $this->view->membership = $membership;

            //get active membership
            $active_membership = $MembershipDB->getActiveMembership($sp);

            //get application
            $applicantTransactionDB = new App_Model_Records_ApplicantTransaction();
            $transaction = $applicantTransactionDB->getData($sp);
            $this->view->transaction = $transaction;


            if (isset($transaction[0]['IdProgram']) && $transaction[0]['IdProgram'] != '') {
                $curiculumDb = new App_Model_Curriculum();
                $this->view->micuriculum = $curiculumDb->getDataByExternal($transaction[0]['IdProgram']);
            }

            //enrolment matrix
            $matrix = array(
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );

            $this->view->expired_flag = false;
            $this->view->qualification_pay = true;
            $this->view->qualification_register = true;

            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired

                    $this->view->expired_flag = true;
                    $matrix['expired_flag'] = true;
                }
            }

            if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                //require membership
                if ($transaction[0]['mr_status'] != 1604 && $transaction[0]['mr_status'] != 1607) { //approve & active
                    $this->view->qualification_pay = false;
                    $this->view->qualification_register = false;
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                    $matrix['qualification_pay_msg'] = "Membership is not active.";
                    $matrix['qualification_register_msg'] = "Membership is not active.";
                }
            } else {
                if ($transaction[0]['registration_type'] == "Qualification Only")
                    $matrix['qualification_register'] = false;
                $matrix['qualification_pay'] = false;
            }

            $this->view->matrix = $matrix;
          
            $studentRegDb = new App_Model_Qualification_StudentRegistration();
            $this->view->qualification_registration = $studentRegDb->getDatabyStudentId($sp);


            if ($auth->role == 'administrator') {

            } else {


                $totalexam = $ExamregDB->getexamReg($sp);
                $totalexam = @count($totalexam);
                $myexams = $ExamregDB->getexamReg($sp);

                foreach ($myexams as $n => $ex) {
                    $myexams[$n]['examStatus'] = $ExamregDB->courseStatus($ex['er_registration_type']);
                }


                /*if($myApplication[0]["IdRegistrationType"] != 2){
                    foreach ($myApplication as $xp=>$appl) {
                        $memberDetl = $MembershipDB->getDataById("", $registrationId);
                        $myApplication[$xp]["membershipDetl"] = $memberDetl;

                        $memberProgramDetl = $MembershipDB->getMemberProgram($memberDetl['mr_idProgram']);
                        $myApplication[$xp]["memberProgramDetl"] = $memberProgramDetl;

                        $memberProgramDetl["IdProgram"];
                        $feeProgramDetl = $MembershipDB->getFeeProgram($memberProgramDetl["IdProgram"]);
                        $myApplication[$xp]["feeProgramDetl"] = $feeProgramDetl;

                        $totalmemberapplication = $memberDetl["totalmemberapp"];
                    }
                }*/

                $applicationDb = new App_Model_SmsApplication();
                $currDb = new App_Model_Curriculum();

                $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
                $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
                if ($myApplication) {
                    foreach ($myApplication as $index => $app) {
                        $smsprogramid = $app["IdProgram"];
                        if ($smsprogramid) {
                            $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                            $myApplication[$index]['cid'] = $curiculum['id'];
                        }
                    }
                }
                $totalapplication = count($myApplication);
            }


            // CPD DETAILS
            $applicationDb = new App_Model_SmsApplication();

        } else {
            $totalexam = 0;
            $totalapplication = 0;
            $myexams = array();
            $myApplication = array();
            $student_category_list = array();
        }
        //$this->view->student_category_list = $student_category_list;

        //orders pending
        $param = array('user_id = ?' => $auth->id, 'status = ?' => 'PENDING');
        $pendingorders = $this->orderDb->getOrders($param);

        $userDb = new App_Model_User();

        $user = $userDb->getUser($auth->id);

        //get all available exam setup
        //$examList = $smsExamDB->getAvailableExam();
        $examList = $smsExamDB->getAvailableExamByUser($sp);
        //$examList = $smsExamDB->getExamList($auth->sp_id);

        $smsStudentRegistrationDB = new App_Model_StudentRegistration();
        $studentRegDetl = $smsStudentRegistrationDB->getDataStudentRegistration($sp);
        $this->view->studentRegDetl = $studentRegDetl;

        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjectsDetail($sp);
        $this->view->StudentSubjectsDetail = $StudentSubjectsDetail;

        $studentExtendDetails = $smsExtendTimeDB->getDataStudReg($studentRegDetl["IdStudentRegistration"]);
        $this->view->studentExtendDetails = $studentExtendDetails;

        $arrayExamList = array();

        //get exam fee
        foreach ($examList as $a => $exam) {

            /*$programmeExamId = explode(',',  $exam['pe_id']);

            $examParam = array(
               // 'IdMode'=> 578, //by default FTF
                'IdProgram'=>$exam['IdProgram'],
                'IdProgramScheme'=>0,
                'Category'=>($auth->nationality == 'MY') ? 579 : 580,
                'ExamOption' => json_encode($programmeExamId),
                'view' => 1
            );

            $get = Cms_Curl::__(SMS_API.'/get/Finance/do/getAmountExam',$examParam);

            $examList[$a]['amount'] = '-';
            if(isset($get['totalFee'])) {
                $examList[$a]['amount'] = new Zend_Currency(array('value' => $get['totalFee'], 'symbol' => $get['fee']['cur_code']));
                //get lms id
                $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
                $examList[$a]['external_id'] = $curriculumData['id'];

                $arrayExamList[$a] = $examList[$a];

            }*/

            $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
            $examList[$a]['external_id'] = $curriculumData['id'];

            $arrayExamList[$a] = $examList[$a];

        }


        //get all qualification application
        /*prerequisites
        if($IdStudentRegistration) {

        $pass_prerequisite = true;
        foreach($prerequisites as $n => $pep) {
            $result = $pepDB->checkPrerequisite($pep['pep_id'], $student_profile['std_id'], $IdStudentRegistration);

            if(!$result['result']) {
                $pass_prerequisite = false;
            }
        }

        // $pass_prerequisite = true;

if(!$pass_prerequisite) {
    continue;
}*/

        // populate registered exams in an array
        $registeredExam = array();
        if (is_array($myexams)) {
            foreach ($myexams as $exam) {
                if (!in_array($exam['er_registration_type'], array(0, 1, 4, 9, 10))) {
                    continue;
                }
                $registeredExam[] = $exam['IdProgram'];
            }
        }

//        // CPD DETAILS
        $applicationDb = new App_Model_SmsApplication();
//
        if ($auth->external_id) {
            $cpd = $applicationDb->getCpdDetailsByStudentId($auth->external_id);
            $totalcpd = @count($cpd);

            $totalcpdhours = $applicationDb->countCpdDurationByStudentId($auth->external_id);
        }
//
        $student_category_list = $applicationDb->fnGetDefinations('CPD Learning');
        $this->view->student_category_list = $student_category_list;

        //views
        $this->view->registeredExam = $registeredExam;
        $this->view->user_id = $user_id;//echo $user_id."+++";
        $this->view->user = $user;
        $this->view->pendingorders = $pendingorders;
        $this->view->welcome = $welcome;

        $this->view->totalenrolled = $totalenrolled;
        $this->view->myprograms = $myprograms;
        $this->view->myexams = @$myexams;

        $this->view->totalcompleted = $totalcompleted;
        $this->view->totalexam = @$totalexam;
        $this->view->completed = $completed;
        $this->view->totalapplication = $totalapplication;
        $this->view->totalcpd = @$totalcpd;
        $this->view->totalcpdhours = $totalcpdhours;
        $this->view->external_id = $auth->external_id;

        //$this->view->totalmemberapplication = $totalmemberapplication;
        $this->view->exam = $arrayExamList;
        $this->view->myApplication = $myApplication;

        $userDb = new App_Model_User();

        $user = $userDb->getUser($this->auth->getIdentity()->id);

        $form = new App_Form_User();
        $form->populate($user);

        $userDocDb = new App_Model_UserDoc();
        $userDocuments = $userDocDb->getUserDoc($user['id']);

        $this->view->documents = $userDocuments;

        //unset
        $form->email->setRequired(false);
        $form->username->setRequired(false);
        $form->gender->setRequired(false);
        $form->estatus->setRequired(false);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($this->auth->getIdentity()->login_as) {//if admin
                $modifiedby = $this->auth->getIdentity()->login_as;
                $modifiedrole = "superadmin";
            } else {
                $modifiedby = $this->auth->getIdentity()->id;
                $modifiedrole = $this->auth->getIdentity()->role;
            }
            if ($form->isValid($formData)) {
                //update
                $data = array(
                    'firstname'    => $formData['firstname'],
                    'lastname'     => $formData['lastname'],
                    'contactno'    => $formData['contactno'],
                    'estatus'      => (isset($formData['estatus']) ? $formData['estatus'] : null),
//                    'modifiedby' => $this->auth->getIdentity()->id,
                    'modifiedby'   => $modifiedby,
//                    'modifiedrole' => $this->auth->getIdentity()->role,
                    'modifiedrole' => $modifiedrole,
                    'modifieddate' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'timezone'     => $formData['timezone'],
                    'address1'     => trim($formData['address1']),
                    'postcode'     => strtoupper($formData['postcode']),
                    'country'      => $formData['country']
                );

                $_SESSION['Zend_Auth']['storage']->firstname = $formData['firstname'];
                $_SESSION['Zend_Auth']['storage']->lastname = $formData['lastname'];

                if (isset($formData['gender']) && $formData['gender']) {
                    $data['gender'] = $formData['gender'];
                }

                if (isset($formData['birthdate']) && Cms_Common::validateDate($formData['birthdate'], 'd-m-Y')) {
                    $data['birthdate'] = date('Y-m-d', strtotime($formData['birthdate']));
                    $_SESSION['Zend_Auth']['storage']->birthdate = $data['birthdate'];
                }

                if (isset($formData['state']) && $formData['state']) {
                    $data['state'] = $formData['state'];
                }

                if (isset($formData['city']) && $formData['city']) {
                    $data['city'] = $formData['city'];
                }
                $data['postcode'] = $formData['postcode'];

                //upload file
                if ($user['photo'] != '' || isset($formData['deletephoto'])) {
                    @unlink($this->uploadDir . $user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir);


                if (!empty($files)) {
                    $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if (!empty($files)) {
                    $temp = array();
                    foreach ($files as $file) {
                        $extension = array("jpg", "jpeg", "png", "gif");

                        if (in_array(substr(strrchr($file['filename'], '.'), 1), $extension)) {
                            $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                            $photochanged = 1;
                        } else {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }

                // save document data (if any)
                if (!empty($temp)) {
                    $dataFile['user_id'] = $user['id'];
                    $dataFile['doc_title'] = $formData['title'];
                    $dataFile['doc_filename'] = $temp['filename'];
                    $dataFile['doc_hashname'] = $temp['fileurl'];
                    $dataFile['doc_url'] = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location'] = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension'] = $temp['ext'];
                    $dataFile['doc_filesize'] = $temp['filesize'];

                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if (($user['id'] == $this->auth->getIdentity()->id) && $photochanged) {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $userDb->update($data, array('id = ?' => $this->auth->getIdentity()->id));

                $response = Cms_Curl::__(SMS_API . '/get/Registration/do/syncProfileToSms', array('external_id' => $user['external_id']));

                Cms_Common::notify('success', 'Information Updated');
                $this->redirect('/dashboard');
            } else {

            }
        }

        $announcements = $this->getannouncement();
        $this->view->announcements = $announcements;

        $event = $this->getcalendar();
        $this->view->event = $event;

        //$this->view->documents = $userDocuments;
        $this->view->user = $user;
        $this->view->form = $form;
    }

    public function chatAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('chat'));

        $auth = Zend_Auth::getInstance()->getIdentity();

        $permResourcesDb = new App_Model_PermissionResources();
        $permRoleDb = new App_Model_PermissionRole();
        $enrolDb = new App_Model_Enrol();
        $userDb = new App_Model_User();
        $onlineDb = new App_Model_UserOnline();
        $chatDb = new App_Model_Chat();



        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $fullname = $auth->firstname . ' ' . $auth->lastname;

            if (isset($formData["message"]) && strlen($formData["message"]) > 0) {
                $username = $auth->id;
                $message = filter_var(trim($formData["message"]), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
                $user_ip = $_SERVER['REMOTE_ADDR'];
                $msg_time = date('h:i A M d'); // current time

                $formData['user_id'] = $auth->id;
                $formData['date_time'] = date('Y-m-d H:i:s', strtotime($msg_time));

                //insert new message in db
                $saveChat = $chatDb->insertChat($formData);

                if ($saveChat) {
                    echo '<div class="shout_msg"><time>' . $msg_time . '</time><span class="username">' . $fullname . '</span><span class="message">' . $message . '</span></div>';
                }
            } elseif ($formData["fetch"] == 1) {
                $results = $chatDb->getChat($auth->id);

                foreach ($results as $row) {
                    $msg_time = date('h:i A M d', strtotime($row["date_time"])); //message posted time
                    echo '<div class="shout_msg"><time>' . $msg_time . '</time><span class="username">' . $row["firstname"] . ' ' . $row['lastname'] . '</span> <span class="message">' . $row["message"] . '</span></div>';
                }
            } else {
                //output error
                header('HTTP/1.1 500 Are you kiddin me?');
                exit();
            }

            exit;
        }

        //get users & no of onilne
        $totalusers = $onlineDb->getOnlineInt();
        $totalstudents = $onlineDb->getOnlineInt('', 'student');
        $totallecturer = $onlineDb->getOnlineInt('', 'lecturer');

        $onlineusers = $onlineDb->getOnline();
        $onlinestudents = $onlineDb->getOnline('', 'student');
        $onlinelecturer = $onlineDb->getOnline('', 'lecturer');

        //enrolled buddies
        $myprograms = $enrolDb->getEnrolledProgramme($auth->id, false);

        $totalenrolledprogram = count($myprograms);
        $totalenrolled = 0;
        $a = 0;
        if ($myprograms) {

            foreach ($myprograms as $a => $mypg) {
                $curriculumId = $mypg['id'];
                $IdProgram = $mypg['external_id'];
                $learningmode = ($mypg['learningmode'] == 'OL') ? 577 : 578;

                $mycourses = $enrolDb->getEnrolledCourses($auth->id, $curriculumId, false);
                $totalenrolled += count($mycourses);
                $myprograms[$a]['courses'] = $mycourses;

                foreach ($mycourses as $index => $course_data) {
                    $onlinecoursemates = $onlineDb->getOnlineCourseMates($course_data['id']);
                    $myprograms[$a]['courses'][$index]['onlinemates'] = $onlinecoursemates;
                }
            }
        }

        $this->view->totalusers = $totalusers;
        $this->view->totalstudents = $totalstudents;
        $this->view->totallecturer = $totallecturer;

        $this->view->onlineusers = $onlineusers;
        $this->view->onlinestudents = $onlinestudents;
        $this->view->onlinelecturer = $onlinelecturer;

        $this->view->myprograms = $myprograms;
        $this->view->auth = $auth;
    }


    public function profileAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('profile'));
        $userDb = new App_Model_User();

        $userDocDb = new App_Model_UserDocuments();

        $user = $userDb->getUser($this->auth->getIdentity()->id);

        // echo "<pre>";
        // print_r($user);
        // die();

        $id = $this->auth->getIdentity()->id;

        $form = new App_Form_User();
        $dbciifpdoc= new App_Model_Ciifpdoc;

         $user_qualification = new App_Model_UserQualification();

         $user_company = new App_Model_UserCompany();
          $form->populate($user);

             $getICDoc = $userDocDb->getICDoc($id);




            $this->view->IC_doc = $getICDoc;

            $getAcDoc = $userDocDb->getAcDoc($id);
           

            $this->view->AC_doc = $getAcDoc;

            $getAtDoc = $userDocDb->getAtDoc($id);

            $this->view->AT_doc = $getAtDoc;
            $getCLDoc = $userDocDb->getCLDoc($id);
            $this->view->CL_doc = $getCLDoc;
            $getPreDoc = $userDocDb->getPreDoc($id);
            $this->view->Pre_doc = $getPreDoc;

            $getCvDoc = $userDocDb->getCvDoc($id);
 
           $this->view->CV_doc = $getCvDoc;        

        //get tbl_takafuloperator (utk get company) `registration_type` = '1026' = Company
        $smsCompanyDb = new Admin_Model_DbTable_Company();
        $companyList = $smsCompanyDb->fetchAll();
        $this->view->companyList = $companyList;

        //unset
        $form->email->setRequired(false);
        $form->username->setRequired(false);
        $form->gender->setRequired(false);
        $form->estatus->setRequired(false);
         $photochanged = 0;
         $dbUser = new App_Model_User;

         
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
           
            
            if (isset($this->auth->getIdentity()->login_as)) {//if admin
                $modifiedby = $this->auth->getIdentity()->login_as;
                $modifiedrole = "superadmin";
            } else {
                $modifiedby = $this->auth->getIdentity()->id;
                $modifiedrole = $this->auth->getIdentity()->role;
            }
            if ($form->isValid($formData)) {
                //update

            $dob =   date('y-m-d', strtotime($formData['birthdate']));
                 $user_data = array(
                    'firstname'    => $formData['firstname'],
                    'salutation'    => $formData['salutation'],
                    'gender'    => $formData['gender'],
                    'address1'    => $formData['address1'],
                    'address2'    => $formData['address2'],
                    'address3'    => $formData['address3'],
                    'lastname'     => $formData['lastname'],
                    'add_qualification'  => $formData['add_qualification'],
                    'birthdate' =>  $dob,
                    'modifiedby'   => $modifiedby,
                    'modifiedrole' => $modifiedrole,
                    'modifieddate' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'timezone'     => isset($formData['timezone']),
                    'address1'     => trim($formData['address1']),
                    'postcode'     => strtoupper($formData['postcode']),
                    'state'      => $formData['state'],
                    'industrial_exp' => $formData['industrial_exp'],
                    'country'      => $formData['country'],
                    'city'      => $formData['city'],
                    'qualification_level' =>$formData['qualification_level'],
                    'update_profile' => 1,
                    'tel_office'     => $formData['tel_office'],
                     'sec_mobile'    => $formData['sec_mobile'],
                     'cor_address1' => $formData['cor_address1'],
                     'cor_address2' => $formData['cor_address2'],
                     'cor_address3' => $formData['cor_address3'],
                     'cor_city'      => $formData['cor_city'],
                     'cor_state'     => $formData['cor_state'],
                     'cor_country'   => $formData['cor_country'],
                     'cor_postcode'  => $formData['cor_postcode'],
                    // 'company'      => $formData['employer-name']
                );


            
      
                    $dbUser->update($user_data, array('id = ?' =>$this->auth->getIdentity()->id));
                   $data = count($formData['qualification']);
                   // echo $data;
                   // die();

if(empty($user['qualification'])){ 
                    $qualification = array('qualification' => $formData['qualification'],
                                        'q_year' => $formData['q_year'],
                                        'q_university' => $formData['q_university'],
                                         'user_id' => $this->auth->getIdentity()->id );
                      

                 $update_user = $user_qualification->insert($qualification);
}else{
     $qualification = array('qualification' => $formData['qualification'],
                                        'q_year' => $formData['q_year'],
                                        'q_university' => $formData['q_university'],
                                         'user_id' => $this->auth->getIdentity()->id );

     $user_qualification->update($qualification, array('user_id = ?' =>$this->auth->getIdentity()->id));
}


   if(empty($user['u_company'])){ 
                   
                 
                    $company = array('u_company' => $formData['u_company'],
                                      
                                        'u_designation' => $formData['u_designation'],
                                        'c_address1' => $formData['c_address1'],
                                        'c_address2' => $formData['c_address2'],
                                        'c_address3' => $formData['c_address3'],
                                        'c_country' => $formData['c_country'],
                                        'c_state' => $formData['c_state'],
                                        'c_fromdate' => date('y-m-d', strtotime($formData['c_fromdate'])),
                                        'c_todate' => date('y-m-d', strtotime($formData['c_todate'])),
                                        'description' =>$formData['description'],
                                        'samecompany' => $formData['samecompany'],
                                         'user_id' => $this->auth->getIdentity()->id );

                // $update_user = $user_company->insert($company);

               

                 $update_user = $user_company->insert($company);
}else{
    $company = array('u_company' => $formData['u_company'],
                                      
                                        'u_designation' => $formData['u_designation'],
                                        'c_address1' => $formData['c_address1'],
                                        'c_address2' => $formData['c_address2'],
                                        'c_address3' => $formData['c_address3'],
                                        'c_city' => $formData['c_city'],
                                        'c_country' => $formData['c_country'],
                                        'c_state' => $formData['c_state'],
                                        'c_postcode' => $formData['c_postcode'],
                                        'c_fromdate' => date('Y-m-d', strtotime($formData['c_fromdate'])),
                                        'c_todate' => date('Y-m-d', strtotime($formData['c_todate'])),
                                        'description' =>$formData['description'],
                                         'user_id' => $this->auth->getIdentity()->id );

     $user_company->update($company, array('user_id = ?' =>$this->auth->getIdentity()->id));
}



      
                  $membership_id= $this->auth->getIdentity()->id;

                   
                              $files  = $_FILES;

                          

                              // FileUpload Academic
                               $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
                                 $target_file1 = 'IC'.'-'.$membership_id.'-'.$_FILES['file1']['name'][$i];

    
                            

     if($getICDoc==null)
                        {
                        

   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH.$target_file1)) {

                       
                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
                           
                           }else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }
                       }
                    elseif(move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH.$target_file1)){

                          
                                            $data = array('doc_name' => $target_file1, );

                                 // $dbciifpdoc->update_doc($getICDoc[0]['id'],$target_file1);
                                $dbciifpdoc->update($data, array('id = ?' => $getICDoc[0]['id']));

                                    } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }


                             


                             for($i=0;$i<count($_FILES['file2']['name']);$i++) 
                              {
                                   if($i==0) {
                                      $k = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $k = $j;
                                   
                                   }
                                 $target_file2 = 'Academic Certificate'.'-'.$membership_id.'-'.$_FILES['file2']['name'][$k];
                        
  if(empty($getAcDoc))
                        {
   if (move_uploaded_file($_FILES["file2"]["tmp_name"][$k], UPLOAD_PATH.$target_file2)) {



                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file2);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                      }else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }
                                        }elseif (move_uploaded_file($_FILES["file2"]["tmp_name"][$k], UPLOAD_PATH.$target_file2)) {

                                              $data = array('doc_name' => $target_file2, );
                                $dbciifpdoc->update($data, array('id = ?' => $getAcDoc[0]['id']));
                                            # code...
                                        }else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        } 
                                        

                              } 


                              for($i=0;$i<count($_FILES['file3']['name']);$i++) 
                              {

                                if($i==0) {
                                      $l = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $l = $j;
                                   
                                   }

                                        // move upload file
                                 $target_file3 = 'Academic transcript'.'-'.$membership_id.'-'.$_FILES['file3']['name'][$l];
 if(empty($getAtDoc))
                        {
   if (move_uploaded_file($_FILES["file3"]["tmp_name"][$l], UPLOAD_PATH.$target_file3)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file3);

            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        }else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }
}elseif (move_uploaded_file($_FILES["file3"]["tmp_name"][$l], UPLOAD_PATH.$target_file3)) {
                                            # code...
                                      $data = array('doc_name' => $target_file3, );

                                $dbciifpdoc->update($data, array('id = ?' => $getAtDoc[0]['id']));
                                        }
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }  
//                               echo "<pe>"
// print_r($_FILES);exit;

                               for($i=0;$i<count($_FILES['file4']['name']);$i++) 
                              {
                                if($i==0) {
                                      $m = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $m = $j;
                                   
                                   }
                                        // move upload file
                                 $target_file4 = 'Confirmation letter'.'-'.$membership_id.'-'.$_FILES['file4']['name'][$m];

 if(empty($getCLDoc))
                        {
   if (move_uploaded_file($_FILES["file4"]["tmp_name"][$m], UPLOAD_PATH.$target_file4)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file4);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        }else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        } 
                                    }

                                        elseif (move_uploaded_file($_FILES["file4"]["tmp_name"][$m], UPLOAD_PATH.$target_file4)) {
                                            # code...
                          $data = array('doc_name' => $target_file4, );

                             $dbciifpdoc->update($data, array('id = ?' => $getCLDoc[0]['id']));
                                        }
                                        else {
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }


                               for($i=0;$i<count($_FILES['file5']['name']);$i++) 
                              {

                                if($i==0) {
                                      $n= 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $n = $j;
                                   
                                   }

                                        // move upload file
                                 $target_file5 = 'Previous Membership'.'-'.$membership_id.'-'.$_FILES['file5']['name'][$n];

if(empty($getPreDoc)){
   if (move_uploaded_file($_FILES["file5"]["tmp_name"][$n], UPLOAD_PATH.$target_file5)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file5);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        }else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }  }elseif (move_uploaded_file($_FILES["file5"]["tmp_name"][$n], UPLOAD_PATH.$target_file5)) {
                                            # code...
                      $data = array('doc_name' => $target_file5 );

                             $dbciifpdoc->update($data, array('id = ?' => $getPreDoc[0]['id']));
                                        }
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }  

                              for($i=0;$i<count($_FILES['file6']['name']);$i++) 
                              {

                                if($i==0) {
                                      $n= 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $n = $j;
                                   
                                   }

                                        // move upload file
                                 $target_file6 = 'CV'.'-'.$membership_id.'-'.$_FILES['file6']['name'][$n];
    if(empty($getCvDoc)){

   if (move_uploaded_file($_FILES["file6"]["tmp_name"][$n], UPLOAD_PATH.$target_file6)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file6);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        }else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        } }elseif (move_uploaded_file($_FILES["file6"]["tmp_name"][$n], UPLOAD_PATH.$target_file6)) {
                                            # code...
  $data = array('doc_name' => $target_file6 );

                             $dbciifpdoc->update($data, array('id = ?' => $getCvDoc[0]['id']));
                                        }
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }  
        

              // // print_r($formData);exi;
              //   $_SESSION['Zend_Auth']['storage']->firstname = $formData['firstname'];
              //   $_SESSION['Zend_Auth']['storage']->lastname = $formData['lastname'];

              //   if (isset($formData['gender']) && $formData['gender']) {
              //       $data['gender'] = $formData['gender'];
              //   }

              //   if (isset($formData['birthdate']) && Cms_Common::validateDate($formData['birthdate'], 'd-m-Y')) {
              //       $data['birthdate'] = date('Y-m-d', strtotime($formData['birthdate']));
              //       $_SESSION['Zend_Auth']['storage']->birthdate = $data['birthdate'];
              //   }

              //   if (isset($formData['state']) && $formData['state']) {
              //       $data['state'] = $formData['state'];
              //   }

              //   if (isset($formData['city']) && $formData['city']) {
              //       $data['city'] = $formData['city'];
              //   }
              //   $data['postcode'] = $formData['postcode'];

              //   //upload file
              //   if ($user['photo'] != '' || isset($formData['deletephoto'])) {
              //       @unlink($this->uploadDir . $user['photo']);
              //       $data['photo'] = null;
              //       $photochanged = 1;
              //   }

              //   //upload file
              //   $files = Cms_Common::uploadFiles($this->uploadDir);


              //   if (!empty($files)) {
              //       $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
              //       $photochanged = 1;
              //   }

              //   //upload file
              //   $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

              //   if (!empty($files)) {
              //       $temp = array();
              //       foreach ($files as $file) {
              //           $extension = array("jpg", "jpeg", "png", "gif");

              //           if (in_array(substr(strrchr($file['filename'], '.'), 1), $extension)) {
              //               $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
              //               $photochanged = 1;
              //           } else {
              //               $temp = $file;
              //               $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
              //           }
              //       }
              //   }

                // save document data (if any)

                              $getICDoc = $userDocDb->getICDoc($id);


            $this->view->IC_doc = $getICDoc;

            $getAcDoc = $userDocDb->getAcDoc($id);
           

            $this->view->AC_doc = $getAcDoc;

            $getAtDoc = $userDocDb->getAtDoc($id);

            $this->view->AT_doc = $getAtDoc;
            $getCLDoc = $userDocDb->getCLDoc($id);
            $this->view->CL_doc = $getCLDoc;
            $getPreDoc = $userDocDb->getPreDoc($id);
            $this->view->Pre_doc = $getPreDoc;

            $getCvDoc = $userDocDb->getCvDoc($id);

          
           $this->view->CV_doc = $getCvDoc;        


                    $get_doc =$dbciifpdoc->get_doc($membership_id);

                   if($formData['register'])
                   {
                              
                     $userModel = new App_Model_User();

                     $userdata = array('estatus' =>591 );
                    
                    $user_id1 =  $userModel->update($userdata, array('id = ?' => $membership_id));
                   }


                    if($formData['submit'])
                   {


                 $userModel = new App_Model_User();

                     $userdata = array('estatus' =>592);
                    
                    $user_id1 =  $userModel->update($userdata, array('id = ?' => $membership_id));

                   }
                
                    $this->redirect('/dashboard');
                

                if ($transaction[0]["at_appl_type"] == 1) {//Membership Only
                    $this->redirect('/membership/apply/mr_id/' . $membership[0]["mr_id"]);
                } else if ($transaction[0]["at_appl_type"] == 2) {//Qualification Only
                    $this->redirect('/user/dashboard/tab/qualification');
                } else if ($transaction[0]["at_appl_type"] == 3) {//Membership & Qualification
                    $this->redirect('/membership/apply/mr_id/' . $membership[0]["mr_id"]);
                } else {
                    $this->redirect('/dashboard');
                }
            } else {
                 $this->redirect('/dashboard');

            }
        }

        //$this->view->documents = $userDocuments;
        $this->view->user = $user;
        $this->view->form = $form;
    }
    public function passwordAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('password'));
        Cms_Common::expire();
        $auth = Zend_Auth::getInstance();

        $id = Zend_Auth::getInstance()->getIdentity()->id;
        
        if ($this->getRequest()->isPost()) {
            $userDb = new App_Model_User();

            $formData = $this->getRequest()->getPost();

            $formData = Cms_Common::cleanArray($formData);

            if (Cms_Common::tokenCheck($formData['token']) == false) {
                $this->view->errMsg = $this->view->translate('Invalid Request. Try again.');
                return;
            }

            $user = $userDb->getUser($id, 'id');
            
            $tab_password = $user['password'];
            $isPassword = $userDb->checkPassword($formData, $tab_password);
           

            if ($isPassword == 0) {
                $this->view->errMsg = $this->view->translate('Invalid Old Password.');
                return false;
            }

            if (strlen($formData['newpass']) < 6) {
                $this->view->errMsg = $this->view->translate('Passwords must be at least 6 characters long');
                return false;
            }

            if ($formData['newpass'] != $formData['confirmpassword']) {
                $this->view->errMsg = $this->view->translate('New Password and Confirm Password does not match. Please try again.');
                return false;
            }

 
            if ($auth->getIdentity()->login_as) {//if admin
                $modifiedby = $auth->getIdentity()->login_as;
                $modifiedrole = "superadmin";
            } else {
                $modifiedby = $auth->getIdentity()->id;
                $modifiedrole = $auth->getIdentity()->role;
            }
            

            //reset password
            $salt = Cms_Common::generateRandomString(22);
            $options = array('salt' => $salt);
            $hash = password_hash($formData['newpass'], PASSWORD_BCRYPT, $options);

            $userDb->update(array('password' => $hash, 'salt' => $salt, 'modifiedby' => $modifiedby, 'modifieddate' => date('Y-m-d'), 'modifiedrole' => $modifiedrole), array('id = ?' => $id));


            Cms_Common::notify('success', 'Your new password has been set. Please login with your new password.');
            $this->redirect('/logout');
            
        }
    }

    public function purchaseHistoryAction()
    {
        $auth = Zend_Auth::getInstance()->getIdentity();
        $userId = $auth->id;

        $smsExamDB = new App_Model_SmsExam();

        $param = array('user_id = ?' => $auth->id, 'a.visibility = ?' => 1);
        $results = $this->orderDb->getOrders($param);

        $smsDb = getDB2();


         $Invoice_detail = new App_Model_InvoiceMainDetails();

           $get_invoice = $Invoice_detail->get_invoice_detail($userId);
         

        $this->view->get_invoice = $get_invoice;

      
        $Invoice = new App_Model_ProformaInvoiceMain();

           $get_qualification = $Invoice->get_invoice($userId);

           

        $this->view->orders = $get_qualification;


     
        Cms_Hooks::push('user.sidebar', self::userSidebar('purchase'));
    }

     public function invoicelistAction()
    {
        $auth = Zend_Auth::getInstance()->getIdentity();
        $userId = $auth->id;

        $smsExamDB = new App_Model_SmsExam();

        $param = array('user_id = ?' => $auth->id, 'a.visibility = ?' => 1);
        $results = $this->orderDb->getOrders($param);

        $smsDb = getDB2();

      
       
     
        Cms_Hooks::push('user.sidebar', self::userSidebar('purchase'));
    }

    public function calendarAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('calendar'));

        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;

        $userDb = new App_Model_User();

        $user = $userDb->getUser($auth->id);

       
         $event = $this->getcalendar();
       

        $this->view->event = $event;
        $this->view->user = $user;
    }

    public function getcalendar()
    {
        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;

        $userDb = new App_Model_User();

       //$user = $userDb->getUser($auth->id);

        $this->coursesDb = new App_Model_Courses();
        $this->calendarDb = new App_Model_CalendarEvent();
        $materialDB = new Admin_Model_DbTable_CpdMaterial();
        //get id
        //$id = $this->_getParam('course');
        if ($materialDB) {
            
            $course = $materialDB->MaterialList();
            $this->view->course = $course;

            $event = $materialDB->MaterialList();

        }
        if ($materialDB) {

            $course = $materialDB->eventList();
            $this->view->course = $course;

            $event = $materialDB->MaterialList();
        } else {

            $event = $this->calendarDb->fetchAll(array('user_id = ?' => $user_id, 'visibility = ?' => '1'));
        }

           
        return $event;
    }

    public function announcementAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('announcement'));

        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;

        $userDb = new App_Model_User();
        $announcementsDb = new App_Model_Announcements();

        $user = $userDb->getUser($auth->id);


        $total_announcemenets = count($announcementsDb->getCourseAnnouncement(array('course_id = ?' => 0, 'visibility = ?' => '1')));

        $announcements = $announcementsDb->getGeneralAnnouncementQuery(array('course_id = ?' => 0, 'visibility = ?' => '1'));
        $announcements = Cms_Paginator::query($announcements);

        $this->view->announcements = $announcements;
        $this->view->total_announcemenets = $total_announcemenets;
    }

    public function getannouncement()
    {

        $announcementsDb = new App_Model_Announcements();
        $announcements = $announcementsDb->getGeneralAnnouncementQuery(array('course_id = ?' => 0, 'visibility = ?' => '1'));
        $announcements = Cms_Paginator::query($announcements);

        return $announcements;
    }

    public function invoiceAction()
    {
        $auth = Zend_Auth::getInstance()->getIdentity();
        $userId = $auth->id;

        $smsExamDB = new App_Model_SmsExam();

        $param = array('user_id = ?' => $auth->id, 'a.visibility = ?' => 1);
        $results = $this->orderDb->getOrders($param);

        $smsDb = getDB2();

      
        $programDb = new App_Model_ProgramCategory();

           $get_qualification = $this->programReg->get_account($userId);
           // echo '<pre>';
           // print_r($get_qualification);
           // die();

        $this->view->orders = $get_qualification;
     
        Cms_Hooks::push('user.sidebar', self::userSidebar('purchase'));
    }

    public function paymentAction()
    {
        $auth = Zend_Auth::getInstance()->getIdentity();
        $id = $this->getParam('id');

        $order = $this->orderDb->getOrder(array('id = ?' => $id));

        //check ownership
        if ($order['user_id'] != $auth->id) {
            return Cms_Render::error('This order doesn\'t belong to you.');
        }

        //check status
        if ($order['status'] != 'PENDING') {
            return Cms_Render::error('You can only pay for pending orders.');
        }


        //get invoice
        $params = array('id' => $order['invoice_id']);
        $invoiceData = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoice', $params);
        //pr($invoiceData);exit;

        if (!isset($invoiceData['id'])) {
            $params = array('ProformaId' => $invoiceData['proforma_invoice_id'], 'creatorId' => 665, 'from' => 1, 'returnCurrency' => 1);
            $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoice', $params);
        } else {
            $invoice = array(
                'amount'     => $invoiceData['bill_amount'],
                'invoiceId'  => $invoiceData['id'],
                'invoiceNo'  => $invoiceData['bill_number'],
                'currencyId' => $invoiceData['currency_id'],
                'error'      => $invoiceData['error']
            );
        }
        //pr($invoice);exit;

        if (empty($invoice)) {
            return Cms_Render::error('Invalid Invoice ID');
        }

        //get migs
        $migs = Cms_Curl::__(URL_PAYMENT . '/migs/api/call/migstrans', array('mt_external_id' => $order['id']));

        //proceed to payment
        /*$params = array(
            'token' => Cms_Common::token('ibfimpay'),
            'invoiceId' => $invoice['id'],
            'requestFrom' => 1,
            'currencyId' => $invoice['currency_id'],
            'invoiceNo' => $invoice['bill_number'],
            'totalAmount' => Cms_Common::MigsAmount($invoice['bill_balance']),
            'die' => 1,
            'order_id' => $order['id']
        );*/

        $params = array(
            'token'       => Cms_Common::token('ibfimpay'),
            'invoiceId'   => $invoice['invoiceId'],
            'requestFrom' => 1,
            'currencyId'  => $invoice['currencyId'],
            'invoiceNo'   => $invoice['invoiceNo'],
            'totalAmount' => Cms_Common::MigsAmount($invoice['amount']),
            'die'         => 1,
            'order_id'    => $order['id']
        );

        if (isset($migs['data']) && !empty($migs['data'])) {
            $params['payOnly'] = 1;
            $params['mtID'] = $migs['data']['mt_id'];
        }

        //redirect to payment gateway
        $urlMigs = Cms_Curl::__(URL_PAYMENT . '/migs/', $params);

        $this->view->pay_url = $urlMigs;

        header('location: ' . $urlMigs);

        exit;
    }

    public static function userSidebar($active = 'dashboard')
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view->active = $active;
        $view->setBasePath(APPLICATION_PATH . '/views/');
        return $view->render('plugins/usersidebar.phtml');
    }

   public function receiptdownloadAction()
    {


         $auth = Zend_Auth::getInstance()->getIdentity();
        $userId = $auth->id;
         $programDb = new App_Model_ProgramCategory();

           $get_qualification = $this->programReg->get_account($userId);
         $InvoiceNo =  'CIIF'.  date('YHims').$userId;
           $Invoice = new App_Model_InvoiceMain();

           $receiptDate = date('YHimhs');

           $get_invoice = $Invoice->get_invoice($userId);
           $name = $get_qualification[0]['firstname'].$get_qualification[0]['lastname'];

           $total_amount = $get_qualification[0]['total_amount'];
         

         
            $payment = $get_qualification[0]['payment_status'];
        $get_member_id =$this->programReg->get_member_id($userId);
       $memberId = $get_member_id[0]['mr_sp_id'];

      $processing_fee="<body>
      <table align='center' style='width: 100%;' cellpadding='10'>
         <tr>
            <td style='padding-left: 12%;padding-bottom: 30%;'>
               <img src='./assets/images/ciifp_new_logo1.jpg' alt='ciifp logo' height= 100px width=300px >
            </td>
            <td>
               <table style='text-align: right;width: 100%;font-size: 12px;'>
                  <tr>
                     <td>KNOWLEDGE | INTEGRITY | SERVICE</td>
                  </tr>
                  <tr>
                     <td>
                        <p style='font-size: 10px;'>
                           19th Floor, Menara Takaful Malaysia,<br/>
                           Jalan Sultan Sulaiman, 50000 Kuala Lumpur<br/>
                           03-2276 5279/5232
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td>&nbsp</td>
                  </tr>
                  <tr>
                     <td style='color:#459bca;font-size: 12px;'>www.ciif-global.org</td>
                  </tr>
               </table>
            </td>
      </table>
      <table align='center' width='80%' style='margin-top: -12%;font-size: 12px;font-weight: bold;'>
         <tr>
            <td>PRIVATE AND CONFIDENTIAL</td>
            <td style='text-align: right;'>RECEIPT NO</td>
         </tr>
         <tr>
            <td>4/4/2017</td>
            <td style='text-align: right;'>receiptDate</td>
         </tr>
         <tr>
            <td>&nbsp</td>
         </tr>
         <tr>
            <td></td>
            <td style='text-align: right;'>MEMBERSHIP NO:<br/>$memberId</td>
         </tr>
      </table>
      <p style='padding-left: 10%;margin-top: 2%;font-size: 12px;'>
         Ariffhidayat Ali,<br>
         Centre of  Consulting  @ Executive Programmes,<br>
         INCEIF,  Jalan Universiti  A,<br>
         59100  Kuala Lumpur 
      </p>
      <p style='padding-left: 10%;margin-top: 3%;width:80%;font-size: 12px;'>Dear Respected Member,</p><br><br>
         <p style='padding-left: 10%;text-align:center;margin-top: 3%;width:80%;font-size: 12px;'><b><u align='center'>2017 MEMBERSHIP FEE RECEIPT</u></b><br>
      </p>
      <table  align='center' border='1' style='border-collapse: collapse;width: 80%;margin-top: 4;font-size: 12px;'>
         <tr style='background-color: #a3a5a8;'>
            <td style='text-align: center;font-weight: bold;'>
               Item
            </td>
            <td style='text-align: center;font-weight: bold;'>Amount</td>
         </tr>";
          $i=1;
$total =0;
foreach ($get_invoice as $key=>$value) {
    
    $description = $value['description'];
    $amount =$value['amount'];
    $processing_fee .="
        <tr>
            <td>$description</td>
            <td style='text-align: right;'>$amount</td>
        </tr>";
$total = $total +$amount;
$i++;
}
        
    $processing_fee .= "<tr>
<td><b>TOTAL</b></td>
<td style='text-align:right'>RM $total</td>
</tr></table>
      <p style='padding-left: 10%;font-size: 12px;margin-top: 2%;'>Thank you for being a member  of  the Chartered Institute of  Islamic Finance Professionals (CIIF).   </p>
      <p style='padding-left: 10%;font-size: 12px;'>If you have  any queries about your  membership, kindly  contact our membership  team  at  +603  2276  
         5279  or  email to  <u style='color: #459bca;'>membership@ciif-global.org </u>
      </p>
      <p style='padding-left: 10%;font-size: 12px;'>This receipt is  computer  generated and no  signature is  required. </p>
      <br/><br/><br/><br/>
      
   </body>";
   
    $fieldValues['[Invoice]'] = $processing_fee;
        require_once '../library/dompdf/dompdf_config.inc.php';
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controllers
        $autoloader->pushAutoloader('DOMPDF_autoload');
 $option = array(
            'content' => $processing_fee,
            'save' => false,
            'file_name' => $name.''.'Receipt',
            'css' => '@page { margin: 20px 20px 20px 20px}
                        body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                        table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                        table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
                        table.tftable tr {background-color:#ffffff;}
                        table.tftable td {font-size:10px;padding: 5px;}
                        hr {
  color: #729ea5;
}'
                        ,

            'header' => ''
        );
 $generate = new Cms_Common();
        $pdf = $generate->generatePdf($option);
        // $slip = Cms_Curl::__(SMS_API . '/get/Registration/do/registrationSlip', $params);

        echo $pdf;
        exit();
    } 

      public function slipperformainvoiceAction()
    {


         $auth = Zend_Auth::getInstance()->getIdentity();
        $userId = $auth->id;
         $programDb = new App_Model_ProgramCategory();

           $get_qualification = $this->programReg->get_account($userId);

            $userDb = new App_Model_User();
            $getMember= $userDb->Details($userId);
           // echo "<pre>";
           // print_r($getMember);
           // die();
         $InvoiceNo =  'CIIF'.  date('YHims').$userId;
           $Invoice = new App_Model_ProformaInvoiceMain();
           $fee = $this->getParam('id');

       
         $performaId = $this->getParam('performaId');  


          
           $name = $getMember['firstname'].$getMember['lastname'];
           $address = $getMember['address1'];
           $address1 = $getMember['address2'];
           $address2 = $getMember['address3'];
           $city = $getMember['city'];
           $state = $getMember['StateName'];
           $country = $getMember['CountryName'];
        $get_member_id =$this->programReg->get_member_id($userId);
       
           $total_amount = $get_qualification[0]['total_amount'];
           

          
            $payment = $get_qualification[0]['payment_status'];
            $memberId = $get_member_id[0]['mr_sp_id'];
           
      $processing_fee="
<body>
    
   <table align='center' border=0 style='width:100%' >
         <tr>
          <td width='10%'>
               &nbsp;
            </td>
            <td width='50%'>
               <img src='./assets/images/ciifp_new_logo1.jpg' alt='ciifp logo' >
            </td>

            <td width='40%'>
              <table>
                 <tr><td style='text-align:right;'>KNOWLEDGE | INTEGRITY | SERVICE</td></tr>
                                  <tr><td  style='text-align:right;'> 3rd Floor, Menara Takaful Malaysia,<br/>
                           Jalan Sultan Sulaiman, 50000 Kuala Lumpur<br/>
                           03-2273 2135 / 2136</td></tr>
                           <tr>
                           <td style='text-align:right;color:#459bca;font-size: 12px;'>www.ciif-global.org</td></tr>

                 
              </table>
            </td>
        </tr>
               
              
      </table>
      <table align='center' width='80%' style='font-size: 12px;font-weight: bold;'>
         <tr>
            <td style='font-size: 12px;'>PRIVATE AND CONFIDENTIAL</td>
            <td style='text-align: right;font-size: 12px;'>INVOICE NO</td>
         </tr>

         <tr>
            <td>01/01/2018</td>
            <td style='text-align: right;'>$InvoiceNo</td>
         </tr>
         <tr>
         <td>
         </td>
         </tr>
         <tr>
            <td></td>
            <td style='text-align: right;font-size: 12px;'>MEMBERSHIP NO:<br/>
            $memberId</td>
         </tr>
      </table>
      <p style='padding-left: 10%;margin-top: 2%;font-size: 10px;'>
         $name<br>
         $address<br>
         $address1 <br>
         $address2 <br>
         $city<br>
         $state<br>
         $country
      </p>
      <p style='padding-left: 10%;margin-top: 1%;width:80%;font-size: 10px;'>Dear Respected Member,<br><br>
         <b><u>ANNUAL MEMBERSHIP FEE FOR YEAR 2018</u></b><br><br>
         Congratulations once again on becoming a member of the CIIF. with reference to the membership confirmation letter sent to you earlier, we wish to inform you that your annual membership fee for the year of 2018 is now due. Details are as follows:
      </p>
      <table align='center' border='1' width='80%' style='font-size: 12px;font-weight: bold;'>
        ";

        if($fee == 0)
        {
         $i=1;
        $get_invoice = $Invoice->get_performainvoice_app($fee,$userId,$performaId);
$total =0;
foreach ($get_invoice as $key=>$value) {
    
    $description = $value['description'];
    $amount =$value['amount'];
    $processing_fee .="
        <tr>
            <td>$description</td>
            <td style='text-align: right;'>RM $amount</td>
        </tr>";
$total = $total +$amount;
$i++;
}
}

if($fee == 2)
        {
         $i=1;
        $get_invoice = $Invoice->get_performainvoice_app($fee,$userId,$performaId);
$total =0;
foreach ($get_invoice as $key=>$value) {
    
    $description = $value['description'];
    $amount =$value['amount'];
    $processing_fee .="
        <tr>
            <td>$description</td>
            <td style='text-align: right;'>RM $amount</td>
        </tr>";
$total = $total +$amount;
$i++;
}
}
 if($fee == 1)
        {
         
         $i=1;
        $get_invoice = $Invoice->get_invoice_print($fee,$userId);
         $get_qualifiacion = $this->programReg->get_qualification($userId);
        

         $route = $get_qualifiacion[0]['route'];
         $level = $get_qualifiacion[0]['level'];
$total =0;
$processing_fee .=" <tr>
         <td>Programme Route</td>
            <td colspan='2' style='text-align: center;'>$route</td>
        </tr>
        <tr>
            <td>Programme Level</td>
            <td colspan='2' style='text-align: center;'>$level</td>
        </tr>";
foreach ($get_invoice as $key=>$value) {


    $program = $value['name'];
    $description = $value['description'];
    $amount =$value['program_amount'];
    $processing_fee .="
       
        <tr> 
            <td>Program</td>
            <td style='text-align: center;'>$program</td>
            <td style='text-align: right;'>RM $amount </td>
        </tr>";

$total = $total +$amount;
$i++;
}
        $programExem = new App_Model_ProgramExemption();
         $get_exem_qualifiacion = $programExem->get_exem_qualification($userId);

if (!empty($get_exem_qualifiacion)) {
    # code...

foreach ($get_exem_qualifiacion as $key=>$value) {


    $program = $value['name'];
    $amount =$value['f_amount'];
    $processing_fee .="
       
        <tr> 
            <td>Exempted Program</td>
            <td style='text-align: center;'>$program</td>
            <td style='text-align: right;'>RM $amount </td>
        </tr>";
        
$total = $total +$amount;
$i++;
}
}
}

     $processing_fee .= "<tr>
<td><b>TOTAL</b></td>
<td colspan='2' style='text-align:right'>RM $total </td>
</tr></table>
      <p><u style='padding-left: 10%;margin-top: 2%;font-size: 12px;'>Payment can be made via:</u></p>
      <table  align='center' border='1' style='border-collapse: collapse;width: 80%;font-size: 12px;'>
         <tr>
            <td style='font-size: 10px;'>
               Online Transfer or CDM
            </td>
            <td style='font-size: 10px;'>
               Account Name: Chartered Institute of Islamic Finance Professionals<br/>
               Bank: CIMB Islamic Bank Berhad<br/>
               Account No: 8602458659
            </td>
            <td rowspan='3' style='font-size: 10px;' style='text-align: center;'>
               E-mail receipt/payment slip to membership@ciif-global.org or mail to:<br/><br/>
               <p>19th Floor,<br> Menera Takaful Malasia, Jalan <br> Sultan Sulaiman,<br> 50000 Kuala Lumpur,<br> Malaysia.</p>
            </td>
         </tr>
         <tr>
            <td style='font-size: 10px;'>
               Cheque, Money Order or Bank Draft
            </td>
            <td style='font-size: 10px;'>
               Made payable to 'Chartered Institute of Islamic Finance Professionals'
            </td>
         </tr>
         <tr>
            <td style='font-size: 10px;'>
               Credit Card via PAYPAL
            </td>
            <td style='font-size: 10px;'>
               Please notify us via e-mail at membership@ciif-global.org should you wish to pay via PAYPAL payment link will then be sent to you accordingly
            </td>
         </tr>
      </table>
      <p style='padding-left: 10%;font-size: 10px;margin-top: 2%;'>Kindly ensure your payment is made within 30 days from the date of this invoice.</p>
      <p style='padding-left: 10%;font-size: 10px;'>You are also encouraged to update your profile and correspondence address by e-mailing your latest details to: membership@ciif-global.org</p>
      <p style='padding-left: 10%;font-size: 10px;'>NOTE: Please refer to the CIIF Terms & Conditions of Membership by visiting the membership page: ciif-global.org/web/membership
         <br/>
      
</body>
";


 $fieldValues['[Invoice]'] = $processing_fee;
 
        require_once '../library/dompdf/dompdf_config.inc.php';
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');
 $option = array(
            'content' => $processing_fee,
            'save' => false,
            'file_name' => $name.''.'invoice',
            'css' => '@page { margin: 20px 20px 20px 20px}
                        body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                        table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                        table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
                        table.tftable tr {background-color:#ffffff;}
                        table.tftable td {font-size:10px;padding: 5px;}
                        hr {
  color: #729ea5;
}'
                        ,

            'header' => ''
        );
 $generate = new Cms_Common();
        $pdf = $generate->generatePdf($option);
        // $slip = Cms_Curl::__(SMS_API . '/get/Registration/do/registrationSlip', $params);

        echo $pdf;
        exit();
    }


    public function slipinvoiceAction()
    {


         $auth = Zend_Auth::getInstance()->getIdentity();
        $userId = $auth->id;
         $programDb = new App_Model_ProgramCategory();

           $get_qualification = $this->programReg->get_account($userId);

            $userDb = new App_Model_User();
            $getMember= $userDb->Details($userId);
           // echo "<pre>";
           // print_r($getMember);
           // die();
         $InvoiceNo =  'CIIF'.  date('YHims').$userId;
           $Invoice = new App_Model_ProformaInvoiceMain();
           $fee = $this->getParam('id');

        $invoiceId = $this->getParam('invoiceId');
         
          
           $name = $getMember['firstname'].$getMember['lastname'];
           $address = $getMember['address1'];
           $address1 = $getMember['address2'];
           $address2 = $getMember['address3'];
           $city = $getMember['city'];
           $state = $getMember['StateName'];
           $country = $getMember['CountryName'];
        $get_member_id =$this->programReg->get_member_id($userId);
       
           $total_amount = $get_qualification[0]['total_amount'];
           

          
            $payment = $get_qualification[0]['payment_status'];
            $memberId = $get_member_id[0]['mr_sp_id'];
           
      $processing_fee="
<body>
    
   <table align='center' border=0 style='width:100%' >
         <tr>
          <td width='10%'>
               &nbsp;
            </td>
            <td width='50%'>
               <img src='./assets/images/ciifp_new_logo1.jpg' alt='ciifp logo' >
            </td>

            <td width='40%'>
              <table>
                 <tr><td style='text-align:right;'>KNOWLEDGE | INTEGRITY | SERVICE</td></tr>
                                  <tr><td  style='text-align:right;'> 3rd Floor, Menara Takaful Malaysia,<br/>
                           Jalan Sultan Sulaiman, 50000 Kuala Lumpur<br/>
                           03-2273 2135 / 2136</td></tr>
                           <tr>
                           <td style='text-align:right;color:#459bca;font-size: 12px;'>www.ciif-global.org</td></tr>

                 
              </table>
            </td>
        </tr>
               
              
      </table>
      <table align='center' width='80%' style='font-size: 12px;font-weight: bold;'>
         <tr>
            <td style='font-size: 12px;'>PRIVATE AND CONFIDENTIAL</td>
            <td style='text-align: right;font-size: 12px;'>INVOICE NO</td>
         </tr>

         <tr>
            <td>01/01/2018</td>
            <td style='text-align: right;'>$InvoiceNo</td>
         </tr>
         <tr>
         <td>
         </td>
         </tr>
         <tr>
            <td></td>
            <td style='text-align: right;font-size: 12px;'>MEMBERSHIP NO:<br/>
            $memberId</td>
         </tr>
      </table>
      <p style='padding-left: 10%;margin-top: 2%;font-size: 10px;'>
         $name<br>
         $address<br>
         $address1 <br>
         $address2 <br>
         $city<br>
         $state<br>
         $country
      </p>
      <p style='padding-left: 10%;margin-top: 1%;width:80%;font-size: 10px;'>Dear Respected Member,<br><br>
         <b><u>ANNUAL MEMBERSHIP FEE FOR YEAR 2018</u></b><br><br>
         Congratulations once again on becoming a member of the CIIF. with reference to the membership confirmation letter sent to you earlier, we wish to inform you that your annual membership fee for the year of 2018 is now due. Details are as follows:
      </p>
      <table align='center' border='1' width='80%' style='font-size: 12px;font-weight: bold;'>
        ";

        if($fee == 0)
        {
         $i=1;
        $get_invoice = $Invoice->get_invoice_app($fee,$userId,$invoiceId);
$total =0;
foreach ($get_invoice as $key=>$value) {
    
    $description = $value['description'];
    $amount =$value['amount'];
    $processing_fee .="
        <tr>
            <td>$description</td>
            <td style='text-align: right;'>RM $amount</td>
        </tr>";
$total = $total +$amount;
$i++;
}
}

if($fee == 2)
        {
         $i=1;
        $get_invoice = $Invoice->get_invoice_app($fee,$userId,$invoiceId);
        
$total =0;
foreach ($get_invoice as $key=>$value) {
    
    $description = $value['description'];
    $amount =$value['amount'];
    $processing_fee .="
        <tr>
            <td>$description</td>
            <td style='text-align: right;'>RM $amount</td>
        </tr>";
$total = $total +$amount;
$i++;
}
}
 if($fee == 1)
        {
         
         $i=1;
        $get_invoice = $Invoice->get_invoice_print($fee,$userId);
         $get_qualifiacion = $this->programReg->get_qualification($userId);
        

         $route = $get_qualifiacion[0]['route'];
         $level = $get_qualifiacion[0]['level'];
$total =0;
$processing_fee .=" <tr>
         <td>Programme Route</td>
            <td colspan='2' style='text-align: center;'>$route</td>
        </tr>
        <tr>
            <td>Programme Level</td>
            <td colspan='2' style='text-align: center;'>$level</td>
        </tr>";
foreach ($get_invoice as $key=>$value) {


    $program = $value['name'];
    $description = $value['description'];
    $amount =$value['program_amount'];
    $processing_fee .="
       
        <tr> 
            <td>Program</td>
            <td style='text-align: center;'>$program</td>
            <td style='text-align: right;'>RM $amount </td>
        </tr>";

$total = $total +$amount;
$i++;
}
        $programExem = new App_Model_ProgramExemption();
         $get_exem_qualifiacion = $programExem->get_exem_qualification($userId);

if (!empty($get_exem_qualifiacion)) {
    # code...

foreach ($get_exem_qualifiacion as $key=>$value) {


    $program = $value['name'];
    $amount =$value['f_amount'];
    $processing_fee .="
       
        <tr> 
            <td>Exempted Program</td>
            <td style='text-align: center;'>$program</td>
            <td style='text-align: right;'>RM $amount </td>
        </tr>";
        
$total = $total +$amount;
$i++;
}
}
}

     $processing_fee .= "<tr>
<td><b>TOTAL</b></td>
<td colspan='2' style='text-align:right'>RM $total </td>
</tr></table>
      <p><u style='padding-left: 10%;margin-top: 2%;font-size: 12px;'>Payment can be made via:</u></p>
      <table  align='center' border='1' style='border-collapse: collapse;width: 80%;font-size: 12px;'>
         <tr>
            <td style='font-size: 10px;'>
               Online Transfer or CDM
            </td>
            <td style='font-size: 10px;'>
               Account Name: Chartered Institute of Islamic Finance Professionals<br/>
               Bank: CIMB Islamic Bank Berhad<br/>
               Account No: 8602458659
            </td>
            <td rowspan='3' style='font-size: 10px;' style='text-align: center;'>
               E-mail receipt/payment slip to membership@ciif-global.org or mail to:<br/><br/>
               <p>19th Floor,<br> Menera Takaful Malasia, Jalan <br> Sultan Sulaiman,<br> 50000 Kuala Lumpur,<br> Malaysia.</p>
            </td>
         </tr>
         <tr>
            <td style='font-size: 10px;'>
               Cheque, Money Order or Bank Draft
            </td>
            <td style='font-size: 10px;'>
               Made payable to 'Chartered Institute of Islamic Finance Professionals'
            </td>
         </tr>
         <tr>
            <td style='font-size: 10px;'>
               Credit Card via PAYPAL
            </td>
            <td style='font-size: 10px;'>
               Please notify us via e-mail at membership@ciif-global.org should you wish to pay via PAYPAL payment link will then be sent to you accordingly
            </td>
         </tr>
      </table>
      <p style='padding-left: 10%;font-size: 10px;margin-top: 2%;'>Kindly ensure your payment is made within 30 days from the date of this invoice.</p>
      <p style='padding-left: 10%;font-size: 10px;'>You are also encouraged to update your profile and correspondence address by e-mailing your latest details to: membership@ciif-global.org</p>
      <p style='padding-left: 10%;font-size: 10px;'>NOTE: Please refer to the CIIF Terms & Conditions of Membership by visiting the membership page: ciif-global.org/web/membership
         <br/>
      
</body>
";


 $fieldValues['[Invoice]'] = $processing_fee;
 
        require_once '../library/dompdf/dompdf_config.inc.php';
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');
 $option = array(
            'content' => $processing_fee,
            'save' => false,
            'file_name' => $name.''.'invoice',
            'css' => '@page { margin: 20px 20px 20px 20px}
                        body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
                        table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
                        table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
                        table.tftable tr {background-color:#ffffff;}
                        table.tftable td {font-size:10px;padding: 5px;}
                        hr {
  color: #729ea5;
}'
                        ,

            'header' => ''
        );
 $generate = new Cms_Common();
        $pdf = $generate->generatePdf($option);
        // $slip = Cms_Curl::__(SMS_API . '/get/Registration/do/registrationSlip', $params);

        echo $pdf;
        exit();
    }


//      public function slipinvoiceAction()
//     {


//          $auth = Zend_Auth::getInstance()->getIdentity();
//         $userId = $auth->id;
//          $programDb = new App_Model_ProgramCategory();

//            $get_qualification = $this->programReg->get_account($userId);
//          $InvoiceNo =  'CIIF'.  date('YHims').$userId;
//            $Invoice = new App_Model_InvoiceMain();

//            $get_invoice = $Invoice->get_invoice($userId);
          
//            $name = $get_qualification[0]['firstname'].$get_qualification[0]['lastname'];
//         $get_member_id =$this->programReg->get_member_id($userId);
       
//            $total_amount = $get_qualification[0]['total_amount'];
           

          
//             $payment = $get_qualification[0]['payment_status'];
//             $memberId = $get_member_id[0]['mr_sp_id'];
           
//       $processing_fee="
// <body>
    
//    <table align='center' border=0 style='width:100%' >
//          <tr>
//           <td width='10%'>
//                &nbsp;
//             </td>
//             <td width='50%'>
//                <img src='./assets/images/ciifp_new_logo1.jpg' alt='ciifp logo' >
//             </td>

//             <td width='40%'>
//               <table>
//                  <tr><td style='text-align:right;'>KNOWLEDGE | INTEGRITY | SERVICE</td></tr>
//                                   <tr><td  style='text-align:right;'> 3rd Floor, Menara Takaful Malaysia,<br/>
//                            Jalan Sultan Sulaiman, 50000 Kuala Lumpur<br/>
//                            03-2273 2135 / 2136</td></tr>
//                            <tr>
//                            <td style='text-align:right;color:#459bca;font-size: 12px;'>www.ciif-global.org</td></tr>

                 
//               </table>
//             </td>
//         </tr>
               
              
//       </table>
//       <table align='center' width='80%' style='font-size: 12px;font-weight: bold;'>
//          <tr>
//             <td style='font-size: 12px;'>PRIVATE AND CONFIDENTIAL</td>
//             <td style='text-align: right;font-size: 12px;'>INVOICE NO</td>
//          </tr>

//          <tr>
//             <td>01/01/2018</td>
//             <td style='text-align: right;'>$InvoiceNo</td>
//          </tr>
//          <tr>
//          <td>
//          </td>
//          </tr>
//          <tr>
//             <td></td>
//             <td style='text-align: right;font-size: 12px;'>MEMBERSHIP NO:<br/>
//             $memberId</td>
//          </tr>
//       </table>
//       <p style='padding-left: 10%;margin-top: 2%;font-size: 10px;'>
//          Mr.Abdul Habi bin Md Rofiee<br>
//          D3-2-3, Pangsapuri Damai<br>
//          Jalan Tasik Raja Lumu U4/17<br>
//          Taman Subang Delima Seksyen U4<br>
//          40150 shah Alam<br>
//          Selangor<br>
//          Malaysia
//       </p>
//       <p style='padding-left: 10%;margin-top: 1%;width:80%;font-size: 10px;'>Dear Respected Member,<br><br>
//          <b><u>ANNUAL MEMBERSHIP FEE FOR YEAR 2018</u></b><br><br>
//          Congratulations once again on becoming a member of the CIIF. with reference to the membership confirmation letter sent to you earlier, we wish to inform you that your annual membership fee for the year of 2018 is now due. Details are as follows:
//       </p>
//       <table align='center' width='80%' style='font-size: 12px;font-weight: bold;'>
//         ";
//          $i=1;
// $total =0;
// foreach ($get_invoice as $key=>$value) {
    
//     $description = $value['description'];
//     $amount =$value['amount'];
//     $processing_fee .="
//         <tr>
//             <td>$description</td>
//             <td style='text-align: right;'>$amount</td>
//         </tr>";
// $total = $total +$amount;
// $i++;
// }

//      $processing_fee .= "<tr>
// <td><b>TOTAL</b></td>
// <td style='text-align:right'>RM $total</td>
// </tr></table>
//       <p><u style='padding-left: 10%;margin-top: 2%;font-size: 12px;'>Payment can be made via:</u></p>
//       <table  align='center' border='1' style='border-collapse: collapse;width: 80%;font-size: 12px;'>
//          <tr>
//             <td style='font-size: 10px;'>
//                Online Transfer or CDM
//             </td>
//             <td style='font-size: 10px;'>
//                Account Name: Chartered Institute of Islamic Finance Professionals<br/>
//                Bank: CIMB Islamic Bank Berhad<br/>
//                Account No: 8602458659
//             </td>
//             <td rowspan='3' style='font-size: 10px;' style='text-align: center;'>
//                E-mail receipt/payment slip to membership@ciif-global.org or mail to:<br/><br/>
//                <p>19th Floor,<br> Menera Takaful Malasia, Jalan <br> Sultan Sulaiman,<br> 50000 Kuala Lumpur,<br> Malaysia.</p>
//             </td>
//          </tr>
//          <tr>
//             <td style='font-size: 10px;'>
//                Cheque, Money Order or Bank Draft
//             </td>
//             <td style='font-size: 10px;'>
//                Made payable to 'Chartered Institute of Islamic Finance Professionals'
//             </td>
//          </tr>
//          <tr>
//             <td style='font-size: 10px;'>
//                Credit Card via PAYPAL
//             </td>
//             <td style='font-size: 10px;'>
//                Please notify us via e-mail at membership@ciif-global.org should you wish to pay via PAYPAL payment link will then be sent to you accordingly
//             </td>
//          </tr>
//       </table>
//       <p style='padding-left: 10%;font-size: 10px;margin-top: 2%;'>Kindly ensure your payment is made within 30 days from the date of this invoice.</p>
//       <p style='padding-left: 10%;font-size: 10px;'>You are also encouraged to update your profile and correspondence address by e-mailing your latest details to: membership@ciif-global.org</p>
//       <p style='padding-left: 10%;font-size: 10px;'>NOTE: Please refer to the CIIF Terms & Conditions of Membership by visiting the membership page: ciif-global.org/web/membership
//          <br/>
      
// </body>
// ";
//  $fieldValues['[Invoice]'] = $processing_fee;
 
//         require_once '../library/dompdf/dompdf_config.inc.php';
//         $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
//         $autoloader->pushAutoloader('DOMPDF_autoload');
//  $option = array(
//             'content' => $processing_fee,
//             'save' => false,
//             'file_name' => $name.''.'invoice',
//             'css' => '@page { margin: 20px 20px 20px 20px}
//                         body { font-family: Helvetica, Arial, sans-serif; font-size:10px }
//                         table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
//                         table.tftable th {font-size:10px;background-color:#acc8cc;border-width: 1px;padding: 5px;border-style: solid;border-color: #729ea5;text-align:left;}
//                         table.tftable tr {background-color:#ffffff;}
//                         table.tftable td {font-size:10px;padding: 5px;}
//                         hr {
//   color: #729ea5;
// }'
//                         ,

//             'header' => ''
//         );
//  $generate = new Cms_Common();
//         $pdf = $generate->generatePdf($option);
//         // $slip = Cms_Curl::__(SMS_API . '/get/Registration/do/registrationSlip', $params);

//         echo $pdf;
//         exit();
//     }

    public function slipInvoicdeAction()
    {

        $data = $this->_getParam('data');
        $data = Cms_Common::encrypt($data, false);

        if (Cms_Common::isJson($data)) {
            $data = json_decode($data, true);
        }

        $invoiceId = $data['invoiceId'];
        $cur_course = $data['curcourse'];
        $learningmode = $data['learningmode'];

        $this->_helper->layout->disableLayout();

        $this->_helper->viewRenderer->setNoRender();

        $params = array(

            'invoiceId'    => $invoiceId,
            'cur_course'   => $cur_course,
            'learningmode' => $learningmode
        );


        $file_name = 'IBFIM_' . $data['invoice_no'] . '.pdf';
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Type: application/force-download");
        header('Content-Disposition: attachment; filename=' . urlencode($file_name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        $slip = Cms_Curl::__(SMS_API . '/get/Finance/do/invoiceSlip', $params);

        echo($slip);
        exit();

    }

    public function slipReceiptAction()
    {
        $data = $this->_getParam('data');
        $data = Cms_Common::encrypt($data, false);

        if (Cms_Common::isJson($data)) {
            $data = json_decode($data, true);
        }


        $receiptId = $data['receiptId'];
        $fullname = $data['fullname'];
        $programName = $data['ProgramName'];
        $learningmode = $data['learningmode'];
        $rcp_no = $data['rcp_no'];

        $this->_helper->layout->disableLayout();

        $this->_helper->viewRenderer->setNoRender();

        $params = array(

            'receiptId'    => $receiptId,
            'fullname'     => $fullname,
            'programName'  => $programName,
            'learningmode' => $learningmode
        );


        $file_name = 'IBFIM_' . $rcp_no . '.pdf';
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Type: application/force-download");
        header('Content-Disposition: attachment; filename=' . urlencode($file_name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        $slip = Cms_Curl::__(SMS_API . '/get/Finance/do/receiptSlip', $params);

        echo $slip;
        exit();

    }

    public static function courseStatus($val)
    {
        switch ($val) {
            case 0:
                $newval = 'Pre-Registered';
                break;
            case 1:
                $newval = 'Registered';
                break;
            case 2:
                $newval = 'Drop';
                break;
            case 3:
                $newval = 'Withdraw';
                break;
            case 4:
                $newval = 'Repeat';
                break;
            case 5:
                $newval = 'Refer';
                break;
            case 9:
                $newval = 'Pre-Repeat';
                break;
            case 10:
                $newval = 'Completed';
                break;
        }

        return $newval;
    }

    public function graduationAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();

        $user = $userDb->getUser($this->auth->getIdentity()->id);

        $this->view->title = 'Graduation';

        //documents download
        $documentDb = new App_Model_Document();
        $this->view->documents = $documentDb->getDocument(1022); //Others

        //template note note
        $convocationDB = new App_Model_Convocation();
        $this->view->convonote = $convocationDB->getDocumentDownload(24);
        //pr( $this->view->convonote);exit;

        //Program Completion
        $awardDB = new App_Model_Invitation();
        $this->view->award = $awardDB->getGraduateInfo($user['external_id']);
        //pr($this->view->award);exit;

        $form = new App_Form_User();
        $form->populate($user);

        //unset
        $form->email->setRequired(false);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            if ($form->isValid($formData)) {
                //update
                $data = array(
                    'firstname'    => $formData['firstname'],
                    'lastname'     => $formData['lastname'],
                    'contactno'    => $formData['contactno'],
                    'modifiedby'   => $this->auth->getIdentity()->id,
                    'modifiedrole' => $this->auth->getIdentity()->role,
                    'modifieddate' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'timezone'     => $formData['timezone']
                );

                

                //upload file
                if ($user['photo'] != '' || isset($formData['deletephoto'])) {
                    @unlink($this->uploadDir . $user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir);


                if (!empty($files)) {
                    $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                    $photochanged = 1;
                }

                //if user is loggedin user
                if (($user['id'] == $this->auth->getIdentity()->id) && $photochanged) {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $userDb->update($data, array('id = ?' => $this->auth->getIdentity()->id));

                Cms_Common::notify('success', 'Information Updated');
                $this->redirect('/user/profile');
            }
        }

        $this->view->user = $user;
        $this->view->form = $form;
    }

    public function certificateOfAttendanceAction()
    {
        $params = array(
            'IdStudentRegistration' => $this->_getParam('IdStudentRegistration', 0),
            'IdSubject'             => $this->_getParam('IdSubject', 0),
            'schedule'              => $this->_getParam('schedule', 0),
        );
        $response = Cms_Curl::__(SMS_API . '/get/Registration/do/certificateOfAttendanceLMS', $params);

        if (isset($response['msg'])) {
            return Cms_Render::error($response['msg']);
        }

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $file_name = 'certificate_of_attendance.pdf';
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Type: application/force-download");
        header('Content-Disposition: attachment; filename=' . urlencode($file_name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');

        echo $response;
        exit;
    }

    public function accessExamMaterialAction()
    {
        $i = $this->getParam('i');
        $i = Cms_Common::encrypt($i, false);
        $i = json_decode($i, true);

        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();

        if (!isset($i['er_main_id']) || !$i['er_main_id']) {
            return Cms_Render::error('Invalid Examination Registration');
        }

        if (!isset($i['IdProgram']) || !$i['IdProgram']) {
            return Cms_Render::error('Invalid Curriculum');
        }

        $er_main_id = $i['er_main_id'];
        $IdProgram = $i['IdProgram'];
        $curriculum_id = null;

        if (!isset($identity->access_content)) {
            $identity->access_content = array();
        }

        if (!isset($identity->access_content[$IdProgram])) {
            $db2 = getDB2();
            $select = $db2->select()
                ->from(array('a' => 'exam_registration'), array('er_id', 'er_main_id', 'er_registration_type', 'exam_status'))
                ->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.student_id', array())
                ->join(array('c' => 'exam_setup_detail'), 'c.esd_id = a.examsetupdetail_id', array())
                ->join(array('d' => 'exam_setup'), 'd.es_id = c.es_id', array())
                ->join(array('e' => 'tbl_program'), 'e.IdProgram = d.es_idProgram', array('IdProgram', 'ProgramName', 'ProgramCode', 'exam_only'))
                ->where('a.er_main_id = ?', $er_main_id)
                ->where('b.sp_id = ?', $identity->sp_id)
                ->where('d.es_idProgram = ?', $IdProgram);
            $exam = $db2->fetchRow($select);

            if (!$exam) {
                return Cms_Render::error('You don\'t have a permission to view this course.');
            }

            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()->from(array('a' => 'curriculum'))->where('a.external_id = ?', $IdProgram)->where('a.parent_id != ?', 0);
            $curriculum = $db->fetchRow($select);

            if (!$curriculum) {
                return Cms_Render::error('Invalid Curriculum');
            }

            $identity->access_content[$IdProgram] = $curriculum_id = $curriculum['id'];
        }

        if (!isset($identity->access_content[$IdProgram])) {
            return Cms_Render::error('You don\'t have a permission to view this course.');
        }

        $curriculum_id = $identity->access_content[$IdProgram];

        $this->redirect('/courses/curriculum-content/cid/' . $curriculum_id);
    }

    public function activityAction()
    {
        Cms_Hooks::push('user.sidebar', self::userSidebar('home'));
        Cms_Common::expire();

        $userDb = new App_Model_User();
        $applicationDb = new App_Model_SmsApplication();
        $this->view->applicationDb = $applicationDb;

        $user = $userDb->getUser($this->auth->getIdentity()->id);

        //$details = $applicationDb->getDataBySpidRow($user['external_id']);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            $data = array(
                'spid'            => $user['external_id'],
                'cpd_learning_id' => $formData['cpd_learning_id'],
                'cpd_category_id' => $formData['cpd_category_id'],
                'cpd_activity_id' => $formData['cpd_activity_id'],
                'cpd_title'       => $formData['cpd_title'],
                'cpd_venue'       => $formData['cpd_venue'],
                'cpd_description' => $formData['cpd_description'],
                'cpd_provider'    => $formData['cpd_provider'],
                'cpd_duration'    => $formData['cpd_duration'],
                'date_started'    => date('Y-m-d', strtotime($formData['date_started'])),
                'date_completion' => date('Y-m-d', strtotime($formData['date_completion'])),
                'created_by'      => $user['external_id'],
                'created_at'      => date('Y-m-d H:i:s'),
                'cpd_status'      => 0

            );

            $cpd_id = $applicationDb->addDataActivity($data);
            $db = getDB2();

            //upload file


            $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

            if (!empty($files)) {
                $temp = array();
                foreach ($files as $file) {
                    $temp = $file;
                    $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));

                    // save document data (if any)
                    if (!empty($temp)) {
                        $dataFile = [];
                        $dataFile['sp_id'] = $user['external_id'];
                        $dataFile['cpd_id'] = $cpd_id;
                        $dataFile['doc_title'] = $formData['cpd_title'];
                        $dataFile['doc_filename'] = $temp['filename'];
                        $dataFile['doc_hashname'] = $temp['fileurl'];
                        $dataFile['doc_url'] = $this->uploadDir . '/' . $temp['fileurl'];
                        //$dataFile['doc_url'] = Cms_System_Links::dataFile($temp['fileurl'], false);
                        $dataFile['doc_location'] = $this->uploadDir . '/' . $temp['fileurl'];
                        $dataFile['doc_extension'] = $temp['ext'];
                        $dataFile['doc_filesize'] = $temp['filesize'];

                        /*echo '<pre>';
                        print_r($dataFile);
                        echo '</pre>';*/

                        //$applicationDb->addDataDoc($dataFile);

                        $uploadid = $db->insert('tbl_cpd_doc', $dataFile);
                    }

                }
            }


//exit;
            Cms_Common::notify('success', 'Activity Updated');
            $this->redirect('/user/cpd');

        }

        $this->view->user = $user;
    }

    public function transactionAction()
    {

        $auth = Zend_Auth::getInstance();
        $this->auth = $auth->getIdentity();

        Cms_Hooks::push('user.sidebar', self::userSidebar('transaction'));
        $userDb = new App_Model_User();

        $sp = isset($this->auth->external_id) ? $this->auth->external_id : 0;

        if (isset($this->auth->login_as)) {
            $sp = isset($this->auth->sp_id) ? $this->auth->sp_id : 0;
        } else {
            $sp = isset($this->auth->external_id) ? $this->auth->external_id : 0;
        }

        //get application
        $applicantTransactionDB = new App_Model_Records_ApplicantTransaction();
        $transaction = $applicantTransactionDB->getData($sp);
        $this->view->transaction = $transaction;

        $MembershipDB = new App_Model_Membership_Membership();
        $membership = $MembershipDB->getMembership($sp);
        $this->view->membership = $membership;

        if (isset($transaction[0]['IdProgram']) && $transaction[0]['IdProgram'] != '') {
            $curiculumDb = new App_Model_Curriculum();
            $this->view->curiculum = $curiculumDb->getDataByExternal($transaction[0]['IdProgram']);
        }


    }

    public function applyExemptionCpdAction()
    {

        Cms_Hooks::push('user.sidebar', self::userSidebar('home'));
        Cms_Common::expire();
        $db = getDB2();

        $userDb = new App_Model_User();
        $applicationDb = new App_Model_SmsApplication();
        $this->view->applicationDb = $applicationDb;

        $user = $userDb->getUser($this->auth->getIdentity()->id);
        $user_id = $this->auth->getIdentity()->id;

        //$details = $applicationDb->getDataBySpidRow($user['external_id']);
        $exemption = $applicationDb->getExemptionList();
        $this->view->exemptionList = $exemption;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            $mr = $applicationDb->getMemberRegByStudId($user_id);

            $data = array(
                'cpde_mr_id'       => $mr['mr_id'],
                'cpde_cpdc_id'     => $formData["exemption"],
                'cpde_description' => $formData['other_desc'],
                'cpde_type'        => 1, //Exemption
                'cpde_status'      => 1, //Applied
                'created_date'     => date('Y-m-d H:i:s'),
                'created_by'       => $user['external_id']
            );

            //tbl_cpd_exemptions
            $cpde_id = $applicationDb->addParams('tbl_cpd_exemptions', $data);
            // echo $cpde_id;
            // die();
            //upload file
            $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,xsls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');
            
            if (!empty($files)) {

                $temp = array();
                foreach ($files as $file) {
                    $temp = $file;
                    $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));

                }
            }

            // save document data (if any)
            if (!empty($temp)) {

               
                $dataFile = [];
                $dataFile['sp_id'] = $user_id;
                $dataFile['cpde_id'] = $cpde_id;
                $dataFile['doc_title'] = $formData['exemption'];
                $dataFile['doc_filename'] = $temp['filename'];
                $dataFile['doc_hashname'] = $temp['fileurl'];
                $dataFile['doc_url'] = DOCUMENT_URL . '/' . $temp['fileurl'];
                //$dataFile['doc_url'] = Cms_System_Links::dataFile($temp['fileurl'], false);
                $dataFile['doc_location'] = $this->uploadDir . '/' . $temp['fileurl'];
                $dataFile['doc_extension'] = $temp['ext'];
                $dataFile['doc_filesize'] = $temp['filesize'];

                /* echo '<pre>';
                 print_r($dataFile);
                 echo '</pre>';*/

                $uploadid = $db->insert('tbl_cpd_doc', $dataFile);

            }

//exit;
            Cms_Common::notify('success', 'CPD Exemption Updated');
            $this->redirect('/user/cpd');

        }

        $this->view->user = $user;


    }

    public function cpdDeclarationAction()
    {

        Cms_Hooks::push('user.sidebar', self::userSidebar('home'));
        Cms_Common::expire();
        $db = getDB2();

        $userDb = new App_Model_User();
        $applicationDb = new App_Model_SmsApplication();
        $this->view->applicationDb = $applicationDb;

        $user = $userDb->getUser($this->auth->getIdentity()->id);

        //$details = $applicationDb->getDataBySpidRow($user['external_id']);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $mr = $applicationDb->getMemberRegByStudId($user['external_id']);

            foreach ($formData['declaration'] as $selected => $value) {
                $data = array(
                    'cpde_mr_id'   => $mr['mr_id'],
                    'cpde_cpdc_id' => $value,
                    'cpde_type'    => 2, //Declaration
                    'cpde_status'  => 1, //Applied
                    'created_date' => date('Y-m-d H:i:s'),
                    'created_by'   => $user['external_id']
                );

                $checkExist = $applicationDb->getExemptionCheckExist($mr['mr_id'], $value, '2');
                if ($checkExist == 0) {
                    $cpde_id = $applicationDb->addParams('tbl_cpd_exemptions', $data);
                }

            }


            Cms_Common::notify('success', 'CPD Declaration Updated');
            $this->redirect('/user/cpd');

        }
//exit;
        $this->view->user = $user;


    }

    public function qualificationAction()
    {

        /*$email = new icampus_Function_Email_Email();

        $info = array(
            'template_id' => 1,
            'name' => 'name',
            'program_name' => 'name',
            'membership_name' => 'name',
            'title' => 'title'
        );

        $email->sendEmail($info);*/

        //error_reporting(E_ALL);
        //ini_set('display_errors', '1');
        //$tab = $this->_getParam('tab', 'qualification');
        // $this->view->tab = $tab;

        Cms_Hooks::push('user.sidebar', self::userSidebar('qualification'));

        $enrolDb = new App_Model_Enrol();
        $auth = Zend_Auth::getInstance()->getIdentity();
        $smsExamDB = new App_Model_SmsExam();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $currDb = new App_Model_Curriculum();
        $pepDB = new App_Model_Exam_ProgrammeExamPrerequisite();
        $erDB = new App_Model_Exam_ExamRegistration();
        $coursesDb = new App_Model_Courses();
        $userGroupDb = new App_Model_UserGroup();
        $userGroupDataDb = new App_Model_UserGroupData();
        $online = new App_Model_UserOnline();
        $smsExtendTimeDB = new App_Model_ExtendTime();

        $count = $online->getTotalUsers();//pr($count);
        $this->view->totalusers = $count;

        $welcome = isset($auth->new) && $auth->new == 1 ? 1 : 0;

        $myprograms = $enrolDb->getEnrolledProgramme($auth->id, false);
        $myexams = array();
        $learningMode_ = array("OL" => "Online", "FTF" => "Face to Face");
        $user_id = $auth->id;

        $totalenrolledprogram = count($myprograms);

        $totalenrolled = 0;
        $a = 0;

        if ($myprograms) {

            foreach ($myprograms as $a => $mypg) {

                $curriculumId = $mypg['id'];
                $IdProgram = $mypg['external_id'];
                $learningmode = ($mypg['learningmode'] == 'OL') ? 577 : 578;

                $mycourses = $enrolDb->getEnrolledCourses($auth->id, $curriculumId, false);
                $totalenrolled += count($mycourses);
                $myprograms[$a]['courses'] = $mycourses;

                foreach ($myprograms[$a]['courses'] as $course) {

                    $learningmode = $course['learningmode'];
                    $group_ = $userGroupDb->getGroupByName($course['code'] . ' - ' . $learningMode_[$learningmode]);

                    if (isset($group_) && !empty($group_)) {
                        $dataGroup = $userGroupDb->getDataByUserAndGroup($user_id, $group_['group_id']);

                        if (!$dataGroup) {
                            $dataGroup = array(
                                'group_id'     => $group_['group_id'],
                                'user_id'      => $user_id,
                                'active'       => 1,
                                'created_date' => new Zend_Db_Expr('NOW()'),
                                'created_by'   => 1
                            );
                            //$groupData = $userGroupDataDb->insertGroupData($dataGroup);
                        }
                    }
                }

                $examSetup = $smsExamDB->getExamByProgram($IdProgram, $learningmode);
                if ($examSetup) {
                    $myprograms[$a]['exam'] = $examSetup;
                }
            }
        }


        // enrolled
        $a = $a + 1;

        $myprograms[$a]['courses'] = $total = $enrolDb->getEnrolledCourses($auth->id, 0, false);
        $totalenrolled += @count($total);

        // completed
        $c = 0;

        $completed[$c]['courses'] = $totalcomplete = $enrolDb->getCompletedCourses($auth->id, 0);
        $totalcompleted = @count($totalcomplete);

        //exam
        $sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }

        if ($sp) {
            // login admin
            /*if ($auth->role =='administrator' || empty($sp)) {
                $profile = $sp;
            } else {
                $profile = $erDB->getProfile($sp);
            }

            $sp = $profile['sp_id'];*/

            //get membership info
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);
            $this->view->membership = $membership;

            //get active membership
            $active_membership = $MembershipDB->getActiveMembership($sp);

            //get application
            $applicantTransactionDB = new App_Model_Records_ApplicantTransaction();
            $transaction = $applicantTransactionDB->getData($sp);
            $this->view->transaction = $transaction;


            if (isset($transaction[0]['IdProgram']) && $transaction[0]['IdProgram'] != '') {
                $curiculumDb = new App_Model_Curriculum();
                $this->view->micuriculum = $curiculumDb->getDataByExternal($transaction[0]['IdProgram']);
            }

            //enrolment matrix
            $matrix = array(
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );

            $this->view->expired_flag = false;
            $this->view->qualification_pay = true;
            $this->view->qualification_register = true;

            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired

                    $this->view->expired_flag = true;
                    $matrix['expired_flag'] = true;
                }
            }

            if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                //require membership
                if ($transaction[0]['mr_status'] != 1604 && $transaction[0]['mr_status'] != 1607) { //approve & active
                    $this->view->qualification_pay = false;
                    $this->view->qualification_register = false;
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                    $matrix['qualification_pay_msg'] = "Membership is not active.";
                    $matrix['qualification_register_msg'] = "Membership is not active.";
                }
            } else {
                if ($transaction[0]['registration_type'] == "Qualification Only")
                    $matrix['qualification_register'] = false;
                $matrix['qualification_pay'] = false;
            }

            $this->view->matrix = $matrix;
            //end enrollment matrix
            //pr($matrix);

            //get qualification info
            $studentRegDb = new App_Model_Qualification_StudentRegistration();
            $this->view->qualification_registration = $studentRegDb->getDatabyStudentId($sp);


            if ($auth->role == 'administrator') {

            } else {


                $totalexam = $ExamregDB->getexamReg($sp);
                $totalexam = @count($totalexam);
                $myexams = $ExamregDB->getexamReg($sp);

                foreach ($myexams as $n => $ex) {
                    $myexams[$n]['examStatus'] = $ExamregDB->courseStatus($ex['er_registration_type']);
                }


                /*if($myApplication[0]["IdRegistrationType"] != 2){
                    foreach ($myApplication as $xp=>$appl) {
                        $memberDetl = $MembershipDB->getDataById("", $registrationId);
                        $myApplication[$xp]["membershipDetl"] = $memberDetl;

                        $memberProgramDetl = $MembershipDB->getMemberProgram($memberDetl['mr_idProgram']);
                        $myApplication[$xp]["memberProgramDetl"] = $memberProgramDetl;

                        $memberProgramDetl["IdProgram"];
                        $feeProgramDetl = $MembershipDB->getFeeProgram($memberProgramDetl["IdProgram"]);
                        $myApplication[$xp]["feeProgramDetl"] = $feeProgramDetl;

                        $totalmemberapplication = $memberDetl["totalmemberapp"];
                    }
                }*/

                $applicationDb = new App_Model_SmsApplication();
                $currDb = new App_Model_Curriculum();

                $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
                $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
                if ($myApplication) {
                    foreach ($myApplication as $index => $app) {
                        $smsprogramid = $app["IdProgram"];
                        if ($smsprogramid) {
                            $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                            $myApplication[$index]['cid'] = $curiculum['id'];
                        }
                    }
                }
                $totalapplication = count($myApplication);
            }


            // CPD DETAILS
            $applicationDb = new App_Model_SmsApplication();

        } else {
            $totalexam = 0;
            $totalapplication = 0;
            $myexams = array();
            $myApplication = array();
            $student_category_list = array();
        }
        //$this->view->student_category_list = $student_category_list;

        //orders pending
        $param = array('user_id = ?' => $auth->id, 'status = ?' => 'PENDING');
        $pendingorders = $this->orderDb->getOrders($param);

        $userDb = new App_Model_User();

        $user = $userDb->getUser($auth->id);

        //get all available exam setup
        //$examList = $smsExamDB->getAvailableExam();
        $examList = $smsExamDB->getAvailableExamByUser($sp);
        //$examList = $smsExamDB->getExamList($auth->sp_id);

        $smsStudentRegistrationDB = new App_Model_StudentRegistration();
        $studentRegDetl = $smsStudentRegistrationDB->getDataStudentRegistration($sp);
        $this->view->studentRegDetl = $studentRegDetl;

        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjectsDetail($sp);
        $this->view->StudentSubjectsDetail = $StudentSubjectsDetail;

        $studentExtendDetails = $smsExtendTimeDB->getDataStudReg($studentRegDetl["IdStudentRegistration"]);
        $this->view->studentExtendDetails = $studentExtendDetails;

        $arrayExamList = array();

        //get exam fee
        foreach ($examList as $a => $exam) {

            /*$programmeExamId = explode(',',  $exam['pe_id']);

            $examParam = array(
               // 'IdMode'=> 578, //by default FTF
                'IdProgram'=>$exam['IdProgram'],
                'IdProgramScheme'=>0,
                'Category'=>($auth->nationality == 'MY') ? 579 : 580,
                'ExamOption' => json_encode($programmeExamId),
                'view' => 1
            );

            $get = Cms_Curl::__(SMS_API.'/get/Finance/do/getAmountExam',$examParam);

            $examList[$a]['amount'] = '-';
            if(isset($get['totalFee'])) {
                $examList[$a]['amount'] = new Zend_Currency(array('value' => $get['totalFee'], 'symbol' => $get['fee']['cur_code']));
                //get lms id
                $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
                $examList[$a]['external_id'] = $curriculumData['id'];

                $arrayExamList[$a] = $examList[$a];

            }*/

            $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
            $examList[$a]['external_id'] = $curriculumData['id'];

            $arrayExamList[$a] = $examList[$a];

        }


        //get all qualification application
        /*prerequisites
        if($IdStudentRegistration) {

        $pass_prerequisite = true;
        foreach($prerequisites as $n => $pep) {
            $result = $pepDB->checkPrerequisite($pep['pep_id'], $student_profile['std_id'], $IdStudentRegistration);

            if(!$result['result']) {
                $pass_prerequisite = false;
            }
        }

        // $pass_prerequisite = true;

if(!$pass_prerequisite) {
    continue;
}*/

        // populate registered exams in an array
        $registeredExam = array();
        if (is_array($myexams)) {
            foreach ($myexams as $exam) {
                if (!in_array($exam['er_registration_type'], array(0, 1, 4, 9, 10))) {
                    continue;
                }
                $registeredExam[] = $exam['IdProgram'];
            }
        }

//        // CPD DETAILS
        $applicationDb = new App_Model_SmsApplication();
//
        if ($auth->external_id) {
            $cpd = $applicationDb->getCpdDetailsByStudentId($auth->external_id);
            $totalcpd = @count($cpd);

            $totalcpdhours = $applicationDb->countCpdDurationByStudentId($auth->external_id);
        }
//
        $student_category_list = $applicationDb->fnGetDefinations('CPD Learning');
        $this->view->student_category_list = $student_category_list;

        //views
        $this->view->registeredExam = $registeredExam;
        $this->view->user_id = $user_id;//echo $user_id."+++";
        $this->view->user = $user;
        $this->view->pendingorders = $pendingorders;
        $this->view->welcome = $welcome;

        $this->view->totalenrolled = $totalenrolled;
        $this->view->myprograms = $myprograms;
        $this->view->myexams = @$myexams;

        $this->view->totalcompleted = $totalcompleted;
        $this->view->totalexam = @$totalexam;
        $this->view->completed = $completed;
        $this->view->totalapplication = $totalapplication;
        $this->view->totalcpd = @$totalcpd;
        $this->view->totalcpdhours = $totalcpdhours;
        $this->view->external_id = $auth->external_id;

        //$this->view->totalmemberapplication = $totalmemberapplication;
        $this->view->exam = $arrayExamList;
        $this->view->myApplication = $myApplication;

        $userDb = new App_Model_User();

        $user = $userDb->getUser($this->auth->getIdentity()->id);

        $form = new App_Form_User();
        $form->populate($user);

        $userDocDb = new App_Model_UserDoc();
        $userDocuments = $userDocDb->getUserDoc($user['id']);

        $this->view->documents = $userDocuments;

        //unset
        $form->email->setRequired(false);
        $form->username->setRequired(false);
        $form->gender->setRequired(false);
        $form->estatus->setRequired(false);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($this->auth->getIdentity()->login_as) {//if admin
                $modifiedby = $this->auth->getIdentity()->login_as;
                $modifiedrole = "superadmin";
            } else {
                $modifiedby = $this->auth->getIdentity()->id;
                $modifiedrole = $this->auth->getIdentity()->role;
            }
            if ($form->isValid($formData)) {
                //update
                $data = array(
                    'firstname'    => $formData['firstname'],
                    'lastname'     => $formData['lastname'],
                    'contactno'    => $formData['contactno'],
                    'estatus'      => (isset($formData['estatus']) ? $formData['estatus'] : null),
//                    'modifiedby' => $this->auth->getIdentity()->id,
                    'modifiedby'   => $modifiedby,
//                    'modifiedrole' => $this->auth->getIdentity()->role,
                    'modifiedrole' => $modifiedrole,
                    'modifieddate' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'timezone'     => $formData['timezone'],
                    'address1'     => trim($formData['address1']),
                    'postcode'     => strtoupper($formData['postcode']),
                    'country'      => $formData['country']
                );

                $_SESSION['Zend_Auth']['storage']->firstname = $formData['firstname'];
                $_SESSION['Zend_Auth']['storage']->lastname = $formData['lastname'];

                if (isset($formData['gender']) && $formData['gender']) {
                    $data['gender'] = $formData['gender'];
                }

                if (isset($formData['birthdate']) && Cms_Common::validateDate($formData['birthdate'], 'd-m-Y')) {
                    $data['birthdate'] = date('Y-m-d', strtotime($formData['birthdate']));
                    $_SESSION['Zend_Auth']['storage']->birthdate = $data['birthdate'];
                }

                if (isset($formData['state']) && $formData['state']) {
                    $data['state'] = $formData['state'];
                }

                if (isset($formData['city']) && $formData['city']) {
                    $data['city'] = $formData['city'];
                }
                $data['postcode'] = $formData['postcode'];

                //upload file
                if ($user['photo'] != '' || isset($formData['deletephoto'])) {
                    @unlink($this->uploadDir . $user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir);


                if (!empty($files)) {
                    $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if (!empty($files)) {
                    $temp = array();
                    foreach ($files as $file) {
                        $extension = array("jpg", "jpeg", "png", "gif");

                        if (in_array(substr(strrchr($file['filename'], '.'), 1), $extension)) {
                            $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                            $photochanged = 1;
                        } else {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }

                // save document data (if any)
                if (!empty($temp)) {
                    $dataFile['user_id'] = $user['id'];
                    $dataFile['doc_title'] = $formData['title'];
                    $dataFile['doc_filename'] = $temp['filename'];
                    $dataFile['doc_hashname'] = $temp['fileurl'];
                    $dataFile['doc_url'] = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location'] = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension'] = $temp['ext'];
                    $dataFile['doc_filesize'] = $temp['filesize'];

                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if (($user['id'] == $this->auth->getIdentity()->id) && $photochanged) {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $userDb->update($data, array('id = ?' => $this->auth->getIdentity()->id));

                $response = Cms_Curl::__(SMS_API . '/get/Registration/do/syncProfileToSms', array('external_id' => $user['external_id']));

                Cms_Common::notify('success', 'Information Updated');
                $this->redirect('/dashboard');
            } else {

            }
        }

        $announcements = $this->getannouncement();
        $this->view->announcements = $announcements;

        $event = $this->getcalendar();
        $this->view->event = $event;

        //$this->view->documents = $userDocuments;
        $this->view->user = $user;
        $this->view->form = $form;
    }

    public function examAction()
    {

        /*$email = new icampus_Function_Email_Email();

        $info = array(
            'template_id' => 1,
            'name' => 'name',
            'program_name' => 'name',
            'membership_name' => 'name',
            'title' => 'title'
        );

        $email->sendEmail($info);*/

        //error_reporting(E_ALL);
        //ini_set('display_errors', '1');
        $tab = $this->_getParam('tab', 'exam');
        $this->view->tab = $tab;

        Cms_Hooks::push('user.sidebar', self::userSidebar('exam'));

        $enrolDb = new App_Model_Enrol();
        $auth = Zend_Auth::getInstance()->getIdentity();
        $smsExamDB = new App_Model_SmsExam();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $currDb = new App_Model_Curriculum();
        $pepDB = new App_Model_Exam_ProgrammeExamPrerequisite();
        $erDB = new App_Model_Exam_ExamRegistration();
        $coursesDb = new App_Model_Courses();
        $userGroupDb = new App_Model_UserGroup();
        $userGroupDataDb = new App_Model_UserGroupData();
        $online = new App_Model_UserOnline();
        $smsExtendTimeDB = new App_Model_ExtendTime();

        $count = $online->getTotalUsers();//pr($count);
        $this->view->totalusers = $count;

        $welcome = isset($auth->new) && $auth->new == 1 ? 1 : 0;

        $myprograms = $enrolDb->getEnrolledProgramme($auth->id, false);
        $myexams = array();
        $learningMode_ = array("OL" => "Online", "FTF" => "Face to Face");
        $user_id = $auth->id;

        $totalenrolledprogram = count($myprograms);

        $totalenrolled = 0;
        $a = 0;

        if ($myprograms) {

            foreach ($myprograms as $a => $mypg) {

                $curriculumId = $mypg['id'];
                $IdProgram = $mypg['external_id'];
                $learningmode = ($mypg['learningmode'] == 'OL') ? 577 : 578;

                $mycourses = $enrolDb->getEnrolledCourses($auth->id, $curriculumId, false);
                $totalenrolled += count($mycourses);
                $myprograms[$a]['courses'] = $mycourses;

                foreach ($myprograms[$a]['courses'] as $course) {

                    $learningmode = $course['learningmode'];
                    $group_ = $userGroupDb->getGroupByName($course['code'] . ' - ' . $learningMode_[$learningmode]);

                    if (isset($group_) && !empty($group_)) {
                        $dataGroup = $userGroupDb->getDataByUserAndGroup($user_id, $group_['group_id']);

                        if (!$dataGroup) {
                            $dataGroup = array(
                                'group_id'     => $group_['group_id'],
                                'user_id'      => $user_id,
                                'active'       => 1,
                                'created_date' => new Zend_Db_Expr('NOW()'),
                                'created_by'   => 1
                            );
                            //$groupData = $userGroupDataDb->insertGroupData($dataGroup);
                        }
                    }
                }

                $examSetup = $smsExamDB->getExamByProgram($IdProgram, $learningmode);
                if ($examSetup) {
                    $myprograms[$a]['exam'] = $examSetup;
                }
            }
        }


        // enrolled
        $a = $a + 1;

        $myprograms[$a]['courses'] = $total = $enrolDb->getEnrolledCourses($auth->id, 0, false);
        $totalenrolled += @count($total);

        // completed
        $c = 0;

        $completed[$c]['courses'] = $totalcomplete = $enrolDb->getCompletedCourses($auth->id, 0);
        $totalcompleted = @count($totalcomplete);

        //exam
        $sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }

        if ($sp) {
            // login admin
            /*if ($auth->role =='administrator' || empty($sp)) {
                $profile = $sp;
            } else {
                $profile = $erDB->getProfile($sp);
            }

            $sp = $profile['sp_id'];*/

            //get membership info
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);
            $this->view->membership = $membership;

            //get active membership
            $active_membership = $MembershipDB->getActiveMembership($sp);

            //get application
            $applicantTransactionDB = new App_Model_Records_ApplicantTransaction();
            $transaction = $applicantTransactionDB->getData($sp);
            $this->view->transaction = $transaction;


            if (isset($transaction[0]['IdProgram']) && $transaction[0]['IdProgram'] != '') {
                $curiculumDb = new App_Model_Curriculum();
                $this->view->micuriculum = $curiculumDb->getDataByExternal($transaction[0]['IdProgram']);
            }

            //enrolment matrix
            $matrix = array(
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );

            $this->view->expired_flag = false;
            $this->view->qualification_pay = true;
            $this->view->qualification_register = true;

            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired

                    $this->view->expired_flag = true;
                    $matrix['expired_flag'] = true;
                }
            }

            if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                //require membership
                if ($transaction[0]['mr_status'] != 1604 && $transaction[0]['mr_status'] != 1607) { //approve & active
                    $this->view->qualification_pay = false;
                    $this->view->qualification_register = false;
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                    $matrix['qualification_pay_msg'] = "Membership is not active.";
                    $matrix['qualification_register_msg'] = "Membership is not active.";
                }
            } else {
                // if ($transaction[0]['registration_type'] == "Qualification Only")
                //     $matrix['qualification_register'] = false;
                // $matrix['qualification_pay'] = false;
            }

            $this->view->matrix = $matrix;
            //end enrollment matrix
            //pr($matrix);

            //get qualification info
            $studentRegDb = new App_Model_Qualification_StudentRegistration();
            $this->view->qualification_registration = $studentRegDb->getDatabyStudentId($sp);


            if ($auth->role == 'administrator') {

            } else {


                $totalexam = $ExamregDB->getexamReg($sp);
                $totalexam = @count($totalexam);
                $myexams = $ExamregDB->getexamReg($sp);

                pr($myexams);
                foreach ($myexams as $n => $ex) {
                    $myexams[$n]['examStatus'] = $ExamregDB->courseStatus($ex['er_registration_type']);
                }


                /*if($myApplication[0]["IdRegistrationType"] != 2){
                    foreach ($myApplication as $xp=>$appl) {
                        $memberDetl = $MembershipDB->getDataById("", $registrationId);
                        $myApplication[$xp]["membershipDetl"] = $memberDetl;

                        $memberProgramDetl = $MembershipDB->getMemberProgram($memberDetl['mr_idProgram']);
                        $myApplication[$xp]["memberProgramDetl"] = $memberProgramDetl;

                        $memberProgramDetl["IdProgram"];
                        $feeProgramDetl = $MembershipDB->getFeeProgram($memberProgramDetl["IdProgram"]);
                        $myApplication[$xp]["feeProgramDetl"] = $feeProgramDetl;

                        $totalmemberapplication = $memberDetl["totalmemberapp"];
                    }
                }*/

                $applicationDb = new App_Model_SmsApplication();
                $currDb = new App_Model_Curriculum();

                $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
                $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
                if ($myApplication) {
                    foreach ($myApplication as $index => $app) {
                        $smsprogramid = $app["IdProgram"];
                        if ($smsprogramid) {
                            $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                            $myApplication[$index]['cid'] = $curiculum['id'];
                        }
                    }
                }
                $totalapplication = count($myApplication);
            }


            // CPD DETAILS
            $applicationDb = new App_Model_SmsApplication();

        } else {
            $totalexam = 0;
            $totalapplication = 0;
            $myexams = array();
            $myApplication = array();
            $student_category_list = array();
        }
        //$this->view->student_category_list = $student_category_list;

        //orders pending
        $param = array('user_id = ?' => $auth->id, 'status = ?' => 'PENDING');
        $pendingorders = $this->orderDb->getOrders($param);

        $userDb = new App_Model_User();

        $user = $userDb->getUser($auth->id);

        //get all available exam setup
        //$examList = $smsExamDB->getAvailableExam();
        $examList = $smsExamDB->getAvailableExamByUser($sp);
        //$examList = $smsExamDB->getExamList($auth->sp_id);

        $smsStudentRegistrationDB = new App_Model_StudentRegistration();
        $studentRegDetl = $smsStudentRegistrationDB->getDataStudentRegistration($sp);
        $this->view->studentRegDetl = $studentRegDetl;

        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjectsDetail($sp);

        if (count($StudentSubjectsDetail) > 0) {
            foreach ($StudentSubjectsDetail as $index => $regsubject) {

                $schedule = $smsStudentRegistrationDB->getScheduleExam($regsubject['IdProgram'], $regsubject['IdSubject']);

                //get exam center
                if (count($StudentSubjectsDetail) > 0) {
                    foreach ($schedule as $i => $sc) {
                        //get center by slot id
                        $exam_center = $smsStudentRegistrationDB->getExamCenter($sc['sl_id']);
                        $schedule[$i]['center'] = $exam_center;
                    }
                }

                $StudentSubjectsDetail[$index]['schedule'] = $schedule;
            }
        }

        $this->view->StudentSubjectsDetail = $StudentSubjectsDetail;

        $studentExtendDetails = $smsExtendTimeDB->getDataStudReg($studentRegDetl["IdStudentRegistration"]);
        $this->view->studentExtendDetails = $studentExtendDetails;

        $arrayExamList = array();

        //get exam fee
        foreach ($examList as $a => $exam) {

            /*$programmeExamId = explode(',',  $exam['pe_id']);

            $examParam = array(
               // 'IdMode'=> 578, //by default FTF
                'IdProgram'=>$exam['IdProgram'],
                'IdProgramScheme'=>0,
                'Category'=>($auth->nationality == 'MY') ? 579 : 580,
                'ExamOption' => json_encode($programmeExamId),
                'view' => 1
            );

            $get = Cms_Curl::__(SMS_API.'/get/Finance/do/getAmountExam',$examParam);

            $examList[$a]['amount'] = '-';
            if(isset($get['totalFee'])) {
                $examList[$a]['amount'] = new Zend_Currency(array('value' => $get['totalFee'], 'symbol' => $get['fee']['cur_code']));
                //get lms id
                $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
                $examList[$a]['external_id'] = $curriculumData['id'];

                $arrayExamList[$a] = $examList[$a];

            }*/

            $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
            $examList[$a]['external_id'] = $curriculumData['id'];

            $arrayExamList[$a] = $examList[$a];

        }


        //get all qualification application
        /*prerequisites
        if($IdStudentRegistration) {

        $pass_prerequisite = true;
        foreach($prerequisites as $n => $pep) {
            $result = $pepDB->checkPrerequisite($pep['pep_id'], $student_profile['std_id'], $IdStudentRegistration);

            if(!$result['result']) {
                $pass_prerequisite = false;
            }
        }

        // $pass_prerequisite = true;

if(!$pass_prerequisite) {
    continue;
}*/

        // populate registered exams in an array
        $registeredExam = array();
        if (is_array($myexams)) {
            foreach ($myexams as $exam) {
                if (!in_array($exam['er_registration_type'], array(0, 1, 4, 9, 10))) {
                    continue;
                }
                $registeredExam[] = $exam['IdProgram'];
            }
        }

//        // CPD DETAILS
        $applicationDb = new App_Model_SmsApplication();
//
        if ($auth->external_id) {
            $cpd = $applicationDb->getCpdDetailsByStudentId($auth->external_id);
            $totalcpd = @count($cpd);

            $totalcpdhours = $applicationDb->countCpdDurationByStudentId($auth->external_id);
        }
//
        $student_category_list = $applicationDb->fnGetDefinations('CPD Learning');
        $this->view->student_category_list = $student_category_list;

        //views
        $this->view->registeredExam = $registeredExam;
        $this->view->user_id = $user_id;//echo $user_id."+++";
        $this->view->user = $user;
        $this->view->pendingorders = $pendingorders;
        $this->view->welcome = $welcome;

        $this->view->totalenrolled = $totalenrolled;
        $this->view->myprograms = $myprograms;
        $this->view->myexams = @$myexams;

        $this->view->totalcompleted = $totalcompleted;
        $this->view->totalexam = @$totalexam;
        $this->view->completed = $completed;
        $this->view->totalapplication = $totalapplication;
        $this->view->totalcpd = @$totalcpd;
        $this->view->totalcpdhours = $totalcpdhours;
        $this->view->external_id = $auth->external_id;

        //$this->view->totalmemberapplication = $totalmemberapplication;
        $this->view->exam = $arrayExamList;
        $this->view->myApplication = $myApplication;

        pr($myprograms);
        $userDb = new App_Model_User();

        $user = $userDb->getUser($this->auth->getIdentity()->id);

        $form = new App_Form_User();
        $form->populate($user);

        $userDocDb = new App_Model_UserDoc();
        $userDocuments = $userDocDb->getUserDoc($user['id']);

        $this->view->documents = $userDocuments;

        //unset
        $form->email->setRequired(false);
        $form->username->setRequired(false);
        $form->gender->setRequired(false);
        $form->estatus->setRequired(false);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($this->auth->getIdentity()->login_as) {//if admin
                $modifiedby = $this->auth->getIdentity()->login_as;
                $modifiedrole = "superadmin";
            } else {
                $modifiedby = $this->auth->getIdentity()->id;
                $modifiedrole = $this->auth->getIdentity()->role;
            }
            if ($form->isValid($formData)) {
                //update
                $data = array(
                    'firstname'    => $formData['firstname'],
                    'lastname'     => $formData['lastname'],
                    'contactno'    => $formData['contactno'],
                    'estatus'      => (isset($formData['estatus']) ? $formData['estatus'] : null),
//                    'modifiedby' => $this->auth->getIdentity()->id,
                    'modifiedby'   => $modifiedby,
//                    'modifiedrole' => $this->auth->getIdentity()->role,
                    'modifiedrole' => $modifiedrole,
                    'modifieddate' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'timezone'     => $formData['timezone'],
                    'address1'     => trim($formData['address1']),
                    'postcode'     => strtoupper($formData['postcode']),
                    'country'      => $formData['country']
                );

                $_SESSION['Zend_Auth']['storage']->firstname = $formData['firstname'];
                $_SESSION['Zend_Auth']['storage']->lastname = $formData['lastname'];

                if (isset($formData['gender']) && $formData['gender']) {
                    $data['gender'] = $formData['gender'];
                }

                if (isset($formData['birthdate']) && Cms_Common::validateDate($formData['birthdate'], 'd-m-Y')) {
                    $data['birthdate'] = date('Y-m-d', strtotime($formData['birthdate']));
                    $_SESSION['Zend_Auth']['storage']->birthdate = $data['birthdate'];
                }

                if (isset($formData['state']) && $formData['state']) {
                    $data['state'] = $formData['state'];
                }

                if (isset($formData['city']) && $formData['city']) {
                    $data['city'] = $formData['city'];
                }
                $data['postcode'] = $formData['postcode'];

                //upload file
                if ($user['photo'] != '' || isset($formData['deletephoto'])) {
                    @unlink($this->uploadDir . $user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir);


                if (!empty($files)) {
                    $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if (!empty($files)) {
                    $temp = array();
                    foreach ($files as $file) {
                        $extension = array("jpg", "jpeg", "png", "gif");

                        if (in_array(substr(strrchr($file['filename'], '.'), 1), $extension)) {
                            $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                            $photochanged = 1;
                        } else {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }

                // save document data (if any)
                if (!empty($temp)) {
                    $dataFile['user_id'] = $user['id'];
                    $dataFile['doc_title'] = $formData['title'];
                    $dataFile['doc_filename'] = $temp['filename'];
                    $dataFile['doc_hashname'] = $temp['fileurl'];
                    $dataFile['doc_url'] = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location'] = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension'] = $temp['ext'];
                    $dataFile['doc_filesize'] = $temp['filesize'];

                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if (($user['id'] == $this->auth->getIdentity()->id) && $photochanged) {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $userDb->update($data, array('id = ?' => $this->auth->getIdentity()->id));

                $response = Cms_Curl::__(SMS_API . '/get/Registration/do/syncProfileToSms', array('external_id' => $user['external_id']));

                Cms_Common::notify('success', 'Information Updated');
                $this->redirect('/dashboard');
            } else {

            }
        }

        $announcements = $this->getannouncement();
        $this->view->announcements = $announcements;

        $event = $this->getcalendar();
        $this->view->event = $event;

        //$this->view->documents = $userDocuments;
        $this->view->user = $user;
        $this->view->form = $form;
    }

   public function cpdAction()
    
    {
      
        Cms_Hooks::push('user.sidebar', self::userSidebar('dashboard'));
        $auth = Zend_Auth::getInstance()->getIdentity();

       
        $user_id = $auth->id;
         echo $user_id;
        $get_expiry = $this->programDb->get_program_details($user_id);
      

        $tab = $this->_getParam('tab', 'cpd');
        $this->view->tab = $tab;

        Cms_Hooks::push('user.sidebar', self::userSidebar('cpd'));

        $enrolDb = new App_Model_Enrol();
        $auth = Zend_Auth::getInstance()->getIdentity();
        $smsExamDB = new App_Model_SmsExam();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $currDb = new App_Model_Curriculum();
        $pepDB = new App_Model_Exam_ProgrammeExamPrerequisite();
        $erDB = new App_Model_Exam_ExamRegistration();
        $coursesDb = new App_Model_Courses();
        $userGroupDb = new App_Model_UserGroup();
        $userGroupDataDb = new App_Model_UserGroupData();
        $online = new App_Model_UserOnline();
        $smsExtendTimeDB = new App_Model_ExtendTime();

        $count = $online->getTotalUsers();//pr($count);
        // echo "<pre>";
        // print_r($count);
        // exit();
        $this->view->totalusers = $count;

        $welcome = isset($auth->new) && $auth->new == 1 ? 1 : 0;

        $myprograms = $enrolDb->getEnrolledProgramme($auth->id, false);

        // echo "<pre>";
        // print_r($myprograms);
        // exit;
       
        $myexams = array();
        $learningMode_ = array("OL" => "Online", "FTF" => "Face to Face");
        $user_id = $auth->id;

        $totalenrolledprogram = count($myprograms);

        $totalenrolled = 0;
        $a = 0;

        if ($myprograms) {

            foreach ($myprograms as $a => $mypg) {

                $curriculumId = $mypg['id'];
                $IdProgram = $mypg['external_id'];
                $learningmode = ($mypg['learningmode'] == 'OL') ? 577 : 578;

                $mycourses = $enrolDb->getEnrolledCourses($auth->id, $curriculumId, false);
                $totalenrolled += count($mycourses);
                $myprograms[$a]['courses'] = $mycourses;

                foreach ($myprograms[$a]['courses'] as $course) {

                    $learningmode = $course['learningmode'];
                    $group_ = $userGroupDb->getGroupByName($course['code'] . ' - ' . $learningMode_[$learningmode]);

                    if (isset($group_) && !empty($group_)) {
                        $dataGroup = $userGroupDb->getDataByUserAndGroup($user_id, $group_['group_id']);

                        if (!$dataGroup) {
                            $dataGroup = array(
                                'group_id'     => $group_['group_id'],
                                'user_id'      => $user_id,
                                'active'       => 1,
                                'created_date' => new Zend_Db_Expr('NOW()'),
                                'created_by'   => 1
                            );
                            //$groupData = $userGroupDataDb->insertGroupData($dataGroup);
                        }
                    }
                }

                $examSetup = $smsExamDB->getExamByProgram($IdProgram, $learningmode);
                if ($examSetup) {
                    $myprograms[$a]['exam'] = $examSetup;
                }
            }
        }


        // enrolled
        $a = $a + 1;

        $myprograms[$a]['courses'] = $total = $enrolDb->getEnrolledCourses($auth->id, 0, false);
        $totalenrolled += @count($total);

        // completed
        $c = 0;

        $completed[$c]['courses'] = $totalcomplete = $enrolDb->getCompletedCourses($auth->id, 0);
        $totalcompleted = @count($totalcomplete);


        //exam
        $sp = isset($auth->external_id) ? $auth->external_id : 0;

        // echo "<pre>";
        // print_r($sp);
        // exit;

        //echo "<pre>".pr($this->auth->getIdentity());

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as
        )) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }

        
        if ($sp)
        {


            // login admin
            /*if ($auth->role =='administrator' || empty($sp)) {
                $profile = $sp;
            } else {
                $profile = $erDB->getProfile($sp);
            }

            $sp = $profile['sp_id'];*/

            //get membership infos
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);


            $this->view->membership = $membership;

            //get active membership
            $active_membership = $MembershipDB->getActiveMembership($sp);

            //get application
            $applicantTransactionDB = new App_Model_Records_ApplicantTransaction();
            $transaction = $applicantTransactionDB->getData($sp);
            $this->view->transaction = $transaction;


            if (isset($transaction[0]['IdProgram']) && $transaction[0]['IdProgram'] != '') {
                $curiculumDb = new App_Model_Curriculum();
                $this->view->micuriculum = $curiculumDb->getDataByExternal($transaction[0]['IdProgram']);
            }

            //enrolment matrix
            $matrix = array(
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );

            $this->view->expired_flag = false;
            $this->view->qualification_pay = true;
            $this->view->qualification_register = true;

            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired

                    $this->view->expired_flag = true;
                    $matrix['expired_flag'] = true;
                }
            }

            if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                //require membership
                if ($transaction[0]['mr_status'] != 1604 && $transaction[0]['mr_status'] != 1607) { //approve & active
                    $this->view->qualification_pay = false;
                    $this->view->qualification_register = false;
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                    $matrix['qualification_pay_msg'] = "Membership is not active.";
                    $matrix['qualification_register_msg'] = "Membership is not active.";
                }
            } else {
                /*if ($transaction[0]['registration_type'] == "Qualification Only")
                    $matrix['qualification_register'] = false;
                $matrix['qualification_pay'] = false;
            */
            }

            $this->view->matrix = $matrix;
            //end enrollment matrix
            //pr($matrix);

            //get qualification info
            $studentRegDb = new App_Model_Qualification_StudentRegistration();
            $this->view->qualification_registration = $studentRegDb->getDatabyStudentId($sp);


            if ($auth->role == 'administrator') {

            } else {


                $totalexam = $ExamregDB->getexamReg($sp);
                $totalexam = @count($totalexam);
                $myexams = $ExamregDB->getexamReg($sp);

                foreach ($myexams as $n => $ex) {
                    $myexams[$n]['examStatus'] = $ExamregDB->courseStatus($ex['er_registration_type']);
                }


                /*if($myApplication[0]["IdRegistrationType"] != 2){
                    foreach ($myApplication as $xp=>$appl) {
                        $memberDetl = $MembershipDB->getDataById("", $registrationId);
                        $myApplication[$xp]["membershipDetl"] = $memberDetl;

                        $memberProgramDetl = $MembershipDB->getMemberProgram($memberDetl['mr_idProgram']);
                        $myApplication[$xp]["memberProgramDetl"] = $memberProgramDetl;

                        $memberProgramDetl["IdProgram"];
                        $feeProgramDetl = $MembershipDB->getFeeProgram($memberProgramDetl["IdProgram"]);
                        $myApplication[$xp]["feeProgramDetl"] = $feeProgramDetl;

                        $totalmemberapplication = $memberDetl["totalmemberapp"];
                    }
                }*/

                $applicationDb = new App_Model_SmsApplication();
                $currDb = new App_Model_Curriculum();

                $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
                $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
                if ($myApplication) {
                    foreach ($myApplication as $index => $app) {
                        $smsprogramid = $app["IdProgram"];
                        if ($smsprogramid) {
                            $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                            $myApplication[$index]['cid'] = $curiculum['id'];
                        }
                    }
                }
                $totalapplication = count($myApplication);
            }


            // CPD DETAILS
            $applicationDb = new App_Model_SmsApplication();

        }
         else {
            $totalexam = 0;
            $totalapplication = 0;
            $myexams = array();
            $myApplication = array();
            $student_category_list = array();
        }
        //$this->view->student_category_list = $student_category_list;

        //orders pending
        $param = array('user_id = ?' => $auth->id, 'status = ?' => 'PENDING');
        $pendingorders = $this->orderDb->getOrders($param);



        $userDb = new App_Model_User();

        $user = $userDb->getUser($auth->id);

            



        //get all available exam setup
        //$examList = $smsExamDB->getAvailableExam();
        $examList = $smsExamDB->getAvailableExamByUser($sp);
        //$examList = $smsExamDB->getExamList($auth->sp_id);

            


        $smsStudentRegistrationDB = new App_Model_StudentRegistration();
        $studentRegDetl = $smsStudentRegistrationDB->getDataStudentRegistration($sp);
        $this->view->studentRegDetl = $studentRegDetl;

        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjectsDetail($sp);


        $this->view->StudentSubjectsDetail = $StudentSubjectsDetail;



        $studentExtendDetails = $smsExtendTimeDB->getDataStudReg($studentRegDetl["IdStudentRegistration"]);



        $this->view->studentExtendDetails = $studentExtendDetails;

        $arrayExamList = array();

        //get exam fee
        foreach ($examList as $a => $exam) {

            /*$programmeExamId = explode(',',  $exam['pe_id']);

            $examParam = array(
               // 'IdMode'=> 578, //by default FTF
                'IdProgram'=>$exam['IdProgram'],
                'IdProgramScheme'=>0,
                'Category'=>($auth->nationality == 'MY') ? 579 : 580,
                'ExamOption' => json_encode($programmeExamId),
                'view' => 1
            );

            $get = Cms_Curl::__(SMS_API.'/get/Finance/do/getAmountExam',$examParam);

            $examList[$a]['amount'] = '-';
            if(isset($get['totalFee'])) {
                $examList[$a]['amount'] = new Zend_Currency(array('value' => $get['totalFee'], 'symbol' => $get['fee']['cur_code']));
                //get lms id
                $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
                $examList[$a]['external_id'] = $curriculumData['id'];

                $arrayExamList[$a] = $examList[$a];

            }*/

            $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
            $examList[$a]['external_id'] = $curriculumData['id'];

            $arrayExamList[$a] = $examList[$a];

        }


        //get all qualification application
        /*prerequisites
        if($IdStudentRegistration) {

        $pass_prerequisite = true;
        foreach($prerequisites as $n => $pep) {
            $result = $pepDB->checkPrerequisite($pep['pep_id'], $student_profile['std_id'], $IdStudentRegistration);

            if(!$result['result']) {
                $pass_prerequisite = false;
            }
        }

        // $pass_prerequisite = true;

            if(!$pass_prerequisite) {
                continue;
            }*/

        // populate registered exams in an array
        $registeredExam = array();
        if (is_array($myexams)) {
            foreach ($myexams as $exam) {
                if (!in_array($exam['er_registration_type'], array(0, 1, 4, 9, 10))) {
                    continue;
                }
                $registeredExam[] = $exam['IdProgram'];
            }
        }

//        // CPD DETAILS
        $applicationDb = new App_Model_SmsApplication();


//
        if ($auth->external_id) {
            $cpd = $applicationDb->getCpdDetailsByStudentId($auth->external_id);
            $totalcpd = @count($cpd);

            $totalcpdhours = $applicationDb->countCpdDurationByStudentId($auth->external_id);
        }
//
        $student_category_list = $applicationDb->fnGetDefinations('CPD Learning');


        $this->view->student_category_list = $student_category_list;

        //views
        $this->view->registeredExam = $registeredExam;
        $this->view->user_id = $user_id;//echo $user_id."+++";
        $this->view->user = $user;
        $this->view->pendingorders = $pendingorders;
        $this->view->welcome = $welcome;

        $this->view->totalenrolled = $totalenrolled;
        $this->view->myprograms = $myprograms;
        $this->view->myexams = @$myexams;

        $this->view->totalcompleted = $totalcompleted;
        $this->view->totalexam = @$totalexam;
        $this->view->completed = $completed;
        $this->view->totalapplication = $totalapplication;
        $this->view->totalcpd = @$totalcpd;
       // $this->view->totalcpdhours = $totalcpdhours;
        $this->view->external_id = $auth->external_id;

        //$this->view->totalmemberapplication = $totalmemberapplication;
        $this->view->exam = $arrayExamList;
        $this->view->myApplication = $myApplication;

        $userDb = new App_Model_User();

        $user = $userDb->getUser($this->auth->getIdentity()->id);

        $form = new App_Form_User();
        $form->populate($user);

        // $userDocDb = new App_Model_UserDoc();
        // $userDocuments = $userDocDb->getUserDoc($user['id']);

            


        // $this->view->documents = $userDocuments;

        //unset
        $form->email->setRequired(false);
        $form->username->setRequired(false);
        $form->gender->setRequired(false);
        $form->estatus->setRequired(false);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($this->auth->getIdentity()->login_as) {//if admin
                $modifiedby = $this->auth->getIdentity()->login_as;
                $modifiedrole = "superadmin";
            } else {
                $modifiedby = $this->auth->getIdentity()->id;
                $modifiedrole = $this->auth->getIdentity()->role;
            }
            if ($form->isValid($formData)) {
                //update
                $data = array(
                    'firstname'    => $formData['firstname'],
                    'lastname'     => $formData['lastname'],
                    'contactno'    => $formData['contactno'],
                    'estatus'      => (isset($formData['estatus']) ? $formData['estatus'] : null),
//                    'modifiedby' => $this->auth->getIdentity()->id,
                    'modifiedby'   => $modifiedby,
//                    'modifiedrole' => $this->auth->getIdentity()->role,
                    'modifiedrole' => $modifiedrole,
                    'modifieddate' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'timezone'     => $formData['timezone'],
                    'address1'     => trim($formData['address1']),
                    'postcode'     => strtoupper($formData['postcode']),
                    'country'      => $formData['country']
                );

                $_SESSION['Zend_Auth']['storage']->firstname = $formData['firstname'];
                $_SESSION['Zend_Auth']['storage']->lastname = $formData['lastname'];

                if (isset($formData['gender']) && $formData['gender']) {
                    $data['gender'] = $formData['gender'];
                }

                if (isset($formData['birthdate']) && Cms_Common::validateDate($formData['birthdate'], 'd-m-Y')) {
                    $data['birthdate'] = date('Y-m-d', strtotime($formData['birthdate']));
                    $_SESSION['Zend_Auth']['storage']->birthdate = $data['birthdate'];
                }

                if (isset($formData['state']) && $formData['state']) {
                    $data['state'] = $formData['state'];
                }

                if (isset($formData['city']) && $formData['city']) {
                    $data['city'] = $formData['city'];
                }
                $data['postcode'] = $formData['postcode'];

                //upload file
                if ($user['photo'] != '' || isset($formData['deletephoto'])) {
                    @unlink($this->uploadDir . $user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir);


                if (!empty($files)) {
                    $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if (!empty($files)) {
                    $temp = array();
                    foreach ($files as $file) {
                        $extension = array("jpg", "jpeg", "png", "gif");

                        if (in_array(substr(strrchr($file['filename'], '.'), 1), $extension)) {
                            $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                            $photochanged = 1;
                        } else {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }

                // save document data (if any)
                if (!empty($temp)) {
                    $dataFile['user_id'] = $user['id'];
                    $dataFile['doc_title'] = $formData['title'];
                    $dataFile['doc_filename'] = $temp['filename'];
                    $dataFile['doc_hashname'] = $temp['fileurl'];
                    $dataFile['doc_url'] = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location'] = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension'] = $temp['ext'];
                    $dataFile['doc_filesize'] = $temp['filesize'];

                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if (($user['id'] == $this->auth->getIdentity()->id) && $photochanged) {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $userDb->update($data, array('id = ?' => $this->auth->getIdentity()->id));

                $response = Cms_Curl::__(SMS_API . '/get/Registration/do/syncProfileToSms', array('external_id' => $user['external_id']));

                Cms_Common::notify('success', 'Information Updated');
                $this->redirect('/dashboard');
            } else {

            }
        }

        $announcements = $this->getannouncement();

        $this->view->announcements = $announcements;

        $event = $this->getcalendar();

        $this->view->event = $event;

        //$this->view->documents = $userDocuments;
        $this->view->user = $user;
        $this->view->form = $form;
    }

    public function membershipAction()
    {

        /*$email = new icampus_Function_Email_Email();

        $info = array(
            'template_id' => 1,
            'name' => 'name',
            'program_name' => 'name',
            'membership_name' => 'name',
            'title' => 'title'
        );

        $email->sendEmail($info);*/

        //error_reporting(E_ALL);
        //ini_set('display_errors', '1');
        $tab = $this->_getParam('tab', 'application');
        $this->view->tab = $tab;

        Cms_Hooks::push('user.sidebar', self::userSidebar('dashboard'));

        $enrolDb = new App_Model_Enrol();
        $auth = Zend_Auth::getInstance()->getIdentity();
        $smsExamDB = new App_Model_SmsExam();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $currDb = new App_Model_Curriculum();
        $pepDB = new App_Model_Exam_ProgrammeExamPrerequisite();
        $erDB = new App_Model_Exam_ExamRegistration();
        $coursesDb = new App_Model_Courses();
        $userGroupDb = new App_Model_UserGroup();
        $userGroupDataDb = new App_Model_UserGroupData();
        $online = new App_Model_UserOnline();
        $smsExtendTimeDB = new App_Model_ExtendTime();

        $count = $online->getTotalUsers();//pr($count);
        $this->view->totalusers = $count;

        $welcome = isset($auth->new) && $auth->new == 1 ? 1 : 0;

        $myprograms = $enrolDb->getEnrolledProgramme($auth->id, false);
        $myexams = array();
        $learningMode_ = array("OL" => "Online", "FTF" => "Face to Face");
        $user_id = $auth->id;

        $totalenrolledprogram = count($myprograms);

        $totalenrolled = 0;
        $a = 0;

        if ($myprograms) {

            foreach ($myprograms as $a => $mypg) {

                $curriculumId = $mypg['id'];
                $IdProgram = $mypg['external_id'];
                $learningmode = ($mypg['learningmode'] == 'OL') ? 577 : 578;

                $mycourses = $enrolDb->getEnrolledCourses($auth->id, $curriculumId, false);
                $totalenrolled += count($mycourses);
                $myprograms[$a]['courses'] = $mycourses;

                foreach ($myprograms[$a]['courses'] as $course) {

                    $learningmode = $course['learningmode'];
                    $group_ = $userGroupDb->getGroupByName($course['code'] . ' - ' . $learningMode_[$learningmode]);

                    if (isset($group_) && !empty($group_)) {
                        $dataGroup = $userGroupDb->getDataByUserAndGroup($user_id, $group_['group_id']);

                        if (!$dataGroup) {
                            $dataGroup = array(
                                'group_id'     => $group_['group_id'],
                                'user_id'      => $user_id,
                                'active'       => 1,
                                'created_date' => new Zend_Db_Expr('NOW()'),
                                'created_by'   => 1
                            );
                            //$groupData = $userGroupDataDb->insertGroupData($dataGroup);
                        }
                    }
                }

                $examSetup = $smsExamDB->getExamByProgram($IdProgram, $learningmode);
                if ($examSetup) {
                    $myprograms[$a]['exam'] = $examSetup;
                }
            }
        }


        // enrolled
        $a = $a + 1;

        $myprograms[$a]['courses'] = $total = $enrolDb->getEnrolledCourses($auth->id, 0, false);
        $totalenrolled += @count($total);

        // completed
        $c = 0;

        $completed[$c]['courses'] = $totalcomplete = $enrolDb->getCompletedCourses($auth->id, 0);
        $totalcompleted = @count($totalcomplete);

        //exam
        $sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }

        if ($sp) {
            // login admin
            /*if ($auth->role =='administrator' || empty($sp)) {
                $profile = $sp;
            } else {
                $profile = $erDB->getProfile($sp);
            }

            $sp = $profile['sp_id'];*/

            //get membership info
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);
            $this->view->membership = $membership;

            //get active membership
            $active_membership = $MembershipDB->getActiveMembership($sp);

            //get application
            $applicantTransactionDB = new App_Model_Records_ApplicantTransaction();
            $transaction = $applicantTransactionDB->getData($sp);
            $this->view->transaction = $transaction;


            if (isset($transaction[0]['IdProgram']) && $transaction[0]['IdProgram'] != '') {
                $curiculumDb = new App_Model_Curriculum();
                $this->view->micuriculum = $curiculumDb->getDataByExternal($transaction[0]['IdProgram']);
            }

            //enrolment matrix
            $matrix = array(
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );

            $this->view->expired_flag = false;
            $this->view->qualification_pay = true;
            $this->view->qualification_register = true;

            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired

                    $this->view->expired_flag = true;
                    $matrix['expired_flag'] = true;
                }
            }

            if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                //require membership
                if ($transaction[0]['mr_status'] != 1604 && $transaction[0]['mr_status'] != 1607) { //approve & active
                    $this->view->qualification_pay = false;
                    $this->view->qualification_register = false;
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                    $matrix['qualification_pay_msg'] = "Membership is not active.";
                    $matrix['qualification_register_msg'] = "Membership is not active.";
                }
            } else {
                if ($transaction[0]['registration_type'] == "Qualification Only")
                    $matrix['qualification_register'] = false;
                $matrix['qualification_pay'] = false;
            }

            $this->view->matrix = $matrix;
            //end enrollment matrix
            //pr($matrix);

            //get qualification info
            $studentRegDb = new App_Model_Qualification_StudentRegistration();
            $this->view->qualification_registration = $studentRegDb->getDatabyStudentId($sp);


            if ($auth->role == 'administrator') {

            } else {


                $totalexam = $ExamregDB->getexamReg($sp);
                $totalexam = @count($totalexam);
                $myexams = $ExamregDB->getexamReg($sp);

                foreach ($myexams as $n => $ex) {
                    $myexams[$n]['examStatus'] = $ExamregDB->courseStatus($ex['er_registration_type']);
                }


                /*if($myApplication[0]["IdRegistrationType"] != 2){
                    foreach ($myApplication as $xp=>$appl) {
                        $memberDetl = $MembershipDB->getDataById("", $registrationId);
                        $myApplication[$xp]["membershipDetl"] = $memberDetl;

                        $memberProgramDetl = $MembershipDB->getMemberProgram($memberDetl['mr_idProgram']);
                        $myApplication[$xp]["memberProgramDetl"] = $memberProgramDetl;

                        $memberProgramDetl["IdProgram"];
                        $feeProgramDetl = $MembershipDB->getFeeProgram($memberProgramDetl["IdProgram"]);
                        $myApplication[$xp]["feeProgramDetl"] = $feeProgramDetl;

                        $totalmemberapplication = $memberDetl["totalmemberapp"];
                    }
                }*/

                $applicationDb = new App_Model_SmsApplication();
                $currDb = new App_Model_Curriculum();

                $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
                $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
                if ($myApplication) {
                    foreach ($myApplication as $index => $app) {
                        $smsprogramid = $app["IdProgram"];
                        if ($smsprogramid) {
                            $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                            $myApplication[$index]['cid'] = $curiculum['id'];
                        }
                    }
                }
                $totalapplication = count($myApplication);
            }


            // CPD DETAILS
            $applicationDb = new App_Model_SmsApplication();

        } else {
            $totalexam = 0;
            $totalapplication = 0;
            $myexams = array();
            $myApplication = array();
            $student_category_list = array();
        }
        //$this->view->student_category_list = $student_category_list;

        //orders pending
        $param = array('user_id = ?' => $auth->id, 'status = ?' => 'PENDING');
        $pendingorders = $this->orderDb->getOrders($param);

        $userDb = new App_Model_User();

        $user = $userDb->getUser($auth->id);

        //get all available exam setup
        //$examList = $smsExamDB->getAvailableExam();
        $examList = $smsExamDB->getAvailableExamByUser($sp);
        //$examList = $smsExamDB->getExamList($auth->sp_id);

        $smsStudentRegistrationDB = new App_Model_StudentRegistration();
        $studentRegDetl = $smsStudentRegistrationDB->getDataStudentRegistration($sp);
        $this->view->studentRegDetl = $studentRegDetl;

        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjectsDetail($sp);
        $this->view->StudentSubjectsDetail = $StudentSubjectsDetail;

        $studentExtendDetails = $smsExtendTimeDB->getDataStudReg($studentRegDetl["IdStudentRegistration"]);
        $this->view->studentExtendDetails = $studentExtendDetails;

        $arrayExamList = array();

        //get exam fee
        foreach ($examList as $a => $exam) {

            /*$programmeExamId = explode(',',  $exam['pe_id']);

            $examParam = array(
               // 'IdMode'=> 578, //by default FTF
                'IdProgram'=>$exam['IdProgram'],
                'IdProgramScheme'=>0,
                'Category'=>($auth->nationality == 'MY') ? 579 : 580,
                'ExamOption' => json_encode($programmeExamId),
                'view' => 1
            );

            $get = Cms_Curl::__(SMS_API.'/get/Finance/do/getAmountExam',$examParam);

            $examList[$a]['amount'] = '-';
            if(isset($get['totalFee'])) {
                $examList[$a]['amount'] = new Zend_Currency(array('value' => $get['totalFee'], 'symbol' => $get['fee']['cur_code']));
                //get lms id
                $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
                $examList[$a]['external_id'] = $curriculumData['id'];

                $arrayExamList[$a] = $examList[$a];

            }*/

            $curriculumData = $currDb->getCurriculum(array('external_id = ?' => $exam['IdProgram']));
            $examList[$a]['external_id'] = $curriculumData['id'];

            $arrayExamList[$a] = $examList[$a];

        }


        //get all qualification application
        /*prerequisites
        if($IdStudentRegistration) {

        $pass_prerequisite = true;
        foreach($prerequisites as $n => $pep) {
            $result = $pepDB->checkPrerequisite($pep['pep_id'], $student_profile['std_id'], $IdStudentRegistration);

            if(!$result['result']) {
                $pass_prerequisite = false;
            }
        }

        // $pass_prerequisite = true;

if(!$pass_prerequisite) {
    continue;
}*/

        // populate registered exams in an array
        $registeredExam = array();
        if (is_array($myexams)) {
            foreach ($myexams as $exam) {
                if (!in_array($exam['er_registration_type'], array(0, 1, 4, 9, 10))) {
                    continue;
                }
                $registeredExam[] = $exam['IdProgram'];
            }
        }

//        // CPD DETAILS
        $applicationDb = new App_Model_SmsApplication();
//
        if ($auth->external_id) {
            $cpd = $applicationDb->getCpdDetailsByStudentId($auth->external_id);
            $totalcpd = @count($cpd);

            $totalcpdhours = $applicationDb->countCpdDurationByStudentId($auth->external_id);
        }
//
        $student_category_list = $applicationDb->fnGetDefinations('CPD Learning');
        $this->view->student_category_list = $student_category_list;

        //views
        $this->view->registeredExam = $registeredExam;
        $this->view->user_id = $user_id;//echo $user_id."+++";
        $this->view->user = $user;
        $this->view->pendingorders = $pendingorders;
        $this->view->welcome = $welcome;

        $this->view->totalenrolled = $totalenrolled;
        $this->view->myprograms = $myprograms;
        $this->view->myexams = @$myexams;

        $this->view->totalcompleted = $totalcompleted;
        $this->view->totalexam = @$totalexam;
        $this->view->completed = $completed;
        $this->view->totalapplication = $totalapplication;
        $this->view->totalcpd = @$totalcpd;
        $this->view->totalcpdhours = $totalcpdhours;
        $this->view->external_id = $auth->external_id;

        //$this->view->totalmemberapplication = $totalmemberapplication;
        $this->view->exam = $arrayExamList;
        $this->view->myApplication = $myApplication;

        $userDb = new App_Model_User();

        $user = $userDb->getUser($this->auth->getIdentity()->id);

        $form = new App_Form_User();
        $form->populate($user);

        $userDocDb = new App_Model_UserDoc();
        $userDocuments = $userDocDb->getUserDoc($user['id']);

        $this->view->documents = $userDocuments;

        //unset
        $form->email->setRequired(false);
        $form->username->setRequired(false);
        $form->gender->setRequired(false);
        $form->estatus->setRequired(false);

        $photochanged = 0;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($this->auth->getIdentity()->login_as) {//if admin
                $modifiedby = $this->auth->getIdentity()->login_as;
                $modifiedrole = "superadmin";
            } else {
                $modifiedby = $this->auth->getIdentity()->id;
                $modifiedrole = $this->auth->getIdentity()->role;
            }
            if ($form->isValid($formData)) {
                //update
                $data = array(
                    'firstname'    => $formData['firstname'],
                    'lastname'     => $formData['lastname'],
                    'contactno'    => $formData['contactno'],
                    'estatus'      => (isset($formData['estatus']) ? $formData['estatus'] : null),
//                    'modifiedby' => $this->auth->getIdentity()->id,
                    'modifiedby'   => $modifiedby,
//                    'modifiedrole' => $this->auth->getIdentity()->role,
                    'modifiedrole' => $modifiedrole,
                    'modifieddate' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'timezone'     => $formData['timezone'],
                    'address1'     => trim($formData['address1']),
                    'postcode'     => strtoupper($formData['postcode']),
                    'country'      => $formData['country']
                );

                $_SESSION['Zend_Auth']['storage']->firstname = $formData['firstname'];
                $_SESSION['Zend_Auth']['storage']->lastname = $formData['lastname'];

                if (isset($formData['gender']) && $formData['gender']) {
                    $data['gender'] = $formData['gender'];
                }

                if (isset($formData['birthdate']) && Cms_Common::validateDate($formData['birthdate'], 'd-m-Y')) {
                    $data['birthdate'] = date('Y-m-d', strtotime($formData['birthdate']));
                    $_SESSION['Zend_Auth']['storage']->birthdate = $data['birthdate'];
                }

                if (isset($formData['state']) && $formData['state']) {
                    $data['state'] = $formData['state'];
                }

                if (isset($formData['city']) && $formData['city']) {
                    $data['city'] = $formData['city'];
                }
                $data['postcode'] = $formData['postcode'];

                //upload file
                if ($user['photo'] != '' || isset($formData['deletephoto'])) {
                    @unlink($this->uploadDir . $user['photo']);
                    $data['photo'] = null;
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir);


                if (!empty($files)) {
                    $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                    $photochanged = 1;
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx,jpg,jpeg,png,gif');

                if (!empty($files)) {
                    $temp = array();
                    foreach ($files as $file) {
                        $extension = array("jpg", "jpeg", "png", "gif");

                        if (in_array(substr(strrchr($file['filename'], '.'), 1), $extension)) {
                            $data['photo'] = '/userphoto/' . $files[0]['fileurl'];
                            $photochanged = 1;
                        } else {
                            $temp = $file;
                            $temp['ext'] = (substr(strrchr($file['filename'], '.'), 1));
                        }
                    }
                }

                // save document data (if any)
                if (!empty($temp)) {
                    $dataFile['user_id'] = $user['id'];
                    $dataFile['doc_title'] = $formData['title'];
                    $dataFile['doc_filename'] = $temp['filename'];
                    $dataFile['doc_hashname'] = $temp['fileurl'];
                    $dataFile['doc_url'] = DOCUMENT_URL . $this->view->folder_name . '/' . $temp['fileurl'];
                    $dataFile['doc_location'] = $this->uploadDir . '/' . $temp['fileurl'];
                    $dataFile['doc_extension'] = $temp['ext'];
                    $dataFile['doc_filesize'] = $temp['filesize'];

                    $userDocDb->addData($dataFile);
                }

                //if user is loggedin user
                if (($user['id'] == $this->auth->getIdentity()->id) && $photochanged) {
                    $authUser = $this->auth->getStorage()->read();
                    $authUser->photo = $data['photo'];

                    $this->auth->getStorage()->write($authUser);
                }

                //update data
                $userDb->update($data, array('id = ?' => $this->auth->getIdentity()->id));

                $response = Cms_Curl::__(SMS_API . '/get/Registration/do/syncProfileToSms', array('external_id' => $user['external_id']));

                Cms_Common::notify('success', 'Information Updated');
                $this->redirect('/dashboard');
            } else {

            }
        }

        $announcements = $this->getannouncement();
        $this->view->announcements = $announcements;

        $event = $this->getcalendar();
        $this->view->event = $event;

        //$this->view->documents = $userDocuments;
        $this->view->user = $user;
        $this->view->form = $form;
    }

    public function cpdDetailsAction()
    {

        Cms_Hooks::push('user.sidebar', self::userSidebar('home'));
        Cms_Common::expire();
        $db = getDB2();

        $userDb = new App_Model_User();
        $applicationDb = new App_Model_SmsApplication();
        $this->view->applicationDb = $applicationDb;

        $id = $this->_getParam('cpd_id');
        // echo $id;
        // die();
        $this->view->cpd_id = $id;

        $user = $userDb->getUser($this->auth->getIdentity()->id);


        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            Cms_Common::notify('success', 'CPD Activity Updated');
            $this->redirect('/user/cpd');

        }

        $this->view->user = $user;


    }

    public function cpdAddDetailsAction()
    {

        Cms_Hooks::push('user.sidebar', self::userSidebar('home'));
        Cms_Common::expire();
        $db = getDB2();

        $userDb = new App_Model_User();
        $applicationDb = new App_Model_SmsApplication();
        $this->view->applicationDb = $applicationDb;

        $cpdm_id = $this->_getParam('cpdm_id');
        $this->view->cpdm_id = $cpdm_id;
        $user_id =$this->auth->getIdentity()->id;
        $user = $userDb->getUser($this->auth->getIdentity()->id);

        $formData = $applicationDb->getMaterialList($cpdm_id);

     
        $checkMaterialExist = $applicationDb->getCheckMaterialList($user_id, $cpdm_id);
        $get_info  = $userDb->getUserInfo($user_id);

if($get_info['id_member'] == 1 || $get_info['id_member'] == 3)
{
    if($formData['cpdm_type'] == 1570)
    {
        $cpd_point = 15;

    }else{
            $cpd_point = 5;
    }

}

if($get_info['id_member'] == 4)
{
    if($formData['cpdm_type'] == 1570)
    {
        $cpd_point = 20;

    }else{
            $cpd_point = 10;
    }

}
if($get_info['id_member'] == 5 || $get_info['id_member'] == 6)
{
    if($formData['cpdm_type'] == 1570)
    {
        $cpd_point = 10;

    }else{
            $cpd_point = 5;
    }

}

        if ($checkMaterialExist == '') {
            $data = array(
                'spid'            => $user_id,
                'cpd_cpdm_id'     => $formData['cpdm_id'],
                'cpd_learning_id' => $formData['cpdm_type'],
                'cpd_category_id' => $formData['cpdm_category'],
                'cpd_activity_id' => $formData['cpdm_activity'],
                'cpd_content_id'  => $formData['cpdm_content'],
                'cpd_title'       => $formData['cpdm_title'],
                'cpd_venue'       => $formData['cpdm_venue'],
                'cpd_description' => $formData['cpdm_desc'],
                'cpd_provider'    => $formData['cpdm_provider'],
                'cpd_duration'    => $formData['cpdm_hours'],
                'date_started'    => date('Y-m-d', strtotime($formData['date_started'])),
                'date_completion' => date('Y-m-d', strtotime($formData['date_completion'])),
                'created_by'      => $user_id,
                'created_at'      => date('Y-m-d H:i:s'),
                'cpd_status'      => 0,
                'cpd_corp_id'     => $formData['cpdm_corp_id'],
                'cpd_point' => $cpd_point

            );

            $cpd_id = $applicationDb->addDataActivity($data);
            $db = getDB2();
            
            if (!empty($formData['cpdm_url'])) {

                if ($formData['cpdm_content'] == '1637') {

                    $dataFile = [];

                    $dataFile['sp_id'] = $user_id;
                    $dataFile['cpd_id'] = $cpd_id;
                    $dataFile['doc_title'] = $formData['cpdm_title'];
                    $dataFile['doc_content_id'] = $formData['cpdm_content'];
                    $dataFile['doc_filename'] = $formData['cpdm_url'];
                    $dataFile['doc_url'] = $formData['cpdm_url'];

                } else {
                    $dataFile = [];

                    $dataFile['sp_id'] = $user_id;
                    $dataFile['cpd_id'] = $cpd_id;
                    $dataFile['doc_title'] = $formData['cpdm_title'];
                    $dataFile['doc_filename'] = $formData['cpdm_filename'];
                    $dataFile['doc_url'] = $formData['cpdm_url'];
                    $dataFile['doc_location'] = $formData['cpdm_location'];
                    $dataFile['doc_extension'] = $formData['cpdm_extension'];
                    $dataFile['doc_filesize'] = $formData['cpdm_filesize'];

                    //pr($dataFile);

                }
                $uploadid = $db->insert('tbl_cpd_doc', $dataFile);

            }
            // exit;
        }

        // echo $cpd_id;
        // die();
        $this->redirect('/user/cpd-details/cpd_id/' . $cpd_id);

        $this->view->user = $user;


    }

    public function viewMaterialAction()
    {

        Cms_Hooks::push('user.sidebar', self::userSidebar('home'));
        Cms_Common::expire();
        $db = getDB2();

        $userDb = new App_Model_User();
        $applicationDb = new App_Model_SmsApplication();
        $this->view->applicationDb = $applicationDb;

        $id = $this->_getParam('cpdm_id');
        $this->view->cpdm_id = $id;

        $user = $userDb->getUser($this->auth->getIdentity()->id);


        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //pr($formData);exit;
            if ($formData['verify'] == on) {

                $this->redirect('/user/cpd-add-details/cpdm_id/' . $id);

            } else {
                Cms_Common::notify('error', 'Please check verification part before submit.');
                $this->redirect('/user/view-material/cpdm_id/' . $id);
            }


        }

        $this->view->user = $user;


    }

    public function downloadAction()
    {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $doc_id = (int)$this->_getParam('doc_id', 0);

        $projectDocumentDB = new App_Model_SmsApplication();
        $document = $projectDocumentDB->getOneProjectDocument($doc_id);

        $fullpath = $document['doc_location'];
        $filename = $document['doc_filename'];
        $filesize = $document['doc_filesize'];
        if ($document['doc_hashname'] == '') {
            $file_part = pathinfo($document['doc_filename']);
            //dd($file_part);
            $extension = strtolower($file_part['extension']);

        } else {
            $file_part = pathinfo($document['doc_hashname']);
            $extension = strtolower($file_part['extension']);
        }


        //echo $extension;

        if ($fd = @fopen($fullpath, 'r')) {
            switch ($extension) {
                case "pdf":
                    header("Content-type: application/pdf");
                    header("Content-Disposition: attachment; filename=\"" . $$filename . "\"");
                    break;
                default:
                    header("Content-type: application/octet-stream");
                    header("Content-Disposition: filename=\"" . $file_part["basename"] . "\"");

            }
            header("Content-length: $filesize");
            header("Cache-control: private");
            while (!feof($fd)) {
                $buffer = fread($fd, 2048);
                echo $buffer;
            }
        }
        @fclose($fd);
        exit();
    }

    public function getLearningCategoryAction()
    {

        $learning_id = $this->_getParam('idLearning');

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $defaultActivityDB = new App_Model_SmsApplication();
        $result = $defaultActivityDB->getLearningCategory($learning_id);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

    public function getLearningActivityAction()
    {

        $learning_id = $this->_getParam('idLearning');
        $category_id = $this->_getParam('idCategory');

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $defaultActivityDB = new App_Model_SmsApplication();
        $result = $defaultActivityDB->getLearningActivity($learning_id, $category_id);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }
     public function newAction()
    {
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();
             // echo '<pre>';
             //    print_r($formData);
             //    die();
            $dbUser   = new App_Model_User;
            $fail     = 0;
            $errmsg   = '';

            $user = $dbUser->fetchRow(array("username = ?" => $formData['username']));
            if (!empty($user)) 
            {
                $fail   = 1;
                $errmsg = $this->view->translate('Username already exists. <a href="/forgotpass">Forgot your password?</a>');
            }

            //check email
            $email = $dbUser->fetchRow(array("email = ?" => $formData['email']));
            if (!empty($email)) 
            {
                $fail   = 1;
                $errmsg = $this->view->translate('Email already in use. <a href="/forgotpass">Forgot your password?</a>');
            }

            //check for illegal characters
            if ( preg_match("/[^a-zA-Z0-9]+/", $formData['username']) && $fail != 1 )
            {
                $fail   = 1;
                $errmsg = $this->view->translate('Username contains illegal characters. Try again. Only alphanumeric characters are allowed.',0);
            }

            if ( ( strlen($formData['username']) < 3 || strlen($formData['username']) > 25 ) && $fail != 1 )
            {
                if ( strlen($formData['username']) < 3 )
                {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username is too short. You need to have at least 3 characters',0);
                }
                else
                {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username is too long. Your username cannot be longer than 25 characters.',0);
                }
            }

            if (!$fail)
            {
                $salt      =  Cms_Common::generateRandomString(22);
                $options   = array('salt' => $salt);
                $hash      = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
                $birthdate = date('Y-m-d', strtotime($formData['dob_year'].'-'.$formData['dob_month'].'-'.$formData['dob_day']));
                $birthdate = ($birthdate == '1970-01-01' ? null : $birthdate);

                foreach ($formData as $index => $value)
                {
                    if (!$value)
                    {
                        $formData[$index] = null;
                    }
                }

                $data = array(
                    'username'      => $formData['username'],
                    'password'      => $hash,
                    'salt'          => $salt,
                    'firstname'     => $formData['firstname'],
                    'lastname'      => $formData['lastname'],
                    'email'         => $formData['email'],
                    'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'role'          => $formData['role'],
                    'contactno'     => $formData['contactno'],
                    'birthdate'     => $birthdate,
                    'qualification' => $formData['qualification'],
                    'modifieddate'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'modifiedby'    => $this->auth->getIdentity()->id,
                    'modifiedrole'  => $this->auth->getIdentity()->role,
                    // 'company'    => $formData['company'],
                    'active'        => 1
                );
                $user_id = $dbUser->insert($data);

                Cms_Common::notify('success', 'User successfully created');
                $this->redirect('/admin/users/');
            }
            else
            {
                unset($formData['password']);
                $this->view->formData_json = json_encode($formData);
                $this->view->errmsg = $errmsg;
            }
        }

        $dataDb       = new Admin_Model_DbTable_ExternalData();
        $company      = $dataDb->getData('Company');
        $companies    = array();
        $company_list = array();
        $currs = $this->currDb->fetchAll()->toArray();
        $results = $this->currDb->printTree($this->currDb->buildTree($currs,0),0,0,false) ;

        $this->view->results = $results;

        foreach( $company as $row )
        {
            $companies[] = addslashes($row['name']);
            $company_list[$row['id']] = $row['name'];
        }

        $qualification = $dataDb->getData('Qualification');

        $qualification_list = array('' => '');
        foreach( $qualification as $row ) 
        {
            $qualification_list[ $row['id'] ] = $row['name'];
        }

        $this->view->company            = json_encode($companies);
        $this->view->company_list       = json_encode($company_list);
        $this->view->qualification_list = $qualification_list;
    }

    public function uploaddocumentAction()
    {
  
       

        $form = new App_Form_Register();

        $redirect = $this->getParam('r');

        //toggle password required
        $form->username->setRequired(true);
        $form->password->setRequired(true);

        //$events = Cms_Events::trigger('auth.register.form', $this, array('form'=>$form));
      
 $auth = Zend_Auth::getInstance()->getIdentity();

 $userDocDb = new App_Model_UserDocuments();
 
       $user_id = $auth->id;
            
             $get_doc_statuss = $userDocDb->get_doc_status($user_id);
             
             $newDocArray = [];
             $newDocArray1 = [];

             // $newDocArray['doc_type'] = 1;
             // $newDocArray['doc_type'] = 2;
             // $newDocArray['doc_type'] = 3;
             // $newDocArray['doc_type'] = 4;
             // $newDocArray['doc_type'] = 5;

             for($i=0;$i<count($get_doc_statuss);$i++) {
                 switch ($get_doc_statuss[$i]['doc_type']) {
                     case '1':
                     $newDocArray['1'] = $get_doc_statuss[$i];
                         break;
                     case '2':
                     $newDocArray['2'] = $get_doc_statuss[$i];
                         break;
                     case '3':
                     $newDocArray['3'] = $get_doc_statuss[$i];
                         break;
                     case '4':
                     $newDocArray['4'] = $get_doc_statuss[$i];
                         break;
                     case '5':
                     $newDocArray['5'] = $get_doc_statuss[$i];
                         break;
                         // case '3':
                         // array_push($newDocArray[3],$get_doc_status[$i]);
                         // break;
                     default:
                         # code...
                         break;
                 }
             }
             // echo '<pre>';
             // print_r($newDocArray);
             // die();
             $get_doc_status = $newDocArray;
                $this->view->doc_status = $get_doc_status;
             // prin_r($this->view->doc_status);
             // exit;

        if ($this->getRequest()->isPost())
        {




            $formData = $this->getRequest()->getPost();

            
            
            if ($formData)
            { 

                $dbUser = new App_Model_User;
                $dbciifp= new App_Model_Ciifpmb;
                $dbciifpdoc= new App_Model_Ciifpdoc;

                          
                


                

                $fail = 0; //temporary solution
                
                if ($fail != 1)
                {
                    
                    

                            $upload = new Zend_File_Transfer();
                   
                   // $id= $userid1['id'];
                            $membership_id= $user_id;
                              $files  = $_FILES;
                          

                              // FileUpload Academic
                               $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
                                 $target_file1 = 'AcademicDoc'.date('YmdHis').$_FILES['file1']['name'][$i];
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH.$target_file1)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
                                           } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }



                             for($i=0;$i<count($_FILES['file2']['name']);$i++) 
                              {
                                   if($i==0) {
                                      $k = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $k = $j;
                                   
                                   }
                                 $target_file2 = 'WorkExperience'.date('YmdHis').$_FILES['file2']['name'][$k];
   if (move_uploaded_file($_FILES["file2"]["tmp_name"][$k], UPLOAD_PATH.$target_file2)) {



                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file2);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              } 

                              for($i=0;$i<count($_FILES['file3']['name']);$i++) 
                              {

                                if($i==0) {
                                      $l = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $l = $j;
                                   
                                   }

                                        // move upload file
                                 $target_file3 = 'PreviosMembershipdoc'.date('YmdHis').$_FILES['file3']['name'][$l];
   if (move_uploaded_file($_FILES["file3"]["tmp_name"][$l], UPLOAD_PATH.$target_file3)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file3);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }  


                               for($i=0;$i<count($_FILES['file4']['name']);$i++) 
                              {
                                if($i==0) {
                                      $m = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $m = $j;
                                   
                                   }
                                        // move upload file
                                 $target_file5 = 'NominationDoc'.date('YmdHis').$_FILES['file5']['name'][$m];
   if (move_uploaded_file($_FILES["file5"]["tmp_name"][$m], UPLOAD_PATH.$target_file5)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file5);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }


                               for($i=0;$i<count($_FILES['file5']['name']);$i++) 
                              {
                                if($i==0) {
                                      $m = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $m = $j;
                                   
                                   }
                                        // move upload file
                                 $target_file5 = 'NominationDoc'.date('YmdHis').$_FILES['file5']['name'][$m];
   if (move_uploaded_file($_FILES["file5"]["tmp_name"][$m], UPLOAD_PATH.$target_file5)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file5);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }

                               for($i=0;$i<count($_FILES['file6']['name']);$i++) 
                              {
                                if($i==0) {
                                      $m = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $m = $j;
                                   
                                   }
                                        // move upload file
                                 $target_file6 = 'NominationDoc'.date('YmdHis').$_FILES['file6']['name'][$m];
   if (move_uploaded_file($_FILES["file6"]["tmp_name"][$m], UPLOAD_PATH.$target_file6)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file4);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }



                               for($i=0;$i<count($_FILES['file6']['name']);$i++) 
                              {

                                if($i==0) {
                                      $n= 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $n = $j;
                                   
                                   }

                                        // move upload file
                                 $target_file5 = 'OtherDoc'.date('YmdHis').$_FILES['file6']['name'][$n];
   if (move_uploaded_file($_FILES["file5"]["tmp_name"][$n], UPLOAD_PATH.$target_file5)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file5);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }  


                                if($user_id)
                                {
        $user = $dbUser->fetchRow(array('id = ?' => $membership_id));
       
        $applicant_name = $user['firstname'].$user['lastname'];
         $applicant_email = $user['email'];

        $get_mail = new Admin_Model_DbTable_Emailtemplate();
        $mail_detail = $get_mail->fnViewTemplte(23);
        
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
                                }
                               Cms_Common::notify('success', 'Application Added successfully Please Login for payment... ');
                            
                            $this->redirect('/login');

          
                }
                else
                {
                    $this->view->errmsg = $errmsg;
                    $form->populate($formData);
                }

            }
            else
            {
                $this->view->errmsg = 'You must fill in all the required fields.';
            }
        }

        //view
        $this->view->redirect = $redirect;
        $this->view->form = $form;

    }
}


