<?php
/**
 * @author Muhamad Alif <muhamad_alif@meteor.com.my>
 * @copyright Copyright (c) 2014, MTCSB
 */
//include_once 'Zend/CIMB/Cimb.php';

class PaymentGatewayController extends Zend_Controller_Action
{

	public function init()
	{

    $this->definationDB =new Admin_Model_DbTable_Definition();

		/* Initialize action controller here */
		$this->Session = new Zend_Session_Namespace('session');
    }

       public function ipay88Action(){
          $auth = Zend_Auth::getInstance();
          $appl_id = $auth->getIdentity()->id;
          // $memberData = new App_Model_User();
          // $member = $memberData->getreguser($appl_id);
          // echo "<pre>";
          // print_r($member);
          // die();
          //$ses_migs = new Zend_Session_Namespace('migs');
		      $db = Zend_Db_Table::getDefaultAdapter();
		
		      $auth = Zend_Auth::getInstance();
		      $appl_id = $auth->getIdentity()->id;
           $this->userDb = new App_Model_User();
		      //$txnId = $ses_migs->txnid;
		
		  //     $proformaInvoiceDb = new App_Model_ProformaInvoiceMain();
		  //     $proforma_list = array();
		 	   //     //print_r($proforma_list[0]);
      // 			//die();
      // 			$total = 0;
			 //     $currency="";
			 //   if(count($proforma_list)>0){ 

				// 	foreach($proforma_list as $proforma):
		 	//     	$total=$total+$proforma['bill_amount'];
		 	// 	    $currency = $proforma['cur_code'];
		 	//   	endforeach;
		 		
		 	// }
          $total = 0;
		    	//echo $currency;
			    //echo $total;
			    //		 die();
          //ech o $student['appl_fname']."<br>";
          //echo $student['appl_email']."<br>";
          //echo $student['appl_phone_mobile']."<br>";
          //print_r($student);
          $amount = number_format((float)$total, 2, '.', '');
          //echo $appl_id;
          //die();
          $ipay88 = new Cimb('CHARTERED INSTITUTE-ECOMM');
          $ipay88->setMerchantKey('000001090700089');
          $ipay88->setField('PaymentId', 2);
          $ipay88->setField('RefNo', 'IPAY0000000001');
          $ipay88->setField('Amount', "1.00");
          $ipay88->setField('Currency', 'MYR');
          $ipay88->setField('ProdDesc', 'Testing');
          $ipay88->setField('UserName', 'dfg'.''.'dsfghn'.'');
          $ipay88->setField('UserEmail', 'email');
          $ipay88->setField('UserContact','contactno');
          $ipay88->setField('Remark', 'Some remarks here..');
          $ipay88->setField('Lang', 'utf-8');
          $ipay88->setField('ResponseURL', AJAX_URL.'payment-gateway/response');
          $ipay88->generateSignature();
          $ipay88_fields = $ipay88->getFields();
          
          $url = Cimb::$epayment_url;
          $data = $ipay88_fields;
          $options = array(
                      'http' => array(
                      'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                      'method'  => 'POST',
                      'content' => http_build_query($data)
                )
          );
          $context  = stream_context_create($options);
          $result = file_get_contents($url, false, $context);
          if ($result === FALSE) {
            echo "error";
           // die();
          }
          die($result);

          //print_r($ipay88_fields);
          //die();

        }

        public function indexAction(){
            $form = new App_Form_Payment();


     $dbMenu = new App_Model_PaymentCard;
   $auth = Zend_Auth::getInstance();
          $id = $auth->getIdentity()->id;
 $applicant_fee = new Admin_Model_DbTable_ApplicationFee();
     $auth = Zend_Auth::getInstance();
     $userDb = new App_Model_User();

      $invoice = new App_Model_PerformaInvoice();
      $get_invoice = $invoice->get_amount($id);
      

      $this->view->amount = $get_invoice['amount'];
     


      $user = $userDb->fetchRow(array('id = ?' => $id))->toArray();
            $this->view->user = $user;

            if($user['nationality'] = 'MY')
        {
           
            $fee = $applicant_fee->local();
            $rate = 1;
            $this->view->rate = $rate;
            $this->view->fee = $fee;
        }else
        {

            $feeModel =  new App_Model_MemberFeeStructure();
                $rate = $feeModel->getCurrencyRate();

                 $this->view->rate = $rate['cr_exchange_rate'];
            $fee = $applicant_fee->international();
                  $this->view->fee = $fee;
        }
   
        
 //$user = $this->userDb->fetchRow(array('id = ?' => $id));

    //process post
    if ($this->getRequest()->isPost()) {
      $formdata = $this->getRequest()->getPost();
    
          $response_type = 'HTTP';
          $this->view->r_type = $response_type;
$response_url = 'http://ciif.com/payment-gateway/response';
         
          $this->view->r_url = $response_url;
          $merchant_no = '000001090700089';
          $this->view->m_no = $merchant_no;

$transaction_password = 'ogvxSn05';

$merchant_id =  rand(1000,100000);//'11111122';
$formdata['TRANSACTION_TYPE'] = '3';

        $this->view->m_id =  $merchant_id;  
           $valustring = $transaction_password.$formdata['AMOUNT'].$formdata['CARD_CVC'].$formdata['CARD_EXP_MM'].$formdata['CARD_EXP_YY'].$formdata['CARD_NO'].$formdata['CARD_TYPE'].$merchant_no.$merchant_id.$response_type.$response_url.$formdata['TRANSACTION_TYPE'].$formdata['TXN_DESC'];

          
          $securevalue = hash('sha512', $valustring);
//           echo $valustring.'\n\n';
// echo $securevalue;
// die();
          $this->view->sign = $securevalue;

            $_SESSION['SECURE_SIGNATURE'] = $securevalue; 
            $_SESSION['AMOUNT']      = $formdata['AMOUNT'];
            $_SESSION['CARD_CVC'] = $formdata['CARD_CVC']; 
            $_SESSION['CARD_EXP_MM'] = $formdata['CARD_EXP_MM']; 
            $_SESSION['CARD_EXP_YY'] = $formdata['CARD_EXP_YY'];
             $_SESSION['CARD_NO'] = $formdata['CARD_NO'];
            $_SESSION['CARD_TYPE'] = $formdata['CARD_TYPE'];
            $_SESSION['MERCHANT_ACC_NO'] = $merchant_no;     
            $_SESSION['MERCHANT_TRANID'] = $merchant_id; 
            $_SESSION['RESPONSE_TYPE']      = $response_type; 
            $_SESSION['RETURN_URL'] = $response_url; 
            $_SESSION['TRANSACTION_TYPE'] = $formdata['TRANSACTION_TYPE']; 
            $_SESSION['TXN_DESC'] = $formdata['TXN_DESC'];
            // echo "<pre>";
            // print_r($_SESSION);
            // die();
            echo "<script>parent.location='confirm'</script>";
            exit;

    
        }
        $this->view->form = $form;
      }

      public function confirmAction() {

        $this->view->payment_data= $_SESSION;
       
      }

      public function paymentAction(){
          $id = $this->_getParam('Id');
    
         $pay = new App_Model_PaymentCard();
          $cat = $pay->fetchAll(array("id = ?" => $id))->toArray();



          // all fields should be in same order.
          // generate the sha512 code 

       


          $this->view->result = $cat;
        }
        public function getpayAction(){
           $mail = new Cms_SendMail();
    $db = getDb();
    // echo "<pre>";
    $users = $db->fetchAll( "SELECT * FROM user where email_sent_status = 0 and email !='' limit 50" );
    //     print_r($users);
    // die();
    foreach ($users as $user) {
        $email = trim($user['email']);
        $message = "<p class='western' lang='en-GB' align='justify'>Assalammualaikum wbt &amp; Good Day,</p>
<p class='western' lang='en-GB' align='justify'>Dear  {{Name}},</p>
<p class='western' lang='en-GB' align='justify'>On 1<sup>st</sup> January 2019, you have received a link that directs you to the CIIF Membership Portal, that allowed you to access your pre-created profile in order to update your details and credentials. Completing these steps would then allow you to access your membership page.</p>
<p class='western' lang='en-GB' align='justify'>Since then, we have received various feedbacks from members who shared with us the problems with accessing their membership pages. Upon further investigation with our solutions provider, we have managed to identify the technical issues that have caused the problem.</p>
<p class='western' lang='en-GB' align='justify'>In order for us to be able to rectify this issue, we have agreed with the solutions provider to reset the entire system and create a new login process. This action, however, will affect your previous login credentials.</p>
<p class='western' lang='en-GB' align='justify'>We regret that members who have logged in before will be required to repeat the first time login process in order to access the membership pages. In order to smoothen the process, we have removed the requirement of updating your profile and credentials and you can immediately login to the portal and change your temporary password.</p>
<p class='western' lang='en-GB' align='justify'>Simply follow the steps below:</p>
<p lang='en-GB' align='justify'>Access the membership portal by clicking on this link&nbsp;<strong>https://register.ciif-global.org</strong></p>
<p class='western' lang='en-GB' align='justify'><span lang='en-MY'>Your NRIC / Passport number will be the username for your portal. Your username is </span><span lang='en-MY'><strong>{{Passport}}</strong></span></p>
<p class='western' lang='en-GB' align='justify'><span lang='en-MY'>Your password will be </span><span lang='en-MY'><strong>{{Password}}</strong></span><span lang='en-MY'>. This password is temporary and you are required to change the password after you have successfully logged in.</span></p>
<p class='western' lang='en-GB' align='justify'>You can update your profile and credentials later in your own time.</p>
<p class='western' lang='en-GB' align='justify'><span lang='en-MY'>Through the portal, you can also apply for the CPIF (the CIIF's new professional qualification), pay membership fees, register for events, workshops and programmes, as well as update and track your CPD activities.&nbsp;</span></p>
<p class='western' lang='en-GB' align='justify'><span lang='en-MY'>We urge all members to log in and update their profiles in the portal. We also welcome feedbacks from members on the said portal.</span></p>
<p class='western' lang='en-GB' align='justify'><span lang='en-MY'>Finally, we apologise once again for the inconvience caused, and we are doing our utmost to rectify any technical issues arising from the implementation of the portal. Thank you for your kind understanding</span></p>
<p class='western' lang='en-GB'>&nbsp;</p>
<p class='western' lang='en-GB'><span style='color: #666666;'>Thank you and best regards,</span></p>
<p class='western' lang='en-GB'><span style='color: #444444;'><strong>The CIIF Management</strong></span></p>
<p class='western' lang='en-GB'><img src='https://register.ciif-global.org/img/ciif_logo.png' alt='CIIF' /></p>
<p class='western' lang='en-GB'><span style='color: #666666;'>19th Floor, Menara Takaful Malaysia,</span></p>
<p class='western' lang='en-GB'><span style='color: #666666;'><span lang='fi-FI'>Jalan Sultan Sulaiman,</span></span></p>
<p class='western' lang='en-GB'><span style='color: #666666;'><span lang='fi-FI'>50000 Kuala Lumpur, Malaysia.</span></span></p>
<p class='western' lang='en-GB'><span style='color: #666666;'>T:03-2276 5279 / 5232</span></p>
<p class='western' lang='en-GB'><img src='https://register.ciif-global.org/img/2.png' /><img src='https://register.ciif-global.org/img/3.png' /><img src='https://register.ciif-global.org/img/4.png' /><img src='https://register.ciif-global.org/img/5.png' /><img src='https://register.ciif-global.org/img/6.png' /></p>
<p class='western' lang='en-GB'><img src='https://register.ciif-global.org/img/1.png' /></p>
<p class='western' lang='en-GB'>Should you need further information and clarification, please email to membership@ciif-global.org or call us at 03-2276 5279.</p>
";

        $message = str_replace('{{Salutation}}', $user['salutation'] , $message);
        $message = str_replace('{{Name}}', $user['firstname'].' '.$user['lastname'] , $message);
        // echo $user['username'];
        // echo $email;
        // die();
        if($user['username'] == ''){
        $message = str_replace('{{Passport}}', $email, $message);
        }
        else{
        $message = str_replace('{{Passport}}', $user['username'] , $message);
        }
        $message = str_replace('{{Password}}', "12345678" , $message);
        if($email != ''){
// $email = "sachin@meteor.com.my";
        $mail->fnSendMail1($email, "CIIF Membership Portal",$message);
        }
        $id = $user['id'];
    $db->exec( "update user set email_sent_status = 1 where id = $id" );
    }
    echo "Please refresh to send for next batch.";
    die();

        }
        public function responseAction(){

          // print_r($_POST);
          // die();
          $this->view->response = $_POST;
         if( $_POST['TXN_STATUS'] == 'A')
          {

            $member_payment = $this->definationDB->getdefCode('Paid');

        $auth = Zend_Auth::getInstance();
          $id = $auth->getIdentity()->id;
      $invoice = new App_Model_PerformaInvoice();
      $get_invoice = $invoice->get_amount($id);
          $user_id = $id;
     if($_POST['TXN_STATUS'])
         {
          $invoice = new App_Model_InvoiceMain();
               
                
            $data_invioce = array('user_id' => $user_id,
                                   'description' => $get_invoice['description'],
                                   'fee_type' => $get_invoice['fee_type'],
                              'amount' => $get_invoice['amount'],
                    'invoice_no' => 'CIIF'.  date('YHims').$user_id,
                    'created_user' => $user_id,
                    'created_date' => date("Y-m-d"),
                    'payment_status'=>1
                         );
                    $invoice_no_id = $invoice->insert($data_invioce);
                  
 $performa_data = new App_Model_PerformaInvoiceDetails();
      $get_data = $performa_data->get_detail($get_invoice['id_invoice']);
        $invoice_data = new App_Model_InvoiceMainDetails();
               
                foreach ($get_data as $get) {
                 $data_invioce_details = array('user_id' => $user_id,
                                   'description' => $get['description'],
                              'amount' => $get['amount'],
                              'invoice_id' => $invoice_no_id,
                            'id_program' => $get['id_program'],  
                         'invoice_no' => 'CIIF'.  date('YHims').$user_id,
                    'created_user' => $user_id,
                    'created_date' => date("Y-m-d"),
                    'payment_status'=>1
                         );
                    $invoice_no = $invoice_data->insert($data_invioce_details);

                    }
                }
             
          }
         

        }
	
}