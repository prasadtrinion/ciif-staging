<?php

class BookmarkController extends Zend_Controller_Action
{

    public function init()
    {
        $this->user_id = Zend_Auth::getInstance()->getIdentity()->id;
        $this->bookmarkDb = new App_Model_Bookmark();
        $this->contentBlockDb = new App_Model_ContentBlockData();
    }

	public function indexAction()
    {
        $this->view->title = 'Bookmarks List';

        // course
        $course_id = $this->_getParam('course');
        $this->coursesDb = new App_Model_Courses();

        $id = $this->_getParam('course');
        
        $course = $this->coursesDb->fetchRow(array('id = ?' => $id));

        $getbookmarks = $this->bookmarkDb->getBookmarkByUser($this->user_id, $id, false);

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
            $formData['search'] = trim($formData['search']);

            if ( $formData['search'] != '')
            {
                $getbookmarks->where('bookmark_name LIKE ? OR bookmark_description LIKE ? OR bookmark_link LIKE ?', '%'.$formData['search'].'%');
            }

            $this->view->formdata = $formData;
        }

        $bookmarks = Cms_Paginator::query($getbookmarks);

        //breadcrumbs
        $bc = array(
            'home',
            array($this->view->baseUrl().'/courses/'.$course['code_safe'].'/learn/' => $course['title']),
            array($this->view->baseUrl().'/bookmark/index/course/'.$id => $this->view->translate('Bookmark')),
        );

        $this->view->course = $course;
        $this->view->bc = $bc;
        $this->view->results = $bookmarks;

    }

    public function addAction() 
    {
        $this->view->title = 'New Bookmark';

        $id = $this->_getParam('id',0);
        $course_id = $this->_getParam('course',0);
        $code_safe = $this->_getParam('code_safe',0);

        $form = new App_Form_Bookmark();

        //form required
        $form->bookmark_name->setRequired(true);
        $form->bookmark_link->setRequired(true);
    
        //process post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                $data = array(
                        'bookmark_name'         => $formData['bookmark_name'],
                        'bookmark_description'  => $formData['bookmark_description'],
                        'bookmark_link'         => $formData['bookmark_link'],
                        'user_id'               => $this->user_id,
                        'course_id'             => $formData['course'],
                        'created_date'          => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'            => $this->user_id
                    );

                if ($id) {
                    $data["data_id"] = $id;
                }

                $bookmark_id = $this->bookmarkDb->saveBookmark($data);

                Cms_Common::notify('success','Bookmark successfully created');

                if ($id) {
                    $this->redirect('/courses/' . $code_safe . '/learn');
                } else {
                    $this->redirect('/bookmark/index/course/' . $formData['course']);
                }

            }
        }

        if ($id) {
            // populate form
            $link = APPLICATION_URL . "/courses/block/id/" . $id . "/course/" . $course_id;
            $data = $this->contentBlockDb->fetchRow(array("data_id = ?" => $id))->toArray();

            $data_form = array(
                    'bookmark_name'         => $data['title'],
                    'bookmark_description'  => $data['description'],
                    'bookmark_link'         => $link
            );

            $form->populate($data_form);
        }

        $this->_helper->layout()->disableLayout();
        $this->view->form = $form;
        $this->view->code_safe = $code_safe;
        $this->view->id = $id;
        $this->view->course_id = $course_id;

    }

    public function editAction() 
    {
        $this->view->title = 'Edit Bookmark';

        $id = $this->_getParam('id',0);
        $course_id = $this->_getParam('course',0);
        $code_safe = $this->_getParam('code_safe',0);

        $form = new App_Form_Bookmark();

        //form required
        $form->bookmark_name->setRequired(true);
        $form->bookmark_link->setRequired(true);
    
        //process post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                $data = array(
                        'bookmark_name'         => $formData['bookmark_name'],
                        'bookmark_description'  => $formData['bookmark_description'],
                        'bookmark_link'         => $formData['bookmark_link'],
                        'user_id'               => $this->user_id,
                        'course_id'             => $formData['course'],
                        'created_date'          => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'            => $this->user_id
                    );

                $bookmark_id = $this->bookmarkDb->editBookmark($data, $id);

                Cms_Common::notify('success','Bookmark successfully updated');
                $this->redirect('/bookmark/index/course/' . $formData['course']);

            }
        }

        $getBookmark = $this->bookmarkDb->getBookmark($id);
        $data_form = array(
                'bookmark_name'         => $getBookmark['bookmark_name'],
                'bookmark_description'  => $getBookmark['bookmark_description'],
                'bookmark_link'         => $getBookmark['bookmark_link']

        );

        $form->populate($data_form);

        $this->_helper->layout()->disableLayout();
        $this->view->form = $form;
        $this->view->code_safe = $code_safe;
        $this->view->id = $id;
        $this->view->course_id = $course_id;

    }

    public function deleteAction() {

        $id = $this->_getParam('id');
        $course_id = $this->_getParam('course');
        $code_safe = $this->_getParam('code_safe');
        $this->user_id;


        $bookmark_delete = $this->bookmarkDb->deleteBookmark($id, $this->user_id);

        Cms_Common::notify('success','Bookmark successfully removed');
        $this->redirect('/bookmark/index/course/' . $course_id);


    }
	
}

