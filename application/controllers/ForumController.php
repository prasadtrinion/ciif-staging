<?php

class ForumController extends Zend_Controller_Action
{
    protected $courseDb;
    protected $contentblockDb;
    protected $blockDataDb;
    protected $enrolDb;
    protected $currDb;
    protected $sidebar  = 1;
    protected $currCoursesDb;
    protected $uploadDir;
    protected $db;

    public function init()
    {
        $this->courseDb = new App_Model_Courses();
        $this->contentBlockDb = new App_Model_ContentBlock();
        $this->blockDataDb = new App_Model_ContentBlockData();
        $this->enrolDb = new App_Model_Enrol();
        $this->currDb = new App_Model_Curriculum();
        $this->currCoursesDb = new App_Model_CurriculumCourses();
        $this->uploadDir = Cms_System_Links::dataFile();

        $this->auth = Zend_Auth::getInstance();
        $this->identity = $this->auth->getIdentity();
    }

	public function indexAction()
    {

    }


	public function forumAction()
    {

		$auth = Zend_Auth::getInstance();
        $this->coursesDb = new App_Model_Courses();
        $this->forumDb = new App_Model_Forum();
        $this->groupingDb = new App_Model_UserGroupMapping();
        $this->groupDataDb = new App_Model_UserGroupData();


        /*
        $islearner = false;

        if ( Cms_Permission::allowed('forum.moderate') === true )
        {
            $islearner = true;
        } */

        // temporary fix !@-@!
        $islearner = true;

		//get id
        $id = $this->_getParam('course');

        $course = $this->coursesDb->fetchRow(array('id = ?' => $id));
		$this->view->course = $course;

		$getcourses = $this->forumDb->getCoursesforum();
		$getcourses->where('a.course_id = ?',$id);

		$db = Zend_Db_Table::getDefaultAdapter();
		$stmt = $db->query(
            'UPDATE forum_thread SET reply_count = (SELECT COUNT(*) FROM forum_reply WHERE thread_id=forum_thread.id)'
        );

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();
			$formData['search'] = trim($formData['search']);

			if ( $formData['search'] != '')
			{
				$getcourses->where('a.name LIKE ? OR f.thread_content LIKE ?', '%'.$formData['search'].'%');
			}

			$this->view->formdata = $formData;
		}

		$forums = Cms_Paginator::query($getcourses);

		//hasenrolled
        $hasenrolled  = false;
        $check = $this->enrolDb->getEnrol(array('user_id = ?' => Zend_Auth::getInstance()->getIdentity()->id, 'course_id = ?' => $course['id']));
        $hasenrolled = !empty($check) ? true : false;

        if ( $hasenrolled == false && $islearner == false )
        {
            return Cms_Render::error('You don\'t have a permission to view this course');
        }

        $this->view->identity = $this->identity;

        // get current user group for this course
        $user_group = $this->groupDataDb->getUserGroup(Zend_Auth::getInstance()->getIdentity()->id, $id);
        $group_permission = array();

        foreach ($forums as $index => $ann)
        {
            // check content grouping
            if (!$this->groupingDb->checkContentPermission($ann['id'], $id, "forum")) // the content is tagged to a group
            {
                // content grouping
                $group = $this->groupingDb->getContentGroup($ann['id'], $id, $user_group['group_id'], 'forum'); // check against the learner's group

                if (empty($group)) {    
                    // it is tagged to other group. not the learner. unset the var so it will not be displayed
                    $group_permission[$index]["permission"] = 0;
                } else {
                    // tag the content to the group
                    $group_permission[$index]["permission"] = 1;
                    $group_permission[$index]["specific_group"] = $group["group_id"];
                    $group_permission[$index]["specific_group_name"] = $group['group_name'];
                }
            }
            else
            {
                // it is tagged for all. no checking needed. moving on
                $group_permission[$index]["permission"] = 1;
            }
        }
        $this->view->results = $forums;
        $this->view->permission = $group_permission;

    }

	public function forumAddAction()
	{
		$auth = Zend_Auth::getInstance();

        $this->coursesDb = new App_Model_Courses();
        $this->forumDb = new App_Model_Forum();
		$this->forumthreadDb = new App_Model_ForumThread();
        $this->filesDb = new App_Model_DataFiles();



        $islearner = false;

        if ( Cms_Permission::allowed('forum.moderate') === true )
        {
            $islearner = true;
        }

		//get id
		$fullname = $auth->getIdentity()->firstname.' '.$auth->getIdentity()->lastname;
        $id = $this->_getParam('course');

        $course = $this->coursesDb->fetchRow(array('id = ?' => $id));
		$this->view->course = $course;

		$form = new Admin_Form_Forum();

		$this->view->title = "New Forum";
        $this->view->course_id = $id;


		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();


            //form 1
			$fail = 0;

			$data = array(
					'name'      	=> $formData['name'],
					'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'created_by'    => $auth->getIdentity()->id,
					'course_id'		=> $id,
					);

			$forum_id = $this->forumDb->insert($data);
            //upload file
            $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');
            //pr($files);exit;
            
            $filesDb = new App_Model_DataFiles();

            if ( !empty($files) )
            {
                foreach ( $files as $file )
                {
                    $fileData = array(
                                        'parent_id' => $forum_id,
                                        'file_type' => 'forumfile',
                                        'file_name' => $file['filename'],
                                        'file_url'  => Cms_System_Links::dataFile($file['fileurl'], false),
                                        'file_size' => $file['filesize'],
                                        'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                                        'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
                    );

                    $filesDb->insert($fileData);

                }
            }
            //form 2
			$data = array(
						'thread_subject'	=> $formData['name'],
						'thread_content'	=> $formData['description'],
                        'forum_id'       => $forum_id,
                        'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'    => $auth->getIdentity()->id
                );

			$this->forumthreadDb->insert($data);
			
            
            // add groups
            $groupDb = new App_Model_UserGroupMapping();
            $db = $groupDb->getDefaultAdapter();
            
            foreach ($formData['group_id'] as $group) {
                $data = array(
                            'map_type'  => 'forum',
                            'group_id'  => $group,
                            'course_id' => $id,
                            'mapped_id' => $forum_id
                    );
                //pr($data);exit;
                $groupDb->addMappingData($data);
            }

			Cms_Common::notify('success','Forum successfully created');
			$link = $this->view->baseUrl()."/forum/forum/course/".$id;
			$this->redirect( $link );

		}

		$this->view->form = $form;

		//breadcrumbs
        $bc = array(
            'home',
            array($this->view->baseUrl().'/courses/' . $course['code_safe'] . '/learn' => $course['title']),
			array($this->view->baseUrl().'/forum/forum/course/'.$id => $this->view->translate('Discussion')),
        );
		$this->view->bc = $bc;
	}

	 public function forumThreadAction()
	{
		$auth = Zend_Auth::getInstance();
   		$this->coursesDb = new App_Model_Courses();
   		$this->forumDb = new App_Model_Forum();
		$this->forumthreadDb = new App_Model_ForumThread();
		$this->forumreplyDb = new App_Model_ForumReply();
        $this->groupDataDb = new App_Model_UserGroupData();
        $this->groupingDb = new App_Model_UserGroupMapping();

        $islearner = false;

        if ( Cms_Permission::allowed('forum.moderate') === true )
        {
            $islearner = true;
        }

		//get id
   		$id = $this->_getParam('course');
		$forum = $this->_getParam('forum');

		//courses
		$course = $this->coursesDb->fetchRow(array('id = ?' => $id));
		$this->view->course = $course;

		//forum title
		$forumt = $this->forumDb->fetchRow(array('id = ?' => $forum));
		$this->view->forumt = $forumt;

		//forum thread
		$forumthread = $this->forumthreadDb->fetchRow(array('forum_id = ?' => $forum));
		$forum_thread = $forumthread['forum_id'];
		$this->view->forumthread = $forumthread;

		$this->view->forum = $forum;
		$this->view->id = $id;

		$getthread = $this->forumthreadDb->getThreads();
		$getthread->where('a.forum_id = ?',$forum_thread.'AND b.course_id = ?',$id );

		$getthreadreply = $this->forumreplyDb->getThreadsreply();
		$getthreadreply ->where('d.forum_id = ?',$forum_thread.'AND b.course_id = ?',$id );

		if ($this->getRequest()->isPost()) {

			$formData = $this->getRequest()->getPost();
			$formData['search'] = trim($formData['search']);

			if ( $formData['search'] != '')
			{
				$getthreadreply->where('reply_content LIKE ?', '%'.$formData['search'].'%');
			}

			$this->view->formdata = $formData;
		}

		$threads = Cms_Paginator::query($getthread);
		$resultreply_paginator = Cms_Paginator::query($getthreadreply);
        $resultreply = array();
        $dataFileDb  = new App_Model_DataFiles();

        $reply_ids = array();
        foreach($resultreply_paginator as $i => $row) {
            $reply_id    = $row['reply_id'];
            $reply_ids[] = $reply_id;
            $row['attachments']     = array();
            $resultreply[$reply_id] = $row;
        }

        $attachments = array();

        if(!empty($reply_ids)) {
            $attachments = $dataFileDb->getDataAll(array(
                'parent_id IN (?)' => $reply_ids,
                'file_type = ?'    => 'forumreplyfile'
            ));
        }
        
        foreach($attachments as $i => $row) {
            $reply_id = $row['parent_id'];
            $resultreply[$reply_id]['attachments'][] = $row;
        }

		$this->view->results = $threads;
		$this->view->resultsreply = $resultreply;
        $this->view->resultreply_paginator = $resultreply_paginator;

		//breadcrumbs
        $bc = array(
            'home',
            array($this->view->baseUrl().'/courses/' . $course['code_safe'] . '/learn' => $course['title']),
			array($this->view->baseUrl().'/forum/forum/course/'.$id => $this->view->translate('Discussion')),
			array($this->view->baseUrl().'/forum/forum/course/'.$id => $forumt['name'] ),
        );
		
        $this->view->bc = $bc;
        $filesDb = new App_Model_DataFiles();
        $contentdata = $filesDb->fetchAll(array('file_type = ?' => 'forumfile' ,'parent_id = ?' => $forum));
        $contentdatareply = $filesDb->fetchAll(array('file_type = ?' => 'forumreplyfile' ,'parent_id = ?' => '64'));
        //$contentdata = $filesDb->fetchAll(array('file_type = ?' => 'blockfile', 'parent_id = ?' => $blockcontent['data_id']));
        $this->view->contentdata = $contentdata;

        $this->view->identity = $this->identity;

        // get current user group for this course
        $user_group = $this->groupDataDb->getUserGroup(Zend_Auth::getInstance()->getIdentity()->id, $id);
        $group_permission = array();

        foreach ($threads as $index => $ann)
        {
            // check content grouping
            if (!$this->groupingDb->checkContentPermission($ann['thread_id'], $id, "forum")) // the content is tagged to a group
            {
                // content grouping
                $group = $this->groupingDb->getContentGroup($ann['thread_id'], $id, $user_group['group_id'], 'forum'); // check against the learner's group

                if (empty($group)) {    
                    // it is tagged to other group. not the learner. unset the var so it will not be displayed
                    $group_permission[$index]["permission"] = 0;
                } 
                else 
                {
                    // tag the content to the group
                    $group_permission[$index]["permission"] = 1;
                    $group_permission[$index]["specific_group"] = $group["group_id"];
                    $group_permission[$index]["specific_group_name"] = $group['group_name'];
                }
            }
            else
            {
                // it is tagged for all. no checking needed. moving on
                $group_permission[$index]["permission"] = 1;
            }
        }

        $this->view->permission = $group_permission;
        $this->view->contentdatareply = $contentdatareply;
	}

	public function forumReplyAction()
		{
		$auth = Zend_Auth::getInstance();
        $this->coursesDb = new App_Model_Courses();
        $this->forumDb = new App_Model_Forum();
		$this->forumthreadDb = new App_Model_ForumThread();
		$this->forumreplyDb = new App_Model_ForumReply();

        $islearner = false;

        if ( Cms_Permission::allowed('forum.moderate') === true )
        {
            $islearner = true;
        }

		//get id
        $id = $this->_getParam('course');
		$forum_id = $this->_getParam('forum');
		$thread_id = $this->_getParam('id');

        $course = $this->coursesDb->fetchRow(array('id = ?' => $id));
		$this->view->course = $course;

		$form = new Admin_Form_Forum();

		$pid = $this->_getParam('pid');

		$this->view->title = "New Forum";

		$forumthread = $this->forumthreadDb->fetchRow(array('id = ?' => $thread_id));
		$threadsubject = $forumthread['thread_subject'];
		$this->view->threadsubject = $threadsubject;


		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			//forum reply
			$data = array(
					'message'	=> $formData['name'],
					'thread_id'      	=> $thread_id,
					'reply_content'	=> $formData['description'],
					'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
					'created_by'    => $auth->getIdentity()->id,
					);


			$reply_id = $this->forumreplyDb->insert($data);

            //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');
                //pr($files);exit;
                
                $filesDb = new App_Model_DataFiles();

                if ( !empty($files) )
                {
                    foreach ( $files as $file )
                    {
                        $fileData = array(

                                            'parent_id' => $reply_id,
                                            'file_type' => 'forumreplyfile',
                                            'file_name' => $file['filename'],
                                            'file_url'  => Cms_System_Links::dataFile($file['fileurl'], false),
                                            'file_size' => $file['filesize'],
                                            'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
                        );

                        $filesDb->insert($fileData);

                    }
                }

			Cms_Common::notify('success','Forum successfully created');
			$link = $this->view->baseUrl()."/forum/forum-thread/course/".$id."/forum/".$forum_id;
			$this->redirect( $link );

		}

		$this->view->form = $form;
		//breadcrumbs
        $bc = array(
            'home',
			array($this->view->baseUrl().'/forum/forum-thread/course/'.$id.'/forum/'.$forum_id => $this->view->translate('Discussion')),
			array($this->view->baseUrl().'/forum/forum-thread/course/'.$id.'/forum/'.$forum_id => $forumthread['thread_subject'] ),
			array($this->view->baseUrl().'/forum/forum-thread/course/'.$id.'/forum/'.$forum_id => 'RE :'.$forumthread['thread_subject'] ),
        );
		$this->view->bc = $bc;

        $this->view->identity = $this->identity;
	}

	public function forumReplyEditAction()
	{
		$auth = Zend_Auth::getInstance();
        $this->coursesDb = new App_Model_Courses();
        $this->forumDb = new App_Model_Forum();
		$this->forumthreadDb = new App_Model_ForumThread();
		$this->forumreplyDb = new App_Model_ForumReply();

		//get id
        $id = $this->_getParam('course');
		$forum_id = $this->_getParam('forum');
		$thread_id = $this->_getParam('tid');
		$reply_id = $this->_getParam('id');

        $course = $this->coursesDb->fetchRow(array('id = ?' => $id));
		$this->view->course = $course;

		$form = new Admin_Form_Forum();

		$this->view->title = "New Forum";

		$forummsg = $this->forumDb->fetchRow(array('id = ?' => $forum_id));
		$forumthread = $this->forumreplyDb->fetchRow(array('id = ?' => $reply_id));
		$message = $forumthread['message'];
		$reply_content = $forumthread['reply_content'];
		$this->view->message = $message;
		$this->view->reply_content = $reply_content;

		//print_r($thread_id);exit;
		//echo "<pre>";print_r($forummsg);exit;

		//$message=array(
						//'name'      	=> test post,
						//'description'	=> this is test post,

			//);

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			//if ($form->isValid($formData))
			//{
				$fail = 0;

				//print_r($course_id);exit;

				//forum reply
				$data = array(
						'message'	=> $formData['name'],
						'thread_id'     => $thread_id,
						'reply_content'	=> $formData['description'],
						'modified_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'modified_by'    => $auth->getIdentity()->id,
						);

				$this->forumreplyDb->update($data, array('id = ?' => $reply_id));
                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');
                
                $filesDb = new App_Model_DataFiles();

                if ( !empty($files) )
                {
                    foreach ( $files as $file )
                    {
                        $fileData = array(
                                            'parent_id' => $reply_id,
                                            'file_type' => 'forumreplyfile',
                                            'file_name' => $file['filename'],
                                            'file_url'  => Cms_System_Links::dataFile($file['fileurl'], false),
                                            'file_size' => $file['filesize'],
                                            'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
                        );

                        $filesDb->insert($fileData);

                    }
                }

				Cms_Common::notify('success','Forum successfully created');
				$link = $this->view->baseUrl()."/forum/forum-thread/course/".$id."/forum/".$forum_id;
				$this->redirect( $link );
			//}
		}

		$this->view->form = $form;
		//breadcrumbs
        $bc = array(
            'home',
			array($this->view->baseUrl().'/forum/forum-thread/course/'.$id.'/forum/'.$forum_id => $this->view->translate('Discussion')),
			array($this->view->baseUrl().'/forum/forum-thread/course/'.$id.'/forum/'.$forum_id => $forummsg['name'] ),
			array($this->view->baseUrl().'/forum/forum-thread/course/'.$id.'/forum/'.$forum_id => $message ),
			//array($this->view->baseUrl().'/forum/forum-thread/course/'.$id.'/forum/'.$forum_id => 'RE :'.$forumthread['thread_subject'] ),
        );
		$this->view->bc = $bc;
		}

	public function forumThreadEditAction()
	{
		$auth = Zend_Auth::getInstance();

        $this->coursesDb = new App_Model_Courses();
        $this->forumDb = new App_Model_Forum();
		$this->forumthreadDb = new App_Model_ForumThread();

        $form = new Admin_Form_Forum();

        $islearner = false;

        if ( Cms_Permission::allowed('forum.moderate') === true )
        {
            $islearner = true;
        }

		//get id
        $id = $this->_getParam('course');
		$forum_id = $this->_getParam('forum');
		$thread_id = $this->_getParam('tid');
        $pid = $this->_getParam('pid');

        $forum = $this->forumDb->getForum(array('id' => $forum_id));
        $forumthread = $this->forumthreadDb->fetchRow(array('id = ?' => $thread_id));
        $course = $this->coursesDb->fetchRow(array('id = ?' => $id));

		$message = $forumthread['thread_subject'];
		$thread_content = $forumthread['thread_content'];

        $this->view->forum = $forum;
		$this->view->message = $message;
		$this->view->thread_content = $thread_content;
        $this->view->course = $course;
        $this->view->title = "New Forum";

		//process post
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            //pr($formData);exit;

                //forum thread edit
				$data = array(
						'thread_subject'	=> $formData['name'],
						'thread_content'	=> $formData['description'],
						'modified_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
						'modified_by'    => $auth->getIdentity()->id
						);
            
                //upload file
  				//$files = Cms_Common::uploadFiles($this->uploadDir);
                //print_r($files);exit;
            
                $this->forumthreadDb->update($data, array('id = ?' => $thread_id));
                $this->forumDb->update(array('name' => $formData['name']), array('id = ?' => $forum_id));
                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');
                //pr($files);exit;
                
                $filesDb = new App_Model_DataFiles();

                if ( !empty($files) )
                {
                    foreach ( $files as $file )
                    {
                        $fileData = array(
                                            'parent_id' => $forum_id,
                                            'file_type' => 'forumfile',
                                            'file_name' => $file['filename'],
                                            'file_url'  => Cms_System_Links::dataFile($file['fileurl'], false),
                                            'file_size' => $file['filesize'],
                                            'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
                        );

                        $filesDb->insert($fileData);

                    }
                }

                //upload file
                $files = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');
                //pr($files);exit;
                
                $filesDb = new App_Model_DataFiles();

                if ( !empty($files) )
                {
                    foreach ( $files as $file )
                    {
                        $fileData = array(
                                            'parent_id' => $forum_id,
                                            'file_type' => 'forumfile',
                                            'file_name' => $file['filename'],
                                            'file_url'  => Cms_System_Links::dataFile($file['fileurl'], false),
                                            'file_size' => $file['filesize'],
                                            'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
                        );

                        $filesDb->insert($fileData);

                    }
                }

				Cms_Common::notify('success','Forum successfully updated');
				$link = $this->view->baseUrl()."/forum/forum-thread/course/".$id."/forum/".$forum_id;
				$this->redirect( $link );
			
		}

        $filesDb = new App_Model_DataFiles();
        //$contentdata = $filesDb->fetchAll(array('file_type = ?' => 'forumfile' ,'parent_id = ?' => $thread_id));
        $contentdata = $filesDb->fetchAll(array('file_type = ?' => 'forumfile' ,'parent_id = ?' => $forum_id));
        //$contentdata = $filesDb->fetchAll(array('file_type = ?' => 'blockfile', 'parent_id = ?' => $blockcontent['data_id']));

        $this->view->contentdata = $contentdata;
        $this->view->form = $form;

		//breadcrumbs
        $bc = array(
            'home',
            array($this->view->baseUrl().'/courses/' . $course['code_safe'] . '/learn' => $course['title']),
			array($this->view->baseUrl().'/forum/forum-thread/course/'.$id.'/forum/'.$forum_id => $this->view->translate('Discussion')),
			array($this->view->baseUrl().'/forum/forum-thread/course/'.$id.'/forum/'.$forum_id => $message ),
			//array($this->view->baseUrl().'/forum/forum-thread/course/'.$id.'/forum/'.$forum_id => 'RE :'.$forumthread['thread_subject'] ),
        );
		
        $this->view->bc = $bc;
	}

	public function forumDeleteAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

        $this->coursesDb = new App_Model_Courses();
        $this->forumDb = new App_Model_Forum();
		$this->forumthreadDb = new App_Model_ForumThread();
		$this->forumreplyDb = new App_Model_ForumReply();

        $islearner = false;

        if ( Cms_Permission::allowed('forum.moderate') === true )
        {
            $islearner = true;
        }

		//get id
    $id = $this->_getParam('course');
		$forum_id = $this->_getParam('forum');
		$thread_id = $this->_getParam('tid');
		$reply_id = $this->_getParam('id');

        $reply = $this->coursesDb->fetchRow(array('id = ?' => $reply_id));

		//delete
		$this->forumreplyDb->delete(array('id = ?' => $reply_id));

		Cms_Common::notify('success','Curriculum successfully deleted');
		$link = $this->view->baseUrl()."/forum/forum-thread/course/".$id."/forum/".$forum_id;
		$this->redirect($link);
	}


	public function forumDeleteallAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

        $this->coursesDb = new App_Model_Courses();
        $this->forumDb = new App_Model_Forum();
		$this->forumthreadDb = new App_Model_ForumThread();
		$this->forumreplyDb = new App_Model_ForumReply();

        $islearner = false;

        if ( Cms_Permission::allowed('forum.moderate') === true )
        {
            $islearner = true;
        }

		//get id
        $id = $this->_getParam('course');
		$forum_id = $this->_getParam('forum');
		$thread_id = $this->_getParam('tid');
		//print_r($id);print_r($forum_id);print_r($thread_id);exit;

		//delete all reply
		$this->forumreplyDb->delete(array('thread_id = ?' => $thread_id));

		//delete thread
		$this->forumthreadDb->delete(array('id = ?' => $thread_id));

		//delete discussion
		$this->forumDb->delete(array('id = ?' => $forum_id));


		Cms_Common::notify('success','Curriculum successfully deleted');
		$link = $this->view->baseUrl()."/forum/forum/course/".$id;
		$this->redirect($link);
	}

    public function getForumGroupAction() 
    {
        if ($this->getRequest()->isPost()) 
        {
            $formData = $this->getRequest()->getPost();

            // get groups for this course
            $groupDb = new App_Model_UserGroup();
            $db = $groupDb->getDefaultAdapter();

            $groupdata = $db->select()
                            ->from(array('a' => 'user_group'))
                            ->joinLeft(array(
                                'c' => 'user_group_mapping'), 
                                '(a.group_id = c.group_id AND c.map_type = "forum" AND c.mapped_id = "' . @$formData['forum_id'] . '")', 
                                array('c.map_id'))
                            ->where('a.course_id = ?', $formData['course_id'])
                            ->group('a.group_id')
                            ->order('a.group_name');

            $groups = $db->fetchAll($groupdata);

            die(json_encode($groups));
        }
    }

    public function forumChangeStatusAction() 
    {
        $course_id = $this->_getParam('course');
        $forum_id = $this->_getParam('forum');

        // get forum data
        $forumDb = new App_Model_Forum();
        $db = $forumDb->getDefaultAdapter();

        $select = $db->select()
                    ->from(array('a' => 'forum'))
                    ->where('a.id = ?', $forum_id);

        $forum = $db->fetchRow($select);

        $new_status = (($forum['display_only'] == 1) ? "re-opened" : "closed");
        $data['display_only'] = (($forum['display_only'] == 1) ? 0 : 1);

        $result = $db->update('forum', $data, array('id = ?' => $forum_id));

        Cms_Common::notify('success','Forum successfully ' . $new_status);
        $this->redirect($this->view->baseUrl()."/forum/forum/course/".$course_id);
    }

}
