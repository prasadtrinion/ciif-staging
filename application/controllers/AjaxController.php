<?php

header('Access-Control-Allow-Origin: *');


class AjaxController extends Zend_Controller_Action
{
    protected $db;

    public function init()
    {
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender();

        $this->db = getDB();
    }

    public function getfeeAction()
{

    //die('getsubtypeAction');
    
    $this->_helper->layout->disableLayout();

     $memberId = $this->_getParam('member_id',null);
     $sub = $this->_getParam('sub',null);
     $year = $this->_getParam('year',null);
     $userid = $this->_getParam('id',null);
      $dbUser = new App_Model_User;
        $getExpiry= $dbUser->getExpiry($userid);

      
        $nationality = $getExpiry['nationality'];
         $date = date('Y-m-d', strtotime($getExpiry['mr_expiry_date']));
    $query =  new Admin_Model_DbTable_MemberRenewFee();

    $memberdata = $query->getrenewalfees($memberId,$sub,$year,$date,$nationality);
    $json = Zend_Json::encode($memberdata);
    print_r($json);

     exit();
}

    public function validiateProgramAction()
{
    $this->_helper->layout->disableLayout();
    $programDb = new App_Model_ProgramCategory();

    $program_route = $this->_getParam('program_route',null);
    $program_level = $this->_getParam('program_level',null);
    $program_category = $this->_getParam('program_category',null);

    $programdata = $programDb->getValidProgram($program_route,$program_level,$program_category);

    
     if(empty($programdata))
     {
        echo 0;
     }else{
        // $programdata = $programDb->getprogramdata($program_route,$program_level);
         $json = Zend_Json::encode($programdata);
         print_r($json);
     }
     exit();
}


      public function getbyprogramAction()
{
    $this->_helper->layout->disableLayout();
    $programDb = new App_Model_ProgramCategory();

    $program_route = $this->_getParam('program_route',null);
    $program_level = $this->_getParam('program_level',null);
    $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;

    $programdata = $programDb->getByProgram($program_route,$program_level,$user_id);

    
     if(empty($programdata))
     {
        echo 0;
     }else{
        echo 1;
     }
     exit();
}

    public function scormAction()
    {
        $id = $this->getParam('contentid');
        $scormDb = new App_Model_DataScorm();

        $contentdata = $scormDb->fetchRow(array('id = ?' => $id))->toArray();

        $url = DOCUMENT_URL .$contentdata['url'] . '/imsmanifest.xml';
        $url = str_replace(' ', '%20', $url);
        $get = file_get_contents( $url );


        header("Content-type: text/xml");
        die ( $get );
    }

    public function scheduleAction()
    {
        if ( !Cms_Auth::isLoggedIn() )
        {
            throw new Exception('You must be logged in to perform this action');
        }

        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            $currDb = new App_Model_Curriculum();
            $courseDb = new App_Model_Courses();
            $dataDb = new Admin_Model_DbTable_ExternalData();
            $learningmodeTypes = $dataDb->getData('LearningMode');

            $courses = $courseDb->getCourses('a.created_date DESC',true);
            $courseByCode = array();
            foreach ( $courses as $course )
            {
                $courseByCode[ $course['code'] ] = $course;
            }

            $learningmodeTypes = array_column($learningmodeTypes, 'value', 'code');

            $regtype = $formData['regtype'];

            if ( empty($regtype) ) die('Cannot fetch schedule #29');

            if ( $regtype == 'curriculum' )
            {
                //$curr = $currDb->fetchRow();
                $curr = $currDb->getCurriculum(array('id = ?' => $formData['id']));
                if (empty($curr)) {
                    return Cms_Render::error('Invalid ' . Cms_Options::__('label_curriculum'));
                }

                //$curr = $curr->toArray();

                /*
                //scheme & landscape
                $params = array(
                    'IdProgram' => $curr['external_id'],
                    'IdMode' => 577,
                );
                $externalData = Cms_Curl::__(SMS_API . '/get/Registration/do/externalData', $params);
                if (empty($externalData)) {
                    return Cms_Render::error('Error Fetching Data. #extd');
                }*/

                $params = array('IdProgram' => $curr['external_id'], 'courses' => $formData['courses']);
                $schedule = Cms_Curl::__(SMS_API . '/get/Schedule/do/getSchedule', $params);
            }
            else
            {
                $course = $courseDb->getCourse(array('a.id = ?' => $formData['id']));
                if (empty($course)) {
                    return Cms_Render::error('Invalid Course');
                }

                $curr = $currDb->getCurriculum(array('id = ?' => $formData['cid']));
                if (empty($curr)) {
                    return Cms_Render::error('Invalid ' . Cms_Options::__('label_curriculum'));
                }

                $params = array('IdSubject'=>$course['external_id'],'IdProgram'=>$curr['external_id']);
                $schedule = Cms_Curl::__(SMS_API . '/get/Schedule/do/getScheduleSingle', $params);

            }

            $this->courses = $courseByCode;
            $this->data = $schedule;

            if(isset($this->data['courses'])) {
                foreach ($this->data['courses'] as $course) {//echo $course ['IdSubject'];
                    if (isset($this->data['schedule'][$course['IdSubject']])) {
                        foreach ($this->data['schedule'][$course['IdSubject']] as $schedule) {
                            foreach ($schedule as $key => $class) {
                                if (strtotime($class['sc_date_start']) < strtotime(date('Y-m-d'))) {
                                    unset($this->data['schedule'][$course['IdSubject']]['listGroup'][$key]);
                                }
                                /*foreach ( $schedule as $class ) {
                                    if (strtotime($class['sc_date_start']) < strtotime(date('Y-m-d'))) {
                                        unset($this->data['schedule'][$course['IdSubject']]);
                                    }
                                }*/
                            }
                        }
                    }
                }
            }
            //pr($this->data);
            //$this->view->courses = $courseByCode;
            //$this->view->data = $schedule;

            $this->view->schedule_data = json_decode($formData['schedule_data'], true); // mcm x logik. tp nk try gak
            $this->view->courses = $this->courses;
            $this->view->data = $this->data;
            $this->view->post_id = $formData['id'];

        }
    }

    public function couponAction()
    {
        $this->session = new Zend_Session_Namespace('register');

        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            $params = array (
                        'couponCode'    => $formData['couponCode'],
                        'regtype'       => $this->session->cart[0]['regtype'],
                        'id'            => $this->session->cart[0]['id'],
                        'external_id'   => $this->session->cart[0]['external_id'],
                        'code'          => $this->session->cart[0]['code'],
                        'price'         => $formData['totalprice'],
                        'currency'      => $formData['currency']
                    );

            $couponResponse = Cms_Curl::__(SMS_API . '/get/Registration/do/getCouponCode', $params);

            if ($couponResponse['error_message'] == '')
            {
                $this->session->cart[0]['coupon_id'] = $couponResponse['coupon_id'];
                $this->session->cart[0]['coupon_code'] = $formData['couponCode'];

                if ($couponResponse['calculation_type'] == 'Percentage')
                {
                    $total = $formData['totalprice'] * ($couponResponse['calculation_amount'] / 100);
                }
                else
                {
                    $total = $couponResponse['calculation_amount'];
                }

                $this->session->cart[0]['coupon_amount'] = $total;
            }

            die(json_encode($couponResponse));
        }
        exit;
    }

    public function addCourseContentAction()
    {

        if ( !Cms_Auth::isLoggedIn() )
        {
            // return false;
        }

        $course_id = $this->getParam('course');

        // get this content grouping
        $groupdata = $this->db->select()
                ->from(array('a' => 'user_group'))
                ->where('a.course_id = ?', $course_id)
                ->order('a.group_name');

        $groups = $this->db->fetchAll($groupdata);

        $this->view->groups = $groups;

    }

    public function contentArrangeAction()
    {

        if ( !Cms_Auth::isLoggedIn() )
        {
            // return false;
        }

        $course_id = $this->getParam('course');
        $block_id = $this->getParam('blockId');

        $blockDb = new App_Model_ContentBlock();
        $blockDataDb = new App_Model_ContentBlockData();

        // process post
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();
            $choice = explode('&', $formData['choices'][0]);

            $i=1;
            foreach($choice as $sort)
            {
                $sort = str_replace('sort=', '', $sort);

                $data = array('arrangement' => $i);
                $where = array('block_id = ?' => $block_id, 'data_id = ?' => $sort);

                $blockDataDb->updateArrangement( $data, $where );

                $i++;
            }
            exit;
        }

        // get block & content
        $blocks = $blockDb->fetchRow(array('id = ?' => $block_id))->toArray();
        $contentData = $blockDataDb->getByBlock($course_id);

        // get this content grouping
        $groupDb = new App_Model_UserGroupMapping();
        $groups = $groupDb->getContentGrouping($course_id);

        $this->_helper->layout()->setLayout('/misc/content');

        $this->view->course_id = $course_id;
        $this->view->block_id = $block_id;
        $this->view->block = $blocks;
        $this->view->content = $contentData[$block_id];
        $this->view->groups = $groups;
    }

    public function apiAction()
    {
    	$auth = Zend_Auth::getInstance();
    	  
    	$smsExamDB = new App_Model_SmsExam();
    	
        $get = $this->getParam('get');
        
        $formData = $this->getRequest()->getPost();
        
        $log['invoice_id'] = $formData['invoiceId'];
        $log['function_name'] = 'enter apiAction()';
        $log['params'] = 'get:'.$get;
        $log['created_at']=date('Y-m-d H:i:s');
        $log['created_by']=$auth->getIdentity()->external_id;
        $smsExamDB->addLogData('drop_registration_log',$log);

        
        
        if ( $get == 'amount' )
        {
            $params = array(
                                'IdMode'    => $formData['IdMode'],
                                'IdProgram' => $formData['IdProgram'],
                                'IdProgramScheme' => $formData['IdProgramScheme'],
                                'Category'  => $formData['Category'],
                                'IdSubject' => $formData['IdSubject'],
                                'totalStudent' => $formData['totalStudent'],
                                'schedule' => $formData['schedule']
            );

            $response = Cms_Curl::__(SMS_API.'/get/Finance/do/getAmount', $params);
        }

        if ( $get == 'discount' )
        {
            $params = array(
                'categoryId' => $formData['categoryId'],
                'companyId' => $formData['companyId'],
                'totalStudent'  => $formData['totalStudent'],
                'isStudent' => $formData['isStudent'],
                'IdMode'    => $formData['IdMode'],
                'IdProgram' => $formData['IdProgram'],
                'registrationItem' => (isset($formData['registrationItem']) ? $formData['registrationItem'] : 889),
            );

            $response = Cms_Curl::__(SMS_API.'/get/Finance/do/getDiscount', $params);
        }

        if ( $get == 'deleteinvoice' )
        {
        	$log['invoice_id'] = $formData['invoiceId'];
        	$log['function_name'] = 'if ($get == deleteinvoice)';
	        $log['params'] = 'invoiceId:'.$formData['invoiceId'].' from:1';
	        $log['created_at']=date('Y-m-d H:i:s');
	        $log['created_by']=$auth->getIdentity()->external_id;
	        $smsExamDB->addLogData('drop_registration_log',$log);  	
        
            $params = array(
                'invoiceId' => $formData['invoiceId'],
                'from' => 1, //lms
            );

            $response = Cms_Curl::__(SMS_API.'/get/Finance/do/cancelInvoiceId', $params);

            $log['invoice_id'] = $formData['invoiceId'];
            $log['function_name'] = 'curl cancelInvoiceId return response ';
	        $log['params'] = 'response::'.$response['error'];
	        $log['created_at']=date('Y-m-d H:i:s');
	        $log['created_by']=$auth->getIdentity()->external_id;
	        $smsExamDB->addLogData('drop_registration_log',$log);  	
        	       
            //cancel order
            if($response['error'] == 0){

            	$log['invoice_id'] = $formData['invoiceId'];
            	$log['function_name'] = 'after response curl error=0  ';
		        $log['params'] = '';
		        $log['created_at']=date('Y-m-d H:i:s');
		        $log['created_by']=$auth->getIdentity()->external_id;
		        $smsExamDB->addLogData('drop_registration_log',$log);  	
		        
                $orderDb = new App_Model_Order();
                $erDB = new App_Model_Exam_ExamRegistration();

                $orderId = $formData['orderId'];

                //get order info
                $orderInfo = $orderDb->getOrder(array('id = ?' => $orderId));

                if($auth->getIdentity()->login_as){//if admin
                    $modifiedby = 1; //1= tbl_user
                }else{
                    $modifiedby = $auth->getIdentity()->external_id;
                }

                $log['invoice_id'] = $formData['invoiceId'];
                $log['function_name'] = 'after response curl error=0  ';
		        $log['params'] = 'order info regtype:'.$orderInfo['regtype'];
		        $log['created_at']=date('Y-m-d H:i:s');
		        $log['created_by']=$auth->getIdentity()->external_id;
		        $smsExamDB->addLogData('drop_registration_log',$log);  	
		        
                if($orderInfo['regtype'] == 'exam'){
                	
                    $examRegistration = $erDB->getDataByOrder($orderId);
                    
                    $log['invoice_id'] = $formData['invoiceId'];
                    $log['function_name'] = 'if regtype==exam > get er id by order id ';
			        $log['params'] = 'order id:'.$orderId;
			        $log['created_at']=date('Y-m-d H:i:s');
			        $log['created_by']=$auth->getIdentity()->external_id;
			        $smsExamDB->addLogData('drop_registration_log',$log);  	

                    if($examRegistration){
                        foreach($examRegistration as $exam){
                            $update = array(
                                'er_registration_type' => 2,
                                'modifieddt'           => date('Y-m-d H:i:s'),
                                'modifiedby'           => $modifiedby
//                                'modifiedby'           => $auth->getIdentity()->external_id
                            );
                            $erDB->updateData($update, $exam['er_id'], 'Cancel Order (LMS)');
                            
                            $log['invoice_id'] = $formData['invoiceId'];
                            $log['function_name'] = 'where loop update reg type=2 cancel order LMS ';
					        $log['params'] = 'er id:'.$exam['er_id'];
					        $log['created_at']=date('Y-m-d H:i:s');
					        $log['created_by']=$auth->getIdentity()->external_id;
					        $smsExamDB->addLogData('drop_registration_log',$log);  	
                        }
                    }
                }

                //update order
                $orderData = array(
                    'status' => 'CANCELLED',
                    'updBy' => 1,
                    'updDate' => date("Y-m-d H:i:s")
                );
                $orderDb->update($orderData,"id =".$orderId);
                
                $log['invoice_id'] = $formData['invoiceId'];
                $log['function_name'] = 'update order ';
		        $log['params'] = 'order id:'.$orderId;
		        $log['created_at']=date('Y-m-d H:i:s');
		        $log['created_by']=$auth->getIdentity()->external_id;
		        $smsExamDB->addLogData('drop_registration_log',$log);  	

            }
        }

        if ( $get == 'amountexam' )
        {
            $params = array(
                'IdMode'    => $formData['IdMode'],
                'IdProgram' => $formData['IdProgram'],
                'IdProgramScheme' => $formData['IdProgramScheme'],
                'Category'  => $formData['Category'],
                'totalStudent' => $formData['totalStudent'],
                'ExamOption' => $formData['ExamOption']
            );

            $response = Cms_Curl::__(SMS_API.'/get/Finance/do/getAmountExam', $params);
        }

        die(json_encode($response));
    }

    public function addfriendAction()
    {
        $form = new App_Form_User();

        $form->firstname->setAttrib('required','required');
        $form->lastname->setAttrib('required','required');
        $form->email->setAttrib('required','required');

        $session = new Zend_session_Namespace('register');


        $events = Cms_Events::trigger('auth.register.form', $this, array('form'=>$form));
        if ( !empty($events) )
        {
            foreach ( $events as $ev )
            {
                Cms_Hooks::push('registration.extra', $ev);
            }
        }

        $form->company->setRequired(false);
        $this->view->form = $form;

        if ( !Cms_Auth::isLoggedIn() )
        {
            throw new Exception('You must be logged in to perform this action');
        }

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ( !empty($formData) )
            {
                if ( !isset($session->guests) )
                {
                    $session->guests = array();
                }

                $formData['birthdate'] = date('Y-m-d', strtotime($formData['dob_year'].'-'.$formData['dob_month'].'-'.$formData['dob_day']));

                unset($formData['dob_year']);
                unset($formData['dob_month']);
                unset($formData['dob_day']);

                $key = Cms_Common::generateRandomString(6);
                $formData['unique'] = $key;

                $session->guests[] = $formData;

                //return json
                $json = array(
                                'name'  => $formData['firstname'].' '.$formData['lastname'],
                                'email' => $formData['email'],
                                'unique'=> $key
                );

                echo Zend_Json::encode($json);
                exit;

            }
        }
    }

    public function removefriendAction()
    {
        $session = new Zend_session_Namespace('register');

        if ( !Cms_Auth::isLoggedIn() )
        {
            echo Zend_Json::encode(array('error'=>1,'msg'=>'Must be logged in to perform this action'));
            exit;
        }

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ( !empty($formData) )
            {
                if ( !isset($session->guests) )
                {
                    $session->guests = array();
                }

                foreach ( $session->guests as $key => $row )
                {
                    if ( $row['unique'] == $formData['id'] )
                    {
                        unset($session->guests[$key]);
                    }
                }

                $json = array ('error' => 0, 'msg' => 'OK');
                echo Zend_Json::encode($json);
                exit;
            }
        }
    }
    
    public function getStateAction()
    {
        $country_id = $this->getParam('id', 0);
        $list       = $this->getParam('list', 1);
        
        $select = $this->db->select()->from(array('a' => 'tbl_state'))->order('a.StateName ASC');
        
        
        if ($country_id)
        {
            $select->where('a.idCountry = ?', $country_id);
        }
        
        $result   = $this->db->fetchAll($select);
        $response = array();
        
        foreach ($result as $row)
        {
            if ($list)
            {
                $response[$row['idState']] = $row['StateName'];
            }
            else
            {
                $response[] = array(
                    'id'   => $row['idState'],
                    'name' => $row['StateName']
                );
            }
        }
    
        echo Zend_Json::encode($response);
        exit;
    }
    
    public function getCityAction()
    {
        $state_id = $this->getParam('id', 0);
        $list     = $this->getParam('list', 1);
        
        $select = $this->db->select()->from(array('a' => 'city'))->order('a.name ASC');
        
        
        if ($state_id)
        {
            $select->where('a.state_id = ?', $state_id);
        }
        
        $result   = $this->db->fetchAll($select);
        $response = array();
        
        foreach ($result as $row)
        {
            if ($list)
            {
                $response[$row['id']] = $row['name'];
            }
            else
            {
                $response[] = array(
                    'id'   => $row['id'],
                    'name' => $row['name']
                );
            }
        }
        
        echo Zend_Json::encode($response);
        exit;
    }



    public function getsubtypeAction()
    {
        
         //ini_set('display_errors','on');
        
        $this->_helper->layout->disableLayout();



         $memberId = $this->_getParam('member_id',null);
        
        $query =  new Admin_Model_DbTable_MemberGet();

        $memberdata = $query->getmemberdata($memberId);
        $json = Zend_Json::encode($memberdata);
                    
                        print_r($json);
                       // die();

         exit();
    }


    public function getexperienceAction()
    {
        
        $this->_helper->layout->disableLayout();

        $memberId = $this->_getParam('member_id',null);
        $memberSubId = $this->_getParam('member_sub_id',null);
        // $degree_Id = $this->_getParam('degree_id',null);
        
        $query =  new App_Model_MemberExperience();
 
        $memberdata = $query->getDegreeExp($memberId,$memberSubId,$degree_Id);
        $json = Zend_Json::encode($memberdata);
                    
        print_r($json);

        exit();
    }

    public function getfeestructureAction()
    {

        $this->_helper->layout->disableLayout();
        
         $member_id = $this->_getParam('member_id',null);
         $member_subtype = $this->_getParam('member_subtype',null);
         $nation_type = $this->_getParam('nation_type',null);
         // $degree = $this->_getParam('degreeType',null);

    $feeModel =  new App_Model_MemberFeeStructure();

         $result = $feeModel->getTotalamount($member_id,$member_subtype ,$nation_type);
         
         $json = Zend_Json::encode($result);
         
         print_r($json);

        exit();

    }



}
