<?php
require_once('External/recaptcha/autoload.php');

class OnlineappController extends Zend_Controller_Action
{
	protected $events;

    public function init()
    {
        //$this->events = Zend_Registry::get('Events');
		//Cms_Events::attach('auth.login.success', function($e){ print_r($e->getParams()); exit; });
        //events moved to Plugins_App
    }

    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();

        if ($auth->hasIdentity())
        {
            $this->redirect('/dashboard');
        }
    }

    public function registerAction()
    {
      //  $this->_helper->layout()->setLayout('/auth/register');

        $form = new App_Form_User();

        $redirect = $this->getParam('r');

        //toggle password required
        $form->username->setRequired(true);
        $form->password->setRequired(true);

        $events = Cms_Events::trigger('auth.register.form', $this, array('form'=>$form));
        if ( !empty($events) )
        {
            foreach ( $events as $ev )
            {
                Cms_Hooks::push('registration.extra', $ev);
            }
        }

        $fail = 0;

        if ($this->getRequest()->isPost())
        {
            Cms_Common::expire();

            $formData = $this->getRequest()->getPost();
    
            if ($formData['nationality'] == 'MY' && !is_numeric($formData['username']))
            {
                $this->view->errmsg = 'Please enter <strong>NRIC</strong> number as Username';
            }
            elseif ($form->isValid($formData))
            {
                $dbUser = new App_Model_User;

                //security
                $recaptcha = new \ReCaptcha\ReCaptcha(RECAPTCHA_SECRET);
                $resp = $recaptcha->verify($formData['g-recaptcha-response'], Cms_Common::getIp() );
                if ($resp->isSuccess())
                {
                    // verified!
                } else {
                    $fail = 1;
                    $errmsg = empty($resp->getErrorCodes()) ? 'Security Captcha Expired' : implode(', ',$resp->getErrorCodes()) ;
                }

                //check username
                $user = $dbUser->fetchRow(array("username = ?" => $formData['username']));
                if (!empty($user)) {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username already exists. <a href="/forgotpass">Forgot your password?</a>');
                }

                //check email
                $email = $dbUser->fetchRow(array("email = ?" => $formData['email']));
                if (!empty($email)) {
                    $fail = 1;
                    $errmsg = $this->view->translate('Email already in use. <a href="/forgotpass">Forgot your password?</a>');
                }

                //check for illegal characters
                if ( preg_match("/[^a-zA-Z0-9]+/", $formData['username']) && $fail != 1 )
                {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username contains illegal characters. Try again. Only alphanumeric characters are allowed.',0);
                }

                $post = Cms_Events::trigger('auth.register.post', $this, $formData);
                $extraData = array();

                if ( !empty($post) && count($post) > 0 )
                {
                     if ( $post[0]['fail'] == 1 )
                    {
                        $fail = 1;
                        $errmsg = $this->view->translate($post[0]['error']);
                    }
                    else
                    {
                        foreach ( $post[0]['data'] as $key => $value )
                        {
                            $extraData[$key] = $value;
                        }
                    }
                }


                if ( ( strlen($formData['username']) < 3 || strlen($formData['username']) > 25 ) && $fail != 1 )
                {
                    if ( strlen($formData['username']) < 3 )
                    {
                        $fail = 1;
                        $errmsg = $this->view->translate('Username is too short. You need to have at least 3 characters',0);
                    }
                    else
                    {
                        $fail = 1;
                        $errmsg = $this->view->translate('Username is too long. Your username cannot be longer than 25 characters.',0);
                    }
                }

                if ($fail != 1)
                {
                    $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);

                    $data = array(
                                    'username'      => $formData['username'],
                                    'password'      => $hash,
                                    'salt'          => $salt,
                                    'firstname'     => $formData['firstname'],
                                    'lastname'      => $formData['lastname'],
                                    'email'         => $formData['email'],
                                    'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                    'role'          => 'student',
                                    'contactno'     => $formData['contactno'],
                                    'active'        => 1
                    );

                    $data = array_merge($data,$extraData);

                    $user_id = $dbUser->insert($data);

                    $session  = new Zend_Session_Namespace('Registration');
                    $session->setExpirationSeconds(60);
                    $session->formData = $formData;
                    
                    Cms_Events::trigger('auth.register.success', $this, array('id'=>$user_id) );

                    $user = $dbUser->fetchRow(array("id = ?" => $user_id))->toArray();
    
                    //$sp = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewProfile', $user);
                    //$user['sp_id'] = $user['std_id'] = $sp['sp_id'];

                    $user['new'] = 1;

                    //write data into session
                    $auth = Zend_Auth::getInstance();
                    $auth->getStorage()->write( (object) $user);

                    if ( $redirect != '' )
                    {
                        header('location:'. Cms_Common::encrypt($redirect, false));
                        return;
                    }
                    else
                    {
                        $this->redirect($this->view->url(array('module' => 'default','controller'=>'index'), 'default', true));
                    }
                }
                else
                {
                    $this->view->errmsg = $errmsg;
                    $form->populate($formData);

                    //$this->redirect($this->view->url(array('module' => 'default','controller'=>'index','action'=>'register'), 'default', true));
                }
            }
            else
            {
                $this->view->errmsg = 'You must fill in all the required fields.';
            }
        }

        //view
        $this->view->redirect = $redirect;
        $this->view->form = $form;

    }

    public function loginAction()
    {
        $this->_helper->layout()->setLayout('/auth/public');
        $form = new App_Form_Login();
        $form->username->setRequired(true);
        $fail = 0;

        if (Zend_Auth::getInstance()->hasIdentity())
        {
            $this->redirect('/dashboard');
        }

        $redirect = $this->getParam('r');

        $key_exist = $this->getParam('key');

        if ($key_exist) 
        {
            $key = Cms_Common::encrypt($key_exist,false);


            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
             $sql = $dbAdapter->select()
                            ->from(array('a'=>'user'))
                            ->where('username = ?', $key);
            $result = $dbAdapter->fetchRow($sql);

            if (!$result) {
                Cms_Common::notify('error', "Failed to Log-in As : No such user");
                $this->redirect($this->view->url(array('module' => 'index','controller'=>'login'), 'default', true));
            }

            $this->view->key = $key;
        }

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData))
            {
                //remember me?
                if ( isset($formData['rememberme']) )
                {
                    ini_set('session.gc_maxlifetime', 604800);
                    Zend_Session::rememberMe(604800);
                }

                // collect the data from the user
                Zend_Loader::loadClass('Zend_Filter_StripTags');
                $filter = new Zend_Filter_StripTags();
                $username = $filter->filter($this->_request->getPost('username'));
                $password = $filter->filter($this->_request->getPost('password'));

                //process form
                $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

                $authAdapter->setTableName('user')
                    ->setIdentityColumn('username')
                    ->setCredentialColumn('password');
                //->setCredentialTreatment('CONCAT(?,salt)');

                //echo Cms_Common::generateRandomString(22);
                //$options  = array('salt' => $salt);
                //$hash = password_hash($pass, PASSWORD_BCRYPT, $options);

                $dbUser = new App_Model_User;
                $user = $dbUser->fetchRow(array("username = ?" => $username));


                if ( empty($user) )
                {
                    $fail = 1;
                }
                else
                {
                    $password = password_hash($password, PASSWORD_BCRYPT, array('salt'=>$user->salt));
                }

                if ( isset($formData['key'])){
                    $icno = $key;
                }else{
                    $icno = $username;
                }

                // Set the input credential values to authenticate against
                $authAdapter->setIdentity($username);
                $authAdapter->setCredential($password);

                // do the authentication
                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authAdapter);

                if ($result->isValid() && $fail != 1)
                {
                    $data = $authAdapter->getResultRowObject(null, 'password');

                    if (!$data)
                    {
                        $fail = 1;
                    }
                    else
                    {

                        // login on behalf
                        if ( isset($formData['key']))
                        {
                            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                            $sql = $dbAdapter->select()
                                            ->from(array('a'=>'user'))
                                            ->where('username = ?', $key);
                            $result = $dbAdapter->fetchRow($sql);

                            unset($result["password"]);

                            $temp = new stdClass();
                            foreach ($result as $key => $value)
                            {
                                $temp->$key = $value;
                            }

                            $data = $temp;
                        }

                        if($data->role == 'student') {

                            $db2 = getDB2();
                            $selectProfile = $db2->select()
                                ->from(array('a' => 'student_profile'))
                                ->where('a.std_idnumber = ?', $icno);
                            $resultProfile = $db2->fetchRow($selectProfile);

                            $std_id = null;
                            if ($resultProfile) {
                                $std_id = $resultProfile['std_id'];
                            } else {

                                $dataProfile = json_decode(json_encode($data), true);
                                $sp = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewProfile', $dataProfile);
                                $std_id = $sp['sp_id'];
                            }
                            
                            if (!$data->external_id && $std_id)
                            {
                                $db = getDB();
                                $db->update('user', array('external_id' => $std_id), array('id = ?' => $data->id));
                            }

                            $data->external_id = $std_id;
                            $data->sp_id = $std_id;
                        }

                        $auth->getStorage()->write($data);

                        //update external_id = student_profile id
                        $dbUser->update(array(
                            'lastlogin' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'external_id' => $std_id
                        ) , array('id = ?'=>$data->id ));
                        
                        Cms_Events::trigger('auth.login.success', $this, (array) $data);

                        if ( $redirect != '' )
                        {
                            header('location:'. Cms_Common::encrypt($redirect, false));
                            return;
                        }
                        else
                        {
                            $this->redirect($this->view->url(array('module' => 'default','controller'=>'dashboard'), 'default', true));
                        }

                    }

                } else {
                    // failure: clear database row from session
                    $fail = l;
                    $errmsg = 'Login failed. Either username or password is incorrect';
                }

            } else {
                $form->populate($formData);
            }
        }

        if ( $fail )
        {
            Cms_Common::notify('error', (!isset($errmsg) ? 'User not found' : $errmsg)  );
            $this->redirect($this->view->url(array('module' => 'default','controller'=>'index','action'=>'login'), 'default', true));
        }

        $this->view->form = $form;
    }

    public function logoutAction()
    {
        $session = Zend_Auth::getInstance()->getIdentity();
        if(isset($session->login_as) && $session->login_as) {
            self::reLogin($session->login_as);
        }

        $storage = new Zend_Auth_Storage_Session();
        $storage->clear();

        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::ForgetMe();
        Zend_Session::destroy(true);

        Cms_Events::trigger('auth.logout', $this);



        $this->redirect($this->view->url(array('module' => 'default'), 'default', true));
    }

    private function reLogin($id) {
        $userDb = new App_Model_User();
        $user   = $userDb->fetchRow(array('id = ?' => $id));

        if(!$user) {
            return;
        }

        $user = $user->toArray();
        unset($user["password"]);
        unset($user["salt"]);

        $data = new stdClass();

        foreach ($user as $key => $value) {
            $data->$key = $value;
        }

        Zend_Auth::getInstance()->getStorage()->write($data);
        Cms_Events::trigger('auth.login.success', $this, (array) $data);
        Cms_Common::notify('success', 'You are now back as : '. $data->firstname .' '. $data->lastname);
        $this->redirect($this->view->url(array('module' => 'default','controller'=>'dashboard'), 'default', true));
    }

    public function privacyAction()
    {

    }

    public function resetpasswordAction()
    {
        Cms_Common::expire();

        $this->view->errMsg = $this->view->sucessMsg = '';

        Cms_Events::trigger('auth.resetpassword', $this);

        $id = $this->getParam('id');
        $key = $this->getParam('key');

        if ($this->getRequest()->isPost())
        {
            $userDb = new App_Model_User();

            $formData = $this->getRequest()->getPost();

            $formData = Cms_Common::cleanArray($formData);

            if ( Cms_Common::tokenCheck($formData['token']) == false )
            {
                $this->view->errMsg = $this->view->translate('Invalid Request. Try again.');
                return;
            }

            $user = $userDb->getUser($id,'id');

            if( empty($user) )
            {
                $this->view->errMsg = $this->view->translate('Invalid User ID.');
            }
            else
            {
                if ( $user['token'] != $key || $key == ''  )
                {
                    $this->view->errMsg = $this->view->translate('Key doesn\'t match. Please try again.');
                    return false;
                }
            }

            if ( strlen($formData['password']) < 6 )
            {
                $this->view->errMsg = $this->view->translate('Passwords must be at least 6 characters long');
                return false;
            }

            if ( $formData['password'] != $formData['confirmpassword'] )
            {
                $this->view->errMsg = $this->view->translate('Passwords does not match. Please try again.');
                return false;
            }

            //reset password
            $salt =  Cms_Common::generateRandomString(22);
            $options  = array('salt' => $salt);
            $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);

            $userDb->update(array('password'=>$hash,'salt' => $salt), array('id = ?' => $id));

            $this->view->successMsg = $this->view->translate('Your new password has been set. You may log in with your new password.');
        }
    }

    public function forgotpasswordAction()
    {
        $this->view->errMsg = $this->view->sucessMsg = '';

        Cms_Common::expire();

        Cms_Events::trigger('auth.forgot', $this);

        if ($this->getRequest()->isPost())
        {
            $userDb = new App_Model_User();

            $formData = $this->getRequest()->getPost();

            $formData = Cms_Common::cleanArray($formData);

            if ( Cms_Common::tokenCheck($formData['token']) == false )
            {
                $this->view->errMsg = $this->view->translate('Invalid Request. Try again.');
                return;
            }

            $user = $userDb->getUser($formData['username'],'username');

            if( empty($user) )
            {
                $this->view->errMsg = $this->view->translate('Username / IC Number does not exists.');
            }
            else
            {
                $key = Cms_Common::generateRandomString(16);
                $keylink = APPLICATION_URL."/index/resetpassword/id/".$user['id']."/key/".$key;

                $userDb->update(array('token'=>$key), array('id=?'=>$user['id']));

                $message = $this->view->translate('Click the link below to reset your password'). "<br />";
                $message .= $keylink;

                Cms_Events::trigger('auth.forgot.success', $this, array('id'=>$user['id']) );

                //send email

                //(user,title,message)
                //($to,$subject,$message,$toname="",$cc=array(),$bcc=array(), $attachments = array()) 
                $db = getDB2();
                 $data = array(
                        'recepient_email' => $user['email'],
                        'subject' => APP_NAME.' - '.$this->view->translate('Forgot Password'),
                        'content' =>  $message,
                        'date_que' =>  date("Y-m-d H:i:s"),
                        'date_send' =>  date("Y-m-d H:i:s"),
                        'smtp' => 1,


                    );

                $db->insert('email_que',$data);

                 $mail = new Cms_SendMail();
                 $mail->fnSendMail($user['email'], APP_NAME.' - '.$this->view->translate('Forgot Password'), $message);

                $this->view->successMsg = $this->view->translate('An email has been sent to your account ('.$user['email'].'). Follow the instruction to reset your password.');

            }
        }
    }

    public function checkUsernameAction()
    {
        $db = getDB();

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $value = $this->_getParam('username');

        $userDb = new App_Model_User();

        $sql = $userDb->select()->where('username = ?',$value);

        $results = $db->fetchAll( $sql );

        $output = array();

        if ( empty($results) )
        {
            $output['valid'] = 'true';
        }
        else
        {
            $output['valid'] = 'false';
        }

        echo Zend_Json::encode($output);
    }

    public function checkEmailAction()
    {
        $db = getDB();

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $value = $this->_getParam('email');

        $userDb = new App_Model_User();

        $sql = $userDb->select()->where('email =?',$value);


        $results = $db->fetchAll( $sql );

        $output = array();

        if ( empty($results) )
        {
            $output['valid'] = 'true';
        }
        else
        {
            $output['valid'] = 'false';
        }


        echo Zend_Json::encode($output);
    }

    public function aboutAction()
    {

    }

    public function activateAction()
    {
        $c = $this->getParam('c',0);
        $this->view->errMsg = $this->view->sucessMsg = '';

        if ( Zend_Auth::getInstance()->hasIdentity() )
        {
            return Cms_Render::error('You cannot be logged in to perform this action.');
        }

        Cms_Common::expire();

        $key = $this->getParam('key');

        $enrolDb = new App_Model_Enrol();
        $orderDb = new App_Model_Order();
        $orderUserDb = new App_Model_OrderGroupUser();
        $userDb = new App_Model_User();
        $auth = Zend_Auth::getInstance();
        $currCoursesDb = new App_Model_CurriculumCourses();

        $this->view->c = $c;

        if ( $c == 1 )
        {
            $user = $userDb->getUser($key,'token');

            if ( empty($user) )
            {
                return Cms_Render::error('Invalid User');
            }

            $this->view->user = $user;

            if ($this->getRequest()->isPost())
            {
                $formData = $this->getRequest()->getPost();
                $formData = Cms_Common::cleanArray($formData);

                if (Cms_Common::tokenCheck($formData['token']))
                {
                    $salt = Cms_Common::generateRandomString(22);
                    $options = array('salt' => $salt);
                    $hash = @password_hash($formData['password'], PASSWORD_BCRYPT, $options);

                    $data = array(
                        'password' => $hash,
                        'salt' => $salt
                    );

                    $userDb->update($data, array('id = ?' => $user['id']));

                    //all done
                    $this->view->successMsg = $this->view->translate('Your password has been set. You can now login using your IC No. and temporary password.');
                }
            }
        }
        else
        {
            $user = $orderUserDb->getUser(array('token = ?' => $key));

            if ( empty($user) )
            {
                return Cms_Render::error('Invalid User');
            }

            if ( $user['activated'] != 0 )
            {
                return Cms_Render::error('Account already activated. Please <a href="/login">login</a> to view your dashboard.');
            }

            $order = $orderDb->getOrder(array('id = ?' =>$user['order_id']));

            if ( empty($order) )
            {
                return Cms_Render::error('Invalid Order ID');
            }

            if ( $order['status'] != 'ACTIVE' )
            {
                return Cms_Render::error('Order has not been paid yet.');
            }

            $this->view->user = $user;

            if ($this->getRequest()->isPost()) {
                $formData = $this->getRequest()->getPost();
                $formData = Cms_Common::cleanArray($formData);

                if (Cms_Common::tokenCheck($formData['token'])) {
                    $salt = Cms_Common::generateRandomString(22);
                    $options = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);

                    $data = array(
                        'username' => $formData['username'],
                        'password' => $hash,
                        'salt' => $salt,
                        'firstname' => $user['firstname'],
                        'lastname' => $user['lastname'],
                        'email' => $user['email'],
                        'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'role' => 'student',
                        'nationality' => $user['nationality'],
                        'company' => $user['company'],
                        'qualification' => $user['qualification'],
                        'active' => 1
                    );

                    $user_id = $userDb->insert($data);

                    //update balik
                    $data = array(
                        'activated' => 1,
                        'activated_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'user_id' => $user_id,
                        'username' => $formData['username']
                    );

                    $orderUserDb->update($data, array('token = ?' => $key));

                    //enrolkan
                    //register
                    if ($order['regtype'] == 'curriculum') {
                        $courses = $currCoursesDb->getData($order['item_id']);

                        //curriculum kena add juga?
                        $data = array(
                            'curriculum_id' => $order['item_id'],
                            'user_id' => $user_id,
                            'regtype' => 'curriculum',
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => $user_id,
                            'order_id' => $order['id']
                        );

                        $enrolDb->insert($data);

                        //courses
                        foreach ($courses as $course) {
                            $data = array(
                                'course_id' => $course['id'],
                                'user_id' => $user_id,
                                'regtype' => 'course',
                                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by' => $user_id,
                                'order_id' => $order['id']
                            );

                            $enrolDb->insert($data);
                        }
                    } else {
                        $data = array(
                            'course_id' => $order['item_id'],
                            'user_id' => $user_id,
                            'regtype' => 'course',
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => $user_id,
                            'order_id' => $order['id']
                        );

                        $enrolDb->insert($data);
                    }

                    //all done
                    $this->view->successMsg = $this->view->translate('Your account has been created. You can now login with your credentials.');
                } else {
                    return Cms_Render::error('Invalid Security Token. Please try again.');
                }
            }

        }
    }
}

