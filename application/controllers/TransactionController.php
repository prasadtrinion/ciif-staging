<?php 
class TransactionController extends Zend_Controller_Action
{

    public function init()
    {
        $auth = Zend_Auth::getInstance();
        $this->auth = $auth->getIdentity();
        
    }
    	

    public function indexAction()
    {

        Cms_Hooks::push('user.sidebar', self::transactionSidebar('index'));
        $userDb = new App_Model_User();
                
     	$sp = isset($this->auth->external_id) ? $this->auth->external_id : 0;
      
        if(isset($this->auth->login_as)){
            $sp = isset($this->auth->sp_id) ? $this->auth->sp_id : 0;
        }else{
            $sp = isset($this->auth->external_id) ? $this->auth->external_id : 0;
        }
                
        //get application
		$applicantTransactionDB = new App_Model_Records_ApplicantTransaction();
        $transaction = $applicantTransactionDB->getData($sp);
        $this->view->transaction = $transaction;              
    }
    
	public static function transactionSidebar($active = 'application')
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view->active = $active;
        $view->setBasePath(APPLICATION_PATH . '/views/');
        return $view->render('plugins/transaction.phtml');
    }
    
     public function transactionDashboardAction()
    {
    	
    	error_reporting(E_ALL);
		ini_set('display_errors', '1');
		
        $tab = $this->_getParam('tab', 'application');
        $this->view->tab = $tab;

        
    }
}
?>