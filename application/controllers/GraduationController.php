<?php

class GraduationController extends Zend_Controller_Action
{
    public function init()
    {
        $this->orderDb = new App_Model_Order();
        $auth = Zend_Auth::getInstance();
        $this->auth = $auth;

        if (!$auth->hasIdentity()) {
            $this->redirect('/index/login');
        }

        Zend_Layout::getMvcInstance()->assign('nowrap', 1);

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;



    }

    public function indexAction()
    {
        $this->view->title = 'Graduation';

    }

    public static function userSidebar($active = 'graduation')
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view->active = $active;
        $view->setBasePath(APPLICATION_PATH . '/views/');
        return $view->render('plugins/usersidebar.phtml');
    }

    public function attendanceAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();

        $auth = Zend_Auth::getInstance();
        $registration_id = $auth->getIdentity()->external_id;

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;

        $this->view->title = 'Attendance';
        $attendDb = new App_Model_Attendance();
        $errMsg = '';

        $convocationDB = new App_Model_Convocation();
        $convocation = $convocationDB->getConvocationGraduate($this->studentinfo[0]['idStudentRegistration']);
        //pr($convocation); exit;
        $this->view->convocation = $convocation;

        $this->view->allow = true;
        if (isset($convocation)) {
            $date1 = new DateTime("now");
            $date2 = new DateTime($convocation['c_confirm_attendance_enddate']);

            if ($date1 > $date2) {
                $this->view->allow = false;
            }
        }

        //fee
        $this->view->fee = $convocationDB->getConvoFee($convocation['c_id'], 52);
        //pr($this->view->fee);exit;

        $check = $attendDb->getPregrad($this->studentinfo[0]['idStudentRegistration']);

        $this->view->attend = 3; //No records

        if (isset($check)) {
            if ($check['confirm_attend'] == 1) {
                $this->view->attend = 1;
            } else if ($check['confirm_attend'] == 0) {
                $this->view->attend = 2;
            }
        }
        $this->view->check = $check;

        if (empty($check)) {
            $errMsg = 'You have no graduation records.';
        }

        $post_data = $this->_request->getPost();
        if (!empty($post_data)) {
            $db = getDB2();

            $invoice_id = null;

            if ($post_data['confirm_attendance'] == 1) {
                //$invoiceClass = new App_Model_Invoice();
                //$invoice_id = $invoiceClass->generateInvoiceConvocation($this->studentinfo[0]['idStudentRegistration'], $convocation['c_id'], 52);

                $params = array(
                    'studentID' => $this->studentinfo[0]['idStudentRegistration'],
                    'convoID' => $post_data['c_id'],
                    'feeID' => 52

                );

                $invoice_id = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoiceConvocation', $params);
            }

            $data = array(
                'confirm_attend' => $post_data['confirm_attendance'],
                'confirm_by' => $this->studentinfo[0]['idStudentRegistration'],
                'confirm_date' => date("Y-m-d H:i:s"),
                'confirm_role' => $this->auth->getIdentity()->role,
                'invoice_id' => $invoice_id
            );

            $db->update('pregraduate_list', $data, array('idStudentRegistration = ?' => $this->studentinfo[0]['idStudentRegistration']));

            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Updated'));
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'record-verification'), 'default', true));

        }

        $this->view->errMsg = $errMsg;
    }

    public function recordVerificationAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();

        $auth = Zend_Auth::getInstance();
        //$registration_id = $auth->getIdentity()->external_id;

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;

        $convocationDB = new App_Model_Convocation();
        $convocation = $convocationDB->getConvocationGraduate($this->studentinfo[0]['idStudentRegistration']);
        $this->view->convocation = $convocation;
        //pr($convocation);exit;

        $this->view->allow = true;
        if (isset($convocation)) {
            $date1 = new DateTime("now");
            $date2 = new DateTime($convocation['c_record_verification_enddate']);
            if ($date1 > $date2) {
                $this->view->allow = false;
            }
        }

        $checklistDb = new App_Model_Checklist();
        $checklist = $checklistDb->get_checklist($convocation['c_id'], $convocation['IdProgram']);
        //pr($checklist);exit;

        //ingat!! penting
        $graduateDB = new App_Model_Invitation();
        $profile = $graduateDB->getGraduateProfile($this->auth->getIdentity()->external_id);

        $flag_submission = false;
        $flag_count = 0;

        foreach ($checklist as $index => $list) {

            if ($list['type'] == 1) {
                //name
                $checklist[$index]['profile_info'] = $profile['student_name'];
            }
            if ($list['type'] == 2) {
                //ic
                $checklist[$index]['profile_info'] = $profile['std_idnumber'];
            }
            if ($list['type'] == 3) {
                //address
                //$city = ($profile['appl_city']!=99) ? $profile['CityName']:$profile['appl_city_others'];
                //$state = ($profile['appl_state']!=99) ? $profile['StateName']:$profile['appl_state_others'];

                $address = (!empty($profile['std_region'])) ? $profile['std_region'] : '';
                $address .= (!empty($profile['std_remark'])) ? '<br>' . $profile['std_remark'] : '';
                $address .= (!empty($profile['std_location'])) ? '<br>' . $profile['std_location'] : '';
                //$address .= (!empty($profile['appl_postcode'])) ? '<br>'.$profile['appl_postcode'].' '.$city:'';
                //$address .= (!empty($profile['CountryName']))   ? '<br>'.$state.' '.$profile['CountryName']:'<br>'.$state;

                $checklist[$index]['profile_info'] = $address;
            }
            if ($list['type'] == 4) {
                //contact no
                $checklist[$index]['profile_info'] = $profile['std_contact_number'];


            }
            if ($list['type'] == 5) {
                //email
                $checklist[$index]['profile_info'] = $profile['std_email'];
            }

            //check if selected by student
            $isStudentChecklist = $checklistDb->isStudentChecklist($this->studentinfo[0]['idStudentRegistration'], $list['id']);

            if ($isStudentChecklist) {
                //$flag_submission = true;

                if ($isStudentChecklist['psc_status'] != 0) {
                    $checklist[$index]['ischecked'] = true;
                    $checklist[$index]['disabled'] = true;
                    $flag_count = $flag_count + 1;
                } else {
                    $checklist[$index]['ischecked'] = false;
                    $checklist[$index]['disabled'] = false;
                }
                $checklist[$index]['remarks_message'] = htmlentities($isStudentChecklist['remarks_message'], ENT_QUOTES);
            } else {
                $checklist[$index]['ischecked'] = false;
                $checklist[$index]['disabled'] = false;
            }

        }
        //pr($checklist);exit;

        if ((count($checklist) == $flag_count) && ($flag_count != 0)) {
            $flag_submission = true;
        }
        $this->view->flag_submission = $flag_submission;
        $this->view->checklist = $checklist;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $formData = clean_array($formData); // clean data
            //var_dump($formData); exit;
            $db = getDB2();

            //check if data already exist?
            $student_checklist = $checklistDb->studentChecklist($this->studentinfo[0]['idStudentRegistration']);
            if (count($student_checklist) > 0) {
                //delete
                //$checklistDb->deleteData($registration_id);
            }


            for ($i = 0; $i < count($formData['checklist_id']); $i++) {

                $id_checklist = $formData['checklist_id'][$i];
                $remarks = $formData['remarks'][$id_checklist];

                $isStudentChecklist2 = $checklistDb->isStudentChecklist($this->studentinfo[0]['idStudentRegistration'], $id_checklist);

                if (!$isStudentChecklist2) {
                    $data = array(
                        'pregraduate_list_id' => $this->studentinfo[0]['id'],
                        'IdStudentRegistration' => $this->studentinfo[0]['idStudentRegistration'],
                        'idChecklist' => $id_checklist,
                        'psc_status' => 1,
                        'remarks_message' => $remarks,
                        'remarks_by' => $this->studentinfo[0]['idStudentRegistration'],
                        'remarks_date' => date("Y-m-d H:i:s"),
                        'remarks_role' => $this->auth->getIdentity()->role,
                        'psc_createdby' => $this->studentinfo[0]['idStudentRegistration'],
                        'psc_createddt' => date("Y-m-d H:i:s"),
                    );

                    $db->insert('pregraduate_student_checklist', $data);
                } else {
                    $data = array(
                        'psc_status' => 1,
                        'remarks_message' => $remarks,
                        'remarks_by' => $this->studentinfo[0]['idStudentRegistration'],
                        'remarks_date' => date("Y-m-d H:i:s"),
                        'remarks_role' => $this->auth->getIdentity()->role,
                    );

                    $db->update('pregraduate_student_checklist', $data, 'psc_id = ' . $isStudentChecklist2['psc_id']);
                }
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Information Updated"));

            //redirect
            $this->_redirect('graduation/guests');
        }

    }

    public function guestsAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();

        $auth = Zend_Auth::getInstance();
        $registration_id = $auth->getIdentity()->external_id;
        $Guest = new App_Model_Guest();

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;


        //$this->view->appl_id = $appl_id;
        $this->view->IdStudentRegistration = $registration_id;

        $post_data = $this->_request->getPost();
        if (!empty($post_data)) {

            $data['IdStudentRegistration'] = $registration_id;
            $data['guests'] = $post_data['guests'];
            $data['note'] = $post_data['note'];
            $Guest->save($data);


            $this->_helper->flashMessenger->addMessage(array('success' => 'Guest Info Updated'));
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'guest'), 'default', true));

        }


        //To get Student Academic Info
        /*  $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
          $student = $studentRegDB->getStudentInfo($registration_id);
          $this->view->student = $student;*/

        $guestDB = new App_Model_Guest();
        $guest = $guestDB->getguests($this->studentinfo[0]['idStudentRegistration'], 1);
        $this->view->guest = $guest;
        //pr($this->view->guest);exit;

        $convocationDB = new App_Model_Convocation();
        $convocation = $convocationDB->getConvocationGraduate($this->studentinfo[0]['idStudentRegistration']);
        $this->view->convocation = $convocation;
        //pr($convocation);exit;

        $this->view->allow = true;
        if (isset($convocation)) {
            $date1 = new DateTime("now");
            $date2 = new DateTime($convocation['c_guest_app_enddate']);
            if ($date1 > $date2) {
                $this->view->allow = false;
            }
        }


        //get fee info
        $feeConvoDB = new App_Model_FeeItem();
        $this->view->feeConvoData = $feeConvoDB->getDataConvoFee($convocation['c_id'],53);

    }


    public function guestsAddAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();

        $auth = Zend_Auth::getInstance();
        $registration_id = $auth->getIdentity()->external_id;

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;


        $GuestDb = new App_Model_Guest();

        $post_data = $this->_request->getPost();
        if (!empty($post_data)) {
            $db = getDB2();

            $data = array(
                'IdStudentRegistration' => $this->studentinfo[0]['idStudentRegistration'],
                'note' => $post_data['note'],
                'status' => 'APPLIED',
                'guests' => $post_data['guests'],
                'applied_by' => $this->studentinfo[0]['idStudentRegistration'],
                'applied_date' => date("Y-m-d H:i:s"),
                'applied_role' => $this->auth->getIdentity()->role,
                'type' => 1
            );

            $db->insert('tbl_graduation_guests', $data, array('idStudentRegistration = ?' => $this->studentinfo[0]['idStudentRegistration']));

            $this->_helper->flashMessenger->addMessage(array('success' => 'Information Updated'));
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'guests'), 'default', true));

        }
    }

    public function guestsCancelAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));

        $auth = Zend_Auth::getInstance();

        $registration_id = $auth->getIdentity()->external_id;

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;

        $Guest = new App_Model_Guest();
        $Guest->deleteGuests($this->studentinfo[0]['idStudentRegistration']);

        $this->_helper->flashMessenger->addMessage(array('success' => 'Information has been deleted'));
        $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'guests'), 'default', true));

    }

    public function collectionAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();

        $this->view->title = 'Certificate Collection';

        $auth = Zend_Auth::getInstance();
        //$registration_id = $auth->getIdentity()->external_id;

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;

        $countryDB = new App_Model_Country();
        $this->view->country_data = $countryDB->getData();

        //collection data
        $convoDB = new App_Model_Convocation();
        $collect = $convoDB->getCollectionInfo($this->studentinfo[0]['idStudentRegistration']);
        $this->view->collect = $collect;
        //pr($collect);

        $attendDb = new App_Model_Attendance();
        $check = $attendDb->getPregrad($this->studentinfo[0]['idStudentRegistration']);

        $this->view->attend = 3; //No records


        $this->view->check = $check;
        if ($this->getRequest()->isPost()) {

            $post_data = $this->getRequest()->getPost();

            $db = getDB2();
            $invoiceClass = new App_Model_Invoice();

            //Scroll,Transcript & Photo Collection

            //$collection['scroll_self_collect'] = $post_data['scroll_self_collect'];
            //$collection['scroll_courier'] = $post_data['scroll_courier'];

            //$collection['photo_collection'] = $post_data['photo_collection'];
            $collection['scroll_collection'] = $post_data['scroll_collection'];

            /*if($post_data['photo_collection']==2){
                $collection['photo_number'] = $post_data['photo_number'];
            }else{
                $collection['photo_number'] = null;
            }*/

            if ($post_data['scroll_collection'] == 2) {
                $collection['scroll_address1'] = $post_data['scroll_address1'];
                $collection['scroll_address2'] = $post_data['scroll_address2'];
                $collection['scroll_postcode'] = $post_data['scroll_postcode'];
                $collection['scroll_country'] = $post_data['scroll_country'];
                $collection['scroll_state'] = $post_data['scroll_state'];
                $collection['scroll_state_others'] = (isset($post_data['scroll_state_others'])) ? $post_data['scroll_state_others'] : '';
                $collection['scroll_city'] = $post_data['scroll_city'];
                $collection['scroll_city_others'] = (isset($post_data['scroll_city_others'])) ? $post_data['scroll_city_others'] : '';
            } else {
                $collection['scroll_address1'] = null;
                $collection['scroll_address2'] = null;
                $collection['scroll_postcode'] = null;
                $collection['scroll_country'] = null;
                $collection['scroll_state'] = null;
                $collection['scroll_state_others'] = null;
                $collection['scroll_city'] = null;
                $collection['scroll_city_others'] = null;
            }

            //collection section
            if (isset($post_data['gc_id']) && $post_data['gc_id'] != '') {

                if ($post_data ['scroll_collection'] == 2) {

                    if ($collect['gc_invoice_id'] == '') {
                        //echo 'masuk generate';
                        //$invoice_id = $invoiceClass->generateInvoicePostage($this->studentinfo[0]['idStudentRegistration'], $feeID = 123, $post_data['scroll_country'], $post_data['scroll_state']);
                        //$collection['gc_invoice_id'] = $invoice_id;

                        $params = array(
                            'studentID' => $this->studentinfo[0]['idStudentRegistration'],
                            'feeID' => 51,
                            'countryId' => $post_data['scroll_country'],
                            'stateId' => $post_data['scroll_state']
                        );

                        $invoice_id = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoicePostage', $params);
                        $collection['gc_invoice_id'] = $invoice_id;

                        $inv['IdStudentRegistration'] = $this->studentinfo[0]['idStudentRegistration'];
                        $inv['gih_invoice_id'] = $invoice_id != false ? $invoice_id : 0;
                        $inv['gih_remarks'] = 'generate invoice postage';
                        $inv['gih_createdby'] = $auth->getIdentity()->id;
                        $inv['gih_createddt'] = date('Y-m-d H:i:s');
                        $convoDB->saveInvoiceHistory($inv);
                    }

                } else
                    if ($post_data['scroll_collection'] != 2) {

                        if ($collect['gc_invoice_id'] != '') {
                            //echo 'masuk cancel';
                            //$invoiceClass->generateCreditNoteEntryMain($this->studentinfo[0]['idStudentRegistration'], $collect['gc_invoice_id'], 100, 0, 1);
                            //$collection['gc_invoice_id'] = null;

                            $params = array(
                                'studentID' => $this->studentinfo[0]['idStudentRegistration'],
                                'invoiceID' => $collect['gc_invoice_id'],
                                'percentageDefined' => 100,
                                'amount' => 0,
                                'commit' => 0,
                                'dateCreated' => 1
                            );

                            Cms_Curl::__(SMS_API . '/get/Finance/do/generateCreditNoteEntryMain', $params);
                            $collection['gc_invoice_id'] = null;
                        }
                    }

                $db->update('tbl_graduation_collection', $collection, "gc_id = " . $post_data['gc_id']);

            } else {

                //generate invoice for photo charges
                if ($post_data['scroll_collection'] == 2) {
                    //echo 'masuk generate';
                    //$invoice_id = $invoiceClass->generateInvoicePostage($this->studentinfo[0]['idStudentRegistration'], $feeID = 33, $post_data['scroll_country'], $post_data['scroll_state']);
                    //$collection['gc_invoice_id'] = $invoice_id;

                    $params = array(
                        'studentID' => $this->studentinfo[0]['idStudentRegistration'],
                        'feeID' => 51,
                        'countryId' => $post_data['scroll_country'],
                        'stateId' => $post_data['scroll_state']
                    );

                    $invoice_id = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoicePostage', $params);
                    $collection['gc_invoice_id'] = $invoice_id;

                    //keep history
                    $inv['IdStudentRegistration'] = $this->studentinfo[0]['idStudentRegistration'];
                    $inv['gih_invoice_id'] = $invoice_id != false ? $invoice_id : 0;
                    $inv['gih_remarks'] = 'generate invoice postage';
                    $inv['gih_createdby'] = $auth->getIdentity()->id;
                    $inv['gih_createddt'] = date('Y-m-d H:i:s');
                    $convoDB->saveInvoiceHistory($inv);
                }

                $collection['idStudentRegistration'] = $this->studentinfo[0]['idStudentRegistration'];
                $collection['gc_createddt'] = date('Y-m-d H:i:s');
                $collection['gc_createdby'] = $auth->getIdentity()->id;
                $db->insert('tbl_graduation_collection', $collection);
            }
            //end collection section

            $this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'payment'), 'default', true));
        }
    }


    public function recordVerificationCertificateAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;

        $auth = Zend_Auth::getInstance();
        //$registration_id = $auth->getIdentity()->external_id;

        $convocationDB = new App_Model_Convocation();
        $convocation = $convocationDB->getConvocationGraduate($this->studentinfo[0]['idStudentRegistration']);
        $this->view->convocation = $convocation;
        //pr($convocation);//exit;

        $this->view->allow = true;
        if (isset($convocation)) {
            $date1 = new DateTime("now");
            $date2 = new DateTime($convocation['c_record_verification_enddate']);
            if ($date1 > $date2) {
                $this->view->allow = false;
            }
        }

        $checklistDb = new App_Model_Checklist();
        $checklist = $checklistDb->get_checklist($convocation['c_id'], $convocation['IdProgram']);
        //pr($checklist);
        $this->view->checklist = $checklist;

        $graduateDB = new App_Model_Invitation();
        $profile = $graduateDB->getGraduateProfile($this->auth->getIdentity()->external_id);

        $flag_submission = false;
        $flag_count = 0;

        foreach ($checklist as $index => $list) {

            if ($list['type'] == 1) {
                //name
                $checklist[$index]['profile_info'] = $profile['student_name'];
            }
            if ($list['type'] == 2) {
                //ic
                $checklist[$index]['profile_info'] = $profile['std_idnumber'];
            }
            if ($list['type'] == 3) {
                //address
                //$city = ($profile['appl_city']!=99) ? $profile['CityName']:$profile['appl_city_others'];
                //$state = ($profile['appl_state']!=99) ? $profile['StateName']:$profile['appl_state_others'];

                $address = (!empty($profile['std_region'])) ? $profile['std_region'] : '';
                $address .= (!empty($profile['std_remark'])) ? '<br>' . $profile['std_remark'] : '';
                $address .= (!empty($profile['std_location'])) ? '<br>' . $profile['std_location'] : '';
                //$address .= (!empty($profile['appl_postcode'])) ? '<br>'.$profile['appl_postcode'].' '.$city:'';
                //$address .= (!empty($profile['CountryName']))   ? '<br>'.$state.' '.$profile['CountryName']:'<br>'.$state;

                $checklist[$index]['profile_info'] = $address;
            }
            if ($list['type'] == 4) {
                //contact no
                $checklist[$index]['profile_info'] = $profile['std_contact_number'];


            }
            if ($list['type'] == 5) {
                //email
                $checklist[$index]['profile_info'] = $profile['std_email'];
            }

            //check if selected by student
            $isStudentChecklist = $checklistDb->isStudentChecklist($this->studentinfo[0]['idStudentRegistration'], $list['id']);

            if ($isStudentChecklist) {
                //$flag_submission = true;

                if ($isStudentChecklist['psc_status'] != 0) {
                    $checklist[$index]['ischecked'] = true;
                    $checklist[$index]['disabled'] = true;
                    $flag_count = $flag_count + 1;
                } else {
                    $checklist[$index]['ischecked'] = false;
                    $checklist[$index]['disabled'] = false;
                }
                $checklist[$index]['remarks_message'] = htmlentities($isStudentChecklist['remarks_message'], ENT_QUOTES);
            } else {
                $checklist[$index]['ischecked'] = false;
                $checklist[$index]['disabled'] = false;
            }

        }

        if ((count($checklist) == $flag_count) && ($flag_count != 0)) {
            $flag_submission = true;
        }
        $this->view->flag_submission = $flag_submission;
        $this->view->checklist = $checklist;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $formData = clean_array($formData); // clean data
            //var_dump($formData); exit;
            $db = getDB2();

            //check if data already exist?
            $student_checklist = $checklistDb->studentChecklist($this->studentinfo[0]['idStudentRegistration']);
            if (count($student_checklist) > 0) {
                //delete
                //$checklistDb->deleteData($registration_id);
            }


            for ($i = 0; $i < count($formData['checklist_id']); $i++) {

                $id_checklist = $formData['checklist_id'][$i];
                $remarks = $formData['remarks'][$id_checklist];

                $isStudentChecklist2 = $checklistDb->isStudentChecklist($this->studentinfo[0]['idStudentRegistration'], $id_checklist);

                if (!$isStudentChecklist2) {
                    $data = array(
                        'pregraduate_list_id' => $this->studentinfo[0]['id'],
                        'IdStudentRegistration' => $this->studentinfo[0]['idStudentRegistration'],
                        'idChecklist' => $id_checklist,
                        'psc_status' => 1,
                        'remarks_message' => $remarks,
                        'remarks_by' => $this->studentinfo[0]['idStudentRegistration'],
                        'remarks_date' => date("Y-m-d H:i:s"),
                        'remarks_role' => $this->auth->getIdentity()->role,
                        'psc_createdby' => $this->studentinfo[0]['idStudentRegistration'],
                        'psc_createddt' => date("Y-m-d H:i:s"),
                    );
                    //pr($data);
                    $db->insert('pregraduate_student_checklist', $data);
                } else {
                    $data = array(
                        'psc_status' => 1,
                        'remarks_message' => $remarks,
                        'remarks_by' => $this->studentinfo[0]['idStudentRegistration'],
                        'remarks_date' => date("Y-m-d H:i:s"),
                        'remarks_role' => $this->auth->getIdentity()->role,
                    );

                    $db->update('pregraduate_student_checklist', $data, 'psc_id = ' . $isStudentChecklist2['psc_id']);
                }
            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Information Updated"));

            //redirect
            $this->_redirect('graduation/collection-certificate');
        }

    }

    public function collectionCertificateAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();

        $this->view->title = 'Certificate Collection';

        $auth = Zend_Auth::getInstance();
        //$registration_id = $auth->getIdentity()->external_id;

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;

        $countryDB = new App_Model_Country();
        $this->view->country_data = $countryDB->getData();
        //pr($this->view->country_data);exit;

        //collection data
        $convoDB = new App_Model_Convocation();
        $collect = $convoDB->getCollectionInfoCertificate($this->studentinfo[0]['idStudentRegistration']);
        $this->view->collect = $collect;
        //pr($collect);exit;

        $attendDb = new App_Model_Attendance();
        $check = $attendDb->getPregrad($this->studentinfo[0]['idStudentRegistration']);
        //pr($check);exit;

        $this->view->attend = 3; //No records


        $this->view->check = $check;

        if ($this->getRequest()->isPost()) {

            $post_data = $this->getRequest()->getPost();

            $db = getDB2();
            $invoiceClass = new App_Model_Invoice();

            $collection['scroll_collection'] = $post_data['scroll_collection'];

            if ($post_data['scroll_collection'] == 2) {
                $collection['scroll_address1'] = $post_data['scroll_address1'];
                $collection['scroll_address2'] = $post_data['scroll_address2'];
                $collection['scroll_postcode'] = $post_data['scroll_postcode'];
                $collection['scroll_country'] = $post_data['scroll_country'];
                $collection['scroll_state'] = $post_data['scroll_state'];
                $collection['scroll_state_others'] = (isset($post_data['scroll_state_others'])) ? $post_data['scroll_state_others'] : '';
                $collection['scroll_city'] = $post_data['scroll_city'];
                $collection['scroll_city_others'] = (isset($post_data['scroll_city_others'])) ? $post_data['scroll_city_others'] : '';
            } else {
                $collection['scroll_address1'] = null;
                $collection['scroll_address2'] = null;
                $collection['scroll_postcode'] = null;
                $collection['scroll_country'] = null;
                $collection['scroll_state'] = null;
                $collection['scroll_state_others'] = null;
                $collection['scroll_city'] = null;
                $collection['scroll_city_others'] = null;
            }

            //collection section
            if (isset($post_data['gc_id']) && $post_data['gc_id'] != '') {

                if ($post_data['scroll_collection'] == 2) {

                    if ($collect['gc_invoice_id'] == '') {
                        //echo 'masuk generate';
                        //$invoice_id = $invoiceClass->generateInvoicePostage($this->studentinfo[0]['idStudentRegistration'],$feeID=54,$post_data['scroll_country'],$post_data['scroll_state']);
                        //$collection['gc_invoice_id'] = $invoice_id;

                        $params = array(
                            'studentID' => $this->studentinfo[0]['idStudentRegistration'],
                            'feeID' => 54,
                            'countryId' => $post_data['scroll_country'],
                            'stateId' => $post_data['scroll_state']
                        );

                        $invoice_id = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoicePostage', $params);
                        $collection['gc_invoice_id'] = $invoice_id;

                        $inv['IdStudentRegistration'] = $this->studentinfo[0]['idStudentRegistration'];
                        $inv['gih_invoice_id'] = $invoice_id != false ? $invoice_id : 0;
                        $inv['gih_remarks'] = 'generate invoice postage';
                        $inv['gih_createdby'] = $auth->getIdentity()->id;
                        $inv['gih_createddt'] = date('Y-m-d H:i:s');
                        $convoDB->saveInvoiceHistory($inv);
                    }

                } else
                    if ($post_data['scroll_collection'] != 2) {

                        if ($collect['gc_invoice_id'] != '') {
                            //echo 'masuk cancel';
                            /*$invoiceClass->generateCreditNoteEntryMain($this->studentinfo[0]['idStudentRegistration'],$collect['gc_invoice_id'],100,0,1);
                            $collection['gc_invoice_id'] = null;*/

                            $params = array(
                                'studentID' => $this->studentinfo[0]['idStudentRegistration'],
                                'invoiceID' => $collect['gc_invoice_id'],
                                'percentageDefined' => 100,
                                'amount' => 0,
                                'commit' => 0,
                                'dateCreated' => 1
                            );

                            Cms_Curl::__(SMS_API . '/get/Finance/do/generateCreditNoteEntryMain', $params);
                            $collection['gc_invoice_id'] = null;
                        }
                    }

                $db->update('tbl_graduation_collectioncertificate', $collection, "gc_id = " . $post_data['gc_id']);

            } else {

                //generate invoice for scroll charges
                if ($post_data['scroll_collection'] == 2) {
                    //echo 'masuk generate';
                    //$invoice_id = $invoiceClass->generateInvoicePostage($this->studentinfo[0]['idStudentRegistration'],$feeID=54,$post_data['scroll_country'],$post_data['scroll_state']);
                    //$collection['gc_invoice_id'] = $invoice_id;

                    $params = array(
                        'studentID' => $this->studentinfo[0]['idStudentRegistration'],
                        'feeID' => 54,
                        'countryId' => $post_data['scroll_country'],
                        'stateId' => $post_data['scroll_state']
                    );

                    $invoice_id = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoicePostage', $params);
                    $collection['gc_invoice_id'] = $invoice_id;
                    //pr($invoice_id);exit;

                    //keep history
                    $inv['IdStudentRegistration'] = $this->studentinfo[0]['idStudentRegistration'];
                    $inv['gih_invoice_id'] = $invoice_id != false ? $invoice_id : 0;
                    $inv['gih_remarks'] = 'generate invoice postage';
                    $inv['gih_createdby'] = $auth->getIdentity()->id;
                    $inv['gih_createddt'] = date('Y-m-d H:i:s');
                    $convoDB->saveInvoiceHistory($inv);
                }

                $collection['idStudentRegistration'] = $this->studentinfo[0]['idStudentRegistration'];
                $collection['gc_createddt'] = date('Y-m-d H:i:s');
                $collection['gc_createdby'] = $auth->getIdentity()->id;
                $db->insert('tbl_graduation_collectioncertificate', $collection);
            }
            //end collection section

            $this->_helper->flashMessenger->addMessage(array('success' => 'Data has been saved'));
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'payment-certificate'), 'default', true));
        }
    }


    public function chargersCertificateAction()
    {

        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();

        $this->_helper->layout->disableLayout();

        $feeCountryDb = new App_Model_FeeCountry();
        $this->view->countries = $feeCountryDb->getFeeByCountry(54, 1); // 74 To be change

        $this->view->state_malaysia = $feeCountryDb->getFeeStateByCountry(54, 121, 1);
    }

    public function chargersAction()
    {

        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();

        $this->_helper->layout->disableLayout();

        $feeCountryDb = new App_Model_FeeCountry();
        $this->view->countries = $feeCountryDb->getFeeByCountry(51, 1); // 74 To be change

        $this->view->state_malaysia = $feeCountryDb->getFeeStateByCountry(51, 121, 1);
    }

    public function paymentCertificateAction()
    {

        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();
        $bill = array();

        $auth = Zend_Auth::getInstance();
        //$registration_id = $auth->getIdentity()->external_id;

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;


        $txnId = $this->studentinfo[0]['idStudentRegistration'];
        if ($txnId == 0) {
            $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate('Unknown transaction id')));
            $this->_redirect($this->view->url(array('module' => 'deafult', 'controller' => 'graduation', 'action' => 'payment-certificate'), 'default', true));
        }

        $currency = $this->_getParam('currency', 1);
        $this->view->curr_id = $currency;

        //check txnid with profile id
        $auth = Zend_Auth::getInstance();


        //profile
        $studentRegistrationDb = new App_Model_StudentRegistration();
        $transaction = $studentRegistrationDb->getTheStudentRegistrationDetail($txnId);
        //$appl_id = $transaction['IdApplication'];

        if (!$transaction) {
            $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate('Invalid transaction id')));
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'payment-certificate'), 'default', true));
        }

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if (!isset($formData['invoice'])) {
                $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate('Please select item to pay')));
                $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'payment-certificate', 'id' => $txnId), 'default', true));
            }

            $ses_migs = new Zend_Session_Namespace('graduation_payment');
            $ses_migs->txnid = $txnId;
            $ses_migs->appl_id = $txnId;


            //store invoice in session if any
            if (isset($formData['invoice'])) {
                $ses_migs->invoice = $formData['invoice'];
            }

            //redirect to confirmation page
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'paymentconfirmation-certificate'), 'default', true));
        }

        //proforma invoice
        $db = getDB2();

        $select_currency = $db->select()
            ->from(array('c' => 'tbl_currency'));
        $row_currency = $db->fetchAll($select_currency);
        $this->view->currency_list = $row_currency;

        $select_invoice = $db->select()
            ->from(array('ivd' => 'invoice_detail'), array('*', 'idDet' => 'ivd.id'))
            ->join(array('im' => 'invoice_main'), 'ivd.invoice_main_id=im.id', array('*', 'record_date' => 'im.invoice_date'))
            ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id', array())
            ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array('*'))
            ->joinLeft(array('fc' => 'fee_item'), 'fc.fi_id=ivd.fi_id', array('*'))
            ->joinLeft(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id=ivd.fi_id', array('fi_name'))
            ->joinLeft(array('fsi' => 'fee_structure_item'), 'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=im.fs_id', array(''))
            ->joinLeft(array('i' => 'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name' => 'i.DefinitionDesc', 'item_code' => 'i.DefinitionCode'))
            //->where('im.currency_id= ?',$currency)
            ->where('im.IdStudentRegistration = ?', $txnId)
            ->where("im.status = 'A'")
            ->where("ivd.balance > 0")
            ->group('ivd.id');

        $row = $db->fetchAll($select_invoice);
        if (!$row) {
            $row = null;
        }

        //$this->view->account = $row;

        $this->view->bill = $row;


    }

    public function paymentAction()
    {

        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();
        $bill = array();

        $auth = Zend_Auth::getInstance();
        $registration_id = $auth->getIdentity()->external_id;

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;

        $txnId = $this->studentinfo[0]['idStudentRegistration'];
        //pr($txnId);exit;

        if ($txnId == 0) {
            $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate('Unknown transaction id')));
            $this->_redirect($this->view->url(array('module' => 'deafult', 'controller' => 'graduation', 'action' => 'payment'), 'default', true));
        }

        $currency = $this->_getParam('currency', 1);
        $this->view->curr_id = $currency;

        //profile
        $studentRegistrationDb = new App_Model_StudentRegistration();
        $transaction = $studentRegistrationDb->getTheStudentRegistrationDetail($txnId);
        //$appl_id = $transaction['IdApplication'];

        if (!$transaction) {
            $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate('Invalid transaction id')));
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'payment'), 'default', true));
        }

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if (!isset($formData['invoice'])) {
                $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate('Please select item to pay')));
                $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'payment', 'id' => $txnId), 'default', true));
            }

            $ses_migs = new Zend_Session_Namespace('graduation_payment');
            $ses_migs->txnid = $txnId;
            $ses_migs->appl_id = $txnId;


            //store invoice in session if any
            if (isset($formData['invoice'])) {
                $ses_migs->invoice = $formData['invoice'];
            }

            //redirect to confirmation page
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'graduation', 'action' => 'paymentconfirmation'), 'default', true));
        }

        //proforma invoice
        $db = getDB2();

        $select_currency = $db->select()
            ->from(array('c' => 'tbl_currency'));
        $row_currency = $db->fetchAll($select_currency);
        $this->view->currency_list = $row_currency;

        $select_invoice = $db->select()
            ->from(array('ivd' => 'invoice_detail'), array('*', 'idDet' => 'ivd.id'))
            ->join(array('im' => 'invoice_main'), 'ivd.invoice_main_id=im.id', array('*', 'record_date' => 'im.invoice_date'))
            ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=im.id AND ivs.invoice_detail_id=ivd.id', array())
            ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array('*'))
            ->joinLeft(array('fc' => 'fee_item'), 'fc.fi_id=ivd.fi_id', array('*'))
            ->joinLeft(array('c' => 'tbl_currency'), 'c.cur_id=im.currency_id', array('cur_code', 'cur_id', 'cur_desc'))
            ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id=ivd.fi_id', array('fi_name'))
            ->joinLeft(array('fsi' => 'fee_structure_item'), 'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=im.fs_id', array(''))
            ->joinLeft(array('i' => 'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name' => 'i.DefinitionDesc', 'item_code' => 'i.DefinitionCode'))
            //->joinLeft(array('o' => 'order'), 'im.order_id=o.id', array('o.status'))
            ->where('im.currency_id= ?',$currency)
            ->where('im.IdStudentRegistration = ?', $txnId)
            ->where("im.status = 'A'")
            ->where("im.bill_balance > 0")
            ->group('ivd.id');

        $row = $db->fetchAll($select_invoice);
        if (!$row) {
            $row = null;
        }

        //$this->view->account = $row;

        $this->view->bill = $row;
        //pr($this->view->bill );

    }

    public function paymentconfirmationAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();
        $bill = array();

        $auth = Zend_Auth::getInstance();
        $sp = $auth->getIdentity()->external_id;

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;

        $ses_migs = new Zend_Session_Namespace('graduation_payment');

        // pr($ses_migs->invoice);
        $this->view->txnid = $ses_migs->txnid;

        $auth = Zend_Auth::getInstance();
        $txnId = $ses_migs->txnid;

        $db = getDB2();

        //proforma
        $invoiceInvoiceDb = new App_Model_InvoiceMain();
        $invoice_list = array();
        $netAmount = 0;
        $ses_migs->pay_invoice = [];

        for ($i = 0; $i < sizeof($ses_migs->invoice); $i++) {

            $selectData = $db->select()
                ->from(array('im' => 'invoice_detail'), array('im.id', 'im.invoice_main_id', 'im.fi_id', 'im.fee_item_description', 'im.cur_id', 'im.amount', 'im.amount_gst', 'im.amount_default_currency', 'im.paid', 'im.balance', 'im.cn_amount', 'im.dn_amount', 'im.sp_amount', 'im.sp_id', 'id_detail' => 'im.id'))
                ->join(array('pim' => 'invoice_main'), 'im.invoice_main_id=pim.id', array('*'))
                ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=pim.id and ivs.invoice_detail_id=im.id', array(''))
                ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array('*'))
                ->joinLeft(array('c' => 'tbl_currency'), 'c.cur_id=pim.currency_id', array('*'))
                ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id=im.fi_id', array(''))
                ->joinLeft(array('fsi' => 'fee_structure_item'), 'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=pim.fs_id', array(''))
                ->joinLeft(array('i' => 'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name' => 'i.DefinitionDesc', 'item_code' => 'i.DefinitionCode'))
                ->where("im.id = ?", (int)$ses_migs->invoice[$i])
                ->group('im.id');


            $invoice_data = $db->fetchRow($selectData);

            $invoice_list[] = $invoice_data;

            $invoice_list[$i]['amount'] = $invoice_list[$i]['bill_balance'];

            if ($invoice_list[$i]['currency_id'] != 1) { //if not in MYR, convert to actual currency 1st

                //get current currency rate
                $currencyRateDb = new App_Model_CurrencyRate();
                $rate = $currencyRateDb->getData($invoice_list[$i]['exchange_rate']);
                $total_invoice_amount_default_currency = round($invoice_list[$i]['balance'] * $rate['cr_exchange_rate'], 2);
                $invoice_list[$i]['amount'] = $total_invoice_amount_default_currency;

            }

            $netAmount = $netAmount + $invoice_list[$i]['amount'];
            $ses_migs->pay_invoice[] = $invoice_data['id'];

        }

        $this->view->invoice_list = $invoice_list;


        $paymentModeDB = new App_Model_Finance_PaymentMode();
        $paymentmodelist = $paymentModeDB->getPaymentModeList(1);


        foreach ($paymentmodelist as $i => $row) {
            $paymentmodelist[$i]['fees_display_amount'] = 0;
            $paymentmodelist[$i]['totalAmount'] = $netAmount;
            $paymentmodelist[$i]['cur_code'] = $invoice_list[0]['cur_code'];

            if ($row['fees_type'] == 1) {
                $paymentmodelist[$i]['fees_display_amount'] = $row['fees_amount'];
                //convert kalau currency bukan MYR
            } elseif ($row['fees_type'] == 2) {
                $paymentmodelist[$i]['fees_display_amount'] = $netAmount * $row['fees_percentage'] / 100;
            }

            $paymentmodelist[$i]['net_amount_with_fees'] = $netAmount + $paymentmodelist[$i]['fees_display_amount'];
        }


        $orderDb = new App_Model_Order();
        $invoiceAmount = $invoice_list[0]['amount'];
        if ($invoiceAmount > 0) {
            $invoiceId = $invoice_list[0]['id'];
            $invoiceNo = $invoice_list[0]['bill_number'];
            $currencyId = $invoice_list[0]['currency_id'];

            // check - invoice no. & pending
            $checkorder = $orderDb->checkOrderID($invoiceId);

            if ($checkorder) {
                // dah ade order
                $order_id = $checkorder['id'];
            } else {
                // add new order
                $order = array(
                    'user_id' => $auth->getIdentity()->id,
                    'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'created_by' => $auth->getIdentity()->id,
                    'amount' => $invoiceAmount,
                    'paymentmethod' => 'MIGS',
                    'status' => 'PENDING',
                    'invoice_id' => $invoiceId,
                    'invoice_no' => $invoiceNo,
                    'invoice_external' => $invoiceId,
                    'regtype' => 'graduation',
                    'curriculum_id' => 0,
                    'item_id' => 0,
                    'learningmode' => 0,
                    'schedule' => 0,
                    'external_id' => $this->studentinfo[0]['idStudentRegistration']
                );

                $order_id = $orderDb->insert($order);
            }

            //update invoice_main
            $invoiceMainDB = new App_Model_Finance_InvoiceMain();
            $invoiceMainDB->updateData(array('order_id' => $order_id), $invoiceId);


            //update exam_registration
            //get student_profile SMS
            $convoDB = new App_Model_Convocation();
            $profile = $convoDB->getProfile($sp);
            $sp_id = $profile['sp_id'];

           /* //get invoice
            $params = array('id' => $invoiceId);
            $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoice', $params);

            $this->view->invoice = $invoice;
            //pr($invoice);exit;

            $totalAmount = $invoice['bill_balance'];

            //get payment mode + service charge
            $paymentmodelist = $paymentModeDB->getPaymentModeList(1);


            foreach ($paymentmodelist as $i => $row) {
                $paymentmodelist[$i]['fees_display_amount'] = 0;
                $paymentmodelist[$i]['totalAmount'] = $totalAmount;
                $paymentmodelist[$i]['cur_code'] = $invoice['cur_code'];

                if ($row['fees_type'] == 1) {
                    $paymentmodelist[$i]['fees_display_amount'] = $row['fees_amount'];
                    //convert kalau currency bukan MYR
                } elseif ($row['fees_type'] == 2) {
                    $paymentmodelist[$i]['fees_display_amount'] = $totalAmount * $row['fees_percentage'] / 100;
                }

                $paymentmodelist[$i]['net_amount_with_fees'] = $totalAmount + $paymentmodelist[$i]['fees_display_amount'];
            }

            $this->view->paymentmodelist = $paymentmodelist;

        }
        else {
            $order_id = $this->_getParam('id', 0);
            $order_id = Cms_Common::encrypt($order_id, false);
            $db       = Zend_Db_Table::GetDefaultAdapter();

            $order   = $db->fetchRow($db->select()->from('order')->where('id = ?', (int) $order_id));

            if(!$order) {
                $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate("Order Not found")));
                $this->_redirect('/');
            }

            $params  = array('id' => $order['invoice_id']);
            $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoice', $params);

            $totalAmount     = $invoice['bill_balance'];
            $paymentmodelist = $paymentModeDB->getPaymentModeList(1);

            foreach ($paymentmodelist as $i => $row) {
                $paymentmodelist[$i]['fees_display_amount'] = 0;
                $paymentmodelist[$i]['totalAmount']         = $totalAmount;
                $paymentmodelist[$i]['cur_code']            = $invoice['cur_code'];

                if ($row['fees_type'] == 1) {
                    $paymentmodelist[$i]['fees_display_amount'] = $row['fees_amount'];
                }
                elseif ($row['fees_type'] == 2) {
                    $paymentmodelist[$i]['fees_display_amount'] = $totalAmount * $row['fees_percentage'] / 100;
                }

                $paymentmodelist[$i]['net_amount_with_fees'] = $totalAmount + $paymentmodelist[$i]['fees_display_amount'];
            }*/


            $this->view->paymentmodelist = $paymentmodelist;
            //pr($paymentmodelist);exit;
            $this->view->order_id = $order_id;
            $this->view->invoice_id = $invoiceId;

        }

    }


    public function paymentconfirmationCertificateAction()
    {
        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('graduation'));
        $userDb = new App_Model_User();
        $bill = array();

        $auth = Zend_Auth::getInstance();
        $sp = $auth->getIdentity()->external_id;

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;
        //pr($invitation);exit;

        $ses_migs = new Zend_Session_Namespace('graduation_payment');
        $this->view->txnid = $ses_migs->txnid;

        $auth = Zend_Auth::getInstance();
        $txnId = $ses_migs->txnid;

        $db = getDB2();

        //proforma
        $invoiceInvoiceDb = new App_Model_InvoiceMain();
        $invoice_list = array();
        $netAmount = 0;
        $ses_migs->pay_invoice = [];

        for ($i = 0; $i < sizeof($ses_migs->invoice); $i++) {

            $selectData = $db->select()
                ->from(array('im' => 'invoice_detail'), array('im.id', 'im.invoice_main_id', 'im.fi_id', 'im.fee_item_description', 'im.cur_id', 'im.amount', 'im.amount_gst', 'im.amount_default_currency', 'im.paid', 'im.balance', 'im.cn_amount', 'im.dn_amount', 'im.sp_amount', 'im.sp_id', 'id_detail' => 'im.id'))
                ->join(array('pim' => 'invoice_main'), 'im.invoice_main_id=pim.id', array('*'))
                ->joinLeft(array('ivs' => 'invoice_subject'), 'ivs.invoice_main_id=pim.id and ivs.invoice_detail_id=im.id', array(''))
                ->joinLeft(array('s' => 'tbl_subjectmaster'), 's.IdSubject=ivs.subject_id', array('*'))
                ->joinLeft(array('c' => 'tbl_currency'), 'c.cur_id=pim.currency_id', array('*'))
                ->joinLeft(array('fi' => 'fee_item'), 'fi.fi_id=im.fi_id', array(''))
                ->joinLeft(array('fsi' => 'fee_structure_item'), 'fsi.fsi_item_id=fi.fi_id AND fsi.fsi_structure_id=pim.fs_id', array(''))
                ->joinLeft(array('i' => 'tbl_definationms'), 'fsi.fsi_registration_item_id = i.idDefinition', array('item_name' => 'i.DefinitionDesc', 'item_code' => 'i.DefinitionCode'))
                ->where("im.id = ?", (int)$ses_migs->invoice[$i])
                ->group('im.id');


            $invoice_data = $db->fetchRow($selectData);

            $invoice_list[] = $invoice_data;

            $invoice_list[$i]['amount'] = $invoice_list[$i]['bill_balance'];

            if ($invoice_list[$i]['currency_id'] != 1) { //if not in MYR, convert to actual currency 1st

                //get current currency rate
                $currencyRateDb = new App_Model_CurrencyRate();
                $rate = $currencyRateDb->getData($invoice_list[$i]['exchange_rate']);
                $total_invoice_amount_default_currency = round($invoice_list[$i]['balance'] * $rate['cr_exchange_rate'], 2);
                $invoice_list[$i]['amount'] = $total_invoice_amount_default_currency;

            }

            $netAmount = $netAmount + $invoice_list[$i]['amount'];
            $ses_migs->pay_invoice[] = $invoice_data['id'];

        }

        $this->view->invoice_list = $invoice_list;


        $paymentModeDB = new App_Model_Finance_PaymentMode();
        $paymentmodelist = $paymentModeDB->getPaymentModeList(1);


        foreach ($paymentmodelist as $i => $row) {
            $paymentmodelist[$i]['fees_display_amount'] = 0;
            $paymentmodelist[$i]['totalAmount'] = $netAmount;
            $paymentmodelist[$i]['cur_code'] = $invoice_list[0]['cur_code'];

            if ($row['fees_type'] == 1) {
                $paymentmodelist[$i]['fees_display_amount'] = $row['fees_amount'];
                //convert kalau currency bukan MYR
            } elseif ($row['fees_type'] == 2) {
                $paymentmodelist[$i]['fees_display_amount'] = $netAmount * $row['fees_percentage'] / 100;
            }

            $paymentmodelist[$i]['net_amount_with_fees'] = $netAmount + $paymentmodelist[$i]['fees_display_amount'];
        }


        $orderDb = new App_Model_Order();
        $invoiceAmount = $invoice_list[0]['amount'];
        if ($invoiceAmount > 0) {
            $invoiceId = $invoice_list[0]['id'];
            $invoiceNo = $invoice_list[0]['bill_number'];
            $currencyId = $invoice_list[0]['currency_id'];

            // check - invoice no. & pending
            $checkorder = $orderDb->checkOrderID($invoiceId);

            if ($checkorder) {
                // dah ade order
                $order_id = $checkorder['id'];
            } else {
                // add new order
                $order = array(
                    'user_id' => $auth->getIdentity()->id,
                    'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'created_by' => $auth->getIdentity()->id,
                    'amount' => $invoiceAmount,
                    'paymentmethod' => 'MIGS',
                    'status' => 'PENDING',
                    'invoice_id' => $invoiceId,
                    'invoice_no' => $invoiceNo,
                    'invoice_external' => $invoiceId,
                    'regtype' => 'graduation',
                    'curriculum_id' => 0,
                    'item_id' => 0,
                    'learningmode' => 0,
                    'schedule' => 0,
                    'external_id' => $this->studentinfo[0]['idStudentRegistration']
                );

                $order_id = $orderDb->insert($order);
            }

            //update invoice_main
            $invoiceMainDB = new App_Model_Finance_InvoiceMain();
            $invoiceMainDB->updateData(array('order_id' => $order_id), $invoiceId);

            //get student_profile SMS
            $convoDB = new App_Model_Convocation();
            $profile = $convoDB->getProfile($sp);
            $sp_id = $profile['sp_id'];

            /* //get invoice
             $params = array('id' => $invoiceId);
             $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoice', $params);

             $this->view->invoice = $invoice;
             //pr($invoice);exit;

             $totalAmount = $invoice['bill_balance'];

             //get payment mode + service charge
             $paymentmodelist = $paymentModeDB->getPaymentModeList(1);


             foreach ($paymentmodelist as $i => $row) {
                 $paymentmodelist[$i]['fees_display_amount'] = 0;
                 $paymentmodelist[$i]['totalAmount'] = $totalAmount;
                 $paymentmodelist[$i]['cur_code'] = $invoice['cur_code'];

                 if ($row['fees_type'] == 1) {
                     $paymentmodelist[$i]['fees_display_amount'] = $row['fees_amount'];
                     //convert kalau currency bukan MYR
                 } elseif ($row['fees_type'] == 2) {
                     $paymentmodelist[$i]['fees_display_amount'] = $totalAmount * $row['fees_percentage'] / 100;
                 }

                 $paymentmodelist[$i]['net_amount_with_fees'] = $totalAmount + $paymentmodelist[$i]['fees_display_amount'];
             }

             $this->view->paymentmodelist = $paymentmodelist;

         }
         else {
             $order_id = $this->_getParam('id', 0);
             $order_id = Cms_Common::encrypt($order_id, false);
             $db       = Zend_Db_Table::GetDefaultAdapter();

             $order   = $db->fetchRow($db->select()->from('order')->where('id = ?', (int) $order_id));

             if(!$order) {
                 $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate("Order Not found")));
                 $this->_redirect('/');
             }

             $params  = array('id' => $order['invoice_id']);
             $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoice', $params);

             $totalAmount     = $invoice['bill_balance'];
             $paymentmodelist = $paymentModeDB->getPaymentModeList(1);

             foreach ($paymentmodelist as $i => $row) {
                 $paymentmodelist[$i]['fees_display_amount'] = 0;
                 $paymentmodelist[$i]['totalAmount']         = $totalAmount;
                 $paymentmodelist[$i]['cur_code']            = $invoice['cur_code'];

                 if ($row['fees_type'] == 1) {
                     $paymentmodelist[$i]['fees_display_amount'] = $row['fees_amount'];
                 }
                 elseif ($row['fees_type'] == 2) {
                     $paymentmodelist[$i]['fees_display_amount'] = $totalAmount * $row['fees_percentage'] / 100;
                 }

                 $paymentmodelist[$i]['net_amount_with_fees'] = $totalAmount + $paymentmodelist[$i]['fees_display_amount'];
             }*/


            $this->view->paymentmodelist = $paymentmodelist;
            $this->view->order_id = $order_id;
            $this->view->invoice_id = $invoiceId;

        }

    }

    public function payAction()
    {
        $auth = Zend_Auth::getInstance();
        $paymentModeDB = new App_Model_Finance_PaymentMode();
        $pimDB = new App_Model_ProformaInvoiceMain();
        $imDB = new App_Model_InvoiceMain();

        $invoice_id = $this->_getParam('invoice_id', 0);
        $order_id = $this->_getParam('order_id', 0);

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
            $payment_mode = $paymentModeDB->getData($formData['payment_mode']);

            $ses_migs     = new Zend_Session_Namespace('graduation_payment');
            $invoice_ids  = $ses_migs->pay_invoice;
            $pre_invoices = [];

            foreach($invoice_ids as $invoice_id) {
                $params = array(
                    'invoice_id'   => $invoice_id,
                    'payment_mode' => $formData['payment_mode'],
                    'creatorId'    => $auth->getIdentity()->external_id
                );
                // $result   = Cms_Curl::__(SMS_API . '/get/Finance/do/updateInvoicePaymentMode', $params);
                $pre_invoices[] = $imDB->getData($invoice_id, false);
            }

            if ($formData['payment_mode'] == 6) { // migs

                $invoices = array();
                $invoiceNo = array();
                $totalAmount = array();

                foreach ($pre_invoices as $inv) {
                    $invoices[]    = $inv['id'];
                    $invoiceNo[]   = $inv['bill_number'];
                    $totalAmount[] = $inv['bill_amount'];
                }

                $params = array(
                    'token' => Cms_Common::token('ibfimpay'),
                    'invoiceId' => $invoices,
                    'requestFrom' => 0,
                    'currencyId' => 1,
                    'invoiceNo' => $invoiceNo,
                    'totalAmount' => $totalAmount,
                    'registrationItem' => 1539,
                    'die' => 1,
                    'order' => $order_id
                );

                $urlMigs = Cms_Curl::__(URL_PAYMENT . '/migs/', $params);
                header('location: ' . $urlMigs);
            } elseif ($formData['payment_mode'] == 8) { // pay later

            } elseif ($formData['payment_mode'] == 123) { // fpx
                $invoices = array();
                $invoiceNo = array();
                $totalAmount = array();

                foreach ($pre_invoices as $inv) {
                    $invoices[] = $inv['id'];
                    $invoiceNo[] = $inv['bill_number'];
                    $totalAmount[] = $inv['bill_amount'];
                }

                $params = array(
                    'token' => Cms_Common::token('ibfimpay'),
                    'invoiceId' => $invoices,
                    'totalAmount' => $totalAmount,
                    'requestFrom' => 1, // CORP
                    'registrationItem' => 1539,
                    'order' => $order_id
                );
                //pr($params);exit;

                $token = encrypt(json_encode($params));

                $this->_redirect(URL_PAYMENT . '/fpx?session=' . $token);
                
            } elseif ($formData['payment_mode'] == 5) { // fpx
                $invoices = array();
                $invoiceNo = array();
                $totalAmount = array();

                foreach ($pre_invoices as $inv) {
                    $invoices[] = $inv['id'];
                    $invoiceNo[] = $inv['bill_number'];
                    $totalAmount[] = $inv['bill_amount'];
                }

                $params = array(
                    'token' => Cms_Common::token('ibfimpay'),
                    'invoiceId' => $invoices,
                    'totalAmount' => $totalAmount,
                    'requestFrom' => 1, // CORP
                    'registrationItem' => 1539,
                    'order' => $order_id
                );
                //pr($params);exit;

                $token = encrypt(json_encode($params));

                $this->_redirect(URL_PAYMENT . '/fpx?session=' . $token);
            }

            //pr($formData);exit;
        }
    }

    public function processPaymentAction()
    {
        $ol_id = $this->_getParam('ol_id', 0);

        $orderDb = new App_Model_Order();
        $invoiceMainDb = new App_Model_InvoiceMain();
        $invoiceDetailDb = new App_Model_InvoiceDetail();

        if ($ol_id) {
            $params = array('ol_id' => $ol_id);
            $payment_status = Cms_Curl::__(SMS_API . '/get/Payment/do/getPaymentStatus', $params);
            pr($payment_status);

            $db = getDB2();

            $invoiceArray = array();
            $invoiceInfoArray = array();
            $student = $this->studentinfo;

            //$payment_status['invoices'] = array(308);

            if (isset($payment_status['invoices'])) {
                foreach ($payment_status['invoices'] as $invoiceId) {
                    $invoiceInfo = $invoiceDetailDb->getFeeId($invoiceId, $balcheck = 0);
                    $invoiceArray[] = $invoiceId;
                    $invoiceInfoArray[] = $invoiceInfo;
                }
            }
            //pr($invoiceInfo);exit;

            if ($payment_status['success'] == 1) {
                if (!$payment_status['receipt_id']) {
                    $type = $payment_status['type'];
                    $txn_id = $payment_status['txn_id'];
                    $amount = $payment_status['amount'];
                    $currency_id = $payment_status['currency_id'];
                    $migs_id = ($type == 'MIGS' ? $payment_status['migs_transaction_id'] : 0);
                    $fpx_id = ($type == 'FPX' ? $payment_status['fpx_transaction_id'] : 0);

                    /*$type = 'FPX' ;
                    $txn_id = 1;
                    $amount = 10;
                    $currency_id = 2;
                    $migs_id = ($type == 'MIGS' ? 30 : 0);
                    $fpx_id = ($type == 'FPX' ? 30 : 0);*/


                    $rcp_description = 'Payment from ' . $type . ' (' . $txn_id . ')';

                    $paymentInfo = array(
                        'rcp_date'         => date("Y-m-d H:i:s"),
                        'rcp_receive_date' => date("Y-m-d H:i:s"),
                        'rcp_payee_type'   => 1,
                        'rcp_corporate_id' => '0',
                        'rcp_description'  => $rcp_description,
                        'rcp_amount'       => $amount,
                        'rcp_gainloss'     => null,
                        'rcp_gainloss_amt' => null,
                        'rcp_cur_id'       => $currency_id,
                        'refNo'            => $txn_id,
                        'migs_id'          => $migs_id,
                        'fpx_id'           => $fpx_id
                    );
                    //pr($invoiceArray);exit;

                    $receiptId = $this->generateReceipt($invoiceArray, $paymentInfo, 6);
                    //pr($receiptId);

                    $creator = $this->studentinfo[0]['idStudentRegistration'];
                    $params = array('ReceiptId' => $receiptId, 'creatorId' => $creator, 'from' => 2);
                    $receipt = Cms_Curl::__(SMS_API . '/get/Finance/do/receiptApproval', $params);

                    if($type == 'MIGS') {
                        $dataUpdate = array(
                            'rcp_id'   => $receiptId,
                            'rcp_date' => date("Y-m-d H:i:s")
                        );
                        $db->update('transaction_migs', $dataUpdate, array('mt_id = ?' => $migs_id));
                    }
                    elseif($type == 'FPX') {
                        $dataUpdate = array(
                            'rcp_id'   => $receiptId,
                            'rcp_date' => date("Y-m-d H:i:s")
                        );
                        $db->update('fpx_transaction', $dataUpdate, array('id = ?' => $fpx_id));
                    }


                    // update order
                    $data = array('status' => 'ACTIVE');
                    $orderDb->update($data, 'id = ' . $invoiceInfo['order_id']);

                    // update payment_status && receipt no.
                    self::updatePaymentStatus($receiptId, $invoiceInfo['fi_id']);

//                }else{
//
//                }
                }
            }
            $this->_helper->flashMessenger->addMessage(array('success' => 'Payment Successful'));
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'user', 'action' => 'purchase-history'), 'default', true));

            //exit;

        }
    }

    public function  updatePaymentStatus($receiptId, $fi_id)
    {
        $auth = Zend_Auth::getInstance();

        //get idStudentRegistration
        $invitationDb = new App_Model_Invitation ();
        $invitation = $invitationDb->getGraduateInfo($this->auth->getIdentity()->external_id);
        $this->studentinfo = $this->view->studentinfo = $invitation;

        $db = getDB2();

            // update payment_status & receipt_id for convocation fee
            if ($fi_id == 52) {
                $data = array(
                    'payment_status' => '1',
                    'receipt_id' => $receiptId
                );
                $db->update('pregraduate_list', $data, array('idStudentRegistration = ?' => $this->studentinfo[0]['idStudentRegistration']));
            }

            // update payment_status & receipt_id for guest fee
            if ($fi_id == 53) {
                $data = array(
                    'payment_status' => '1',
                    'receipt_id' => $receiptId
                );
                $db->update('tbl_graduation_guests', $data, array('idStudentRegistration = ?' => $this->studentinfo[0]['idStudentRegistration']));
            }

            // update payment_status & receipt_id for courier charges fee (convo)
            if ($fi_id == 51) {
                $data = array(
                    'payment_status' => '1',
                    'receipt_id' => $receiptId
                );
                $db->update('tbl_graduation_collection', $data, array('idStudentRegistration = ?' => $this->studentinfo[0]['idStudentRegistration']));
            }

            // update payment_status & receipt_id for courier charges fee (record verification)
            if ($fi_id == 54) {
                $data = array(
                    'payment_status' => '1',
                    'receipt_id' => $receiptId
                );
                $db->update('tbl_graduation_collectioncertificate', $data, array('idStudentRegistration = ?' => $this->studentinfo[0]['idStudentRegistration']));
            }
            //}
            //pr($data);exit;
    }

    private function generateReceipt($invoiceArray, $paymentInfo, $IdPaymentMode, $uplId = 0)
    {
        $student = $this->view->student = $this->studentinfo;

        $invoiceMainDb = new App_Model_InvoiceMain();
        $receiptDb = new App_Model_Receipt();
        $receiptInvoiceDb = new App_Model_ReceiptInvoice();
        $currencyRateDb = new App_Model_CurrencyRate();
        $migsTransactionDetailDb = new App_Model_TransactionMigsDetail();
        $fpxOrderDetailDb = new App_Model_FpxOrderDetail();


        $paymentClass = new App_Model_PaymentInfo();

        $invoice = array();

        $invoiceInfo = $invoiceMainDb->getDataArray($invoiceArray,1 );

        $invoice = $invoiceInfo;


        $db = getDB2();
        $db->beginTransaction();

        try {

            $currencyUsed = $invoice[0]['currency_id'];
            /* $invDate = $invoice['invoice_date'];
             $invAmountMain = $invoice['bill_amount'];
             $invAmountBalance = $invoice['bill_balance'];
             $invAmountPaid = $invoice['bill_paid'];*/

            if ($IdPaymentMode == 6) { //migs
                $currencyUsed = 1; //always myr
            }

            // 1=current rate, 2 = invoice rate
            $rateUsed = 1;

            if ($rateUsed == 1) {
                //current rate
                $currency = $currencyRateDb->getCurrentExchangeRate($currencyUsed);
                $cr_id = $currency['cr_id'];
            }
            /* elseif ($rateUsed == 2) {
                 //invoice rate - by invoice date
                 $currency = $currencyRateDb->getRateByDate($currencyUsed, $invDate);
                 $cr_id = $currency['cr_id'];
             }*/

            //add receipt
            $receiptNo = $paymentClass->getReceiptNoSeq();

            $receiptDate = date('Y-m-d');
            $amountPaid = $receiptAmount = $paymentInfo['rcp_amount'];
            if(!isset($paymentInfo['fpx_id'])) {
                $paymentInfo['fpx_id'] = 0;
            }

            $receipt_data = array(
                'rcp_no'           => $receiptNo,
                'rcp_account_code' => $IdPaymentMode, //tak tahu nk letak ape lagi
                'rcp_date'         => $paymentInfo['rcp_date'],
                'rcp_receive_date' => date("Y-m-d H:i:s"),
                'rcp_payee_type'   => $paymentInfo['rcp_payee_type'],
                'rcp_corporate_id' => $paymentInfo['rcp_corporate_id'],
                'rcp_description'  => $paymentInfo['rcp_description'],
                'rcp_amount'       => $receiptAmount,
                'rcp_cur_id'       => $paymentInfo['rcp_cur_id'],
                'upl_id'           => isset($uplId) ? $uplId : 0,
                'rcp_from'         => 1,
                'migs_id'          => $paymentInfo['migs_id'],
                'fpx_id'           => $paymentInfo['fpx_id'],
                'exchange_rate'    => isset($cr_id) ? $cr_id : 0,
            );

            $db->insert('receipt',$receipt_data);
            $receipt_id = $db->lastInsertId();

            //add payment
            $paymentDb = new App_Model_Payment();
            $payment_data = array(
                'p_rcp_id'          => $receipt_id,
                'p_payment_mode_id' => $IdPaymentMode,
                'p_cheque_no'       => isset($paymentInfo['refNo']) ? $paymentInfo['refNo'] : null,
                'p_card_no'         => isset($paymentInfo['refNo']) ? $paymentInfo['refNo'] : null,
                'p_cur_id'          => $paymentInfo['rcp_cur_id'],
                'p_amount'          => $receiptAmount,
                'p_migs'            => $paymentInfo['migs_id'],
                'p_fpx'             => $paymentInfo['fpx_id'],
                'created_date'      => date("Y-m-d H:i:s"),
                'created_by'        => 665,
            );

            //$paymentDb->insert($payment_data);
            $db->insert('payment',$payment_data);
            $paymentId = $db->lastInsertId();
            //pr($paymentId);

            $currencyPaid = $paymentInfo['rcp_cur_id'];

            foreach ($invoice as $invData) {
                $migs_id = $paymentInfo['migs_id'];
                $fpx_id  = $paymentInfo['fpx_id'];

                $invoiceId   = $invData['id'];
                $invoiceData = [];
                $amount      = 0;

                if($migs_id) {
                    $invoiceData = $migsTransactionDetailDb->getData($migs_id, $invoiceId);
                    $amount = $invoiceData['tmd_amount'];
                }
                elseif($fpx_id) {
                    $invoiceData = $fpxOrderDetailDb->getData($fpx_id, $invoiceId);
                    $amount = $invoiceData['amount'];
                }


                $data_rcp_inv = array(
                    'rcp_inv_rcp_id' => $receipt_id,
                    'rcp_inv_rcp_no' => $receiptNo,
                    'rcp_inv_invoice_id' => $invoiceId,
                    'rcp_inv_invoice_dtl_id' => 0,
                    'rcp_inv_amount' => $amount,
                    'rcp_inv_cur_id' => $currencyPaid
                );

                //pr($data_rcp_inv);
                //pr($invoiceData);
                //$receiptInvoiceDb->insert($data_rcp_inv);
                $db->insert('receipt_invoice',$data_rcp_inv);

            }

            $db->commit();

            return $receipt_id;

        } catch (Exception $e) {
            $db->rollBack();

            $sysErDb = new App_Model_SystemError();
            $msg['se_IdStudentRegistration'] = $this->studentinfo[0]['idStudentRegistration'];
            $msg['se_title'] = 'Receipt MIGS';
            $msg['se_message'] = $e->getMessage();
            $msg['se_createdby'] = 665;
            $msg['se_createddt'] = date("Y-m-d H:i:s");
            $msg['se_from'] = 1;//corporate
            $sysErDb->addData($msg);

            //pr($e->getMessage());
            return 0;
        }
    }

}