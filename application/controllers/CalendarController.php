<?php

class CalendarController extends Zend_Controller_Action
{
    protected $courseDb;
    protected $contentblockDb;
    protected $blockDataDb;
    protected $enrolDb;
    protected $currDb;
    protected $sidebar  = 1;
    protected $currCoursesDb;

    public function init()
    {
        $this->courseDb = new App_Model_Courses();
        $this->contentBlockDb = new App_Model_ContentBlock();
        $this->blockDataDb = new App_Model_ContentBlockData();
        $this->enrolDb = new App_Model_Enrol();
        $this->currDb = new App_Model_Curriculum();
        $this->currCoursesDb = new App_Model_CurriculumCourses();
    }

	public function viewAction()
    {
		//have you enrolled in this course before
        $auth = Zend_Auth::getInstance();
        $user_id = $auth->getIdentity()->id;

        $this->coursesDb = new App_Model_Courses();
		$this->calendarDb = new App_Model_CalendarEvent();

		//get id
        $id = $this->_getParam('course');
		
        if ($id) {
            $course = $this->coursesDb->fetchRow(array('id = ?' => $id));
    		$this->view->course = $course;
    		
    		$event = $this->calendarDb->fetchAll(array('course_id = ?' => $id, 'visibility = ?' => '1'));
        } else {
            $event = $this->calendarDb->fetchAll(array('user_id = ?' => $user_id, 'visibility = ?' => '1'));
        }

        // date
        $today = date('Y-m-d');
        $weekStart = (date('w') == 0) ? date('Y-m-d') : date('Y-m-d', strtotime('Last Sunday'));
        $weekEnd = (date('w') == 6) ? date('Y-m-d') : date('Y-m-d', strtotime('This Saturday'));

        $weekEvent = $this->calendarDb->getEventsByDuration($weekStart, $weekEnd, false, 0, $id);

        $this->view->weekEvent = $weekEvent;
		$this->view->event = $event;
	}
}
