
<?php

ini_set('display_errors', 'on');

class QualificationController extends Zend_Controller_Action {

    public function init() {
        $this->smsDb = getDB();

        $auth = Zend_Auth::getInstance();
        $this->auth = $auth;

        if (isset($this->auth->getIdentity()->login_as)) {
            $this->sp_id = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        } else {
            $this->sp_id = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }

        Zend_Layout::getMvcInstance()->assign('nowrap', 1);

        $this->uploadDir = Cms_System_Links::dataFile();
        $this->exemptionDb  = new App_Model_ProgramExemption();
        $this->programReg = new App_Model_ProgramRegister();

            }

    public static function userSidebar($active = 'dashboard') {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view->active = $active;
        $view->setBasePath(APPLICATION_PATH . '/views/');
        return $view->render('plugins/usersidebar.phtml');
    }


    public function indexAction() {


        $dbUser = new App_Model_User;
        
        $MembershipDB = new App_Model_Membership_Membership();
        $auth        = Zend_Auth::getInstance()->getIdentity();
        

        $user_id       = $auth->id;

        $this->view->user_id = $user_id;
        $get_info = $dbUser->getUserInfo($user_id);
        $this->view->get_info = $get_info;
        $get_membership = $MembershipDB->get_member($user_id);
        $qualification = $auth->id_qualification;
        
         $get_qualifiacion = $this->programReg->get_qualification($user_id);

         // $get_exem_qualifiacion = $this->programReg->get_exemption_qualification($user_id);
         
         // if(!empty($get_exem_qualifiacion))
         // {
         //     $this->view->get_qualifiacion = $get_exem_qualifiacion;
         // }else{
             $this->view->get_qualifiacion = $get_qualifiacion;

         //}
        

           $get_exem = $this->programReg->exemption_total($user_id);
        $this->view->get_exem = $get_exem;
    
        if(empty($get_qualifiacion) && $get_info['idprogram'] == 1)
        {
            Cms_Common::notify('success', 'Your already applied for the Programme');

             $this->redirect('/membership/index');
        }
       
        
            if(empty($get_qualifiacion))
            {
             $this->redirect('/membership/addqualificaion');   
            }

            if($get_qualifiacion[0]['idprogram'] == 2)
            {
                $this->redirect('/membership/qualificationpayment'); 
            }
       


        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        } else {
            $sp = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }

        if ($sp)
        {
            //get membership info
            $MembershipDB = new App_Model_Membership_Membership();
            $membership = $MembershipDB->getMembership($sp);
            $this->view->membership = $membership;

            //get qualification info
            $applicationDb = new App_Model_SmsApplication();
            $currDb = new App_Model_Curriculum();
            $smsStudentRegistrationDB = new App_Model_StudentRegistration();

            $registrationId = $applicationDb->getIdDataRegistration($auth->external_id);
            $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id);
            if ($myApplication) {
                foreach ($myApplication as $index => $app) {
                    $smsprogramid = $app["IdProgram"];
                    if ($smsprogramid) {
                        $curiculum = $currDb->fetchRow(array("external_id = ?" => $smsprogramid));
                        $myApplication[$index]['cid'] = $curiculum['id'];

                        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjects($sp,$smsprogramid);
                        $myApplication[$index]['modules'] = $StudentSubjectsDetail;
                    }
                }
            }
            $this->view->myApplication = $myApplication;
            //pr($myApplication);

            //enrolment matrix
            $matrix = array(
                'active_flag'                => true,
                'expired_flag'               => false,
                'expired_flag_msg'           => '',
                'qualification_pay'          => true,
                'qualification_pay_msg'      => '',
                'qualification_register'     => true,
                'qualification_register_msg' => ''
            );


            //check active
            if (isset($membership[0]["mr_status_id"]) && $membership[0]["mr_status_id"] != 1607 ) { //active
                $matrix['active_flag'] = false;
            }else{
                if (isset($transaction[0]['required_membership']) && $transaction[0]['required_membership'] == 1) {
                    $matrix['qualification_pay'] = false;
                    $matrix['qualification_register'] = false;
                }
            }

            //check expired
            if (isset($membership[0]["mr_expiry_date"]) && $membership[0]["mr_expiry_date"] != '') {

                $expiredate = strtotime($membership[0]["mr_expiry_date"]);
                $today = strtotime(date("Y-m-d H:i:s"));

                if ($expiredate < $today) { //expired
                    $matrix['expired_flag'] = true;
                }
            }
            // pr($matrix);
            $this->view->matrix = $matrix;

        }//end sp

    }

    public function documentsAction() {

        Cms_Hooks::push('user.sidebar', self::userSidebar('qualification'));

        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;
        $this->view->user_id = $user_id;

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        } else {
            $sp = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }

        $txnid = $this->getParam('txn');
        $this->view->txnid = $txnid;

        $applicationDb = new App_Model_SmsApplication();
        $studentRegDb = new App_Model_StudentRegistration();
        $idStudentRegistration = $studentRegDb->getInfoStudentRegistration($txnid);
        $curriculumDb = new App_Model_Curriculum();

        $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id,$txnid);

        if($myApplication) {
            $smsprogramid = $myApplication["IdProgram"];
            if ($smsprogramid) {
                $lmscurriculumid = $curriculumDb->fetchRow(array("external_id = ?" => $smsprogramid));
                if ($lmscurriculumid) {
                    $this->view->lmscurriculumid = $lmscurriculumid["id"];
                }
            }
        }
        $this->view->myApplication = $myApplication;

        $m_id = 0;
        $this->view->m_id = $m_id;

        $mrc_type = 4;
        $this->view->mrc_type = $mrc_type;

        $checklistDb = new App_Model_Membership_MembershipRegistrationChecklist();
        $document_checklist = $checklistDb->getListData($m_id, $mrc_type);

        $memberRegDoc = new App_Model_Membership_MembershipRegistrationDocuments();
        foreach ($document_checklist as $index => $doc) {
            $member_doc = $memberRegDoc->getDataDocUser($doc['mrc_id'],$m_id,$user_id);

            if ($member_doc) {
                $document_checklist[$index]['mrd_id'] = $member_doc['mrd_id'];
                $document_checklist[$index]['mrd_name'] = $member_doc['mrd_name'];
            }
        }
        $this->view->document_checklist = $document_checklist;

        if ($this->getRequest()->isPost()) {
            $idStudentRegistration = $idStudentRegistration["IdStudentRegistration"];
            $formData = $this->getRequest()->getPost();

            //add documents info
            if (count($_FILES) > 0) {

                foreach ($_FILES as $fieldname => $doc) {

                    if ($doc['name'] != '') {
                        $filedata['mr_id'] = $formData['m_id'];
                        $filedata['mrc_id'] = substr($fieldname, 6);
                        $filedata['mrd_name'] = $doc['name'];
                        $filedata['mrd_createddt'] = date('Y-m-d H:i:s');
                        $filedata['mrd_createdby'] = $this->auth->getIdentity()->id;
                        $memberRegDoc->addData($filedata);
                    }
                }
            }

            //Update Transaction Status
            $dataTxn = array(
                'TransactionStatus' => 1603,//pending approval
            );
            $studentRegDb->updateData($dataTxn,$idStudentRegistration);

            $this->redirect('/qualification');
        }
    }

public function exemptionpaymentAction()

    {

            $dbUser = new App_Model_User;
           $auth = Zend_Auth::getInstance()->getIdentity();
            $user_id = $auth->id;
            $this->view->user_id = $user_id;

             if ($this->getRequest()->isPost())
        {
            Cms_Common::expire();

            $formData = $this->getRequest()->getPost();
          

           $auth = Zend_Auth::getInstance()->getIdentity();

        
                $user_id = $auth->id;
                 $this->view->user_id = $user_id;
          
               
                $dbciifp= new App_Model_Ciifpmb;
                $dbciifpdoc= new App_Model_Ciifpdoc;
            $programRegister = new App_Model_ProgramRegister();
            $auth = Zend_Auth::getInstance()->getIdentity();
            $user_id = $auth->id;
         $data = array('payment_status' => 1657 );
         $user_id1 =  $programRegister->update($data, array('mr_id = ?' => $user_id));
                $upload = new Zend_File_Transfer();
                    $userid1 = $user_id;
                   // $id= $userid1['id'];
                            $membership_id= $user_id;
                             $files  = $_FILES;
 
                            // FileUpload Academic
                               $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
        $target_file1 = 'ExemptionPayment'.date('YmdHis').$_FILES['file1']['name'][$i];
                                 
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH."\\".$target_file1)) {



                         $userDoc = $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
$user_exem =  $dbUser->exemptionpayment($user_id);

                                           } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }
                               if($user_id)
         {

            $get_amount = $this->programReg->total_amount($user_id);
        
          $invoice = new App_Model_InvoiceMain();
               
                
            $data_invioce = array('user_id' => $user_id,
                                   'description' => 'Exemption Payment',
                              'amount' =>  $get_amount[0]['sum'],
                    'invoice_no' => 'CIIF'.  date('YHims').$user_id,
                    'created_user' => $user_id,
                    'created_date' => date("Y-m-d"),
                    //'payment_status'=>$member_status[0]['idDefinition']
                         );
                    $invoice_no = $invoice->insert($data_invioce);
                    }


                            Cms_Common::notify('success', 'Payment is Successfull!');
                             $this->redirect('/qualification');
            
            
        }
       

    }


        public function applyexemptionAction(){

        
        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;
        $this->view->user_id = $user_id;
         
        $get_qualifiacion = $this->programReg->modules_exemption($user_id);
 
         $get_exem = $this->programReg->exemption_total($user_id);
         if($get_exem)
         {
            $this->redirect('/qualification/exemptionfee');
         }
       
        $this->view->StudentSubjectsDetail = $get_qualifiacion;
        // $userModel = new App_Model_User();

        if($this->getRequest()->isPost())
        {
             $formData = $this->getRequest()->getPost();
                
                $dbUser = new App_Model_User;
                
                $user_exem =  $dbUser->exemption($user_id);
                
            

        if($formData)
        {       
        foreach ($formData['program_id'] as $exemption) {
              $data['user_id']       = $user_id;
              $data['program_id']    = $exemption;
              $data['program_route'] = $get_qualifiacion[0]['program_route'];
              $data['program_level'] = $get_qualifiacion[0]['program_level'];

            $data['created_date']    = date('y-m-d H:i:s');
                $data['created_by']  = $user_id;
                       $this->exemptionDb->insert($data);

           
            foreach ($formData['exemtion_status'] as $value) {
               
            $update['exempted_status'] = 1;
            $this->exemptionDb->update($update,array('program_id = ?' =>$value));
    
           
           

           
            $get_amount = $this->exemptionDb->get_amount($value);
           
    

            $updateamt['f_amount'] = $get_amount['amount'];

                
             
         $exem_program =   $this->exemptionDb->update($updateamt,array('program_id = ?' =>$value));
            
        }

        $get_amount = $this->programReg->total_amount($user_id);
         
         
         }
         if($get_amount)
         {
          $invoice = new App_Model_PerformaInvoice();
               
                
            $data_invioce = array('user_id' => $user_id,
                                   'description' => 'Exemption Payment',
                              'amount' => $get_amount[0]['sum'],
                    'invoice_no' => 'CIIF'.  date('YHims').$user_id,
                    'created_user' => $user_id,
                    'created_date' => date("Y-m-d"),
                         );
                    $invoice_no = $invoice->insert($data_invioce);
            }

        
}


$this->redirect('/qualification/exemptionfee');
}
}

    public function exemptionfeeAction()
    {
        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;
        $this->view->user_id = $user_id;
         
        $get_qualifiacion = $this->programReg->exemption_total($user_id);

          $get_amount = $this->programReg->total_amount($user_id);
        $this->view->get_amount = $get_amount;
       
        $this->view->StudentSubjectsDetail = $get_qualifiacion;

       


        
}
    public function viewDocumentsAction() {

        //Cms_Hooks::push('user.sidebar', self::userSidebar('qualification'));
        $MembershipDB = new App_Model_Membership_Membership();

        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;
        $this->view->user_id = $user_id;


        $auth        = Zend_Auth::getInstance()->getIdentity();
        $txnid = $this->getParam('id');

        $user_id       = $auth->id;
        $this->view->user_id = $user_id;
        
        $get_membership = $MembershipDB->get_member($user_id);
        $qualification = $auth->id_qualification;
        
    $get_qualifiacion = $this->programReg->get_qualification($user_id);
        

        $get_status = $MembershipDB->get_status($get_membership['mr_status']);
        $this->view->get_status = $get_status;
       

        $this->view->get_qualifiacion = $get_qualifiacion;

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        } else {
            $sp = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }

        $txnid = $this->getParam('txn');
        $this->view->txnid = $txnid;

        $applicationDb = new App_Model_SmsApplication();
        $studentRegDb = new App_Model_StudentRegistration();
     
        $curriculumDb = new App_Model_Curriculum();

        
        $m_id = 0;
        $this->view->m_id = $m_id;

        $mrc_type = 4;
        $this->view->mrc_type = $mrc_type;

        $checklistDb = new App_Model_Membership_MembershipRegistrationChecklist();
        $memberRegDoc = new App_Model_Membership_MembershipRegistrationDocuments();
        $MembershipDB = new App_Model_Membership_Membership();


      
        $get_doc = $MembershipDB->get_doc($user_id);
        $this->view->document_checklist = $get_doc;
    }

    public function registerModulesAction() {

        Cms_Hooks::push('user.sidebar', self::userSidebar('qualification'));

        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;
        $this->view->user_id = $user_id;

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        } else {
            $sp = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }

        $cid = $this->getParam('cid');
        $txnid = $this->getParam('txnid');
        $this->view->txnid = $txnid;

        $ApplicantTransactionDb = new App_Model_Records_ApplicantTransaction();
        $curriculumDb = new App_Model_Curriculum();
        $curriculumCoursesDb = new App_Model_CurriculumCourses();
        $enrolDb = new App_Model_Enrol();
        $courseGroupDb = new App_Model_Qualification_CourseGroup();
        $studentRegDb = new App_Model_StudentRegistration();
        $LandscapeSubjectDB = new App_Model_LandscapeSubject();
        $StudentExemptionDB = new App_Model_StudentExemption();
        $MembershipDB = new App_Model_Membership_Membership();

        $TransactionData = $ApplicantTransactionDb->getTransactionById($txnid);
        $idStudentRegistration = $studentRegDb->getInfoStudentRegistration($txnid);
        $this->view->idStudentRegistration = $idStudentRegistration["IdStudentRegistration"];

        if ($TransactionData["at_appl_type"]==1) {//Membership
            $view = "no";
        } else if ($TransactionData["at_appl_type"]==2) {//Qualification
            if ($idStudentRegistration["TransactionStatus"]==1604) {//approved
                $view = "yes";
            } else {
                $view = "no";
            }
        } else {//Membership & Qualification
            $MembershipData = $MembershipDB->getMembershipByTxnId($txnid);
            if ($MembershipData["mr_status_id"] == 1607 && $idStudentRegistration["TransactionStatus"]==1604) {
                $view = "yes";
            } else {
                $view = "no";
            }
        }
        $this->view->view = $view;

        $StudentSubjectsDetail = $studentRegDb->getStudentSubjectsDetail($idStudentRegistration["sp_id"]);
        $this->view->StudentSubjectsDetail = $StudentSubjectsDetail;

        $totalElective = $LandscapeSubjectDB->getRequirementCourseByProgram($idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"],275);//get elective subjects
        $totalCore = $LandscapeSubjectDB->getRequirementCourseByProgram($idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"],394);//get core subjects

        $totalElectivePaper = (isset($totalElective[0]["CreditHours"])) ? $totalElective[0]["CreditHours"]:0;
        $totalCorePaper = (isset($totalCore[0]["CreditHours"])) ? $totalCore[0]["CreditHours"]:0;

        $pathwaySubjectListElective = $LandscapeSubjectDB->getLandscapeCourseByProgram($idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"],275);//get elective subjects
        $pathwaySubjectListCore = $LandscapeSubjectDB->getLandscapeCourseByProgram($idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"],394);//get core subjects

        $arraySubjectProgramCore = array();
        foreach ($pathwaySubjectListCore as $psc) {
            array_push($arraySubjectProgramCore, $psc["IdSubject"]);
        }

        if ($totalElectivePaper != count($pathwaySubjectListElective)) {
            $arraySubjectProgramElective = array();
            foreach ($pathwaySubjectListElective as $pse) {

                $getStudentExemptedCourse = $StudentExemptionDB->getDataStud($idStudentRegistration["IdStudentRegistration"],$idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"],$pse["IdSubject"]);
                if ($getStudentExemptedCourse) {
                    array_push($arraySubjectProgramElective, $pse["IdSubject"]);
                }
            }
        } else {
            $arraySubjectProgramElective = array();
            foreach ($pathwaySubjectListElective as $pse) {
                array_push($arraySubjectProgramElective, $pse["IdSubject"]);
            }
        }

        $arraySubjectProgram = array_merge($arraySubjectProgramCore,$arraySubjectProgramElective);

        $cur = $curriculumDb->fetchRow(array("id = ?" => $cid));

        if ($this->getRequest()->isPost())
        {
            $idStudentRegistration = $idStudentRegistration["IdStudentRegistration"];
            $formData = $this->getRequest()->getPost();

            //Update Transaction Status
            $dataTxn = array(
                'TransactionStatus' => 1603,
            );
            $studentRegDb->updateData($dataTxn,$idStudentRegistration);
            $this->redirect('/dashboard');

        }

        if ( empty($cur)  )
        {
            return Cms_Render::error(Cms_Options::__('label_curriculum').' doesn\'t existsssss.');
        }
        else
        {
            $cur = $cur->toArray();
            $cur['parent_external_id'] = 0;

            if($cur['parent_id']) {
                $parent = $curriculumDb->fetchRow(array("id = ?" => $cur['parent_id']));
                $cur['parent_external_id'] = $parent['external_id'];
            }

        }
        //canview
        if ( $cur['visibility'] == 0 && Zend_Auth::getInstance()->getIdentity()->role != 'administrator' )
        {
            return Cms_Render::error(Cms_Options::__('label_curriculum').' is not available at the moment.');
        }

        //breadcrumbs
        $bc = array(
            'home',
            array($this->view->baseUrl().'/courses' => $this->view->translate('Modules')),
            $cur['name']
        );

        //courses
        $getcourses = $curriculumCoursesDb->getData2($cid,$arraySubjectProgram);

        $x=0;
        foreach ($getcourses as $gcourse) {
            $gcourse["external_id"];

            $subjectinfo = $StudentExemptionDB->getDataStudSubject($idStudentRegistration['IdStudentRegistration'], $idStudentRegistration['IdProgram'], $idStudentRegistration['IdMajoringPathway'],$gcourse["external_id"]);
            $getcourses[$x]["subjectinfo"] = $subjectinfo;

            //get data from tbl_studentexemption
            $exemptionInfo2 = $StudentExemptionDB->getDataStud($idStudentRegistration['IdStudentRegistration'], $idStudentRegistration['IdProgram'], $idStudentRegistration['IdMajoringPathway'],$gcourse["external_id"]);
            $getcourses[$x]["exemptionstatus"] = $exemptionInfo2;

            $IdSubject = $gcourse["external_id"];
            $courseGroup = $courseGroupDb->getData($IdSubject = $gcourse["external_id"]);
            $getcourses[$x]["schedule"] = $courseGroup;

            $x++;
        }

        //get data from tbl_studentexemption
        $exemptionInfo = $StudentExemptionDB->getDataByStudRegID($idStudentRegistration['IdStudentRegistration'], $idStudentRegistration['IdProgram'], $idStudentRegistration['IdMajoringPathway']);
        $this->view->exemptionInfo = $exemptionInfo;

        $this->view->courses = $getcourses;
        $this->view->cur = $cur;

        //hasenrolled
        $hasenrolled  = false;

        if ( Zend_Auth::getInstance()->hasIdentity() )
        {
            $check = $enrolDb->getEnrol(array('user_id = ?' => Zend_Auth::getInstance()->getIdentity()->id, 'regtype = ?' => 'curriculum', 'curriculum_id = ?' => $cid));
            $hasenrolled = !empty($check) ? true : false;
        }

        Cms_Hooks::push('body.bottom', Cms_Render::login());
        $this->view->hasenrolled = $hasenrolled;
        $this->view->cur = $cur;
        $this->view->cid = $cid;
    }

    public function enrollModulesAction() {

        $this->_helper->layout->disableLayout();

        $studRegDb = new App_Model_StudentRegistration();
        $invoiceidDB = new App_Model_Order();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            $txnId = $formData['txnid'];

            for($i=1; $i<=$formData['totalsubject'];$i++){

                if($formData['total']){

                    $mdpriceexemptsts = $formData['mdprice'.$i];

                    if ($mdpriceexemptsts) {
                        $arrayval = explode(":", $mdpriceexemptsts);
                        $mdprice = $arrayval[0];
                        $exemptedsts = $arrayval[1];

                        if ($exemptedsts == "") {
                            $exemptstatus = 0;
                        } else {
                            $exemptstatus = 1;
                        }

                        $idSubject = $formData['idsubject' . $i];
                        $idStudentRegistration = $formData['idStudentRegistration'];
                        $dataSubject = array(
                            'IdStudentRegistration' => $formData['idStudentRegistration'],
                            'IdSubject' => $idSubject,
                            'ExemptionStatus' => $exemptstatus,
                            'SubjectsApproved' => 30,
                            'Active' => 1,
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()')
                        );
                        $hasRegistered = $studRegDb->checkStudentRegSubjects($idStudentRegistration, $idSubject);

                        if (!$hasRegistered) {
                            if (isset($mdprice) && $mdprice != "") {

                                if ($formData['IdProgram'] == 52) {//PKMC
                                    //update IdCourseTaggingGroup at tbl_studentregsubjects
                                    $IdCourseTaggingGroup = $formData['IdCourseTaggingGroup' . $i];
                                    $class_detail = array(
                                        'IdCourseTaggingGroup' => $IdCourseTaggingGroup
                                    );
                                    $dataSubject = array_merge($class_detail, $dataSubject);
                                }

                                $studRegDb->addRegSubjectsData("tbl_studentregsubjects",$dataSubject);
                            }
                        }
                    }
                }
            }
            //add invoice
            $txnId = $txnId;
            $dataInvoice = array(
                'bill_number' => 'IN'.date('Y').rand(),
                'trans_id' => $txnId,
                'IdStudentRegistration' => $idStudentRegistration,
                'bill_amount' => $formData['total'],
                'status' => 'X'
            );
            $invoiceid = $invoiceidDB->addInvoiceNew("invoice_main",$dataInvoice);
            $this->_redirect('/register/checkout-modedemo/i/'.$txnId);
        }
    }

    public function viewModulesAction() {

        Cms_Hooks::push('user.sidebar', self::userSidebar('qualification'));

        $auth = Zend_Auth::getInstance()->getIdentity();
        $user_id = $auth->id;
        $this->view->user_id = $user_id;
         
        $get_qualifiacion = $this->programReg->modules_type($user_id);
        
       
        $this->view->StudentSubjectsDetail = $get_qualifiacion;
    }
}

?>

