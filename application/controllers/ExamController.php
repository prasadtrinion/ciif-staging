<?php

class ExamController extends Zend_Controller_Action
{
    
 	public function init()
    {
        $this->smsDb = getDB();

        $auth = Zend_Auth::getInstance();
        $this->auth = $auth;

   		if(isset($this->auth->getIdentity()->login_as)){
            $this->sp_id = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        }else{
            $this->sp_id = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }
        
    } 
    
    public static function userSidebar($active = 'dashboard')
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view->active = $active;
        $view->setBasePath(APPLICATION_PATH . '/views/');
        return $view->render('plugins/usersidebar.phtml');
    }


    public function viewExamDetailAction()
    {
        $auth = Zend_Auth::getInstance()->getIdentity();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $id = $this->getParam('id');

        $myexams = $ExamregDB->getExamregid($id);

        if(!$myexams) {
            return Cms_Render::error('Data not found');
        }
        if(isset($auth->login_as)){
            $auth->external_id = $auth->sp_id;
        }else{
            $auth->external_id = $auth->external_id;
        }

        if($auth->external_id != $myexams['std_id']) {
            return Cms_Render::error('You are not authorized to view this data');
        }
        //echo $myexams['days']."----";
        $limit_days      = $myexams['days'];
        $exam_date       = $myexams['es_date'];//$exam_date = '2017-04-26';echo $exam_date;
        $exam_date_unix  = strtotime($exam_date);
        $today_date      = date('Y-m-d');
        $today_date_unix = strtotime($today_date);
        $limit_date      = date('Y-m-d', strtotime('-'.$limit_days.' days', $exam_date_unix));
        $limit_date_unix = strtotime($limit_date);
        $allow_payment   = true;
    
        //$myexams['er_registration_type'] = 1;
    
        if ($today_date_unix >= $limit_date_unix && in_array($myexams['er_registration_type'], array(0, 9)))
        {
            $allow_payment = false;
        }
        
        $params   = array(
            'examschedule_id'    => $myexams['examschedule_id'],
            'examcenter_id'      => $myexams['examcenter_id'],
            'examtaggingslot_id' => $myexams['examtaggingslot_id']
        );
        $response = Cms_Curl::__(SMS_API . '/get/Examination/do/getRoom', $params);
        
        if (isset($response['examroom_id']) && $response['examroom_id'] == -1 && in_array($myexams['er_registration_type'], array(0, 9)))
        {
            $myexams['must_reschedule'] = 1;
        }
        
        if (!$myexams['days'])
        {
            $myexams['days'] = 14;
        }

        //views
        $this->view->myexams       = $myexams;
        $this->view->allow_payment = $allow_payment;

        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('exam'));

        //breadcrumbs
        $bc = array(
            'home',
        );
        $this->view->bc = $bc;
    }

    public function scheduleExamAction()
    {
        $auth = Zend_Auth::getInstance()->getIdentity();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $examSetupDB = new App_Model_Exam_ExamSetup();

        $id = $this->getParam('id');

        $myexams = $ExamregDB->getExamregid($id);

        //views
        $this->view->myexams = $myexams;

        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('exam'));

        $bes = $ExamregDB->getDataByErId($id);

        foreach ($bes as $i => $row) {
            $params = array(
                'es_id'           => $row['examsetup_id'],
                'category_id'     => 1,
                'pe_id'           => $myexams['esd_pe_id']
                //'examschedule_id' => $row['examschedule_id'],
                
            );
            $result   = Cms_Curl::__(SMS_API . '/get/Examination/do/getExamDetail', $params);
            $bes[$i]['details'] = $result;
            // $bes[$i]['details'] = $examSetupDB->getExamDetail($row['examsetup_id'], $row['examschedule_id']);
        }

        $this->view->bes = $bes;
    }


    public function updateScheduleExamAction()
    {
        $auth = Zend_Auth::getInstance();
        $erDB = new App_Model_Exam_ExamRegistration();
        $ecrDB = new App_Model_Exam_ExamRoom();

        $id = $this->getParam('id');
        $er = $erDB->getData($id);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $remarks  = 'Defer (LMS)';

            if($auth->getIdentity()->login_as){//if admin
                $modifiedby = $auth->getIdentity()->login_as;
            }else{
                $modifiedby = $auth->getIdentity()->id;
            }

            $changescheduleexam = array(
                'modifieddt'         => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                'modifiedby'         => $modifiedby,
//                'modifiedby'         => $auth->getIdentity()->id,
                'examsetupdetail_id' => $formData[0]['examsetupdetail_id'],
                'examschedule_id'    => $formData[0]['new-examschedule_id'],
                'examcenter_id'      => $formData[0]['new-examcenter_id'],
                'examtaggingslot_id' => $formData[0]['new-examtaggingslot_id']
            );
            
            if (!$changescheduleexam['examsetupdetail_id'])
            {
                return Cms_Render::error('Something is wrong with your registration. Please try again #101');
            }
    
            $db2    = getDB2();
            $select = $db2->select()->from(array('a' => 'exam_setup_detail'))->where('a.esd_id = ?', $changescheduleexam['examsetupdetail_id']);
            $esd    = $db2->fetchRow($select);
    
            if (!$esd)
            {
                return Cms_Render::error('Something is wrong with your registration. Please try again #102');
            }
    
            if (!$changescheduleexam['examcenter_id'])
            {
                return Cms_Render::error('Something is wrong with your registration. Please try again #104');
            }
    
            if (!$changescheduleexam['examtaggingslot_id'])
            {
                return Cms_Render::error('Something is wrong with your registration. Please try again #105');
            }
            
            if ($er['must_reschedule'])
            {
                $remarks = 'Reschedule (LMS)';
                $changescheduleexam['must_reschedule']      = 0;
                $changescheduleexam['reschedule']           = 1;
                $changescheduleexam['attendance_status']    = null;
//                $changescheduleexam['modifiedby']           = $auth->getIdentity()->external_id;
                $changescheduleexam['modifiedby']           = $modifiedby;
                $changescheduleexam['attendance_updby']     = 665;
                $changescheduleexam['attendance_upddt']     = $changescheduleexam['modifieddt'] = date('Y-m-d H:i:s');
                $changescheduleexam['attendance_updrole']   = $auth->getIdentity()->IdRole;
                $changescheduleexam['mc_reason']            = null;
                $changescheduleexam['result_status']        = 0;
                $changescheduleexam['pass']                 = null;
                $changescheduleexam['grade_name']           = null;
                $changescheduleexam['total_marks']          = null;
                $changescheduleexam['result_status']        = null;
                $changescheduleexam['result_endorsed_by']   = 0;
                $changescheduleexam['result_endorsed_date'] = null;
                $changescheduleexam['exam_status']          = null;
                $changescheduleexam['pull_status']          = null;
                $changescheduleexam['pull_date']            = null;
            }
            
            if ($er['examroom_id'])
            {
                $params = array(
                    'examschedule_id'    => $changescheduleexam['examschedule_id'],
                    'examcenter_id'      => $changescheduleexam['examcenter_id'],
                    'examtaggingslot_id' => $changescheduleexam['examtaggingslot_id'],
                );
                $response = Cms_Curl::__(SMS_API . '/get/Examination/do/getRoom', $params);
                $examroom_id = $response['examroom_id'];
    
                if($examroom_id == -1) {
                    $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate("Failed to change schedule. Slots are fully booked")));
                    $this->redirect('/exam/view-exam-detail/id/' . $id);
                }
    
                $changescheduleexam['examroom_id'] = $examroom_id;
            }
            
            $result  = $erDB->updateDataSc($changescheduleexam, $id, $remarks);
            
            if ($result)
            {
                if (in_array($er['er_registration_type'], array('01', '04')))
                {
                    $params = array(
                        'er_id'      => $er['er_id'],
                        'email'      => 1,
                        'reschedule' => 1
                    );
                    $response = Cms_Curl::__(SMS_API . '/get/Examination/do/examSlip', $params);
                }
            }
            
            Cms_Common::notify('success', 'Exam schedule successfully updated');
            $this->redirect('/exam/view-exam-detail/id/' . $id);

        }
    }

    public function getExamLandscapeAction()
    {
        //echo '2';exit;
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //$params = array('exam_closing_date' => $this->identity->exam_closing_date);
        $params = array('exam_closing_date' => 0);

        //$IdProgram = '1';
        $IdProgram = $this->_getParam('IdProgram', 0);
        // $landscapeDB = new App_Model_Smsconnect();
        // $landscapes = $landscapeDB->getExamLandscape($IdProgram, $params = 0);
//        pr($landscapes);
        // echo json_encode($landscapes);
        // exit;

        $params = array(
            'IdProgram'   => $IdProgram,
            'category_id' => 1
        );
       
        $result   = Cms_Curl::__(SMS_API . '/get/Examination/do/getExamLandscape', $params);
        echo json_encode($result);
        exit;
    }

    public function getExamOptionAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //$params = array('exam_closing_date' => $this->identity->exam_closing_date);
        //$params = array('exam_closing_date' => 0) ;

        $IdLandscape = $this->_getParam('IdLandscape', 0);
        $std_id = $this->_getParam('std_id', 0);
        //$IdLandscape = '2';
        // $examSetupDB = new App_Model_Smsconnect();
        // $options = $examSetupDB->getExamOption($IdLandscape, $params = 0);
        //pr($options);
        // echo json_encode($options);
        // exit;

        $params = array(
            'IdLandscape' => $IdLandscape,
            'std_id'      => $std_id,
            'category_id' => 1
        );
       
        $result   = Cms_Curl::__(SMS_API . '/get/Examination/do/getExamOption', $params);
        echo json_encode($result);
        exit;
    }

    public function getExamDetailAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //$params = array('exam_closing_date' => $this->identity->exam_closing_date);
        $params = array('exam_closing_date' => 30);

        $es_id = $this->_getParam('es_id', 0);
        //$es_id = '6';

        // $examSetupDB = new App_Model_Smsconnect();
        // $details = $examSetupDB->getExamDetail($es_id, 0, $params);

        // echo json_encode($details);
        // exit;

        $es_id  = $this->_getParam('es_id', 0);
        $std_id = $this->_getParam('std_id', 0);

        //tmbh student_profile id
        $auth          = Zend_Auth::getInstance();
        $params = array(
            'es_id'       => $es_id,
            'std_id'      => $std_id,
            'category_id' => 1,
            'sp_id' => $auth->getIdentity()->external_id,
            'sts' => 'readprofile',
        );
        
        $result   = Cms_Curl::__(SMS_API . '/get/Examination/do/getExamDetail', $params);
        echo json_encode($result);
        exit;
    }

    public function getExamDateAction()
    {
        $this->_helper->layout->disableLayout();

        //$params = array('exam_closing_date' => $this->identity->exam_closing_date);

        $esd_id = $this->_getParam('esd_id', 0);
        $ec_id = $this->_getParam('ec_id', 0);
        $year   = $this->_getParam('year', date('Y'));

        $params = array(
            'esd_id'      => $esd_id,
            'ec_id'       => $ec_id,
            'category_id' => 1,
            'year'        => $year
        );
        
        $schedules   = Cms_Curl::__(SMS_API . '/get/Examination/do/getExamDate', $params);
        $exam_center = Cms_Curl::__(SMS_API . '/get/Examination/do/getExamCenter', array('ec_id' => $ec_id));
        $address     = null;
        
        if (isset($exam_center['error']) && $exam_center['error'] == 0)
        {
            $address = $exam_center['data']['address'];
        }
        
        $this->view->schedules      = $schedules;
        $this->view->schedules_json = json_encode($schedules);
        $this->view->year           = $year;
        $this->view->esd_id         = $esd_id;
        $this->view->address        = $address;
    }

    public function examcheckoutAction()
    {
        $auth          = Zend_Auth::getInstance();
        $paymentModeDB = new App_Model_Finance_PaymentMode();
        $invoiceMainDB = new App_Model_Finance_InvoiceMain();
        $erDB          = new App_Model_Exam_ExamRegistration();
        $ermDB         = new App_Model_Exam_ExamRegistrationMain();
        $currCoursesDb = new App_Model_CurriculumCourses();
        $orderDb       = new App_Model_Order();
        $currDB        = new App_Model_Curriculum();

        if ($this->getRequest()->isPost()) {

            $orderDb = new App_Model_Order();

            if(isset($auth->getIdentity()->login_as)){//if admin
                $auth->getIdentity()->external_id = 1;
                $createby = $auth->getIdentity()->external_id;//$createby  = 1
                $ordercreateby =  $auth->getIdentity()->login_as;
                $sp = $auth->getIdentity()->sp_id;
            }else{
                $createby = $auth->getIdentity()->external_id;
                $ordercreateby = $auth->getIdentity()->id;
                $sp = $createby;
            }

            //$sp = $auth->getIdentity()->external_id; //student_profile
            //echo "<pre>";print_r($auth->getIdentity());//exit;
            $formData            = $this->getRequest()->getPost();
           
            $registrationInfo    = $erDB->getRegistration($sp);
            $learningmode        = $registrationInfo['learningmode'];
            $IdProgram           = $formData['IdProgram'];
            $IdProgramScheme     = $formData['IdProgramScheme'];
            $IdLandscape         = $formData['IdLandscape'];
            $fs_id               = $formData['fs_id'];
            
            $this->view->program = $erDB->getProgram($formData['IdProgram']);
            $courses             = $currDB->getDataByExternal($IdProgram);
            $profile             = $erDB->getProfile($sp);
            $sp_id               = $sp;

            if (!$formData['IdProgram'])
            {
                return Cms_Render::error('Something is wrong with your registration. Please try again #001');
            }
    
            if (!$formData['IdLandscape'])
            {
                return Cms_Render::error('Something is wrong with your registration. Please try again #002');
            }
    
            if (!$formData['IdProgramScheme'])
            {
                return Cms_Render::error('Something is wrong with your registration. Please try again #003');
            }
            
            if (!$formData['examsetup_id'])
            {
                return Cms_Render::error('Something is wrong with your registration. Please try again #004');
            }
    
            foreach ($formData['examschedule_id'] as $i => $selected)
            {
                if (!$selected)
                {
                    continue;
                }
                
                if (!$formData['examsetupdetail_id'][$i])
                {
                    return Cms_Render::error('Something is wrong with your registration. Please try again #101');
                }
                
                $db2    = getDB2();
                $select = $db2->select()->from(array('a' => 'exam_setup_detail'))->where('a.esd_id = ?', $formData['examsetupdetail_id'][$i]);
                $esd    = $db2->fetchRow($select);
                
                if (!$esd)
                {
                    return Cms_Render::error('Something is wrong with your registration. Please try again #102');
                }
                
                if ($esd['es_id'] != $formData['examsetup_id'])
                {
                    return Cms_Render::error('Something is wrong with your registration. Please try again #103');
                }
    
                if (!$formData['examcenter_id'][$i])
                {
                    return Cms_Render::error('Something is wrong with your registration. Please try again #104');
                }
    
                if (!$formData['examtaggingslot_id'][$i])
                {
                    return Cms_Render::error('Something is wrong with your registration. Please try again #105');
                }
            }
            
            foreach ($formData['examsetupdetail_id'] as $index => $examsetupdetail_id) {
                
                if (!$formData['examschedule_id'][$index])
                {
                    continue;
                }

                $params = array(
                    'std_id'             => $sp_id,
                    'IdProgram'          => $IdProgram,
                    'examsetupdetail_id' => $examsetupdetail_id
                );
                
                $exam_check = Cms_Curl::__(SMS_API . '/get/Examination/do/checkRegistrationStatus', $params);
                
                if ($exam_check['data']['allow_registration'] == 0) {
                    if ($exam_check['data']['has_record'] == 1) {
                        $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate("You are already enroled to this examination")));
                    }
                    else {
                        $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate("You are not allwowed to enrol to this examination")));
                    }
                    $this->redirect('/user/dashboard');
                }
            }
            //check existing - taknak check dl sbb ibfim kata boleh register byk learning mode utk 1 program (TBC)
//            $orderExist = $orderDb->getOrderExam($sp, $courses['id']);

//            if (empty($orderExist)) {

                //check existing - tbl_studentregistration
                $registrationCourse = $erDB->getDataRegistration($sp_id,$IdProgram, $IdProgramScheme);

                if(empty($registrationCourse)){
                    //insert tbl_studentregistration
                    $myRegistrationId = $this->generateRegistrationID();
                    $dataStdRegistration = array(
                        'sp_id'           => $sp_id,
                        'batch_id'        => 0,
                        'registrationId'  => $myRegistrationId,
                        'IdLandscape'     => $IdLandscape,
                        'IdProgram'       => $IdProgram,
                        'IdProgramScheme' => $IdProgramScheme,
                        'IdIntake'        => 0,
                        'fs_id'           => $fs_id,
                        'fs_id_upddate'   => date('Y-m-d H:i:s'),
                        'createdDate'     => date('Y-m-d H:i:s'),
//                        'createdBy'       => $sp_id,
                        'createdBy'       => $createby,
                        'company_id'      => $sp_id,
                        'category'        => 1,
                        'profileStatus'   => 92,
                    );

                    $IdStudentRegistration = $erDB->addDataStudentRegistration($dataStdRegistration);

                }else{
                    $IdStudentRegistration = $registrationCourse['IdStudentRegistration'];
                }

                $examRegId = array();

                $er_main = array(
                    'IdStudentRegistration' => $IdStudentRegistration,
                    'IdProgram'             => $IdProgram,
                    'ber_id'                => 0,
                    'created_at'            => date('Y-m-d H:i:s'),
//                    'created_by'            => $sp_id
                    'created_by'       => $createby,
                );

                $er_main_id = $er_main['id'] = $ermDB->addData($er_main);
            //echo $sp_id."-sp";print_r($er_main);exit;
                foreach ($formData['examschedule_id'] as $i => $schedulerow) {

                    if (!$schedulerow)
                    {
                        continue;
                    }

                    $examScheduleInfo = $erDB->getDataByExamSchedule($schedulerow);

                    $exam_registration = array(
                        'registrationId'     => Cms_Curl::__(SMS_API . '/get/Examination/do/generateExaminationId', array()),
                        'IdProgram'          => $formData['IdProgram'],
                        'er_main_id'         => $er_main_id,
                        'landscape_id'       => $formData['IdLandscape'],
                        'examsetup_id'       => $formData['examsetup_id'],
                        'createddt'          => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'createdby'          => $auth->getIdentity()->id,
                        'student_id'         => $IdStudentRegistration, //$IdStudentRegistration,
                        'examsetupdetail_id' => $examScheduleInfo['es_esd_id'],
                        'examschedule_id'    => $schedulerow,
                        'examcenter_id'      => $formData['examcenter_id'][$i],
                        'examtaggingslot_id' => $formData['examtaggingslot_id'][$i],
                        'intakeprogram_id'   => 0,
                        'er_registration_type'=> 1, //auto registered; add for aicb 300118
                        'fs_id'              => $fs_id,
                        'pass'               => null,
                        'grade_name'         => null,
                        'exam_status'        => null,
                        'total_marks'        => null,
                        'createddt'          => date('Y-m-d H:i:s'),
                        'createdby'          => $createby
//                         'createdby'          => $sp_id

                    );

                    $erId = $erDB->addData($exam_registration);

                    $examRegId[] = $erId;

                }

                //generate proforma invoice
                $paramProforma = array(
                    'IdSubject'             => 0,
                    'IdStudentRegistration' => $IdStudentRegistration,
                    'categoryId'            => ($auth->getIdentity()->nationality == 'MY') ? 579 : 580,
                    'totalStudent'          => 1,
                    'item'                  => 879, //exam,
                    'er_main_id'            => $er_main_id
                );

//            pr($paramProforma);

//                $proformaInvoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateProformaIndividual', $paramProforma);
//
//           /* echo "<pre>";
//            print_r($proformaInvoice);
//            exit;*/
//
//                if (!isset($proformaInvoice['error'])) {
//                    return Cms_Render::error('Unable to generate proforma invoice. #1');
//                }
//
//                if (isset($proformaInvoice['error']) && $proformaInvoice['error'] == 1) {
//                    return Cms_Render::error('Unable to generate proforma invoice. #2');
//                }
//
//                //generate invoice from proforma
//                //generate invoice from proforma
//                $paramsInvoice = array('ProformaId' => $proformaInvoice['id'], 'creatorId' => 665, 'from' => 1, 'returnCurrency' => 1);
//                $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoice', $paramsInvoice);
////            echo "<pre>";
////            print_r($invoice);
////            exit;
//                $invoiceAmount = $invoice['amount'];
//                if (isset($invoice['error']) && $invoice['error'] == 0 && $invoiceAmount > 0) {
//                    $invoiceId = $invoice['invoiceId'];
//                    $invoiceNo = $invoice['invoiceNo'];
//                    $currencyId = $invoice['currencyId'];
//
//
//                    //add order
//                    $order = array(
//                        'user_id' => $auth->getIdentity()->id,
//                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
////                        'created_by' => $auth->getIdentity()->id,
//                        'created_by' => $ordercreateby,
//                        'amount' => $invoiceAmount,
//                        'paymentmethod' => 'MIGS',
//                        'status' => 'PENDING',
//                        'invoice_id' => $invoiceId,
//                        'invoice_no' => $invoiceNo,
//                        'invoice_external' => $invoiceId,
//                        'regtype' => 'exam',
//                        'curriculum_id' => $courses['id'],
//                        'item_id' => $er_main_id,
//                        'learningmode' => $learningmode,
//                        'schedule' => implode(',', $formData['examschedule_id']),
//                        'external_id' => $sp
//                    );
//
//                    $order_id = $orderDb->insert($order);
//
//                    //update invoice_main
//                    $invoiceMainDB->updateData(array('order_id' => $order_id), $invoiceId);
//
//                    //update exam_registration
//
//                    if($examRegId) {
//                        foreach ($examRegId as $erid) {
//                            $erDB->updateData(array('order_id'=>$order_id), $erid, 'New registration - assign order_id (LMS)');
//                        }
//                    }
//
//                    //$this->sendMailernotpaid($orderId);
//                    /*//redirect
//                    $params = array(
//                        'token' => Cms_Common::token('ibfimpay'),
//                        'invoiceId' => $invoiceId,
//                        'requestFrom' => 1,
//                        'currencyId' => $currencyId,
//                        'invoiceNo' => $invoiceNo,
//                        'totalAmount' => Cms_Common::MigsAmount($invoiceAmount),
//                        'die' => 1,
//                        'order_id' => $order_id,
//                        'registrationItem' => 879
//                    );
//
//                    //redirect to payment gateway
//                    $urlMigs = Cms_Curl::__(URL_PAYMENT . '/migs/', $params);
//
//                    $this->view->pay_url = $urlMigs;
//
//                    header('location: ' . $urlMigs);*/
//
//                } else {
//                    if($examRegId) {
//                        foreach ($examRegId as $erid) {
//                            $erDB->updateData(array('er_registration_type' => 2), $erid, 'Drop - Failed to generate invoice (LMS)');
//                        }
//                    }
//
//                    $db = Zend_Db_Table::getDefaultAdapter();
//                    $db->insert('system_log', array(
//                        'logtype'      => 'enrolExam',
//                        'message'      => json_encode($invoice),
//                        'created_date' => date('Y-m-d H:i:s'),
//                        'created_by'   => 1
//                    ));
//
//                    $external_id   = Zend_Auth::getInstance()->getIdentity()->external_id;
//                    $sync_response = Cms_Curl::__(SMS_API . '/get/Registration/do/syncProfileToSms', array('external_id' => $external_id));
//
//                    return Cms_Render::error('Failed to generate invoice. Please try again.');
//                }
//
//            /*} else {
//                $invoiceId = $orderExist['invoice_external'];
//                $order_id = $orderExist['id'];
//            }*/
//
//            $this->view->invoiceId = $invoiceId;
//            $this->view->orderId = $order_id;
//
//            $data = array(
//                'token'    => Cms_Common::token(),
//                'order_id' => $order_id
//            );
//            $hash = Cms_Common::encrypt(json_encode($data));
//            $this->_redirect('/register/checkout-mode/i/'. $hash);
//
//            //get invoice
//            $params = array('id' => $invoiceId);
//            $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoice', $params);
//
//            $this->view->invoice = $invoice;
//
//            $totalAmount = $invoice['bill_balance'];
//
//            //get payment mode + service charge
//            $paymentmodelist = $paymentModeDB->getPaymentModeList(1);
//
//
//            foreach ($paymentmodelist as $i => $row) {
//                $paymentmodelist[$i]['fees_display_amount'] = 0;
//                $paymentmodelist[$i]['totalAmount'] = $totalAmount;
//                $paymentmodelist[$i]['cur_code'] = $invoice['cur_code'];
//
//                if ($row['fees_type'] == 1) {
//                    $paymentmodelist[$i]['fees_display_amount'] = $row['fees_amount'];
//                    //convert kalau currency bukan MYR
//                } elseif ($row['fees_type'] == 2) {
//                    $paymentmodelist[$i]['fees_display_amount'] = $totalAmount * $row['fees_percentage'] / 100;
//                }
//
//                $paymentmodelist[$i]['net_amount_with_fees'] = $totalAmount + $paymentmodelist[$i]['fees_display_amount'];
//            }
//
//            $this->view->paymentmodelist = $paymentmodelist;

        }
        else {
            $order_id = $this->_getParam('id', 0);
            $order_id = Cms_Common::encrypt($order_id, false);
            $db       = Zend_Db_Table::GetDefaultAdapter();

            $order   = $db->fetchRow($db->select()->from('order')->where('id = ?', (int) $order_id));

            if(!$order) {
                $this->_helper->flashMessenger->addMessage(array('error' => $this->view->translate("Order Not found")));
                $this->_redirect('/');
            }

            $params  = array('id' => $order['invoice_id']);
            $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoice', $params);

            $totalAmount     = $invoice['bill_balance'];
            $paymentmodelist = $paymentModeDB->getPaymentModeList(1);

            foreach ($paymentmodelist as $i => $row) {
                $paymentmodelist[$i]['fees_display_amount'] = 0;
                $paymentmodelist[$i]['totalAmount']         = $totalAmount;
                $paymentmodelist[$i]['cur_code']            = $invoice['cur_code'];

                if ($row['fees_type'] == 1) {
                    $paymentmodelist[$i]['fees_display_amount'] = $row['fees_amount'];
                }
                elseif ($row['fees_type'] == 2) {
                    $paymentmodelist[$i]['fees_display_amount'] = $totalAmount * $row['fees_percentage'] / 100;
                }

                $paymentmodelist[$i]['net_amount_with_fees'] = $totalAmount + $paymentmodelist[$i]['fees_display_amount'];
            }

            $this->view->paymentmodelist = $paymentmodelist;
            $this->view->invoice   = $invoice;
            $this->view->invoiceId = $invoice['id'];
            $this->view->order     = $order;
            $this->view->orderId   = $order_id;
            
        }

        $this->redirect('/user/dashboard/tab/exam');
    }

    public
    function paymentAction()
    {

        $erDB = new App_Model_Exam_ExamRegistration();
        $auth = Zend_Auth::getInstance();

        if ($this->getRequest()->isPost()) {

            $sp = $auth->getIdentity()->external_id;
            $formData = $this->getRequest()->getPost();

            $invoiceId = $formData['invoiceId'];
            $paymentMode = $formData['payment_mode'];
            $orderId = $formData['orderId'];

            $params = array('invoice_id' => $invoiceId,'payment_mode'=>$paymentMode,'creatorId'=>$sp);
            $updatePaymentMode = Cms_Curl::__(SMS_API . '/get/Finance/do/updateInvoicePaymentMode', $params);

            //get invoice
            $params = array('id' => $invoiceId);
            $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoice', $params);



            if($paymentMode == 6){
                //redirect to payment gateway

                $params = array(
                    'token' => Cms_Common::token('ibfimpay'),
                    'invoiceId' => $invoiceId,
                    'requestFrom' => 1,
                    'currencyId' => $invoice['currency_id'],
                    'invoiceNo' => $invoice['bill_number'],
                    'totalAmount' => Cms_Common::MigsAmount($invoice['bill_amount']),
                    'die' => 1,
                    'order_id' => $orderId,
                    'registrationItem' => 879
                );

                $urlMigs = Cms_Curl::__(URL_PAYMENT . '/migs/', $params);

                $this->view->pay_url = $urlMigs;

                header('location: ' . $urlMigs);

                //get exam_registration
                $examReg = $erDB->getDataByOrder($orderId);

                foreach($examReg as $reg) {
                    $params = array('er_id' => $reg['er_id']);
                    //send mail
                        if($reg['er_registration_type']==0){
                            $this->sendMailernotpaid($reg['er_id']);
                        }
                        else if($reg['er_registration_type']==1){
                            $this->sendMailerpaid($reg['er_id']);
                        } else {
                        return Cms_Render::error('Something went wrong #111.');
                        }
                }

            }
            elseif($formData['payment_mode'] == 5) {
                $invoices    = array();
                $invoiceNo   = array();
                $totalAmount = array();

                $invoices[]    = $invoice['id'];
                $invoiceNo[]   = $invoice['bill_number'];
                $totalAmount[] = $invoice['bill_amount'];

                $params = array(
                    'token'            => Cms_Common::token('ibfimpay'),
                    'invoiceId'        => $invoices,
                    'totalAmount'      => $totalAmount,
                    'requestFrom'      => 1, // LMS
                    'registrationItem' => $invoice['registration_item'],
                    'order_id'         => $orderId
                );

                $token = encrypt(json_encode($params));

                $this->_redirect(URL_PAYMENT .'/fpx?session='. $token);
            }
            else {

                //auto registered

                //get exam_registration
                $examReg = $erDB->getDataByOrder($orderId);

                foreach($examReg as $reg) {
                    $params = array('er_id' => $reg['er_id']);
                    $invoice = Cms_Curl::__(SMS_API . '/get/Examination/do/updateExamRegistrationStatus', $params);
                }

                $examRegstatus = $erDB->getDataByOrder($orderId);

                foreach($examRegstatus as $reg) {
                    //send mail
                        if($reg['er_registration_type']==0){
                            $this->sendMailernotpaid($reg['er_id']);
                        }
                        else if($reg['er_registration_type']==1){
                            $this->sendMailerpaid($reg['er_id']);
                        } else {
                        return Cms_Render::error('Something went wrong #111.');
                        }
                }

                $msg = 'Exam Registration Completed';

                $this->view->msg = '<div class="uk-alert">' . $msg . '</div>';

                //$this->sendMail($orderId);


                //$redirect = '/dashboard';
                $redirect = '/user/dashboard/tab/exam';
                if ($redirect != '') {
                    $this->view->msg .= Cms_Common::htmlRedirect($redirect);
                }
            }
        }


    }

    public function examinationlmsSlipAction()
    {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $er_main_id = $this->_getParam('id', 0);
        $file_name = 'AICB_Examination_Slip.pdf';
        $file_name = 'AICB_ER' . str_pad($er_main_id, 6, '0',  STR_PAD_LEFT) .'.pdf';
        
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Type: application/force-download");
        header('Content-Disposition: attachment; filename=' . urlencode($file_name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');

        // $params = array('IdStudentRegistration' => $IdStudentRegistration, 'er_id' => $er_id);
        // $slip   = Cms_Curl::__(SMS_API . '/get/Examination/do/examinationlmsSlip', $params);
        
        $params = array('er_id' => $this->_getParam('id', 0));
        $slip   = Cms_Curl::__(SMS_API . '/get/Examination/do/examSlip', $params);

        echo $slip;
        exit();

    }
    
    public function ermSlipAction()
    {
        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $er_main_id = $this->_getParam('id', 0);
        $file_name = 'AICB_Examination_Slip.pdf';
        $file_name = 'AICB_ER' . str_pad($er_main_id, 6, '0',  STR_PAD_LEFT) .'.pdf';
        
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Type: application/force-download");
        header('Content-Disposition: attachment; filename=' . urlencode($file_name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        
        // $params = array('IdStudentRegistration' => $IdStudentRegistration, 'er_id' => $er_id);
        // $slip   = Cms_Curl::__(SMS_API . '/get/Examination/do/examinationlmsSlip', $params);
        
        $params = array('er_main_id' => $this->_getParam('id', 0));
        $slip   = Cms_Curl::__(SMS_API . '/get/Examination/do/examinationSlip', $params);
        
        echo $slip;
        exit();
        
    }

    public function resultSliplmsAction() {
        $er_main_id = $this->_getParam('id', 0);
        $er_id      = $this->_getParam('er', 0);

        $auth    = Zend_Auth::getInstance()->getIdentity();
        $ermDB   = new App_Model_Exam_ExamRegistrationMain();
        $params  = array('id' => $er_main_id, 'join_studentregistration' => true);
        $er_main = $ermDB->getData($params);

        /*if (!$er_main)
        {
            return Cms_Render::error('Data not found');
        }

        if ($auth->external_id != $er_main['std_id'])
        {
            return Cms_Render::error('You are not authorized to view this data');
        }*/

        $fileName = 'AICB_Examination_Result_Slip.pdf';
        $fileName = 'AICB_RS' . str_pad($er_main_id, 6, '0',  STR_PAD_LEFT) .'.pdf';
        
        //if ($er_id)
        //{
        //    $er_main_id = 0;
        //}
        
        $params = array(
            'er_main_id' => $er_main_id,
            'er_id'      => $er_id
        );
        $slip   = Cms_Curl::__(SMS_API . '/get/Examination/do/resultSlip', $params);
        
        if (isset($slip['error']) && $slip['error'])
        {
            return Cms_Render::error($slip['message']);
        }
    
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Type: application/force-download");
        header('Content-Disposition: attachment; filename=' . urlencode($fileName));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        
        echo $slip;
        exit();
    }



    public function generateRegistrationID()
    {

        $generateLib = new Cms_System_Format();
        $myRegistrationId = $generateLib->generateStudentID(0);

        return $myRegistrationId;

    }

    //public function sendmailAction()
    public static function sendMailernotpaid($er_id)
    {
        $erDB = new App_Model_Exam_ExamRegistration();
        $examReg = $erDB->getDataEr($er_id);
        
        foreach($examReg as $a=>$ex){
                $examReg[$a]['curr'] = $invoice = $erDB->getInvoiceMain($ex['order_id']);
            }
        

        $auth = Zend_Auth::getInstance();
        //$email = $auth->getIdentity()->email;
        $email = 'ibfim.staging@gmail.com';

        $params = array(
                'type'      => 18
                );
        // email message to new user
        $msg = Cms_Curl::__(SMS_API . '/get/Registration/do/getCommunicationTemplate', $params);

        $fullname = $examReg[0]['std_fullname'];
        $program = $examReg[0]['ProgramName'];
        $pe_name = $examReg[0]['pe_name'];
        $examdate = $examReg[0]['es_date'];
        $venue = $examReg[0]['ec_name'];
        $currency = $examReg[0]['curr']['cur_symbol_prefix'];
        $amount = $examReg[0]['curr']['bill_balance'];
        $createddt = $examReg[0]['createddt'];

        $NotiExam = array(
                'firstname'   => $fullname,
                'program name'      => $program,
                'pe name'      => $pe_name,
                'exam date'      => $examdate,
                'venue'      => $venue,
                'currency'      => $currency ,
                'payment value'      => $amount,
                //'Email date'      => new Zend_Db_Expr('UTC_TIMESTAMP()')
                'Email date'      => new Zend_Date()
                );
        
        //pr($NotiExam);exit;
        /*$NotiExam = array(
                'firstname'   => 'Anuar Daniel',
                'program name'      => 'Associate Qualification in Islamic Finance',
                'exam date'      => '18-05-2016',
                'venue'      => 'Open University Malaysia Kuala Lumpur',
                'currency'      => 'RM',
                'payment value'      => '748.00',
                'Email date'      => '13-04-2016'
                );*/


        // email message to existing user
        $msg = str_replace('[Name]', $NotiExam['firstname'] , $msg);
        $msg = str_replace('[program name]', $NotiExam['program name'], $msg);
        $msg = str_replace('[pe name]', $NotiExam['pe name'], $msg);
        $msg = str_replace('[exam date]', $NotiExam['exam date'],$msg);
        $msg = str_replace('[venue]', $NotiExam['venue'],$msg);
        $msg = str_replace('[currency]', $NotiExam['currency'],$msg);
        $msg = str_replace('[payment value]', $NotiExam['payment value'],$msg);
        $msg = str_replace('[Email date]', $NotiExam['Email date'],$msg);
         //send email
        $mail = new Cms_SendMail();
        //$mail->fnSendMail('anuard@meteor.com.my', $msg['tpl_name'],$msg['tpl_content']);
        $mail->fnSendMail($email, $msg['tpl_name'],$msg['tpl_content']);
        //pr($msg);exit;
    }

 public static function sendMailerpaid($er_id)
    {
        $erDB = new App_Model_Exam_ExamRegistration();
        $examReg = $erDB->getDataEr($er_id);
        
        foreach($examReg as $a=>$ex){
                $examReg[$a]['curr'] = $invoice = $erDB->getInvoiceMain($ex['order_id']);
            }
        
            //pr($examReg);exit;
        $auth = Zend_Auth::getInstance();
        //$useremail = $auth->getIdentity()->email;
        $uemail = 'ibfim.staging@gmail.com';

        $params = array(
                'type'      => 19
                );
        // email message to new user
        $msg = Cms_Curl::__(SMS_API . '/get/Registration/do/getCommunicationTemplate', $params);

        $fullname = $examReg[0]['std_fullname'];
        $email = $uemail ;
        $es_online = $examReg[0]['es_online'];
            if($es_online==0){
                $es_online = 'Manual';
            }
            else if($es_online==1) {
                $es_online = 'Online';
            }

        $createddt = $examReg[0]['createddt'];
        $examslip = APP_URL.'/exam/examinationlms-slip/id/'. $examReg[0]['er_main_id'];
        $invoice = APP_URL.'/user/invoice/id/'.$examReg[0]['order_id'];
        //$receipt = -;
        //pr($examslip);exit;

        $NotiExam = array(
                'name'   => $fullname,
                'email address'   => $email,
                'Examination type'   => $es_online,
                'examslip'   => $examslip,
                'invoice'   => $invoice,
                //'receipt'   => $receipt,
                //'Email Date'      => new Zend_Db_Expr('UTC_TIMESTAMP()')
                'Email Date'      => new Zend_Date()

                );

        // email message to existing user
        $msg = str_replace('[Name]', $NotiExam['name'] , $msg);
        $msg = str_replace('[email address]', $NotiExam['email address'], $msg);
        $msg = str_replace('[Examination type]', $NotiExam['Examination type'], $msg);
        $msg = str_replace('[examslip]', $NotiExam['examslip'], $msg);
        $msg = str_replace('[invoice]', $NotiExam['invoice'], $msg);
        //$msg = str_replace('[receipt]', $NotiExam['receipt'], $msg);
        $msg = str_replace('[Email Date]', $NotiExam['Email Date'],$msg);
         //send email
        $mail = new Cms_SendMail();
        //$mail->fnSendMail('anuard@meteor.com.my', $msg['tpl_name'],$msg['tpl_content']);
        $mail->fnSendMail($email, $msg['tpl_name'],$msg['tpl_content']);
        //pr($msg);exit;
    }

    /*public function testAction(){
        $this->sendMailernotpaid(86);
    }*/

    public function cancelExamRegistrationAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $auth = Zend_Auth::getInstance()->getIdentity();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $id = $this->getParam('id', 0);

        $myexams = $ExamregDB->getExamregid($id);

        $exam = array();
        $exam['cancel_apply_date'] = date('Y-m-d H:i:s');
        $exam['cancel_apply_by']   = $auth->external_id;

        $ExamregDB->updateData($exam, $id, 'Apply cancel exam (LMS)');

        $this->_helper->flashMessenger->addMessage(array('success' => $this->view->translate("Application to cancel this exam registration has been submitted")));
        $this->_redirect('/exam/view-exam-detail/id/'. $id);
    }

    public function skippaymentAction()
    {
        //FOR DEVELOPMENT USE ONLY
        $id = $this->getParam('id');
        $SmsExamDB = new App_Model_SmsExam();
        $migsid = $SmsExamDB->getTransMigs($id);

        $order_id =  $migsid['mt_external_id'];
        //pr($migsid);exit;

        $order = array();
        // $order['status']  = 'ACTIVE';

        $ExamregDB = new App_Model_SmsExam();
        $myorder = $ExamregDB->updateDataOrder($order, $order_id);
        $this->_redirect('/user/purchase-history');
        //pr($migsid);exit;

    }

    public function certificateOfAttendanceAction() {
        $er_id = $this->_getParam('id', 0);
        $params   = array('er_id' => $er_id);
        $response = Cms_Curl::__(SMS_API.'/get/Examination/do/certificateOfAttendance', $params);

        if(isset($response['msg'])) {
            return Cms_Render::error($response['msg']);
        }

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    
        $file_name = 'AICB_CA' . str_pad($er_id, 6, '0',  STR_PAD_LEFT) .'.pdf';

        //$file_name = 'certificate_of_attendance.pdf';
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Type: application/force-download");
        header('Content-Disposition: attachment; filename=' . urlencode($file_name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');

        echo $response;
        exit;
    }
    
    public function registerExamAction(){
    	
    	$auth          = Zend_Auth::getInstance();
    	
    	$erDB          = new App_Model_Exam_ExamRegistration();
        $ermDB         = new App_Model_Exam_ExamRegistrationMain();
        $ercDb = new App_Model_Exam_ExamRegistrationCycle();
        $studentRegDb = new App_Model_Qualification_StudentRegistration();
    	
    	if ($this->getRequest()->isPost()) {

            if(isset($auth->getIdentity()->login_as)){//if admin
                $auth->getIdentity()->external_id = 1;
                $createby = $auth->getIdentity()->external_id;//$createby  = 1
                $ordercreateby =  $auth->getIdentity()->login_as;
                $sp = $auth->getIdentity()->sp_id;
            }else{
                $createby = $auth->getIdentity()->external_id;
                $ordercreateby = $auth->getIdentity()->id;
                $sp = $createby;
            }
    	
            $formData = $this->getRequest()->getPost();
            
            if(count($formData['idSubject'])>0){
            	
            	$cur_cycle =0;
	            $cur_sitting = 0;   
	            $erc_id = 0;
	            		
            	if($formData['IdProgram']==52){ //PKMC
            		
            		$smsStudentRegistrationDB = new App_Model_StudentRegistration();
        			$studentRegDetl = $smsStudentRegistrationDB->getRegistrationLandscapeDetails($formData['IdStudentRegistration']);
        
	            	//before submit check current cycle
	            	//$er_cycle = $ercDb->getExamCycle($formData['IdStudentRegistration']);  
	            	if(isset($formData['erc_id']) && $formData['erc_id']!=0){
	            		
	            		$cycle_info = $ercDb->getData($formData['erc_id']);
	            		
	            		//check no of exam sitting
	            		$exam_sitting = $ermDB->getCountExamSitting($formData['IdStudentRegistration'],$formData['erc_id']);
	            		
	            		if(count($exam_sitting)==$studentRegDetl['MaxExamSitting']){
	            			
	            			//add new cycle
	            			$cycleData['IdStudentRegistration']=$formData['IdStudentRegistration'];
		            		$cycleData['cycle_no']=2;
		            		$cycleData['cycle_status']=0;
		            		$cycleData['first_exam_sitting']=$formData['est_id'];
		            		$cycleData['erc_createddt']=date('Y-m-d H:i:s');
		            		$cycleData['erc_createdby']=$createby;
		            		$erc_id = $ercDb->addData($cycleData);
		            		
		            		//update studentreistraion
		            		$studentRegDb->updateData(array('erc_id'=>$erc_id),$formData['IdStudentRegistration']);
		            		
		            		//update current cycle to completed
		            		$ercDb->updateData(array('cycle_status'=>1),$where="erc_id=".$formData['erc_id']);
		            		
		            		//set sitting to 1 for new cycle
		            		$cur_cycle =2;
		            		$cur_sitting = 1;
		            		
	            		}else{
	            			
	            			$below_max = ($studentRegDetl['MaxExamSitting']-1);
	            			if(count($exam_sitting)==$below_max){
	            				//update current cycle to completed
		            			$ercDb->updateData(array('cycle_status'=>1),$where="erc_id=".$formData['erc_id']);
	            			}
	            			//maintain cycle
	            			$erc_id = $formData['erc_id'];
	            			
	            			//just add new sitting for cyrrent cycle
	            			$cur_sitting = count($exam_sitting)+1;
	            		}
	            		
	            		
	            	}else{
	            		
	            		//1: add exam cycle
	            		$cycleData['IdStudentRegistration']=$formData['IdStudentRegistration'];
	            		$cycleData['cycle_no']=1;
	            		$cycleData['cycle_status']=0;
	            		$cycleData['first_exam_sitting']=$formData['est_id'];
	            		$cycleData['erc_createddt']=date('Y-m-d H:i:s');
	            		$cycleData['erc_createdby']=$createby;
	            		$erc_id = $ercDb->addData($cycleData);
	            		
	            		//update studentreistraion
		            	$studentRegDb->updateData(array('erc_id'=>$erc_id),$formData['IdStudentRegistration']);
		            		
	            		
	            		$cur_cycle =1;
	            		$cur_sitting = 1; 
	            		
	            	}
            			            	
            	}//end pkmc
            	
            	//2:add main exam regiatration and exam sitting
            	$er_main = array(
                    'IdStudentRegistration' => $formData['IdStudentRegistration'],
                    'IdProgram'             => $formData['IdProgram'],
                    'ber_id'                => 0,
                    'created_at'            => date('Y-m-d H:i:s'),
                    'created_by'       		=> $createby,
            		'erc_id'			    =>$erc_id,
            		'exam_sitting_no'		=>$cur_sitting,
            		'exam_sitting_status'	=>1,
            		'exam_sitting_id'		=>$formData['est_id'],
            	
                );
                $er_main_id = $ermDB->addData($er_main);
            	
                
                //3: add student reg subject
            	foreach($formData['idSubject'] as $subject_id=>$checbox_val){
            		
            		if($checbox_val==1){
	            		$schedule_arr = explode('-',$formData['schedule'][$subject_id]); //schedule_id-es_id-exam_tagging_slot-esd_id
	            		
	            		$exam_registration = array(
				                        'registrationId'     => Cms_Curl::__(SMS_API . '/get/Examination/do/generateExaminationId', array()),
				                        'IdProgram'          => $formData['IdProgram'],
				                        'er_main_id'         => $er_main_id,
				                        'landscape_id'       => $formData['IdLandscape'],
				                        'examsetup_id'       => $schedule_arr[1],
				                        'createddt'          => new Zend_Db_Expr('UTC_TIMESTAMP()'),
				                        'createdby'          => $auth->getIdentity()->id,
				                        'student_id'         => $formData['IdStudentRegistration'], //$IdStudentRegistration,
				                        'examsetupdetail_id' => $schedule_arr[2],
				                        'examschedule_id'    => $schedule_arr[0],
				                        'examcenter_id'      => $formData['examcenter_id'][$subject_id],
				                        'examtaggingslot_id' => $schedule_arr[3],
				                        'intakeprogram_id'   => 0,
				                        'er_registration_type'=> 1, //auto registered; add for aicb 300118
				                        'fs_id'              => '',
				                        'pass'               => null,
				                        'grade_name'         => null,
				                        'exam_status'        => null,
				                        'total_marks'        => null,
				                        'createddt'          => date('Y-m-d H:i:s'),
				                        'createdby'          => $createby,
	            						'IdStudentRegSubjects' => $subject_id
	
	                    );
						
	                   $erId = $erDB->addData($exam_registration);
	                   
            		}//end check_val
            	}//end forach
            }

             $this->_helper->flashMessenger->addMessage(array('success' => $this->view->translate("Exam registration successfull!!")));
             $this->redirect('/exam/index');
    	}//end post
    }
    
    public function examRegistrationNormalAction(){
    	
     	Cms_Hooks::push('user.sidebar', self::userSidebar('exam')); 

        

    	if(isset($this->auth->getIdentity()->login_as)){
            $sp = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        }else{
            $sp = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }
        
        $smsExamDB = new App_Model_SmsExam();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $currDb = new App_Model_Curriculum();
        
        $examList = $smsExamDB->getAvailableExamByUser($sp);
        $this->view->exam = $examList;
        
     	$smsStudentRegistrationDB = new App_Model_StudentRegistration();
        $studentRegDetl = $smsStudentRegistrationDB->getDataStudentRegistration($sp);
        $this->view->studentRegDetl = $studentRegDetl;

        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjectsDetail($sp);

    	if(count($StudentSubjectsDetail)>0){
        	foreach($StudentSubjectsDetail as $index=>$regsubject){
        		
        		 $schedule = $smsStudentRegistrationDB->getScheduleExam($regsubject['IdProgram'],$regsubject['IdSubject']);
        		
        		//get exam center
        		if(count($StudentSubjectsDetail)>0){
	        		foreach($schedule as $i=>$sc){
	        			//get center by slot id
	        			$exam_center = $smsStudentRegistrationDB->getExamCenter($sc['sl_id']);
	        			$schedule[$i]['center']=$exam_center;
	        		}
        		}
        		
        		$StudentSubjectsDetail[$index]['schedule']=$schedule;
        		
        		//get exam registration details
        	}
        }
         $this->view->StudentSubjectsDetail = $StudentSubjectsDetail;
       
    }
    
    
	public function examRegistrationAction(){
    

    	
     	Cms_Hooks::push('user.sidebar', self::userSidebar('exam')); 
        $i = $this->_getParam('i');

        $this->view->i = $i;
        
        $i = json_decode(encrypt($i, false), true);

    	if(isset($this->auth->getIdentity()->login_as)){
            $sp = isset($this->auth->getIdentity()->sp_id) ? $this->auth->getIdentity()->sp_id : 0;
        }else{
            $sp = isset($this->auth->getIdentity()->external_id) ? $this->auth->getIdentity()->external_id : 0;
        }



        $program_id = $i['idProgram']; 
        $idStudentRegistration = $i['idStudentRegistration'];
           
        $smsExamDB = new App_Model_SmsExam();

        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $examRegMainDB = new App_Model_Exam_ExamRegistrationMain();
        $currDb = new App_Model_Curriculum();
        $examCycleDb = new App_Model_Exam_ExamRegistrationCycle();
        $prerequisiteDb = new App_Model_Exam_ProgrammeExamPrerequisite();
        $attendanceDb = new App_Model_Qualification_CourseGroupAttendance();
        $examSitingDb = new App_Model_Exam_ExamSitting();    
        $landscapeSubjectDb = new App_Model_General_LandscapeSubject();
        
        //set default
        $exam_open=true;  
        $est_id = ''; //exam sitting id
        $exam_sitting = array();
      
        if(isset($program_id) && $program_id==52){
	        	
	        //get open exam sitting registration        	    	
	        $exam_sitting = $examSitingDb->getOpenExamSitting(); 
	        if($exam_sitting){     
	        	$est_id=$exam_sitting['est_id']; 
	        }else{
	        	$exam_open=false; 
	        }
	        	        
        } 
    
        //get candidate registraion info
     	$smsStudentRegistrationDB = new App_Model_StudentRegistration();
        $studentRegDetl = $smsStudentRegistrationDB->getRegistrationLandscapeDetails($idStudentRegistration);
        $this->view->studentRegDetl = $studentRegDetl;

        //get registered subject
        $StudentRegistrationSubjectDB = new App_Model_Qualification_StudentRegistrationSubject();
        $StudentSubjectsDetail = $StudentRegistrationSubjectDB->getDatabyStudentId($idStudentRegistration);

    	if(count($StudentSubjectsDetail)>0){
        	foreach($StudentSubjectsDetail as $index=>$regsubject){
        		
        		         		 
        		 //check if already registered and passed or (pass but nullified)
        		 $isPass = $ExamregDB->isPass($regsubject['IdStudentRegSubjects']);
        		
        		 //check if already registered for current exam sitting
        		 $isRegistered = $ExamregDB->isRegistered($regsubject['IdStudentRegSubjects'],$est_id);
        		 
        		 if($isPass || $isRegistered){
        		 		unset($StudentSubjectsDetail[$index]);
        		 		//$StudentSubjectsDetail[$index]['er_registration_type']=$isExam['er_registration_type'];
        		 }else{
        		 	
        		 	$allow_register = true;
        		 	
        		 	 //check landscape subject
	        		 $landscape_subject_info = $landscapeSubjectDb->getLandscapeSubjectInfo($studentRegDetl['IdLandscape'],$studentRegDetl['IdMajoringPathway'],$regsubject['IdSubject']);
	        		 
	        		 if($landscape_subject_info && $landscape_subject_info['initial_exam_paper']==1){
	        		 	$StudentSubjectsDetail[$index]['initial']=1;
	        		 }else{
	        		 	$StudentSubjectsDetail[$index]['initial']=0;
	        		 }
        	
        		 	        		 	
        		 	//skip check prerequisite just direct cari attendance
        		 	if($program_id==52){
        		 		$StudentSubjectsDetail[$index]['prerequisite']=array('type'=>'attendance','status'=>'Workshop');
        		 		if($regsubject['IdCourseTaggingGroup']>0){
	        				$attendance = $attendanceDb->getAttendanceStatus($regsubject['IdCourseTaggingGroup'],$idStudentRegistration);
        		 			if((!$attendance) || ($attendance && $attendance['attendance_status']!=395)){	        					
	        					$allow_register = false;
	        				}
        		 		}	        				
        		 	}
        		 	//end attenndace
        		 	
            		
	        		 //get schedule exam
	        		$schedule = $smsStudentRegistrationDB->getScheduleExam($regsubject['IdProgram'],$regsubject['IdSubject'],$exam_sitting);
	        		
	        		//get exam center
	        		if(count($StudentSubjectsDetail)>0){
	        			if(count($schedule)>0){
			        		foreach($schedule as $i=>$sc){
			        			//get center by slot id
			        			$exam_center = $smsStudentRegistrationDB->getExamCenter($sc['sl_id']);
			        			$schedule[$i]['center']=$exam_center;
			        		}
	        			}else{
	        				$allow_register = false;
	        			}
	        		}
	        		
	        		
	        		//exemption
	        		if($regsubject["ExemptionStatusSubject"] == 1){
	        			$allow_register = false;
	        		}
	        		
	        		$StudentSubjectsDetail[$index]['allow_register']=$allow_register;
	        		$StudentSubjectsDetail[$index]['schedule']=$schedule;
	        		
        		 }//end else
        		
        		//get exam registration details
        	}
        }
        //pr($StudentSubjectsDetail);
        
        
                
        
        //================= PKMC ONLY =================
        
		if(count($StudentSubjectsDetail)>0){			
	     		     	
			if(isset($program_id) && $program_id==52){
	        		        	        	
	        	if($exam_open==true){        
	        		  
	
	        		//current_open exam sitting
	        		$est_id=$exam_sitting['est_id'];
	        		
		        	//1: get candidate exam registration cycle
		        	$candidate_cycle_list = $examCycleDb->getListExamCycle($idStudentRegistration);
		        	$total_exam_cycle = count($candidate_cycle_list);
		        	        	
		        	if($total_exam_cycle==0 || $studentRegDetl['erc_id']==0){
		        		
		        		$this->view->first_time_registration = true;
		        		$this->view->no_of_cycle  = 1;
		        		$this->view->current_erc_id  = 0;
		        		//this is candidate first time exams registration
		        		
		        	}else if(count($candidate_cycle_list)>0){
		        		
		        		$this->view->first_time_registration = false;
		        		
		        		//1:dah pernah register
		        		$no_of_cycle  = count($candidate_cycle_list);
		        		$current_erc_id = $candidate_cycle_list[0]['erc_id'];    
		        				        		
		        		//2:get no of exam sitting
		        		$candidate_exam_sitting_list = $examRegMainDB->getCountExamSitting($idStudentRegistration,$current_erc_id);     
						$this->view->sitting = count($candidate_exam_sitting_list);
						
						//check if complete cycle 1		        		        		
						if($no_of_cycle==1 && ($candidate_cycle_list[0]['cycle_status']==1 || count($candidate_exam_sitting_list)==$studentRegDetl['MaxExamSitting'])){
							//complete cycle 1
							//kene sit balik semua exam and nullified kan semua markah
							$this->updExamStatus($idStudentRegistration);
		        		}
		        	
		        		if($no_of_cycle==2 && count($candidate_exam_sitting_list)==$studentRegDetl['MaxExamSitting']){
		        			//not eligible to register exam
		        			echo '======nor eligble==========';
		        		}
		        		
		        		
		        		echo 'cycle no'.$this->view->no_of_cycle = $no_of_cycle;
		        		echo '<br>cycle id'.$this->view->current_erc_id = $current_erc_id;
		        	
		        	}//end cycle
	        		
	        	}//end exam sitting ada
	        	
	        	$this->view->exam_sitting = $exam_sitting;
	        	        	
	        	
	        	//if fisrt sem eq cur sem => this is new student
	    		//if no registered any semester status => this is new student set first semester as current semester
	    		
	        	// get subject to register list based on landscape 	 
	        	
	        	//based on subject list //check dah  lulus prev sitting atau dah pernah register	        	
	        }
	     	
	    }//end subject count       
        
        //================= PKMC ONLY =================
        
        
        //is completed
        if(count($StudentSubjectsDetail)>0){
        	//ada remaining courses yg belum completed
        }
        
        $this->view->exam_open=$exam_open;  
        $this->view->StudentSubjectsDetail = $StudentSubjectsDetail;
       
    }
    
    
    
    public function indexAction(){
    	
    	Cms_Hooks::push('user.sidebar', self::userSidebar('exam')); 
    	 
    	$auth = Zend_Auth::getInstance()->getIdentity();
    	
    	$sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }
        
        $examRegMainDB = new App_Model_Exam_ExamRegistrationMain();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $examSitingDb = new App_Model_Exam_ExamSitting();
        $examCycleDb = new App_Model_Exam_ExamRegistrationCycle();

        if ($sp) {
        	
        	$studentRegDb = new App_Model_Qualification_StudentRegistration();
            $qualification_registration = $studentRegDb->getDatabyStudentId($sp);
            
            foreach($qualification_registration as $index=>$program){
            	            	
            	$qualification_registration[$index]['exam_open']=true;
            	
            	if($program['IdProgram']==52){//PKMC 
            		
            		//get open exam sitting registration
		        	$exam_sitting = $examSitingDb->getOpenExamSitting(); 
		        	if(!$exam_sitting){
		        		$qualification_registration[$index]['exam_open']=false;
		        	}
		        
            	}
            	
            	$myexams = $ExamregDB->getCurrentExamRegByProgram($program['IdStudentRegistration'],$program['IdProgram']);
            	
            	foreach($myexams as $n=>$ex){
            		$myexams[$n]['examStatus'] = $ExamregDB->courseStatus($ex['er_registration_type']);
            	}
            	$qualification_registration[$index]['exams']=$myexams;            	
            }
            
            $this->view->qualification_registration = $qualification_registration;
        }
    }
    
    
	public function examRegistrationHistoryAction(){
    	
    	Cms_Hooks::push('user.sidebar', self::userSidebar('exam')); 
    	 
    	$auth = Zend_Auth::getInstance()->getIdentity();
    	
    	$sp = isset($auth->external_id) ? $auth->external_id : 0;//echo "<pre>".pr($this->auth->getIdentity());

        $this->view->auth = $this->auth->getIdentity();

        if (isset($this->auth->getIdentity()->login_as)) {
            $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
        } else {
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        }
        
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $examSitingDb = new App_Model_Exam_ExamSitting();
        $examCycleDb = new App_Model_Exam_ExamRegistrationCycle();
         $examRegMainDB = new App_Model_Exam_ExamRegistrationMain();
        
        $form = new App_Form_SearchRegisterExam();
        $this->view->form = $form;
        
        if ($sp) {
        	
        	$studentRegDb = new App_Model_Qualification_StudentRegistration();
            $qualification_registration = $studentRegDb->getDatabyStudentId($sp);
            
            foreach($qualification_registration as $index=>$program){   

            		$candidate_cycle = $examCycleDb->getListExamCycle($program['IdStudentRegistration']);
            		
            		if(count($candidate_cycle)>0){
            			foreach($candidate_cycle as $key=>$cycle){
            				//get sitting
            				$exam_sitting = $examRegMainDB->getMyExamRegistration($program['IdStudentRegistration'],$cycle['erc_id']);
            				$candidate_cycle[$key]['sitting']=$exam_sitting;
            			}
            		}
            		$this->view->candidate_cycle = $candidate_cycle;
            	
            	$myexams = $ExamregDB->getExamRegistrationHistory($program['IdStudentRegistration'],$program['IdProgram']);
            	
            	foreach($myexams as $n=>$ex){
            		$myexams[$n]['examStatus'] = $ExamregDB->courseStatus($ex['er_registration_type']);
            	}
            	$qualification_registration[$index]['exams']=$myexams;            	
            }
            
            $this->view->qualification_registration = $qualification_registration;
            //pr($qualification_registration);
        }
    }
    
    public function updExamStatus($idStudentRegistration){
    	
    	//$idStudentRegistration = $this->getParam('id',);
    	
    	$ExamregDB = new App_Model_Exam_ExamRegistration();
    	$ExamregDB->updateExamStatus($idStudentRegistration);
            	
    }
 	

}
     
