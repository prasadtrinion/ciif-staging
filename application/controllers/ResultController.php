<?php

class ResultController extends Zend_Controller_Action
{
    public function indexAction()
    {

        $ec_id = $this->_getParam('ec_id', 0);

        $this->view->title = "View Registration Details";

        $form = new App_Form_Candidate();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {

            $formdata = $this->getRequest()->getPost();

            if ($form->isValid($formdata)) {
                $form->populate($formdata);

                $icno = $formdata['icno'];

                $examRegistrationDb = new App_Model_Exam_ExamRegistration();

                //get exam registration
                $examRegistration = $examRegistrationDb->getRegistrationByStudent($icno);

                if ($examRegistration) {
                    foreach ($examRegistration as $a => $exam) {

                        $regStatus = NULL;
                        if($exam['pass'] == 1){
                            $regStatus = 'Pass';
                        }else if($exam['pass'] == '0'){
                            $regStatus = 'Failed';
                        }else if($exam['pass'] == NULL){
                            $regStatus = 'No Record';
                        }else {

                            if($exam['attendance_status_desc'] == NULL ){
                                $regStatus = $examRegistrationDb->courseStatus($exam['er_registration_type']);
                            }else{
                                $regStatus = $exam['attendance_status_desc'];
                            }
                        }
                        $examRegistration[$a]['registration_status'] = $regStatus;  

                    }
                }

                $this->view->examregistration = $examRegistration;
                $this->view->ic = $icno;

            }
        }
    }

    public
    function registrationStatus($id)
    {

        $registrationStatus = array(
            0 => 'Pre-Registered',
            1 => 'Registered',


        );

    }
}

?>