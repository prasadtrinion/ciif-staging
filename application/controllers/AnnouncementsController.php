<?php

class AnnouncementsController extends Zend_Controller_Action
{
    protected $courseDb;
    protected $contentblockDb;
    protected $blockDataDb;
    protected $enrolDb;
    protected $currDb;
    protected $sidebar  = 1;
    protected $currCoursesDb;

    public function init()
    {
        $this->courseDb = new App_Model_Courses();
        $this->coursesDb = new App_Model_Courses();
        $this->contentBlockDb = new App_Model_ContentBlock();
        $this->blockDataDb = new App_Model_ContentBlockData();
        $this->enrolDb = new App_Model_Enrol();
        $this->currDb = new App_Model_Curriculum();
        $this->currCoursesDb = new App_Model_CurriculumCourses();
        $this->announcementsDb = new App_Model_Announcements();

        $this->groupingDb = new App_Model_UserGroupMapping();
        $this->groupDataDb = new App_Model_UserGroupData();
    }

	public function viewAction()
    {
		//have you enrolled in this course before
        $auth = Zend_Auth::getInstance();
        $user_id = $auth->getIdentity()->id;

		//get id
        $id = $this->_getParam('course');
		
        $course = $this->coursesDb->fetchRow(array('id = ?' => $id));
		$this->view->course = $course;
		
		$announcements = $this->announcementsDb->getCourseAnnouncement(array('course_id = ?' => $id, 'visibility = ?' => '1'));

        // get current user group for this course
        $user_group = $this->groupDataDb->getUserGroup(Zend_Auth::getInstance()->getIdentity()->id, $id);

        $total_announcements = 0;
        foreach ($announcements as $index => $ann)
        {           
            // check content grouping
            if (!$this->groupingDb->checkContentPermission($ann['id'], $id, "announcements")) // the content is tagged to a group
            {
                // content grouping
                $group = $this->groupingDb->getContentGroup($ann['id'], $id, $user_group['group_id'], 'announcements'); // check against the learner's group

                if (empty($group)) {    
                    // it is tagged to other group. not the learner. unset the var so it will not be displayed
                    unset($announcements[$index]);
                    continue;
                }

                // tag the content to the group
                $announcements[$index]["specific_group"] = $group["group_id"];
                $announcements[$index]["specific_group_name"] = $group['group_name'];
            }
            else
            {
                // it is tagged for all. no checking needed. moving on
            }
            $total_announcements++;
        }

        $announcements = Cms_Paginator::arrays($announcements);

		$this->view->announcements = $announcements;
        $this->view->total_announcements = $total_announcements;
	}
}
