<?php

class RegisterController extends Zend_Controller_Action
{
    protected $courseDb;
    protected $contentblockDb;
    protected $blockDataDb;
    protected $enrolDb;
    protected $currDb;
    protected $sidebar  = 1;
    protected $currCoursesDb;
    protected $paymentDb;
    protected $session;
    protected $orderDb;
    protected $orderGroupDb;
    protected $orderGroupUserDb;

    public function init()
    {
        $this->courseDb = new App_Model_Courses();
        $this->contentBlockDb = new App_Model_ContentBlock();
        $this->blockDataDb = new App_Model_ContentBlockData();
        $this->enrolDb = new App_Model_Enrol();
        $this->currDb = new App_Model_Curriculum();
        $this->currCoursesDb = new App_Model_CurriculumCourses();
        $this->paymentDb    = new App_Model_PaymentHistory();
        $this->orderDb = new App_Model_Order();
        $this->orderGroupDb = new App_Model_OrderGroup();
        $this->orderGroupUserDb = new App_Model_OrderGroupUser();

        $this->session = new Zend_Session_Namespace('register');

        if ( !isset($this->session->guests) )
        {
            $this->session->guests = array();
        }

        //Enrolment
        Cms_Events::attach('payment.success', array($this, 'processEnrollment'));
    }

    public function indexAction()
    {
        $this->redirect('/courses');
    }

    public function paymentAction()
    {
        $error = 0;
        $msg = '';
        $redirect = '';

        /*
         * Testing Purpose - auto paid & auto enrol
         * http://ibfimlms.local/register/payment/?mtid=46
         * mtid=from SMS - table transaction_migs
         */

        $ol_id = $this->_getParam('ol_id', 0);
        $mtid  = $this->_getParam('mtid', 0);
        $params         = array('ol_id' => $ol_id, 'mtid' => $mtid);
        $payment_status = Cms_Curl::__(SMS_API . '/get/Payment/do/getPaymentStatus', $params);

        $type     = $payment_status['type'];
        $migs_id  = $mtid = ($type == 'MIGS' ? $payment_status['migs_transaction_id'] : 0);
        $fpx_id   = ($type == 'FPX' ? $payment_status['fpx_transaction_id'] : 0);
        $redirect = '';

        // $mtid = $this->getParam('mtid');
        $fid = $this->getParam('fid');
        
        if (isset($payment_status['pending']) && $payment_status['pending'] == 1)
        {
            $msg = 'Your payment status is pending, it will be updated automatically shortly. Please do <b>NOT</b> delete your order nor make any attempt for another payment.';
            $this->view->msg = '<div class="uk-alert uk-alert-warning">'.$msg.'</div>';
            return;
        }

        if($payment_status['success'] != 1) {
            $this->view->msg = '<div class="uk-alert uk-alert-danger">'.$payment_status['msg'].'</div>';
            return;
        }
        
        // $this->processEnrollment($payment_status);
        // $get = Cms_Curl::__(URL_PAYMENT.'/migs/api/call/migstrans',array('mtid'=>$mtid));
        Cms_Events::trigger('payment.success', $this, $payment_status); // go to processEnrollment()

        /*
        if ( $mtid == '' && $fid == '' )
        {
            $error = 1;
            $msg = 'Invalid Parameters';
        }

        $type = $mtid != '' && $fid == '' ? 'MIGS' : 'FPX';

        if ( $type == 'MIGS' )
        {

            if (empty($mtid))
            {
                $error = 1;
                $msg = 'Invalid MTID';
            }
            else
            {
                $get = Cms_Curl::__(URL_PAYMENT.'/migs/api/call/migstrans',array('mtid'=>$mtid));

                if (is_array($get)) {
                    switch ($get['data']['mt_status']) {
                        case 'ENTRY':

                                            $msg = 'Please pay first. You will redirect to your Purchase History';
                                            $redirect = '/user/purchase-history';
                        break;

                            $msg = 'Please pay first. You will be redirected to your Purchase History.';
                            break;

                        case 'FAIL':
                            $msg = 'Payment was not successful. #' . $get['data']['mt_rep_txn_response_code_desc'];
                            $error = 1;
                            break;
                        case 'SUCCESS':
                            $msg = 'Payment successful! Thank you for your purchase. You will get forwarded back to your dashboard.';
                            $redirect = '/dashboard';

                            Cms_Events::trigger('payment.success', $this, $get);
                            break;
                    }
                }
            }
        }

        if ( $error )
        {
            $this->view->msg = '<div class="uk-alert uk-alert-danger">'.$msg.'</div>';
            return;
        }
        */

        $this->view->msg = '<div class="uk-alert">Payment successful! Thank you for your purchase. You will get forwarded back to your dashboard.</div>';

        if(isset($payment_status['redirect']) && $payment_status['redirect']) {
            $redirect = $payment_status['redirect'];
        }

        if ( $redirect != '' )
        {
            $this->view->msg .= Cms_Common::htmlRedirect($redirect);
        }
    }

    public function enrollMultipleAction()
    {
        $auth       = Zend_Auth::getInstance();
        $identity   = $auth->getIdentity();
        $birth_date = $identity->birthdate;
        $is_valid   = Cms_Common::validateDate($birth_date);

        if (!$is_valid)
        {
            Cms_Common::notify('error', 'Please update your Date of Birth before continuing to enrol');
            $this->redirect('/user/profile');
        }

        $dataDb = new Admin_Model_DbTable_ExternalData();
        $learningmodeTypes = $dataDb->getData('LearningMode');
        $learningmodeTypes = array_column($learningmodeTypes, 'value', 'code');

        $error = false;
        $data = $this->getParam('i');
        $regtype = $cid = $course_id = '';

        if ( !empty($data) )
        {
            $data = Cms_Common::encrypt($data, false);

            if ( Cms_Common::isJson($data) )
            {
                $data = json_decode( $data, true);

                $regtype = $data['regtype'];
                $cid = isset($data['cid']) ? $data['cid'] : '';
                $course_id = isset($data['course_id']) ? $data['course_id'] : '';

                $cur = $this->courseDb->fetchRow(array("id = ?" => $course_id));

                $this->view->course_id = $cur['code_safe'];

                $time = $data['time'];
            }
            else
            {
                $error = true;
            }
        }

        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            $regtype = $formData['regtype'];
            $cid = isset($formData['cid']) ? $formData['cid'] : '';
            $course_id = isset($formData['course_id']) ? $formData['course_id'] : '';
        }

//        pr($data);
        //check
        if ( $regtype == '' ) $error = true;
        if ( $regtype == 'curriculum' && $cid == '' ) $error = true;
        if ( $regtype == 'course' && $course_id == '' ) $error = true;


        if ( $regtype == 'course' )
        {//echo $course_id;
            $course = $this->courseDb->getCourse(array("a.id = ?" => $course_id));

            if ( empty($course)  )
            {
                return Cms_Render::error('Course doesn\'t exist.');
            }
        }

        if ( $regtype == 'curriculum' )
        {
            $cur = $this->currDb->fetchRow(array("id = ?" => $cid));

            if ( empty($cur)  )
            {
                return Cms_Render::error(Cms_Options::__('label_curriculum').' doesn\'t exist.');
            }

            $post = Cms_Events::trigger('enroll.curriculum.check', $this, array('cid'=>$cid));

            if ( !empty($post) && count($post) > 0 )
            {
                if ( $post[0]['fail'] == 1 )
                {
                    return Cms_Render::error($post[0]['error']);
                }
            }

            $cur = $cur->toArray();
        }


//        echo $regtype;exit;
        //check again
        if ( !Zend_Auth::getInstance()->hasIdentity() )
        {
            return Cms_Render::error('You must be logged in to enroll in a course. <a href="'.$this->view->baseUrl().'/index/register/course/'.$course['id'].'">Login</a> or <a href="'.$this->view->baseUrl().'/register">Sign Up</a>');
        }

        if ( $error == true )
        {
            $errorMsg = true;
        }

        /***  disabled for fpx ***
        if ( !empty($time) && $time+900 < time() )
        {
        return Cms_Render::error('Session Ended. Please try again.');
        }
        /*** end disable for fpx ***/

        //canenrol
        $canenrol = true;

        //have you enrolled in this course before
        $auth = Zend_Auth::getInstance();
        $user_id = $auth->getIdentity()->id;

        $check_id = $regtype == 'course' ? $course_id : $cid;
        $check = $this->enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' => $regtype, ($regtype == 'course'?'course_id':'curriculum_id').' = ?' => $check_id));

        if ( !empty($check) )
        {
            $canenrol = false;
            $errorMsg = 'You already enrolled in this course';
        }

        //order
        $check2 = $this->orderDb->getOrder(array('status != ?' => 'CANCELLED', 'user_id = ?' => $user_id, 'regtype = ?' => $regtype, 'item_id = ?' => $check_id));


        if ( !empty($check2) ) {
            if ($check2['status'] == 'ACTIVE') {
                $canenrol = false;
                $errorMsg = 'You already enrolled in this ' . ($regtype == 'course' ? 'course' : Cms_Options::__('label_curriculum'));
            } else if ($check2['status'] == 'PENDING') {

                $canenrol = false;
                $errorMsg = 'You already enrolled in this ' . ($regtype == 'course' ? 'course' : Cms_Options::__('label_curriculum') ). ' and is pending approval/payment. <a class="uk-button uk-button-mini" href="'.$this->view->baseUrl().'/user/purchase-history">View</a>';
            }
        }


        //are you not a student
        if ( $auth->getIdentity()->role != 'student' )
        {
            $canenrol = false;
            $errorMsg = 'Only students can enroll in courses.';
        }

        //views
        $this->view->regtype = $regtype;
        $this->view->cid = $cid;

        if ( $regtype == 'course')
        {
            $this->view->course  = $course;
        }
        else
        {
            //courses
            $getcourses = $this->currCoursesDb->getData($cid);

            $this->view->courses = $getcourses;
            $this->view->cur = $cur;
        }

        //error trap
        if ( $canenrol == false || $error == true )
        {
            return Cms_Render::error($errorMsg);
        }

        //cart
        $cart = array();

        if ( $regtype == 'curriculum' )
        {

            $curcoursesDB = new App_Model_CurriculumCourses();
            $courses = $curcoursesDB->getData($cur['id']);

            $datacourses = implode(',', array_column($courses, 'external_id'));

            $data = array(
                'IdProgram'         => $cur['external_id'],
                'IdProgramScheme'   => 0,
                'StudentCategory'   => $auth->getIdentity()->nationality == 'MY' ? 579 : 580,
                'IdSubject'         => 0,
                'courses'           => $datacourses
            );

            $cart[] = array(

                'name'          => $cur['name'],
                'code'          => $cur['code'],
                'description'   => $cur['description'],
                'external_id'   => $cur['external_id'],
                'price'         => 0,
                'link'          => $this->view->baseUrl().'/courses/curriculum/cid/'.$cur['id'],
                'regtype'       => 'curriculum',
                'id'            => $cur['id'],
                'data'          => $data
            );

        }

        if ( $regtype == 'course' )
        {
            $cur = $this->currDb->getCurriculum(array('id = ?' => $cid));

            if ( empty($cur) )
            {
                return Cms_Render::error('Invalid Cur ID');
            }

            $data = array(
                'IdProgram'         => $cur['external_id'],
                'IdProgramScheme'   => 0,
                'StudentCategory'   => $auth->getIdentity()->nationality == 'MY' ? 579 : 580,
                'IdSubject'         => $course['external_id'],
                'courses'            => $course['external_id']
            );

            $cart[] = array(
                'name'          => $course['title'],
                'code'          => $course['code'],
                'description'   => $course['description'],
                'external_id'   => $course['external_id'],
                'price'         => 0,
                'link'          => $this->view->baseUrl().'/courses/'.$course['code_safe'].'/pid/'.$course['curriculum_id'],
                'curriculum_id' => $course['curriculum_id'],
                'id'            => $course['id'],
                'regtype'       => 'course',
                'data'          => $data
            );

        }

        //friends
        $this->view->guests = $this->session->guests;

        //last check
        $this->view->cart = $cart;
        if ( empty($cart) )
        {
            return Cms_Render::error('Incomplete data for enrolment.');
        }
        else
        {
            $this->session->cart = $cart;
        }
    }


    public function enrollAction()
    {
        $auth       = Zend_Auth::getInstance();
        $identity   = $auth->getIdentity();
        $birth_date = $identity->birthdate;
        $is_valid   = Cms_Common::validateDate($birth_date);
    
        if (!$is_valid)
        {
            Cms_Common::notify('error', 'Please update your Date of Birth before continuing to enrol');
            $this->redirect('/user/profile');
        }
        
        $dataDb = new Admin_Model_DbTable_ExternalData();
        $learningmodeTypes = $dataDb->getData('LearningMode');
        $learningmodeTypes = array_column($learningmodeTypes, 'value', 'code');

        $error = false;
        $data = $this->getParam('i');
        $regtype = $cid = $course_id = '';

        if ( !empty($data) )
        {
            $data = Cms_Common::encrypt($data, false);

            if ( Cms_Common::isJson($data) )
            {
                $data = json_decode( $data, true);

                $regtype = $data['regtype'];
                $cid = isset($data['cid']) ? $data['cid'] : '';
                $course_id = isset($data['course_id']) ? $data['course_id'] : '';

                $cur = $this->courseDb->fetchRow(array("id = ?" => $course_id));

                $this->view->course_id = $cur['code_safe'];

                $time = $data['time'];
            }
            else
            {
                $error = true;
            }
        }

        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();

            $regtype = $formData['regtype'];
            $cid = isset($formData['cid']) ? $formData['cid'] : '';
            $course_id = isset($formData['course_id']) ? $formData['course_id'] : '';
        }

//        pr($data);
        //check
        if ( $regtype == '' ) $error = true;
        if ( $regtype == 'curriculum' && $cid == '' ) $error = true;
        if ( $regtype == 'course' && $course_id == '' ) $error = true;


        if ( $regtype == 'course' )
        {//echo $course_id;
            $course = $this->courseDb->getCourse(array("a.id = ?" => $course_id));

            if ( empty($course)  )
            {
                return Cms_Render::error('Course doesn\'t exist.');
            }
        }

        if ( $regtype == 'curriculum' )
        {
            $cur = $this->currDb->fetchRow(array("id = ?" => $cid));

            if ( empty($cur)  )
            {
                return Cms_Render::error(Cms_Options::__('label_curriculum').' doesn\'t exist.');
            }

            $post = Cms_Events::trigger('enroll.curriculum.check', $this, array('cid'=>$cid));

            if ( !empty($post) && count($post) > 0 )
            {
                if ( $post[0]['fail'] == 1 )
                {
                    return Cms_Render::error($post[0]['error']);
                }
            }

            $cur = $cur->toArray();
        }


//        echo $regtype;exit;
        //check again
        if ( !Zend_Auth::getInstance()->hasIdentity() )
        {
            return Cms_Render::error('You must be logged in to enroll in a course. <a href="'.$this->view->baseUrl().'/index/register/course/'.$course['id'].'">Login</a> or <a href="'.$this->view->baseUrl().'/register">Sign Up</a>');
        }

        if ( $error == true )
        {
            $errorMsg = true;
        }

        /***  disabled for fpx ***
        if ( !empty($time) && $time+900 < time() )
        {
           return Cms_Render::error('Session Ended. Please try again.');
        }
        /*** end disable for fpx ***/

        //canenrol
        $canenrol = true;

        //have you enrolled in this course before
        $auth = Zend_Auth::getInstance();
        $user_id = $auth->getIdentity()->id;

        $check_id = $regtype == 'course' ? $course_id : $cid;
        $check = $this->enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' => $regtype, ($regtype == 'course'?'course_id':'curriculum_id').' = ?' => $check_id));

        if ( !empty($check) )
        {
            $canenrol = false;
            $errorMsg = 'You already enrolled in this course';
        }

        //order
        $check2 = $this->orderDb->getOrder(array('status != ?' => 'CANCELLED', 'user_id = ?' => $user_id, 'regtype = ?' => $regtype, 'item_id = ?' => $check_id));


        if ( !empty($check2) ) {
            if ($check2['status'] == 'ACTIVE') {
                $canenrol = false;
                $errorMsg = 'You already enrolled in this ' . ($regtype == 'course' ? 'course' : Cms_Options::__('label_curriculum'));
            } else if ($check2['status'] == 'PENDING') {

                $canenrol = false;
                $errorMsg = 'You already enrolled in this ' . ($regtype == 'course' ? 'course' : Cms_Options::__('label_curriculum') ). ' and is pending approval/payment. <a class="uk-button uk-button-mini" href="'.$this->view->baseUrl().'/user/purchase-history">View</a>';
            }
        }


        //are you not a student
        if ( $auth->getIdentity()->role != 'student' )
        {
            $canenrol = false;
            $errorMsg = 'Only students can enroll in courses.';
        }

        //views
        $this->view->regtype = $regtype;
        $this->view->cid = $cid;

        if ( $regtype == 'course')
        {
            $this->view->course  = $course;
        }
        else
        {
            //courses
            $getcourses = $this->currCoursesDb->getData($cid);

            $this->view->courses = $getcourses;
            $this->view->cur = $cur;
        }

        //error trap
        if ( $canenrol == false || $error == true )
        {
            return Cms_Render::error($errorMsg);
        }

        //cart
        $cart = array();

        if ( $regtype == 'curriculum' )
        {

            $curcoursesDB = new App_Model_CurriculumCourses();
            $courses = $curcoursesDB->getData($cur['id']);

            $datacourses = implode(',', array_column($courses, 'external_id'));

            $data = array(
                'IdProgram'         => $cur['external_id'],
                'IdProgramScheme'   => 0,
                'StudentCategory'   => $auth->getIdentity()->nationality == 'MY' ? 579 : 580,
                'IdSubject'         => 0,
                'courses'           => $datacourses
            );

            $cart[] = array(

                'name'          => $cur['name'],
                'code'          => $cur['code'],
                'description'   => $cur['description'],
                'external_id'   => $cur['external_id'],
                'price'         => 0,
                'link'          => $this->view->baseUrl().'/courses/curriculum/cid/'.$cur['id'],
                'regtype'       => 'curriculum',
                'id'            => $cur['id'],
                'data'          => $data
            );

        }

        if ( $regtype == 'course' )
        {
            $cur = $this->currDb->getCurriculum(array('id = ?' => $cid));

            if ( empty($cur) )
            {
                return Cms_Render::error('Invalid Cur ID');
            }

            $data = array(
                'IdProgram'         => $cur['external_id'],
                'IdProgramScheme'   => 0,
                'StudentCategory'   => $auth->getIdentity()->nationality == 'MY' ? 579 : 580,
                'IdSubject'         => $course['external_id'],
                'courses'            => $course['external_id']
            );

            $cart[] = array(
                'name'          => $course['title'],
                'code'          => $course['code'],
                'description'   => $course['description'],
                'external_id'   => $course['external_id'],
                'price'         => 0,
                'link'          => $this->view->baseUrl().'/courses/'.$course['code_safe'].'/pid/'.$course['curriculum_id'],
                'curriculum_id' => $course['curriculum_id'],
                'id'            => $course['id'],
                'regtype'       => 'course',
                'data'          => $data
            );

        }

        //friends
        $this->view->guests = $this->session->guests;

        //last check
        $this->view->cart = $cart;
        if ( empty($cart) )
        {
           return Cms_Render::error('Incomplete data for enrolment.');
        }
        else
        {
            $this->session->cart = $cart;
        }
    }

    public function checkoutAction()
    {
     $currDb = new App_Model_Curriculum();
     $courseDb = new App_Model_Courses();
     // $this->_redirect('/register/checkout-modedemo/i/');
     $dataDb = new Admin_Model_DbTable_ExternalData();
     $learningmodeTypes = $dataDb->getData('LearningMode');
     $learningmodeTypes = array_column($learningmodeTypes, 'value', 'code');
     $order = array();
     if ($this->getRequest()->isPost())
     {
         $formData = $this->getRequest()->getPost();
            // echo '<pre>';
            // print_r($formData);
            // die();
         if ( Cms_Common::tokenCheck($formData['token']) === false )
         {
             return Cms_Render::error('Invalid Session. Please try again.');
         }

         if ( !isset($this->session->cart) || count($this->session->cart) == 0 )
         {
             return Cms_Render::error('Cart is empty. Please try again.');
         }

         if ( !Zend_Auth::getInstance()->hasIdentity() )
         {
             return Cms_Render::error('You must be logged in or. <a href="'.$this->view->baseUrl().'/login">Login</a> or <a href="'.$this->view->baseUrl().'/register">Sign Up</a>');
         }

         //have you enrolled in this course before
         $auth = Zend_Auth::getInstance();
         $user_id = $auth->getIdentity()->id;

         $registration_category = count($this->session->guests) ? 2 : 1; //1 solo 2 party queue

         $totalapplication = count($this->session->guests) + 1;//pr($this->session->cart);//exit;
         foreach ( $this->session->cart as $row )
         {
             //canenrol
             $canenrol = true;

             $regtype = $row['regtype'];
             $type = $regtype == 'course' ? 'course_id' : 'curriculum_id';
             $id = $row['id'];

             $check = $this->enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' => $regtype, $type.'= ?' => $id));

             if (!empty($check)) {
                 $canenrol = false;
                 $error = 'You already enrolled in this '.($regtype=='course'?'course':Cms_Options::__('label_curriculum'));
             }

             //order
             $check2 = $this->orderDb->getOrder(array('status != ?' => 'CANCELLED', 'user_id = ?' => $user_id, 'regtype = ?' => $regtype, 'item_id = ?' => $id));
             if ( !empty($check2) )
             {
                 if ( $check2['status'] == 'ACTIVE')
                 {
                     $canenrol = false;
                     $error = 'You already enrolled in this '.($regtype=='course'?'course':Cms_Options::__('label_curriculum'));
                 }
                 else if ( $check2['status'] == 'PENDING' )
                 {
                     $canenrol = false;
                     $error = 'You already enrolled in this '.($regtype=='course'?'course':Cms_Options::__('label_curriculum').' and is pending approval/payment.');
                 }
             }

             //are you not a student
             if ($auth->getIdentity()->role != 'student') {
                 $canenrol = false;
                 $error = 'Only students can enrol in courses.';
             }

             //error trap
             if ($canenrol == false) {
                 return Cms_Render::error($error);
             } else {

                 $userDb = new App_Model_User();
                 $user = $userDb->getUser($auth->getIdentity()->id);
                 $extDataDb = new Admin_Model_DbTable_ExternalData();

                 $company_id = !empty($user['company']) ? $extDataDb->getValue($user['company'])['code'] : '';
                 $qualification_id = !empty($user['qualification']) ? $extDataDb->getValue($user['qualification'])['code'] : '';

                 $learningmode = 'OL';
                 $schedule = array();

                 //single
                 if ( $regtype == 'course' )
                 {
                     /*
                      * NOTE: move all IBFIM related enrollment to plugin
                      */
                     $course = $courseDb->getCourse(array('a.id = ?'=>$row['id']));
                     if ( empty($course) )
                     {
                         return Cms_Render::error('Invalid Course ID');
                     }

                     $curr = $currDb->fetchRow( array('id = ?' => $row['curriculum_id']));
                     if ( empty($curr) )
                     {
                         return Cms_Render::error('Invalid '.Cms_Options::__('label_curriculum'));
                     }


                     $schedule = array();
                     if ( !isset($formData['scheduledata'][$row['id']]) )
                     {
                         return Cms_Render::error('Schedule must be selected');
                     }


                     $getschedule = json_decode($formData['scheduledata'][$row['id']],true);
                     if ( is_array($getschedule) )
                     {
                         //return Cms_Render::error('Invalid Schedule Data');
                         //schedule[curr_id][course_id] = course_group_tagging_id;
                         foreach ( $getschedule as $key => $value)
                         {
                             $schedule[$row['id']][$key] = $value;
                         }
                     }

                     $learningmode = Plugins_App_Registration::getLearningMode((!isset($schedule[$row['id']]) ? array() : $schedule[$row['id']]));

                     $scheduledata = isset($schedule[$row['id']]) ? json_encode($schedule[$row['id']]) : null;

                     $curr = $curr->toArray();

                     //1. register as student kalau tak exists
                     $params = array(
                         'company_id' => $company_id,
                         'qualification_id' => $qualification_id
                     );

                     $params = $params + $user;

                     //student profile
                     if ( !empty($user['external_id']) || $user['external_id'] != 0 )
                     {
                         $external_id = $user['external_id'];
                     }
                     else
                     {
                         $sp = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewProfile', $params);
                         $userDb->update(array('external_id' => $sp['sp_id']), array('id = ?' => $user['id']));
                         $external_id = $sp['sp_id'];
                     }

                     //student registration
                     if ( empty($check['external_id']) || empty($check2['external_id']) )
                     {
                         //scheme & landscape
                         $params = array(
                             'IdProgram' => $row['data']['IdProgram'],
                             'IdMode'    => $learningmodeTypes[$learningmode],
                         );

                         $externalData = Cms_Curl::__(SMS_API . '/get/Registration/do/externalData', $params);

                         if ( empty($externalData) )
                         {
                             return Cms_Render::error('Error Fetching Data. #extd');
                         }

                         $params = array(
                             'sp_id' => $external_id,
                             'IdProgram' => $row['data']['IdProgram'],
                             'IdProgramScheme' => $externalData['scheme']['IdProgramScheme'],
                             'IdLandscape' => $externalData['landscape']['IdLandscape']
                         );


                         $studregistrationDb = new App_Model_StudentRegistration();



                         if($external_id!=0){

                             //get idstudentregistration n update scheme,landscape
                             $regStudent = $studregistrationDb->getDataStudentRegistration($external_id);//pr($regStudent);
                             $dataProfile = array(
                                 // 'sp_id' => $external_id,
                                 'IdProgram' => $row['data']['IdProgram'],
                                 'IdProgramScheme' => $externalData['scheme']['IdProgramScheme'],
                                 'IdLandscape' => $externalData['landscape']['IdLandscape']
                             );
//
                             //$studregistrationDb->update(array('IdProgramScheme' =>  $externalData['scheme']['IdProgramScheme']), array('IdStudentRegistration = ?' => $regStudent["IdStudentRegistration"]));
                             $studregistrationDb->updateData($dataProfile,$regStudent["IdStudentRegistration"]);

                         }

                         //    $regStudent = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewReg', $params);

                     }
                     else
                     {
                         return Cms_Render::error('Already registered for #'.$row['id'].' #476');
                     }

                     //register
                     $register_external = array();
//ni code asal, utk register subject kne pass id tbl subjectmaster msk dlm studregsubjects, bukan id courses dlm lms
//                     $register_external[] = array(
//                         'lms_course_id'             =>  $course['external_id'],
//                         'course_id'                 => $course['id']
//                     );
                     $register_external[] = array(
                         'lms_course_id'             =>  $course['external_id'],
                         'course_id'                 => $course['external_id']
                     );

                     $params = array('courses' => $register_external, 'check' => 1, 'student_id' => $regStudent['IdStudentRegistration']);

                     $course_schedule = isset($schedule[$row['id']]) ? $schedule[$row['id']] : array();

                     $params = $params + array('schedule' => $course_schedule);


                     $registered_external = Cms_Curl::__(SMS_API . '/get/Registration/do/addSubject', $params);
                    if ( empty($register_external) )
                     {
                         return Cms_Render::error('Unable to sync registration data.');
                     }

                     //1.1.1.1 nak kena create amount dulu rupanya. sad life
                     //ni yg baru masa proforma pn paggil id subject dr tbl subjectmaster bkn id courses dr lms
                     $proformaInvoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateProformaIndividual',array('IdSubject' => $row['data']['IdSubject'], 'IdStudentRegistration' => $regStudent['IdStudentRegistration'],'categoryId' => $registration_category,'totalStudent'=>$totalapplication));
                     //ni ori
                    // $proformaInvoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateProformaIndividual',array('IdSubject' => $row['id'], 'IdStudentRegistration' => $regStudent['IdStudentRegistration'],'categoryId' => $registration_category,'totalStudent'=>$totalapplication));
             //      echo "sini"; pr( $proformaInvoice);
                  //  echo $proformaInvoice["bill_number"]."-billnumber";
//exit;
                     if ( empty($proformaInvoice) || $proformaInvoice['error'] == 1 )
                     {
                         return Cms_Render::error('Unable to generate proforma invoice.');
                     }

                     //generate invoice from proforma
                     $params = array('ProformaId' => $proformaInvoice['id'], 'creatorId' => 665, 'from' => 1, 'returnCurrency' => 1);

                     if (isset($this->session->cart[0]['coupon_id'])) {
                         $params['coupon_id'] = $this->session->cart[0]['coupon_id'];
                         $params['coupon_amount'] = $this->session->cart[0]['coupon_amount'];
                     }

                     $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoice', $params);

                     if ($invoice['error']==0)
                     {
                         $invoiceId = $invoice['invoiceId'];
                         $invoiceNo = $invoice['invoiceNo'];
                         $currencyId = $invoice['currencyId'];
                         $invoiceAmount = $invoice['amount'];

                         //last resort. update amount using data from invoice
                         $paramInvoice = array(
                             'invoiceId'             => $invoiceId,
                             'coupon_code'           => @$this->session->cart[0]['coupon_code'],
                             'creator_id'            => $auth->getIdentity()->id
                         );
                         $paramGetInvoice = array(
                             'id'                    => $invoiceId
                         );

                         $regen_invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/re_generateInvoice', $paramInvoice);
                         $latest_invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoice', $paramGetInvoice);

                         if ($latest_invoice['error'] != 1)
                         {
                             $invoiceAmount = $latest_invoice['bill_amount'];
                         }

                         //2. add order
                         $order = array(
                             'user_id'               => $auth->getIdentity()->id,
                             'created_date'          => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                             'created_by'            => $auth->getIdentity()->id,
                             'amount'                => $invoiceAmount,
                             'paymentmethod'         => 'MIGS',
                             'status'                => 'PENDING',
                             'invoice_id'            => $invoiceId,
                             'invoice_no'            => $invoiceNo,
                             'invoice_external'      => 1,
                             'regtype'               => $regtype,
                             'curriculum_id'         => $row['curriculum_id'],
                             'item_id'               => $row['id'],
                             'learningmode'          => $learningmode,
                             'schedule'              => $scheduledata,
                             'external_id'           => $regStudent['IdStudentRegistration']
                         );

                         $order_id = $this->orderDb->insert($order);
                         $order['id'] = $order_id;

                         //3. add guests
                         $this->processGuests($order_id);

                         //redirect
                         $params = array(
                             'token'                 => Cms_Common::token('ibfimpay'),
                             'invoiceId'             => $invoiceId,
                             'requestFrom'           => 1,
                             'currencyId'            => $currencyId,
                             'invoiceNo'             => $invoiceNo,
                             'totalAmount'           => Cms_Common::MigsAmount($invoiceAmount),
                             'die'                   => 1,
                             'order_id'              => $order_id
                         );

                         //redirect to payment gateway
                         $urlMigs  = Cms_Curl::__(URL_PAYMENT . '/migs/', $params);

                         /*                            if($urlMigs) {

                                                         $this->view->pay_url = $urlMigs;

                                                         header('location: ' . $urlMigs);
                                                     }else{
                                                         return Cms_Render::error('Something went wrong #617.');
                                                     }*/
                         // todo payment mode

                     } else {
                         return Cms_Render::error('Something went wrong #552.');
                     }

                     /*//1. register as student kalau tak exists
                    $params = array(
                                        'company_id' => $company_id,
                                        'qualification_id' => $qualification_id
                    );

                    $params = $params + $user;

                    if ( !empty($user['external_id']) || $user['external_id'] != 0 )
                    {
                        $external_id = $user['external_id'];
                    }
                    else
                    {
                        $external_id = Cms_Curl::__(SMS_API . '/get/Registration/do/addNew', $params);
                        $userDb->update(array('external_id' => $external_id), array('id = ?' => $user['id']));
                    }

                    //generate proforma based on selected program and stuff

                    //2. add order
                    $order = array(
                                    'user_id',
                                    'created_date',
                                    'created_by',
                                    'amount',
                                    'paymentmethod',
                                    'status' => 'PENDING',
                                    'invoice_id' => $invoice_id,
                                    'invoice_no' => $invoice_no,
                                    'invoice_external' => 1,
                                    'regtype' => $regtype,
                                    'item_id' => $row['id']
                    );

                    $order_id = $this->orderDb->insert($order);

                    //redirect payment gateway after user chose

                    //payment
                    $payment = array(
                                        'user_id'   => $auth->getIdentity()->id,
                                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                        'created_by' => $auth->getIdentity()->id,
                                        'regtype'   => $regtype,
                                        $type => $row['id'],
                                        'logdata' => ''
                    );

                    $payment_id = $this->paymentDb->insert($payment);

                    $data = array(
                        $type => $id,
                        'user_id' => $user_id,
                        'regtype' => $regtype,
                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by' => $auth->getIdentity()->id,
                        'payment_id' => $payment_id
                    );

                    $this->enrolDb->insert($data);*/
                 }
                 else
                 {
                     /*
                      * NOTE: move all IBFIM related enrollment to plugin
                      */
                     $curr = $currDb->fetchRow( array('id = ?' => $row['id']));
                     if ( empty($curr) )
                     {
                         return Cms_Render::error('Invalid '.Cms_Options::__('label_curriculum'));
                     }

                     $schedule = array();
                     if ( !isset($formData['scheduledata'][$row['id']]) )
                     {
                         return Cms_Render::error('Schedule must be selected');
                     }

                     $getschedule = json_decode($formData['scheduledata'][$row['id']],true);
                     if ( is_array($getschedule) )
                     {
                         //return Cms_Render::error('Invalid Schedule Data');
                         //schedule[curr_id][course_id] = course_group_tagging_id;
                         foreach ( $getschedule as $key => $value)
                         {
                             $schedule[$row['id']][$key] = $value;
                         }
                     }

                     $learningmode = Plugins_App_Registration::getLearningMode((!isset($schedule[$row['id']]) ? array() : $schedule[$row['id']]));

                     $scheduledata = isset($schedule[$row['id']]) ? json_encode($schedule[$row['id']]) : null;

                     $curr = $curr->toArray();

                     //1. register as student kalau tak exists
                     $params = array(
                         'company_id' => $company_id,
                         'qualification_id' => $qualification_id
                     );

                     $params = $params + $user;

                     //student profile
                     if ( !empty($user['external_id']) || $user['external_id'] != 0 )
                     {
                         $external_id = $user['external_id'];
                     }
                     else
                     {
                         // $sp = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewProfile', $params);
                         $userDb->update(array('external_id' => $sp['sp_id']), array('id = ?' => $user['id']));
                         $external_id = $sp['sp_id'];
                     }

                     //student registration
                     if ( empty($check['external_id']) || empty($check2['external_id']) )
                     {
                         //scheme & landscape
                         $params = array(
                             'IdProgram' => $curr['external_id'],
                             'IdMode'    => $learningmodeTypes[$learningmode],
                         );

                         // $externalData = Cms_Curl::__(SMS_API . '/get/Registration/do/externalData', $params);
                         if ( empty($externalData) )
                         {
                             return Cms_Render::error('Error Fetching Data. #extd');
                         }

                         if ( empty($externalData['scheme']) )
                         {
                             return Cms_Render::error('No training available for selected learning mode. Some courses/program are not available in Blended learning mode (mixture of online and face to face).');
                         }




                         $params = array(
                             'sp_id' => $external_id,
                             'IdProgram' => $curr['external_id'],
                             'IdProgramScheme' => $externalData['scheme']['IdProgramScheme'],
                             'IdLandscape' => $externalData['landscape']['IdLandscape']
                         );

                         $regStudent = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewReg', $params);

                     }
                     else
                     {
                         return Cms_Render::error('Already registered for #'.$row['id'].' #476');
                     }

                     //register
                     $courses = $this->currCoursesDb->getData($row['id']);

                     $register_external = array();

                     foreach ( $courses as $course )
                     {
                         $register_external[] = array(
                             'lms_course_id'             =>  $course['external_id'],
                             'course_id'                 => $course['id']

                         );
                     }

                     $params = array('courses' => $register_external, 'check' => 1, 'student_id' => $regStudent['IdStudentRegistration']);

                     $params = $params + array('schedule' => $schedule[$row['id']]);

                     $registered_external = Cms_Curl::__(SMS_API . '/get/Registration/do/addSubject', $params);

                     if ( empty($register_external) )
                     {
                         return Cms_Render::error('Unable to sync registration data.');
                     }

                     //1.1.1.1 nak kena create amount dulu rupanya. sad life
                     $proformaInvoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateProformaIndividual',array('IdSubject' => 0, 'IdStudentRegistration' => $regStudent['IdStudentRegistration'],'categoryId' => $registration_category,'totalStudent'=>$totalapplication));

//                        echo "<pre>";
//                        print_r($proformaInvoice);
//                        exit;

                     if ( ($proformaInvoice['error'] == 1) )
                     {
                         return Cms_Render::error('Unable to generate proforma invoice.');
                     }

                     //generate invoice from proforma
                     $params = array('ProformaId' => $proformaInvoice['id'], 'creatorId' => 665, 'from' => 1, 'returnCurrency' => 1);

                     if (isset($this->session->cart[0]['coupon_id'])) {
                         $params['coupon_id'] = $this->session->cart[0]['coupon_id'];
                         $params['coupon_amount'] = $this->session->cart[0]['coupon_amount'];
                     }

                     $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoice', $params);
//echo "sinisss";exit;
//
//                        exit;
                     $invoice['error'] = 0;
                     if ($invoice['error']==0)
                     {
                         $invoiceId = $invoice['invoiceId'];
                         $invoiceNo = $invoice['invoiceNo'];
                         $currencyId = $invoice['currencyId'];
                         $invoiceAmount = $invoice['amount'];

                         //last resort. update amount using data from invoice
                         $paramInvoice = array(
                             'invoiceId'             => $invoiceId,
                             'coupon_code'           => @$this->session->cart[0]['coupon_code'],
                             'creator_id'            => $auth->getIdentity()->id
                         );
                         $paramGetInvoice = array(
                             'id'                    => $invoiceId
                         );

                         $regen_invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/re_generateInvoice', $paramInvoice);
                         $latest_invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoice', $paramGetInvoice);

                         if ($latest_invoice['error'] != 1)
                         {
                             $invoiceAmount = $latest_invoice['bill_amount'];
                         }

                         //2. add order
                         $order = array(
                             'user_id'               => $auth->getIdentity()->id,
                             'created_date'          => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                             'created_by'            => $auth->getIdentity()->id,
                             'amount'                => $invoiceAmount,
                             'paymentmethod'         => 'MIGS',
                             'status'                => 'PENDING',
                             'invoice_id'            => $invoiceId,
                             'invoice_no'            => $invoiceNo,
                             'invoice_external'      => 1,
                             'regtype'               => $regtype,
                             'curriculum_id'         => $row['id'],
                             'item_id'               => $row['id'],
                             'learningmode'          => $learningmode,
                             'schedule'              => $scheduledata,
                             'external_id'           => $regStudent['IdStudentRegistration']
                         );

                         $order_id = $this->orderDb->insert($order);
                         $order['id'] = $order_id;


//echo $order_id."+++";exit;
                         //3. add guests
//                         $this->processGuests($order_id);
//echo "order_id".$order_id;
                         //redirect
                         $params = array(
                             'token'                 => Cms_Common::token('ibfimpay'),
                             'invoiceId'             => $invoiceId,
                             'requestFrom'           => 1,
                             'currencyId'            => $currencyId,
                             'invoiceNo'             => $invoiceNo,
                             'totalAmount'           => Cms_Common::MigsAmount($invoiceAmount),
                             'die'                   => 1,
                             'order_id'              => $order_id
                         );

                         //redirect to payment gateway
                         $urlMigs  = Cms_Curl::__(URL_PAYMENT . '/migs/', $params);

                         /*if($urlMigs) {

                             $this->view->pay_url = $urlMigs;

                             header('location: ' . $urlMigs);
                         }else{
                             return Cms_Render::error('Something went wrong #871.');
                         }*/
                         // todo payment mode

                     } else {
                         return Cms_Render::error('Something went wrong #552.');
                     }


                     //courses
                     /*$courses = $this->currCoursesDb->getData($row['id']);

                     $payment = array(
                         'user_id'   => $auth->getIdentity()->id,
                         'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                         'created_by' => $auth->getIdentity()->id,
                         'regtype'   => $regtype,
                         $type => $row['id'],
                         'logdata' => ''
                     );

                     $payment_id = $this->paymentDb->insert($payment);

                     //curriculum kena add juga?
                     $data = array(
                         'curriculum_id' => $row['id'],
                         'user_id' => $user_id,
                         'regtype' => 'curriculum',
                         'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                         'created_by' => $auth->getIdentity()->id,
                         'payment_id' => $payment_id
                     );

                     $this->enrolDb->insert($data);

                     //courses
                     foreach ( $courses as $course )
                     {
                         $data = array(
                             'course_id' => $course['id'],
                             'user_id' => $user_id,
                             'regtype' => 'course',
                             'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                             'created_by' => $auth->getIdentity()->id,
                             'payment_id' => $payment_id
                         );

                         $this->enrolDb->insert($data);
                     }*/
                 }

             }
            // exit;
         }

         Zend_Session::namespaceUnset('register');

     }
     else
     {
         return Cms_Render::error('Woops. You stumbled on this page the wrong way.');
     }

     $data = array(
         'token'    => Cms_Common::token(),
         'order_id' => $order_id
     );
//echo "last";exit;
     $data_json = json_encode($data);
     $hash      = Cms_Common::encrypt($data_json);

     $this->_redirect('/register/checkout-mode/cid/'.$id.'/i/'. $hash);
 }
    protected function processGuests($order_id)
    {
        if ( !empty($this->session->guests) )
        {
            $this->orderDb->update(array('hasgroup' => 1), array('id = ?' => $order_id));

            //add entry
            $data = array(
                            'order_id' => $order_id,
                            'totaluser' => count($this->session->guests),
                            'description' => null,
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => Zend_Auth::getInstance()->getIdentity()->id,
            );
            $ordergroup_id = $this->orderGroupDb->insert($data);

            //loop
            foreach ( $this->session->guests as $guest )
            {
                /*
                 *  [firstname] => Munzir
                    [lastname] => Rosdi
                    [email] => munzir@gmail.com
                    [nationality] => AS
                    [company] => 183
                    [qualification] => 3
                    [birthdate] => 2012-02-03
                 */
                $data = array(
                                'group_id'      => $ordergroup_id,
                                'order_id'      => $order_id,
                                'email'         => $guest['email'],
                                'firstname'     => $guest['firstname'],
                                'lastname'      => $guest['lastname'],
                                'token'         => Cms_Common::generateRandomString(22),
                                'birthdate'     => $guest['birthdate'],
                                'nationality'   => $guest['nationality'],
                                'company'       => $guest['company'],
                                'qualification' => $guest['qualification'],
                                'activated'     => 0
                );

                $this->orderGroupUserDb->insert($data);
            }
        }
    }

    public static function processEnrollment($e)
    {
        $auth = Zend_Auth::getInstance();
        $orderDb = new App_Model_Order();
        $currCoursesDb = new App_Model_CurriculumCourses();
        $currDb = new App_Model_Curriculum();
        $paymentDb    = new App_Model_PaymentHistory();
        $enrolDb = new App_Model_Enrol();
        $courseDb = new App_Model_Courses();
        $erDB = new App_Model_Exam_ExamRegistration();

        $payment_status = $e->getParams();

        if(!isset($payment_status['success'])) {
            $payment_status['success'] = 0;
        }

        if(!isset($payment_status['msg'])) {
            $payment_status['msg'] = 'Payment Data Not Found';
        }

        if($payment_status['success'] != 1) {
            return Cms_Render::error($payment_status['msg']);
            // throw new Exception($payment_status['msg']);
        }

        $order_id = $payment_status['external_id'];
        $order    = $orderDb->getOrder(array('a.id = ?' => $order_id), 1);

        if(empty($order)) {
            return Cms_Render::error('Invalid Order ID');
            // throw new Exception('Invalid Order ID');
        }

        if($order['status'] != 'PENDING') {
            return Cms_Render::error('Invalid Order Status');
            // throw new Exception('Invalid Order Status');
        }

        $type = $order['regtype'] == 'curriculum' ? 'curriculum_id' : 'course_id';

        $mode_type   = $payment_status['type'];
        $txn_id      = $payment_status['txn_id'];
        $amount      = $payment_status['amount'];
        $currency_id = $payment_status['currency_id'];
        $migs_id     = ($mode_type == 'MIGS' ? $payment_status['migs_transaction_id'] : 0);
        $fpx_id      = ($mode_type == 'FPX'  ? $payment_status['fpx_transaction_id']  : 0);

        if(!$payment_status['receipt_id']) { // kalau xde receipt, baru generate

            $rcp_description = 'Payment from '. $mode_type .' (' . $txn_id . ')';
            $receiptInfo = array(
                'rcp_date'                  => date("Y-m-d H:i:s"),
                'rcp_receive_date'          => date("Y-m-d H:i:s"),
                'rcp_payee_type'            => 1,
                'rcp_idStudentRegistration' => $order['external_id'],
                'rcp_description'           => $rcp_description,
                'rcp_amount'                => $payment_status['amount'],
                'rcp_gainloss'              => null,
                'rcp_from'                  => 2,
                'rcp_gainloss_amt'          => null,
                'rcp_cur_id'                => $payment_status['currency_id'],
                'refNo'                     => $payment_status['txn_id'],
                'migs_id'                   => $migs_id,
                'fpx_id'                    => $fpx_id,
                'rcp_create_by'             => 665

            );

            $params = array(
                'receiptInfo'           => $receiptInfo,
                'invoiceId'             => $order['invoice_id'],
                'IdStudentRegistration' => $order['external_id']
            );

            $receiptApproval = Cms_Curl::__(SMS_API . '/get/Finance/do/receiptProcess', $params);

            $paymentTesting = 0;
            if (defined("PAYMENT_TESTING")) {
                $paymentTesting = PAYMENT_TESTING;
            }

            if ($paymentTesting == 0) {
                if (empty($receiptApproval) || $receiptApproval['error'] == 1) {
                    //return Cms_Render::error('Receipt Processing Error. #916');
                    die('paymenttesting = 0');
                }
            }
            else {
                @$receiptApproval['error'] = 0;
            }

            if (is_array($receiptApproval) && $receiptApproval['error'] == 0) {
                $receiptId = ($paymentTesting == 0) ? $receiptApproval['receiptId'] : 1;
                $logdata   = ($payment_status['migs_transaction_id'] ? $payment_status['migs_transaction_id'] : $payment_status['fpx_transaction_id']);
                $db2       = getDB2();
                
                if($mode_type == 'MIGS') {
                    $dataUpdate = array(
                        'rcp_id'   => $receiptId,
                        'rcp_date' => date("Y-m-d H:i:s")
                    );
                    $db2->update('transaction_migs', $dataUpdate, array('mt_id = ?' => $migs_id));
                }
                elseif($mode_type == 'FPX') {
                    $dataUpdate = array(
                        'rcp_id'   => $receiptId,
                        'rcp_date' => date("Y-m-d H:i:s")
                    );
                    $db2->update('fpx_transaction', $dataUpdate, array('id = ?' => $fpx_id));
                }
                
                $payment_type = $type;
                if ($order['regtype'] == 'exemption')
                {
                    $payment_type = 'course_id';
                }
                
                //save payment data
                $payment = array(
                    'user_id'          => $auth->getIdentity()->id,
                    'order_id'         => $order['id'],
                    'created_date'     => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'created_by'       => $auth->getIdentity()->id,
                    'regtype'          => $order['regtype'],
                    $payment_type      => $order['item_id'],
                    'logdata'          => $logdata,
                    'payment_mode'     => $payment_status['type'],
                    'receipt_id'       => $receiptId,
                    'receipt_external' => 1
                );

                $payment_id = $paymentDb->insert($payment);

                //update order
                $orderDb->update(array('status' => 'ACTIVE','payment_id'=>$payment_id), array('id = ?'=>$order['id']));
                
                if ($order['regtype'] == 'exemption')
                {
                    Cms_Common::notify('success', 'Credit transfer has been processed successfully');
                    $response = Cms_Curl::__(SMS_API . '/get/Registration/do/processCreditTransfer', array('exemption_id' => $order['external_id']));
                    return;
                }
            
                if($payment_status['registration_item_id'] == 889) { //course

                    //check enrol existing
                    $curEnrol = $enrolDb->getEnrol(array('user_id = ?' => $auth->getIdentity()->id, 'regtype = ?' => 'curriculum', 'curriculum_id = ?' => $order['curriculum_id']));

                    if (empty($curEnrol)) {

                        $cur         = $currDb->getCurriculum(array('id = ?' => $order['curriculum_id']));
                        $expiry_date = $cur['accessperiod'] == null ? null : Plugins_App_Course::accessPeriodDate($cur['accessperiod']);

                        //curriculum kena add
                        $data = array(
                            'curriculum_id' => $order['curriculum_id'],
                            'user_id'       => $auth->getIdentity()->id,
                            'regtype'       => 'curriculum',
                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by'    => $auth->getIdentity()->id,
                            'order_id'      => $order['id'],
                            'learningmode'  => $order['learningmode'],
                            'schedule'      => $order['schedule'],
                            'expiry_date'   => $expiry_date
                        );

                        $enrolDb->insert($data);
                    }

                    $schedules     = json_decode($order['schedule'], true);
                    $learningMode_ = array("OL" => "Online", "FTF" => "Face to Face");

                    //register
                    if ($order['regtype'] == 'course') {

                        $course = $courseDb->getCourse(array('id = ?' => $order['item_id']));

                        $expiry_date = $course['accessperiod'] == null ? null : Plugins_App_Course::accessPeriodDate($course['accessperiod']);

                        $schedule = isset($schedules[$course['external_id']]) ? $schedules[$course['external_id']] : 0;

                        $learningmode = isset($schedule) && $schedule > 0 ? 'FTF' : 'OL';

                        $data = array(
                            'curriculum_id' => $order['curriculum_id'],
                            'course_id'     => $order['item_id'],
                            'user_id'       => $auth->getIdentity()->id,
                            'regtype'       => 'course',
                            'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by'    => $auth->getIdentity()->id,
                            'order_id'      => $order['id'],
                            'learningmode'  => $learningmode,
                            'schedule'      => $schedule,
                            'expiry_date'   => $expiry_date
                        );

                        $enrolDb->insert($data);

                        // auto-populate to group here
                        $coursesDb = new App_Model_Courses();
                        $course_   = $coursesDb->getCourse(array('id = ?' => $order['item_id']));

                        $userGroupDb = new App_Model_UserGroup();
                        $group_      = $userGroupDb->getGroupByName($course_['code'] . ' - ' . $learningMode_[$learningmode]);

                        if (isset($group_) && !empty($group_))
                        {
                            $dataGroup = array(
                                'group_id'      => $group_['group_id'],
                                'user_id'       => $auth->getIdentity()->id,
                                'active'        => 1,
                                'created_date'  => new Zend_Db_Expr('NOW()'),
                                'created_by'    => 1
                            );

                            $userGroupDataDb = new App_Model_UserGroupData();
                            $groupData_      = $userGroupDataDb->insertGroupData($dataGroup);
                        }

                    } 
                    else { // $order['regtype'] != 'course'

                        $courses = $currCoursesDb->getData($order['curriculum_id']);

                        //courses
                        foreach ($courses as $course) {

                            $schedule     = isset($schedules[$course['external_id']]) ? $schedules[$course['external_id']] : 0;
                            $learningmode = $schedule > 0 ? 'FTF' : 'OL';

                            $data = array(
                                'curriculum_id' => $order['curriculum_id'],
                                'course_id'     => $course['id'],
                                'user_id'       => $auth->getIdentity()->id,
                                'regtype'       => 'course',
                                'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by'    => $auth->getIdentity()->id,
                                'order_id'      => $order['id'],
                                'learningmode'  => $learningmode,
                                'schedule'      => $schedule,
                                'expiry_date'   => $expiry_date
                            );

                            $enrolDb->insert($data);

                            // auto-populate to group here
                            $coursesDb = new App_Model_Courses();
                            $course_   = $coursesDb->getCourse(array('id = ?' => $course['id']));

                            $userGroupDb = new App_Model_UserGroup();
                            $group_      = $userGroupDb->getGroupByName($course_['code'] . ' - ' . $learningMode_[$learningmode]);

                            if (isset($group_) && !empty($group_))
                            {
                                $dataGroup = array(
                                    'group_id'      => $group_['group_id'],
                                    'user_id'       => $auth->getIdentity()->id,
                                    'active'        => 1,
                                    'created_date'  => new Zend_Db_Expr('NOW()'),
                                    'created_by'    => 1
                                );

                                $userGroupDataDb = new App_Model_UserGroupData();
                                $groupData_      = $userGroupDataDb->insertGroupData($dataGroup);
                            }
                        }
                    }

                    Cms_Events::trigger('auth.register.course', null, array('order_id' => $order_id));
                }
                elseif ($payment_status['registration_item_id'] == 879) {//exam

                    //get exam registration
                    /*$orderExam = $erDB->getExamRegistration($order['external_id'], $order['IdProgram'], false);
                    if(!$orderExam) {
                        $orderExam = $erDB->getExamRegistrationByStdId($order['external_id'], $order['IdProgram'], false);
                    }*/
                	$orderExam = $erDB->getExamRegistrationByStdId($order['external_id'], $order['IdProgram'], false);

                    //update exam registration status
                    if($orderExam){
                        foreach($orderExam as $exam){
                            $params = array(
                                'er_id' => $exam['er_id'],
                                'remarks'  => 'Payment success (LMS) - user_id:'.$auth->getIdentity()->id
                            );

                            $examRegStatus = Cms_Curl::__(SMS_API . '/get/Examination/do/updateExamRegistrationStatus', $params);

                            if(isset($examRegStatus['error']) && $examRegStatus['error'] == 1){
                                $msg = $examRegStatus['msg'];
                                $msg .= ' Click <a href="/exam/view-exam-detail/id/'.$exam['er_id'].'"><button class="uk-button uk-button-small">here</button></a> to change schedule. ';

                                return Cms_Render::error($msg);
                            }

                        }
                    }
    
                    Cms_Events::trigger('auth.register.exam', null, array('order_id' => $order_id));

                }
            }
            else { // !is_array($receiptApproval) || $receiptApproval['error'] != 0
                return Cms_Render::error('Error on generating receipt');
            }
        }
        else { // dah ada resit
            // kalau da ada resit nak buat apa?? mana ada pape....kot.
        }
        return;
        exit;


        $data = $e->getParams();

        $data = $data['data'];

        //get migs  mt_external_id to get order info
        $order = $orderDb->getOrder(array('a.id = ?' => $data['mt_external_id']), 1);

        if (empty($order)) throw new Exception('Invalid Order ID');

        if ( $order['status'] != 'PENDING' )
        {
            return;
        }

        //TABLEY
        $success = false;

        $type = $order['regtype'] == 'curriculum' ? 'curriculum_id' : 'course_id';

        //generate & receipt approval
        if ($data['mt_status'] == 'SUCCESS' && $data['mt_rep_txn_response_code'] == '0'  && $data['rcp_id'] == 0)
        {
            $rcpDesc = 'Payment from MIGS (' . $data['mt_txn_no'] . ')';
            $receiptInfo = array(
                'rcp_date' => date("Y-m-d H:i:s"),
                'rcp_receive_date' => date("Y-m-d H:i:s"),
                'rcp_payee_type' => 1,
                'rcp_idStudentRegistration' => $data['mt_external_id'],
                'rcp_description' => $rcpDesc,
                'rcp_amount' => $data['mt_amount'],
                'rcp_gainloss' => null,
                'rcp_from' => 2,
                'rcp_gainloss_amt' => null,
                'rcp_cur_id' => $data['mt_currency_id'],
                'refNo' => $data['mt_txn_no'],
                'migs_id' => $data['mt_id'],
                'rcp_create_by'=>665

            );

            $params = array(
                'receiptInfo'   => $receiptInfo,
                'invoiceId'     => $order['invoice_id'],
                'IdStudentRegistration' => $order['external_id']
            );

            $receiptApproval = Cms_Curl::__(SMS_API . '/get/Finance/do/receiptProcess', $params);
            
            $paymentTesting = 0;
            if(PAYMENT_TESTING){
                $paymentTesting = PAYMENT_TESTING;
            }

            if($paymentTesting == 0) {
                if (empty($receiptApproval) || $receiptApproval['error'] == 1) {
                    //return Cms_Render::error('Receipt Processing Error. #916');
                    die('paymenttesting = 0');
                }
            }else{
                @$receiptApproval['error'] = 0;
            }

            if ( is_array($receiptApproval) && $receiptApproval['error'] == 0 )
            {
                $receiptId = ($paymentTesting == 0) ? $receiptApproval['receiptId'] : 1;

                //save payment data
                $payment = array(
                    'user_id'   => $auth->getIdentity()->id,
                    'order_id'  => $order['id'],
                    'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'created_by' => $auth->getIdentity()->id,
                    'regtype'   => $order['regtype'],
                    $type => $order['item_id'],
                    'logdata' => $data['mt_id'],
                    'payment_mode' => 'MIGS',
                    'receipt_id' => $receiptId,
                    'receipt_external' => 1
                );

                $payment_id = $paymentDb->insert($payment);

                //update order
                $orderDb->update(array('status' => 'ACTIVE','payment_id'=>$payment_id), array('id = ?'=>$order['id']));

                $success=true;
            }
        }

        $user_id = $auth->getIdentity()->id;

        if ( $success == true ) {

            if($data['mt_registration_item_id'] == 889) { //course

                //check enrol existing

                $curEnrol = $enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' =>
                    'curriculum', 'curriculum_id = ?' => $order['curriculum_id']));

                if (empty($curEnrol)) {

                    $cur = $currDb->getCurriculum(array('id = ?' => $order['curriculum_id']));

                    $expiry_date = $cur['accessperiod'] == null ? null : Plugins_App_Course::accessPeriodDate($cur['accessperiod']);

                    //curriculum kena add
                    $data = array(
                        'curriculum_id' => $order['curriculum_id'],
                        'user_id' => $auth->getIdentity()->id,
                        'regtype' => 'curriculum',
                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by' => $auth->getIdentity()->id,
                        'order_id' => $order['id'],
                        'learningmode' => $order['learningmode'],
                        'schedule' => $order['schedule'],
                        'expiry_date' => $expiry_date
                    );

                    $enrolDb->insert($data);
                }

                $schedules = json_decode($order['schedule'], true);

                $learningMode_ = array("OL" => "Online", "FTF" => "Face to Face");

                //register
                if ($order['regtype'] == 'course') {

                    $course = $courseDb->getCourse(array('id = ?' => $order['item_id']));

                    $expiry_date = $course['accessperiod'] == null ? null : Plugins_App_Course::accessPeriodDate($course['accessperiod']);

                    $schedule = isset($schedules[$course['external_id']]) ? $schedules[$course['external_id']] : 0;

                    $learningmode = isset($schedule) && $schedule > 0 ? 'FTF' : 'OL';

                    $data = array(
                        'curriculum_id' => $order['curriculum_id'],
                        'course_id' => $order['item_id'],
                        'user_id' => $auth->getIdentity()->id,
                        'regtype' => 'course',
                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by' => $auth->getIdentity()->id,
                        'order_id' => $order['id'],
                        'learningmode' => $learningmode,
                        'schedule' => $schedule,
                        'expiry_date' => $expiry_date
                    );

                    $enrolDb->insert($data);

                    // auto-populate to group here
                    $coursesDb = new App_Model_Courses();
                    $course_ = $courseDb->getCourse(array('id = ?', $order['item_id']));

                    $userGroupDb = new App_Model_UserGroup();
                    $group_ = $userGroupDb->getGroupByName($course_['code'] . ' - ' . $learningMode_[$learningmode]);

                    $dataGroup = array(
                                    'group_id'      => $group_['group_id'],
                                    'user_id'       => $auth->getIdentity()->id,
                                    'active'        => 1,
                                    'created_date'  => new Zend_Db_Expr('NOW()'),
                                    'created_by'    => 1
                        );

                    $userGroupDataDb = new App_Model_UserGroupData();
                    $groupData_ = $userGroupDataDb->insertGroupData($dataGroup);

                } else {

                    $courses = $currCoursesDb->getData($order['curriculum_id']);

                    //courses
                    foreach ($courses as $course) {

                        $schedule = isset($schedules[$course['external_id']]) ? $schedules[$course['external_id']] : 0;
                        $learningmode = $schedule > 0 ? 'FTF' : 'OL';

                        $data = array(
                            'curriculum_id' => $order['curriculum_id'],
                            'course_id' => $course['id'],
                            'user_id' => $auth->getIdentity()->id,
                            'regtype' => 'course',
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => $auth->getIdentity()->id,
                            'order_id' => $order['id'],
                            'learningmode' => $learningmode,
                            'schedule' => $schedule,
                            'expiry_date' => $expiry_date
                        );

                        $enrolDb->insert($data);

                        // auto-populate to group here
                        $coursesDb = new App_Model_Courses();
                        $course_ = $courseDb->getCourse(array('id = ?', $course['id']));

                        $userGroupDb = new App_Model_UserGroup();
                        $group_ = $userGroupDb->getGroupByName($course_['code'] . ' - ' . $learningMode_[$learningmode]);

                        $dataGroup = array(
                                        'group_id'      => $group_['group_id'],
                                        'user_id'       => $auth->getIdentity()->id,
                                        'active'        => 1,
                                        'created_date'  => new Zend_Db_Expr('NOW()'),
                                        'created_by'    => 1
                            );

                        $userGroupDataDb = new App_Model_UserGroupData();
                        $groupData_ = $userGroupDataDb->insertGroupData($dataGroup);

                    }
                }
            }elseif($data['mt_registration_item_id'] == 879){//exam

                //get exam registration
               // $orderExam = $erDB->getExamRegistration($order['external_id'], $order['IdProgram']);
               $orderExam = $erDB->getExamRegistrationByStdId($order['external_id'], $order['IdProgram']);

                //update exam registration status
                if($orderExam){
                    foreach($orderExam as $exam){
                        $params = array(
                            'er_id' => $exam['er_id']
                        );

                        $examRegStatus = Cms_Curl::__(SMS_API . '/get/Examination/do/updateExamRegistrationStatus', $params);

                        if($examRegStatus['error'] == 1){
                            $msg = $examRegStatus['msg'];
                            $msg .= ' Click <a href="/exam/view-exam-detail/id/'.$exam['er_id'].'"><button class="uk-button uk-button-small">here</button></a> to change schedule. ';

                            return Cms_Render::error($msg);
                        }

                    }
                }

            }

        }
        else
        {
            //TODO: redirect to order page
            return Cms_Render::error('Payment failed. Please try again.');
        }

        //courses
        /*$courses = $this->currCoursesDb->getData($row['id']);

        $payment = array(
            'user_id'   => $auth->getIdentity()->id,
            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
            'created_by' => $auth->getIdentity()->id,
            'regtype'   => $regtype,
            $type => $row['id'],
            'logdata' => ''
        );

        $payment_id = $this->paymentDb->insert($payment);

        //curriculum kena add juga?
        $data = array(
            'curriculum_id' => $row['id'],
            'user_id' => $user_id,
            'regtype' => 'curriculum',
            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
            'created_by' => $auth->getIdentity()->id,
            'payment_id' => $payment_id
        );

        $this->enrolDb->insert($data);

        //courses
        foreach ( $courses as $course )
        {
            $data = array(
                'course_id' => $course['id'],
                'user_id' => $user_id,
                'regtype' => 'course',
                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                'created_by' => $auth->getIdentity()->id,
                'payment_id' => $payment_id
            );

            $this->enrolDb->insert($data);
        }*/
    }

    public function enrollexamAction()
    {

        $dataDb = new Admin_Model_DbTable_ExternalData();
        $learningmodeTypes = $dataDb->getData('LearningMode');
        $erDB = new App_Model_Exam_ExamRegistration();

        $learningmodeTypes = array_column($learningmodeTypes, 'value', 'code');

        $error = false;
        $data = $this->getParam('i');

        $regtype = $cid = $external_id = '';
        $cur = array();
        
        /*
         * Array
			(
			    [regtype] => exam
			    [cid] => 84 //program
			    [time] => 1519178107
			    [module_id] => 109
			)
		 */

        if (!empty($data)) {
            $data = Cms_Common::encrypt($data, false);

            if (Cms_Common::isJson($data)) {
                $data = json_decode($data, true);

                $regtype = $data['regtype'];
                $cid = isset($data['cid']) ? $data['cid'] : '';
                $course_id = isset($data['external_id']) ? $data['external_id'] : '';
                $module_id = isset($data['module_id']) ? $data['module_id'] : '';

                //why compare with external maybe ada data type course
                //$cur = $this->courseDb->fetchRow(array("id = ?" => $course_id));
                //yati change 21/2/2018
                $cur = $this->courseDb->fetchRow(array("external_id = ?" => $module_id));

                $this->view->course_id = $cur['code_safe'];

                $time = $data['time'];
            } else {
                $error = true;
            }
        }


        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $regtype = $formData['regtype'];
            $cid = isset($formData['cid']) ? $formData['cid'] : '';
            $course_id = isset($formData['external_id']) ? $formData['external_id'] : '';
        }


        //check
        if ($regtype == '') $error = true;
        if ($regtype == 'curriculum' && $cid == '') $error = true;
        if ($regtype == 'course' && $course_id == '') $error = true;


        if ($regtype == 'course') {
            $course = $this->courseDb->getCourse(array("a.id = ?" => $course_id));

            if (empty($course)) {
                return Cms_Render::error('Course doesn\'t exist.');
            }
        }

        if ($regtype == 'exam') {
        	
        	//get program
            $cur = $this->currDb->fetchRow(array("id = ?" => $cid));

            if (empty($cur)) {
                return Cms_Render::error(Cms_Options::__('label_curriculum') . ' doesn\'t exist.');
            }

            // $post = Cms_Events::trigger('enroll.curriculum.check', $this, array('cid' => $cid));

            // if (!empty($post) && count($post) > 0) {
            //     if ($post[0]['fail'] == 1) {
            //         return Cms_Render::error($post[0]['error']);
            //     }
            // }

            $cur = $cur->toArray();
        }


        //check again
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            return Cms_Render::error('You must be logged in to enroll in a course. <a href="' . $this->view->baseUrl() . '/index/register/course/' . $course['id'] . '">Login</a> or <a href="' . $this->view->baseUrl() . '/register">Sign Up</a>');
        }

        if ($error == true) {
            $errorMsg = true;
        }
    
        if (!Zend_Auth::getInstance()->getIdentity()->nationality) {
            return Cms_Render::error('Your profile is incomplete. Please contact IT Helpdesk to set the nationality for your account before you can enrol to any exam.');
        }
        
        if ($cur['code'] == 'TBE' && Zend_Auth::getInstance()->getIdentity()->nationality != 'MY')
        {
            return Cms_Render::error('Only Malaysians are allowed to enrol into Takaful Basic Examination (TBE)');
        }
        
        /*if ( !empty($time) && $time+900 < time() )
        {
           return Cms_Render::error('Session Ended. Please try again.');
        }*/

        //canenrol
        $canenrol = true;

        //have you enrolled in this course before
        $auth = Zend_Auth::getInstance();
        $user_id = $auth->getIdentity()->id;

        $check_id = $regtype == 'course' ? $course_id : $cid;
        // $check = $this->enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' => $regtype, ($regtype == 'course' ? 'course_id' : 'curriculum_id') . ' = ?' => $check_id));

        // if (!empty($check)) {
        //     $canenrol = false;
        //     $errorMsg = 'You already enrolled in this course';
        // }

        //check exam
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $external_id = $auth->getIdentity()->external_id;//student_profile
        $IdProgram = $data['cid'];
        
        if (!$external_id)
        {
            $icampus_Plugin_Student = new icampus_Plugin_Student();
            $external_id = $icampus_Plugin_Student->createSmsProfile();
        }

        // get student_profile SMS
        // $profile = $erDB->getProfile($external_id);
        // $sp_id = $profile['sp_id'];
        $sp_id = $external_id;

        $dataRegistration = $ExamregDB->getDataRegistration($sp_id, $IdProgram, 0, 1); //1=profilestatus

        $auth = Zend_Auth::getInstance();
        if ($dataRegistration){
            $data = $auth->getIdentity();
            $IdStudentRegistration = $dataRegistration['IdStudentRegistration'];
            $data->IdStudentRegistration = $IdStudentRegistration;
            $auth->getStorage()->write($data);

        }

        $check3 = $ExamregDB->getExamstudentid($sp_id,$IdProgram);
           //pr($check3);exit;

        $external_id = isset($IdStudentRegistration)?$IdStudentRegistration:0;

        if ( !empty($check3) ) {
            if ($check3['student_id'] == $external_id || $check3['IdProgram'] != $IdProgram) {
                $canenrol = false;
                $errorMsg = 'You already enrolled in this Exam' ;
            }
        }


        //order
        $check2 = $this->orderDb->getOrder(array('status != ?' => 'CANCELLED', 'user_id = ?' => $user_id, 'regtype = ?' => $regtype, 'item_id = ?' => $check_id));


        if ( !empty($check2) ) {
            if ($check2['status'] == 'ACTIVE') {
                $canenrol = false;
                $errorMsg = 'You already enrolled in this exam';
            } else if ($check2['status'] == 'PENDING') {

                $canenrol = false;
                $errorMsg = 'You already enrolled in this exam and is pending approval/payment. <a class="uk-button uk-button-mini" href="'.$this->view->baseUrl().'/user/purchase-history">View</a>';
            }
        }


        //are you not a student
        if ( $auth->getIdentity()->role != 'student' )
        {
            $canenrol = false;
            $errorMsg = 'Only students can enroll in courses.';
        }

        //views
        $this->view->regtype = $regtype;
        $this->view->cid = $cid;

        if ( $regtype == 'course')
        {
            $this->view->course  = $course;
        }
        else
        {
            //courses
            $getcourses = $this->currCoursesDb->getData($cid);

            $this->view->courses = $getcourses;
            $this->view->cur = $cur;
        }

        //error trap
        if ( $canenrol == false || $error == true )
        {
            return Cms_Render::error($errorMsg);
        }

        //cart
        $cart = array();

        if ( $regtype == 'exam' )
        {

            $curcoursesDB = new App_Model_CurriculumCourses();
            $courses = $curcoursesDB->getData($cur['id']);

            $datacourses = implode(',', array_column($courses, 'external_id'));

            $data = array(
                            'IdProgram'         => $cur['external_id'],
                            'IdProgramScheme'   => 0,
                            'StudentCategory'   => $auth->getIdentity()->nationality == 'MY' ? 579 : 580,
                            'IdSubject'         => 0,
                            'courses'           => $datacourses
            );

            $cart[] = array(
                                'name'          => $cur['name'],
                                'description'   => $cur['description'],
                                'price'         => 0,
                                'link'          => $this->view->baseUrl().'/courses/curriculum/cid/'.$cur['id'],
                                'regtype'       => 'exam',
                                'id'            => $cur['id'],
                                'data'          => $data
            );

        }
        
        if ( $regtype == 'course' )
        {
            $cur = $this->currDb->getCurriculum(array('id = ?' => $cid));

            if ( empty($cur) )
            {
                return Cms_Render::error('Invalid Cur ID');
            }

            $data = array(
                'IdProgram'         => $cur['external_id'],
                'IdProgramScheme'   => 0,
                'StudentCategory'   => $auth->getIdentity()->nationality == 'MY' ? 579 : 580,
                'IdSubject'         => $course['external_id'],
                'courses'            => $course['external_id']
            );

            $cart[] = array(
                'name'          => $course['title'],
                'description'   => $course['description'],
                'price'         => 0,
                'link'          => $this->view->baseUrl().'/courses/'.$course['code_safe'].'/pid/'.$course['curriculum_id'],
                'curriculum_id' => $course['curriculum_id'],
                'id'            => $course['id'],
                'regtype'       => 'course',
                'data'          => $data
            );

        }

        //friends
        $this->view->guests = $this->session->guests;

        //last check
        $this->view->cart = $cart;
        if ( empty($cart) )
        {
           return Cms_Render::error('Incomplete data for enrolment.');
        }
        else
        {
            $this->session->cart = $cart;
        }
        $programDB    = new App_Model_Smsconnect();
        $programGroup = $programDB->getProgramGroup();
        $programs     = $programGroup;

        foreach ($programGroup as $n => $progGroup) {
            $pGroup        = $progGroup['group_id'];
            $programDetail = $programDB->getProgramByGroup($pGroup);
            if ($programDetail) {
                $programs[$n]['program'] = $programDetail;
            }
        }
        
        $this->view->std_id   = $auth->getIdentity()->external_id;
        $this->view->programs = $programs;
    }

    public function applyExemptionAction() {
        $auth     = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();

        $i = $this->_getParam('i');
        $i = json_decode(encrypt($i, false), true);

        if(!isset($i['cid'])) {
            return Cms_Render::error('Invalid Course');
        }

        $curriculum_id = $i['cid'];

        if(!$curriculum_id) {
            return Cms_Render::error('Invalid Course');
        }
        
        $curriculum           = $cur = $this->currDb->fetchRow(array("id = ?" => $curriculum_id));
        $IdProgram            = $curriculum['external_id'];
        $IdSubject            = (isset($i['IdSubject']) ? $i['IdSubject'] : null);
        $IdStudentRegSubjects = (isset($i['IdStudentRegSubjects']) ? $i['IdStudentRegSubjects'] : null);

        if($this->getRequest()->isPost()) {
            $db       = getDB2();
            $formData = $this->getRequest()->getPost();
            $std_id   = $identity->external_id;
            
            $IdSubject = (isset($formData['IdSubject']) ? $formData['IdSubject'] : null);
            

            $db->beginTransaction();

            $params = array(
                'std_id'               => $std_id,
                'IdProgram'            => $IdProgram,
                'IdSubject'            => $IdSubject,
                'IdStudentRegSubjects' => $IdStudentRegSubjects,
                'reason'               => $formData['reason'],
            );

            if(isset($i['mode'])) {
                $params['mode'] = $i['mode'];
            }

            $response    = Cms_Curl::__(SMS_API . '/get/Registration/do/applyExemption', $params);
            $api_success = false;

            if(isset($response['success']) && $response['success']) {
                $exemption_id = $response['exemption_id'];
                $api_success  = true;
            }

            if(!$api_success) {
                return Cms_Render::error('Failed to process data');
            }

            $params     = array('std_id' => $std_id);
            $response   = Cms_Curl::__(SMS_API . '/get/Registration/do/getRepository', $params);
            $repository = encrypt($response['repository'], false);

            $folder       = $repository . '/attachment/';
            $dir          = DOCUMENT_PATH . $folder;
            $allow_upload = true;

            if (!is_dir($dir)) {
                if (mkdir($dir) === false) {
                    $allow_upload = false;
                }
            }

            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx,xls,xlsx', 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);
            $files = $adapter->getFileInfo();

            foreach ($files as $file => $info) {
                if(!$allow_upload) {
                    break;
                }

                if ($file && !$adapter->isValid()) {
                    $db->rollback();
                    throw New Exception('Failed to upload file. CODE:1');
                    exit;
                }
                elseif ($file && $adapter->isValid()) {
                    $fileOriName = $info['name'];
                    $fileRename  = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                    $filepath    = $info['destination'] . '/' . $fileRename;

                    if (!$adapter->isUploaded($file)) {
                        $db->rollback();
                        throw New Exception('Failed to upload file. CODE:2');
                        exit;
                    }

                    $adapter->addFilter('Rename', array('target' => $filepath, 'overwrite' => true), $file);

                    if ($adapter->receive($file) != 1) {
                        $db->rollback();
                        throw New Exception('Failed to upload file. CODE:3');
                        exit;
                    }

                    if(!is_file($filepath)) {
                        $db->rollback();
                        if(!move_uploaded_file($file['tmp'], $filepath)) {
                            throw New Exception('Failed to upload file. CODE:4');
                            exit;
                        }
                    }

                    $updFile = [];
                    $updFile["std_id"]                   = $std_id;
                    $updFile["IdStudentRegistration"]    = 0;
                    $updFile["upl_corporate_id"]         = 0;
                    $updFile["upl_filename"]             = $fileOriName;
                    $updFile["batch_id"]                 = 0;
                    $updFile["invoice_id"]               = 0;
                    $updFile["examregistration_main_id"] = 0;
                    $updFile["exemption_main_id"]        = $exemption_id;
                    $updFile["type"]                     = 3;
                    $updFile["upl_filerename"]           = $fileRename;
                    $updFile["upl_filelocation"]         = $folder . $fileRename;
                    $updFile["upl_filesize"]             = $info['size'];
                    $updFile["upl_mimetype"]             = $info['type'];
                    $updFile['created_iduser']           = $std_id;
                    $updFile["upl_upddate"]              = new Zend_Db_Expr('UTC_TIMESTAMP()');
                    $updFile["upl_upduser"]              = $std_id;
                    $updFile["status"]                   = 1;

                    $uploadid = $db->insert('tbl_student_upload', $updFile);
                }
            }

            if(isset($i['mode'])) {
                $params['mode'] = $i['mode'];

                if($i['mode'] == 'CS') {
                    $this->_helper->flashMessenger->addMessage(array('success' => 'Challenge Status application has been submitted'));
                }
                elseif($i['mode'] == 'FT') {
                    $this->_helper->flashMessenger->addMessage(array('success' => 'Fast Track application has been submitted'));
                }
                else {
                    $this->_helper->flashMessenger->addMessage(array('success' => 'Credit Transfer application has been submitted'));
                }
            }
            else {
                $this->_helper->flashMessenger->addMessage(array('success' => 'Credit Transfer application has been submitted'));
            }
            
            $db->commit();
            $this->_redirect($this->view->url(array('module'=> 'default', 'controller'=>'user', 'action'=>'dashboard'),'default',true));
        }

        $courses = array();

        if ($curriculum)
        {
            $curriculum = $curriculum->toArray();
            unset($curriculum['description']);

            $courses = $this->currCoursesDb->getData($curriculum_id);

            foreach($courses as $i => $row)
            {
                unset($courses[$i]['description']);
            }
        }

        $this->view->curriculum = $curriculum;
        $this->view->courses    = $courses;
        $this->view->IdSubject  = $IdSubject;
    }

    public function applyTimeExtendAction() {
        $auth     = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();

        $i = $this->_getParam('i');
        $i = json_decode(encrypt($i, false), true);

        if(!isset($i['cid'])) {
           // return Cms_Render::error('Invalid Course');
        }

        $curriculum_id = $i['cid'];
        $program_id = $i['idProgram'];

        if(!$curriculum_id) {
            //return Cms_Render::error('Invalid Course');
        }

        //$curriculum           = $cur = $this->currDb->fetchRow(array("id = ?" => $curriculum_id));
        $curriculum           = $cur = $this->currDb->fetchRow(array("external_id = ?" => $program_id));
        $this->view->curriculum = $curriculum;

        $dbStudentRegistration = new App_Model_StudentRegistration();
        $studentDetail = $dbStudentRegistration->getDataFromProfile($identity->external_id);
        $this->view->studentDetail = $studentDetail;
        //$identity->external_id

        $IdProgram            = $curriculum['external_id'];
        $IdSubject            = (isset($i['IdSubject']) ? $i['IdSubject'] : null);
        $IdStudentRegSubjects = (isset($i['IdStudentRegSubjects']) ? $i['IdStudentRegSubjects'] : null);

        /*$dbSmsExam = new App_Model_SmsExam();
        $courses = array();
        $coursesArray = array();

        if ($curriculum)
        {
            $curriculum = $curriculum->toArray();
            unset($curriculum['description']);

            $courses = $this->currCoursesDb->getData($curriculum_id);

            foreach($courses as $i => $row)
            {
                array_push($coursesArray,$courses[$i]['external_id']);

                unset($courses[$i]['description']);
            }
        }

        $registeredCourses = $dbSmsExam->getRegisteredExamList($identity->external_id,$coursesArray);

        $this->view->curriculum = $curriculum;
        $this->view->courses    = $courses;
        $this->view->registeredCourses    = $registeredCourses;
        $this->view->IdSubject  = $IdSubject;*/

        if($this->getRequest()->isPost()) {
            $db       = getDB2();
            $formData = $this->getRequest()->getPost();
            $std_id   = $identity->external_id;

            $IdStudentRegistration = $formData["IdStudentRegistration"];
//            pr($formData);exit;

            /*$arraydt = explode(":",$formData['IdSubject']);
            $arraydt[0];
            $arraydt[1];

            $IdRegSubject = (isset($arraydt[0]) ? $arraydt[0] : null);
            $IdSubject = (isset($arraydt[1]) ? $arraydt[1] : null);*/
//            $IdSubject = (isset($formData['IdSubject']) ? $formData['IdSubject'] : null);

            $updateExtensionTimeStudReg = array(
                'TimeExtension' => 1
            );
            $db->update('tbl_studentregistration', $updateExtensionTimeStudReg, array("IdStudentRegistration = $IdStudentRegistration"));

            $updateExtensionDetails = array(
                'IdStudentRegistration' => $IdStudentRegistration,
                'IdProgramExtend' => $formData['IdProgram'],
                'ReasonExtend' => addslashes(trim($formData['reason'])),
                'DateApplyExtend' => date('Y-m-d H:i:s')
            );
            $IdExtend = $db->insert('tbl_studentextendtime', $updateExtensionDetails);

            /*$db->beginTransaction();

            $params = array(
                'std_id'               => $std_id,
                'IdProgram'            => $IdProgram,
                'IdSubject'            => $IdSubject,
                'IdStudentRegSubjects' => $IdStudentRegSubjects,
                'reason'               => $formData['reason'],
            );

            if(isset($i['mode'])) {
                $params['mode'] = $i['mode'];
            }

            $response    = Cms_Curl::__(SMS_API . '/get/Registration/do/applyExemption', $params);
            $api_success = false;

            if(isset($response['success']) && $response['success']) {
                $exemption_id = $response['exemption_id'];
                $api_success  = true;
            }

            if(!$api_success) {
                return Cms_Render::error('Failed to process data');
            }

            $params     = array('std_id' => $std_id);
            $response   = Cms_Curl::__(SMS_API . '/get/Registration/do/getRepository', $params);
            $repository = encrypt($response['repository'], false);

            $folder       = $repository . '/attachment/';
            $dir          = DOCUMENT_PATH . $folder;
            $allow_upload = true;

            if (!is_dir($dir)) {
                if (mkdir($dir) === false) {
                    $allow_upload = false;
                }
            }

            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Extension', false, array('extension' => 'pdf,jpg,png,gif,zip,doc,docx,xls,xlsx', 'case' => false));
            $adapter->addValidator('NotExists', false, $dir);
            $adapter->setDestination($dir);
            $files = $adapter->getFileInfo();

            foreach ($files as $file => $info) {
                if(!$allow_upload) {
                    break;
                }

                if ($file && !$adapter->isValid()) {
                    $db->rollback();
                    throw New Exception('Failed to upload file. CODE:1');
                    exit;
                }
                elseif ($file && $adapter->isValid()) {
                    $fileOriName = $info['name'];
                    $fileRename  = date('YmdHis') . '_' . str_replace(' ', '', $fileOriName);
                    $filepath    = $info['destination'] . '/' . $fileRename;

                    if (!$adapter->isUploaded($file)) {
                        $db->rollback();
                        throw New Exception('Failed to upload file. CODE:2');
                        exit;
                    }

                    $adapter->addFilter('Rename', array('target' => $filepath, 'overwrite' => true), $file);

                    if ($adapter->receive($file) != 1) {
                        $db->rollback();
                        throw New Exception('Failed to upload file. CODE:3');
                        exit;
                    }

                    if(!is_file($filepath)) {
                        $db->rollback();
                        if(!move_uploaded_file($file['tmp'], $filepath)) {
                            throw New Exception('Failed to upload file. CODE:4');
                            exit;
                        }
                    }

                    $updFile = [];
                    $updFile["std_id"]                   = $std_id;
                    $updFile["IdStudentRegistration"]    = 0;
                    $updFile["upl_corporate_id"]         = 0;
                    $updFile["upl_filename"]             = $fileOriName;
                    $updFile["batch_id"]                 = 0;
                    $updFile["invoice_id"]               = 0;
                    $updFile["examregistration_main_id"] = 0;
                    $updFile["exemption_main_id"]        = $exemption_id;
                    $updFile["type"]                     = 3;
                    $updFile["upl_filerename"]           = $fileRename;
                    $updFile["upl_filelocation"]         = $folder . $fileRename;
                    $updFile["upl_filesize"]             = $info['size'];
                    $updFile["upl_mimetype"]             = $info['type'];
                    $updFile['created_iduser']           = $std_id;
                    $updFile["upl_upddate"]              = new Zend_Db_Expr('UTC_TIMESTAMP()');
                    $updFile["upl_upduser"]              = $std_id;
                    $updFile["status"]                   = 1;

                    $uploadid = $db->insert('tbl_student_upload', $updFile);
                }
            }

            if(isset($i['mode'])) {
                $params['mode'] = $i['mode'];

                if($i['mode'] == 'CS') {
                    $this->_helper->flashMessenger->addMessage(array('success' => 'Challenge Status application has been submitted'));
                }
                elseif($i['mode'] == 'FT') {
                    $this->_helper->flashMessenger->addMessage(array('success' => 'Fast Track application has been submitted'));
                }
                else {
                    $this->_helper->flashMessenger->addMessage(array('success' => 'Credit Transfer application has been submitted'));
                }
            }
            else {
                $this->_helper->flashMessenger->addMessage(array('success' => 'Credit Transfer application has been submitted'));
            }

            $db->commit();
            $this->_redirect($this->view->url(array('module'=> 'default', 'controller'=>'user', 'action'=>'dashboard'),'default',true));
            */

            Cms_Common::notify('success', 'Extension of Time is Updated');
            $this->_redirect($this->view->url(array('module'=> 'default', 'controller'=>'exam', 'action'=>'index'),'default',true));

        }
    }

    public function specialprogramAction() {
        $auth     = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();

        $encoded_i = $i = $this->_getParam('i');
        $i = json_decode(encrypt($i, false), true);

        if(!isset($i['cid'])) {
            return Cms_Render::error('Invalid Course');
        }

        $course_id = $i['cid'];

        if(!$course_id) {
            return Cms_Render::error('Invalid Course');
        }
        
        $course    = $cur = $this->currDb->fetchRow(array("id = ?" => $course_id));
        $IdProgram = $course['external_id'];

        $this->view->course    = $course;
        $this->view->encoded_i = $encoded_i;
        $this->view->data      = $i;
    }

    public function checkoutModedemoAction() {
        $auth     = Zend_Auth::getInstance();
        $paymentModeDB = new App_Model_Finance_PaymentMode();
        $orderDb       = new App_Model_Order();

        $paymentmodelist = $paymentModeDB->getPaymentModeList(1);
        $this->view->paymentmodelist    = $paymentmodelist;
        $userid = $this->_getParam('userid', null);
        $this->view->userid = $userid;

        $txnid = $this->_getParam('i', 0);
        $this->view->txnid = $txnid;

        $totalpay = $this->_getParam('totalpay', null);
        $this->view->totalpay = $totalpay;

        $progNameId = $this->_getParam('progNameId', null);
        $this->view->progNameId = $progNameId;

        $memid = $this->_getParam('memid', null);
        $this->view->memid = $memid;

//        $MembershipDB = new App_Model_Membership_Membership();
//        $registrationDetl = $MembershipDB->getStudReg("", $auth->sp_id, 1);
//
        $arrayData = array();
        $studRegDb = new App_Model_StudentRegistration();
        $studRegInfos = $studRegDb->getInfoStudentRegistration($txnid);
        if($studRegInfos){
            $invoiceinfo = $orderDb->getInvoiceInfo($studRegInfos["IdStudentRegistration"]);
            $this->view->invnum = $invoiceinfo["bill_number"];
            $this->view->amount = $invoiceinfo["bill_amount"];
        }

//
//        $memberDetl = $MembershipDB->getDataById("", $registrationDetl['IdStudentRegistration']);
//        $arrayData["membershipDetl"] = $memberDetl;
//
//        $memberProgramDetl = $MembershipDB->getMemberProgram($memberDetl['mr_idProgram']);
//        $arrayData["memberProgramDetl"] = $memberProgramDetl;
//
//        $memberProgramDetl["IdProgram"];
//        $feeProgramDetl = $MembershipDB->getFeeProgram($memberProgramDetl["IdProgram"]);
//        $arrayData["feeProgramDetl"] = $feeProgramDetl;

        $timestamp = new Zend_Db_Expr('NOW()');

        $this->view->arrayData = $arrayData;

        if ( $this->getRequest()->isPost() ) {
            $formData = $this->getRequest()->getPost();//pr($formData);
            $studRegInfo = $studRegDb->getInfoStudentRegistrationSp($formData["txnid"]);
            $idStudentRegistration = $studRegInfo["IdStudentRegistration"];
            $idProgram = $studRegInfo["IdProgram"];
            if($formData["payment_mode"] == '10'){
           // pr($studRegInfo);

                if ($formData["agreement"]=="on") {

                    /*$dataTxn = array(
                        'TransactionStatus' => 1603
                    );
                    $studRegDb->updateData($dataTxn,$idStudentRegistration);*/

                    if($studRegInfo['std_corporate_id']){
                        $corpId = $studRegInfo['std_corporate_id'];
                    }else{
                        $corpId = 35;
                    }

                    $dataSponsor = array(
                        'corporate_id' => $corpId,
                        'external_id' => $studRegInfo['sp_id'],
                        'sponsor_type' => 2,
                        'sponsor_program_id' => $idProgram,
                        'created_date' => $timestamp,
                        'created_by' => $studRegInfo['sp_id'],
                        'sponsor_amount' => $formData["payment_amount"],
                        'sponsor_balance' => $formData["payment_amount"],
                        'payment_mode' => 10,
                        'payment_mode_date' => $timestamp,
                        'status' => 1 //applied
                    );
                    $studRegDb->addRegSubjectsData('tbl_sponsor_registration',$dataSponsor);

                    Cms_Common::notify('success', 'Bank Sponsor have been submitted. Your bank sponsor will be approve soon.');
                    $this->redirect('/user/qualification');
                }


            }else{

                //exit;
                if ($formData["agreement"]=="on") {
                    //Update Transaction Status
                    $dataTxn = array(
                        'TransactionStatus' => 1607
                    );
                    $studRegDb->updateData($dataTxn,$idStudentRegistration);
                    $this->_redirect('/user/dashboard');
                }
            }

        }
    }

    public function checkoutModeAction() {
        $hash = $this->_getParam('i', null);
        $data = Cms_Common::encrypt($hash, false);
        $data = json_decode($data, true);
        $courseid = $this->_getParam('cid', null);
        $this->view->courseid    = $courseid;

        if ( !isset($data['token']) )
        {
            return Cms_Render::error('Invalid Session. Please try again.');
        }

        if ( Cms_Common::tokenCheck($data['token']) === false )
        {
            return Cms_Render::error('Invalid Session. Please try again.');
        }

        $orderDb       = new App_Model_Order();
        $paymentModeDB = new App_Model_Finance_PaymentMode();
        $currDb        = new App_Model_Curriculum();
        $courseDb      = new App_Model_Courses();
        $dataDb        = new Admin_Model_DbTable_ExternalData();
        $invoiceMainDb = new App_Model_InvoiceMain();
        $invoiceDb     = new App_Model_Finance_InvoiceMain();
        $erDb          = new App_Model_Exam_ExamRegistration();
        $auth          = Zend_Auth::getInstance();
        $lmsDb         = Zend_Db_Table::getDefaultAdapter();
        $db            = getDB2();
//echo "<pre>";print_r($auth->getIdentity());exit;
        $order_id = $data['order_id'];
        $params   = array('a.id = ?' => $order_id);
        $order    = $orderDb->getOrder($params, true);

        if (!$order && isset($data['er_id']) && $data['er_id'])
        {
            $select = $db->select()
                ->from(array('a' => 'exam_registration'))
                ->join(array('b' => 'exam_schedule'), 'b.es_id = a.examschedule_id', array('es_date'))
                ->where('a.er_id = ?', $data['er_id']);
            $exam = $db->fetchRow($select);

            if ($exam)
            {
                $er_main_id = $exam['er_main_id'];

                $select = $db->select()
                    ->from(array('a' => 'exam_registration'))
                    ->join(array('b' => 'exam_schedule'), 'b.es_id = a.examschedule_id', array('es_date'))
                    ->where('a.er_main_id = ?', $er_main_id);
                $exams  = $db->fetchAll($select);

                $paramProforma = array(
                    'IdSubject'             => 0,
                    'IdStudentRegistration' => $exam['student_id'],
                    'categoryId'            => ($auth->getIdentity()->nationality == 'MY') ? 579 : 580,
                    'totalStudent'          => 1,
                    'item'                  => 879, //exam,
                    'er_main_id'            => $er_main_id
                );
                $proformaInvoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateProformaIndividual', $paramProforma);

                if (!isset($proformaInvoice['error'])) {
                    return Cms_Render::error('Failed to generate proforma invoice. #1');
                }

                if (isset($proformaInvoice['error']) && $proformaInvoice['error'] == 1) {
                    return Cms_Render::error('Failed to generate proforma invoice. #2');
                }

                $paramsInvoice = array('ProformaId' => $proformaInvoice['id'], 'creatorId' => 665, 'from' => 1, 'returnCurrency' => 1);
                $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoice', $paramsInvoice);

                if (!isset($invoice['error'])) {
                    return Cms_Render::error('Failed to generate invoice. #1');
                }

                if (isset($invoice['error']) && $proformaInvoice['error'] == 1) {
                    return Cms_Render::error('Failed to generate invoice. #2');
                }

                $invoiceId     = $invoice['invoiceId'];
                $invoiceNo     = $invoice['invoiceNo'];
                $currencyId    = $invoice['currencyId'];
                $invoiceAmount = $invoice['amount'];

                $select     = $lmsDb->select()->from(array('a' => 'curriculum'))->where('a.external_id = ?', $exam['IdProgram'])->where('a.parent_id = ?', 0);
                $curriculum = $lmsDb->fetchRow($select);

                $examschedule_id = array();
                foreach ($exams as $row)
                {
                    $examschedule_id[] = $row['examschedule_id'];
                }


                if($auth->getIdentity()->login_as){//if admin
                    $ordercreateby = $auth->getIdentity()->login_as;
                }else{
                    $ordercreateby = $auth->getIdentity()->id;
                }

                $order = array(
                    'user_id'          => $auth->getIdentity()->id,
                    'created_date'     => new Zend_Db_Expr('UTC_TIMESTAMP()'),
//                    'created_by'       => $auth->getIdentity()->id,
                    'created_by'       => $ordercreateby,
                    'amount'           => $invoiceAmount,
                    'paymentmethod'    => 'MIGS',
                    'status'           => 'PENDING',
                    'invoice_id'       => $invoiceId,
                    'invoice_no'       => $invoiceNo,
                    'invoice_external' => $invoiceId,
                    'regtype'          => 'exam',
                    'curriculum_id'    => $curriculum['id'],
                    'item_id'          => $er_main_id,
                    'learningmode'     => 'OL',
                    'schedule'         => implode(',', $examschedule_id),
                    'external_id'      => $auth->getIdentity()->external_id
                );
                $order_id = $orderDb->insert($order);
                $invoiceDb->updateData(array('order_id' => $order_id), $invoiceId);
                foreach ($exams as $row)
                {
                    $update_id = $row['er_id'];
                    $erDb->updateData(array('order_id'=>$order_id), $update_id, 'New registration - assign order_id (LMS)');
                }

                $params   = array('a.id = ?' => $order_id);
                $order = $orderDb->getOrder($params, true);
            }
        }

        if ( !$order ) {
            return Cms_Render::error('Invalid Order. Please try again.');
        }

        if( $order['status'] != 'PENDING' ) {
            return Cms_Render::error('Invalid Order Status. Please try again.');
        }

        $total_amount = $order['amount'];
        $invoice_id   = $order['invoice_id'];
        $invoice_no   = $order['invoice_no'];
        $invoice      = $invoiceMainDb->getData($invoice_id, false, true);
        $total_amount = $invoice['bill_balance'];
        $IdProgram    = $order['IdProgram'];

        if( $order['status'] != 'PENDING' ) {
            return Cms_Render::error('Invalid Invoice. Please try again.');
        }

        if ($order['regtype'] == 'exam')
        {
            $error_slot = false;

            $select = $db->select()
                ->from(array('a' => 'exam_registration'))
                ->join(array('b' => 'exam_schedule'), 'b.es_id = a.examschedule_id', array('es_date'))
                ->where('a.order_id = ?', $order_id);
            $exams  = $db->fetchAll($select);

            foreach ($exams as $i => $row)
            {
                $exam_date       = $row['es_date'];
                $exam_date_unix  = strtotime($exam_date);
                $today_date      = date('Y-m-d');
                $today_date_unix = strtotime($today_date);
                $limit_date      = date('Y-m-d', strtotime('-3 days', $exam_date_unix));
                $limit_date_unix = strtotime($limit_date);

                if ($today_date_unix >= $limit_date_unix)
                {
                    //$this->_helper->flashMessenger->addMessage(array('error' => 'You are not allowed to make payment within 3 days before exam'));
                    $this->_redirect('/exam/view-exam-detail/id/'. $row['er_id']);
                }

                $params   = array(
                    'examschedule_id'    => $row['examschedule_id'],
                    'examcenter_id'      => $row['examcenter_id'],
                    'examtaggingslot_id' => $row['examtaggingslot_id']
                );
                $response = Cms_Curl::__(SMS_API . '/get/Examination/do/getRoom', $params);

                if (!isset($response['examroom_id']))
                {
                    return Cms_Render::error('System\'s Examination API is down. Please try again later.');
                }

                $examroom_id = $response['examroom_id'];
                $update      = array();

                if ($examroom_id == -1)
                {
                    $error_slot = true;

                    if (!$row['must_reschedule'])
                    {
                        $update['must_reschedule'] = 1;
                        $erDb->updateData($update, $row['er_id'], 'Set must_reschedule = 1 (LMS)');
                    }
                }
                else if ($examroom_id > 0 && $row['must_reschedule'])
                {
                    $update['must_reschedule'] = 0;
                    $erDb->updateData($update, $row['er_id'], 'Set must_reschedule = 0 (LMS)');
                }
            }

            if ($error_slot)
            {
                return Cms_Render::error('Exam slot is full. Please reschedule your exam');
            }

        }

        if ( $this->getRequest()->isPost() ) {
            $formData          = $this->getRequest()->getPost();
            $payment_mode_id   = $formData['payment_mode'];
            $payment_mode      = $paymentModeDB->getData($payment_mode_id);
            $payment_mode_name = $payment_mode['payment_mode'];
            $timestamp         = new Zend_Db_Expr('NOW()');

            // 1. update invoice
            $db->update('invoice_main', array('payment_mode' => $payment_mode_id, 'payment_mode_date' => $timestamp), array("id = $invoice_id"));

            // 2. update order
            $orderDb->update(array('paymentmethod' => $payment_mode_name), array("id = $order_id"));

            if ( $payment_mode_id == 6 ) // MIGS
            {

                $bill_balance = $invoice['bill_balance'];
                if($invoice['currency_id'] != 1){//MYR
                    $params         = array('amount' => $invoice['bill_balance'], 'currency_id' => $invoice['currency_id']);
                    $payment_amount = Cms_Curl::__(SMS_API . '/get/Payment/do/getAmount', $params);
                    $bill_balance = $payment_amount['amount'];
                }

                $params = array(
                    'token'            => Cms_Common::token('ibfimpay'),
                    'invoiceId'        => $invoice_id,
                    'requestFrom'      => 1,
                    'currencyId'       => $invoice['currency_id'],
                    'invoiceNo'        => $invoice['bill_number'],
                    'totalAmount'      => Cms_Common::MigsAmount($bill_balance),
                    'die'              => 1,
                    'order_id'         => $order_id,
                    'registrationItem' => $invoice['registration_item'],
                    'program'          => $IdProgram
                );
                $urlMigs = Cms_Curl::__(URL_PAYMENT . '/migs/', $params);
                header('location: ' . $urlMigs);
            }
            elseif ( $payment_mode_id == 5 )
            {
                if ($invoice['currency_id'] != 1)
                {
                    $params                  = array('amount' => $invoice['bill_balance'], 'currency_id' => $invoice['currency_id']);
                    $payment_amount          = Cms_Curl::__(SMS_API . '/get/Payment/do/getAmount', $params);
                    $invoice['bill_balance'] = $payment_amount['amount'];
                }

                $invoices    = array();
                $invoiceNo   = array();
                $totalAmount = array();

                $invoices[]    = $invoice['id'];
                $invoiceNo[]   = $invoice['bill_number'];
                $totalAmount[] = $invoice['bill_balance'];

                $params = array(
                    'token'            => Cms_Common::token('ibfimpay'),
                    'invoiceId'        => $invoices,
                    'totalAmount'      => $totalAmount,
                    'requestFrom'      => 1, // LMS
                    'registrationItem' => $invoice['registration_item'],
                    'order_id'         => $order_id,
                    'program'          => $IdProgram
                );
                $token = encrypt(json_encode($params));
                $this->_redirect(URL_PAYMENT .'/fpx?session='. $token);
            }
            elseif ($payment_mode_id == -999)
            {
                if ($invoice['currency_id'] != 1)
                {
                    $params                  = array('amount' => $invoice['bill_balance'], 'currency_id' => $invoice['currency_id']);
                    $payment_amount          = Cms_Curl::__(SMS_API . '/get/Payment/do/getAmount', $params);
                    $invoice['bill_balance'] = $payment_amount['amount'];
                }

                $invoices    = array();
                $invoiceNo   = array();
                $totalAmount = array();

                $invoices[]    = $invoice['id'];
                $invoiceNo[]   = $invoice['bill_number'];
                $totalAmount[] = $invoice['bill_balance'];

                $params = array(
                    'token'            => Cms_Common::token('ibfimpay'),
                    'invoiceId'        => $invoices,
                    'totalAmount'      => $totalAmount,
                    'requestFrom'      => 1, // LMS
                    'registrationItem' => $invoice['registration_item'],
                    'order_id'         => $order_id,
                    'PAYMENT_TESTING'  => 1,
                    'program'          => $IdProgram
                );
                $token = encrypt(json_encode($params));
                $this->_redirect(URL_PAYMENT .'/fpx?session='. $token);
            }
            elseif ( $payment_mode_id == 8 )
            {
                $this->_redirect('/user/dashboard');
            }

                // kalau takde, throw error la. sbb invalid payment mode
            return Cms_Render::error('Invalid Payment Mode. Please try again.');

        }

        $paymentmodelist = $paymentModeDB->getPaymentModeList(1);

        foreach ( $paymentmodelist as $i => $row )
        {
            $paymentmodelist[$i]['fees_display_amount'] = 0;
            $paymentmodelist[$i]['totalAmount'] = $total_amount;
            $paymentmodelist[$i]['cur_code']    = $invoice['cur_code'];

            if ( $row['fees_type'] == 1 )
            {
                $paymentmodelist[$i]['fees_display_amount'] = $row['fees_amount'];
            }
            elseif ( $row['fees_type'] == 2 )
            {
                $paymentmodelist[$i]['fees_display_amount'] = $total_amount * $row['fees_percentage'] / 100;
            }

            $paymentmodelist[$i]['net_amount_with_fees'] = $total_amount + $paymentmodelist[$i]['fees_display_amount'];
        }

        $program = array();

        $db = Zend_Db_Table::getDefaultAdapter();
        if ($order['regtype'] == 'exemption')
        {
            $smsDb  = getDB2();
            $select = $smsDb->select()
                ->from(array('a' => 'exemption_main'))
                ->where('a.id = ?', $order['external_id']);
            $exemption = $smsDb->fetchRow($select);

            $select = null;

            if (in_array($exemption['mode'], array('CS', 'FT')))
            {
                $select = $db->select()
                    ->from(array('a' => 'curriculum'), array(
                        'ProgramName' => 'name',
                        'ProgramCode' => 'code',
                        'IdProgram'   => 'external_id'
                    ))
                    ->where('a.id = ?', $order['item_id']);
            }
            else
            {
                $select = $db->select()
                    ->from(array('a' => 'courses'), array())
                    ->join(array('b' => 'curriculum_courses'), 'b.course_id=a.id', array())
                    ->join(array('c' => 'curriculum'), 'c.id=b.curriculum_id', array(
                        'ProgramName' => 'name',
                        'ProgramCode' => 'code',
                        'IdProgram'   => 'external_id'
                    ))
                    ->where('a.id = ?', $order['item_id']);
            }

            $program = $db->fetchRow($select);
        }
        else
        {
            $db2 = getDB2();

            if($order['IdProgram']) {
                $select = $db2->select()
                    ->from(array('a' => 'tbl_program'))
                    ->where('a.IdProgram = ?', $IdProgram);
                $program = $db2->fetchRow($select);
            }
        }


        $this->view->order           = $order;
        $this->view->order_id        = $order_id;
        $this->view->invoice         = $invoice;
        $this->view->invoice_id      = $invoice_id;
        $this->view->invoice_no      = $invoice_no;
        $this->view->paymentmodelist = $paymentmodelist;
        $this->view->program         = $program;
    }

    public function skippaymentAction(){

        $enrolDb = new App_Model_Enrol();
        $orderDb = new App_Model_Order();
        $studRegDb = new App_Model_StudentRegistration();
        $auth          = Zend_Auth::getInstance();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            $studRegSubj = $studRegDb->getStudentSubjectsInfo($formData["IdStudentRegistration"]);
            //$getStudRegSubjectsId =
            $invoiceid = $formData["invoice_id"];
            $mode_of_learning = 'OL';
            $registration['IdStudentRegSubjects'] = $studRegSubj["IdStudentRegSubjects"];
            $schedule = 0;
            $item['type'] = 0;
            $userid = $auth->getIdentity()->id;
            $lms_course['id'] = $formData["course_id"];

            $enrol = array(
                'user_id' => $userid,
                'course_id' => $lms_course['id'],
                'batch_id' => 0,
                'curriculum_id' => 0,
                'regtype' => 'course',
                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                'created_by' => 1,
                'haspayment' => 0,
                'completed' => ($item['type'] == 1 ? 1 : 0),
                'completed_date' => ($item['type'] == 1 ? new Zend_Db_Expr('UTC_TIMESTAMP()') : null),
                'invoice_id' => $invoiceid,
                'order_id' => 0,//$formData["order_id"],
                'learningmode' => $mode_of_learning,
                'schedule' => $schedule,
                'external_id' => $registration['IdStudentRegSubjects'],
                'expiry_date' => null,
                'active' => 1
            );//pr($enrol);
            $enrolDb->saveEnrol($enrol);


            //update order pending to approved
            $orderData = array(
                'status' => 'ACTIVE')
            ;
            $orderDb->update($orderData,"invoice_id =".$invoiceid);
            //$enrolDb->insert('enrol', $enrol);
           // exit;
            $this->_redirect('/user/dashboard');
        }

    }

    public function autopaidAction () {
        $auth       = Zend_Auth::getInstance();
        $identity   = $auth->getIdentity();
        $birth_date = $identity->birthdate;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            /*echo "#####<pre>";
            print_r($formData);*/

            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->addActionContext('view', 'html');
            $ajaxContext->addActionContext('view', 'html')
                ->addActionContext('form', 'html')
                ->addActionContext('process', 'json')
                ->initContext();

            $result[] = array(
                'returnid' => "a"
            );

            $json = Zend_Json::encode($result);
            echo $json;
            exit;

        }
    }


   
}
