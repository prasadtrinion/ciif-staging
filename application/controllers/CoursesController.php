<?php

ini_set('display_errors','Off');

class CoursesController extends Zend_Controller_Action
{
    protected $courseDb;
    protected $contentblockDb;
    protected $blockDataDb;
    protected $enrolDb;
    protected $currDb;
    protected $sidebar  = 1;
    protected $currCoursesDb;
    protected $uploadFolder;
    protected $uploadDir;

    public function init()
    {

        $this->courseDb = new App_Model_Courses();
        $this->coursesDb = new App_Model_Courses();
        $this->contentBlockDb = new App_Model_ContentBlock();
        $this->blockDataDb = new App_Model_ContentBlockData();
        $this->enrolDb = new App_Model_Enrol();
        $this->currDb = new App_Model_Curriculum();
        $this->currCoursesDb = new App_Model_CurriculumCourses();
        $auth = Zend_Auth::getInstance();
        $this->auth = $auth;
        //$this->uploadFolder = 'assignments';
        //$this->uploadDir = DOCUMENT_PATH. '/files/'.$this->uploadFolder;
        $this->uploadDir = Cms_System_Links::dataFile();
    }

    public static function userSidebar($active = 'dashboard')
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view->active = $active;
        $view->setBasePath(APPLICATION_PATH . '/views/');
        return $view->render('plugins/usersidebar.phtml');
    }

	public function indexAction()
    {

        if ( Cms_Options::__('catalog_curriculum') == 1 )
        {
            $cid = $this->_getParam('cid',0);

            $this->sidebar = 0;
            $curr = $this->currDb->fetchAll(array('visibility = 1'));

            $curriculum = $currById =array();
            //currbyparent
            foreach ( $curr as $row )
            {
                $curriculum[ $row['parent_id'] ][] = $row;
                $currById[ $row['id'] ] = $row;
            }

            $this->view->currById = $currById;
            $this->view->cid = $cid;
            $this->view->results = isset($curriculum[$cid]) ? $curriculum[$cid] : null;

            if ( $cid > 0 )
            {

                $bc = array(
                                'home',
                                array($this->view->baseUrl().'/courses' => $this->view->translate('Modules')),
                                $currById[$cid]['name']
                );

                Cms_Hooks::push('breadcrumbs',Cms_Breadcrumbs::__($bc));

                //courses
                $getcourses = $this->currCoursesDb->getData($cid, false);
                $courses = Cms_Paginator::query($getcourses);
                $this->view->courses = $courses;
            }

        }
        else
        {
            $getcourses = $this->courseDb->select()->where('visibility != ?', 0);
            $courses = Cms_Paginator::query($getcourses);
            $this->view->results = $courses;
        }

        $this->view->sidebar = $this->sidebar;


    }

    public function curriculumRegisterAction()
    {
        $studentRegDb = new App_Model_StudentRegistration();

        $auth      = Zend_Auth::getInstance();
        $identity  = $auth->getIdentity();

        $userid = $this->auth->getIdentity()->id;
        $this->view->userid = $userid;

        $cid = $this->getParam('cid');
        $txnid = $this->getParam('txnid');

        $idStudentRegistration = $studentRegDb->getInfoStudentRegistration($txnid);

        $this->view->txnid = $txnid;
        $this->view->idStudentRegistration = $idStudentRegistration["IdStudentRegistration"];

        $smsStudentRegistrationDB = new App_Model_StudentRegistration();
        $LandscapeSubjectDB = new App_Model_LandscapeSubject();
        $StudentExemptionDB = new App_Model_StudentExemption();

        $StudentSubjectsDetail = $smsStudentRegistrationDB->getStudentSubjectsDetail($idStudentRegistration["sp_id"]);
        $this->view->StudentSubjectsDetail = $StudentSubjectsDetail;
        

        $totalElective = $LandscapeSubjectDB->getRequirementCourseByProgram($idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"],275);//get elective subjects
        $totalCore = $LandscapeSubjectDB->getRequirementCourseByProgram($idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"],394);//get core subjects

        $totalElectivePaper = (isset($totalElective[0]["CreditHours"])) ? $totalElective[0]["CreditHours"]:0;
        $totalCorePaper = (isset($totalCore[0]["CreditHours"])) ? $totalCore[0]["CreditHours"]:0;

        $pathwaySubjectListElective = $LandscapeSubjectDB->getLandscapeCourseByProgram($idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"],275);//get elective subjects
        $pathwaySubjectListCore = $LandscapeSubjectDB->getLandscapeCourseByProgram($idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"],394);//get core subjects

        $arraySubjectProgramCore = array();
        foreach ($pathwaySubjectListCore as $psc) {
            array_push($arraySubjectProgramCore, $psc["IdSubject"]);
        }

        if ($totalElectivePaper != count($pathwaySubjectListElective)) {
            $arraySubjectProgramElective = array();
            foreach ($pathwaySubjectListElective as $pse) {

                $getStudentExemptedCourse = $StudentExemptionDB->getDataStud($idStudentRegistration["IdStudentRegistration"],$idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"],$pse["IdSubject"]);
                if ($getStudentExemptedCourse) {
                    array_push($arraySubjectProgramElective, $pse["IdSubject"]);
                }
            }
        } else {
            $arraySubjectProgramElective = array();
            foreach ($pathwaySubjectListElective as $pse) {
                array_push($arraySubjectProgramElective, $pse["IdSubject"]);
            }
        }

        $arraySubjectProgram = array_merge($arraySubjectProgramCore,$arraySubjectProgramElective);

        /*$arraySubjectProgram = array();
        foreach ($pathwaySubjectList as $ps) {
            array_push($arraySubjectProgram, $ps["IdSubject"]);
        }

        pr($arraySubjectProgram);*/

        /*$pathwaySubjectList = $LandscapeSubjectDB->getLandscapeCourseByProgram($idStudentRegistration["IdProgram"],$idStudentRegistration["IdMajoringPathway"]);
        $this->view->pathwaySubjectList = $pathwaySubjectList;

        $arraySubjectProgram = array();
        foreach ($pathwaySubjectList as $ps) {
            array_push($arraySubjectProgram, $ps["IdSubject"]);
        }

        pr($arraySubjectProgram);*/

        $cur = $this->currDb->fetchRow(array("id = ?" => $cid));

        if ($this->getRequest()->isPost())
        {//echo "here".$idStudentRegistration;exit;
            $idStudentRegistration = $idStudentRegistration["IdStudentRegistration"];
            $formData = $this->getRequest()->getPost();

            //Update Transaction Status
            $dataTxn = array(
                'TransactionStatus' => 1603,
            );
            $studentRegDb->updateData($dataTxn,$idStudentRegistration);
            $this->redirect('/dashboard');

        }else{
          //  echo "he2222re";exit;
        }

        if ( empty($cur)  )
        {
            return Cms_Render::error(Cms_Options::__('label_curriculum').' doesn\'t existsssss.');
        }
        else
        {
            $cur = $cur->toArray();
            $cur['parent_external_id'] = 0;

            if($cur['parent_id']) {
                $parent = $this->currDb->fetchRow(array("id = ?" => $cur['parent_id']));
                $cur['parent_external_id'] = $parent['external_id'];
            }

        }
        //canview
        if ( $cur['visibility'] == 0 && Zend_Auth::getInstance()->getIdentity()->role != 'administrator' )
        {
            return Cms_Render::error(Cms_Options::__('label_curriculum').' is not available at the moment.');
        }

        //breadcrumbs
        $bc = array(
            'home',
            array($this->view->baseUrl().'/courses' => $this->view->translate('Modules')),
            $cur['name']
        );

        Cms_Hooks::push('breadcrumbs',Cms_Breadcrumbs::__($bc));

        //courses
        //$getcourses = $this->currCoursesDb->getData($cid);
        $getcourses = $this->currCoursesDb->getData2($cid,$arraySubjectProgram);

        //pr ($getcourses);

        $x=0;
        foreach ($getcourses as $gcourse) {
            $gcourse["external_id"];
            //get data from tbl_studentexemption
            $exemptionInfo2 = $StudentExemptionDB->getDataStud($idStudentRegistration['IdStudentRegistration'], $idStudentRegistration['IdProgram'], $idStudentRegistration['IdMajoringPathway'],$gcourse["external_id"]);
            $getcourses[$x]["exemptionstatus"] = $exemptionInfo2;
            $x++;
        }

//        pr($getcourses);
        //get data from tbl_studentexemption
        $exemptionInfo = $StudentExemptionDB->getDataByStudRegID($idStudentRegistration['IdStudentRegistration'], $idStudentRegistration['IdProgram'], $idStudentRegistration['IdMajoringPathway']);
        $this->view->exemptionInfo = $exemptionInfo;//pr($exemptionInfo);

        $this->view->courses = $getcourses;
        $this->view->cur = $cur;

        //hasenrolled
        $hasenrolled  = false;

        if ( Zend_Auth::getInstance()->hasIdentity() )
        {
            $check = $this->enrolDb->getEnrol(array('user_id = ?' => Zend_Auth::getInstance()->getIdentity()->id, 'regtype = ?' => 'curriculum', 'curriculum_id = ?' => $cid));
            $hasenrolled = !empty($check) ? true : false;
        }

        Cms_Hooks::push('body.bottom', Cms_Render::login());
        $this->view->hasenrolled = $hasenrolled;
        $this->view->cur = $cur;
        $this->view->cid = $cid;
    }

    /**
     * Curriculum page
     *
     */
    public function curriculumAction()
    {
        $cid = $this->getParam('cid');

        $cur = $this->currDb->fetchRow(array("id = ?" => $cid));

        if ( empty($cur)  )
        {
            return Cms_Render::error(Cms_Options::__('label_curriculum').' doesn\'t exist111.');
        }
        else
        {
            $cur = $cur->toArray();
            $cur['parent_external_id'] = 0;

            if($cur['parent_id']) {
                $parent = $this->currDb->fetchRow(array("id = ?" => $cur['parent_id']));
                $cur['parent_external_id'] = $parent['external_id'];
            }
            
        }
        //canview
        if ( $cur['visibility'] == 0 && Zend_Auth::getInstance()->getIdentity()->role != 'administrator' )
        {
            return Cms_Render::error(Cms_Options::__('label_curriculum').' is not available at the moment.');
        }

        //breadcrumbs
        $bc = array(
            'home',
            array($this->view->baseUrl().'/courses' => $this->view->translate('Modules')),
            $cur['name']
        );

        Cms_Hooks::push('breadcrumbs',Cms_Breadcrumbs::__($bc));

        //courses
        $getcourses = $this->currCoursesDb->getData($cid);

        $this->view->courses = $getcourses;
        $this->view->cur = $cur;

        //hasenrolled
        $hasenrolled  = false;

        if ( Zend_Auth::getInstance()->hasIdentity() )
        {
            $check = $this->enrolDb->getEnrol(array('user_id = ?' => Zend_Auth::getInstance()->getIdentity()->id, 'regtype = ?' => 'curriculum', 'curriculum_id = ?' => $cid));
            $hasenrolled = !empty($check) ? true : false;
        }

        Cms_Hooks::push('body.bottom', Cms_Render::login());
        $this->view->hasenrolled = $hasenrolled;
        $this->view->cur = $cur;
        $this->view->cid = $cid;
    }


    /**
     * View single course
     */
    public function viewAction()
    {
        $course_id = $this->getParam('course_id');
        $cid = $this->getParam('pid');
        $canenroll = true;

        //$course = $this->courseDb->fetchRow(array("code_safe = ?" => $course_id))->toArray();
        $course = $this->courseDb->getCourse(array("code_safe = ?" => $course_id));

        if ( empty($course)  )
        {
//            return Cms_Render::error('Course doesn\'t exist12345.');
            $this->registerModulesAction();
        }
    
        $curriculum_id = $course['curriculum_id'];
        $curriculum    = $this->currDb->getCurriculum(array('id = ?' => $curriculum_id));

        if ( !Cms_System_Course::canView($course) )
        {
            return Cms_Render::error('You don\'t have permission to view this course');
        }

        if ( $cid != '' )
        {
            $cur = $this->currDb->fetchRow(array('id = ?' => $cid))->toArray();
            $this->view->cur = $cur;

            $bc = array(
                'home',
                array($this->view->baseUrl().'/courses' => $this->view->translate('Courses')),
                array($this->view->baseUrl().'/courses/index/cid/'.$cid => $cur['name']),
                $course['title']
            );

            Cms_Hooks::push('breadcrumbs',Cms_Breadcrumbs::__($bc));
            Cms_Hooks::push('body.bottom', Cms_Render::login());

            if ( $cur['allow_course_registration'] != 1)
            {
                $canenroll = false;
            }
        }

        //hasenrolled
        $hasenrolled  = false;


        if ( Zend_Auth::getInstance()->hasIdentity() )
        {
            $check = $this->enrolDb->getEnrol(array('user_id = ?' => Zend_Auth::getInstance()->getIdentity()->id, 'regtype = ?' => 'course', 'course_id = ?' => $course['id']));

            $hasenrolled = !empty($check) ? true : false;
        }

        //blocks
        $blocks = $this->contentBlockDb->fetchAll()->toArray();

        //block content
        $content = $this->blockDataDb->getByBlock($course['id']);

        $this->view->cid = $cid;
        $this->view->blocks = $blocks;
        $this->view->content = $content;
        $this->view->course  = $course;
        $this->view->hasenrolled = $hasenrolled;
        $this->view->canenroll = $canenroll;
        $this->view->curriculum = $curriculum;
    }

    /**
     * start learning.
     */

    public function learnAction()
    {
    
        $auth      = Zend_Auth::getInstance();
        $identity  = $auth->getIdentity();
        $course_id = $this->getParam('course_id');

		//print_r($course_id);exit;

        $course = $this->courseDb->getCourse(array("code_safe = ?" => $course_id), true);
        
        $curriculum_id = $course['curriculum_id'];
        $canView       = true;

        if ( empty($course)  )
        {
            return Cms_Render::error('Course doesn\'t exist7890.');
        }
        
        $curriculum = $this->currDb->getCurriculum(array('id = ?' => $curriculum_id));

        if ( !Cms_System_Course::canView($course) )
        {
            //return Cms_Render::error('You don\'t have permission to view this course');
            $canView = false;
        }
        
        if (in_array($curriculum_id, $identity->access_content))
        {
            $canView = true;
        }
    
        if ( !$canView )
        {
            return Cms_Render::error('You don\'t have permission to view this course');
        }

        if ( !Cms_System_Course::canEdit($course) && Zend_Auth::getInstance()->getIdentity()->role != 'student' )
        {
            if ( Cms_Permission::allowed('courses.edit') === false )
            {
                return Cms_Render::error('You don\'t have permission to view this course');
            }
        }

        //hasenrolled active enrolment
        $hasenrolled  = false;
        $check = $this->enrolDb->getEnrol(array('user_id = ?' => Zend_Auth::getInstance()->getIdentity()->id, 'course_id = ?' => $course['id'],'active = ?' => 1));
        $hasenrolled = !empty($check) ? true : false;
    
        if (in_array($curriculum_id, $identity->access_content))
        {
            $hasenrolled = true;
        }

        if ( $hasenrolled == false )
        {
            if ( Cms_Permission::allowed('courses.edit') === false ) {
                return Cms_Render::error('You don\'t have a permission to view this course.');
            }
        }

        //expiry
        if ( $check['expiry_date'] != null && date('Y-m-d') > $check['expiry_date'] )
        {
            return Cms_Render::error('You no longer have access to this course.');
        }

        //blocks
        $blocks = $this->contentBlockDb->getBlocks(array('course_id = ?' => $course['id']));

        //block content
        $content = $this->blockDataDb->getByBlock($course['id']);

        // bookmarking & grouping
        $bookmarkDb = new App_Model_Bookmark();
        $groupingDb = new App_Model_UserGroupMapping();
        $groupDataDb = new App_Model_UserGroupData();

        // get current user group for this course
        $user_group = $groupDataDb->getUserGroup(Zend_Auth::getInstance()->getIdentity()->id, $course['id']);

        foreach($content as $k => $c)
        {
            foreach ($c as $key => $val)
            {
                $content[$k][$key] = $content[$k][$key]->toArray();
                // content bookmarking
                $bookmark = $bookmarkDb->getBookmarkByDataId($val['data_id'], Zend_Auth::getInstance()->getIdentity()->id);


                if ($bookmark["id"] && $content[$k][$key]) 

                {
                    try {
                        $content[$k][$key]["bookmark"] = $bookmark["id"];
                    } catch (Exception $e) { }
                }

                // check content grouping
                if (!$groupingDb->checkContentPermission($val['data_id'], $course['id'], "content") && !is_null($user_group['group_id'])) // the content is tagged to a group
                {
                    // content grouping
                    $group = $groupingDb->getContentGroup($val['data_id'], $course['id'], $user_group['group_id'], 'content'); // check against the learner's group

                    if (empty($group)) {
                        // it is tagged to other group. not the learner. unset the var so it will not be displayed
                        unset($content[$k][$key]);
                        continue;
                    }

                    // tag the content to the group
                    $content[$k][$key]["specific_group"] = $group["group_id"];
                    $content[$k][$key]["specific_group_name"] = $group['group_name'];
                }
                else
                {
                    // it is tagged for all. no checking needed. moving on
                }
            }
        }

        $userScormDB        = new App_Model_UserScorm();
        $user_id            = Zend_Auth::getInstance()->getIdentity()->id;
        $activity_count     = 0;
        $activity_completed = 0;
        $progress           = 0;

        foreach($content as $i => $sub_content) {
            foreach($sub_content as $sub_i => $row) {
                if($row['data_type'] == 'scorm') {
                    $activity_count++;

                    $row['completed'] = 0;
                    $scorm_id  = $row['data_id'];

                    $completed = false;

                    if($check) {
                        $completed = $userScormDB->checkForCompleted($user_id, $scorm_id);
                    }

                    if($completed) {
                        $activity_completed++;
                        $row['completed'] = 1;
                    }

                    $content[$i][$sub_i] = $row;
                }
            }
        }
        
        if (!$activity_count) {
            $progress = 0;
        }
        else {
            $progress = $activity_completed / $activity_count;
            $progress = $progress * 100;
            $progress = round($progress, 2);
        }

        // $progress = 100;

        if($check && $progress != $check['activity_progress'] && $check['activity_progress'] != 100) {
            $check['activity_progress'] = $progress;
            $this->enrolDb->updateProgress($check['id'], $progress, $check['external_id']);
        }

        $this->view->progress = $progress;

        $this->view->blocks = $blocks;
        $this->view->content = $content;
        $this->view->course  = $course;
        $this->view->hasenrolled = $hasenrolled;
        $this->view->check = $check;
        $this->view->curriculum = $curriculum;
    }

    public function blockAction()
    {
        $auth      = Zend_Auth::getInstance();
        $identity  = $auth->getIdentity();
        
        $islearner = false;

        if ( Cms_Permission::allowed('courses.edit') === true )
        {
            $islearner = true;
        }

        $id = $this->_getParam('id');

        $blockcontent = $this->blockDataDb->fetchRow(array('data_id = ?' => $id));

        if ( empty($blockcontent) )
        {
            throw new Exception('Invalid Content');
        }

        $blockcontent = $blockcontent->toArray();

        $block = $this->contentBlockDb->fetchRow(array("id = ?" => $blockcontent['block_id']))->toArray();
        $course = $this->coursesDb->getCourse(array("a.id = ?" => $blockcontent['course_id']));

        if ( empty($block) )
        {
            throw new Exception('Invalid Block');
        }

        if ( empty($course) )
        {
            throw new Exception('Invalid Course');
        }
        
        $curriculum_id = $course['curriculum_id'];
        $canView = true;
        
        if ( !Cms_System_Course::canView($course) )
        {
            $canView = false;
        }
    
        if (in_array($curriculum_id, $identity->access_content))
        {
            $canView = true;
        }
    
        if ( !$canView )
        {
            return Cms_Render::error('You don\'t have permission to view this course.');
        }

        //hasenrolled
        $hasenrolled  = false;
        $check = $this->enrolDb->getEnrol(array('user_id = ?' => Zend_Auth::getInstance()->getIdentity()->id, 'course_id = ?' => $course['id']));
        $hasenrolled = !empty($check) ? true : false;
    
        if (in_array($curriculum_id, $identity->access_content))
        {
            $hasenrolled = true;
        }

        if ( $hasenrolled == false && $islearner == false )
        {
            return Cms_Render::error('You don\'t have a permission to view this course');
        }

        $dataDb = new App_Model_DataContent();
		$wikiDb = new App_Model_DataWiki();
        $filesDb = new App_Model_DataFiles();
        $linkDb = new App_Model_DataLink();
        $videoDb = new App_Model_DataVideo();
        $scormDb = new App_Model_DataScorm();
        $assignmDb = new App_Model_DataAssignments();
        $userDb = new App_Model_User();

        switch ( $blockcontent['data_type'] )
        {
            case "content":
                $contentdata = $dataDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));
                break;

			case "assignment":

                $contentdata = $filesDb->fetchAll(array(
                    'file_type = ?' => 'blockfile', 
                    'parent_id = ?' => $blockcontent['data_id']
                ));

                $asgmtdata = $contentdata;
                
                $assignment = $assignmDb->fetchRow(array(
                    'parent_id = ?' => $blockcontent['data_id'], 
                    'user_id   = ?' => $this->auth->getIdentity()->id
                ));

                if (!empty($assignment) )
                {
                    $username = $userDb->fetchRow(array('id = ?' => $assignment['grade_by']));
                    $this->view->fullname = $username['firstname'];
                }

                $getasgmtdata = $filesDb->getlimitData(array(
                    'file_type  = ?' => 'asgmfile', 
                    'parent_id  = ?' => $blockcontent['data_id'], 
                    'created_by = ?' => $this->auth->getIdentity()->id
                ));

                if ($this->getRequest()->isPost()) {
                    $formData = $this->getRequest()->getPost();

                    $data = array(
                        'course_id'     => $course['id'],
                        'user_id'       => $this->auth->getIdentity()->id,
                        'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'    => $this->auth->getIdentity()->id,
                        'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'modified_by'   => $this->auth->getIdentity()->id,
                        'parent_id'     =>  $blockcontent['data_id'],
                        'status'        =>  1,
                    );

                    if($assignment['parent_id'] != $blockcontent['data_id'])
                    {
                        $asgt    = $assignmDb->insert($data);
                        $files   = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');
                        $filesDb = new App_Model_DataFiles();
                        $dfileid = null;

                        if ( !empty($files) )
                        {
                            foreach ( $files as $file )
                            {
                                $fileData = array(
                                    'parent_id'    => $blockcontent['data_id'],
                                    'file_type'    => 'asgmfile',
                                    'file_name'    => $file['filename'],
                                    'file_url'     => Cms_System_Links::dataFile($file['fileurl'], false),
                                    'file_size'    => $file['filesize'],
                                    'created_by'   => Zend_Auth::getInstance()->getIdentity()->id,
                                    'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()')
                                );
                                $dfileid = $filesDb->insert($fileData);
                             }
                        }

                        $getasgmtdata2 = $filesDb->getlimitData(array(
                            'file_type  = ?' => 'asgmfile', 
                            'parent_id  = ?' => $blockcontent['data_id'],
                            'created_by = ?' => $this->auth->getIdentity()->id
                        ));

                        $data2 = array(
                            'file_name' => $getasgmtdata2[0]['file_name'],
                            'data_id'   => $dfileid,
                        );
                        $asgt2 = $assignmDb->update($data2,array('id = ?' => $asgt));

                        Cms_Common::notify('success','successfully added');
                        $link = $this->view->baseUrl()."/courses/block/id/".$blockcontent['data_id']."/course/".$course['id'];
                        $this->redirect( $link );
                    } 
                    else 
                    {
                        //upload file
                        $files   = Cms_Common::uploadFiles($this->uploadDir, 'pptx,xls,zip,ppt,rar,jpg,pdf,doc,docx,xlsx');
                        $filesDb = new App_Model_DataFiles();
                        $fileid  = null;

                        if ( !empty($files) )
                        {
                            foreach ( $files as $file )
                            {
                                $fileData = array(
                                    'parent_id'    => $blockcontent['data_id'],
                                    'file_type'    => 'asgmfile',
                                    'file_name'    => $file['filename'],
                                    'file_url'     => Cms_System_Links::dataFile($file['fileurl'], false),
                                    'file_size'    => $file['filesize'],
                                    'created_by'   => Zend_Auth::getInstance()->getIdentity()->id,
                                    'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()')
                                );
                                $fileid = $filesDb->insert($fileData);
                            }
                        }

                        $data = array(
                            'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'modified_by'   => $this->auth->getIdentity()->id,
                            'status'        => 1,
                            'data_id'       => $fileid
                        );
                        $asgt = $assignmDb->update($data,array('id = ?' => $assignment['id']));

                        Cms_Common::notify('success', 'Information Updated');
                        $link = $this->view->baseUrl()."/courses/block/id/".$blockcontent['data_id']."/course/".$course['id'];
                        $this->redirect( $link );
                    }
                }
                $user = $this->auth->getIdentity()->id;
                $this->view->getasgmtdata = $getasgmtdata;

                $this->view->asgmtdata = $asgmtdata;
                $this->view->assignment = $assignment;
                $this->view->user = $user;

                break;

            case "support":
                $contentdata = $filesDb->fetchAll(array('file_type = ?' => 'blockfile', 'parent_id = ?' => $blockcontent['data_id']));
                break;

            case "quiz":
                $contentdata = $filesDb->fetchAll(array('file_type = ?' => 'blockfile', 'parent_id = ?' => $blockcontent['data_id']));

                $id = $blockcontent['data_id'];
                $cid = $blockcontent['course_id'];
                $quizDb = new App_Model_Quiz(); 
                $getquestion = $quizDb->getquescount($id);
                $gettmark = $quizDb->getmarksum($id);
                $getpointsum = $quizDb->getpointsum($id);
                //Quiz attempt
                $user = $this->auth->getIdentity()->id;
                $quizaDb = new App_Model_Quizattempt();
                $getuserAttempt = $quizaDb->getuAttempt($user,$cid); 
                $attempts = $quizaDb->getallAttempt($user,$id);
                //pr($getuserAttempt);exit;
                $this->view->getpointsum = $getpointsum['point'];
                $this->view->getuserAttempt = $getuserAttempt;
                $this->view->countquestion = count($getquestion);
                $this->view->gettmark = $gettmark['sum'];
                $this->view->attempts = $attempts;
                break;

            case "audio":
                $contentdata = $filesDb->fetchAll(array('file_type = ?' => 'audiofile', 'parent_id = ?' => $blockcontent['data_id']));
                break;

            case "scorm":
               $contentdata = $scormDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));

            break;

            case "link":
                /*
				$contentdata = $linkDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));
                $course_id  = $blockcontent['course_id'];
                $link = $this->view->baseUrl()."/block/index/index/id/".$blockcontent['data_id']."/course/".$course['id'];
              	$this->redirect( $link );
				*/
				$contentdata = $linkDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));
                break;

            case "video":
                $contentdata = $videoDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));
                break;

			case "wiki":
                $contentdata = $wikiDb->fetchRow(array('data_id = ?' => $blockcontent['data_id']));
                break;
        }

        if ( !isset($contentdata) || empty($contentdata) )
        {
            //throw new Exception('Invalid Content Data for Block');
            return Cms_Render::error('Invalid Content Data for Block');
        }
        else
        {
            $contentdata = !is_array($contentdata) ? $contentdata->toArray() : $contentdata;
        }

        /**
        * for scorm popup
        **/
        $modal = $this->_getParam('nowin');
        if ($modal)
        {
            $this->_helper->layout()->setLayout('/misc/content');
        }

        $this->view->modal = $modal;
        $this->view->blockcontent = $blockcontent;
        $this->view->course  = $course;
        $this->view->contentdata = $contentdata;


        // tracking (per-click)
        $data_tracking = array(
                'data_id'   => $blockcontent['data_id'],
                'user_id'   => Zend_Auth::getInstance()->getIdentity()->id,

            );

        $trackingDb = new App_Model_UserContentTracking();
        $tracking = $trackingDb->insert($data_tracking);
    }

	/*public function registerAction()
    {
        $id = $this->getParam('id');
        $regtype = $this->getParam('regtype','course');

        $course = $this->courseDb->getCourse(array("a.id = ?" => $id));

        if ( empty($course)  )
        {
            return Cms_Render::error('Course doesn\'t exist.');
        }

        if ( !Zend_Auth::getInstance()->hasIdentity() )
        {
            return Cms_Render::error('You must be logged in to enroll in a course. <a href="'.$this->view->baseUrl().'/index/register/course/'.$course['id'].'">Login</a> or <a href="'.$this->view->baseUrl().'/register">Sign Up</a>');
        }

        //canenrol
        $canenrol = true;

        //have you enrolled in this course before
        $auth = Zend_Auth::getInstance();
        $user_id = $auth->getIdentity()->id;

        $check = $this->enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' => $regtype, 'course_id = ?' => $id));

        if ( !empty($check) )
        {
            $canenrol = false;
            $error = 'You already enrolled in this course';
        }

        //are you not a student
        if ( $auth->getIdentity()->role != 'student' )
        {
            $canenrol = false;
            $error = 'Only students can enroll in courses.';
        }

        //views
        $this->view->course  = $course;

        //error trap
        if ( $canenrol == false )
        {
            return Cms_Render::error($error);
        }
        else
        {
            $data = array(
                            'course_id'     => $id,
                            'user_id'       => $user_id,
                            'regtype'       => $regtype,
                            'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by'    => $auth->getIdentity()->id
                    );

            $this->enrolDb->insert($data);
        }
    }*/
    public function viewQualificationDetailAction()

   {
       $auth = Zend_Auth::getInstance()->getIdentity();
       $userid = $this->getParam('userid');
       $txnid = $this->getParam('txn');
       $this->view->txnid = $txnid;
       $this->sidebar = 1;

       //user sidebar
       Cms_Hooks::push('user.sidebar', self::userSidebar('course'));

       if(isset($auth->login_as)){
           $auth->external_id = $auth->sp_id;
       }else{
           $auth->external_id = $auth->external_id;
       }

       $applicationDb = new App_Model_SmsApplication();
       $registrationId =  $applicationDb->getIdDataRegistration($auth->external_id);

       $myApplication = $applicationDb->getAppTransactionByApplicant($auth->external_id,$txnid);

       if($myApplication) {
           $smsprogramid = $myApplication["IdProgram"];
           if ($smsprogramid) {
               $lmscurriculumid = $this->currDb->fetchRow(array("external_id = ?" => $smsprogramid));
               if ($lmscurriculumid) {
                   $this->view->lmscurriculumid = $lmscurriculumid["id"];
               }
           }
       }
       //user sidebar
       // Cms_Hooks::push('user.sidebar', self::userSidebar('exam'));
       //  echo "here";exit;
       if ($this->getRequest()->isPost()) {

           $formData = $this->getRequest()->getPost();
           $this->redirect('/dashboard');
         exit;
           $this->uploadFolder = 'applicantdocs/';
           if ( !is_dir( $this->uploadDir ) )
           {
               mkdir($this->uploadDir);
           }
           $this->uploadFolder = 'applicantdocs/'.date('Y');
           $this->uploadDir = DOCUMENT_PATH1. '/'.$this->uploadFolder;

           if ( !is_dir( $this->uploadDir ) )
           {
               mkdir($this->uploadDir);
           }

           $this->uploadFolder = 'applicantdocs/'.date('Y').'/'.trim($formData['regid']);
           $this->uploadDir = DOCUMENT_PATH1. '/'.$this->uploadFolder;

           if ( !is_dir( $this->uploadDir ) )
           {
               mkdir($this->uploadDir);
           }
          // pr($formData);
           $upload = new Zend_File_Transfer_Adapter_Http();
           $files  = $upload->getFileInfo();pr($files);exit;
           foreach($files as $file => $fileInfo) {
               if ($upload->isUploaded($file)) {
                   if ($upload->isValid($file)) {
                       if ($upload->receive($file)) {
                           $info = $upload->getFileInfo($file);
                           $tmp  = $info[$file]['tmp_name'];
                           // here $tmp is the location of the uploaded file on the server
                           // var_dump($info); to see all the fields you can use
                       }
                   }
               }
           }

           $files = Cms_Common::uploadFiles($this->uploadDir,"xlsx,doc,docx");
           $data = array(
               'ad_txn_id'     => $course['id'],
               'ad_filepath'   => $this->auth->getIdentity()->id,
               'ad_filename'   => new Zend_Db_Expr('UTC_TIMESTAMP()'),
               'ad_createdby'  => $this->auth->getIdentity()->id,
               'ad_createddt'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),

           );
       }

       //breadcrumbs
       $bc = array(
           'home',
       );
       $this->view->bc = $bc;
       $this->view->userid = $userid;
       $this->view->myApplication = $myApplication;
   }

    public function viewMembershipDetailAction()
    {
        $auth = Zend_Auth::getInstance()->getIdentity();
        $ExamregDB = new App_Model_Exam_ExamRegistration();
        $userid = $this->getParam('userid');

        //get application
        $applicationDb = new App_Model_SmsApplication();
        $registrationId =  $applicationDb->getIdDataRegistration($auth->external_id);

        $MembershipDB = new App_Model_Membership_Membership();
        $registype = "1,3";
        //$registrationDetl = $MembershipDB->getStudReg("", $auth->sp_id, $registype);

        $arrayData = array();

        $memberDetl = $MembershipDB->getDataById("", $registrationId);
        $arrayData["membershipDetl"] = $memberDetl;

        $memberProgramDetl = $MembershipDB->getMemberProgram($memberDetl['mr_idProgram']);
        $arrayData["memberProgramDetl"] = $memberProgramDetl;

        $memberProgramDetl["IdProgram"];
        $feeProgramDetl = $MembershipDB->getFeeProgram($memberProgramDetl["IdProgram"]);
        $arrayData["feeProgramDetl"] = $feeProgramDetl;

        $this->view->arrayData = $arrayData;

        $membershipProgram = $MembershipDB->getProgram("","Yes");

        foreach ($membershipProgram as $key=>$mp) {
            $mp['IdProgram'];
            $MPfeeProgramDetl = $MembershipDB->getFeeProgram($mp['IdProgram']);
            $membershipProgram[$key]["feeProgramDetl"] = $MPfeeProgramDetl;
        }
        
        $this->view->membershipProgram = $membershipProgram;

        $this->sidebar = 1;

        //user sidebar
        Cms_Hooks::push('user.sidebar', self::userSidebar('course'));

        if($auth->login_as){
            $auth->external_id = $auth->sp_id;
        }else{
            $auth->external_id = $auth->external_id;
        }


        //user sidebar
        // Cms_Hooks::push('user.sidebar', self::userSidebar('exam'));
        //  echo "here";exit;
        //breadcrumbs
        $bc = array(
            'home',
        );
        $this->view->bc = $bc;
        $this->view->userid = $userid;
    }


    public function curriculumContentAction()
    {
        $cid = $this->getParam('cid', 0);
        
        if (!$cid)
        {
            return Cms_Render::error('Invalid Curriculum');
        }
    
        $auth     = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        $db       = Zend_Db_Table::getDefaultAdapter();
        
        $select     = $db->select()->from(array('a' => 'curriculum'))->where('a.id = ?', $cid);
        $curriculum = $db->fetchRow($select);
        
        if (!$curriculum)
        {
            return Cms_Render::error('Invalid Curriculum');
        }
    
        $getcourses = $this->currCoursesDb->getData($cid);
        
        $this->view->cur     = $curriculum;
        $this->view->courses = $getcourses;
    }

    public function registerModulesAction(){

        $this->_helper->layout->disableLayout();
        $studRegDb = new App_Model_StudentRegistration();
        $invoiceidDB = new App_Model_Order();
        //$txnId = $this->_getParam('txnid',0);
        if ($this->getRequest()->isPost())
        {//echo "heere";
            $formData = $this->getRequest()->getPost();

            $txnId = $formData['txnid'];//echo $txnId;exit;
            /*if (isset($formData['exempt'])) {
               if ($formData['exempt'] == "on") {
                   $exempt = 1;
               } else {
                   $exempt = 0;
               }
            } else {
                $exempt = 0;
            }*/

            for($i=1; $i<=$formData['totalsubject'];$i++){

                if($formData['total']){
                    $mdpriceexemptsts = $formData['mdprice'.$i];
                    $arrayval = explode(":",$mdpriceexemptsts);
                    $mdprice = $arrayval[0];
                    $exemptedsts = $arrayval[1];

                    if ($exemptedsts=="") {
                        $exemptstatus = 0;
                    } else {
                        $exemptstatus = 1;
                    }

                    //$mdprice = $formData['mdprice'.$i];
                    $idSubject = $formData['idsubject'.$i];
                    $idStudentRegistration = $formData['idStudentRegistration'];
                    $dataSubject = array(
                        'IdStudentRegistration' => $formData['idStudentRegistration'],
                        'IdSubject' => $idSubject,
                        'ExemptionStatus' => $exemptstatus,
                        'SubjectsApproved' => 30,
                        'Active' => 1,
                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()')
                    );
                    $hasRegistered = $studRegDb->checkStudentRegSubjects($idStudentRegistration,$idSubject);
                    if(!$hasRegistered){
                        if (isset($mdprice) && $mdprice!="") {
                            $studRegDb->addRegSubjectsData("tbl_studentregsubjects",$dataSubject);
                        }

                        //Update Transaction Status
//                        $dataTxn = array(
//                            'TransactionStatus' => 1603,
//                            'ExemptionStatus' => $exempt
//                        );
//                        $studRegDb->updateData($dataTxn,$idStudentRegistration);

//                        $invoiceidDB->add
                    }else{

                    }

                }
                //pr($dataSubject);

            }

            //add invoice
            //$studRegInfo = $studRegDb->getDataStudentRegistrationInfo($idStudentRegistration);
            $txnId = $txnId;
            $dataInvoice = array(
                'bill_number' => 'IN'.date('Y').rand(),
                'trans_id' => $txnId,
                'IdStudentRegistration' => $idStudentRegistration,
                'bill_amount' => $formData['total'],
                'status' => 'X'
            );
            $invoiceid = $invoiceidDB->addInvoiceNew("invoice_main",$dataInvoice);//echo $txnId; exit;
            $this->_redirect('/register/checkout-modedemo/i/'.$txnId);
            //$this->redirect('user/dashboard/');



        }
    }

}
