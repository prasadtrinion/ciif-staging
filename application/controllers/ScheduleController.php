<?php 
class ScheduleController extends Zend_Controller_Action
{
    protected $alphabets;
    protected $alphabets_flip;
    
    public function init()
    {
        $alphabets = array();
        $alp       = 'A';
        while ($alp !== 'AAA')
        {
            $alphabets[] = $alp++;
        }
        $this->alphabets      = array_combine(range(1, count($alphabets)), array_values($alphabets));
        $this->alphabets_flip = array_flip($this->alphabets);
    }
    
	public function examScheduleAction(){
		
		 $ec_id = $this->_getParam('ec_id',0);
         $admin = $this->_getParam('admin',0);
		 
		 $this->view->title = "Examination Schedule";
		 
		 $form  = new App_Form_SearchExamSchedule();
		 $this->view->form = $form;
		 $search = 0;
		 
		 if ($this->getRequest()->isPost()) {
		 	$search   = 1;
            $formdata = $this->getRequest()->getPost();
            
            if($form->isValid($formdata))
            {
            	$form->populate($formdata);
            
	            $scheduleDb = new App_Model_ExamSchedule();
	            
	            //get exam center
	            $ec_list = $scheduleDb->getExamCenterInfo($formdata);
	            
	            //get exam date at selected exam center
	            $exam_dates = $scheduleDb->getExamDate($formdata);
	            $this->view->exam_dates = $exam_dates;
	            
	            if(count($ec_list)>0){
	            	foreach($ec_list as $index=>$center){ 
	            		foreach($exam_dates as $key=>$edate){
	            			//get exam schedule at each exam date at respective lc
	            			$schedule_exception = $scheduleDb->getExamException($center['ec_id'],$edate['es_date']);
	            			
	            			if(!$schedule_exception){
		            			$schedule = $scheduleDb->getExamSchedule($center['ec_id'],$edate['es_date']);	            			            			       			            				
	            				$exam_dates[$key]['schedule']=$schedule;   
	            			
			            		$slot = $scheduleDb->getExamSlot($center['ec_id'],$edate['es_date']);
			            		
			            		//get total no of student for each slot
			            		if(count($slot)>0){
				            		foreach($slot as $index_slot=>$sl){
				            			$candidate = $scheduleDb->getRegisteredCandidate($center['ec_id'],$schedule['es_id_list'],$sl['sl_id']);
				            			$slot[$index_slot]['total_candidate']=$candidate['total_candidate'];
				            		}
			            		}
			            		$exam_dates[$key]['slot']=$slot; 
			            		
			            		$ec_list[$index]['exam_date_schedule']=$exam_dates;
	            				$ec_list[$index]['exam_date_slot']=$exam_dates; 
	            			}	            			
	            			    		   		
	            		}
	            		
	            	}
	            }
	           
	            $this->view->ec_list = $ec_list;
            }           
            
		 }
		 
		 $this->view->admin = $admin;
		 $this->view->search = $search;
	}
	
	public function downloadAction()
    {
        $year = $this->_getParam('year', date('Y'));
        $sms  = getDB2();
        
        $months = array_fill(1, 12, array());
        
        $select = $sms->select()
            ->from(array('a' => 'exam_schedule'), array(
                'examschedule_id' => 'es_id', 'es_date',
                'date'  => 'day(es_date)',
                'month' => 'month(es_date)',
                'year'  => 'year(es_date)'
            ))
            ->join(array('b' => 'exam_setup_detail'), 'b.esd_id = a.es_esd_id', array())
            ->join(array('c' => 'programme_exam'), 'c.pe_id = b.esd_pe_id', array('pe_name'))
            ->where('YEAR(a.es_date) = ?', $year)
            ->where('a.corporate_id = ?', 0)
            ->where('a.es_active = ?', 1)
            ->order('es_date')
            ->order('pe_name');
        $result = $sms->fetchAll($select);
        
        foreach ($result as $i => $row)
        {
            $select = $sms->select()
                ->from(array('a' => 'exam_scheduletaggingslot'), array())
                ->join(array('b' => 'exam_slot'), 'b.sl_id = a.sts_slot_id', array('sl_name', 'sl_starttime', 'sl_endtime'))
                ->join(array('c' => 'exam_taggingslotcenterroom'), 'c.tsc_slotid = a.sts_slot_id', array())
                ->join(array('d' => 'tbl_exam_center'), 'd.ec_id = c.tsc_examcenterid', array('ec_id', 'ec_name'))
                ->where('a.sts_schedule_id = ?', $row['examschedule_id'])
                ->order('d.ec_name')
                ->order('b.sl_starttime');
            $slots = $sms->fetchAll($select);
    
            $row['slots'] = $slots;
            $result[$i]   = $row;
            //$months[$row['month']][] = $row;
        }
        
        $exam_centers = array();
        $select       = $sms->select()
            ->from(array('a' => 'tbl_exam_center'), array('ec_id', 'ec_code', 'ec_name'))
            ->where('a.is_local = ?', 1)
            ->order('a.ec_name');
        $tmp = $sms->fetchAll($select);
        
        foreach ($tmp as $row)
        {
            $exam_centers[$row['ec_id']] = $row;
        }
        
        foreach ($months as $month => $data)
        {
            $data    = $exam_centers;
            $max_day = date('t', strtotime("$year-$month-01"));
            $days    = array_fill(1, $max_day, array());
            
            foreach ($data as $ec_id => $exam_center)
            {
                $data[$ec_id]['date'] = $days;
            }
            
            $months[$month] = $data;
        }
        
        
        foreach ($result as $i => $row)
        {
            $date  = $row['date'];
            $month = $row['month'];
            $paper = $row['pe_name'];
            
            foreach ($row['slots'] as $slot)
            {
                $ec_id         = $slot['ec_id'];
                $data          = $slot;
                $data['paper'] = $paper;
                unset($data['ec_id'], $data['ec_name']);
                
                $months[$month][$ec_id]['date'][$date][] = $data;
                
            }
        }
    
    
        $objPHPExcel = self::createExcel('TBE Examination Schedule '. $year);
        
        foreach ($months as $month => $row)
        {
            $sheet      = $month - 1;
            $month_desc = date('F', strtotime("$year-$month-01"));
            $max_day    = date('t', strtotime("$year-$month-01"));
            
            if ($sheet > 0)
            {
                $objPHPExcel->createSheet($sheet);
            }
    
            $objPHPExcel->setActiveSheetIndex($sheet);
            $objPHPExcel->getActiveSheet()->setTitle("$month_desc");
            
            $cell_alp = 'A';
            $cell_num = 1;
    
            $headers = array('Exam Center');
            for ($i=1; $i<=$max_day; $i++)
            {
                $headers[] = date('d/m/Y', strtotime("$year-$month-$i"));
            }
            self::setExcelHeaders($objPHPExcel, $headers);
            
            
            
            foreach ($row as $ec_id => $exam_center)
            {
                $cell_num++;
                $cell_alp = 'A';
                
                self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $exam_center['ec_name']);
                
                $original_cell_num = $cell_num;
                
                foreach ($exam_center['date'] as $date => $slots)
                {
                    $cell_alp = self::traverseAlphabet($cell_alp);
                    $cell_num = $original_cell_num;
                    
                    foreach ($slots as $slot)
                    {
                        
                        $value    = $slot['paper'] ." (" . date('h:iA', strtotime($slot['sl_starttime'])) ." -- ". date('h:iA', strtotime($slot['sl_endtime'])) .')';
                        self::setCellValue($objPHPExcel, $cell_alp . $cell_num, $value);
                        
                        $cell_num++;
                    }
                }
            }
        }
        
        //pr($months[2][1]); exit;
        
        self::generateExcel($objPHPExcel, 'TBE_EXAMINATION_SCHEDULE_'. $year);
        
        exit;
        
    }
    
    private function traverseAlphabet($alp, $move = 1)
    {
        $index = $this->alphabets_flip[$alp] + $move;
        
        if (isset($this->alphabets[$index]))
        {
            return $this->alphabets[$index];
        }
        
        die('Invalid alphabet index #'. $index);
    }
    
    private function createExcel($title = 'IBFIM')
    {
        require_once('../library/PHPExcel/Classes/PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->getProperties()->setCreator("IBFIM");
        $objPHPExcel->getProperties()->setLastModifiedBy("IBFIM");
        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setSubject($title);
        $objPHPExcel->getProperties()->setDescription($title);
        $objPHPExcel->setActiveSheetIndex(0);
        
        return $objPHPExcel;
    }
    
    private function setExcelHeaders(&$objPHPExcel, $headers = array(), $cell_num = 1, $cell_alp = 'A') {
        
        foreach ($headers as $i => $desc)
        {
            self::setCellValue($objPHPExcel, $cell_alp.$cell_num, $desc, true, true, 'FFFFFF', '16365C');
            self::setAlignCenter($objPHPExcel, $cell_alp.$cell_num);
            $cell_alp = self::traverseAlphabet($cell_alp);
        }
        
        $cell_num++;
        return $cell_num;
    }
    
    private function setCellValue(&$objPHPExcel, $cell, $value = '', $border = false, $bold = false, $color = null, $background = null)
    {
        $objPHPExcel->getActiveSheet()->SetCellValue($cell, "$value");
        
        if ($border)
        {
            self::setCellBordered($objPHPExcel, $cell);
        }
        
        if ($bold)
        {
            self::setCellBold($objPHPExcel, $cell);
        }
        
        if ($color)
        {
            self::setCellColor($objPHPExcel, $cell, $color);
        }
        
        if ($background)
        {
            self::setCellBackgroundColor($objPHPExcel, $cell, $background);
        }
    }
    
    private function setCellBordered(&$objPHPExcel, $cell)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    }
    
    private function setCellBold(&$objPHPExcel, $cell)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setBold(true);
    }
    
    private function setCellColor(&$objPHPExcel, $cell, $color)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->getColor()->setRGB($color);
    }
    
    private function setCellBackgroundColor(&$objPHPExcel, $cell, $color)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($color);
    }
    
    private function setCellFormatNumber(&$objPHPExcel, $cell)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
    }
    
    private function setAlignCenter(&$objPHPExcel, $cell)
    {
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    }
    
    private function generateExcel(&$objPHPExcel, $filename = 'report')
    {
        $objPHPExcel->setActiveSheetIndex(0);
        
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$filename.'.xls"');
        
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save('php://output');
    }
}
?>