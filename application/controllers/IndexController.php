<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
require_once('External/recaptcha/autoload.php');
header('Access-Control-Allow-Origin: *');
class IndexController extends Zend_Controller_Action
{
    protected $events;

    public function init()
    {
        error_reporting(E_ERROR | E_PARSE);
        
         $this->auth = Zend_Auth::getInstance();
        
    }

    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();


        if ($auth->hasIdentity())
        {
            
            $get_user =$auth->getIdentity()->username;
            // echo $get_user;
            // die();
            if($get_user == 'admin' )
            {
                 $this->redirect('/admin');

            }else{
                    $this->redirect('/dashboard');   
            }
            
            $sp = isset($auth->external_id) ? $auth->external_id : 0;
        
            if(isset($auth->getIdentity()->login_as)){
                $sp = isset($auth->sp_id) ? $auth->sp_id : 0;
            }else{
                $sp = isset($auth->external_id) ? $auth->external_id : 0;
            }
            
           
        }else{
             $this->redirect('/login');
        }
    }
    // public function getemailAction()
    // {
 
    //   $mail = new Cms_SendMail();
    //   $send_mail = $mail->fnSendMail();
    
    //   if($send_mail)
    //   {
    //     $this->redirect('/login');
    //   }

    // }
    public function facebookAction() 
        {
        
         $token = $this->getRequest()->getParam('token',false);
            if($token == false) {
        return false; // redirect instead 
        }
 
        $auth = Zend_Auth::getInstance();
        $adapter = new Zend_Auth_Adapter_Facebook($token);
        $result = $auth->authenticate($adapter);
    if($result->isValid()) {
        $user = $adapter->getUser();
        $auth->getStorage()->write($user);
        return true; // redirect instead 
        }
        return false; // redirect instead 
        }

    
    public function getuserAction()
    {
            $this->_helper->layout->disableLayout();
            $username = $this->_getParam('username',null);

             $dbUser = new App_Model_User;     
                //check username
                $user = $dbUser->fetchRow(array("username = ?" => $username));
                if (!empty($user)) {
                   echo 1;
                   exit;
                }
    }

    public function getemailAction()
    {
            $this->_helper->layout->disableLayout();
            $email = $this->_getParam('email',null);

             $dbUser = new App_Model_User;     
                //check username
                $user = $dbUser->fetchRow(array("email = ?" => $email));
                if (!empty($user)) {
                   echo 1;
                   exit;
                }
    }

    public function sendAction()
    {
            $this->_helper->layout->disableLayout();
            
           $mail = new Cms_SendMail();

           $mail_body =  'Testing';
           $applicant_name = 'Testing';
           $applicant_email = 'nparthi92@gmail.com';

           $message = 'Hi '. $applicant_name . $mail_body;

 
           $sendEmail = $mail->fnSendMail1($applicant_email,$mail_body,$mail_body);
           $this->_redirect( $this->baseUrl . '/register');
    }
    
    public function registerAction()
    {
  
        $this->_helper->layout()->setLayout('/auth/register');

        $form = new App_Form_Register();

        $redirect = $this->getParam('r');

        $countryDb = new App_Model_Country();




        //toggle password required
        $form->username->setRequired(true);
        $form->password->setRequired(true);

        //$events = Cms_Events::trigger('auth.register.form', $this, array('form'=>$form));
        if ( !empty($events) )
        {
            foreach ( $events as $ev )
            {
                Cms_Hooks::push('registration.extra', $ev);
            }
        }

        $fail = 0;

        if ($this->getRequest()->isPost())
        {
            Cms_Common::expire();

            $formData = $this->getRequest()->getPost();
        
            
            if($formData['Experience_type']== 'Yes')
            {
                $experience = $formData['industrialExp'];

            }else
            {
                $experience = 0;
            }

            if($formData['lectureshipRadio']== 'Yes')
            {
                $lectureship = $formData['lectureshipRadio'];

            }else
            {
                $lectureship = 0;
            }
            
           
            if ($form->isValid($formData))
            {

                $dbUser = new App_Model_User;
                $dbciifp= new App_Model_Ciifpmb;
                $dbciifpdoc= new App_Model_Ciifpdoc;

                    $country =  $countryDb->fetchRow(array("idCountry = ?" => $formData['countryList']));

                  if($country['CountryName'] == 'MALAYSIA')
                  {

                    $formData['nationality'] = 'MY';
                  }else{

                    $formData['nationality'] = 'OTH';
                  }       
                //check username
                $user = $dbUser->fetchRow(array("username = ?" => $formData['username']));
                if (!empty($user)) {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username already exists. <a href="/forgotpass">Forgot your password?</a>');
                }

                //check email
                $email = $dbUser->fetchRow(array("email = ?" => $formData['email']));
                if (!empty($email)) {
                    $fail = 1;
                    $errmsg = $this->view->translate('Email already in use. <a href="/forgotpass">Forgot your password?</a>');
                }

                //check for illegal characters
                if ( preg_match("/[^a-zA-Z0-9]+/", $formData['username']) && $fail != 1 )
                {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username contains illegal characters. Try again. Only alphanumeric characters are allowed.',0);
                }

                $post = Cms_Events::trigger('auth.register.post', $this, $formData);
                $extraData = array();

                if ( !empty($post) && count($post) > 0 )
                {
                    
                    if ( $post[0]['fail'] == 1 )
                    {
                        $fail = 1;
                     
                    }
                    else
                    {
                        foreach ( $post[0]['data'] as $key => $value )
                        {
                            $extraData[$key] = $value;
                        }
                    }
                }



                if ( ( strlen($formData['username']) < 3 || strlen($formData['username']) > 25 ) && $fail != 1 )
                {
                    if ( strlen($formData['username']) < 3 )
                    {
                        $fail = 1;
                        $errmsg = $this->view->translate('Username is too short. You need to have at least 3 characters',0);
                    }
                    else
                    {
                        $fail = 1;
                        $errmsg = $this->view->translate('Username is too long. Your username cannot be longer than 25 characters.',0);
                    }
                }

                $fail = 0; //temporary solution
                
                if ($fail != 1)
                {
                    $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);
                   

                    $data = array(
                                    'username'      => $formData['username'],
                                    'password'      => $hash,
                                    'salutation'    => $formData['Salutation'],
                                    'salt'          => $salt,
                                    'firstname'     => $formData['firstname'],
                                    'lastname'      => $formData['lastname'],
                                    'email'         => $formData['email'],
                                    'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                    'role'          => 'student',
                                    'contactno'     => $formData['c_code'].'-'.$formData['contactno'],
                                    'active'        => 1,
                                    'country'  => $formData['countryList'],
                                    'nationality'   => $formData['nationality'],
                                    'id_member'   => $formData['membershiptype'],
                                    'id_member_subtype' => $formData['subclassifaication'],
                                    'approve_status'=> 0,
                                    'estatus'=> 591,
                                    'payment_status'=> 0,
                                    'id_qualification' => $formData['ddlQualification'],
                                    'id_experience' => $formData['workexperience'],
                                    
                                    'lecturship_exp' => $lectureship,
                                    'prof_des' => $formData['prof_des'],
                              );

                                  
                    $data = array_merge($data,$extraData);
                    //ADD USER


                  

                    
                    $user_id = $dbUser->insert($data);


                            $upload = new Zend_File_Transfer();
                    $userid1 = $dbUser->getUserByUsername($formData['username']);
                   // $id= $userid1['id'];
                            $membership_id= $userid1['id'];
                              $files  = $_FILES;
                          

                              // FileUpload Academic
                               $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
                                 $target_file1 = 'AcademicDoc'.date('YmdHis').$_FILES['file1']['name'][$i];
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH."\\".$target_file1)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
                                           } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }



                             for($i=0;$i<count($_FILES['file2']['name']);$i++) 
                              {
                                   if($i==0) {
                                      $k = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $k = $j;
                                   
                                   }
                                 $target_file2 = 'WorkExperience'.date('YmdHis').$_FILES['file2']['name'][$k];
   if (move_uploaded_file($_FILES["file2"]["tmp_name"][$k], UPLOAD_PATH."\\".$target_file2)) {



                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file2);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              } 


                              for($i=0;$i<count($_FILES['file3']['name']);$i++) 
                              {

                                if($i==0) {
                                      $l = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $l = $j;
                                   
                                   }

                                        // move upload file
                                 $target_file3 = 'PreviosMembershipdoc'.date('YmdHis').$_FILES['file3']['name'][$l];
   if (move_uploaded_file($_FILES["file3"]["tmp_name"][$l], UPLOAD_PATH."\\".$target_file3)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file3);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }  


                               for($i=0;$i<count($_FILES['file4']['name']);$i++) 
                              {
                                if($i==0) {
                                      $m = 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $m = $j;
                                   
                                   }
                                        // move upload file
                                 $target_file4 = 'NominationDoc'.date('YmdHis').$_FILES['file4']['name'][$m];
   if (move_uploaded_file($_FILES["file4"]["tmp_name"][$m], UPLOAD_PATH."\\".$target_file4)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file4);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }


                               for($i=0;$i<count($_FILES['file5']['name']);$i++) 
                              {

                                if($i==0) {
                                      $n= 0;
                                        // move upload file
                                   } else {
                                     $j++;
                                    $n = $j;
                                   
                                   }

                                        // move upload file
                                 $target_file5 = 'OtherDoc'.date('YmdHis').$_FILES['file5']['name'][$n];
   if (move_uploaded_file($_FILES["file5"]["tmp_name"][$n], UPLOAD_PATH."\\".$target_file5)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file5);
            //echo "The file ". basename($_FILES["file1"]["name"][$i]). " has been uploaded.";
                                        } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }  


                                if($user_id)
                                {
        $user = $dbUser->fetchRow(array('id = ?' => $membership_id));
       
        $applicant_name = $user['firstname'].' '.$user['lastname'];
         $applicant_email = $user['email'];

        $get_mail = new Admin_Model_DbTable_Emailtemplate();
        $mail_detail = $get_mail->fnViewTemplte(23);
        
           $mail = new Cms_SendMail();
           $message = 'Hi '. $applicant_name .$mail_detail['TemplateBody'];

 
           $sendEmail = $mail->fnSendMail($applicant_email, $mail_detail['TemplateSubject'],$message);
                                }
                               Cms_Common::notify('success', 'Please complete your profile . ');
                            
                           Zend_Loader::loadClass('Zend_Filter_StripTags');
                $filter = new Zend_Filter_StripTags();
                $username = $filter->filter($formData['username']);
                $password = $filter->filter($formData['password']);

                //process form
                $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

                $authAdapter->setTableName('user')
                    ->setIdentityColumn('username')
                    ->setCredentialColumn('password');
                   // ->setCredentialColumn('approve_status');
                //->setCredentialTreatment('CONCAT(?,salt)');

                //echo Cms_Common::generateRandomString(22);
                //$options  = array('salt' => $salt);
                //$hash = password_hash($pass, PASSWORD_BCRYPT, $options);

                $dbUser = new App_Model_User;
                $user = $dbUser->fetchRow(array("username = ?" => $username));


                if(empty($user))
                {

                    $authAdapter->setTableName('user')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');
                    $user = $dbUser->fetchRow(array("email = ?" => $username));
                }
                $login_appoval_status = $user->approve_status;
                $payment = $user->payment_status;
                $approval = $user['approve_status'];
               
            
             //     echo $payment;
             //    // print_r($user);
             // die();


                if ( empty($user) )
                {
                    $fail = 1;
                    $approval = 2;
                }
                
                else
                {
                    $password = password_hash($password, PASSWORD_BCRYPT, array('salt'=>$user->salt));
                    // $password;

                }

                if (empty($user))
        {
            echo "<script>$('#user').append('Invalid username or password');</script>";
             

        }

                if ( isset($formData['key'])){
                    $icno = $key;
                }else{
                    $icno = $username;
                }

                $authAdapter->setIdentity($username);
                $authAdapter->setCredential($password);
               
                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authAdapter);


                if ($result->isValid() && $fail != 1)
                {
                    
                    $data = $authAdapter->getResultRowObject(null, 'password');


                    if (!$data)
                    {
                        $fail = 1;
                    }
                    else
                    {

                        // login on behalf
                        if ( isset($formData['key']))
                        {
                            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                            $sql = $dbAdapter->select()
                                            ->from(array('a'=>'user'))
                                            ->where('username = ?', $key);
                            $result = $dbAdapter->fetchRow($sql);

                            unset($result["password"]);

                            $temp = new stdClass();
                            foreach ($result as $key => $value)
                            {
                                $temp->$key = $value;
                            }

                            $data = $temp;
                        }


                       

                        $auth->getStorage()->write($data);

                        //update external_id = student_profile id
                        $dbUser->update(array(
                            'lastlogin' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'external_id' => $std_id
                        ) , array('id = ?'=>$data->id ));
                        



                        Cms_Events::trigger('auth.login.success', $this, (array) $data);

                        if ( $redirect != '' )
                        {
                            header('location:'. Cms_Common::encrypt($redirect, false));
                            return;
                        }
                        else if($data->role == 'student')
                        {
                            $this->redirect($this->view->url(array('module' => 'default','controller'=>'dashboard'), 'default', true));

                        }
                        else
                        {
                            $this->redirect($this->view->url(array('module' => 'admin','controller'=>'index'), 'default', true));
                        }

                    }

                }

          
                }
                else
                {
                    $this->view->errmsg = $errmsg;
                    $form->populate($formData);
                }

            }
            else
            {
                $this->view->errmsg = 'You must fill in all the required fields.';
            }
        }

        //view
        $this->view->redirect = $redirect;
        $this->view->form = $form;

    }

    
    
    public function registermODAction()
    {
        $this->_helper->layout()->setLayout('/auth/register');

        $form = new App_Form_Register();

        $redirect = $this->getParam('r');

        //toggle password required
        $form->username->setRequired(true);
        $form->password->setRequired(true);

        $events = Cms_Events::trigger('auth.register.form', $this, array('form'=>$form));
        if ( !empty($events) )
        {
            foreach ( $events as $ev )
            {
                Cms_Hooks::push('registration.extra', $ev);
            }
        }

        $fail = 0;

        if ($this->getRequest()->isPost())
        {
            Cms_Common::expire();

            $formData = $this->getRequest()->getPost();

            // print_r($formData);
            // die('register controller');

            if ($formData['nationality'] == 'MY' && !is_numeric($formData['username']))
            {
                $this->view->errmsg = 'Please enter <strong>NRIC</strong> number as Username';
            }
            elseif ($form->isValid($formData))
            {
                $dbUser = new App_Model_User;

                

                //check username
                $user = $dbUser->fetchRow(array("username = ?" => $formData['username']));
                if (!empty($user)) {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username already exists. <a href="/forgotpass">Forgot your password?</a>');
                }

                //check email
                $email = $dbUser->fetchRow(array("email = ?" => $formData['email']));
                if (!empty($email)) {
                    $fail = 1;
                    $errmsg = $this->view->translate('Email already in use. <a href="/forgotpass">Forgot your password?</a>');
                }

                //check for illegal characters
                if ( preg_match("/[^a-zA-Z0-9]+/", $formData['username']) && $fail != 1 )
                {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username contains illegal characters. Try again. Only alphanumeric characters are allowed.',0);
                }

                $post = Cms_Events::trigger('auth.register.post', $this, $formData);
                $extraData = array();

                if ( !empty($post) && count($post) > 0 )
                {
                    if ( $post[0]['fail'] == 1 )
                    {//echo pr($post);"sini";
                        $fail = 1;
                        $errmsg = $this->view->translate($post[0]['error']);
                    }
                    else
                    {
                        foreach ( $post[0]['data'] as $key => $value )
                        {
                            $extraData[$key] = $value;
                        }
                    }
                }


                if ( ( strlen($formData['username']) < 3 || strlen($formData['username']) > 25 ) && $fail != 1 )
                {
                    if ( strlen($formData['username']) < 3 )
                    {
                        $fail = 1;
                        $errmsg = $this->view->translate('Username is too short. You need to have at least 3 characters',0);
                    }
                    else
                    {
                        $fail = 1;
                        $errmsg = $this->view->translate('Username is too long. Your username cannot be longer than 25 characters.',0);
                    }
                }

                $fail = 0;//tipu kt sini jap

                if ($fail != 1)
                {
                    $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);

                    $data = array(
                        'username'      => $formData['username'],
                        'password'      => $hash,
                        'salt'          => $salt,
                        'firstname'     => $formData['firstname'],
                        'lastname'      => $formData['lastname'],
                        'email'         => $formData['email'],
                        'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'role'          => 'student',
                        'contactno'     => $formData['contactno'],
                        'nationality'   => $formData['nationality'],
                        'active'        => 1,
                    );

                    $data = array_merge($data,$extraData);

                    //ADD USER
                    $user_id = $dbUser->insert($data);

                   
                    //student registration
                    $auth = Zend_Auth::getInstance();
                    //$user_id = $auth->getIdentity()->id;
                    //$dataDb = new Admin_Model_DbTable_ExternalData();
                    $userDb = new App_Model_User();
                   // $extDataDb = new Admin_Model_DbTable_ExternalData();
                    $user = $userDb->getUser($user_id);



                   // $company_id = !empty($user['company']) ? $extDataDb->getValue($user['company'])['code'] : '';
                //                    $qualification_id = !empty($user['qualification']) ? $extDataDb->getValue($user['qualification'])['code'] : '';
                    $company_id = 0;
                    $qualification_id = 0;

                    //echo $company_id."company";exit;
                  //  $learningmodeTypes = $dataDb->getData('LearningMode');
                    //$learningmodeTypes = array_column(;$learningmodeTypes, 'value', 'code');

                   // $learningmode = Plugins_App_Registration::getLearningMode((!isset($schedule[$row['id']]) ? array() : $schedule[$row['id']]));

                    //1. register as student kalau tak exists
                    $params = array(
                        'company_id' => $company_id,
                        'qualification_id' => $qualification_id
                    );
                    //echo "ssss";//exit;
                    $params = $params + $user;
                    //$params = $params;

                //                    echo "------------userid".$user_id.$user['external_id']."externalid";exit;
                //
                //pr($params);
                                    //student profile
                    if ( !empty($user['external_id']) || $user['external_id'] != 0 )
                    {//echo "666";
                        $external_id = $user['external_id'];
                    }
                    else
                    {//echo "7777";
                        $sp = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewProfile', $params);//echo SMS_API . '/get/Registration/do/addNewProfile';//exit;
                      //  pr( $sp)
            //                        echo $sp;
            //                       echo json_encode($sp);
                        $userDb->update(array('external_id' => $sp), array('id = ?' => $user['id']));
                        $external_id = $sp;
                    }

                    //echo "------------external_id".$external_id;//exit;
                   // exit;

                  //  $check = $this->enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' => $regtype, $type.'= ?' => $id));

                    if ( !empty($external_id))
//                        if ( empty($check['external_id']) || empty($check2['external_id']) )
                    {
                       // echo "sss";echo $external_id."--extdata";
//                        //scheme & landscape---to be updated after approval
//                        $params = array(
//                            'IdProgram' => $row['data']['IdProgram'],
//                            'IdMode'    => $learningmodeTypes[$learningmode],
//                        );
//
//                        $externalData = Cms_Curl::__(SMS_API . '/get/Registration/do/externalData', $params);

//                        if ( empty($externalData) )
//                        {
//                            return Cms_Render::error('Error Fetching Data. #extd');
//                        }

//                        $params = array(
//                            'sp_id' => $external_id,
//                            'IdProgram' => $row['data']['IdProgram'],
//                            'IdProgramScheme' => $externalData['scheme']['IdProgramScheme'],
//                            'IdLandscape' => $externalData['landscape']['IdLandscape']
//                        );
//
//                        $params = array(
//                            'sp_id' => $external_id,
//                            'IdProgram' => $row['data']['IdProgram'],
//                            'IdProgramScheme' => 0,
//                            'IdLandscape' => 0
//                        );

                      //  echo SMS_API . '/get/Registration/do/addNewReg';
                        $params1 = array(
                            'sp_id' => $external_id,
                            'IdProgram' => $formData['program'],
                            'IdProgramScheme' => 0,
                            'IdLandscape' => 0,
                            'IdRegtype' => $formData['appcategory'],
                            'IdMembership' => $formData['membership'],
                            'IdRequirement' => $formData['progEntry']
                        );
                        $params =  $params+ $params1;

                      //  $params = array('courses' => $register_external, 'check' => 1, 'student_id' => $regStudent['IdStudentRegistration']);

                        //pr($params);
//pr($user);
                     //   $regStudent = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewReg', $user);
                        $regStudent = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewReg', $params);//exit;

                    }
                    else
                    {
//                        return Cms_Render::error('Already registered for #'.$row['id'].' #476');
                    }

                    //register
//                    $register_external = array();
//
//                    $register_external[] = array(
//                        'lms_course_id'             =>  $course['external_id'],
//                        'course_id'                 => $course['id']
//                    );


               //     $params = array('courses' => $register_external, 'check' => 1, 'student_id' => $regStudent['IdStudentRegistration']);

                 //   $course_schedule = isset($schedule[$row['id']]) ? $schedule[$row['id']] : array();

                 //   $params = $params + array('schedule' => $course_schedule);

                  //  $registered_external = Cms_Curl::__(SMS_API . '/get/Registration/do/addSubject', $params);

//                    if ( empty($register_external) )
//                    {
//                        return Cms_Render::error('Unable to sync registration data.');
//                    }



                    $session  = new Zend_Session_Namespace('Registration');
                    $session->setExpirationSeconds(60);
                    $session->formData = $formData;

                    Cms_Events::trigger('auth.register.success', $this, array('id'=>$user_id) );

                    $user = $dbUser->fetchRow(array("id = ?" => $user_id))->toArray();

//                    $sp = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewProfile', $user);
                    //$user['sp_id'] = $user['std_id'] = $sp['sp_id'];

                    $user['new'] = 1;

                    //write data into session

                    $auth = Zend_Auth::getInstance();
                    $auth->getStorage()->write( (object) $user);
//exit;
                    if ( $redirect != '' )
                    {
                        header('location:'. Cms_Common::encrypt($redirect, false));
                        return;
                    }
                    else
                    {
                        $this->redirect($this->view->url(array('module' => 'default','controller'=>'index'), 'default', true));
                    }
                }
                else
                {
                    $this->view->errmsg = $errmsg;
                    $form->populate($formData);

                    //$this->redirect($this->view->url(array('module' => 'default','controller'=>'index','action'=>'register'), 'default', true));
                }
            }
            else
            {
                $this->view->errmsg = 'You must fill in all the required fields.';
            }
        }

        //view
        $this->view->redirect = $redirect;
        $this->view->form = $form;

    }


    public function registerOldAction()
    {

        //die('registerold');
        $this->_helper->layout()->setLayout('/auth/register');

        $form = new App_Form_User();

        $redirect = $this->getParam('r');

        //toggle password required
        $form->username->setRequired(true);
        $form->password->setRequired(true);

        $events = Cms_Events::trigger('auth.register.form', $this, array('form'=>$form));
        if ( !empty($events) )
        {
            foreach ( $events as $ev )
            {
                Cms_Hooks::push('registration.extra', $ev);
            }
        }

        $fail = 0;

        if ($this->getRequest()->isPost())
        {
            Cms_Common::expire();

            $formData = $this->getRequest()->getPost();
    
            if ($formData['nationality'] == 'MY' && !is_numeric($formData['username']))
            {
                $this->view->errmsg = 'Please enter <strong>NRIC</strong> number as Username';
            }
            elseif ($form->isValid($formData))
            {
                $dbUser = new App_Model_User;


                //check username
                $user = $dbUser->fetchRow(array("username = ?" => $formData['username']));
                if (!empty($user)) {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username already exists. <a href="/forgotpass">Forgot your password?</a>');
                }

                //check email
                $email = $dbUser->fetchRow(array("email = ?" => $formData['email']));
                if (!empty($email)) {
                    $fail = 1;
                    $errmsg = $this->view->translate('Email already in use. <a href="/forgotpass">Forgot your password?</a>');
                }

                //check for illegal characters
                if ( preg_match("/[^a-zA-Z0-9]+/", $formData['username']) && $fail != 1 )
                {
                    $fail = 1;
                    $errmsg = $this->view->translate('Username contains illegal characters. Try again. Only alphanumeric characters are allowed.',0);
                }

                $post = Cms_Events::trigger('auth.register.post', $this, $formData);
                $extraData = array();

                if ( !empty($post) && count($post) > 0 )
                {
                     if ( $post[0]['fail'] == 1 )
                    {
                        $fail = 1;
                        $errmsg = $this->view->translate($post[0]['error']);
                    }
                    else
                    {
                        foreach ( $post[0]['data'] as $key => $value )
                        {
                            $extraData[$key] = $value;
                        }
                    }
                }


                if ( ( strlen($formData['username']) < 3 || strlen($formData['username']) > 25 ) && $fail != 1 )
                {
                    if ( strlen($formData['username']) < 3 )
                    {
                        $fail = 1;
                        $errmsg = $this->view->translate('Username is too short. You need to have at least 3 characters',0);
                    }
                    else
                    {
                        $fail = 1;
                        $errmsg = $this->view->translate('Username is too long. Your username cannot be longer than 25 characters.',0);
                    }
                }

                if ($fail != 1)
                {
                    $salt =  Cms_Common::generateRandomString(22);
                    $options  = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);

                    $data = array(
                                    'username'      => $formData['username'],
                                    'password'      => $hash,
                                    'salt'          => $salt,
                                    'firstname'     => $formData['firstname'],
                                    'lastname'      => $formData['lastname'],
                                    'email'         => $formData['email'],
                                    'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                    'role'          => 'student',
                                    'contactno'     => $formData['contactno'],
                                    'active'        => 1
                    );

                    $data = array_merge($data,$extraData);

                    $user_id = $dbUser->insert($data);

                    $session  = new Zend_Session_Namespace('Registration');
                    $session->setExpirationSeconds(60);
                    $session->formData = $formData;
                    
                    Cms_Events::trigger('auth.register.success', $this, array('id'=>$user_id) );

                    $user = $dbUser->fetchRow(array("id = ?" => $user_id))->toArray();
    
                    //$sp = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewProfile', $user);
                    //$user['sp_id'] = $user['std_id'] = $sp['sp_id'];

                    $user['new'] = 1;

                    //write data into session
                    $auth = Zend_Auth::getInstance();
                    $auth->getStorage()->write( (object) $user);

                    if ( $redirect != '' )
                    {
                        header('location:'. Cms_Common::encrypt($redirect, false));
                        return;
                    }
                    else
                    {
                        $this->redirect($this->view->url(array('module' => 'default','controller'=>'index'), 'default', true));
                    }
                }
                else
                {
                    $this->view->errmsg = $errmsg;
                    $form->populate($formData);

                    //$this->redirect($this->view->url(array('module' => 'default','controller'=>'index','action'=>'register'), 'default', true));
                }
            }
            else
            {
                $this->view->errmsg = 'You must fill in all the required fields.';
            }
        }

        //view
        $this->view->redirect = $redirect;
        $this->view->form = $form;

    }

    public function loginAction()
    {
        $this->_helper->layout()->setLayout('/auth/public');
        $form = new App_Form_Login();
        $form->username->setRequired(true);
        $fail = 0;
        $approval = 0;

        // if (Zend_Auth::getInstance()->hasIdentity())
        // {
        //     $this->redirect('/dashboard');
        // }

        $redirect = $this->getParam('r');

        $key_exist = $this->getParam('key');
        if (isset($key_exist)) 
        {
            $key = Cms_Common::encrypt($key_exist,false);
            // $formData['key'] = $key;
// print_r($key);
// die();
            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
             $sql = $dbAdapter->select()
                            ->from(array('a'=>'user'))
                            ->where('email = ?', $key);
            $result = $dbAdapter->fetchRow($sql);
// print_r($result);
// die();
            if (!$result) {
                Cms_Common::notify('error', "Failed to Log-in As : No such user");
                $this->redirect($this->view->url(array('module' => 'index','controller'=>'login'), 'default', true));
            }

            $this->view->key = $key;
        }

        if ($this->getRequest()->isPost()) 
        {

                $formData = $this->getRequest()->getPost();
// print_r($formData);
// die();
              if ($form->isValid($formData))
              {
                  //remember me?
                  if ( isset($formData['rememberme']) )
                  {
                      ini_set('session.gc_maxlifetime', 604800);
                      Zend_Session::rememberMe(604800);
                  }

                  // collect the data from the user
                  Zend_Loader::loadClass('Zend_Filter_StripTags');
                  $filter = new Zend_Filter_StripTags();
                  $username = $filter->filter($this->_request->getPost('username'));
                  $password = $filter->filter($this->_request->getPost('password'));

                  //process form
                  $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                  $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

                  $authAdapter->setTableName('user')
                              ->setIdentityColumn('username')
                              ->setCredentialColumn('password');
                 
                  $dbUser = new App_Model_User;
                  $user = $dbUser->fetchRow(array("username = ?" => $username));


                  if(empty($user))
                  {

                    $authAdapter->setTableName('user')
                                ->setIdentityColumn('email')
                                ->setCredentialColumn('password');
                    $user = $dbUser->fetchRow(array("email = ?" => $username));
                  }
                  $login_appoval_status = $user->approve_status;
                  $payment = $user->payment_status;
                  $approval = $user['approve_status'];
               
            
                  if ( empty($user) )
                  {
                      $fail = 1;
                      $approval = 2;
                  }
                  
                  else
                  {
                      $password = password_hash($password, PASSWORD_BCRYPT, array('salt'=>$user->salt));
                      // $password;
                  }
                  if (empty($user))
                  {
                      echo "<script>$('#user').append('Invalid username or password');</script>";
                  }

                  if ( isset($formData['key']))
                  {
                      $icno = $key;
                  }else{
                      $icno = $username;
                  }

                  $authAdapter->setIdentity($username);
                  $authAdapter->setCredential($password);
                 
                  $auth = Zend_Auth::getInstance();
                  $result = $auth->authenticate($authAdapter);

                  if ($result->isValid() && $fail != 1)
                  {
                    
                    $data = $authAdapter->getResultRowObject(null, 'password');


                    if (!$data)
                    {
                        $fail = 1;
                    }
                    else
                    {

                        // login on behalf
                        if ( isset($formData['key']))
                        {
                            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                            $sql = $dbAdapter->select()
                                            ->from(array('a'=>'user'))
                                            ->where('email = ?', $key);
                            $result = $dbAdapter->fetchRow($sql);

                            unset($result["password"]);

                            $temp = new stdClass();
                            foreach ($result as $key => $value)
                            {
                                $temp->$key = $value;
                            }

                            $data = $temp;
                        }


                       

                        $auth->getStorage()->write($data);

                        //update external_id = student_profile id
                        $dbUser->update(array(
                            'lastlogin' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'external_id' => $std_id
                        ) , array('id = ?'=>$data->id ));
                        



                        Cms_Events::trigger('auth.login.success', $this, (array) $data);

                        if ( $redirect != '' )
                        {
                            header('location:'. Cms_Common::encrypt($redirect, false));
                            return;
                        }
                        else if($data->role == 'student')
                        {
                            $this->redirect($this->view->url(array('module' => 'default','controller'=>'dashboard'), 'default', true));

                        }
                        else
                        {
                            $this->redirect($this->view->url(array('module' => 'admin','controller'=>'index'), 'default', true));
                        }

                    }

                  } 
                  elseif($approval == 0 )
                  {
                      $this->redirect($this->view->url(array('module' => 'default','controller'=>'dashboard'), 'default', true));
                      
                  }
                  else
                  {
                      
                      // failure: clear database row from session
                      $fail = 0;
                      $errmsg = 'Login failed. Either username or password is incorrect';
                      
                      
                      
                  }

            }
            else {
                $form->populate($formData);
            }
        }

        
        

        $this->view->form = $form;
    }

    public function logoutAction()
    {
        $session = Zend_Auth::getInstance()->getIdentity();
        if(isset($session->login_as) && $session->login_as) {
            self::reLogin($session->login_as);
        }

        $storage = new Zend_Auth_Storage_Session();
        $storage->clear();

        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::ForgetMe();
        Zend_Session::destroy(true);

        Cms_Events::trigger('auth.logout', $this);



        $this->redirect($this->view->url(array('module' => 'default'), 'default', true));
    }

    private function reLogin($id) {
        $userDb = new App_Model_User();
        $user   = $userDb->fetchRow(array('id = ?' => $id));

        if(!$user) {
            return;
        }

        $user = $user->toArray();
        unset($user["password"]);
        unset($user["salt"]);

        $data = new stdClass();

        foreach ($user as $key => $value) {
            $data->$key = $value;
        }

        Zend_Auth::getInstance()->getStorage()->write($data);
        Cms_Events::trigger('auth.login.success', $this, (array) $data);
        Cms_Common::notify('success', 'You are now back as : '. $data->firstname .' '. $data->lastname);
        $this->redirect($this->view->url(array('module' => 'default','controller'=>'dashboard'), 'default', true));
    }

    public function privacyAction()
    {

    }

    public function resetpasswordAction()
    {
        Cms_Common::expire();

        $this->view->errMsg = $this->view->sucessMsg = '';

        Cms_Events::trigger('auth.resetpassword', $this);

        $id = $this->getParam('id');
        $key = $this->getParam('key');

        if ($this->getRequest()->isPost())
        {
            $userDb = new App_Model_User();

            $formData = $this->getRequest()->getPost();

            $formData = Cms_Common::cleanArray($formData);

            if ( Cms_Common::tokenCheck($formData['token']) == false )
            {
                $this->view->errMsg = $this->view->translate('Invalid Request. Try again.');
                return;
            }

            $user = $userDb->getUser($id,'id');

            if( empty($user) )
            {
                $this->view->errMsg = $this->view->translate('Invalid User ID.');
            }
            else
            {
                if ( $user['token'] != $key || $key == ''  )
                {
                    $this->view->errMsg = $this->view->translate('Key doesn\'t match. Please try again.');
                    return false;
                }
            }

            if ( strlen($formData['password']) < 6 )
            {
                $this->view->errMsg = $this->view->translate('Passwords must be at least 6 characters long');
                return false;
            }

            if ( $formData['password'] != $formData['confirmpassword'] )
            {
                $this->view->errMsg = $this->view->translate('Passwords does not match. Please try again.');
                return false;
            }

            //reset password
            $salt =  Cms_Common::generateRandomString(22);
            $options  = array('salt' => $salt);
            $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);

            $userDb->update(array('password'=>$hash,'salt' => $salt), array('id = ?' => $id));

            $this->view->successMsg = $this->view->translate('Your new password has been set. You may log in with your new password.');
        }
    }

    public function forgotpasswordAction()
    {
        $this->view->errMsg = $this->view->sucessMsg = '';

        Cms_Common::expire();

        Cms_Events::trigger('auth.forgot', $this);

        if ($this->getRequest()->isPost())
        {
            $userDb = new App_Model_User();

            $formData = $this->getRequest()->getPost();

            $formData = Cms_Common::cleanArray($formData);

            if ( Cms_Common::tokenCheck($formData['token']) == false )
            {
                $this->view->errMsg = $this->view->translate('Invalid Request. Try again.');
                return;
            }

            $user = $userDb->getUser($formData['username'],'username');

            if( empty($user) )
            {
                $this->view->errMsg = $this->view->translate('Username / IC Number does not exists.');
            }
            else
            {
                $key = Cms_Common::generateRandomString(16);
                $keylink = APPLICATION_URL."/index/resetpassword/id/".$user['id']."/key/".$key;

                $userDb->update(array('token'=>$key), array('id=?'=>$user['id']));

                $message = $this->view->translate('Click the link below to reset your password'). "<br />";
                $message .= $keylink;

                Cms_Events::trigger('auth.forgot.success', $this, array('id'=>$user['id']) );

                //send email

                //(user,title,message)
                //($to,$subject,$message,$toname="",$cc=array(),$bcc=array(), $attachments = array()) 
                 $db = getDB2();
                 $data = array(
                        'recepient_email' => $user['email'],
                        'subject' => APP_NAME.' - '.$this->view->translate('Forgot Password'),
                        'content' =>  $message,
                        'date_que' =>  date("Y-m-d H:i:s"),
                        'date_send' =>  date("Y-m-d H:i:s"),
                        'smtp' => 1,
                         );

                $db->insert('email_que',$data);

                 $mail = new Cms_SendMail();
                 $mail->fnSendMail($user['email'], APP_NAME.' - '.$this->view->translate('Forgot Password'), $message);

                $this->view->successMsg = $this->view->translate('An email has been sent to your account ('.$user['email'].'). Follow the instruction to reset your password.');

            }
        }
    }

    public function checkUsernameAction()
    {

        // echo "checkUsernameAction";
        // exit();
        $db = getDB();

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $value = $this->_getParam('username');

        $userDb = new App_Model_User();

        $sql = $userDb->select()->where('username = ?',$value);

        $results = $db->fetchAll( $sql );

        $output = array();

        if ( empty($results) )
        {
            $output['valid'] = 'true';
        }
        else
        {
            $output['valid'] = 'false';
        }

        echo Zend_Json::encode($output);
    }

    public function checkEmailAction()
    {

        $db = getDB();

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $value = $this->_getParam('email');

        $userDb = new App_Model_User();

        $sql = $userDb->select()->where('email =?',$value);


        $results = $db->fetchAll( $sql );

        $output = array();

        if ( empty($results) )
        {
            $output['valid'] = 'true';
        }
        else
        {
            $output['valid'] = 'false';
        }


        echo Zend_Json::encode($output);
    }
    public function paymentAction(){
       // $membership_id =$this->getParam('id');
        $user_id = $this->getParam('id');
        $this->view->user_id =  $user_id;
         $this->_helper->layout()->setLayout('/auth/register');

        $form = new App_Form_Register();

        $redirect = $this->getParam('r');

        
      

        if ($this->getRequest()->isPost())
        {
            Cms_Common::expire();

            $formData = $this->getRequest()->getPost();
           // echo "<pre>";
           // print_r($formData);
           // die();

           
            if ($form->isValid($formData))
            {

                //$dbPayment = new App_Model_Paymentupload;
                $dbciifp= new App_Model_Ciifpmb;
                $dbciifpdoc= new App_Model_Ciifpdoc;

          
                            $upload = new Zend_File_Transfer();
                    $userid1 = $formData['member_id'];
                   // $id= $userid1['id'];
                            $membership_id= $userid1;
                              $files  = $_FILES;
                 $j=0;
                              for($i=0;$i<count($_FILES['file1']['name']);$i++) 
                              {

                                        // move upload file
                                $j= $j+$i;
                                 $target_file1 = 'Doc'.date('YmdHis').$_FILES['file1']['name'][$i];
   if (move_uploaded_file($_FILES["file1"]["tmp_name"][$i], UPLOAD_PATH."\\".$target_file1)) {


                            $dbciifpdoc->addmembershipdocdetail($membership_id,$target_file1);
                                           } 
                                        else {
                                             print_r(error_get_last());
                                            echo "Sorry, there was an error uploading your file.";
                                        }

                              }



                           



                            $this->view->msgsuccess = 'Application Added successfully';
                            $this->redirect('/Index');
          
                
                
            }
            
        }

        //view
        $this->view->redirect = $redirect;
        $this->view->form = $form;
    }
public function composemsgAction()
    {
        //title
        $this->view->title = "Communication - Compose";
        $auth = Zend_Auth::getInstance(); 
        $form = new Communication_Form_Compose();
        $this->view->trigger = 0;
                
        $cancompose = 0;
        if ($this->_request->isPost () && $this->_request->getPost ( 'compose' ))
        {
            $formData = $this->getRequest()->getPost();
            
            // echo "<pre>";
            // print_r($formData);
            // die();

            $total = $this->view->total = count($formData['rec']);
            
            $this->view->comp_rectype = $formData['rectype'];
            
            if ( $total > 0 )
            {
                $cancompose = 1;
                $rec_ids = array();

                foreach ( $formData['rec'] as $rec_id => $rec )
                {
                    $rec_ids[] = $rec_id;
                }
                $this->view->rec = implode(',', $rec_ids);
            }
                        
            if (isset($formData['fromwhere']))
            {
                $this->view->fromwhere = $formData['fromwhere'];
            }
            else
            {
                $this->view->fromwhere = 0;
            }
        }
        
        if ($this->_request->isPost () && $this->_request->getPost ( 'composefromprev' ))
        {
            $formData = $this->getRequest()->getPost();
            $this->view->formData = $formData;

            $reclist = explode(',',$formData['rec']);

            $total = $this->view->total = count($reclist);
            
            if ( $total > 0 )
            {
                $cancompose = 1;
                
                $this->view->rec = implode(',', $reclist);
            }
            
            $this->view->trigger = 1;
            $form->populate($formData);
        }

        if ( $cancompose == 0 )
        {
            throw new Exception('You have to select recipients first');
        }
        
        //comm types
        $commTypes = $this->view->commTypes = $this->communicationTypes();
        $commTypesByName = $this->view->commTypesByName = array_flip($commTypes);

        //templates
        if ( isset($formData['category']) )
        {
            $tplData = $this->tplModel->getTemplatesByCategory($this->module, 'quicksend');
        }
        else
        {
            $tplData = $this->tplModel->getTemplates($this->module);
        }

        $form->comp_tpl_id->addMultiOption('', 'Select');
        $form->comp_type->addMultiOption('', 'Select');
        $form->comp_type->setValue($commTypesByName['Email']);

        $tpls[] = array();

        foreach ($tplData as $tpl)
        {
            $tpls[$tpl['tpl_type']][$tpl['tpl_id']] = $tpl['tpl_name'];
            //$form->comp_tpl_id->addMultiOption($tpl['tpl_id'], $tpl['tpl_name']);
        }
    
        foreach ( $commTypes  as $type_id => $type_name )
        {
            $form->comp_type->addMultiOption($type_id, $type_name);

            if ( !empty($tpls[$type_id]) ) 
            {
                if ( count($tpls[$type_id]) > 0 )
                {
                    $form->comp_tpl_id->addMultiOption($type_name, $tpls[$type_id]);
                }
            }
        }
        
        //radio lang
        $form->comp_lang->addMultiOptions($this->locales);
        if ( isset($formData['comp_lang']) && $formData['comp_lang'] != '' )
        {
            $form->comp_lang->setValue($formData['comp_lang']);
        }
        else
        {
            $form->comp_lang->setValue($this->currLocale);
        }

        //
        $this->view->form = $form;
    }
    public function aboutAction()
    {
        // echo "About ";
        // die();
    }

    public function activateAction()
    {
        $c = $this->getParam('c',0);
        $this->view->errMsg = $this->view->sucessMsg = '';

        if ( Zend_Auth::getInstance()->hasIdentity() )
        {
            return Cms_Render::error('You cannot be logged in to perform this action.');
        }

        Cms_Common::expire();

        $key = $this->getParam('key');

        $enrolDb = new App_Model_Enrol();
        $orderDb = new App_Model_Order();
        $orderUserDb = new App_Model_OrderGroupUser();
        $userDb = new App_Model_User();
        $auth = Zend_Auth::getInstance();
        $currCoursesDb = new App_Model_CurriculumCourses();

        $this->view->c = $c;

        if ( $c == 1 )
        {
            $user = $userDb->getUser($key,'token');

            if ( empty($user) )
            {
                return Cms_Render::error('Invalid User');
            }

            $this->view->user = $user;

            if ($this->getRequest()->isPost())
            {
                $formData = $this->getRequest()->getPost();
                $formData = Cms_Common::cleanArray($formData);

                if (Cms_Common::tokenCheck($formData['token']))
                {
                    $salt = Cms_Common::generateRandomString(22);
                    $options = array('salt' => $salt);
                    $hash = @password_hash($formData['password'], PASSWORD_BCRYPT, $options);

                    $data = array(
                        'password' => $hash,
                        'salt' => $salt
                    );

                    $userDb->update($data, array('id = ?' => $user['id']));

                    //all done
                    $this->view->successMsg = $this->view->translate('Your password has been set. You can now login using your IC No. and temporary password.');
                }
            }
        }
        else
        {
            $user = $orderUserDb->getUser(array('token = ?' => $key));

            if ( empty($user) )
            {
                return Cms_Render::error('Invalid User');
            }

            if ( $user['activated'] != 0 )
            {
                return Cms_Render::error('Account already activated. Please <a href="/login">login</a> to view your dashboard.');
            }

            $order = $orderDb->getOrder(array('id = ?' =>$user['order_id']));

            if ( empty($order) )
            {
                return Cms_Render::error('Invalid Order ID');
            }

            if ( $order['status'] != 'ACTIVE' )
            {
                return Cms_Render::error('Order has not been paid yet.');
            }

            $this->view->user = $user;

            if ($this->getRequest()->isPost()) {
                $formData = $this->getRequest()->getPost();
                $formData = Cms_Common::cleanArray($formData);

                if (Cms_Common::tokenCheck($formData['token'])) {
                    $salt = Cms_Common::generateRandomString(22);
                    $options = array('salt' => $salt);
                    $hash = password_hash($formData['password'], PASSWORD_BCRYPT, $options);

                    $data = array(
                        'username' => $formData['username'],
                        'password' => $hash,
                        'salt' => $salt,
                        'firstname' => $user['firstname'],
                        'lastname' => $user['lastname'],
                        'email' => $user['email'],
                        'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'role' => 'student',
                        'nationality' => $user['nationality'],
                        'company' => $user['company'],
                        'qualification' => $user['qualification'],
                        'active' => 1
                    );

                    $user_id = $userDb->insert($data);

                    //update balik
                    $data = array(
                        'activated' => 1,
                        'activated_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'user_id' => $user_id,
                        'username' => $formData['username']
                    );

                    $orderUserDb->update($data, array('token = ?' => $key));

                    //enrolkan
                    //register
                    if ($order['regtype'] == 'curriculum') {
                        $courses = $currCoursesDb->getData($order['item_id']);

                        //curriculum kena add juga?
                        $data = array(
                            'curriculum_id' => $order['item_id'],
                            'user_id' => $user_id,
                            'regtype' => 'curriculum',
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => $user_id,
                            'order_id' => $order['id']
                        );

                        $enrolDb->insert($data);

                        //courses
                        foreach ($courses as $course) {
                            $data = array(
                                'course_id' => $course['id'],
                                'user_id' => $user_id,
                                'regtype' => 'course',
                                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by' => $user_id,
                                'order_id' => $order['id']
                            );

                            $enrolDb->insert($data);
                        }
                    } else {
                        $data = array(
                            'course_id' => $order['item_id'],
                            'user_id' => $user_id,
                            'regtype' => 'course',
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => $user_id,
                            'order_id' => $order['id']
                        );

                        $enrolDb->insert($data);
                    }

                    //all done
                    $this->view->successMsg = $this->view->translate('Your account has been created. You can now login with your credentials.');
                } else {
                    return Cms_Render::error('Invalid Security Token. Please try again.');
                }
            }

        }
    }


    // public function loadrequirementAjaxAction(){
    //     $programid = $this->_getParam('programid',null);

    //     $externalDb = new Admin_Model_DbTable_ExternalData();

    //     $this->_helper->layout->disableLayout();

    //     $ajaxContext = $this->_helper->getHelper('AjaxContext');
    //     $ajaxContext->addActionContext('view', 'html');
    //     $ajaxContext->initContext();

    //     $ajaxContext->addActionContext('view', 'html')
    //         ->addActionContext('form', 'html')
    //         ->addActionContext('process', 'json')
    //         ->initContext();

    //     $getItemInfo = $externalDb->fngetAllentry($programid);

    //     $json = Zend_Json::encode($getItemInfo);

    //     echo $json;
    //     exit();
    // }

    // public function loadmembershipAjaxAction(){
    //     $programid = $this->_getParam('programid',null);
    //     //$programid = 43;
    //     $dataDb = new Admin_Model_DbTable_ExternalData();

    //     $this->_helper->layout->disableLayout();

    //     $ajaxContext = $this->_helper->getHelper('AjaxContext');
    //     $ajaxContext->addActionContext('view', 'html');
    //     $ajaxContext->initContext();

    //     $ajaxContext->addActionContext('view', 'html')
    //         ->addActionContext('form', 'html')
    //         ->addActionContext('process', 'json')
    //         ->initContext();

    //     if($programid != 0){
    //         $programid = $programid;
    //     }else{$programid = 0;}
    //     $getItemInfo = $dataDb->getMembershipProgram($programid);

    //     $json = Zend_Json::encode($getItemInfo);

    //     echo $json;
    //     exit();
    // }
    
    // public function getMembershipAjaxAction(){
    
    //  ini_set('display_errors','on');
        
  //       $this->_helper->layout->disableLayout();

  //       $ajaxContext = $this->_helper->getHelper('AjaxContext');
  //       $ajaxContext->addActionContext('view', 'html');
  //       $ajaxContext->initContext();

  //       $ajaxContext->addActionContext('view', 'html')
  //           ->addActionContext('form', 'html')
  //           ->addActionContext('process', 'json')
  //           ->initContext();
    
  //       $membershipDb = new  App_Model_Membership_TblMembership();
  //       $result = $membershipDb->getListMembership(1);
       
  //       $json = Zend_Json::encode($result);

  //       echo $json;
  //       exit();
  //   }

    public function getsubtypeAction()
    {
        
         ini_set('display_errors','on');
        
        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();



        echo $memberId = $this->_getParam('member_id',null);
        // die();
        $query =  new Admin_Model_DbTable_MemberGet();

        $memberdata = $query->getmemberdata($memberId);
        $json = Zend_Json::encode($memberdata);

        
                    
                        print_r($json);
                       // die();

         exit();
    }



}



