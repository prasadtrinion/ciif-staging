<?php

class ChatController extends Zend_Controller_Action
{
    protected $courseDb;
    protected $contentblockDb;
    protected $blockDataDb;
    protected $enrolDb;
    protected $currDb;
    protected $sidebar  = 1;
    protected $currCoursesDb;

	public function indexAction()
    {
        $course_id = $this->getParam('course');

		$auth = Zend_Auth::getInstance()->getIdentity();

        $permResourcesDb = new App_Model_PermissionResources();
        $permRoleDb = new App_Model_PermissionRole();
        $coursesDb = new App_Model_Courses();
        $enrolDb = new App_Model_Enrol();
        $userDb = new App_Model_User();
        $onlineDb = new App_Model_UserOnline();
        $chatDb = new App_Model_Chat();

        if ($this->getRequest()->isPost()) 
        {
            $formData = $this->getRequest()->getPost();
            $fullname = $auth->firstname.' '.$auth->lastname;

            if(isset($formData["message"]) && strlen($formData["message"])>0)
            {
                $username = $auth->id;
                $message = filter_var(trim($formData["message"]),FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
                $user_ip = $_SERVER['REMOTE_ADDR'];
                $msg_time = date('h:i A M d'); // current time

                $formData['user_id'] = $auth->id;
                $formData['date_time'] = date('Y-m-d H:i:s', strtotime($msg_time));
                
                //insert new message in db
                $saveChat = $chatDb->insertChat($formData);

                if($saveChat)
                {
                    echo '<div class="shout_msg"><time>'.$msg_time.'</time><span class="username">'.$fullname.'</span><span class="message">'.$message.'</span></div>';
                }
            }
            elseif($formData["fetch"]==1)
            {
                $results = $chatDb->getChat($auth->id, 'course', $formData['course_id']);

                foreach($results as $row)
                {
                    $msg_time = date('h:i A M d',strtotime($row["date_time"])); //message posted time
                    echo '<div class="shout_msg"><time>'.$msg_time.'</time><span class="username">'.$row["firstname"].' '.$row['lastname'].'</span> <span class="message">'.$row["message"].'</span></div>';
                }
            }
            else
            {
                //output error
                header('HTTP/1.1 500 Are you kiddin me?');
                exit();
            }

            exit;
        }
        $course_id = 50;

        // get course info
        $course = $coursesDb->getCourse(array("id = ?" => $course_id));

        //get users & no of onilne
        $totalusers = $onlineDb->getOnlineCourseMatesInt($course_id);
        $totalstudents = $onlineDb->getOnlineCourseMatesInt($course_id,'','student');
        $totallecturer = $onlineDb->getOnlineCourseMatesInt($course_id,'','lecturer');

        $onlineusers = $onlineDb->getOnlineCourseMates($course_id);
        $onlinestudents = $onlineDb->getOnlineCourseMates($course_id,'','student');
        $onlinelecturer = $onlineDb->getOnlineCourseMates($course_id,'','lecturer');

        //enrolled buddies
        $myprograms = $enrolDb->getEnrolledProgramme($auth->id, false);

        $totalenrolledprogram = count($myprograms);
        $totalenrolled = 0;
        $a = 0;
        if ($myprograms) {

            foreach ($myprograms as $a => $mypg) {
                $curriculumId = $mypg['id'];
                $IdProgram = $mypg['external_id'];
                $learningmode = ($mypg['learningmode'] == 'OL') ? 577 : 578;

                $mycourses = $enrolDb->getEnrolledCourses($auth->id, $curriculumId, false);
                $totalenrolled += count($mycourses);
                $myprograms[$a]['courses'] = $mycourses;

                foreach ($mycourses as $index => $course_data) {
                    $onlinecoursemates = $onlineDb->getOnlineCourseMates($course_data['id']);
                    $myprograms[$a]['courses'][$index]['onlinemates'] = $onlinecoursemates;
                }
            }
        }

        $this->view->totalusers = $totalusers;
        $this->view->totalstudents = $totalstudents;
        $this->view->totallecturer = $totallecturer;

        $this->view->onlineusers = $onlineusers;
        $this->view->onlinestudents = $onlinestudents;
        $this->view->onlinelecturer = $onlinelecturer;

        $this->view->course = $course;
        $this->view->course_id = $course_id;
        $this->view->myprograms = $myprograms;
        $this->view->auth = $auth;
	}
}
