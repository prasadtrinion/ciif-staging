<?php
class ServicesController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
	}
	
	public function indexAction()
	{
		die(json_encode(array('msg' => '500', 'error' => 1)));
	}

	public function scormAction()
	{
		Cms_Scorm_Api::restful();
	}

	public function apiAction()
    {
		$request = $this->_request->getParams();

		if ( isset($request['get']) && isset($request['do']) )
		{

			$call = array("Cms_Api_" . $request['get'], $request['do']);

			if (is_callable($call)) {
				call_user_func($call);
			}
			else {
				Cms_Api::error('Invalid Function');
			}
		}else{
			Cms_Api::error('Invalid Parameters');
		}

	}

}

