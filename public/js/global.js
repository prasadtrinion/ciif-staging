/* common js functions */
function sure(el, msg)
{
	if (msg == undefined)
	{
		var msg = 'Are you sure?';
	}

	alertify.confirm(msg, function () {
		window.location = $(el).attr('href');
	});

	return false;
}

function redirect(url)
{
	window.location = url;
}

function escapeHtml(string) 
{
	var entityMap = 
	{
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': '&quot;',
		"'": '&#39;',
		"/": '&#x2F;'
	};
	return String(string).replace(/[&<>"'\/]/g, function (s) {
	  return entityMap[s];
	});
}

function filesarea(evt)
{
	var files = evt.target.files; // FileList object
	for (var i = 0, f; f = files[i]; i++)
	{
		var reader = new FileReader();
		var isimg = f.type.match('image.*') ? true : false;

		// Closure to capture the file information.
		reader.onload = (function(theFile) {
			return function(e) {

				if ( isimg ) {
					// Render thumbnail.
					$(".uploadlist").append('<li><img class="fileareathumb" src="'+e.target.result+'" title="'+escape(theFile.name)+'"/>' +"</li>", null);
				}
				else
				{
					$(".uploadlist").append("<li><i class='uk-icon-file-o uk-text-muted uk-margin-small-right'></i> "+theFile.name+"</li>");
				}
			};
		})(f);

		// Read in the image file as a data URL.
		reader.readAsDataURL(f);

	}
}

var appInit = {
	all: function()
	{
		this.filer();
		this.filesarea();
		this.validate();
		//this.button();
		this.plugin();
	},
	plugin: function()
	{
		$.widget( "custom.scheduleselect", $.ui.selectmenu, {
				  _renderItem: function( ul, item ) {
						var li = $( "<li>" ),
							wrapper = $( "<div>", {
								title: item.element.attr( "title" )
							} );
						
							  

						if ( item.disabled ) {
							this._addClass( li, null, "ui-state-disabled" );
						}
						this._setText( wrapper, item.label );

						 li.append( wrapper ).appendTo( ul );
						$( "<span>", {
							  text: item.element.attr( "data-description" ),
							  "class": "ui-selectmenu-description"
							}).appendTo( wrapper );

						 return li;
					}
				});
	},
	button: function(e)
	{
		/*$(".uk-button.uk-button-primary").on('click', function(){

			if ( $(this).hasClass("button-loading") )
			{
				$(this).removeClass("button-loading");
				$(this).html($(this).data("text"));
			}
			else
			{

				$(this).attr("style", "width:"+ ($(this).width()+30) + "px" );
				$(this).addClass("button-loading");
				$(this).data('text', $(this).html());
				$(this).empty();

			}
		});*/

		$(e.target).find(".uk-button.uk-button-primary").each(function()
		{
			if ( $(this).hasClass("button-loading") )
			{
				$(this).removeClass("button-loading");
				$(this).html($(this).data("text"));
			}
			else
			{

				$(this).attr("style", "width:"+ ($(this).width()+30) + "px" );
				$(this).addClass("button-loading");
				$(this).data('text', $(this).html());
				$(this).empty();

			}
		});
	},
	filer : function()
	{
		$('.fileupload.img').filer({
			limit: 1,
			maxSize: 50,
			extensions: ['jpg', 'jpeg', 'png', 'gif'],
			changeInput: true,
			showThumbs: true,
			templates: appvar.filerTpl
		});

		$('.fileupload.docs').filer({
			limit: 1,
			maxSize: 50,
			extensions: ['zip', 'rar', 'doc', 'docx', 'txt', 'xls','pdf', 'xlsx', 'ppt', 'pptx'],
			changeInput: true,
			showThumbs: true,
			templates: appvar.filerTpl
		});

		$('.fileupload.mp4').filer({
			limit: 1,
			maxSize: 50,
			extensions: ['mp4'],
			changeInput: true,
			showThumbs: true,
			templates: appvar.filerTpl,
			errors: {
				filesType: "Only mp4 videos are allowed to be uploaded"
			}
		});



		$('.fileupload.all').filer({
			limit: 1,
			maxSize: 50,
			extensions: ['gif','png','jpg','jpeg','zip', 'rar', 'doc', 'docx', 'txt', 'xls','pdf', 'xlsx', 'ppt', 'pptx'],
			changeInput: true,
			showThumbs: true,
			templates: appvar.filerTpl
		});
	},
	filesarea: function()
	{
		//filesarea
		$(".filesarea").each( function()
		{
			var $this = $(this), hasdragCls = false;

			$this.on("drop", function(e){
				e.stopPropagation();
				e.preventDefault();

				$this.removeClass('uk-dragover');

				$.each( e.originalEvent.dataTransfer.files, function(index, file)
				{
					var isimg = file.type.match('image.*') ? true : false;
					var fileReader = new FileReader();

					fileReader.onload = (function(file) {
						return function(e) {
							if ( isimg ) {
								// Render thumbnail.
								$(".uploadlist").append('<li><img class="fileareathumb" src="'+e.target.result+'" title="'+escape(file.name)+'"/>' +"</li>", null);
							}
							else
							{
								$(".uploadlist").append("<li><i class='uk-icon-file-o uk-text-muted uk-margin-small-right'></i> "+file.name+"</li>");
							}
						};
					})(file);
					fileReader.readAsDataURL(file);
				});

				//test
				//$(this).find("input[type='file']").prop("files", e.originalEvent.dataTransfer.files);

			}).on("dragenter", function(e){
				e.stopPropagation();
				e.preventDefault();
			}).on("dragover", function(e){
				e.stopPropagation();
				e.preventDefault();

				if (!hasdragCls) {
					$this.addClass('uk-dragover');
					hasdragCls = true;
				}
			}).on("dragleave", function(e){
				e.stopPropagation();
				e.preventDefault();
				$this.removeClass('uk-dragover');
				hasdragCls = false;
			});

			$(this).find("input[type=file]").on('change', filesarea );
			if ( $(this).parent().is( "a" ) )
			{
				$(this).parent().after("<ol class='uploadlist' />");
			}
			else
			{
				$(this).after("<ol class='uploadlist' />");
			}

		});
	},
	validate: function()
	{
		$(".validate").each(function()
		{
			$(this).formValidation({ framework: 'uikit' }).on('success.form.fv', function()
			{
				appInit.button(this);
			});
		});
	}

}
/* checkbox toggle */
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}(';(3($){$.w.A=3(m,h){2 4={o:3(7){g $(7).z(\':j\')},b:3(7){g(4.o(7))?1:0},c:3(n){2 a=$(\'<k u="r-5-a"></k>\');g a.q(\'y\',3(e){2 5=$(6).B();2 d=!4.b(5);2 8=(d)?1:0;2 9=4.c(8);5.i(9).x(\'j\',d);$(6).s();4.f(e,5)}).l(\'I\',\'L\').M(m[n])},f:3(e,5){p(K(h)==\'3\'){h(e,5)}}};$.J(6,3(E,7){2 8=4.b(7);2 9=4.c(8);$(7).i(9).l(\'C\',\'F\').q(\'G\',3(e){2 t=$(6).v().H(\'u\');p(t==\'r-5-a\'){$(6).v().s();2 8=4.b(6);2 9=4.c(8);$(6).i(9);4.f(e,$(6))}})})}})(D);',49,49,'||var|function|TC|checkbox|this|element|contentIndex|toggleContent|container|getContentIndex|getContent|nextChecked||fireCallback|return|callback|after|checked|span|css|toggleContents|index|isChecked|if|on|toggle|remove|className|class|next|fn|prop|click|is|toggleCheckbox|prev|display|jQuery|key|none|change|attr|cursor|each|typeof|pointer|html'.split('|'),0,{}))

/* misc */
var appvar = {
	filerTpl: {
		box: '<ul class="jFiler-item-list"></ul>',
		item: '<li class="jFiler-item">\
                    <div class="jFiler-item-container">\
                        <div class="jFiler-item-inner">\
                            <div class="jFiler-item-thumb">\
                                <div class="jFiler-item-status"></div>\
                                <div class="jFiler-item-info">\
                                    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                    <span class="jFiler-item-others">{{fi-size2}}</span>\
                                </div>\
                                {{fi-image}}\
                            </div>\
                            <div class="jFiler-item-assets jFiler-row">\
                                <ul class="list-inline pull-right">\
                                    <li><a class="uk-icon-trash-o jFiler-item-trash-action"></a></li>\
                                </ul>\
                            </div>\
                        </div>\
                    </div>\
                </li>',
		itemAppendToEnd: false,
		removeConfirmation: true,
		_selectors: {
			list: '.jFiler-item-list',
			item: '.jFiler-item',
			remove: '.jFiler-item-trash-action',
		}
	}
}


$(function(){

	//jQuery.event.props.push('dataTransfer');

	//flashmessenger
	setTimeout(function(){
		$(".fsm.message:not(.uk-alert-danger)").slideUp(function(){
			$(this).remove();
		});
	},2000);

	appInit.all();
});
