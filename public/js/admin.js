$(function()
{
    $('.form-check').formValidation({ framework: 'uikit' });
    $(document.body).append('<div id="ajax-modal" class="uk-modal"><div class="uk-modal-dialog uk-modal-dialog-large"><button type="button" class="uk-modal-close uk-close "></button><div id="modal-ajax-container"></div></div></div></div>');
});

var AppLang = {
    deletefile : 'Are you sure you want to delete this file?'
}

var AppAdmin = {
    checkVideo: function(e, target, thumb)
    {
        var url = $(e).val();

        if ( url != '' ) {

            //clear
            $("#"+thumb).hide();
            $("#"+thumb+"-value").val('');

            //perform
            $.ajax({
                url: "/admin/ajax/check-url",
                type: "post",
                data: {'url': url},
                dataType: 'json',
                beforeSend: function()
                {
                    $(e).after('<i class="uk-icon-refresh uk-icon-spin loading-spinner"></i>');
                },
                success: function (data)
                {
                    $(e).next().remove();

                    if (data.msg == '') {
                        $("#"+target).val(data.title);
                        $("#"+thumb).show();
                        $("#"+thumb).html('<img src="'+data.thumb+'" alt=""/>');
                        $("#"+thumb+"-value").val(data.thumb);
                    }
                    else {
                        console.log(data.msg);
                    }
                },
                error: function () {
                    alert("failure");
                }
            });
        }
    },
    checkUrl: function(e, target)
    {
        var url = $(e).val();

        if ( url != '' ) {
            //perform
            $.ajax({
                url: "/admin/ajax/check-url",
                type: "post",
                data: {'url': url},
                dataType: 'json',
                beforeSend: function()
                {
                    $(e).after('<i class="uk-icon-refresh uk-icon-spin loading-spinner"></i>');
                },
                success: function (data)
                {
                    $(e).next().remove();

                    if (data.msg == '') {
                       $("#"+target).val(data.title);
                    }
                    else {
                        console.log(data.msg);
                    }
                },
                error: function () {
                    alert("failure");
                }
            });
        }
    },
    deleteFile: function(id)
    {
        alertify.confirm(AppLang.deletefile, function ()
        {
            //perform
            $.ajax({
                url: "/admin/ajax/delete-file",
                type: "post",
                data: {'id':id},
                dataType: 'json',
                success: function(data)
                {
                    if (data.msg == '' )
                    {
                        $('#filerow-'+id).remove();
                    }
                    else
                    {
                        alert(data.msg);
                    }
                },
                error:function(){
                    alert("failure");
                }
            });
        });

        return false;
    },
    modal : function (url) {

        if ( typeof url === 'object')
        {
            url = $(url).attr("href");
        }


        var modal = UIkit.modal("#ajax-modal");

        if ( modal.isActive() ) {
            modal.hide();
        } else {
            modal.show();

            $.ajax({
                url: url,
                data: {},
                type: 'POST',
                beforeSend: function() {

                    $('#modal-ajax-container').html('...');
                },
                complete: function(){

                }
            })
            .done(function ( data ) {
                setTimeout(function() { $("#modal-ajax-container").html ( data ); } , 300 );
            });

        }

        return false;
    },
    ckeditor: function()
    {
        $('.textarea').each( function () {
            CKEDITOR.replace(this.id, {height: 150});
        });
    }
}