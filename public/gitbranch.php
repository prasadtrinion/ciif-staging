<?php
/**
 * @filename: currentgitbranch.php
 * @usage: Include this file after the '<body>' tag in your project
 * @author Kevin Ridgway 
 */
    // edit/add application.ini constants.GIT_HEAD = "/Applications/XAMPP/htdocs/OUM/ibfim-lms/"
	$path = APPLICATION_PATH .'/../'; // for mac/linux
    //$path = 'C:\xampp\htdocs\ibfimlms';  // for windows
	//print_r($path);
	$stringfromfile = file($path.'.git/HEAD', FILE_USE_INCLUDE_PATH); // setting for mac/linux
    //$stringfromfile = file($path.'\.git/HEAD', FILE_USE_INCLUDE_PATH); // setting for windows

    //print_r($stringfromfile);

    $firstLine = $stringfromfile[0]; //get the string from the array

    $explodedstring = explode("/", $firstLine, 3); //seperate out by the "/" in the string

    $branchname = $explodedstring[2]; //get the one that is always the branch name

    echo "<div style='clear: both; width: 100%; font-size: 14px; font-family: Helvetica; color: #30121d; 
    background: #bcbf77; padding: 5px; text-align: center; display: none;'>Current branch: <span style='color:#fff; 
    font-weight: bold; text-transform: uppercase;'>" . $branchname . "</span></div>"; //show it on the page

?>