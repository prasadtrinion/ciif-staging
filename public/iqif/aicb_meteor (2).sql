-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `advance_payment`;
CREATE TABLE `advance_payment` (
  `advpy_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `advpy_appl_id` bigint(20) DEFAULT NULL,
  `advpy_trans_id` int(11) DEFAULT NULL,
  `advpy_payee_type` int(11) DEFAULT '0' COMMENT 'fk tbl_definationms',
  `advpy_corporate_id` int(11) DEFAULT '0',
  `advpy_idStudentRegistration` int(11) DEFAULT NULL,
  `advpy_acad_year_id` bigint(20) DEFAULT NULL,
  `advpy_sem_id` bigint(20) DEFAULT NULL,
  `advpy_prog_code` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advpy_fomulir` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_invoice_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advpy_invoice_id` int(11) DEFAULT NULL,
  `advpy_invoice_det_id` int(11) NOT NULL DEFAULT '0' COMMENT 'invoice_detail',
  `advpy_payment_id` bigint(20) DEFAULT NULL,
  `advpy_rcp_id` int(11) NOT NULL,
  `advpy_refund_id` bigint(20) DEFAULT NULL,
  `advpy_cur_id` int(11) NOT NULL,
  `advpy_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_amount` decimal(20,2) NOT NULL,
  `advpy_total_paid` decimal(20,2) NOT NULL,
  `advpy_total_balance` decimal(20,2) NOT NULL,
  `advpy_status` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A: active; X: inactive',
  `advpy_creator` bigint(20) NOT NULL,
  `advpy_date` date NOT NULL,
  `advpy_create_date` datetime NOT NULL,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_exchange_rate` int(11) NOT NULL,
  `advpy_approve_by` int(11) NOT NULL DEFAULT '0',
  `advpy_approve_date` datetime DEFAULT NULL,
  `advpy_cancel_by` int(11) NOT NULL,
  `advpy_cancel_date` datetime NOT NULL,
  `IdReceivableAdjustment` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_receivable_adjustment',
  PRIMARY KEY (`advpy_id`),
  KEY `advpy_appl_id` (`advpy_appl_id`),
  KEY `advpy_trans_id` (`advpy_trans_id`),
  KEY `advpy_idStudentRegistration` (`advpy_idStudentRegistration`),
  KEY `advpy_invoice_id` (`advpy_invoice_id`),
  KEY `advpy_cur_id` (`advpy_cur_id`),
  KEY `advpy_rcp_id` (`advpy_rcp_id`),
  KEY `advpy_fomulir` (`advpy_fomulir`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_program`;
CREATE TABLE `applicant_program` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `ap_at_trans_id` bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  `ap_prog_id` bigint(20) NOT NULL,
  `ap_prog_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk tbl_program',
  `ap_ptest_prog_id` int(11) DEFAULT NULL COMMENT 'aps_app_id fk to appl_placement_program(app_id)',
  `ap_preference` tinyint(2) NOT NULL DEFAULT '1',
  `mode_study` int(11) NOT NULL,
  `program_mode` int(11) NOT NULL,
  `program_type` int(11) NOT NULL,
  `ap_prog_scheme` int(11) NOT NULL DEFAULT '0',
  `upd_date` datetime NOT NULL,
  UNIQUE KEY `app_id` (`ap_id`),
  KEY `ap_at_trans_id` (`ap_at_trans_id`),
  KEY `ap_prog_code` (`ap_prog_code`),
  KEY `ap_ptest_prog_id` (`ap_ptest_prog_id`),
  KEY `ap_preference` (`ap_preference`),
  KEY `ap_id` (`ap_id`),
  KEY `ap_at_trans_id_2` (`ap_at_trans_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_qualification`;
CREATE TABLE `applicant_qualification` (
  `ae_id` int(11) NOT NULL AUTO_INCREMENT,
  `ae_appl_id` bigint(20) NOT NULL,
  `ae_transaction_id` int(11) NOT NULL,
  `ae_qualification` int(11) DEFAULT NULL COMMENT 'PK tbl_qualificationmaster',
  `ae_degree_awarded` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_majoring` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_class_degree` int(11) DEFAULT NULL,
  `ae_result` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_year_graduate` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_institution_country` int(11) DEFAULT NULL COMMENT 'fk tbl_countries',
  `ae_institution` int(11) DEFAULT NULL COMMENT 'fk.school_master',
  `ae_medium_instruction` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_order` int(11) NOT NULL,
  `others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`ae_id`),
  KEY `ae_transaction_id` (`ae_transaction_id`),
  KEY `ae_appl_id` (`ae_appl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_status_history`;
CREATE TABLE `applicant_status_history` (
  `ash_id` int(11) NOT NULL AUTO_INCREMENT,
  `ash_trans_id` int(11) NOT NULL DEFAULT '0',
  `ash_status` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ash_oldStatus` int(11) NOT NULL,
  `ash_changeType` int(11) NOT NULL COMMENT '1 : change status 2:change scheme',
  `ash_userType` int(11) NOT NULL DEFAULT '0' COMMENT '0:applicant 1:student',
  `ash_updDate` datetime NOT NULL,
  `ash_updUser` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ash_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_transaction`;
CREATE TABLE `applicant_transaction` (
  `at_trans_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `at_appl_id` int(11) NOT NULL COMMENT 'fk applicant_profile(ap_id)',
  `at_pes_id` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_appl_type` int(11) NOT NULL COMMENT '1 - placementtest 2 -HS',
  `at_academic_year` int(11) DEFAULT '3' COMMENT 'fk academic_year(ay_id)',
  `at_intake` int(11) DEFAULT NULL,
  `at_intake_prev` int(11) DEFAULT NULL,
  `at_period` int(11) DEFAULT NULL,
  `at_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'DRAFT,ENTRY,OFFERED,REJECTED,COMPLETE,INCOMPLETE,SHORTLISTED,KIV,ACCEPTED,DECLINE,ENROLLED.ARCHIEVE',
  `at_selection_status` int(11) DEFAULT '0' COMMENT '0:Waiting for dean rating 1: Waiting for rector verification 2: Waiting for approval 3:cpmleted 4: In Pool (USM)',
  `at_create_by` int(11) NOT NULL,
  `at_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `at_submit_date` datetime DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `entry_type` int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  `at_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:not paid 1:paid tobe removed x pakai dah; 2: biasiswa',
  `at_quit_status` int(4) NOT NULL DEFAULT '0' COMMENT '1: Apply Quit 2:Approved Quit 3:Reject 4:Incomplete ',
  `at_move_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk applicant_change_program',
  `at_document_verified` int(11) NOT NULL DEFAULT '0' COMMENT '0:No 1:yes',
  `at_document_verifiedby` int(11) DEFAULT NULL,
  `at_document_verifieddt` datetime DEFAULT NULL,
  `at_registration_status` int(11) DEFAULT NULL COMMENT '1: Active',
  `at_IdStudentRegistration` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_studentregistration',
  `at_registration_date` datetime DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `at_copy_status` tinyint(4) DEFAULT '0' COMMENT '0: Default 1:Copy2:Not Copy',
  `at_bookmark` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_repository` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_fs_id` int(11) DEFAULT NULL,
  `at_dp_id` int(11) unsigned DEFAULT NULL,
  `at_dp_by` int(11) unsigned DEFAULT NULL,
  `at_dp_date` datetime DEFAULT NULL,
  `at_fs_date` datetime DEFAULT NULL,
  `at_fs_by` int(11) DEFAULT NULL,
  `at_processing_fee` enum('ISSUED','PAID') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_tution_fee` enum('ISSUED','PAID') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_offer_acceptance_date` datetime DEFAULT NULL,
  `at_default_qlevel` int(11) DEFAULT NULL,
  `at_reason` int(11) DEFAULT NULL,
  `IdLandscape` bigint(20) DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '1',
  `at_remarks` longtext COLLATE utf8mb4_unicode_ci,
  `at_ref_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`at_trans_id`),
  UNIQUE KEY `at_pes_id` (`at_pes_id`),
  KEY `at_appl_id` (`at_appl_id`),
  KEY `at_appl_type` (`at_appl_type`),
  KEY `at_period` (`at_period`),
  KEY `at_status` (`at_status`),
  KEY `at_intake` (`at_intake`),
  KEY `entry_type` (`entry_type`),
  KEY `at_selection_status` (`at_selection_status`),
  KEY `at_document_verified` (`at_document_verified`),
  KEY `at_appl_id_2` (`at_appl_id`),
  KEY `at_appl_id_3` (`at_appl_id`),
  KEY `at_status_2` (`at_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_transaction_rosak`;
CREATE TABLE `applicant_transaction_rosak` (
  `at_trans_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `at_appl_id` int(11) NOT NULL,
  `at_appl_type` int(11) NOT NULL,
  `at_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'DRAFT,ENTRY,OFFERED,REJECTED,COMPLETE,INCOMPLETE,SHORTLISTED,KIV,ACCEPTED,DECLINE,ENROLLED.ARCHIEVE',
  `at_create_by` int(11) NOT NULL,
  `at_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `at_submit_date` datetime NOT NULL,
  `entry_type` int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  PRIMARY KEY (`at_trans_id`),
  KEY `at_appl_id` (`at_appl_id`),
  KEY `at_appl_type` (`at_appl_type`),
  KEY `at_status` (`at_status`),
  KEY `entry_type` (`entry_type`),
  KEY `at_appl_id_2` (`at_appl_id`),
  KEY `at_appl_id_3` (`at_appl_id`),
  KEY `at_status_2` (`at_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `assessment_type`;
CREATE TABLE `assessment_type` (
  `at_id` int(11) NOT NULL AUTO_INCREMENT,
  `at_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_description` text COLLATE utf8mb4_unicode_ci,
  `at_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_markentry_readonly` tinyint(4) DEFAULT '0',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddt` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `attendance_mark`;
CREATE TABLE `attendance_mark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL COMMENT 'FK tbl_studentregistration',
  `IdProgram` int(11) NOT NULL COMMENT 'FK tbl_program',
  `IdSubject` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_subjectmaster',
  `coursegroup_id` int(11) NOT NULL COMMENT 'FK course_group',
  `coursegroupschedule_id` int(11) NOT NULL COMMENT 'FK course_group_schedule',
  `marks` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'in percentage',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_exam_registration`;
CREATE TABLE `batch_exam_registration` (
  `ber_id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'batch_no',
  `corporate_id` int(11) NOT NULL COMMENT 'tbl_takafuloperator',
  `inhouse` int(11) NOT NULL DEFAULT '0' COMMENT '0:Non-Inhouse 1:Inhouse',
  `IdProgram` int(11) NOT NULL,
  `IdLandscape` int(11) NOT NULL,
  `examsetup_id` int(11) NOT NULL COMMENT 'exam_setup.es_id',
  `type_nationality` int(11) NOT NULL DEFAULT '579',
  `total_student` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee_structure.fs_id',
  `submitted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes',
  `paid_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Unpaid 1:Paid',
  `paymentmode` int(11) NOT NULL DEFAULT '0',
  `at_processing_fee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_role` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `tbe_id` int(11) DEFAULT NULL COMMENT 'tbe.idBatchRegistration',
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_updatedate` datetime DEFAULT NULL COMMENT 'to update wrong id',
  PRIMARY KEY (`ber_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_exam_registration_history`;
CREATE TABLE `batch_exam_registration_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ber_id` int(11) NOT NULL,
  `batch_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'batch_no',
  `corporate_id` int(11) NOT NULL COMMENT 'tbl_takafuloperator',
  `inhouse` int(11) NOT NULL DEFAULT '0' COMMENT '0:Non-Inhouse 1:Inhouse',
  `IdProgram` int(11) NOT NULL,
  `IdLandscape` int(11) NOT NULL,
  `examsetup_id` int(11) NOT NULL COMMENT 'exam_setup.es_id',
  `type_nationality` int(11) NOT NULL DEFAULT '579',
  `total_student` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee_structure.fs_id',
  `submitted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes',
  `paid_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Unpaid 1:Paid',
  `paymentmode` int(11) NOT NULL DEFAULT '0',
  `at_processing_fee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_role` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `tbe_id` int(11) DEFAULT NULL COMMENT 'tbe.idBatchRegistration',
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_updatedate` datetime DEFAULT NULL COMMENT 'to update wrong id',
  `remarks` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdStudentRegSubjects` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_exam_schedule`;
CREATE TABLE `batch_exam_schedule` (
  `bes_id` int(11) NOT NULL AUTO_INCREMENT,
  `ber_id` int(11) NOT NULL COMMENT 'batch_exam_registration.ber_id',
  `es_id` int(11) NOT NULL COMMENT 'exam_schedule.es_id',
  `examtaggingslot_id` int(11) NOT NULL,
  `examcenter_id` int(11) NOT NULL,
  `individual` int(11) NOT NULL DEFAULT '0' COMMENT '0:Main Schedule, 1:Individual',
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `tbe_migratedate` datetime NOT NULL,
  PRIMARY KEY (`bes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_registration`;
CREATE TABLE `batch_registration` (
  `btch_id` int(11) NOT NULL AUTO_INCREMENT,
  `btch_corporate_id` int(11) NOT NULL,
  `btch_upl_id` int(11) NOT NULL,
  `btch_no` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `btch_program_id` int(11) NOT NULL,
  `btch_scheme_id` int(11) NOT NULL,
  `btch_intake_id` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `btch_landscape_id` int(11) NOT NULL,
  `btch_contact_person` int(11) NOT NULL COMMENT 'fk takaful_operator',
  `btch_type_nationality` int(11) DEFAULT NULL,
  `number_candidate` int(11) NOT NULL,
  `number_candidate_maximum` int(11) NOT NULL DEFAULT '0',
  `number_candidate_accepted` int(11) NOT NULL DEFAULT '0',
  `upload` int(11) NOT NULL COMMENT '1=excel,0=no',
  `closing_date` date DEFAULT '0000-00-00' COMMENT 'last date to add/edit participant',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `btch_fs_id` int(11) NOT NULL,
  `fs_id_upddate` datetime NOT NULL,
  `at_processing_fee` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_mode` int(11) DEFAULT NULL COMMENT 'fk payment_mode',
  `payment_mode_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1=done',
  `last_step` int(11) DEFAULT NULL COMMENT 'tbl_definationtypems 196',
  `step_date` datetime DEFAULT NULL,
  `btch_country_id` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `manual` int(11) DEFAULT '0' COMMENT '1=manual entry sms',
  `candidate_changes` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No Changes 1=Has Changes',
  `lms_access` int(11) DEFAULT '0' COMMENT '1=yes, 0=no ',
  `inhouse` int(11) DEFAULT '0' COMMENT '1=inhouse,0=normal ',
  `inhouse_status` int(11) DEFAULT '1' COMMENT '1=active, 0=inactive',
  `sendmail_date` datetime DEFAULT NULL,
  PRIMARY KEY (`btch_id`),
  KEY `btch_corporate_id` (`btch_corporate_id`),
  KEY `btch_upl_id` (`btch_upl_id`),
  KEY `btch_program_id` (`btch_program_id`),
  KEY `btch_scheme_id` (`btch_scheme_id`),
  KEY `btch_intake_id` (`btch_intake_id`),
  KEY `btch_landscape_id` (`btch_landscape_id`),
  KEY `btch_fs_id` (`btch_fs_id`),
  KEY `payment_mode` (`payment_mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `batch_registration_course`;
CREATE TABLE `batch_registration_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `btch_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `IdCourseTaggingGroup` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_registration_schedule`;
CREATE TABLE `batch_registration_schedule` (
  `brs_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment',
  `br_id` int(11) NOT NULL COMMENT 'batch_registration.btch_id',
  `cgs_id` int(11) NOT NULL COMMENT 'course_group_schedule.sc_id',
  `course_id` int(11) NOT NULL DEFAULT '0' COMMENT 'perlu ke ada field ni??',
  `individual` int(11) NOT NULL COMMENT '0:Main Schedule, 1:Individual',
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive, 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`brs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_compose`;
CREATE TABLE `comm_compose` (
  `comp_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `comp_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_tpl_id` int(10) unsigned NOT NULL,
  `comp_type` smallint(10) unsigned NOT NULL,
  `comp_rectype` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'applicants',
  `comp_lang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_totalrecipients` int(10) unsigned NOT NULL,
  `comp_rec` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`comp_id`),
  KEY `comp_module` (`comp_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_compose_recipients`;
CREATE TABLE `comm_compose_recipients` (
  `cr_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `cr_comp_id` bigint(11) unsigned NOT NULL,
  `cr_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cr_content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cr_rec_id` int(10) unsigned NOT NULL,
  `cr_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_attachment_id` int(10) unsigned DEFAULT NULL,
  `cr_attachment_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_attachment_filename` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cr_datesent` datetime DEFAULT NULL,
  PRIMARY KEY (`cr_id`),
  KEY `cr_comp_id` (`cr_comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_groups`;
CREATE TABLE `comm_groups` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_rectype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_type` int(11) NOT NULL DEFAULT '0',
  `group_status` int(11) NOT NULL,
  `group_description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_recipients` smallint(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  KEY `module` (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_groups_recipients`;
CREATE TABLE `comm_groups_recipients` (
  `rec_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rec_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `recipient_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rec_id`),
  KEY `group_id` (`group_id`,`recipient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_template`;
CREATE TABLE `comm_template` (
  `tpl_id` int(10) NOT NULL AUTO_INCREMENT,
  `tpl_module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_type` smallint(10) unsigned NOT NULL COMMENT 'fk tbl_definations (type:121)',
  `tpl_categories` mediumtext COLLATE utf8mb4_unicode_ci,
  `email_from_name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `tpl_pname` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`tpl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_template_content`;
CREATE TABLE `comm_template_content` (
  `content_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tpl_id` int(11) unsigned NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_content` mediumtext COLLATE utf8mb4_unicode_ci COMMENT '1: Standard Doc-> URL Lain2 Type -> html content',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_template_tags`;
CREATE TABLE `comm_template_tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tpl_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_value` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group`;
CREATE TABLE `course_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'fk tbl_subjectmaster',
  `maxstud` int(11) NOT NULL,
  `IdLecturer` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IdSubject` (`IdSubject`),
  KEY `IdLecturer` (`IdLecturer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_attendance`;
CREATE TABLE `course_group_attendance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSchedule` bigint(20) NOT NULL COMMENT 'fk course_group_schedule_detail',
  `IdStudentRegistration` bigint(20) unsigned NOT NULL COMMENT 'fk tbl_studentregistration',
  `status` int(11) DEFAULT NULL COMMENT 'fk_tbl_definationms 91',
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_program`;
CREATE TABLE `course_group_program` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `program_id` bigint(20) NOT NULL,
  `program_scheme_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`,`program_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_schedule`;
CREATE TABLE `course_group_schedule` (
  `sc_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdSchedule` int(11) NOT NULL COMMENT 'fk course_group_schedule',
  `IdScheduleParent` int(11) DEFAULT '0',
  `idClassType` int(11) DEFAULT NULL,
  `sc_date_start` date NOT NULL,
  `sc_date_end` date NOT NULL,
  `sc_duration` int(11) NOT NULL,
  `sc_start_time` time NOT NULL,
  `sc_end_time` time NOT NULL,
  `sc_venue` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sc_address` text COLLATE utf8mb4_unicode_ci,
  `sc_class` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sc_day` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sc_remark` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdLecturer` int(11) DEFAULT NULL,
  `sc_createdby` int(11) NOT NULL,
  `sc_createddt` datetime NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `sc_visibility` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `sc_custom` int(11) NOT NULL,
  PRIMARY KEY (`sc_id`),
  KEY `IdSchedule` (`IdSchedule`),
  KEY `idClassType` (`idClassType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_schedule_detail`;
CREATE TABLE `course_group_schedule_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scDetId` int(11) NOT NULL COMMENT 'fk course_group_schedule_detail',
  `sc_date` date NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scDetId` (`scDetId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_language`;
CREATE TABLE `course_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseid` varchar(50) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `credit_note`;
CREATE TABLE `credit_note` (
  `cn_trans_id` int(11) DEFAULT NULL,
  `cn_corporate_id` int(11) DEFAULT '0',
  `cn_IdStudentRegistration` int(11) DEFAULT NULL,
  `cn_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cn_billing_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_fomulir` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_appl_id` bigint(20) DEFAULT NULL,
  `cn_amount` decimal(20,2) NOT NULL,
  `cn_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_creator` bigint(20) NOT NULL,
  `cn_create_date` datetime NOT NULL,
  `cn_approver` bigint(20) DEFAULT NULL,
  `cn_approve_date` datetime DEFAULT NULL,
  `cn_cancel_by` bigint(20) DEFAULT NULL,
  `cn_cancel_date` datetime DEFAULT NULL,
  `cn_cur_id` int(11) NOT NULL DEFAULT '1',
  `cn_source` int(11) NOT NULL COMMENT '0=sms, 1=student portal',
  `cn_invoice_id` int(11) NOT NULL,
  `cn_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT 'A=active/approved, X=cancel,0=entry',
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cn_type` int(11) NOT NULL,
  `type_amount` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cn_exam_registration_main_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cn_id`),
  KEY `cn_fomulir` (`cn_fomulir`),
  KEY `appl_id` (`cn_appl_id`),
  KEY `cn_trans_id` (`cn_trans_id`),
  KEY `cn_IdStudentRegistration` (`cn_IdStudentRegistration`),
  KEY `cn_cur_id` (`cn_cur_id`),
  KEY `cn_invoice_id` (`cn_invoice_id`),
  KEY `cn_billing_no` (`cn_billing_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `credit_note_detail`;
CREATE TABLE `credit_note_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cn_id` int(11) NOT NULL,
  `invoice_main_id` int(11) NOT NULL,
  `invoice_detail_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `cur_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cn_fomulir` (`invoice_main_id`),
  KEY `appl_id` (`invoice_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cron_ol_exam`;
CREATE TABLE `cron_ol_exam` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `es_date` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_id` int(11) DEFAULT NULL,
  `results` mediumtext COLLATE utf8mb4_unicode_ci COMMENT 'if any results needed',
  `created_date` datetime NOT NULL,
  `start_date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `executed_date` datetime DEFAULT NULL,
  `executed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `execution_time` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cron_ol_exam_tempdata`;
CREATE TABLE `cron_ol_exam_tempdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coe_id` int(11) NOT NULL COMMENT 'cron_ol_exam',
  `es_id` int(11) DEFAULT NULL COMMENT 'exam_setup',
  `esd_id` int(11) DEFAULT NULL COMMENT 'exam_setup_detail',
  `ec_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL COMMENT 'exam_schedule',
  `scheduletaggingslot_id` int(11) DEFAULT NULL COMMENT 'exam_scheduletaggingslot',
  `idExam` int(11) DEFAULT NULL,
  `total_set` int(11) DEFAULT NULL,
  `total_student` int(11) DEFAULT NULL,
  `total_student_generated` int(11) DEFAULT NULL,
  `pe_id` int(11) DEFAULT NULL,
  `idmarkdistribution` int(11) DEFAULT NULL,
  `results` text,
  `executed` int(11) DEFAULT NULL COMMENT '1=yes,0=no',
  `ep_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `executed_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `data_migration_exam_result_4`;
CREATE TABLE `data_migration_exam_result_4` (
  `No` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(61) DEFAULT NULL,
  `Mode` varchar(11) DEFAULT NULL,
  `Type` varchar(8) DEFAULT NULL,
  `Category` varchar(13) DEFAULT NULL,
  `Code_Org` varchar(15) DEFAULT NULL,
  `Organization` varchar(59) DEFAULT NULL,
  `Nric` varchar(16) DEFAULT NULL,
  `Program` varchar(62) DEFAULT NULL,
  `Program_Code` varchar(7) DEFAULT NULL,
  `Exam_Date` varchar(15) DEFAULT NULL,
  `Grade` varchar(16) DEFAULT NULL,
  `Marks` varchar(2) DEFAULT NULL,
  `Passing_Marks` varchar(2) DEFAULT NULL,
  `Award` varchar(21) DEFAULT NULL,
  `Date_of_award` varchar(10) DEFAULT NULL,
  `Exam_Centre` varchar(33) DEFAULT NULL,
  `Serial_Certificate_No` varchar(16) DEFAULT NULL,
  `Exam_Type` varchar(13) DEFAULT NULL,
  `Exam_Name` varchar(20) NOT NULL,
  `Remarks` varchar(37) DEFAULT NULL,
  `mapping_status` int(10) DEFAULT NULL,
  `mapping_remarks` varchar(100) DEFAULT NULL,
  `takaful_operator_id` int(11) DEFAULT NULL,
  `student_profile_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `program_scheme_id` int(11) DEFAULT NULL,
  `landscape_id` int(11) DEFAULT NULL,
  `programme_exam_id` int(11) DEFAULT NULL,
  `mark_dist_setup_id` int(11) DEFAULT NULL,
  `mark_dist_master_id` int(11) DEFAULT NULL,
  `mark_dist_detail_id` int(11) DEFAULT NULL,
  `exam_setup_id` int(11) DEFAULT NULL,
  `exam_setup_detail_id` int(11) DEFAULT NULL,
  `exam_center_id` int(11) DEFAULT NULL,
  `exam_schedule_id` int(11) DEFAULT NULL,
  `examtaggingslot_id` int(11) DEFAULT NULL,
  `migrate_status` varchar(10) DEFAULT NULL,
  `migrate_remarks` varchar(200) DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `repeat_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `discount`;
CREATE TABLE `discount` (
  `dcnt_id` int(11) NOT NULL AUTO_INCREMENT,
  `dcnt_fomulir_id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `dcnt_discount_id` int(11) NOT NULL COMMENT 'tbl_discount',
  `dcnt_amount` decimal(20,2) NOT NULL,
  `dcnt_amount_adj` decimal(20,2) NOT NULL,
  `dcnt_description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dcnt_invoice_id` bigint(20) DEFAULT NULL,
  `dcnt_creator` int(11) NOT NULL,
  `dcnt_create_date` datetime NOT NULL,
  `dcnt_approve_by` int(11) NOT NULL,
  `dcnt_approve_date` datetime NOT NULL,
  `dcnt_cancel_by` int(11) NOT NULL,
  `dcnt_cancel_date` datetime NOT NULL,
  `dcnt_update_by` int(11) NOT NULL,
  `dcnt_update_date` datetime NOT NULL,
  `dcnt_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dcnt_currency_id` int(11) NOT NULL,
  `dcnt_IdStudentRegistration` int(11) DEFAULT '0',
  `dcnt_corporate_id` int(11) DEFAULT '0',
  PRIMARY KEY (`dcnt_id`),
  UNIQUE KEY `dcnt_fomulir_id` (`dcnt_fomulir_id`),
  KEY `dcnt_invoice_id` (`dcnt_invoice_id`),
  KEY `dcnt_creator` (`dcnt_creator`),
  KEY `dcnt_cancel_by` (`dcnt_cancel_by`),
  KEY `dcnt_approve_by` (`dcnt_approve_by`),
  KEY `dcnt_type_id` (`dcnt_discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `discount_detail`;
CREATE TABLE `discount_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dcnt_id` bigint(20) NOT NULL COMMENT 'fk_discount',
  `dcnt_amount` decimal(20,2) NOT NULL,
  `dcnt_invoice_id` bigint(20) DEFAULT NULL,
  `dcnt_invoice_det_id` int(11) DEFAULT NULL,
  `dcnt_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'A',
  `status_by` int(11) NOT NULL,
  `status_date` datetime NOT NULL,
  `adjustId` int(11) NOT NULL COMMENT 'pk discount_adjustment',
  PRIMARY KEY (`id`),
  KEY `dcnt_id` (`dcnt_id`),
  KEY `dcnt_invoice_id` (`dcnt_invoice_id`),
  KEY `dcnt_invoice_det_id` (`dcnt_invoice_det_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `drop_registration_log`;
CREATE TABLE `drop_registration_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `er_id` int(11) DEFAULT NULL,
  `invoice_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `function_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'external id = =student_profule',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `email_que`;
CREATE TABLE `email_que` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `er_id` int(11) NOT NULL DEFAULT '0',
  `ec_id` int(11) NOT NULL DEFAULT '0',
  `recepient_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment_filename` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_path` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comm_rec_id` bigint(10) unsigned DEFAULT NULL,
  `date_que` datetime DEFAULT NULL,
  `date_publish` datetime DEFAULT NULL,
  `date_send` datetime DEFAULT NULL,
  `retry_count` int(11) NOT NULL DEFAULT '0',
  `pull_date` datetime DEFAULT NULL,
  `pull_status` tinyint(4) DEFAULT NULL COMMENT '1:yes',
  `smtp` tinyint(4) NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_change_status`;
CREATE TABLE `exam_change_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_change_status_id',
  `IdStudentRegistration` int(11) NOT NULL,
  `examregistration_main_id` int(11) NOT NULL COMMENT 'FK exam_registration_main',
  `examregistration_id` int(11) NOT NULL COMMENT 'FK exam_registration',
  `type` int(11) NOT NULL COMMENT '1=Deferment, 2=Withdrawal/Cancel',
  `reasons` text COLLATE utf8mb4_unicode_ci,
  `old_examschedule_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_schedule',
  `new_examschedule_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_schedule',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `old_examtaggingslot_id` int(11) NOT NULL COMMENT 'exam_scheduletaggingslot',
  `new_examtaggingslot_id` int(11) NOT NULL COMMENT 'exam_scheduletaggingslot',
  `old_examcenter_id` int(11) NOT NULL COMMENT 'tbl_examcenter',
  `new_examcenter_id` int(11) NOT NULL COMMENT 'tbl_examcenter',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2018-03-05 10:12:34
