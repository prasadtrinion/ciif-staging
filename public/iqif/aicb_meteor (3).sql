-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `advance_payment`;
CREATE TABLE `advance_payment` (
  `advpy_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `advpy_appl_id` bigint(20) DEFAULT NULL,
  `advpy_trans_id` int(11) DEFAULT NULL,
  `advpy_payee_type` int(11) DEFAULT '0' COMMENT 'fk tbl_definationms',
  `advpy_corporate_id` int(11) DEFAULT '0',
  `advpy_idStudentRegistration` int(11) DEFAULT NULL,
  `advpy_acad_year_id` bigint(20) DEFAULT NULL,
  `advpy_sem_id` bigint(20) DEFAULT NULL,
  `advpy_prog_code` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advpy_fomulir` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_invoice_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advpy_invoice_id` int(11) DEFAULT NULL,
  `advpy_invoice_det_id` int(11) NOT NULL DEFAULT '0' COMMENT 'invoice_detail',
  `advpy_payment_id` bigint(20) DEFAULT NULL,
  `advpy_rcp_id` int(11) NOT NULL,
  `advpy_refund_id` bigint(20) DEFAULT NULL,
  `advpy_cur_id` int(11) NOT NULL,
  `advpy_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_amount` decimal(20,2) NOT NULL,
  `advpy_total_paid` decimal(20,2) NOT NULL,
  `advpy_total_balance` decimal(20,2) NOT NULL,
  `advpy_status` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A: active; X: inactive',
  `advpy_creator` bigint(20) NOT NULL,
  `advpy_date` date NOT NULL,
  `advpy_create_date` datetime NOT NULL,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_exchange_rate` int(11) NOT NULL,
  `advpy_approve_by` int(11) NOT NULL DEFAULT '0',
  `advpy_approve_date` datetime DEFAULT NULL,
  `advpy_cancel_by` int(11) NOT NULL,
  `advpy_cancel_date` datetime NOT NULL,
  `IdReceivableAdjustment` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_receivable_adjustment',
  PRIMARY KEY (`advpy_id`),
  KEY `advpy_appl_id` (`advpy_appl_id`),
  KEY `advpy_trans_id` (`advpy_trans_id`),
  KEY `advpy_idStudentRegistration` (`advpy_idStudentRegistration`),
  KEY `advpy_invoice_id` (`advpy_invoice_id`),
  KEY `advpy_cur_id` (`advpy_cur_id`),
  KEY `advpy_rcp_id` (`advpy_rcp_id`),
  KEY `advpy_fomulir` (`advpy_fomulir`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_program`;
CREATE TABLE `applicant_program` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `ap_at_trans_id` bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  `ap_prog_id` bigint(20) NOT NULL,
  `ap_prog_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk tbl_program',
  `ap_ptest_prog_id` int(11) DEFAULT NULL COMMENT 'aps_app_id fk to appl_placement_program(app_id)',
  `ap_preference` tinyint(2) NOT NULL DEFAULT '1',
  `mode_study` int(11) NOT NULL,
  `program_mode` int(11) NOT NULL,
  `program_type` int(11) NOT NULL,
  `ap_prog_scheme` int(11) NOT NULL DEFAULT '0',
  `upd_date` datetime NOT NULL,
  UNIQUE KEY `app_id` (`ap_id`),
  KEY `ap_at_trans_id` (`ap_at_trans_id`),
  KEY `ap_prog_code` (`ap_prog_code`),
  KEY `ap_ptest_prog_id` (`ap_ptest_prog_id`),
  KEY `ap_preference` (`ap_preference`),
  KEY `ap_id` (`ap_id`),
  KEY `ap_at_trans_id_2` (`ap_at_trans_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_qualification`;
CREATE TABLE `applicant_qualification` (
  `ae_id` int(11) NOT NULL AUTO_INCREMENT,
  `ae_appl_id` bigint(20) NOT NULL,
  `ae_transaction_id` int(11) NOT NULL,
  `ae_qualification` int(11) DEFAULT NULL COMMENT 'PK tbl_qualificationmaster',
  `ae_degree_awarded` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_majoring` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_class_degree` int(11) DEFAULT NULL,
  `ae_result` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_year_graduate` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_institution_country` int(11) DEFAULT NULL COMMENT 'fk tbl_countries',
  `ae_institution` int(11) DEFAULT NULL COMMENT 'fk.school_master',
  `ae_medium_instruction` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_order` int(11) NOT NULL,
  `others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`ae_id`),
  KEY `ae_transaction_id` (`ae_transaction_id`),
  KEY `ae_appl_id` (`ae_appl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_status_history`;
CREATE TABLE `applicant_status_history` (
  `ash_id` int(11) NOT NULL AUTO_INCREMENT,
  `ash_trans_id` int(11) NOT NULL DEFAULT '0',
  `ash_status` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ash_oldStatus` int(11) NOT NULL,
  `ash_changeType` int(11) NOT NULL COMMENT '1 : change status 2:change scheme',
  `ash_userType` int(11) NOT NULL DEFAULT '0' COMMENT '0:applicant 1:student',
  `ash_updDate` datetime NOT NULL,
  `ash_updUser` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ash_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_transaction`;
CREATE TABLE `applicant_transaction` (
  `at_trans_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `at_appl_id` int(11) NOT NULL COMMENT 'fk applicant_profile(ap_id)',
  `at_pes_id` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_appl_type` int(11) NOT NULL COMMENT '1 - placementtest 2 -HS',
  `at_academic_year` int(11) DEFAULT '3' COMMENT 'fk academic_year(ay_id)',
  `at_intake` int(11) DEFAULT NULL,
  `at_intake_prev` int(11) DEFAULT NULL,
  `at_period` int(11) DEFAULT NULL,
  `at_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'DRAFT,ENTRY,OFFERED,REJECTED,COMPLETE,INCOMPLETE,SHORTLISTED,KIV,ACCEPTED,DECLINE,ENROLLED.ARCHIEVE',
  `at_selection_status` int(11) DEFAULT '0' COMMENT '0:Waiting for dean rating 1: Waiting for rector verification 2: Waiting for approval 3:cpmleted 4: In Pool (USM)',
  `at_create_by` int(11) NOT NULL,
  `at_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `at_submit_date` datetime DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `entry_type` int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  `at_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:not paid 1:paid tobe removed x pakai dah; 2: biasiswa',
  `at_quit_status` int(4) NOT NULL DEFAULT '0' COMMENT '1: Apply Quit 2:Approved Quit 3:Reject 4:Incomplete ',
  `at_move_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk applicant_change_program',
  `at_document_verified` int(11) NOT NULL DEFAULT '0' COMMENT '0:No 1:yes',
  `at_document_verifiedby` int(11) DEFAULT NULL,
  `at_document_verifieddt` datetime DEFAULT NULL,
  `at_registration_status` int(11) DEFAULT NULL COMMENT '1: Active',
  `at_IdStudentRegistration` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_studentregistration',
  `at_registration_date` datetime DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `at_copy_status` tinyint(4) DEFAULT '0' COMMENT '0: Default 1:Copy2:Not Copy',
  `at_bookmark` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_repository` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_fs_id` int(11) DEFAULT NULL,
  `at_dp_id` int(11) unsigned DEFAULT NULL,
  `at_dp_by` int(11) unsigned DEFAULT NULL,
  `at_dp_date` datetime DEFAULT NULL,
  `at_fs_date` datetime DEFAULT NULL,
  `at_fs_by` int(11) DEFAULT NULL,
  `at_processing_fee` enum('ISSUED','PAID') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_tution_fee` enum('ISSUED','PAID') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_offer_acceptance_date` datetime DEFAULT NULL,
  `at_default_qlevel` int(11) DEFAULT NULL,
  `at_reason` int(11) DEFAULT NULL,
  `IdLandscape` bigint(20) DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '1',
  `at_remarks` longtext COLLATE utf8mb4_unicode_ci,
  `at_ref_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`at_trans_id`),
  UNIQUE KEY `at_pes_id` (`at_pes_id`),
  KEY `at_appl_id` (`at_appl_id`),
  KEY `at_appl_type` (`at_appl_type`),
  KEY `at_period` (`at_period`),
  KEY `at_status` (`at_status`),
  KEY `at_intake` (`at_intake`),
  KEY `entry_type` (`entry_type`),
  KEY `at_selection_status` (`at_selection_status`),
  KEY `at_document_verified` (`at_document_verified`),
  KEY `at_appl_id_2` (`at_appl_id`),
  KEY `at_appl_id_3` (`at_appl_id`),
  KEY `at_status_2` (`at_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_transaction_rosak`;
CREATE TABLE `applicant_transaction_rosak` (
  `at_trans_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `at_appl_id` int(11) NOT NULL,
  `at_appl_type` int(11) NOT NULL,
  `at_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'DRAFT,ENTRY,OFFERED,REJECTED,COMPLETE,INCOMPLETE,SHORTLISTED,KIV,ACCEPTED,DECLINE,ENROLLED.ARCHIEVE',
  `at_create_by` int(11) NOT NULL,
  `at_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `at_submit_date` datetime NOT NULL,
  `entry_type` int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  PRIMARY KEY (`at_trans_id`),
  KEY `at_appl_id` (`at_appl_id`),
  KEY `at_appl_type` (`at_appl_type`),
  KEY `at_status` (`at_status`),
  KEY `entry_type` (`entry_type`),
  KEY `at_appl_id_2` (`at_appl_id`),
  KEY `at_appl_id_3` (`at_appl_id`),
  KEY `at_status_2` (`at_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `assessment_type`;
CREATE TABLE `assessment_type` (
  `at_id` int(11) NOT NULL AUTO_INCREMENT,
  `at_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_description` text COLLATE utf8mb4_unicode_ci,
  `at_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_markentry_readonly` tinyint(4) DEFAULT '0',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddt` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `attendance_mark`;
CREATE TABLE `attendance_mark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL COMMENT 'FK tbl_studentregistration',
  `IdProgram` int(11) NOT NULL COMMENT 'FK tbl_program',
  `IdSubject` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_subjectmaster',
  `coursegroup_id` int(11) NOT NULL COMMENT 'FK course_group',
  `coursegroupschedule_id` int(11) NOT NULL COMMENT 'FK course_group_schedule',
  `marks` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'in percentage',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_exam_registration`;
CREATE TABLE `batch_exam_registration` (
  `ber_id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'batch_no',
  `corporate_id` int(11) NOT NULL COMMENT 'tbl_takafuloperator',
  `inhouse` int(11) NOT NULL DEFAULT '0' COMMENT '0:Non-Inhouse 1:Inhouse',
  `IdProgram` int(11) NOT NULL,
  `IdLandscape` int(11) NOT NULL,
  `examsetup_id` int(11) NOT NULL COMMENT 'exam_setup.es_id',
  `type_nationality` int(11) NOT NULL DEFAULT '579',
  `total_student` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee_structure.fs_id',
  `submitted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes',
  `paid_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Unpaid 1:Paid',
  `paymentmode` int(11) NOT NULL DEFAULT '0',
  `at_processing_fee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_role` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `tbe_id` int(11) DEFAULT NULL COMMENT 'tbe.idBatchRegistration',
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_updatedate` datetime DEFAULT NULL COMMENT 'to update wrong id',
  PRIMARY KEY (`ber_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_exam_registration_history`;
CREATE TABLE `batch_exam_registration_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ber_id` int(11) NOT NULL,
  `batch_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'batch_no',
  `corporate_id` int(11) NOT NULL COMMENT 'tbl_takafuloperator',
  `inhouse` int(11) NOT NULL DEFAULT '0' COMMENT '0:Non-Inhouse 1:Inhouse',
  `IdProgram` int(11) NOT NULL,
  `IdLandscape` int(11) NOT NULL,
  `examsetup_id` int(11) NOT NULL COMMENT 'exam_setup.es_id',
  `type_nationality` int(11) NOT NULL DEFAULT '579',
  `total_student` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee_structure.fs_id',
  `submitted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes',
  `paid_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Unpaid 1:Paid',
  `paymentmode` int(11) NOT NULL DEFAULT '0',
  `at_processing_fee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_role` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `tbe_id` int(11) DEFAULT NULL COMMENT 'tbe.idBatchRegistration',
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_updatedate` datetime DEFAULT NULL COMMENT 'to update wrong id',
  `remarks` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdStudentRegSubjects` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_exam_schedule`;
CREATE TABLE `batch_exam_schedule` (
  `bes_id` int(11) NOT NULL AUTO_INCREMENT,
  `ber_id` int(11) NOT NULL COMMENT 'batch_exam_registration.ber_id',
  `es_id` int(11) NOT NULL COMMENT 'exam_schedule.es_id',
  `examtaggingslot_id` int(11) NOT NULL,
  `examcenter_id` int(11) NOT NULL,
  `individual` int(11) NOT NULL DEFAULT '0' COMMENT '0:Main Schedule, 1:Individual',
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `tbe_migratedate` datetime NOT NULL,
  PRIMARY KEY (`bes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_registration`;
CREATE TABLE `batch_registration` (
  `btch_id` int(11) NOT NULL AUTO_INCREMENT,
  `btch_corporate_id` int(11) NOT NULL,
  `btch_upl_id` int(11) NOT NULL,
  `btch_no` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `btch_program_id` int(11) NOT NULL,
  `btch_scheme_id` int(11) NOT NULL,
  `btch_intake_id` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `btch_landscape_id` int(11) NOT NULL,
  `btch_contact_person` int(11) NOT NULL COMMENT 'fk takaful_operator',
  `btch_type_nationality` int(11) DEFAULT NULL,
  `number_candidate` int(11) NOT NULL,
  `number_candidate_maximum` int(11) NOT NULL DEFAULT '0',
  `number_candidate_accepted` int(11) NOT NULL DEFAULT '0',
  `upload` int(11) NOT NULL COMMENT '1=excel,0=no',
  `closing_date` date DEFAULT '0000-00-00' COMMENT 'last date to add/edit participant',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `btch_fs_id` int(11) NOT NULL,
  `fs_id_upddate` datetime NOT NULL,
  `at_processing_fee` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_mode` int(11) DEFAULT NULL COMMENT 'fk payment_mode',
  `payment_mode_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1=done',
  `last_step` int(11) DEFAULT NULL COMMENT 'tbl_definationtypems 196',
  `step_date` datetime DEFAULT NULL,
  `btch_country_id` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `manual` int(11) DEFAULT '0' COMMENT '1=manual entry sms',
  `candidate_changes` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No Changes 1=Has Changes',
  `lms_access` int(11) DEFAULT '0' COMMENT '1=yes, 0=no ',
  `inhouse` int(11) DEFAULT '0' COMMENT '1=inhouse,0=normal ',
  `inhouse_status` int(11) DEFAULT '1' COMMENT '1=active, 0=inactive',
  `sendmail_date` datetime DEFAULT NULL,
  PRIMARY KEY (`btch_id`),
  KEY `btch_corporate_id` (`btch_corporate_id`),
  KEY `btch_upl_id` (`btch_upl_id`),
  KEY `btch_program_id` (`btch_program_id`),
  KEY `btch_scheme_id` (`btch_scheme_id`),
  KEY `btch_intake_id` (`btch_intake_id`),
  KEY `btch_landscape_id` (`btch_landscape_id`),
  KEY `btch_fs_id` (`btch_fs_id`),
  KEY `payment_mode` (`payment_mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `batch_registration_course`;
CREATE TABLE `batch_registration_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `btch_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `IdCourseTaggingGroup` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_registration_schedule`;
CREATE TABLE `batch_registration_schedule` (
  `brs_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment',
  `br_id` int(11) NOT NULL COMMENT 'batch_registration.btch_id',
  `cgs_id` int(11) NOT NULL COMMENT 'course_group_schedule.sc_id',
  `course_id` int(11) NOT NULL DEFAULT '0' COMMENT 'perlu ke ada field ni??',
  `individual` int(11) NOT NULL COMMENT '0:Main Schedule, 1:Individual',
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive, 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`brs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_compose`;
CREATE TABLE `comm_compose` (
  `comp_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `comp_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_tpl_id` int(10) unsigned NOT NULL,
  `comp_type` smallint(10) unsigned NOT NULL,
  `comp_rectype` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'applicants',
  `comp_lang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_totalrecipients` int(10) unsigned NOT NULL,
  `comp_rec` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`comp_id`),
  KEY `comp_module` (`comp_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_compose_recipients`;
CREATE TABLE `comm_compose_recipients` (
  `cr_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `cr_comp_id` bigint(11) unsigned NOT NULL,
  `cr_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cr_content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cr_rec_id` int(10) unsigned NOT NULL,
  `cr_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_attachment_id` int(10) unsigned DEFAULT NULL,
  `cr_attachment_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_attachment_filename` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cr_datesent` datetime DEFAULT NULL,
  PRIMARY KEY (`cr_id`),
  KEY `cr_comp_id` (`cr_comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_groups`;
CREATE TABLE `comm_groups` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_rectype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_type` int(11) NOT NULL DEFAULT '0',
  `group_status` int(11) NOT NULL,
  `group_description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_recipients` smallint(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  KEY `module` (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_groups_recipients`;
CREATE TABLE `comm_groups_recipients` (
  `rec_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rec_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `recipient_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rec_id`),
  KEY `group_id` (`group_id`,`recipient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_template`;
CREATE TABLE `comm_template` (
  `tpl_id` int(10) NOT NULL AUTO_INCREMENT,
  `tpl_module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_type` smallint(10) unsigned NOT NULL COMMENT 'fk tbl_definations (type:121)',
  `tpl_categories` mediumtext COLLATE utf8mb4_unicode_ci,
  `email_from_name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `tpl_pname` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`tpl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_template_content`;
CREATE TABLE `comm_template_content` (
  `content_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tpl_id` int(11) unsigned NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_content` mediumtext COLLATE utf8mb4_unicode_ci COMMENT '1: Standard Doc-> URL Lain2 Type -> html content',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_template_tags`;
CREATE TABLE `comm_template_tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tpl_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_value` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group`;
CREATE TABLE `course_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'fk tbl_subjectmaster',
  `maxstud` int(11) NOT NULL,
  `IdLecturer` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IdSubject` (`IdSubject`),
  KEY `IdLecturer` (`IdLecturer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_attendance`;
CREATE TABLE `course_group_attendance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSchedule` bigint(20) NOT NULL COMMENT 'fk course_group_schedule_detail',
  `IdStudentRegistration` bigint(20) unsigned NOT NULL COMMENT 'fk tbl_studentregistration',
  `status` int(11) DEFAULT NULL COMMENT 'fk_tbl_definationms 91',
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_program`;
CREATE TABLE `course_group_program` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `program_id` bigint(20) NOT NULL,
  `program_scheme_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`,`program_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_schedule`;
CREATE TABLE `course_group_schedule` (
  `sc_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdSchedule` int(11) NOT NULL COMMENT 'fk course_group_schedule',
  `IdScheduleParent` int(11) DEFAULT '0',
  `idClassType` int(11) DEFAULT NULL,
  `sc_date_start` date NOT NULL,
  `sc_date_end` date NOT NULL,
  `sc_duration` int(11) NOT NULL,
  `sc_start_time` time NOT NULL,
  `sc_end_time` time NOT NULL,
  `sc_venue` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sc_address` text COLLATE utf8mb4_unicode_ci,
  `sc_class` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sc_day` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sc_remark` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdLecturer` int(11) DEFAULT NULL,
  `sc_createdby` int(11) NOT NULL,
  `sc_createddt` datetime NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `sc_visibility` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `sc_custom` int(11) NOT NULL,
  PRIMARY KEY (`sc_id`),
  KEY `IdSchedule` (`IdSchedule`),
  KEY `idClassType` (`idClassType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_schedule_detail`;
CREATE TABLE `course_group_schedule_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scDetId` int(11) NOT NULL COMMENT 'fk course_group_schedule_detail',
  `sc_date` date NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scDetId` (`scDetId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_language`;
CREATE TABLE `course_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseid` varchar(50) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `credit_note`;
CREATE TABLE `credit_note` (
  `cn_trans_id` int(11) DEFAULT NULL,
  `cn_corporate_id` int(11) DEFAULT '0',
  `cn_IdStudentRegistration` int(11) DEFAULT NULL,
  `cn_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cn_billing_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_fomulir` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_appl_id` bigint(20) DEFAULT NULL,
  `cn_amount` decimal(20,2) NOT NULL,
  `cn_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_creator` bigint(20) NOT NULL,
  `cn_create_date` datetime NOT NULL,
  `cn_approver` bigint(20) DEFAULT NULL,
  `cn_approve_date` datetime DEFAULT NULL,
  `cn_cancel_by` bigint(20) DEFAULT NULL,
  `cn_cancel_date` datetime DEFAULT NULL,
  `cn_cur_id` int(11) NOT NULL DEFAULT '1',
  `cn_source` int(11) NOT NULL COMMENT '0=sms, 1=student portal',
  `cn_invoice_id` int(11) NOT NULL,
  `cn_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT 'A=active/approved, X=cancel,0=entry',
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cn_type` int(11) NOT NULL,
  `type_amount` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cn_exam_registration_main_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cn_id`),
  KEY `cn_fomulir` (`cn_fomulir`),
  KEY `appl_id` (`cn_appl_id`),
  KEY `cn_trans_id` (`cn_trans_id`),
  KEY `cn_IdStudentRegistration` (`cn_IdStudentRegistration`),
  KEY `cn_cur_id` (`cn_cur_id`),
  KEY `cn_invoice_id` (`cn_invoice_id`),
  KEY `cn_billing_no` (`cn_billing_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `credit_note_detail`;
CREATE TABLE `credit_note_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cn_id` int(11) NOT NULL,
  `invoice_main_id` int(11) NOT NULL,
  `invoice_detail_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `cur_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cn_fomulir` (`invoice_main_id`),
  KEY `appl_id` (`invoice_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cron_ol_exam`;
CREATE TABLE `cron_ol_exam` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `es_date` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_id` int(11) DEFAULT NULL,
  `results` mediumtext COLLATE utf8mb4_unicode_ci COMMENT 'if any results needed',
  `created_date` datetime NOT NULL,
  `start_date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `executed_date` datetime DEFAULT NULL,
  `executed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `execution_time` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cron_ol_exam_tempdata`;
CREATE TABLE `cron_ol_exam_tempdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coe_id` int(11) NOT NULL COMMENT 'cron_ol_exam',
  `es_id` int(11) DEFAULT NULL COMMENT 'exam_setup',
  `esd_id` int(11) DEFAULT NULL COMMENT 'exam_setup_detail',
  `ec_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL COMMENT 'exam_schedule',
  `scheduletaggingslot_id` int(11) DEFAULT NULL COMMENT 'exam_scheduletaggingslot',
  `idExam` int(11) DEFAULT NULL,
  `total_set` int(11) DEFAULT NULL,
  `total_student` int(11) DEFAULT NULL,
  `total_student_generated` int(11) DEFAULT NULL,
  `pe_id` int(11) DEFAULT NULL,
  `idmarkdistribution` int(11) DEFAULT NULL,
  `results` text,
  `executed` int(11) DEFAULT NULL COMMENT '1=yes,0=no',
  `ep_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `executed_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `data_migration_exam_result_4`;
CREATE TABLE `data_migration_exam_result_4` (
  `No` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(61) DEFAULT NULL,
  `Mode` varchar(11) DEFAULT NULL,
  `Type` varchar(8) DEFAULT NULL,
  `Category` varchar(13) DEFAULT NULL,
  `Code_Org` varchar(15) DEFAULT NULL,
  `Organization` varchar(59) DEFAULT NULL,
  `Nric` varchar(16) DEFAULT NULL,
  `Program` varchar(62) DEFAULT NULL,
  `Program_Code` varchar(7) DEFAULT NULL,
  `Exam_Date` varchar(15) DEFAULT NULL,
  `Grade` varchar(16) DEFAULT NULL,
  `Marks` varchar(2) DEFAULT NULL,
  `Passing_Marks` varchar(2) DEFAULT NULL,
  `Award` varchar(21) DEFAULT NULL,
  `Date_of_award` varchar(10) DEFAULT NULL,
  `Exam_Centre` varchar(33) DEFAULT NULL,
  `Serial_Certificate_No` varchar(16) DEFAULT NULL,
  `Exam_Type` varchar(13) DEFAULT NULL,
  `Exam_Name` varchar(20) NOT NULL,
  `Remarks` varchar(37) DEFAULT NULL,
  `mapping_status` int(10) DEFAULT NULL,
  `mapping_remarks` varchar(100) DEFAULT NULL,
  `takaful_operator_id` int(11) DEFAULT NULL,
  `student_profile_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `program_scheme_id` int(11) DEFAULT NULL,
  `landscape_id` int(11) DEFAULT NULL,
  `programme_exam_id` int(11) DEFAULT NULL,
  `mark_dist_setup_id` int(11) DEFAULT NULL,
  `mark_dist_master_id` int(11) DEFAULT NULL,
  `mark_dist_detail_id` int(11) DEFAULT NULL,
  `exam_setup_id` int(11) DEFAULT NULL,
  `exam_setup_detail_id` int(11) DEFAULT NULL,
  `exam_center_id` int(11) DEFAULT NULL,
  `exam_schedule_id` int(11) DEFAULT NULL,
  `examtaggingslot_id` int(11) DEFAULT NULL,
  `migrate_status` varchar(10) DEFAULT NULL,
  `migrate_remarks` varchar(200) DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `repeat_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `discount`;
CREATE TABLE `discount` (
  `dcnt_id` int(11) NOT NULL AUTO_INCREMENT,
  `dcnt_fomulir_id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `dcnt_discount_id` int(11) NOT NULL COMMENT 'tbl_discount',
  `dcnt_amount` decimal(20,2) NOT NULL,
  `dcnt_amount_adj` decimal(20,2) NOT NULL,
  `dcnt_description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dcnt_invoice_id` bigint(20) DEFAULT NULL,
  `dcnt_creator` int(11) NOT NULL,
  `dcnt_create_date` datetime NOT NULL,
  `dcnt_approve_by` int(11) NOT NULL,
  `dcnt_approve_date` datetime NOT NULL,
  `dcnt_cancel_by` int(11) NOT NULL,
  `dcnt_cancel_date` datetime NOT NULL,
  `dcnt_update_by` int(11) NOT NULL,
  `dcnt_update_date` datetime NOT NULL,
  `dcnt_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dcnt_currency_id` int(11) NOT NULL,
  `dcnt_IdStudentRegistration` int(11) DEFAULT '0',
  `dcnt_corporate_id` int(11) DEFAULT '0',
  PRIMARY KEY (`dcnt_id`),
  UNIQUE KEY `dcnt_fomulir_id` (`dcnt_fomulir_id`),
  KEY `dcnt_invoice_id` (`dcnt_invoice_id`),
  KEY `dcnt_creator` (`dcnt_creator`),
  KEY `dcnt_cancel_by` (`dcnt_cancel_by`),
  KEY `dcnt_approve_by` (`dcnt_approve_by`),
  KEY `dcnt_type_id` (`dcnt_discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `discount_detail`;
CREATE TABLE `discount_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dcnt_id` bigint(20) NOT NULL COMMENT 'fk_discount',
  `dcnt_amount` decimal(20,2) NOT NULL,
  `dcnt_invoice_id` bigint(20) DEFAULT NULL,
  `dcnt_invoice_det_id` int(11) DEFAULT NULL,
  `dcnt_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'A',
  `status_by` int(11) NOT NULL,
  `status_date` datetime NOT NULL,
  `adjustId` int(11) NOT NULL COMMENT 'pk discount_adjustment',
  PRIMARY KEY (`id`),
  KEY `dcnt_id` (`dcnt_id`),
  KEY `dcnt_invoice_id` (`dcnt_invoice_id`),
  KEY `dcnt_invoice_det_id` (`dcnt_invoice_det_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `drop_registration_log`;
CREATE TABLE `drop_registration_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `er_id` int(11) DEFAULT NULL,
  `invoice_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `function_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'external id = =student_profule',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `email_que`;
CREATE TABLE `email_que` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `er_id` int(11) NOT NULL DEFAULT '0',
  `ec_id` int(11) NOT NULL DEFAULT '0',
  `recepient_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment_filename` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_path` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comm_rec_id` bigint(10) unsigned DEFAULT NULL,
  `date_que` datetime DEFAULT NULL,
  `date_publish` datetime DEFAULT NULL,
  `date_send` datetime DEFAULT NULL,
  `retry_count` int(11) NOT NULL DEFAULT '0',
  `pull_date` datetime DEFAULT NULL,
  `pull_status` tinyint(4) DEFAULT NULL COMMENT '1:yes',
  `smtp` tinyint(4) NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_change_status`;
CREATE TABLE `exam_change_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_change_status_id',
  `IdStudentRegistration` int(11) NOT NULL,
  `examregistration_main_id` int(11) NOT NULL COMMENT 'FK exam_registration_main',
  `examregistration_id` int(11) NOT NULL COMMENT 'FK exam_registration',
  `type` int(11) NOT NULL COMMENT '1=Deferment, 2=Withdrawal/Cancel',
  `reasons` text COLLATE utf8mb4_unicode_ci,
  `old_examschedule_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_schedule',
  `new_examschedule_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_schedule',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `old_examtaggingslot_id` int(11) NOT NULL COMMENT 'exam_scheduletaggingslot',
  `new_examtaggingslot_id` int(11) NOT NULL COMMENT 'exam_scheduletaggingslot',
  `old_examcenter_id` int(11) NOT NULL COMMENT 'tbl_examcenter',
  `new_examcenter_id` int(11) NOT NULL COMMENT 'tbl_examcenter',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_registration`;
CREATE TABLE `exam_registration` (
  `er_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_login` int(11) NOT NULL DEFAULT '0',
  `er_registration_type` int(11) NOT NULL COMMENT '0:pre-register 1:Register 2: Drop 3:Withdraw 4:Repeat 5:Refer 6:CT 9:Pre-Repeat',
  `reschedule` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=false, 1=true',
  `must_reschedule` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=false, 1=true',
  `er_main_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_registration_main',
  `registrationId` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ber_id` int(11) NOT NULL DEFAULT '0' COMMENT 'batch exam registration id',
  `bes_id` int(11) NOT NULL DEFAULT '0',
  `landscape_id` int(11) DEFAULT NULL,
  `intakeprogram_id` int(11) DEFAULT NULL,
  `student_id` int(11) NOT NULL COMMENT 'tbl_studentregistration',
  `examsetup_id` int(11) NOT NULL,
  `examsetupdetail_id` int(11) NOT NULL,
  `examschedule_id` int(11) NOT NULL,
  `examtaggingslot_id` int(11) NOT NULL COMMENT 'exam_scheduletaggingslot',
  `examcenter_id` int(11) NOT NULL COMMENT 'tbl_examcenter',
  `examroom_id` int(11) NOT NULL DEFAULT '0',
  `seat_no` int(11) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:pending 1:paid:3:payment cancelled (tbe)',
  `total_marks` double DEFAULT NULL,
  `pass` tinyint(4) DEFAULT NULL COMMENT '0:Fail 1:Pass',
  `grade_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '0:Fail 1:Pass',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifieddt` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `exam_status` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'C: Completed IN:Incomplete MG:Missing Grade F:Fraud NR:No Record',
  `attendance_status` int(11) DEFAULT '0' COMMENT 'FK definationms',
  `attendance_updby` int(11) DEFAULT NULL,
  `attendance_upddt` datetime DEFAULT NULL,
  `attendance_updrole` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `push_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: No 1:yes',
  `pull_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '	1:yes 2:No (default 0 no pending payment)',
  `mc_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result_status` int(11) DEFAULT '0' COMMENT '0:Entry 1:Pending 2:Approved',
  `result_endorsed_by` int(11) NOT NULL DEFAULT '0',
  `result_endorsed_date` datetime DEFAULT NULL,
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee structure',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `IdProgram` int(11) DEFAULT NULL,
  `pull_status` tinyint(4) DEFAULT NULL COMMENT '1:yes',
  `pull_date` datetime DEFAULT NULL,
  `award` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `award_date` date DEFAULT NULL,
  `Serial_Certificate_No` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_id` int(11) DEFAULT NULL,
  `tbe_examresult_migratedt` datetime DEFAULT NULL,
  `IdStudentRegSubjects` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`er_id`),
  KEY `student_id` (`student_id`),
  KEY `examsetup_id` (`examsetup_id`),
  KEY `examsetupdetail_id` (`examsetupdetail_id`),
  KEY `examschedule_id` (`examschedule_id`),
  KEY `examtaggingslot_id` (`examtaggingslot_id`),
  KEY `examcenter_id` (`examcenter_id`),
  KEY `examroom_id` (`examroom_id`),
  KEY `IdProgram` (`IdProgram`),
  KEY `er_main_id` (`er_main_id`),
  KEY `ber_id` (`ber_id`),
  KEY `bes_id` (`bes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_registration_cycle`;
CREATE TABLE `exam_registration_cycle` (
  `erc_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) unsigned NOT NULL,
  `cycle_no` int(10) unsigned NOT NULL DEFAULT '0',
  `cycle_status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '1: Complete 6 sitting based on landscape',
  `first_exam_sitting` int(10) unsigned NOT NULL COMMENT 'fk exam_sitting',
  `erc_createddt` datetime NOT NULL,
  `erc_createdby` int(11) NOT NULL,
  PRIMARY KEY (`erc_id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `exam_registration_history`;
CREATE TABLE `exam_registration_history` (
  `erh_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_login` int(11) DEFAULT NULL,
  `er_id` int(11) NOT NULL,
  `IdProgram` int(11) DEFAULT NULL,
  `registrationId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `er_registration_type` int(11) NOT NULL,
  `reschedule` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=false, 1=true',
  `must_reschedule` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=false, 1=true',
  `er_main_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_registration_main',
  `ber_id` int(11) DEFAULT '0',
  `bes_id` int(11) NOT NULL DEFAULT '0',
  `landscape_id` int(11) DEFAULT NULL,
  `intakeprogram_id` int(11) DEFAULT NULL,
  `student_id` int(11) NOT NULL COMMENT 'PK tbl_studentregistration',
  `examsetup_id` int(11) NOT NULL,
  `examsetupdetail_id` int(11) NOT NULL,
  `examschedule_id` int(11) NOT NULL,
  `examtaggingslot_id` int(11) NOT NULL COMMENT 'exam_scheduletaggingslot',
  `examcenter_id` int(11) NOT NULL COMMENT 'tbl_examcenter',
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `examroom_id` int(11) NOT NULL DEFAULT '0',
  `seat_no` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `total_marks` double DEFAULT NULL,
  `pass` tinyint(4) DEFAULT NULL,
  `grade_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exam_status` tinyint(4) DEFAULT '0' COMMENT '0: Draft, 1: Pending for approval, 2: approved',
  `attendance_status` int(11) DEFAULT '0' COMMENT 'FK definationms',
  `attendance_updby` int(11) DEFAULT NULL,
  `attendance_upddt` datetime DEFAULT NULL,
  `attendance_updrole` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `push_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: No 1:yes',
  `pull_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '	1:yes 2:No (default 0 no pending payment)',
  `mc_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifieddt` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `result_status` int(11) DEFAULT '0' COMMENT '0:Entry 1:Pending 2:Approved',
  `result_endorsed_by` int(11) NOT NULL DEFAULT '0',
  `result_endorsed_date` datetime DEFAULT NULL,
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `pull_status` int(11) DEFAULT NULL,
  `pull_date` datetime DEFAULT NULL,
  `award` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `award_date` datetime DEFAULT NULL,
  `Serial_Certificate_No` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_id` int(10) DEFAULT '0',
  `tbe_examresult_migratedt` datetime DEFAULT NULL,
  `IdStudentRegSubjects` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`erh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exam_registration_log`;
CREATE TABLE `exam_registration_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `er_id` int(11) DEFAULT NULL,
  `function_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_registration_main`;
CREATE TABLE `exam_registration_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL,
  `IdProgram` int(11) NOT NULL,
  `ber_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK batch_exam_registration',
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Inactive 1=Active',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  `erc_id` int(11) NOT NULL DEFAULT '0',
  `exam_sitting_no` int(11) NOT NULL DEFAULT '0',
  `exam_sitting_id` int(11) NOT NULL DEFAULT '0',
  `exam_sitting_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:Active 0:Not active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_schedule`;
CREATE TABLE `exam_schedule` (
  `es_id` int(11) NOT NULL AUTO_INCREMENT,
  `es_examsetup_id` int(11) NOT NULL COMMENT 'exam_setup',
  `es_esd_id` int(11) NOT NULL COMMENT 'exam_setup_detail',
  `es_course` int(11) NOT NULL,
  `es_exam_center` int(11) DEFAULT NULL,
  `es_date` date DEFAULT NULL,
  `es_time` time DEFAULT NULL,
  `es_session` int(11) DEFAULT NULL,
  `es_start_time` time DEFAULT NULL,
  `es_end_time` time DEFAULT NULL,
  `es_createddt` datetime DEFAULT NULL,
  `es_publish_start_date` date DEFAULT NULL,
  `es_publish_start_time` time DEFAULT NULL,
  `es_publish_immediately` tinyint(4) NOT NULL DEFAULT '0',
  `es_createdby` int(11) DEFAULT NULL,
  `es_modifydt` datetime NOT NULL,
  `es_modifyby` int(11) NOT NULL,
  `es_active` int(11) NOT NULL,
  `corporate_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`es_id`),
  KEY `es_examsetup_id` (`es_examsetup_id`),
  KEY `es_esd_id` (`es_esd_id`),
  KEY `es_date` (`es_date`),
  KEY `es_exam_center` (`es_exam_center`),
  KEY `es_course` (`es_course`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_scheduletaggingslot`;
CREATE TABLE `exam_scheduletaggingslot` (
  `sts_id` int(11) NOT NULL AUTO_INCREMENT,
  `sts_schedule_id` int(11) NOT NULL COMMENT 'exam_schedule',
  `sts_slot_id` int(11) NOT NULL COMMENT 'exam_slot',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`sts_id`),
  KEY `sts_schedule_id` (`sts_schedule_id`),
  KEY `sts_slot_id` (`sts_slot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_taggingslotcenterroom`;
CREATE TABLE `exam_taggingslotcenterroom` (
  `tsc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsc_slotid` int(11) NOT NULL COMMENT 'exam_slot',
  `tsc_examcenterid` int(11) NOT NULL COMMENT 'tbl_exam_center',
  `tsc_room_id` int(11) DEFAULT NULL COMMENT 'tbl_exam_room',
  `createdby` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`tsc_id`),
  KEY `tsc_slotid` (`tsc_slotid`),
  KEY `tsc_examcenterid` (`tsc_examcenterid`),
  KEY `tsc_room_id` (`tsc_room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_item_country`;
CREATE TABLE `fee_item_country` (
  `fic_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fic_fi_id` int(10) unsigned NOT NULL,
  `fic_idCountry` int(10) unsigned NOT NULL,
  `fic_cur_id` smallint(5) unsigned NOT NULL,
  `fic_amount` decimal(11,2) NOT NULL,
  `fic_added_by` int(11) unsigned DEFAULT NULL,
  `fic_added_date` datetime DEFAULT NULL,
  `fic_updated_by` int(11) unsigned DEFAULT NULL,
  `fic_updated_date` datetime DEFAULT NULL,
  `fic_year` int(11) DEFAULT '1',
  PRIMARY KEY (`fic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `fee_item_history`;
CREATE TABLE `fee_item_history` (
  `fi_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fi_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fi_name_bahasa` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fi_name_short` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fi_code` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fi_amount_calculation_type` int(11) NOT NULL COMMENT 'fk_tbl_definationms (Fee Item Calculation Type)',
  `fi_frequency_mode` int(11) NOT NULL COMMENT 'fk_tbl_definationms (Fee Item Frequency Mode)',
  `fi_fc_id` int(11) NOT NULL COMMENT 'FK tbl_fee_category id',
  `fi_ac_id` int(11) NOT NULL COMMENT 'fk tbl_account_code',
  `fi_refundable` tinyint(4) NOT NULL DEFAULT '0',
  `fi_non_invoice` tinyint(4) NOT NULL DEFAULT '0',
  `fi_active` tinyint(4) NOT NULL DEFAULT '1',
  `fi_exclude` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `fi_gst` int(11) DEFAULT NULL,
  `fi_gst_tax` int(11) DEFAULT NULL,
  `fi_effective_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fi_amount_calculation_type` (`fi_amount_calculation_type`),
  KEY `fi_frequency_mode` (`fi_frequency_mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_item_state`;
CREATE TABLE `fee_item_state` (
  `fis_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fic_fi_id` int(10) unsigned NOT NULL,
  `fic_idCountry` int(10) unsigned NOT NULL,
  `fic_idState` int(11) NOT NULL,
  `fic_cur_id` smallint(5) unsigned NOT NULL,
  `fic_amount` decimal(11,2) NOT NULL,
  `fic_added_by` int(11) unsigned DEFAULT NULL,
  `fic_added_date` datetime DEFAULT NULL,
  `fic_updated_by` int(11) unsigned DEFAULT NULL,
  `fic_updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`fis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `fee_structure`;
CREATE TABLE `fee_structure` (
  `fs_id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fs_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `fs_effective_date` date NOT NULL,
  `fs_intake_start` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `fs_intake_end` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `fs_semester_start` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `fs_student_category` int(11) NOT NULL COMMENT 'fl_tbl_definationms (Fee Structure Student Category)',
  `fs_cur_id` int(11) NOT NULL COMMENT 'fk tbl_currency',
  `fs_ac_id` bigint(20) NOT NULL,
  `fs_estimated_total_fee` decimal(20,2) DEFAULT NULL,
  `fs_cp` int(10) unsigned DEFAULT NULL,
  `fs_prepayment_ac` int(11) DEFAULT NULL,
  `fs_status` int(11) NOT NULL COMMENT '1=active, 0=draft, 2=inactive',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `upd_date` datetime NOT NULL,
  `upd_by` int(11) NOT NULL,
  `fs_IdProgram` int(11) DEFAULT NULL,
  PRIMARY KEY (`fs_id`),
  KEY `fs_intake_start` (`fs_intake_start`),
  KEY `fs_intake_end` (`fs_intake_end`),
  KEY `fs_student_category` (`fs_student_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_item`;
CREATE TABLE `fee_structure_item` (
  `fsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsi_structure_id` int(11) NOT NULL COMMENT 'fk_fee_structure',
  `fsi_item_id` int(11) NOT NULL COMMENT 'fk_fee_item',
  `fsi_amount` decimal(11,2) NOT NULL,
  `fsi_cur_id` bigint(20) NOT NULL COMMENT 'FK tbl_currency',
  `fsi_registration_item_id` int(11) NOT NULL COMMENT 'Registration item',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`fsi_id`),
  KEY `fsi_cur_id` (`fsi_cur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_program_course`;
CREATE TABLE `fee_structure_program_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_id` int(11) NOT NULL COMMENT 'fk fee_structure',
  `fsp_id` int(11) NOT NULL COMMENT 'fk fee_structure_program',
  `course_id` int(11) NOT NULL DEFAULT '0',
  `exam_id` int(11) NOT NULL DEFAULT '0' COMMENT 'programme_exam',
  `member_id` int(11) DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1=course, 2=exam',
  `cur_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fpx_order`;
CREATE TABLE `fpx_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ol_id` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ENTRY','PENDING','SUCCESS','FAIL') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ENTRY',
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `registration_item_id` int(11) NOT NULL DEFAULT '889',
  `program_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `fpx_buyerAccNo` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerBankBranch` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerBankId` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerEmail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerIban` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerId` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerName` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_checkSum` text COLLATE utf8mb4_unicode_ci,
  `fpx_makerName` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_msgToken` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_msgType` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_productDesc` text COLLATE utf8mb4_unicode_ci,
  `fpx_sellerBankCode` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_sellerExId` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_sellerExOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_sellerOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_sellerTxnTime` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_txnAmount` decimal(20,2) DEFAULT NULL,
  `fpx_txnCurrency` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_version` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_sellerId` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_pay` int(2) NOT NULL DEFAULT '0',
  `fpx_inprogresspay` int(11) NOT NULL DEFAULT '0',
  `fpx_inprogresspay_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fpx_order_detail`;
CREATE TABLE `fpx_order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `proforma_invoice_id` int(11) NOT NULL DEFAULT '0',
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fpx_order_history`;
CREATE TABLE `fpx_order_history` (
  `his_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(11) unsigned NOT NULL,
  `ol_id` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_no` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ENTRY','PENDING','SUCCESS','FAIL') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ENTRY',
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `registration_item_id` int(11) NOT NULL DEFAULT '889',
  `program_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fpx_transaction`;
CREATE TABLE `fpx_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `fpx_creditAuthNo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mandatory if authcode 00',
  `fpx_msgToken` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_sellerOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_creditAuthCode` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_fpxTxnTime` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_makerName` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'optional',
  `fpx_debitAuthNo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mandatory if authcode 00',
  `fpx_txnAmount` decimal(16,2) NOT NULL,
  `fpx_sellerExId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerBankId` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_msgType` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_checkSum` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_sellerExOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerName` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fpx_sellerTxnTime` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_sellerId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerIban` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_debitAuthCode` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerId` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerBankBranch` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_fpxTxnId` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL COMMENT 'server timestamp',
  `updated_date` datetime DEFAULT NULL COMMENT 'requery',
  `transaction_date` datetime NOT NULL COMMENT 'from fpxTxnTime',
  `manual` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `rcp_id` int(11) DEFAULT NULL COMMENT 'FK receipt',
  `rcp_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='schema according to merchant spec 2.1';


DROP TABLE IF EXISTS `fpx_transaction_history`;
CREATE TABLE `fpx_transaction_history` (
  `ids` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `fpx_sellerOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_debitAuthCode` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_creditAuthNo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mandatory if authcode 00',
  `fpx_msgToken` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_creditAuthCode` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_fpxTxnTime` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_makerName` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'optional',
  `fpx_debitAuthNo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mandatory if authcode 00',
  `fpx_txnAmount` decimal(16,2) NOT NULL,
  `fpx_sellerExId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerBankId` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_msgType` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_checkSum` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_sellerExOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerName` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fpx_sellerTxnTime` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_sellerId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerIban` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerId` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerBankBranch` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_fpxTxnId` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL COMMENT 'server timestamp',
  `updated_date` datetime DEFAULT NULL COMMENT 'requery',
  `transaction_date` datetime NOT NULL COMMENT 'from fpxTxnTime',
  `manual` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `rcp_id` int(11) DEFAULT NULL COMMENT 'FK receipt',
  `rcp_date` datetime DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `as_fid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_txnCurrency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='schema according to merchant spec 2.1';


DROP TABLE IF EXISTS `ilms_user`;
CREATE TABLE `ilms_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_record_registration_date` datetime DEFAULT NULL,
  `training_record_completion_date` datetime DEFAULT NULL,
  `correction_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_record` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correction` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci,
  `remark_takafuloperator` text COLLATE utf8mb4_unicode_ci,
  `remark_studentregistration` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `ilms_user_bak`;
CREATE TABLE `ilms_user_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_record_registration_date` datetime DEFAULT NULL,
  `training_record_completion_date` datetime DEFAULT NULL,
  `correction_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_record` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correction` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci,
  `remark_takafuloperator` text COLLATE utf8mb4_unicode_ci,
  `remark_studentregistration` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `invoice_detail`;
CREATE TABLE `invoice_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_main_id` bigint(20) NOT NULL,
  `invoice_student_id` int(11) NOT NULL DEFAULT '0',
  `fi_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk fee_item',
  `fee_item_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cur_id` bigint(20) NOT NULL COMMENT 'FK tbl_currency',
  `amount` decimal(20,2) NOT NULL,
  `amount_gst` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'amount before gst',
  `amount_default_currency` decimal(20,2) NOT NULL DEFAULT '0.00',
  `paid` decimal(20,2) NOT NULL DEFAULT '0.00',
  `balance` decimal(20,2) NOT NULL,
  `exchange_rate` int(11) NOT NULL DEFAULT '0' COMMENT 'PK currency_rate',
  `cn_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `dn_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `sp_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `sp_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pk sponsor_invoice_Detail',
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cur_id` (`cur_id`),
  KEY `invoice_main_id` (`invoice_main_id`),
  KEY `exchange_rate` (`exchange_rate`),
  KEY `fi_id` (`fi_id`),
  KEY `invoice_student_id` (`invoice_student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `invoice_main`;
CREATE TABLE `invoice_main` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bill_number` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_ref_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_id` int(20) NOT NULL DEFAULT '0',
  `corporate_id` int(20) DEFAULT '0' COMMENT 'fk tbl_takafuloperator',
  `IdStudentRegistration` bigint(20) DEFAULT '0' COMMENT 'fk tbl_studentregistration',
  `batchId` bigint(20) DEFAULT NULL,
  `ber_id` int(11) DEFAULT '0' COMMENT 'fk batch_exam_registration',
  `er_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk exam_registration',
  `mr_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk membership_registration',
  `order_id` int(11) DEFAULT NULL COMMENT 'ibfim-lms.order',
  `category_id` int(11) DEFAULT '1' COMMENT 'tbl_category',
  `totalStudent` int(11) DEFAULT '1',
  `bill_amount` decimal(20,2) NOT NULL COMMENT 'nett = bill_amount_gst + gst',
  `bill_amount_gst` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'amount before gst',
  `gst_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'gst amount',
  `service_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'amount charged for service fees. eg: migs, fpx, sst, etc',
  `bill_paid` decimal(20,2) NOT NULL DEFAULT '0.00',
  `bill_balance` decimal(20,2) NOT NULL DEFAULT '0.00',
  `bill_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program_id` bigint(20) NOT NULL DEFAULT '0',
  `date_create` datetime NOT NULL,
  `invoice_date` date NOT NULL,
  `creator` bigint(20) NOT NULL DEFAULT '665' COMMENT 'fk tbl_user',
  `from` int(11) NOT NULL DEFAULT '0' COMMENT '0=sms, 1=lms, 2=corporate',
  `fs_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'fk fee_structure',
  `fsp_id` bigint(20) DEFAULT NULL COMMENT 'fk fee_structure_plan',
  `cn_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `dn_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `sp_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `sp_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pk sponsor_invoice_main',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'A:active X: cancel',
  `approve_date` datetime DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL,
  `cancel_by` bigint(20) DEFAULT NULL,
  `cancel_from` tinyint(4) DEFAULT NULL COMMENT '0=sms,1=lms,2=corp',
  `cancel_date` datetime DEFAULT NULL,
  `currency_id` bigint(20) NOT NULL DEFAULT '1',
  `proforma_invoice_id` bigint(20) DEFAULT NULL COMMENT 'FK proforma_invoice_main',
  `upd_by` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `exchange_rate` int(11) DEFAULT '0' COMMENT 'PK currencry_rate',
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `invoice_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_item` int(11) DEFAULT NULL COMMENT 'tbl_definationms 160',
  `not_include` int(11) NOT NULL DEFAULT '0' COMMENT '1=no,0=yes',
  `adv_transfer_id` int(11) NOT NULL DEFAULT '0' COMMENT 'PK advance_payment',
  `payment_mode` int(11) NOT NULL DEFAULT '0' COMMENT 'payment_mode',
  `payment_mode_date` datetime DEFAULT NULL,
  `manual` tinyint(4) DEFAULT '0' COMMENT '0=auto, 1=manual',
  `coupon_id` int(11) DEFAULT '0' COMMENT 'tbl_coupon',
  PRIMARY KEY (`id`),
  UNIQUE KEY `bill_number` (`bill_number`),
  KEY `status` (`status`),
  KEY `program_id` (`program_id`),
  KEY `fs_id` (`fs_id`),
  KEY `proforma_invoice_id` (`proforma_invoice_id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`),
  KEY `currency_id` (`currency_id`),
  KEY `trans_id` (`corporate_id`),
  KEY `exchange_rate` (`exchange_rate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `invoice_student`;
CREATE TABLE `invoice_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL COMMENT 'fk invoice_main',
  `IdStudentRegistration` int(11) NOT NULL,
  `examregistration_main_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_registration_main',
  `cur_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `paid` decimal(20,2) NOT NULL,
  `balance` decimal(20,2) NOT NULL,
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `invoice_subject`;
CREATE TABLE `invoice_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_main_id` bigint(20) NOT NULL COMMENT 'FK invoice_main',
  `invoice_detail_id` bigint(20) NOT NULL COMMENT 'FK invoice_detail',
  `invoice_student_id` bigint(20) NOT NULL,
  `subject_id` bigint(20) NOT NULL COMMENT 'FK tbl_subjectmaster',
  `cur_id` bigint(20) NOT NULL COMMENT 'FK tbl_currency',
  `amount` decimal(20,2) NOT NULL,
  `amount_default_currency` decimal(20,2) NOT NULL DEFAULT '0.00',
  `paid` decimal(20,2) NOT NULL,
  `balance` decimal(20,2) NOT NULL,
  `rcp_id` bigint(20) DEFAULT NULL,
  `rcp_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_rate` int(11) NOT NULL DEFAULT '0' COMMENT 'PK currency_rate',
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proforma_invoice_main_id` (`invoice_main_id`,`invoice_detail_id`,`subject_id`),
  KEY `cur_id` (`cur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `log_api`;
CREATE TABLE `log_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `function` varchar(250) DEFAULT NULL,
  `message` text,
  `line` text,
  `file` text,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership_application`;
CREATE TABLE `membership_application` (
  `ma_id` int(11) NOT NULL AUTO_INCREMENT,
  `ma_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:Upgrade 2:Reinstate',
  `sp_id` bigint(20) NOT NULL,
  `ma_createddt` datetime DEFAULT NULL,
  `ma_createdby` int(11) DEFAULT NULL,
  `ma_modifieddt` datetime DEFAULT NULL,
  `ma_modifiedby` int(11) DEFAULT NULL,
  `ma_submit_date` datetime DEFAULT NULL,
  `ma_approve_date` datetime DEFAULT NULL,
  `ma_approve_by` int(11) NOT NULL,
  `ma_reject_date` datetime DEFAULT NULL,
  `ma_reject_by` int(11) DEFAULT NULL,
  `ma_activation_date` datetime DEFAULT NULL,
  `ma_expiry_date` datetime DEFAULT NULL,
  `ma_status` int(11) NOT NULL DEFAULT '0' COMMENT 'tbl_definations',
  `ma_payment_status` int(11) NOT NULL DEFAULT '0',
  `mf_id` int(11) NOT NULL DEFAULT '0' COMMENT 'membership_fee',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `membershipId` int(11) DEFAULT NULL COMMENT 'fk tbl_membership',
  `ma_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: Complete 2:Incomplete',
  `ma_remarks` text COLLATE utf8mb4_unicode_ci,
  `ma_prev_mrid` int(11) NOT NULL DEFAULT '0' COMMENT 'previous mr id',
  `ma_application_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:in progress 1:completed',
  PRIMARY KEY (`ma_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `membership_fee`;
CREATE TABLE `membership_fee` (
  `mf_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL COMMENT 'fee_item',
  `member_id` int(11) NOT NULL COMMENT 'tbl_membership',
  `mf_effective_date` date NOT NULL,
  `mf_start_date` date DEFAULT NULL COMMENT 'year start',
  `mf_end_date` date DEFAULT NULL COMMENT 'year end',
  `mf_amount` double NOT NULL,
  `mf_createddt` datetime NOT NULL,
  `mf_createdby` int(11) NOT NULL,
  PRIMARY KEY (`mf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership_registration`;
CREATE TABLE `membership_registration` (
  `mr_id` int(11) NOT NULL AUTO_INCREMENT,
  `mr_transaction_id` bigint(20) NOT NULL,
  `mr_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: New Application 1: Renewal',
  `mr_sp_id` bigint(20) NOT NULL,
  `mr_registrationId` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `mr_bmr_id` int(11) NOT NULL DEFAULT '0' COMMENT 'batch membership registration id',
  `mr_student_id` int(11) NOT NULL COMMENT 'tbl_studentregistration',
  `mr_activation_date` datetime DEFAULT NULL,
  `mr_expiry_date` datetime DEFAULT NULL,
  `mr_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-entry',
  `mr_approve_date` datetime DEFAULT NULL,
  `mr_approve_by` int(11) NOT NULL DEFAULT '0',
  `mr_reject_date` datetime DEFAULT NULL,
  `mr_reject_by` int(11) DEFAULT NULL,
  `mr_payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:pending 1:paid:3:payment cancelled (tbe)',
  `mr_exemption_status` int(11) DEFAULT NULL,
  `mr_exemption_type` int(11) DEFAULT NULL,
  `mr_createddt` datetime DEFAULT NULL,
  `mr_createdby` int(11) DEFAULT NULL,
  `mr_modifieddt` datetime DEFAULT NULL,
  `mr_modifiedby` int(11) DEFAULT NULL,
  `mr_cancel_apply_date` datetime DEFAULT NULL,
  `mr_cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `mr_cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `mr_cancel_approve_date` datetime DEFAULT NULL,
  `mr_cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `mr_fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee structure',
  `mf_id` int(11) NOT NULL DEFAULT '0' COMMENT 'membership_fee',
  `mr_order_id` int(11) NOT NULL DEFAULT '0',
  `mr_membershipId` int(11) DEFAULT NULL COMMENT 'fk tbl_membership',
  `mr_submit_date` datetime DEFAULT NULL,
  `mr_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: Complete 2:Incomplete',
  `mr_remarks` text COLLATE utf8mb4_unicode_ci,
  `mr_prev_mrid` int(11) NOT NULL DEFAULT '0' COMMENT 'previous mr id',
  PRIMARY KEY (`mr_id`),
  KEY `student_id` (`mr_student_id`),
  KEY `IdProgram` (`mr_membershipId`),
  KEY `bmr_id` (`mr_bmr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `membership_registration_checklist`;
CREATE TABLE `membership_registration_checklist` (
  `mrc_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_id` int(11) NOT NULL COMMENT 'fk tbl_membership',
  `mrc_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Membership new application 2: Membership Upgrade',
  `mrc_name` tinytext NOT NULL,
  `mrc_status` int(11) NOT NULL,
  `mrc_remarks` text NOT NULL,
  `mrc_downloadable` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  `mrc_createddt` datetime NOT NULL,
  `mrc_createdby` int(11) NOT NULL,
  PRIMARY KEY (`mrc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership_registration_documents`;
CREATE TABLE `membership_registration_documents` (
  `mrd_id` int(11) NOT NULL AUTO_INCREMENT,
  `mr_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk membership_registration',
  `ma_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk membership_application',
  `mrc_id` int(11) NOT NULL COMMENT 'fk membership_registration_checklist',
  `mrd_name` varchar(250) NOT NULL,
  `mrd_application_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: new application 2: upgrade 3 :reinstate',
  `mrd_createddt` datetime NOT NULL,
  `mrd_createdby` int(11) NOT NULL,
  PRIMARY KEY (`mrd_id`),
  KEY `mr_id` (`mr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership_registration_history`;
CREATE TABLE `membership_registration_history` (
  `mrh_id` int(11) NOT NULL AUTO_INCREMENT,
  `mr_id` int(11) NOT NULL,
  `mr_transaction_id` bigint(20) NOT NULL,
  `mr_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: New Application 1: Renewal',
  `mr_sp_id` bigint(20) NOT NULL,
  `mr_registrationId` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `mr_bmr_id` int(11) NOT NULL DEFAULT '0' COMMENT 'batch membership registration id',
  `mr_student_id` int(11) NOT NULL COMMENT 'tbl_studentregistration',
  `mr_activation_date` datetime DEFAULT NULL,
  `mr_expiry_date` datetime DEFAULT NULL,
  `mr_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-entry',
  `mr_approve_date` datetime NOT NULL,
  `mr_approve_by` int(11) NOT NULL,
  `mr_reject_date` datetime DEFAULT NULL,
  `mr_reject_by` int(11) DEFAULT NULL,
  `mr_payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:pending 1:paid:3:payment cancelled (tbe)',
  `mr_exemption_status` int(11) DEFAULT NULL,
  `mr_exemption_type` int(11) DEFAULT NULL,
  `mr_createddt` datetime DEFAULT NULL,
  `mr_createdby` int(11) DEFAULT NULL,
  `mr_modifieddt` datetime DEFAULT NULL,
  `mr_modifiedby` int(11) DEFAULT NULL,
  `mr_cancel_apply_date` datetime DEFAULT NULL,
  `mr_cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `mr_cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `mr_cancel_approve_date` datetime DEFAULT NULL,
  `mr_cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `mr_fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee structure',
  `mf_id` int(11) NOT NULL DEFAULT '0' COMMENT 'membership_fee',
  `mr_order_id` int(11) NOT NULL DEFAULT '0',
  `mr_membershipId` int(11) DEFAULT NULL COMMENT 'fk tbl_membership',
  `mr_submit_date` datetime DEFAULT NULL,
  `mr_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: Complete 2:Incomplete',
  `mr_remarks` text COLLATE utf8mb4_unicode_ci,
  `mr_prev_mrid` int(11) NOT NULL DEFAULT '0' COMMENT 'previous mr id',
  `mrh_amount_fee` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'total fee',
  `mrh_createddt` datetime DEFAULT NULL,
  `mrh_createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`mrh_id`),
  KEY `student_id` (`mr_student_id`),
  KEY `IdProgram` (`mr_membershipId`),
  KEY `bmr_id` (`mr_bmr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `from_id` int(11) unsigned NOT NULL,
  `to_id` int(11) unsigned NOT NULL,
  `folder_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `haveunread` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `read_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `from_id` (`from_id`),
  KEY `to_id` (`to_id`),
  KEY `haveread` (`haveunread`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `mg_log`;
CREATE TABLE `mg_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) DEFAULT NULL COMMENT 'exam_schedule',
  `ExamDate` date DEFAULT NULL,
  `pe_id` int(11) NOT NULL COMMENT 'programme_exam',
  `ExamName` varchar(200) NOT NULL COMMENT 'tbe.programname',
  `examschedule_id` int(11) DEFAULT '0',
  `examtaggingslot_id` int(11) DEFAULT '0',
  `examsetup_id` int(11) DEFAULT '0',
  `examsetupdetail_id` int(11) DEFAULT '0',
  `total_migrate` int(11) DEFAULT '0',
  `total_all` int(11) DEFAULT '0',
  `total_failed` int(11) DEFAULT '0',
  `total_incomplete` int(11) DEFAULT '0' COMMENT 'incomplete data mapping',
  `total_unmapped` int(11) DEFAULT '0',
  `total_exam` int(11) DEFAULT '0' COMMENT 'exam_registration + result',
  `remarks` text,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scheduleId` (`scheduleId`),
  KEY `ExamDate` (`ExamDate`),
  KEY `pe_id` (`pe_id`),
  KEY `examschedule_id` (`examschedule_id`),
  KEY `examtaggingslot_id` (`examtaggingslot_id`),
  KEY `examsetup_id` (`examsetup_id`),
  KEY `examsetupdetail_id` (`examsetupdetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='log tbe migration log';


DROP TABLE IF EXISTS `mg_tbe`;
CREATE TABLE `mg_tbe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `function` varchar(150) NOT NULL,
  `total_row` int(11) DEFAULT NULL,
  `total_update` int(11) DEFAULT NULL,
  `executed_date` datetime DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mifp`;
CREATE TABLE `mifp` (
  `semester` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `studentId` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark` decimal(4,2) DEFAULT NULL,
  `approval_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attendance_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdProgram` int(11) DEFAULT NULL,
  `migrate_status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_message` longtext COLLATE utf8mb4_unicode_ci,
  `migrate_date` datetime DEFAULT NULL,
  `migrate_by` int(11) DEFAULT NULL,
  `IdSemester` int(11) DEFAULT NULL,
  `IdSubject` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrate_applicant_files`;
CREATE TABLE `migrate_applicant_files` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filesize` bigint(11) unsigned NOT NULL,
  `applicant_id` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assigned_to` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrate_applicant_mapping`;
CREATE TABLE `migrate_applicant_mapping` (
  `id` int(11) NOT NULL,
  `metric_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicant_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program_id` int(11) NOT NULL,
  `program_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2018-03-05 10:13:46
