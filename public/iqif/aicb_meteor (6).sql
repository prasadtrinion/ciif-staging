-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `certificate_seq_no` (
  `csn_id` int(11) NOT NULL AUTO_INCREMENT,
  `csn_program_id` int(11) NOT NULL DEFAULT '0',
  `csn_seq_no` int(11) NOT NULL DEFAULT '1',
  `csn_year` year(4) DEFAULT NULL,
  PRIMARY KEY (`csn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `certificate_template` (
  `crt_id` int(11) NOT NULL AUTO_INCREMENT,
  `crt_content` text NOT NULL,
  `crt_updby` int(11) NOT NULL DEFAULT '0',
  `crt_upddate` datetime DEFAULT NULL,
  PRIMARY KEY (`crt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


SET NAMES utf8mb4;

CREATE TABLE `change_status` (
  `cs_id` int(11) NOT NULL AUTO_INCREMENT,
  `cs_registrationId` int(11) DEFAULT NULL,
  `cs_IdStudentRegSubjects` int(11) DEFAULT NULL,
  `cs_type` int(11) NOT NULL COMMENT '1: mark approve - mark entry',
  `cs_remarks` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cs_createdby` int(11) NOT NULL,
  `cs_createddt` datetime NOT NULL,
  PRIMARY KEY (`cs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `cn_double` (
  `cn_billing_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count(1)` bigint(21) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `comm_compose` (
  `comp_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `comp_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_tpl_id` int(10) unsigned NOT NULL,
  `comp_type` smallint(10) unsigned NOT NULL,
  `comp_rectype` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'applicants',
  `comp_lang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_totalrecipients` int(10) unsigned NOT NULL,
  `comp_rec` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`comp_id`),
  KEY `comp_module` (`comp_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `comm_compose_attachment` (
  `ca_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `ca_comp_id` bigint(11) unsigned NOT NULL,
  `ca_filename` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ca_fileurl` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ca_filesize` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ca_id`),
  KEY `ca_comp_id` (`ca_comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `comm_compose_recipients` (
  `cr_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `cr_comp_id` bigint(11) unsigned NOT NULL,
  `cr_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cr_content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cr_rec_id` int(10) unsigned NOT NULL,
  `cr_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_attachment_id` int(10) unsigned DEFAULT NULL,
  `cr_attachment_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_attachment_filename` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cr_datesent` datetime DEFAULT NULL,
  PRIMARY KEY (`cr_id`),
  KEY `cr_comp_id` (`cr_comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `comm_groups` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_rectype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_type` int(11) NOT NULL DEFAULT '0',
  `group_status` int(11) NOT NULL,
  `group_description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_recipients` smallint(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  KEY `module` (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `comm_groups_recipients` (
  `rec_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rec_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `recipient_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rec_id`),
  KEY `group_id` (`group_id`,`recipient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `comm_template` (
  `tpl_id` int(10) NOT NULL AUTO_INCREMENT,
  `tpl_module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_type` smallint(10) unsigned NOT NULL COMMENT 'fk tbl_definations (type:121)',
  `tpl_categories` mediumtext COLLATE utf8mb4_unicode_ci,
  `email_from_name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `tpl_pname` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`tpl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `comm_template_content` (
  `content_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tpl_id` int(11) unsigned NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_content` mediumtext COLLATE utf8mb4_unicode_ci COMMENT '1: Standard Doc-> URL Lain2 Type -> html content',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `comm_template_tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tpl_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_value` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `convocation` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_year` year(4) NOT NULL,
  `c_date_from` date DEFAULT NULL,
  `c_date_to` date DEFAULT NULL,
  `c_session` int(11) NOT NULL,
  `c_capacity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'capacity per session',
  `c_guest` int(11) NOT NULL DEFAULT '0',
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`c_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='list of convocation event';


CREATE TABLE `convocation_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(11) NOT NULL,
  `convocation_id` int(20) NOT NULL,
  `apply_date` datetime NOT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT 'NULL: pending; 0: reject; 1: approved',
  `status_date` datetime DEFAULT NULL,
  `status_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`),
  KEY `convocation_id` (`convocation_id`),
  KEY `status_by` (`status_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='list of convocation application';


CREATE TABLE `convocation_award` (
  `ca_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdAward` int(11) NOT NULL COMMENT 'fk tbl_graduation_award',
  `convocation_id` int(11) NOT NULL COMMENT 'fk table convocation',
  `ca_createddt` datetime NOT NULL,
  `ca_createdby` int(11) NOT NULL,
  PRIMARY KEY (`ca_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `convocation_checklist` (
  `cc_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL COMMENT 'fk convocation',
  `idProgram` int(11) NOT NULL COMMENT 'tbl_program',
  `cc_createddt` datetime NOT NULL,
  `cc_createdby` int(11) NOT NULL,
  PRIMARY KEY (`cc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `convocation_graduate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idStudentRegistration` int(11) NOT NULL,
  `convocation_id` int(11) NOT NULL,
  `robe_collect` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `robe_return` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `robe_serial_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `robe_size` enum('S','M','L','XL','XXL') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hood_collect` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `hood_return` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `mortar_collect` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `mortar_return` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `robe_date_collected` date DEFAULT NULL,
  `robe_date_returned` date DEFAULT NULL,
  `hood_date_collected` date DEFAULT NULL,
  `hood_date_returned` date DEFAULT NULL,
  `mortar_date_collected` date DEFAULT NULL,
  `mortar_date_returned` date DEFAULT NULL,
  `date_collected` date DEFAULT NULL,
  `date_returned` date DEFAULT NULL,
  `add_by` bigint(20) NOT NULL,
  `add_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idStudentRegistration` (`idStudentRegistration`,`convocation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='store graduate list that has been approved to follow convoca';


CREATE TABLE `convocation_program_checklist` (
  `cpc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cc_id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `idChecklist` int(11) NOT NULL,
  PRIMARY KEY (`cpc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `course_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'fk tbl_subjectmaster',
  `maxstud` int(11) NOT NULL,
  `IdLecturer` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IdSubject` (`IdSubject`),
  KEY `IdLecturer` (`IdLecturer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `course_group_attendance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSchedule` bigint(20) NOT NULL COMMENT 'fk course_group_schedule_detail',
  `IdStudentRegistration` bigint(20) unsigned NOT NULL COMMENT 'fk tbl_studentregistration',
  `status` int(11) DEFAULT NULL COMMENT 'fk_tbl_definationms 91',
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `course_group_attendance_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `course_group_att_id` bigint(20) NOT NULL COMMENT 'fk course_group_attendance',
  `student_id` bigint(20) NOT NULL COMMENT 'fk tbl_studentregistration',
  `student_nim` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL COMMENT 'fk_tbl_definationms',
  `last_edit_by` bigint(20) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `course_group_program` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `program_id` bigint(20) NOT NULL,
  `program_scheme_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`,`program_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `course_group_schedule` (
  `sc_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdSchedule` int(11) NOT NULL COMMENT 'fk course_group_schedule',
  `IdScheduleParent` int(11) DEFAULT '0',
  `idClassType` int(11) DEFAULT NULL,
  `sc_date_start` date NOT NULL,
  `sc_date_end` date NOT NULL,
  `sc_duration` int(11) NOT NULL,
  `sc_start_time` time NOT NULL,
  `sc_end_time` time NOT NULL,
  `sc_venue` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sc_address` text COLLATE utf8mb4_unicode_ci,
  `sc_class` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sc_day` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sc_remark` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdLecturer` int(11) DEFAULT NULL,
  `sc_createdby` int(11) NOT NULL,
  `sc_createddt` datetime NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `sc_visibility` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `sc_custom` int(11) NOT NULL,
  PRIMARY KEY (`sc_id`),
  KEY `IdSchedule` (`IdSchedule`),
  KEY `idClassType` (`idClassType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `course_group_schedule_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scDetId` int(11) NOT NULL COMMENT 'fk course_group_schedule_detail',
  `sc_date` date NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scDetId` (`scDetId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `course_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseid` varchar(50) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `course_migration_ce` (
  `IdSemester` int(11) DEFAULT NULL,
  `IdSubject` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `studentId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdProgram` int(11) DEFAULT NULL,
  `grade_name` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shariah` decimal(11,2) DEFAULT NULL,
  `finance` decimal(11,2) DEFAULT NULL,
  `migrate_message` longtext COLLATE utf8mb4_unicode_ci,
  `migrate_status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `migrate_by` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `course_migration_ct` (
  `program` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `studentId` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdSubject` int(11) DEFAULT NULL,
  `IdProgram` int(11) DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `migrate_by` int(11) DEFAULT NULL,
  `migrate_status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_message` longtext COLLATE utf8mb4_unicode_ci,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `creditcard_terminal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terminal_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` bigint(20) NOT NULL COMMENT 'fk tbl_branchofficevenue',
  `default` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: true; 0: false',
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1: active; 0: incative',
  PRIMARY KEY (`id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `credit_note` (
  `cn_trans_id` int(11) DEFAULT NULL,
  `cn_corporate_id` int(11) DEFAULT '0',
  `cn_IdStudentRegistration` int(11) DEFAULT NULL,
  `cn_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cn_billing_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_fomulir` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_appl_id` bigint(20) DEFAULT NULL,
  `cn_amount` decimal(20,2) NOT NULL,
  `cn_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_creator` bigint(20) NOT NULL,
  `cn_create_date` datetime NOT NULL,
  `cn_approver` bigint(20) DEFAULT NULL,
  `cn_approve_date` datetime DEFAULT NULL,
  `cn_cancel_by` bigint(20) DEFAULT NULL,
  `cn_cancel_date` datetime DEFAULT NULL,
  `cn_cur_id` int(11) NOT NULL DEFAULT '1',
  `cn_source` int(11) NOT NULL COMMENT '0=sms, 1=student portal',
  `cn_invoice_id` int(11) NOT NULL,
  `cn_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT 'A=active/approved, X=cancel,0=entry',
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cn_type` int(11) NOT NULL,
  `type_amount` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cn_exam_registration_main_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cn_id`),
  KEY `cn_fomulir` (`cn_fomulir`),
  KEY `appl_id` (`cn_appl_id`),
  KEY `cn_trans_id` (`cn_trans_id`),
  KEY `cn_IdStudentRegistration` (`cn_IdStudentRegistration`),
  KEY `cn_cur_id` (`cn_cur_id`),
  KEY `cn_invoice_id` (`cn_invoice_id`),
  KEY `cn_billing_no` (`cn_billing_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `credit_note_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cn_id` int(11) NOT NULL,
  `invoice_main_id` int(11) NOT NULL,
  `invoice_detail_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `cur_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cn_fomulir` (`invoice_main_id`),
  KEY `appl_id` (`invoice_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `cron_ol_exam` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `es_date` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_id` int(11) DEFAULT NULL,
  `results` mediumtext COLLATE utf8mb4_unicode_ci COMMENT 'if any results needed',
  `created_date` datetime NOT NULL,
  `start_date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `executed_date` datetime DEFAULT NULL,
  `executed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `execution_time` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `cron_ol_exam_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coe_id` int(11) NOT NULL COMMENT 'cron_ol_exam',
  `created_date` datetime NOT NULL,
  `executed_date` datetime NOT NULL,
  `execution_time` float DEFAULT NULL,
  `executed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `cron_ol_exam_tempdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coe_id` int(11) NOT NULL COMMENT 'cron_ol_exam',
  `es_id` int(11) DEFAULT NULL COMMENT 'exam_setup',
  `esd_id` int(11) DEFAULT NULL COMMENT 'exam_setup_detail',
  `ec_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL COMMENT 'exam_schedule',
  `scheduletaggingslot_id` int(11) DEFAULT NULL COMMENT 'exam_scheduletaggingslot',
  `idExam` int(11) DEFAULT NULL,
  `total_set` int(11) DEFAULT NULL,
  `total_student` int(11) DEFAULT NULL,
  `total_student_generated` int(11) DEFAULT NULL,
  `pe_id` int(11) DEFAULT NULL,
  `idmarkdistribution` int(11) DEFAULT NULL,
  `results` text,
  `executed` int(11) DEFAULT NULL COMMENT '1=yes,0=no',
  `ep_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `executed_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `cron_service` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `results` mediumtext COLLATE utf8mb4_unicode_ci COMMENT 'if any results needed',
  `created_date` datetime NOT NULL,
  `executed_date` datetime DEFAULT NULL,
  `executed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `execution_time` float unsigned DEFAULT NULL,
  `inprogress` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recurring` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recurring_day` tinyint(2) unsigned DEFAULT NULL,
  `recurring_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recurring_lastexecuted` date DEFAULT NULL,
  `date` date DEFAULT NULL COMMENT 'to pass specific date to execute',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2018-03-05 10:24:02
