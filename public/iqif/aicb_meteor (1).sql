-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `advance_payment`;
CREATE TABLE `advance_payment` (
  `advpy_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `advpy_appl_id` bigint(20) DEFAULT NULL,
  `advpy_trans_id` int(11) DEFAULT NULL,
  `advpy_payee_type` int(11) DEFAULT '0' COMMENT 'fk tbl_definationms',
  `advpy_corporate_id` int(11) DEFAULT '0',
  `advpy_idStudentRegistration` int(11) DEFAULT NULL,
  `advpy_acad_year_id` bigint(20) DEFAULT NULL,
  `advpy_sem_id` bigint(20) DEFAULT NULL,
  `advpy_prog_code` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advpy_fomulir` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_invoice_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advpy_invoice_id` int(11) DEFAULT NULL,
  `advpy_invoice_det_id` int(11) NOT NULL DEFAULT '0' COMMENT 'invoice_detail',
  `advpy_payment_id` bigint(20) DEFAULT NULL,
  `advpy_rcp_id` int(11) NOT NULL,
  `advpy_refund_id` bigint(20) DEFAULT NULL,
  `advpy_cur_id` int(11) NOT NULL,
  `advpy_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_amount` decimal(20,2) NOT NULL,
  `advpy_total_paid` decimal(20,2) NOT NULL,
  `advpy_total_balance` decimal(20,2) NOT NULL,
  `advpy_status` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A: active; X: inactive',
  `advpy_creator` bigint(20) NOT NULL,
  `advpy_date` date NOT NULL,
  `advpy_create_date` datetime NOT NULL,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_exchange_rate` int(11) NOT NULL,
  `advpy_approve_by` int(11) NOT NULL DEFAULT '0',
  `advpy_approve_date` datetime DEFAULT NULL,
  `advpy_cancel_by` int(11) NOT NULL,
  `advpy_cancel_date` datetime NOT NULL,
  `IdReceivableAdjustment` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_receivable_adjustment',
  PRIMARY KEY (`advpy_id`),
  KEY `advpy_appl_id` (`advpy_appl_id`),
  KEY `advpy_trans_id` (`advpy_trans_id`),
  KEY `advpy_idStudentRegistration` (`advpy_idStudentRegistration`),
  KEY `advpy_invoice_id` (`advpy_invoice_id`),
  KEY `advpy_cur_id` (`advpy_cur_id`),
  KEY `advpy_rcp_id` (`advpy_rcp_id`),
  KEY `advpy_fomulir` (`advpy_fomulir`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `advance_payment_detail`;
CREATE TABLE `advance_payment_detail` (
  `advpydet_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `advpydet_advpy_id` bigint(20) NOT NULL COMMENT 'fk advance_payment',
  `advpydet_inv_id` int(11) DEFAULT NULL,
  `advpydet_inv_det_id` int(11) NOT NULL DEFAULT '0',
  `advpydet_refund_cheque_id` bigint(20) DEFAULT NULL,
  `advpydet_refund_id` bigint(20) DEFAULT NULL,
  `advpydet_total_paid` decimal(20,2) NOT NULL,
  `advpydet_updby` int(11) NOT NULL DEFAULT '0',
  `advpydet_upddate` datetime DEFAULT NULL,
  `advpydet_exchange_rate` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`advpydet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='amout used from adv payment table';


DROP TABLE IF EXISTS `agent_form_number`;
CREATE TABLE `agent_form_number` (
  `afn_id` int(11) NOT NULL AUTO_INCREMENT,
  `afn_form_no` int(8) NOT NULL,
  `afn_taken_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: available; 1:taken',
  `afn_type` int(11) NOT NULL,
  `afn_academic_year` int(11) DEFAULT NULL,
  `afn_intake` int(11) NOT NULL DEFAULT '78',
  PRIMARY KEY (`afn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='tbl to hold manual form pes id';


DROP TABLE IF EXISTS `announcement`;
CREATE TABLE `announcement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci,
  `program_id` int(11) unsigned DEFAULT NULL,
  `intake_id` int(11) unsigned DEFAULT NULL,
  `semester_id` int(11) unsigned DEFAULT NULL,
  `views` bigint(11) unsigned NOT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `sticky` tinyint(1) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `html` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `program_id_intake_id_semester_id` (`program_id`,`intake_id`,`semester_id`),
  KEY `type` (`type`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `announcement_files`;
CREATE TABLE `announcement_files` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` int(11) unsigned NOT NULL,
  `filename` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileurl` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filesize` bigint(11) unsigned NOT NULL,
  `total_download` bigint(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `announcement_id` (`announcement_id`),
  CONSTRAINT `announcement_files_ibfk_1` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `announcement_read`;
CREATE TABLE `announcement_read` (
  `read_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` int(11) unsigned NOT NULL,
  `user_id` bigint(11) unsigned NOT NULL,
  `announcement_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`read_id`),
  KEY `user_id_announcement_type` (`user_id`,`announcement_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_accomodation`;
CREATE TABLE `applicant_accomodation` (
  `acd_id` int(11) NOT NULL AUTO_INCREMENT,
  `acd_trans_id` bigint(20) NOT NULL,
  `acd_assistance` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: no 1:yes',
  `acd_type` int(11) NOT NULL DEFAULT '0',
  `acd_updateby` int(11) NOT NULL,
  `acd_updatedt` datetime NOT NULL,
  PRIMARY KEY (`acd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_assessment`;
CREATE TABLE `applicant_assessment` (
  `aar_id` int(11) NOT NULL AUTO_INCREMENT,
  `aar_trans_id` int(11) NOT NULL,
  `aar_rating_dean` tinyint(1) DEFAULT NULL,
  `aar_dean_status` int(11) DEFAULT NULL,
  `aar_rating_rector` tinyint(11) DEFAULT NULL,
  `aar_rector_status` int(11) DEFAULT NULL,
  `aar_remarks` longtext COLLATE utf8mb4_unicode_ci,
  `aar_dean_selectionid` int(11) NOT NULL,
  `aar_rector_selectionid` int(11) NOT NULL,
  `aar_final_selectionid` int(11) NOT NULL,
  `aar_dean_rateby` int(11) DEFAULT NULL,
  `aar_dean_ratedt` datetime DEFAULT NULL,
  `aar_rector_rateby` int(11) DEFAULT NULL,
  `aar_rector_ratedt` datetime DEFAULT NULL,
  `aar_approvalby` int(11) DEFAULT NULL,
  `aar_approvaldt` datetime NOT NULL,
  `aar_academic_year` int(11) DEFAULT NULL,
  `aar_reg_start_date` date DEFAULT NULL,
  `aar_reg_end_date` date DEFAULT NULL,
  `aar_payment_start_date` date DEFAULT NULL,
  `aar_payment_end_date` date DEFAULT NULL,
  PRIMARY KEY (`aar_id`),
  UNIQUE KEY `aar_id` (`aar_id`),
  KEY `aar_trans_id` (`aar_trans_id`),
  KEY `aar_dean_selectionid` (`aar_dean_selectionid`),
  KEY `aar_rector_selectionid` (`aar_rector_selectionid`),
  KEY `aar_rector_selectionid_2` (`aar_rector_selectionid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_assessment_date`;
CREATE TABLE `applicant_assessment_date` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `lock_status` tinyint(4) NOT NULL COMMENT '0: not lock; 1: lock',
  `lock_by` int(11) NOT NULL COMMENT 'fk_tbl_user',
  `lock_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_assessment_usm`;
CREATE TABLE `applicant_assessment_usm` (
  `aau_id` int(11) NOT NULL AUTO_INCREMENT,
  `aau_trans_id` int(11) NOT NULL,
  `aau_ap_id` int(11) NOT NULL COMMENT 'fk applicant_program',
  `aau_createddt` datetime NOT NULL,
  `aau_createdby` int(11) NOT NULL,
  `aau_rector_ranking` int(11) DEFAULT NULL,
  `aau_rector_status` int(11) DEFAULT NULL,
  `aau_rector_createby` int(11) DEFAULT NULL,
  `aau_rector_createdt` datetime DEFAULT NULL,
  `aau_rector_selectionid` int(11) DEFAULT NULL COMMENT 'applicant_assessment_usm_detl',
  `aau_reversal_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:yes',
  PRIMARY KEY (`aau_id`),
  KEY `aau_trans_id` (`aau_trans_id`),
  KEY `aau_ap_id` (`aau_ap_id`),
  KEY `aau_rector_selectionid` (`aau_rector_selectionid`),
  KEY `aau_rector_status` (`aau_rector_status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_assessment_usm_detl`;
CREATE TABLE `applicant_assessment_usm_detl` (
  `aaud_id` int(11) NOT NULL AUTO_INCREMENT,
  `aaud_type` int(11) NOT NULL COMMENT '1: Dean 2:Rektor',
  `aaud_nomor` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aaud_decree_date` date DEFAULT NULL,
  `aaud_academic_year` int(11) DEFAULT NULL,
  `aaud_selection_period` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aaud_reg_start_date` date DEFAULT NULL,
  `aaud_reg_end_date` date DEFAULT NULL,
  `aaud_payment_start_date` date DEFAULT NULL,
  `aaud_payment_end_date` date DEFAULT NULL,
  `aaud_lock_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:locked  0:unlock',
  `aaud_lock_by` int(11) DEFAULT NULL,
  `aaud_lock_date` datetime DEFAULT NULL,
  `aaud_createddt` datetime DEFAULT NULL,
  `aaud_createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`aaud_id`),
  KEY `aaud_nomor` (`aaud_nomor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_bulk_history`;
CREATE TABLE `applicant_bulk_history` (
  `b_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `b_status` enum('NEW','IMPORTED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NEW',
  `b_filename` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b_fileurl` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b_totalapp` smallint(10) unsigned NOT NULL,
  `b_totaladded` smallint(10) unsigned NOT NULL DEFAULT '0',
  `b_created_by` int(10) unsigned NOT NULL,
  `b_created_date` datetime NOT NULL,
  PRIMARY KEY (`b_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_bulk_history_item`;
CREATE TABLE `applicant_bulk_history_item` (
  `bi_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `b_id` int(10) unsigned NOT NULL,
  `bi_trans_id` int(10) unsigned NOT NULL,
  `bi_appl_id` int(10) unsigned NOT NULL,
  `bi_bid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bi_id`),
  KEY `b_id` (`b_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_changescheme_attachment`;
CREATE TABLE `applicant_changescheme_attachment` (
  `aca_id` int(11) NOT NULL AUTO_INCREMENT,
  `aca_ach_id` int(11) NOT NULL DEFAULT '0',
  `aca_fileName` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aca_fileUrl` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aca_updDate` datetime NOT NULL,
  `aca_updUser` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aca_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_changescheme_history`;
CREATE TABLE `applicant_changescheme_history` (
  `ach_id` int(11) NOT NULL AUTO_INCREMENT,
  `ach_trans_id` int(11) NOT NULL DEFAULT '0',
  `ach_new_scheme_id` int(11) NOT NULL DEFAULT '0',
  `ach_old_scheme_id` int(11) NOT NULL DEFAULT '0',
  `ach_send_email` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: not send email  1:email send',
  `ach_updUser` int(11) NOT NULL DEFAULT '0',
  `ach_updDate` datetime NOT NULL,
  PRIMARY KEY (`ach_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_change_program`;
CREATE TABLE `applicant_change_program` (
  `acp_id` int(11) NOT NULL AUTO_INCREMENT,
  `acp_appl_id` int(11) NOT NULL,
  `acp_trans_id_from` int(11) NOT NULL,
  `acp_trans_id_to` int(11) NOT NULL,
  `acp_createddt` datetime NOT NULL,
  `acp_createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`acp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_declaration`;
CREATE TABLE `applicant_declaration` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `txn_id` int(11) NOT NULL,
  `declaration_status` tinyint(4) NOT NULL COMMENT '0: No 1:Yes',
  `declaration_date` date NOT NULL,
  PRIMARY KEY (`ad_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_documents`;
CREATE TABLE `applicant_documents` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_txn_id` int(11) NOT NULL COMMENT 'sebenarnye txn ID bukan appl_id',
  `ad_dcl_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_documentchecklist_dcl',
  `ad_ads_id` int(11) NOT NULL,
  `ad_section_id` int(11) DEFAULT '0',
  `ad_table_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'name of table that reflect ad_table_id',
  `ad_table_id` int(11) DEFAULT '0',
  `ad_filepath` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_filename` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_ori_filename` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ad_createddt` date NOT NULL,
  `ad_createdby` int(11) NOT NULL,
  `ad_role` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ad_id`),
  KEY `ad_txn_id` (`ad_txn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_document_checklisthistory`;
CREATE TABLE `applicant_document_checklisthistory` (
  `adc_id` int(11) NOT NULL AUTO_INCREMENT,
  `adc_adh_id` int(11) NOT NULL DEFAULT '0',
  `adc_dcl_id` int(11) NOT NULL DEFAULT '0',
  `adc_stdCtgy` int(11) NOT NULL DEFAULT '0',
  `adc_programScheme` int(11) NOT NULL DEFAULT '0',
  `adc_sectionid` int(11) NOT NULL DEFAULT '0',
  `adc_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adc_malayName` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adc_desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `adc_type` int(11) NOT NULL DEFAULT '0',
  `adc_activeStatus` int(11) NOT NULL DEFAULT '0',
  `adc_priority` int(11) NOT NULL DEFAULT '0',
  `adc_uplStatus` int(11) NOT NULL DEFAULT '0',
  `adc_uplType` int(11) NOT NULL DEFAULT '0',
  `adc_finance` tinyint(2) NOT NULL DEFAULT '0',
  `adc_mandatory` int(11) NOT NULL DEFAULT '0',
  `adc_specific` int(11) NOT NULL DEFAULT '0',
  `adc_upddate` date NOT NULL,
  `adc_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_document_history`;
CREATE TABLE `applicant_document_history` (
  `adh_id` int(11) NOT NULL AUTO_INCREMENT,
  `adh_txn_id` int(11) NOT NULL DEFAULT '0',
  `adh_type` int(11) NOT NULL DEFAULT '0',
  `adh_oldprogramscheme` int(11) NOT NULL DEFAULT '0',
  `adh_newprogramscheme` int(11) NOT NULL DEFAULT '0',
  `adh_oldstudentcat` int(11) NOT NULL DEFAULT '0',
  `adh_newstudentcat` int(11) NOT NULL DEFAULT '0',
  `adh_upddate` date DEFAULT NULL,
  `adh_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_document_status`;
CREATE TABLE `applicant_document_status` (
  `ads_id` int(11) NOT NULL AUTO_INCREMENT,
  `ads_txn_id` int(11) NOT NULL DEFAULT '0',
  `ads_dcl_id` int(11) NOT NULL DEFAULT '0',
  `ads_ad_id` int(11) DEFAULT NULL COMMENT 'fk applicant_documents',
  `ads_appl_id` int(11) NOT NULL DEFAULT '0',
  `ads_section_id` int(11) NOT NULL DEFAULT '0',
  `ads_table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ads_table_id` int(11) NOT NULL DEFAULT '0',
  `ads_confirm` tinyint(11) NOT NULL DEFAULT '0' COMMENT '1: Confirm (can proceed processing data)',
  `ads_status` tinyint(11) NOT NULL DEFAULT '0' COMMENT '1: New   2: Viewed   3: Incomplete   4: Complete',
  `ads_comment` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ads_createBy` int(11) NOT NULL DEFAULT '0',
  `ads_createDate` datetime NOT NULL,
  `ads_modBy` int(11) NOT NULL DEFAULT '0',
  `ads_modDate` datetime NOT NULL,
  PRIMARY KEY (`ads_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_document_statushistory`;
CREATE TABLE `applicant_document_statushistory` (
  `adt_id` int(11) NOT NULL AUTO_INCREMENT,
  `adt_adh_id` int(11) NOT NULL DEFAULT '0',
  `adt_adc_id` int(11) NOT NULL DEFAULT '0',
  `adt_txn_id` int(11) NOT NULL DEFAULT '0',
  `adt_dcl_id` int(11) NOT NULL DEFAULT '0',
  `adt_ad_id` int(11) NOT NULL DEFAULT '0',
  `adt_appl_id` int(11) NOT NULL DEFAULT '0',
  `adt_section_id` int(11) NOT NULL DEFAULT '0',
  `adt_table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adt_table_id` int(11) NOT NULL DEFAULT '0',
  `adt_confirm` tinyint(2) NOT NULL DEFAULT '0',
  `adt_status` int(2) NOT NULL DEFAULT '0',
  `adt_comment` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adt_upddate` date DEFAULT NULL,
  `adt_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_document_uploadhistory`;
CREATE TABLE `applicant_document_uploadhistory` (
  `adu_id` int(11) NOT NULL AUTO_INCREMENT,
  `adu_adh_id` int(11) NOT NULL DEFAULT '0',
  `adu_adc_id` int(11) NOT NULL DEFAULT '0',
  `adu_txn_id` int(11) NOT NULL DEFAULT '0',
  `adu_dcl_id` int(11) NOT NULL DEFAULT '0',
  `adu_ads_id` int(11) NOT NULL DEFAULT '0',
  `adu_appl_id` int(11) NOT NULL DEFAULT '0',
  `adu_section_id` int(11) NOT NULL DEFAULT '0',
  `adu_table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adu_table_id` int(11) NOT NULL DEFAULT '0',
  `adu_filepath` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adu_filename` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adu_ori_filename` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adu_upddate` date DEFAULT NULL,
  `adu_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_education`;
CREATE TABLE `applicant_education` (
  `ae_id` int(11) NOT NULL AUTO_INCREMENT,
  `ae_appl_id` bigint(20) NOT NULL,
  `ae_transaction_id` int(11) NOT NULL,
  `ae_institution` int(11) NOT NULL COMMENT 'fk.school_master',
  `ae_discipline_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk school_majoring(smj_code)',
  `ae_year_from` date NOT NULL,
  `ae_year_end` date NOT NULL,
  `ae_year` tinyint(4) DEFAULT NULL,
  `ae_award` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ae_id`),
  KEY `ae_transaction_id` (`ae_transaction_id`),
  KEY `ae_appl_id` (`ae_appl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_education_detl`;
CREATE TABLE `applicant_education_detl` (
  `aed_id` int(11) NOT NULL AUTO_INCREMENT,
  `aed_ae_id` int(11) NOT NULL COMMENT 'fk applicant_education(ae_id)',
  `aed_subject_id` int(11) NOT NULL COMMENT 'fk chool_majoring_subject(sms_id)',
  `aed_sem1` int(3) DEFAULT NULL,
  `aed_sem2` int(3) DEFAULT NULL,
  `aed_sem3` int(3) DEFAULT NULL,
  `aed_sem4` int(3) DEFAULT NULL,
  `aed_sem5` int(3) DEFAULT NULL,
  `aed_sem6` int(3) DEFAULT NULL,
  `aed_grade` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aed_average` decimal(10,1) DEFAULT NULL,
  PRIMARY KEY (`aed_id`),
  KEY `applicant_education_detl_fk1` (`aed_ae_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_eligible`;
CREATE TABLE `applicant_eligible` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program` int(11) NOT NULL COMMENT 'fk tbl_program',
  `qualification` int(11) NOT NULL COMMENT 'fk tbl_qualificationmaster',
  `appl_id` int(11) DEFAULT NULL,
  `appl_trans_id` int(11) DEFAULT NULL COMMENT 'xperlu',
  `status` int(11) NOT NULL COMMENT '1=eligible 0 = no',
  `age` int(11) DEFAULT NULL,
  `work_exp` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_employment`;
CREATE TABLE `applicant_employment` (
  `ae_id` int(11) NOT NULL AUTO_INCREMENT,
  `ae_appl_id` int(11) NOT NULL,
  `ae_trans_id` int(11) NOT NULL,
  `ae_status` int(11) NOT NULL COMMENT 'Employment Status',
  `ae_comp_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_comp_address` longtext COLLATE utf8mb4_unicode_ci,
  `ae_comp_phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_comp_fax` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_position` int(11) DEFAULT NULL COMMENT 'position level',
  `ae_from` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_to` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emply_year_service` int(11) DEFAULT NULL,
  `ae_industry` int(11) DEFAULT NULL COMMENT 'Industry Working Experience',
  `ae_job_desc` longtext COLLATE utf8mb4_unicode_ci,
  `ae_income` float DEFAULT NULL,
  `ae_industry_type` int(11) DEFAULT NULL,
  `ae_is_current` int(11) DEFAULT NULL,
  `ae_order` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ae_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_english_proficiency`;
CREATE TABLE `applicant_english_proficiency` (
  `ep_id` int(11) NOT NULL AUTO_INCREMENT,
  `ep_transaction_id` int(11) NOT NULL,
  `ep_test` int(11) DEFAULT NULL,
  `ep_test_detail` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ep_date_taken` date DEFAULT NULL,
  `ep_score` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ep_updatedt` datetime NOT NULL,
  `ep_updateby` int(11) NOT NULL,
  PRIMARY KEY (`ep_id`),
  UNIQUE KEY `ep_id` (`ep_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_essay`;
CREATE TABLE `applicant_essay` (
  `e_id` int(11) NOT NULL AUTO_INCREMENT,
  `e_trans_id` int(10) unsigned NOT NULL,
  `e_file` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_updatedt` datetime NOT NULL,
  `e_updateby` int(11) NOT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_family`;
CREATE TABLE `applicant_family` (
  `af_id` int(11) NOT NULL AUTO_INCREMENT,
  `af_appl_id` int(11) NOT NULL,
  `af_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `af_relation_type` int(11) NOT NULL COMMENT 'fk sis_setup_detl(ssd_id)',
  `af_family_condition` int(11) DEFAULT NULL COMMENT 'fk sis_setup_detl',
  `af_address_rt` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_address_rw` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_address1` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `af_address2` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_kelurahan` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_kecamatan` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_city` int(11) NOT NULL,
  `af_state` bigint(20) unsigned NOT NULL,
  `af_province` int(11) DEFAULT NULL,
  `af_postcode` bigint(5) NOT NULL,
  `af_phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `af_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `af_job` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `af_education_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`af_id`),
  KEY `applicant_family_fk_1` (`af_state`),
  KEY `applicant_family_fk_2` (`af_city`),
  KEY `applicant_family_fk_3` (`af_relation_type`),
  KEY `af_appl_id` (`af_appl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_financial`;
CREATE TABLE `applicant_financial` (
  `af_id` int(11) NOT NULL AUTO_INCREMENT,
  `af_appl_id` int(11) NOT NULL,
  `af_trans_id` int(11) NOT NULL,
  `af_method` int(11) NOT NULL,
  `af_sponsor_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_type_scholarship` int(11) DEFAULT NULL,
  `af_scholarship_secured` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_scholarship_apply` int(11) DEFAULT NULL,
  `af_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upd_date` int(11) NOT NULL,
  PRIMARY KEY (`af_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_generated_document`;
CREATE TABLE `applicant_generated_document` (
  `tgd_id` int(11) NOT NULL AUTO_INCREMENT,
  `tgd_txn_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk applicant_transaction',
  `tgd_appl_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk to applicant_profile',
  `sth_id` int(11) NOT NULL COMMENT 'fk tbl_statusattachment_sth',
  `tgd_filename` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgd_fileurl` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgd_userType` int(11) NOT NULL DEFAULT '0' COMMENT '0: applicant 1:admin',
  `tgd_updUser` int(11) NOT NULL DEFAULT '0',
  `tgd_updDate` datetime NOT NULL,
  PRIMARY KEY (`tgd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_health_condition`;
CREATE TABLE `applicant_health_condition` (
  `ah_id` int(11) NOT NULL AUTO_INCREMENT,
  `ah_appl_id` int(11) NOT NULL,
  `ah_trans_id` int(11) NOT NULL,
  `ah_status` int(11) NOT NULL,
  `upd_date` datetime NOT NULL,
  `ah_others` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ah_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_id_seqno`;
CREATE TABLE `applicant_id_seqno` (
  `seq_id` int(11) NOT NULL AUTO_INCREMENT,
  `seq_no` int(11) NOT NULL,
  `seq_year` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seq_createdby` int(11) NOT NULL,
  `seq_createddt` datetime NOT NULL,
  PRIMARY KEY (`seq_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_language`;
CREATE TABLE `applicant_language` (
  `al_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `al_language` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`al_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_mark_raw`;
CREATE TABLE `applicant_mark_raw` (
  `tmp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tmp_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_ptest_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_set_code` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_answerraw` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_sched_id` int(11) NOT NULL,
  `tmp_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`tmp_id`),
  KEY `tmp_sched_id` (`tmp_sched_id`),
  KEY `tmp_set_code` (`tmp_set_code`),
  KEY `tmp_file` (`tmp_file`(191)),
  KEY `tmp_id` (`tmp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_profile`;
CREATE TABLE `applicant_profile` (
  `appl_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl_salutation` int(11) DEFAULT NULL,
  `appl_fname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_mname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_lname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_idnumber` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_idnumber_type` int(11) DEFAULT NULL,
  `appl_passport_expiry` date DEFAULT NULL,
  `appl_address1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_address2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_address3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_postcode` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_city` int(10) DEFAULT NULL,
  `appl_state` int(11) DEFAULT NULL,
  `appl_city_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_state_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_country` int(11) DEFAULT NULL COMMENT 'Country',
  `appl_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_username` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'later nanti nak pakai just create aje',
  `appl_password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_dob` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'dd-mm-yyyy',
  `appl_gender` tinyint(1) DEFAULT NULL COMMENT '1:  male 2: female',
  `appl_prefer_lang` int(11) NOT NULL DEFAULT '2' COMMENT 'fk applicant_languange(al_id)',
  `appl_nationality` int(11) DEFAULT NULL,
  `appl_type_nationality` int(11) DEFAULT NULL COMMENT 'tbl maintenance',
  `appl_category` int(11) NOT NULL COMMENT 'fk tbl_definations (type:)',
  `appl_admission_type` int(11) DEFAULT NULL COMMENT 'jgn pakai ni lagi guna at_appl_type dkt transaction',
  `appl_religion` int(11) DEFAULT NULL,
  `appl_religion_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_race` int(11) DEFAULT NULL,
  `appl_race_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_marital_status` int(11) DEFAULT NULL,
  `appl_bumiputera` int(11) DEFAULT NULL,
  `appl_takafuloperator` int(11) DEFAULT NULL,
  `appl_no_of_child` int(11) DEFAULT NULL,
  `appl_phone_home` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_phone_mobile` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_phone_office` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_caddress1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'correspondence add',
  `appl_caddress2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_caddress3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_ccity` int(10) DEFAULT NULL,
  `appl_cstate` int(11) DEFAULT NULL,
  `appl_ccountry` int(11) DEFAULT NULL,
  `appl_ccity_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cstate_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cpostcode` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cphone_home` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cphone_mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cphone_office` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cfax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_contact_home` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_contact_mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_contact_office` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `change_passwordby` int(11) DEFAULT NULL,
  `change_passworddt` datetime DEFAULT NULL,
  `appl_role` int(11) DEFAULT NULL COMMENT '0:Applicant  1:Student',
  `fromOldSys` text COLLATE utf8mb4_unicode_ci,
  `verifyKey` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'untuk tujuan verify email',
  `create_date` datetime DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `prev_student` tinyint(4) DEFAULT '0' COMMENT '0: No 1:yes',
  `prev_txn_id` bigint(20) DEFAULT NULL COMMENT 'student_profile txn id',
  `prev_studentID` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'refer to registrationid at tbl_studentregistration',
  `branch_id` int(11) DEFAULT NULL,
  `appl_company` int(11) DEFAULT NULL,
  `appl_designation` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_department` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_default_qlevel` int(11) DEFAULT NULL COMMENT 'highest qualification',
  `appl_category_type` int(11) DEFAULT NULL COMMENT 'tbl_category',
  `appl_company_id` int(11) DEFAULT NULL COMMENT 'tbl_takafuloperator',
  `appl_membership` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userKey` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_email` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`appl_id`),
  KEY `applicant_profile_fk1` (`appl_prefer_lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_proforma_invoice`;
CREATE TABLE `applicant_proforma_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `billing_no` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payee_id` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appl_id` int(11) NOT NULL COMMENT 'fk_applicant_profile',
  `txn_id` int(11) NOT NULL COMMENT 'fk_applicant_transaction',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'faculty_code-shortname',
  `ref2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'blank',
  `ref3` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'program_code-shortname',
  `ref4` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'intake year',
  `ref5` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_total` decimal(20,2) NOT NULL,
  `amount1` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'SP',
  `amount2` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'BPP-POKOK',
  `amount3` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'BPP-SKS',
  `amount4` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'PRAKTIKUM',
  `amount5` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount6` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount7` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount8` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount9` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount10` decimal(20,2) NOT NULL DEFAULT '0.00',
  `register_no` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'random generated no.',
  `due_date` date DEFAULT NULL,
  `offer_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `billing_no` (`billing_no`),
  KEY `payee_id` (`payee_id`),
  KEY `txn_id` (`txn_id`),
  KEY `appl_id` (`appl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_program`;
CREATE TABLE `applicant_program` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `ap_at_trans_id` bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  `ap_prog_id` bigint(20) NOT NULL,
  `ap_prog_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk tbl_program',
  `ap_ptest_prog_id` int(11) DEFAULT NULL COMMENT 'aps_app_id fk to appl_placement_program(app_id)',
  `ap_preference` tinyint(2) NOT NULL DEFAULT '1',
  `mode_study` int(11) NOT NULL,
  `program_mode` int(11) NOT NULL,
  `program_type` int(11) NOT NULL,
  `ap_prog_scheme` int(11) NOT NULL DEFAULT '0',
  `upd_date` datetime NOT NULL,
  UNIQUE KEY `app_id` (`ap_id`),
  KEY `ap_at_trans_id` (`ap_at_trans_id`),
  KEY `ap_prog_code` (`ap_prog_code`),
  KEY `ap_ptest_prog_id` (`ap_ptest_prog_id`),
  KEY `ap_preference` (`ap_preference`),
  KEY `ap_id` (`ap_id`),
  KEY `ap_at_trans_id_2` (`ap_at_trans_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_qualification`;
CREATE TABLE `applicant_qualification` (
  `ae_id` int(11) NOT NULL AUTO_INCREMENT,
  `ae_appl_id` bigint(20) NOT NULL,
  `ae_transaction_id` int(11) NOT NULL,
  `ae_qualification` int(11) DEFAULT NULL COMMENT 'PK tbl_qualificationmaster',
  `ae_degree_awarded` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_majoring` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_class_degree` int(11) DEFAULT NULL,
  `ae_result` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_year_graduate` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_institution_country` int(11) DEFAULT NULL COMMENT 'fk tbl_countries',
  `ae_institution` int(11) DEFAULT NULL COMMENT 'fk.school_master',
  `ae_medium_instruction` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_order` int(11) NOT NULL,
  `others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`ae_id`),
  KEY `ae_transaction_id` (`ae_transaction_id`),
  KEY `ae_appl_id` (`ae_appl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_quantitative_proficiency`;
CREATE TABLE `applicant_quantitative_proficiency` (
  `qp_id` int(11) NOT NULL AUTO_INCREMENT,
  `qp_trans_id` bigint(20) NOT NULL,
  `qp_level` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qp_grade_mark` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qp_institution` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qp_createdby` int(11) NOT NULL,
  `qp_createddt` datetime NOT NULL,
  PRIMARY KEY (`qp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_quit`;
CREATE TABLE `applicant_quit` (
  `aq_id` int(11) NOT NULL AUTO_INCREMENT,
  `aq_trans_id` int(11) DEFAULT NULL,
  `aq_reason` int(11) DEFAULT NULL,
  `aq_authorised_personnel` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aq_relationship` int(11) DEFAULT NULL,
  `aq_address` mediumtext COLLATE utf8mb4_unicode_ci,
  `aq_identity_type` int(11) DEFAULT NULL,
  `aq_identity_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aq_createddt` datetime DEFAULT NULL,
  `aq_approvaldt` date DEFAULT NULL COMMENT 'for status approved/reject',
  `aq_approvalby` int(11) DEFAULT NULL COMMENT 'for status approved/reject',
  `aq_processdt` datetime DEFAULT NULL COMMENT 'ini system date bila admin process appliation ',
  `aq_processby` int(11) DEFAULT NULL,
  `aq_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `aq_cheque_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`aq_id`),
  KEY `aq_trans_id` (`aq_trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_quit_revert`;
CREATE TABLE `applicant_quit_revert` (
  `aqr_id` int(11) NOT NULL AUTO_INCREMENT,
  `aq_id` int(11) NOT NULL,
  `aq_trans_id` int(11) DEFAULT NULL,
  `aq_reason` int(11) DEFAULT NULL,
  `aq_authorised_personnel` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aq_relationship` int(11) DEFAULT NULL,
  `aq_address` mediumtext COLLATE utf8mb4_unicode_ci,
  `aq_identity_type` int(11) DEFAULT NULL,
  `aq_identity_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aq_createddt` datetime DEFAULT NULL,
  `aq_approvaldt` date DEFAULT NULL COMMENT 'for status approved/reject',
  `aq_approvalby` int(11) DEFAULT NULL COMMENT 'for status approved/reject',
  `aq_processdt` datetime DEFAULT NULL COMMENT 'ini system date bila admin process appliation ',
  `aq_processby` int(11) DEFAULT NULL,
  `aq_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `aq_cheque_id` int(11) DEFAULT NULL,
  `aqr_remark` longtext COLLATE utf8mb4_unicode_ci,
  `aqr_by` bigint(20) NOT NULL,
  `aqr_date` datetime NOT NULL,
  PRIMARY KEY (`aqr_id`),
  KEY `aq_trans_id` (`aq_trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_referees`;
CREATE TABLE `applicant_referees` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `r_txn_id` bigint(20) NOT NULL,
  `r_ref1_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_position` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_add1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_add2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_add3` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_postcode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_city` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_city_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_country` int(11) DEFAULT NULL,
  `r_ref1_state` int(11) DEFAULT NULL,
  `r_ref1_state_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_position` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_add1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_add2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_add3` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_postcode` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_city` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_city_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_country` int(11) DEFAULT NULL,
  `r_ref2_state` int(11) DEFAULT NULL,
  `r_ref2_state_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_updateby` int(11) NOT NULL,
  `r_updatedt` datetime NOT NULL,
  PRIMARY KEY (`r_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_research_publication`;
CREATE TABLE `applicant_research_publication` (
  `rpd_id` int(11) NOT NULL AUTO_INCREMENT,
  `rpd_trans_id` bigint(20) NOT NULL,
  `rpd_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rpd_date` date DEFAULT NULL,
  `rpd_publisher` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rpd_createddt` datetime NOT NULL,
  `rpd_createdby` int(11) NOT NULL,
  PRIMARY KEY (`rpd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_reversal_track`;
CREATE TABLE `applicant_reversal_track` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_appl_type` int(11) NOT NULL,
  `art_trans_id` int(11) NOT NULL,
  `art_createdby` int(11) NOT NULL,
  `art_createddt` datetime NOT NULL,
  PRIMARY KEY (`art_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_section_status`;
CREATE TABLE `applicant_section_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appl_id` int(11) NOT NULL,
  `appl_trans_id` int(11) NOT NULL,
  `idSection` int(11) NOT NULL COMMENT 'PK tbl_application_section',
  `status` int(11) NOT NULL,
  `retrieve_status` tinyint(4) DEFAULT '0' COMMENT '0: Default  1:Not Retrieve 2:Retrieve',
  `upd_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_selection_detl`;
CREATE TABLE `applicant_selection_detl` (
  `asd_id` int(11) NOT NULL AUTO_INCREMENT,
  `asd_type` int(11) NOT NULL COMMENT '1: Dean 2: Rector 3:Final Approval',
  `asd_nomor` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asd_decree_date` date NOT NULL,
  `asd_faculty_id` int(11) NOT NULL,
  `asd_selection_period` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asd_intake_id` int(11) NOT NULL,
  `asd_academic_year` int(11) NOT NULL COMMENT 'fk_tbl_academic_year (ay_id)',
  `asd_period_id` int(11) NOT NULL,
  `asd_createddt` datetime NOT NULL,
  `asd_createdby` int(11) NOT NULL,
  PRIMARY KEY (`asd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_status_history`;
CREATE TABLE `applicant_status_history` (
  `ash_id` int(11) NOT NULL AUTO_INCREMENT,
  `ash_trans_id` int(11) NOT NULL DEFAULT '0',
  `ash_status` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ash_oldStatus` int(11) NOT NULL,
  `ash_changeType` int(11) NOT NULL COMMENT '1 : change status 2:change scheme',
  `ash_userType` int(11) NOT NULL DEFAULT '0' COMMENT '0:applicant 1:student',
  `ash_updDate` datetime NOT NULL,
  `ash_updUser` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ash_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_temp_usm_selection`;
CREATE TABLE `applicant_temp_usm_selection` (
  `ats_id` int(11) NOT NULL AUTO_INCREMENT,
  `ats_transaction_id` int(11) NOT NULL,
  `ats_ap_id` int(11) NOT NULL COMMENT 'fk applicant_program ',
  `ats_program_code` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ats_preference` tinyint(11) NOT NULL,
  PRIMARY KEY (`ats_id`),
  KEY `ats_transaction_id` (`ats_transaction_id`),
  KEY `ats_ap_id` (`ats_ap_id`),
  KEY `ats_program_code` (`ats_program_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_transaction`;
CREATE TABLE `applicant_transaction` (
  `at_trans_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `at_appl_id` int(11) NOT NULL COMMENT 'fk applicant_profile(ap_id)',
  `at_pes_id` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_appl_type` int(11) NOT NULL COMMENT '1 - placementtest 2 -HS',
  `at_academic_year` int(11) DEFAULT '3' COMMENT 'fk academic_year(ay_id)',
  `at_intake` int(11) DEFAULT NULL,
  `at_intake_prev` int(11) DEFAULT NULL,
  `at_period` int(11) DEFAULT NULL,
  `at_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'DRAFT,ENTRY,OFFERED,REJECTED,COMPLETE,INCOMPLETE,SHORTLISTED,KIV,ACCEPTED,DECLINE,ENROLLED.ARCHIEVE',
  `at_selection_status` int(11) DEFAULT '0' COMMENT '0:Waiting for dean rating 1: Waiting for rector verification 2: Waiting for approval 3:cpmleted 4: In Pool (USM)',
  `at_create_by` int(11) NOT NULL,
  `at_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `at_submit_date` datetime DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `entry_type` int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  `at_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:not paid 1:paid tobe removed x pakai dah; 2: biasiswa',
  `at_quit_status` int(4) NOT NULL DEFAULT '0' COMMENT '1: Apply Quit 2:Approved Quit 3:Reject 4:Incomplete ',
  `at_move_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk applicant_change_program',
  `at_document_verified` int(11) NOT NULL DEFAULT '0' COMMENT '0:No 1:yes',
  `at_document_verifiedby` int(11) DEFAULT NULL,
  `at_document_verifieddt` datetime DEFAULT NULL,
  `at_registration_status` int(11) DEFAULT NULL COMMENT '1: Active',
  `at_IdStudentRegistration` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_studentregistration',
  `at_registration_date` datetime DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `at_copy_status` tinyint(4) DEFAULT '0' COMMENT '0: Default 1:Copy2:Not Copy',
  `at_bookmark` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_repository` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_fs_id` int(11) DEFAULT NULL,
  `at_dp_id` int(11) unsigned DEFAULT NULL,
  `at_dp_by` int(11) unsigned DEFAULT NULL,
  `at_dp_date` datetime DEFAULT NULL,
  `at_fs_date` datetime DEFAULT NULL,
  `at_fs_by` int(11) DEFAULT NULL,
  `at_processing_fee` enum('ISSUED','PAID') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_tution_fee` enum('ISSUED','PAID') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_offer_acceptance_date` datetime DEFAULT NULL,
  `at_default_qlevel` int(11) DEFAULT NULL,
  `at_reason` int(11) DEFAULT NULL,
  `IdLandscape` bigint(20) DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '1',
  `at_remarks` longtext COLLATE utf8mb4_unicode_ci,
  `at_ref_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`at_trans_id`),
  UNIQUE KEY `at_pes_id` (`at_pes_id`),
  KEY `at_appl_id` (`at_appl_id`),
  KEY `at_appl_type` (`at_appl_type`),
  KEY `at_period` (`at_period`),
  KEY `at_status` (`at_status`),
  KEY `at_intake` (`at_intake`),
  KEY `entry_type` (`entry_type`),
  KEY `at_selection_status` (`at_selection_status`),
  KEY `at_document_verified` (`at_document_verified`),
  KEY `at_appl_id_2` (`at_appl_id`),
  KEY `at_appl_id_3` (`at_appl_id`),
  KEY `at_status_2` (`at_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_transaction_rosak`;
CREATE TABLE `applicant_transaction_rosak` (
  `at_trans_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `at_appl_id` int(11) NOT NULL,
  `at_appl_type` int(11) NOT NULL,
  `at_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'DRAFT,ENTRY,OFFERED,REJECTED,COMPLETE,INCOMPLETE,SHORTLISTED,KIV,ACCEPTED,DECLINE,ENROLLED.ARCHIEVE',
  `at_create_by` int(11) NOT NULL,
  `at_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `at_submit_date` datetime NOT NULL,
  `entry_type` int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  PRIMARY KEY (`at_trans_id`),
  KEY `at_appl_id` (`at_appl_id`),
  KEY `at_appl_type` (`at_appl_type`),
  KEY `at_status` (`at_status`),
  KEY `entry_type` (`entry_type`),
  KEY `at_appl_id_2` (`at_appl_id`),
  KEY `at_appl_id_3` (`at_appl_id`),
  KEY `at_status_2` (`at_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_visa`;
CREATE TABLE `applicant_visa` (
  `av_id` int(11) NOT NULL AUTO_INCREMENT,
  `av_appl_id` int(11) NOT NULL,
  `av_trans_id` int(11) NOT NULL,
  `av_malaysian_visa` int(11) NOT NULL COMMENT '1=yes, 0=no',
  `av_status` int(11) DEFAULT NULL,
  `av_expiry` date DEFAULT NULL,
  `upd_date` datetime NOT NULL,
  PRIMARY KEY (`av_id`),
  UNIQUE KEY `av_appl_id` (`av_appl_id`,`av_trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_working_experience`;
CREATE TABLE `applicant_working_experience` (
  `aw_id` int(11) NOT NULL AUTO_INCREMENT,
  `aw_appl_id` int(11) NOT NULL,
  `aw_trans_id` int(11) NOT NULL,
  `aw_industry_id` int(11) NOT NULL COMMENT 'Industry Working Experience',
  `aw_years` int(11) NOT NULL COMMENT 'Years of industry',
  `upd_date` datetime NOT NULL,
  `aw_others` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`aw_id`),
  UNIQUE KEY `aw_id` (`aw_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_ansscheme`;
CREATE TABLE `appl_ansscheme` (
  `aas_id` int(11) NOT NULL AUTO_INCREMENT,
  `aas_ptest_code` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aas_set_code` int(11) NOT NULL,
  `aas_total_quest` int(11) NOT NULL,
  `aas_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active 0:inactive',
  PRIMARY KEY (`aas_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_ansscheme_detl`;
CREATE TABLE `appl_ansscheme_detl` (
  `aad_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `aad_anscheme_id` int(11) NOT NULL,
  `aad_ques_no` int(5) NOT NULL,
  `aad_ques_ans` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`aad_anscheme_id`,`aad_ques_no`),
  UNIQUE KEY `aad_id` (`aad_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_family_job`;
CREATE TABLE `appl_family_job` (
  `afj_id` int(11) NOT NULL AUTO_INCREMENT,
  `afj_title` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `afj_description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`afj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_location`;
CREATE TABLE `appl_location` (
  `al_id` int(11) NOT NULL AUTO_INCREMENT,
  `al_location_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `al_location_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `al_address1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `al_address2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `al_city` bigint(20) DEFAULT NULL,
  `al_state` bigint(20) DEFAULT NULL,
  `al_country` bigint(20) DEFAULT NULL,
  `al_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `al_contact_person` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `al_status` tinyint(1) NOT NULL COMMENT '1=Active 0= Inactive',
  `al_update_by` bigint(20) NOT NULL,
  `al_update_date` datetime NOT NULL,
  PRIMARY KEY (`al_location_code`),
  UNIQUE KEY `al_id` (`al_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_pin_to_bank`;
CREATE TABLE `appl_pin_to_bank` (
  `billing_no` int(8) DEFAULT NULL,
  `PAYEE_ID` int(8) NOT NULL DEFAULT '0',
  `ADDRESS_1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'Universitas Trisakti',
  `BILL_REF_1` varchar(17) COLLATE utf8mb4_unicode_ci DEFAULT 'Biaya Pendaftaran',
  `BILL_REF_2` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'USM online',
  `BILL_REF_3` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT '2013/2014',
  `AMOUNT_TOTAL` int(6) DEFAULT '250000',
  `REGISTER_NO` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bank` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'BNI',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'E' COMMENT 'E -Entry - P -PROCESS',
  `intakeId` int(11) NOT NULL DEFAULT '78',
  PRIMARY KEY (`PAYEE_ID`,`REGISTER_NO`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_fee_setup`;
CREATE TABLE `appl_placement_fee_setup` (
  `apfs_id` int(11) NOT NULL AUTO_INCREMENT,
  `apfs_fee_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk appl_placement_fee_type(apft_fee_code)',
  `apfs_value` int(11) NOT NULL COMMENT 'Fee Type Prog - number of prog, fee location value location id',
  `apfs_amt` decimal(10,2) NOT NULL,
  `apfs_currency` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apfs_start_date` date NOT NULL,
  `apfs_end_date` date DEFAULT NULL,
  `apfs_create_by` int(11) NOT NULL,
  `apfs_create_date` datetime NOT NULL,
  PRIMARY KEY (`apfs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_fee_type`;
CREATE TABLE `appl_placement_fee_type` (
  `apft_fee_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apft_fee_desc` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`apft_fee_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_head`;
CREATE TABLE `appl_placement_head` (
  `aph_id` int(11) NOT NULL AUTO_INCREMENT,
  `aph_placement_code` varchar(10) CHARACTER SET utf8 NOT NULL,
  `aph_placement_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `aph_academic_year` int(11) NOT NULL COMMENT 'PK tbl_intake',
  `aph_batch` int(11) DEFAULT NULL,
  `aph_fees_program` tinyint(1) NOT NULL COMMENT '1=''Yes'', 0 =No',
  `aph_fees_location` tinyint(1) NOT NULL COMMENT '1=''Yes'', 0 =No',
  `aph_start_date` date NOT NULL,
  `aph_end_date` date NOT NULL,
  `aph_effective_date` date NOT NULL,
  `aph_create_by` int(10) NOT NULL,
  `aph_create_date` datetime NOT NULL,
  `aph_wizard` tinyint(4) NOT NULL DEFAULT '1',
  `aph_appl_running_no` int(11) NOT NULL DEFAULT '0',
  `aph_testtype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aph_placement_code`),
  UNIQUE KEY `aph_id` (`aph_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_program`;
CREATE TABLE `appl_placement_program` (
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_placement_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk appl_placement_head(apd_placement_code)',
  `app_program_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_pass_mark` int(3) NOT NULL,
  `app_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`app_placement_code`,`app_program_code`),
  UNIQUE KEY `app_id` (`app_id`),
  KEY ` appl_placement_program_fk_2` (`app_program_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_program_setup`;
CREATE TABLE `appl_placement_program_setup` (
  `apps_id` int(11) NOT NULL AUTO_INCREMENT,
  `apps_program_id` bigint(20) unsigned NOT NULL COMMENT 'fk tbl_program(idProgram)',
  `apps_comp_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apps_create_by` int(11) NOT NULL COMMENT 'fk tbl_user',
  `apps_create_date` datetime NOT NULL,
  `aph_type` smallint(6) NOT NULL DEFAULT '0' COMMENT '0: USM 1:Psychology',
  PRIMARY KEY (`apps_program_id`,`apps_comp_code`),
  UNIQUE KEY `apps_id` (`apps_id`),
  KEY `appl_placement_program_setup_ibfk_2` (`apps_comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_schedule`;
CREATE TABLE `appl_placement_schedule` (
  `aps_id` int(11) NOT NULL AUTO_INCREMENT,
  `aps_placement_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk to appl_placement_head(aph_placement_code)',
  `aps_location_id` int(11) NOT NULL COMMENT 'fk appl_location',
  `aps_test_date` date NOT NULL,
  `aps_start_time` time NOT NULL,
  PRIMARY KEY (`aps_id`),
  KEY `appl_placement_schedule_ibfk_1` (`aps_placement_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_schedule_seatno`;
CREATE TABLE `appl_placement_schedule_seatno` (
  `apss_id` int(11) NOT NULL AUTO_INCREMENT,
  `apss_aps_id` int(11) NOT NULL,
  `apss_room_id` int(11) NOT NULL,
  `apss_exam_capasity` int(11) NOT NULL,
  `apss_exam_apply` int(11) NOT NULL DEFAULT '0',
  `apss_examno_flag` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'F',
  `apss_examno_f` int(3) NOT NULL DEFAULT '0',
  `apss_examno_l` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`apss_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_schedule_time`;
CREATE TABLE `appl_placement_schedule_time` (
  `apst_id` int(11) NOT NULL AUTO_INCREMENT,
  `apst_aps_id` int(11) NOT NULL COMMENT 'fk appl_placement_schedule(aps_id)',
  `apst_test_type` int(11) NOT NULL COMMENT 'fk appl_test_type(act_id)',
  `apst_time_start` time NOT NULL,
  `apst_time_end` time DEFAULT NULL,
  PRIMARY KEY (`apst_id`),
  KEY `appl_placement_schedule_time_fk1` (`apst_aps_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_weightage`;
CREATE TABLE `appl_placement_weightage` (
  `apw_id` int(11) NOT NULL AUTO_INCREMENT,
  `apw_app_id` int(11) NOT NULL,
  `apw_apd_id` int(11) NOT NULL COMMENT 'fk appl_placement_detl(apd_id)',
  `apw_weightage` int(3) NOT NULL,
  PRIMARY KEY (`apw_app_id`,`apw_apd_id`),
  UNIQUE KEY `apw_id` (`apw_id`),
  KEY `appl_placement_weightage_fk_2` (`apw_apd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_program_req`;
CREATE TABLE `appl_program_req` (
  `apr_id` int(11) NOT NULL AUTO_INCREMENT,
  `apr_academic_year` int(11) NOT NULL COMMENT 'fk tbl_program(program_code)',
  `apr_program_code` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apr_decipline_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`apr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_program_req_mig`;
CREATE TABLE `appl_program_req_mig` (
  `AcademicYear` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Periode` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ProgramCode` int(3) DEFAULT NULL,
  `HighSchoolDeciplineCode` int(3) DEFAULT NULL,
  `semester` int(1) DEFAULT NULL,
  `EntranceCode` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_room`;
CREATE TABLE `appl_room` (
  `av_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `av_location_code` int(11) unsigned NOT NULL COMMENT 'FK to tbl_appl_location',
  `av_room_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `av_room_name_short` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `av_room_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `av_building` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `av_tutorial_capacity` int(11) NOT NULL DEFAULT '0',
  `av_exam_capacity` int(11) NOT NULL DEFAULT '0',
  `av_update_by` bigint(20) NOT NULL,
  `av_update_date` datetime NOT NULL,
  `av_status` tinyint(1) NOT NULL DEFAULT '1',
  `av_seq` int(11) DEFAULT NULL,
  PRIMARY KEY (`av_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_room_assign`;
CREATE TABLE `appl_room_assign` (
  `ara_id` int(11) NOT NULL AUTO_INCREMENT,
  `ara_program1` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk_app_id',
  `ara_program2` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ara_room` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ara_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_room_test_type`;
CREATE TABLE `appl_room_test_type` (
  `artt_id` int(11) NOT NULL AUTO_INCREMENT,
  `artt_code` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `artt_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`artt_code`),
  UNIQUE KEY `artt_id` (`artt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_room_type`;
CREATE TABLE `appl_room_type` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_room_id` int(11) NOT NULL,
  `art_test_type` int(3) NOT NULL,
  PRIMARY KEY (`art_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_test_type`;
CREATE TABLE `appl_test_type` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT,
  `act_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `act_start_time` time DEFAULT NULL,
  `act_end_time` time DEFAULT NULL,
  PRIMARY KEY (`act_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_upload_file`;
CREATE TABLE `appl_upload_file` (
  `auf_id` int(11) NOT NULL AUTO_INCREMENT,
  `auf_appl_id` int(11) NOT NULL COMMENT 'changed to transaction_id',
  `auf_file_name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auf_file_type` int(11) DEFAULT NULL,
  `auf_upload_date` datetime NOT NULL,
  `auf_upload_by` int(11) NOT NULL,
  `pathupload` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`auf_id`),
  KEY `auf_appl_id` (`auf_appl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `assessment_type`;
CREATE TABLE `assessment_type` (
  `at_id` int(11) NOT NULL AUTO_INCREMENT,
  `at_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_description` text COLLATE utf8mb4_unicode_ci,
  `at_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_markentry_readonly` tinyint(4) DEFAULT '0',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddt` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `attendance_mark`;
CREATE TABLE `attendance_mark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL COMMENT 'FK tbl_studentregistration',
  `IdProgram` int(11) NOT NULL COMMENT 'FK tbl_program',
  `IdSubject` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_subjectmaster',
  `coursegroup_id` int(11) NOT NULL COMMENT 'FK course_group',
  `coursegroupschedule_id` int(11) NOT NULL COMMENT 'FK course_group_schedule',
  `marks` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'in percentage',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `auditlog`;
CREATE TABLE `auditlog` (
  `audit_id` int(11) NOT NULL AUTO_INCREMENT,
  `tableName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rowPK` int(11) DEFAULT NULL,
  `fieldName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` blob,
  `new_value` blob,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`audit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `audit_log`;
CREATE TABLE `audit_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log_module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_identity` bigint(10) unsigned NOT NULL,
  `log_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `log_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_user` int(10) unsigned NOT NULL,
  `log_date` datetime NOT NULL,
  `log_ip` int(10) unsigned NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `log_identity` (`log_identity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_exam_registration`;
CREATE TABLE `batch_exam_registration` (
  `ber_id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'batch_no',
  `corporate_id` int(11) NOT NULL COMMENT 'tbl_takafuloperator',
  `inhouse` int(11) NOT NULL DEFAULT '0' COMMENT '0:Non-Inhouse 1:Inhouse',
  `IdProgram` int(11) NOT NULL,
  `IdLandscape` int(11) NOT NULL,
  `examsetup_id` int(11) NOT NULL COMMENT 'exam_setup.es_id',
  `type_nationality` int(11) NOT NULL DEFAULT '579',
  `total_student` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee_structure.fs_id',
  `submitted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes',
  `paid_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Unpaid 1:Paid',
  `paymentmode` int(11) NOT NULL DEFAULT '0',
  `at_processing_fee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_role` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `tbe_id` int(11) DEFAULT NULL COMMENT 'tbe.idBatchRegistration',
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_updatedate` datetime DEFAULT NULL COMMENT 'to update wrong id',
  PRIMARY KEY (`ber_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_exam_registration_history`;
CREATE TABLE `batch_exam_registration_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ber_id` int(11) NOT NULL,
  `batch_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'batch_no',
  `corporate_id` int(11) NOT NULL COMMENT 'tbl_takafuloperator',
  `inhouse` int(11) NOT NULL DEFAULT '0' COMMENT '0:Non-Inhouse 1:Inhouse',
  `IdProgram` int(11) NOT NULL,
  `IdLandscape` int(11) NOT NULL,
  `examsetup_id` int(11) NOT NULL COMMENT 'exam_setup.es_id',
  `type_nationality` int(11) NOT NULL DEFAULT '579',
  `total_student` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee_structure.fs_id',
  `submitted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes',
  `paid_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Unpaid 1:Paid',
  `paymentmode` int(11) NOT NULL DEFAULT '0',
  `at_processing_fee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_role` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `tbe_id` int(11) DEFAULT NULL COMMENT 'tbe.idBatchRegistration',
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_updatedate` datetime DEFAULT NULL COMMENT 'to update wrong id',
  `remarks` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdStudentRegSubjects` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_exam_schedule`;
CREATE TABLE `batch_exam_schedule` (
  `bes_id` int(11) NOT NULL AUTO_INCREMENT,
  `ber_id` int(11) NOT NULL COMMENT 'batch_exam_registration.ber_id',
  `es_id` int(11) NOT NULL COMMENT 'exam_schedule.es_id',
  `examtaggingslot_id` int(11) NOT NULL,
  `examcenter_id` int(11) NOT NULL,
  `individual` int(11) NOT NULL DEFAULT '0' COMMENT '0:Main Schedule, 1:Individual',
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `tbe_migratedate` datetime NOT NULL,
  PRIMARY KEY (`bes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_registration`;
CREATE TABLE `batch_registration` (
  `btch_id` int(11) NOT NULL AUTO_INCREMENT,
  `btch_corporate_id` int(11) NOT NULL,
  `btch_upl_id` int(11) NOT NULL,
  `btch_no` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `btch_program_id` int(11) NOT NULL,
  `btch_scheme_id` int(11) NOT NULL,
  `btch_intake_id` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `btch_landscape_id` int(11) NOT NULL,
  `btch_contact_person` int(11) NOT NULL COMMENT 'fk takaful_operator',
  `btch_type_nationality` int(11) DEFAULT NULL,
  `number_candidate` int(11) NOT NULL,
  `number_candidate_maximum` int(11) NOT NULL DEFAULT '0',
  `number_candidate_accepted` int(11) NOT NULL DEFAULT '0',
  `upload` int(11) NOT NULL COMMENT '1=excel,0=no',
  `closing_date` date DEFAULT '0000-00-00' COMMENT 'last date to add/edit participant',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `btch_fs_id` int(11) NOT NULL,
  `fs_id_upddate` datetime NOT NULL,
  `at_processing_fee` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_mode` int(11) DEFAULT NULL COMMENT 'fk payment_mode',
  `payment_mode_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1=done',
  `last_step` int(11) DEFAULT NULL COMMENT 'tbl_definationtypems 196',
  `step_date` datetime DEFAULT NULL,
  `btch_country_id` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `manual` int(11) DEFAULT '0' COMMENT '1=manual entry sms',
  `candidate_changes` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No Changes 1=Has Changes',
  `lms_access` int(11) DEFAULT '0' COMMENT '1=yes, 0=no ',
  `inhouse` int(11) DEFAULT '0' COMMENT '1=inhouse,0=normal ',
  `inhouse_status` int(11) DEFAULT '1' COMMENT '1=active, 0=inactive',
  `sendmail_date` datetime DEFAULT NULL,
  PRIMARY KEY (`btch_id`),
  KEY `btch_corporate_id` (`btch_corporate_id`),
  KEY `btch_upl_id` (`btch_upl_id`),
  KEY `btch_program_id` (`btch_program_id`),
  KEY `btch_scheme_id` (`btch_scheme_id`),
  KEY `btch_intake_id` (`btch_intake_id`),
  KEY `btch_landscape_id` (`btch_landscape_id`),
  KEY `btch_fs_id` (`btch_fs_id`),
  KEY `payment_mode` (`payment_mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `batch_registration_course`;
CREATE TABLE `batch_registration_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `btch_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `IdCourseTaggingGroup` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `batch_registration_schedule`;
CREATE TABLE `batch_registration_schedule` (
  `brs_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment',
  `br_id` int(11) NOT NULL COMMENT 'batch_registration.btch_id',
  `cgs_id` int(11) NOT NULL COMMENT 'course_group_schedule.sc_id',
  `course_id` int(11) NOT NULL DEFAULT '0' COMMENT 'perlu ke ada field ni??',
  `individual` int(11) NOT NULL COMMENT '0:Main Schedule, 1:Individual',
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive, 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`brs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `certificate_seq_no`;
CREATE TABLE `certificate_seq_no` (
  `csn_id` int(11) NOT NULL AUTO_INCREMENT,
  `csn_program_id` int(11) NOT NULL DEFAULT '0',
  `csn_seq_no` int(11) NOT NULL DEFAULT '1',
  `csn_year` year(4) DEFAULT NULL,
  PRIMARY KEY (`csn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `certificate_template`;
CREATE TABLE `certificate_template` (
  `crt_id` int(11) NOT NULL AUTO_INCREMENT,
  `crt_content` text NOT NULL,
  `crt_updby` int(11) NOT NULL DEFAULT '0',
  `crt_upddate` datetime DEFAULT NULL,
  PRIMARY KEY (`crt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `change_status`;
CREATE TABLE `change_status` (
  `cs_id` int(11) NOT NULL AUTO_INCREMENT,
  `cs_registrationId` int(11) DEFAULT NULL,
  `cs_IdStudentRegSubjects` int(11) DEFAULT NULL,
  `cs_type` int(11) NOT NULL COMMENT '1: mark approve - mark entry',
  `cs_remarks` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cs_createdby` int(11) NOT NULL,
  `cs_createddt` datetime NOT NULL,
  PRIMARY KEY (`cs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cn_double`;
CREATE TABLE `cn_double` (
  `cn_billing_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count(1)` bigint(21) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_compose`;
CREATE TABLE `comm_compose` (
  `comp_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `comp_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_tpl_id` int(10) unsigned NOT NULL,
  `comp_type` smallint(10) unsigned NOT NULL,
  `comp_rectype` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'applicants',
  `comp_lang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_totalrecipients` int(10) unsigned NOT NULL,
  `comp_rec` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp_content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`comp_id`),
  KEY `comp_module` (`comp_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_compose_attachment`;
CREATE TABLE `comm_compose_attachment` (
  `ca_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `ca_comp_id` bigint(11) unsigned NOT NULL,
  `ca_filename` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ca_fileurl` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ca_filesize` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ca_id`),
  KEY `ca_comp_id` (`ca_comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_compose_recipients`;
CREATE TABLE `comm_compose_recipients` (
  `cr_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `cr_comp_id` bigint(11) unsigned NOT NULL,
  `cr_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cr_content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cr_rec_id` int(10) unsigned NOT NULL,
  `cr_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_attachment_id` int(10) unsigned DEFAULT NULL,
  `cr_attachment_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_attachment_filename` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cr_datesent` datetime DEFAULT NULL,
  PRIMARY KEY (`cr_id`),
  KEY `cr_comp_id` (`cr_comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_groups`;
CREATE TABLE `comm_groups` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_rectype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_type` int(11) NOT NULL DEFAULT '0',
  `group_status` int(11) NOT NULL,
  `group_description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_recipients` smallint(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  KEY `module` (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_groups_recipients`;
CREATE TABLE `comm_groups_recipients` (
  `rec_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rec_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `recipient_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rec_id`),
  KEY `group_id` (`group_id`,`recipient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_template`;
CREATE TABLE `comm_template` (
  `tpl_id` int(10) NOT NULL AUTO_INCREMENT,
  `tpl_module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_type` smallint(10) unsigned NOT NULL COMMENT 'fk tbl_definations (type:121)',
  `tpl_categories` mediumtext COLLATE utf8mb4_unicode_ci,
  `email_from_name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `tpl_pname` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`tpl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_template_content`;
CREATE TABLE `comm_template_content` (
  `content_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tpl_id` int(11) unsigned NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tpl_content` mediumtext COLLATE utf8mb4_unicode_ci COMMENT '1: Standard Doc-> URL Lain2 Type -> html content',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `comm_template_tags`;
CREATE TABLE `comm_template_tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tpl_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_value` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `convocation`;
CREATE TABLE `convocation` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_year` year(4) NOT NULL,
  `c_date_from` date DEFAULT NULL,
  `c_date_to` date DEFAULT NULL,
  `c_session` int(11) NOT NULL,
  `c_capacity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'capacity per session',
  `c_guest` int(11) NOT NULL DEFAULT '0',
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`c_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='list of convocation event';


DROP TABLE IF EXISTS `convocation_application`;
CREATE TABLE `convocation_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(11) NOT NULL,
  `convocation_id` int(20) NOT NULL,
  `apply_date` datetime NOT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT 'NULL: pending; 0: reject; 1: approved',
  `status_date` datetime DEFAULT NULL,
  `status_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`),
  KEY `convocation_id` (`convocation_id`),
  KEY `status_by` (`status_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='list of convocation application';


DROP TABLE IF EXISTS `convocation_award`;
CREATE TABLE `convocation_award` (
  `ca_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdAward` int(11) NOT NULL COMMENT 'fk tbl_graduation_award',
  `convocation_id` int(11) NOT NULL COMMENT 'fk table convocation',
  `ca_createddt` datetime NOT NULL,
  `ca_createdby` int(11) NOT NULL,
  PRIMARY KEY (`ca_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `convocation_checklist`;
CREATE TABLE `convocation_checklist` (
  `cc_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL COMMENT 'fk convocation',
  `idProgram` int(11) NOT NULL COMMENT 'tbl_program',
  `cc_createddt` datetime NOT NULL,
  `cc_createdby` int(11) NOT NULL,
  PRIMARY KEY (`cc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `convocation_graduate`;
CREATE TABLE `convocation_graduate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idStudentRegistration` int(11) NOT NULL,
  `convocation_id` int(11) NOT NULL,
  `robe_collect` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `robe_return` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `robe_serial_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `robe_size` enum('S','M','L','XL','XXL') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hood_collect` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `hood_return` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `mortar_collect` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `mortar_return` tinyint(4) DEFAULT '0' COMMENT '0:No 1:Yes',
  `robe_date_collected` date DEFAULT NULL,
  `robe_date_returned` date DEFAULT NULL,
  `hood_date_collected` date DEFAULT NULL,
  `hood_date_returned` date DEFAULT NULL,
  `mortar_date_collected` date DEFAULT NULL,
  `mortar_date_returned` date DEFAULT NULL,
  `date_collected` date DEFAULT NULL,
  `date_returned` date DEFAULT NULL,
  `add_by` bigint(20) NOT NULL,
  `add_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idStudentRegistration` (`idStudentRegistration`,`convocation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='store graduate list that has been approved to follow convoca';


DROP TABLE IF EXISTS `convocation_program_checklist`;
CREATE TABLE `convocation_program_checklist` (
  `cpc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cc_id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `idChecklist` int(11) NOT NULL,
  PRIMARY KEY (`cpc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group`;
CREATE TABLE `course_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'fk tbl_subjectmaster',
  `maxstud` int(11) NOT NULL,
  `IdLecturer` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IdSubject` (`IdSubject`),
  KEY `IdLecturer` (`IdLecturer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_attendance`;
CREATE TABLE `course_group_attendance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSchedule` bigint(20) NOT NULL COMMENT 'fk course_group_schedule_detail',
  `IdStudentRegistration` bigint(20) unsigned NOT NULL COMMENT 'fk tbl_studentregistration',
  `status` int(11) DEFAULT NULL COMMENT 'fk_tbl_definationms 91',
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_attendance_detail`;
CREATE TABLE `course_group_attendance_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `course_group_att_id` bigint(20) NOT NULL COMMENT 'fk course_group_attendance',
  `student_id` bigint(20) NOT NULL COMMENT 'fk tbl_studentregistration',
  `student_nim` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL COMMENT 'fk_tbl_definationms',
  `last_edit_by` bigint(20) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_program`;
CREATE TABLE `course_group_program` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `program_id` bigint(20) NOT NULL,
  `program_scheme_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`,`program_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_schedule`;
CREATE TABLE `course_group_schedule` (
  `sc_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdSchedule` int(11) NOT NULL COMMENT 'fk course_group_schedule',
  `IdScheduleParent` int(11) DEFAULT '0',
  `idClassType` int(11) DEFAULT NULL,
  `sc_date_start` date NOT NULL,
  `sc_date_end` date NOT NULL,
  `sc_duration` int(11) NOT NULL,
  `sc_start_time` time NOT NULL,
  `sc_end_time` time NOT NULL,
  `sc_venue` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sc_address` text COLLATE utf8mb4_unicode_ci,
  `sc_class` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sc_day` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sc_remark` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdLecturer` int(11) DEFAULT NULL,
  `sc_createdby` int(11) NOT NULL,
  `sc_createddt` datetime NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `sc_visibility` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `sc_custom` int(11) NOT NULL,
  PRIMARY KEY (`sc_id`),
  KEY `IdSchedule` (`IdSchedule`),
  KEY `idClassType` (`idClassType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_group_schedule_detail`;
CREATE TABLE `course_group_schedule_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scDetId` int(11) NOT NULL COMMENT 'fk course_group_schedule_detail',
  `sc_date` date NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scDetId` (`scDetId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_language`;
CREATE TABLE `course_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseid` varchar(50) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `course_migration_ce`;
CREATE TABLE `course_migration_ce` (
  `IdSemester` int(11) DEFAULT NULL,
  `IdSubject` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `studentId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdProgram` int(11) DEFAULT NULL,
  `grade_name` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shariah` decimal(11,2) DEFAULT NULL,
  `finance` decimal(11,2) DEFAULT NULL,
  `migrate_message` longtext COLLATE utf8mb4_unicode_ci,
  `migrate_status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `migrate_by` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `course_migration_ct`;
CREATE TABLE `course_migration_ct` (
  `program` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `studentId` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdSubject` int(11) DEFAULT NULL,
  `IdProgram` int(11) DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `migrate_by` int(11) DEFAULT NULL,
  `migrate_status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_message` longtext COLLATE utf8mb4_unicode_ci,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `creditcard_terminal`;
CREATE TABLE `creditcard_terminal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terminal_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` bigint(20) NOT NULL COMMENT 'fk tbl_branchofficevenue',
  `default` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: true; 0: false',
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1: active; 0: incative',
  PRIMARY KEY (`id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `credit_note`;
CREATE TABLE `credit_note` (
  `cn_trans_id` int(11) DEFAULT NULL,
  `cn_corporate_id` int(11) DEFAULT '0',
  `cn_IdStudentRegistration` int(11) DEFAULT NULL,
  `cn_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cn_billing_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_fomulir` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_appl_id` bigint(20) DEFAULT NULL,
  `cn_amount` decimal(20,2) NOT NULL,
  `cn_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn_creator` bigint(20) NOT NULL,
  `cn_create_date` datetime NOT NULL,
  `cn_approver` bigint(20) DEFAULT NULL,
  `cn_approve_date` datetime DEFAULT NULL,
  `cn_cancel_by` bigint(20) DEFAULT NULL,
  `cn_cancel_date` datetime DEFAULT NULL,
  `cn_cur_id` int(11) NOT NULL DEFAULT '1',
  `cn_source` int(11) NOT NULL COMMENT '0=sms, 1=student portal',
  `cn_invoice_id` int(11) NOT NULL,
  `cn_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT 'A=active/approved, X=cancel,0=entry',
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cn_type` int(11) NOT NULL,
  `type_amount` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cn_exam_registration_main_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cn_id`),
  KEY `cn_fomulir` (`cn_fomulir`),
  KEY `appl_id` (`cn_appl_id`),
  KEY `cn_trans_id` (`cn_trans_id`),
  KEY `cn_IdStudentRegistration` (`cn_IdStudentRegistration`),
  KEY `cn_cur_id` (`cn_cur_id`),
  KEY `cn_invoice_id` (`cn_invoice_id`),
  KEY `cn_billing_no` (`cn_billing_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `credit_note_detail`;
CREATE TABLE `credit_note_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cn_id` int(11) NOT NULL,
  `invoice_main_id` int(11) NOT NULL,
  `invoice_detail_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `cur_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cn_fomulir` (`invoice_main_id`),
  KEY `appl_id` (`invoice_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cron_ol_exam`;
CREATE TABLE `cron_ol_exam` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `es_date` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_id` int(11) DEFAULT NULL,
  `results` mediumtext COLLATE utf8mb4_unicode_ci COMMENT 'if any results needed',
  `created_date` datetime NOT NULL,
  `start_date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `executed_date` datetime DEFAULT NULL,
  `executed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `execution_time` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cron_ol_exam_history`;
CREATE TABLE `cron_ol_exam_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coe_id` int(11) NOT NULL COMMENT 'cron_ol_exam',
  `created_date` datetime NOT NULL,
  `executed_date` datetime NOT NULL,
  `execution_time` float DEFAULT NULL,
  `executed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cron_ol_exam_tempdata`;
CREATE TABLE `cron_ol_exam_tempdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coe_id` int(11) NOT NULL COMMENT 'cron_ol_exam',
  `es_id` int(11) DEFAULT NULL COMMENT 'exam_setup',
  `esd_id` int(11) DEFAULT NULL COMMENT 'exam_setup_detail',
  `ec_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL COMMENT 'exam_schedule',
  `scheduletaggingslot_id` int(11) DEFAULT NULL COMMENT 'exam_scheduletaggingslot',
  `idExam` int(11) DEFAULT NULL,
  `total_set` int(11) DEFAULT NULL,
  `total_student` int(11) DEFAULT NULL,
  `total_student_generated` int(11) DEFAULT NULL,
  `pe_id` int(11) DEFAULT NULL,
  `idmarkdistribution` int(11) DEFAULT NULL,
  `results` text,
  `executed` int(11) DEFAULT NULL COMMENT '1=yes,0=no',
  `ep_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `executed_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cron_service`;
CREATE TABLE `cron_service` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `results` mediumtext COLLATE utf8mb4_unicode_ci COMMENT 'if any results needed',
  `created_date` datetime NOT NULL,
  `executed_date` datetime DEFAULT NULL,
  `executed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `execution_time` float unsigned DEFAULT NULL,
  `inprogress` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recurring` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recurring_day` tinyint(2) unsigned DEFAULT NULL,
  `recurring_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recurring_lastexecuted` date DEFAULT NULL,
  `date` date DEFAULT NULL COMMENT 'to pass specific date to execute',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `data_fin_invoice`;
CREATE TABLE `data_fin_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studid` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `studname` char(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invno` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invdate` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invdesc` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invtype` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invamt_rm` decimal(10,2) NOT NULL,
  `invamt_usd` decimal(10,2) NOT NULL,
  `semester` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `programme` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee_code` int(11) NOT NULL,
  `migrateDate` datetime NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `doctype` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sponsor` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exist` int(11) NOT NULL COMMENT '1=not exist',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `data_fin_os_release`;
CREATE TABLE `data_fin_os_release` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studid` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `studname` varchar(41) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invdate` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invno` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `COL 6` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `COL 7` varchar(42) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee_code` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invtype` varchar(37) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invamt_rm` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invamt_usd` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invdesc` varchar(59) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrateDate` datetime NOT NULL,
  `invoice_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `data_fin_paid`;
CREATE TABLE `data_fin_paid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invno` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `dateUpd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `data_fin_renewal`;
CREATE TABLE `data_fin_renewal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studid` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `studname` varchar(39) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `COL 3` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `COL 4` varchar(42) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invdesc` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee_code` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invamt_usd` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invamt_rm` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_id` int(11) NOT NULL,
  `migrateDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `data_migration_exam_result_4`;
CREATE TABLE `data_migration_exam_result_4` (
  `No` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(61) DEFAULT NULL,
  `Mode` varchar(11) DEFAULT NULL,
  `Type` varchar(8) DEFAULT NULL,
  `Category` varchar(13) DEFAULT NULL,
  `Code_Org` varchar(15) DEFAULT NULL,
  `Organization` varchar(59) DEFAULT NULL,
  `Nric` varchar(16) DEFAULT NULL,
  `Program` varchar(62) DEFAULT NULL,
  `Program_Code` varchar(7) DEFAULT NULL,
  `Exam_Date` varchar(15) DEFAULT NULL,
  `Grade` varchar(16) DEFAULT NULL,
  `Marks` varchar(2) DEFAULT NULL,
  `Passing_Marks` varchar(2) DEFAULT NULL,
  `Award` varchar(21) DEFAULT NULL,
  `Date_of_award` varchar(10) DEFAULT NULL,
  `Exam_Centre` varchar(33) DEFAULT NULL,
  `Serial_Certificate_No` varchar(16) DEFAULT NULL,
  `Exam_Type` varchar(13) DEFAULT NULL,
  `Exam_Name` varchar(20) NOT NULL,
  `Remarks` varchar(37) DEFAULT NULL,
  `mapping_status` int(10) DEFAULT NULL,
  `mapping_remarks` varchar(100) DEFAULT NULL,
  `takaful_operator_id` int(11) DEFAULT NULL,
  `student_profile_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `program_scheme_id` int(11) DEFAULT NULL,
  `landscape_id` int(11) DEFAULT NULL,
  `programme_exam_id` int(11) DEFAULT NULL,
  `mark_dist_setup_id` int(11) DEFAULT NULL,
  `mark_dist_master_id` int(11) DEFAULT NULL,
  `mark_dist_detail_id` int(11) DEFAULT NULL,
  `exam_setup_id` int(11) DEFAULT NULL,
  `exam_setup_detail_id` int(11) DEFAULT NULL,
  `exam_center_id` int(11) DEFAULT NULL,
  `exam_schedule_id` int(11) DEFAULT NULL,
  `examtaggingslot_id` int(11) DEFAULT NULL,
  `migrate_status` varchar(10) DEFAULT NULL,
  `migrate_remarks` varchar(200) DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `repeat_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `discount`;
CREATE TABLE `discount` (
  `dcnt_id` int(11) NOT NULL AUTO_INCREMENT,
  `dcnt_fomulir_id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `dcnt_discount_id` int(11) NOT NULL COMMENT 'tbl_discount',
  `dcnt_amount` decimal(20,2) NOT NULL,
  `dcnt_amount_adj` decimal(20,2) NOT NULL,
  `dcnt_description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dcnt_invoice_id` bigint(20) DEFAULT NULL,
  `dcnt_creator` int(11) NOT NULL,
  `dcnt_create_date` datetime NOT NULL,
  `dcnt_approve_by` int(11) NOT NULL,
  `dcnt_approve_date` datetime NOT NULL,
  `dcnt_cancel_by` int(11) NOT NULL,
  `dcnt_cancel_date` datetime NOT NULL,
  `dcnt_update_by` int(11) NOT NULL,
  `dcnt_update_date` datetime NOT NULL,
  `dcnt_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dcnt_currency_id` int(11) NOT NULL,
  `dcnt_IdStudentRegistration` int(11) DEFAULT '0',
  `dcnt_corporate_id` int(11) DEFAULT '0',
  PRIMARY KEY (`dcnt_id`),
  UNIQUE KEY `dcnt_fomulir_id` (`dcnt_fomulir_id`),
  KEY `dcnt_invoice_id` (`dcnt_invoice_id`),
  KEY `dcnt_creator` (`dcnt_creator`),
  KEY `dcnt_cancel_by` (`dcnt_cancel_by`),
  KEY `dcnt_approve_by` (`dcnt_approve_by`),
  KEY `dcnt_type_id` (`dcnt_discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `discount_adjustment`;
CREATE TABLE `discount_adjustment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AdjustmentCode` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AdjustmentType` int(11) NOT NULL COMMENT '1=cancel',
  `IdDiscount` int(20) NOT NULL COMMENT 'pk discount_detail',
  `Remarks` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cur_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `Status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'APPROVE, ENTRY, CANCEL',
  `EnterBy` bigint(20) DEFAULT NULL,
  `EnterDate` date DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `cancel_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `cancel_date` datetime NOT NULL,
  `cancel_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `discount_batch`;
CREATE TABLE `discount_batch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `batch_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `semester_id` int(11) NOT NULL,
  `dcnt_id` int(11) NOT NULL,
  `total_discount` int(10) unsigned NOT NULL,
  `approval` int(11) NOT NULL COMMENT '1=approved, 0=entry',
  PRIMARY KEY (`id`),
  KEY `semester_id` (`semester_id`),
  KEY `created_by` (`created_by`),
  KEY `dcnt_id` (`dcnt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `discount_detail`;
CREATE TABLE `discount_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dcnt_id` bigint(20) NOT NULL COMMENT 'fk_discount',
  `dcnt_amount` decimal(20,2) NOT NULL,
  `dcnt_invoice_id` bigint(20) DEFAULT NULL,
  `dcnt_invoice_det_id` int(11) DEFAULT NULL,
  `dcnt_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'A',
  `status_by` int(11) NOT NULL,
  `status_date` datetime NOT NULL,
  `adjustId` int(11) NOT NULL COMMENT 'pk discount_adjustment',
  PRIMARY KEY (`id`),
  KEY `dcnt_id` (`dcnt_id`),
  KEY `dcnt_invoice_id` (`dcnt_invoice_id`),
  KEY `dcnt_invoice_det_id` (`dcnt_invoice_det_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `discount_type`;
CREATE TABLE `discount_type` (
  `dt_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dt_discount` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dt_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dt_discountitem` smallint(10) unsigned NOT NULL,
  `dt_creator` bigint(20) NOT NULL,
  `dt_create_date` datetime NOT NULL,
  `dt_delete_status` tinyint(4) NOT NULL DEFAULT '0',
  `dt_delete_by` bigint(20) DEFAULT NULL,
  `dt_delete_date` datetime DEFAULT NULL,
  PRIMARY KEY (`dt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `discount_type_fee_info`;
CREATE TABLE `discount_type_fee_info` (
  `IdDiscountFeeInfo` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdDiscountType` bigint(20) NOT NULL,
  `FeeCode` bigint(20) NOT NULL,
  `CalculationMode` tinyint(1) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdDiscountFeeInfo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `discount_type_tag`;
CREATE TABLE `discount_type_tag` (
  `dtt_id` int(11) NOT NULL AUTO_INCREMENT,
  `dt_id` int(11) NOT NULL,
  `IdStudentRegistration` int(11) NOT NULL,
  `dtt_start_date` date DEFAULT NULL,
  `dtt_end_date` date DEFAULT NULL,
  `dtt_createddt` datetime NOT NULL,
  `dtt_createdby` int(11) NOT NULL,
  `dtt_modifyby` int(11) DEFAULT NULL,
  `dtt_modifydt` datetime DEFAULT NULL,
  PRIMARY KEY (`dtt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `document_verification`;
CREATE TABLE `document_verification` (
  `dv_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProgram` int(11) NOT NULL,
  `IdLandscape` int(11) NOT NULL DEFAULT '0',
  `dv_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dv_active` tinyint(4) NOT NULL DEFAULT '1',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifieddt` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`dv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `drop_registration_log`;
CREATE TABLE `drop_registration_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `er_id` int(11) DEFAULT NULL,
  `invoice_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `function_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'external id = =student_profule',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `email_blast_history`;
CREATE TABLE `email_blast_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_date` datetime NOT NULL,
  `send_by` int(11) NOT NULL,
  `application_transaction` int(11) NOT NULL,
  `subject` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1: success; 0: fail',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `email_blast_tmpl`;
CREATE TABLE `email_blast_tmpl` (
  `ebt_id` int(11) NOT NULL AUTO_INCREMENT,
  `ebt_template_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ebt_email_from` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ebt_email_from_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ebt_create_by` bigint(20) unsigned NOT NULL,
  `ebt_create_date` datetime NOT NULL,
  `ebt_update_by` bigint(20) DEFAULT NULL,
  `ebt_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ebt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `email_blast_tmpl_content`;
CREATE TABLE `email_blast_tmpl_content` (
  `ebtc_id` int(11) NOT NULL AUTO_INCREMENT,
  `ebtc_ebt_id` int(11) NOT NULL,
  `ebtc_language` tinyint(4) NOT NULL COMMENT '1: english; 2: Indonesia',
  `ebtc_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ebtc_header` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ebtc_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ebtc_footer` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ebtc_id`),
  KEY `email_template_detl_fk1` (`ebtc_ebt_id`),
  KEY `email_template_detl_fk2` (`ebtc_language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `email_que`;
CREATE TABLE `email_que` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `er_id` int(11) NOT NULL DEFAULT '0',
  `ec_id` int(11) NOT NULL DEFAULT '0',
  `recepient_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment_filename` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_path` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comm_rec_id` bigint(10) unsigned DEFAULT NULL,
  `date_que` datetime DEFAULT NULL,
  `date_publish` datetime DEFAULT NULL,
  `date_send` datetime DEFAULT NULL,
  `retry_count` int(11) NOT NULL DEFAULT '0',
  `pull_date` datetime DEFAULT NULL,
  `pull_status` tinyint(4) DEFAULT NULL COMMENT '1:yes',
  `smtp` tinyint(4) NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `email_que_attachment`;
CREATE TABLE `email_que_attachment` (
  `eqa_id` int(11) NOT NULL AUTO_INCREMENT,
  `eqa_emailque_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk to email_que',
  `eqa_filename` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eqa_path` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eqa_updDate` datetime NOT NULL,
  `eqa_updUser` int(11) NOT NULL,
  PRIMARY KEY (`eqa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `email_smtp`;
CREATE TABLE `email_smtp` (
  `esmtp_id` int(11) NOT NULL AUTO_INCREMENT,
  `smtp_username` varchar(50) NOT NULL,
  `smtp_password` varchar(50) NOT NULL,
  `smtp_port` int(11) NOT NULL,
  `smtp_server` varchar(50) NOT NULL,
  `smtp_from` varchar(50) NOT NULL,
  `smtp_from_email` varchar(50) NOT NULL,
  `emailcc` varchar(50) NOT NULL,
  `emailbcc` varchar(50) NOT NULL,
  PRIMARY KEY (`esmtp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `email_template_detl`;
CREATE TABLE `email_template_detl` (
  `etd_id` int(11) NOT NULL AUTO_INCREMENT,
  `etd_eth_id` int(11) NOT NULL,
  `etd_language` tinyint(4) NOT NULL,
  `etd_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etd_header` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etd_body` varchar(7277) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etd_footer` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`etd_id`),
  KEY `email_template_detl_fk1` (`etd_eth_id`),
  KEY `email_template_detl_fk2` (`etd_language`),
  CONSTRAINT `email_template_detl_ibfk_1` FOREIGN KEY (`etd_eth_id`) REFERENCES `email_template_head` (`eth_id`),
  CONSTRAINT `email_template_detl_ibfk_2` FOREIGN KEY (`etd_language`) REFERENCES `applicant_language` (`al_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `email_template_head`;
CREATE TABLE `email_template_head` (
  `eth_id` int(11) NOT NULL AUTO_INCREMENT,
  `eth_template_name` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eth_email_from` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eth_email_from_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eth_create_by` bigint(20) unsigned NOT NULL,
  `eth_create_date` datetime NOT NULL,
  PRIMARY KEY (`eth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_calendar`;
CREATE TABLE `exam_calendar` (
  `ec_id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_idActivity` int(11) NOT NULL,
  `ec_idSemester` int(11) NOT NULL,
  `ec_start_date` date NOT NULL,
  `ec_end_date` date NOT NULL,
  `ec_createdt` datetime NOT NULL,
  `ec_createdby` int(11) NOT NULL,
  PRIMARY KEY (`ec_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_center_setup`;
CREATE TABLE `exam_center_setup` (
  `ecs_id` int(11) NOT NULL AUTO_INCREMENT,
  `idSemester` int(11) NOT NULL,
  `ec_id` int(11) NOT NULL,
  `ecs_publish` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Not Publish 1: Publish',
  `ecs_createddt` datetime NOT NULL,
  `ecs_createdby` int(11) NOT NULL,
  PRIMARY KEY (`ecs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_change_status`;
CREATE TABLE `exam_change_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_change_status_id',
  `IdStudentRegistration` int(11) NOT NULL,
  `examregistration_main_id` int(11) NOT NULL COMMENT 'FK exam_registration_main',
  `examregistration_id` int(11) NOT NULL COMMENT 'FK exam_registration',
  `type` int(11) NOT NULL COMMENT '1=Deferment, 2=Withdrawal/Cancel',
  `reasons` text COLLATE utf8mb4_unicode_ci,
  `old_examschedule_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_schedule',
  `new_examschedule_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_schedule',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `old_examtaggingslot_id` int(11) NOT NULL COMMENT 'exam_scheduletaggingslot',
  `new_examtaggingslot_id` int(11) NOT NULL COMMENT 'exam_scheduletaggingslot',
  `old_examcenter_id` int(11) NOT NULL COMMENT 'tbl_examcenter',
  `new_examcenter_id` int(11) NOT NULL COMMENT 'tbl_examcenter',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_configuration`;
CREATE TABLE `exam_configuration` (
  `ec_id` int(11) NOT NULL AUTO_INCREMENT,
  `cont_prob_sem` tinyint(4) NOT NULL COMMENT '0:No 1:yes',
  `max_prob_sem_allow` int(11) NOT NULL,
  `mark_dist_approval` tinyint(4) NOT NULL COMMENT '0:No 1:yes',
  `ec_updatedt` datetime NOT NULL,
  `ec_updateby` int(11) NOT NULL,
  PRIMARY KEY (`ec_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_document_checklist`;
CREATE TABLE `exam_document_checklist` (
  `dc_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `doc_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL COMMENT '0:No 1:yes',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifieddt` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`dc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_group`;
CREATE TABLE `exam_group` (
  `eg_id` int(20) NOT NULL AUTO_INCREMENT,
  `eg_sem_id` int(10) unsigned NOT NULL,
  `eg_sub_id` int(10) unsigned NOT NULL,
  `eg_group_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eg_group_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eg_assessment_type` bigint(20) DEFAULT NULL COMMENT 'fk tbl_examination_assessment_type',
  `eg_capacity` int(11) NOT NULL,
  `eg_room_id` int(11) unsigned DEFAULT NULL COMMENT 'fk appl_room',
  `eg_date` date NOT NULL,
  `eg_repeat_status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eg_start_time` time NOT NULL,
  `eg_end_time` time NOT NULL,
  `eg_create_by` bigint(20) NOT NULL,
  `eg_create_date` datetime NOT NULL,
  PRIMARY KEY (`eg_id`),
  KEY `eg_room_id` (`eg_room_id`),
  KEY `eg_sem_id` (`eg_sem_id`,`eg_sub_id`),
  KEY `eg_assessment_type` (`eg_assessment_type`),
  CONSTRAINT `exam_group_ibfk_1` FOREIGN KEY (`eg_room_id`) REFERENCES `appl_room` (`av_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_group_attendance`;
CREATE TABLE `exam_group_attendance` (
  `ega_id` int(11) NOT NULL AUTO_INCREMENT,
  `ega_eg_id` int(11) NOT NULL COMMENT 'fk exam_group',
  `ega_student_id` bigint(20) NOT NULL COMMENT 'fk tbl_studentregistration',
  `ega_student_nim` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ega_status` int(11) NOT NULL COMMENT 'fk_tbl_definationms',
  `ega_last_edit_by` bigint(20) NOT NULL,
  `ega_last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`ega_id`),
  UNIQUE KEY `ega_eg_id_2` (`ega_eg_id`,`ega_student_id`),
  KEY `ega_eg_id` (`ega_eg_id`),
  CONSTRAINT `exam_group_attendance_ibfk_1` FOREIGN KEY (`ega_eg_id`) REFERENCES `exam_group` (`eg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_group_program`;
CREATE TABLE `exam_group_program` (
  `egp_id` int(11) NOT NULL AUTO_INCREMENT,
  `egp_eg_id` int(11) NOT NULL COMMENT 'fk exam_group',
  `egp_program_id` int(11) NOT NULL COMMENT 'fk tbl_program',
  PRIMARY KEY (`egp_id`),
  KEY `egp_eg_id` (`egp_eg_id`,`egp_program_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_group_student`;
CREATE TABLE `exam_group_student` (
  `egst_id` int(11) NOT NULL AUTO_INCREMENT,
  `egst_group_id` int(11) NOT NULL COMMENT 'fk exam_group',
  `egst_student_id` int(11) NOT NULL COMMENT 'fk tbl_studentregistration',
  `egst_student_nim` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `egst_subject_id` int(11) NOT NULL,
  `egst_semester_id` bigint(20) NOT NULL COMMENT 'fk tbl_semesterMain',
  `egst_seat` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`egst_id`),
  KEY `egst_group_id` (`egst_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_group_supervisor`;
CREATE TABLE `exam_group_supervisor` (
  `egs_id` int(11) NOT NULL AUTO_INCREMENT,
  `egs_eg_id` int(11) NOT NULL,
  `egs_staff_id` bigint(20) unsigned NOT NULL,
  `egs_create_by` bigint(20) NOT NULL,
  `egs_create_date` datetime NOT NULL,
  PRIMARY KEY (`egs_id`),
  KEY `egs_eg_id` (`egs_eg_id`,`egs_staff_id`),
  CONSTRAINT `exam_group_supervisor_ibfk_1` FOREIGN KEY (`egs_eg_id`) REFERENCES `exam_group` (`eg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_registration`;
CREATE TABLE `exam_registration` (
  `er_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_login` int(11) NOT NULL DEFAULT '0',
  `er_registration_type` int(11) NOT NULL COMMENT '0:pre-register 1:Register 2: Drop 3:Withdraw 4:Repeat 5:Refer 6:CT 9:Pre-Repeat',
  `reschedule` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=false, 1=true',
  `must_reschedule` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=false, 1=true',
  `er_main_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_registration_main',
  `registrationId` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ber_id` int(11) NOT NULL DEFAULT '0' COMMENT 'batch exam registration id',
  `bes_id` int(11) NOT NULL DEFAULT '0',
  `landscape_id` int(11) DEFAULT NULL,
  `intakeprogram_id` int(11) DEFAULT NULL,
  `student_id` int(11) NOT NULL COMMENT 'tbl_studentregistration',
  `examsetup_id` int(11) NOT NULL,
  `examsetupdetail_id` int(11) NOT NULL,
  `examschedule_id` int(11) NOT NULL,
  `examtaggingslot_id` int(11) NOT NULL COMMENT 'exam_scheduletaggingslot',
  `examcenter_id` int(11) NOT NULL COMMENT 'tbl_examcenter',
  `examroom_id` int(11) NOT NULL DEFAULT '0',
  `seat_no` int(11) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:pending 1:paid:3:payment cancelled (tbe)',
  `total_marks` double DEFAULT NULL,
  `pass` tinyint(4) DEFAULT NULL COMMENT '0:Fail 1:Pass',
  `grade_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '0:Fail 1:Pass',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifieddt` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `exam_status` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'C: Completed IN:Incomplete MG:Missing Grade F:Fraud NR:No Record',
  `attendance_status` int(11) DEFAULT '0' COMMENT 'FK definationms',
  `attendance_updby` int(11) DEFAULT NULL,
  `attendance_upddt` datetime DEFAULT NULL,
  `attendance_updrole` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `push_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: No 1:yes',
  `pull_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '	1:yes 2:No (default 0 no pending payment)',
  `mc_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result_status` int(11) DEFAULT '0' COMMENT '0:Entry 1:Pending 2:Approved',
  `result_endorsed_by` int(11) NOT NULL DEFAULT '0',
  `result_endorsed_date` datetime DEFAULT NULL,
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee structure',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `IdProgram` int(11) DEFAULT NULL,
  `pull_status` tinyint(4) DEFAULT NULL COMMENT '1:yes',
  `pull_date` datetime DEFAULT NULL,
  `award` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `award_date` date DEFAULT NULL,
  `Serial_Certificate_No` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_id` int(11) DEFAULT NULL,
  `tbe_examresult_migratedt` datetime DEFAULT NULL,
  `IdStudentRegSubjects` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`er_id`),
  KEY `student_id` (`student_id`),
  KEY `examsetup_id` (`examsetup_id`),
  KEY `examsetupdetail_id` (`examsetupdetail_id`),
  KEY `examschedule_id` (`examschedule_id`),
  KEY `examtaggingslot_id` (`examtaggingslot_id`),
  KEY `examcenter_id` (`examcenter_id`),
  KEY `examroom_id` (`examroom_id`),
  KEY `IdProgram` (`IdProgram`),
  KEY `er_main_id` (`er_main_id`),
  KEY `ber_id` (`ber_id`),
  KEY `bes_id` (`bes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_registration_cycle`;
CREATE TABLE `exam_registration_cycle` (
  `erc_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) unsigned NOT NULL,
  `cycle_no` int(10) unsigned NOT NULL DEFAULT '0',
  `cycle_status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '1: Complete 6 sitting based on landscape',
  `first_exam_sitting` int(10) unsigned NOT NULL COMMENT 'fk exam_sitting',
  `erc_createddt` datetime NOT NULL,
  `erc_createdby` int(11) NOT NULL,
  PRIMARY KEY (`erc_id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `exam_registration_history`;
CREATE TABLE `exam_registration_history` (
  `erh_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_login` int(11) DEFAULT NULL,
  `er_id` int(11) NOT NULL,
  `IdProgram` int(11) DEFAULT NULL,
  `registrationId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `er_registration_type` int(11) NOT NULL,
  `reschedule` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=false, 1=true',
  `must_reschedule` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=false, 1=true',
  `er_main_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_registration_main',
  `ber_id` int(11) DEFAULT '0',
  `bes_id` int(11) NOT NULL DEFAULT '0',
  `landscape_id` int(11) DEFAULT NULL,
  `intakeprogram_id` int(11) DEFAULT NULL,
  `student_id` int(11) NOT NULL COMMENT 'PK tbl_studentregistration',
  `examsetup_id` int(11) NOT NULL,
  `examsetupdetail_id` int(11) NOT NULL,
  `examschedule_id` int(11) NOT NULL,
  `examtaggingslot_id` int(11) NOT NULL COMMENT 'exam_scheduletaggingslot',
  `examcenter_id` int(11) NOT NULL COMMENT 'tbl_examcenter',
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `examroom_id` int(11) NOT NULL DEFAULT '0',
  `seat_no` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `total_marks` double DEFAULT NULL,
  `pass` tinyint(4) DEFAULT NULL,
  `grade_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exam_status` tinyint(4) DEFAULT '0' COMMENT '0: Draft, 1: Pending for approval, 2: approved',
  `attendance_status` int(11) DEFAULT '0' COMMENT 'FK definationms',
  `attendance_updby` int(11) DEFAULT NULL,
  `attendance_upddt` datetime DEFAULT NULL,
  `attendance_updrole` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `push_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: No 1:yes',
  `pull_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '	1:yes 2:No (default 0 no pending payment)',
  `mc_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifieddt` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `result_status` int(11) DEFAULT '0' COMMENT '0:Entry 1:Pending 2:Approved',
  `result_endorsed_by` int(11) NOT NULL DEFAULT '0',
  `result_endorsed_date` datetime DEFAULT NULL,
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `pull_status` int(11) DEFAULT NULL,
  `pull_date` datetime DEFAULT NULL,
  `award` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `award_date` datetime DEFAULT NULL,
  `Serial_Certificate_No` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_id` int(10) DEFAULT '0',
  `tbe_examresult_migratedt` datetime DEFAULT NULL,
  `IdStudentRegSubjects` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`erh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exam_registration_inceif`;
CREATE TABLE `exam_registration_inceif` (
  `er_id` int(11) NOT NULL AUTO_INCREMENT,
  `er_idCountry` int(11) DEFAULT NULL,
  `er_idCity` int(11) DEFAULT NULL,
  `er_idCityOthers` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `er_ec_id` int(11) DEFAULT NULL,
  `er_idSemester` int(11) NOT NULL,
  `er_idProgram` int(11) NOT NULL,
  `er_idStudentRegistration` int(11) NOT NULL,
  `er_idSubject` int(11) NOT NULL,
  `er_status` int(11) NOT NULL,
  `er_attendance_status` int(11) DEFAULT NULL,
  `er_attendance_by` int(11) DEFAULT NULL,
  `er_attendance_dt` datetime DEFAULT NULL,
  `mc_start_date` date DEFAULT NULL,
  `mc_end_date` date DEFAULT NULL,
  `mc_reason` mediumtext COLLATE utf8mb4_unicode_ci,
  `er_createdby` int(11) NOT NULL,
  `er_createddt` datetime NOT NULL,
  PRIMARY KEY (`er_id`),
  KEY `er_idCountry` (`er_idCountry`),
  KEY `er_idCity` (`er_idCity`),
  KEY `er_examcenter_id` (`er_ec_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_registration_log`;
CREATE TABLE `exam_registration_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `er_id` int(11) DEFAULT NULL,
  `function_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_registration_main`;
CREATE TABLE `exam_registration_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL,
  `IdProgram` int(11) NOT NULL,
  `ber_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK batch_exam_registration',
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Inactive 1=Active',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  `erc_id` int(11) NOT NULL DEFAULT '0',
  `exam_sitting_no` int(11) NOT NULL DEFAULT '0',
  `exam_sitting_id` int(11) NOT NULL DEFAULT '0',
  `exam_sitting_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:Active 0:Not active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_scaling`;
CREATE TABLE `exam_scaling` (
  `es_id` int(11) NOT NULL AUTO_INCREMENT,
  `es_semester` int(11) NOT NULL,
  `es_program` int(11) DEFAULT NULL,
  `es_subject` int(11) NOT NULL,
  `es_mark_type` int(11) NOT NULL COMMENT 'fk tbl_definations',
  `es_operational` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `es_rules` decimal(10,2) NOT NULL,
  `es_createddt` datetime NOT NULL,
  `es_createdby` int(11) NOT NULL,
  PRIMARY KEY (`es_id`),
  KEY `es_id` (`es_id`,`es_semester`,`es_program`,`es_subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_scaling_detail`;
CREATE TABLE `exam_scaling_detail` (
  `esd_id` int(11) NOT NULL AUTO_INCREMENT,
  `es_id` int(11) NOT NULL,
  `esd_mark` decimal(10,2) NOT NULL,
  PRIMARY KEY (`esd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_schedule`;
CREATE TABLE `exam_schedule` (
  `es_id` int(11) NOT NULL AUTO_INCREMENT,
  `es_examsetup_id` int(11) NOT NULL COMMENT 'exam_setup',
  `es_esd_id` int(11) NOT NULL COMMENT 'exam_setup_detail',
  `es_course` int(11) NOT NULL,
  `es_exam_center` int(11) DEFAULT NULL,
  `es_date` date DEFAULT NULL,
  `es_time` time DEFAULT NULL,
  `es_session` int(11) DEFAULT NULL,
  `es_start_time` time DEFAULT NULL,
  `es_end_time` time DEFAULT NULL,
  `es_createddt` datetime DEFAULT NULL,
  `es_publish_start_date` date DEFAULT NULL,
  `es_publish_start_time` time DEFAULT NULL,
  `es_publish_immediately` tinyint(4) NOT NULL DEFAULT '0',
  `es_createdby` int(11) DEFAULT NULL,
  `es_modifydt` datetime NOT NULL,
  `es_modifyby` int(11) NOT NULL,
  `es_active` int(11) NOT NULL,
  `corporate_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`es_id`),
  KEY `es_examsetup_id` (`es_examsetup_id`),
  KEY `es_esd_id` (`es_esd_id`),
  KEY `es_date` (`es_date`),
  KEY `es_exam_center` (`es_exam_center`),
  KEY `es_course` (`es_course`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_scheduletaggingslot`;
CREATE TABLE `exam_scheduletaggingslot` (
  `sts_id` int(11) NOT NULL AUTO_INCREMENT,
  `sts_schedule_id` int(11) NOT NULL COMMENT 'exam_schedule',
  `sts_slot_id` int(11) NOT NULL COMMENT 'exam_slot',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`sts_id`),
  KEY `sts_schedule_id` (`sts_schedule_id`),
  KEY `sts_slot_id` (`sts_slot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_schedule_exception`;
CREATE TABLE `exam_schedule_exception` (
  `ese_id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_id` int(11) NOT NULL,
  `ese_exam_date` date NOT NULL,
  `slot_id` int(11) NOT NULL,
  `schedule_tagging_slot_id` int(11) NOT NULL,
  `ese_remarks` text COLLATE utf8_unicode_ci,
  `ese_createddt` datetime NOT NULL,
  `ese_createdby` int(11) NOT NULL,
  PRIMARY KEY (`ese_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `exam_setup`;
CREATE TABLE `exam_setup` (
  `es_id` int(11) NOT NULL AUTO_INCREMENT,
  `es_idLandscape` int(11) NOT NULL,
  `es_idProgram` int(11) NOT NULL,
  `es_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `es_compulsory` tinyint(4) NOT NULL COMMENT '0:No 1:Yes',
  `es_online` tinyint(4) DEFAULT '0' COMMENT '0: offline, 1: online',
  `es_awardname` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `es_createddt` datetime NOT NULL,
  `es_createdby` int(11) NOT NULL,
  `es_modifydt` datetime DEFAULT NULL,
  `es_modifyby` int(11) DEFAULT NULL,
  `es_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`es_id`),
  KEY `es_idLandscape` (`es_idLandscape`),
  KEY `es_idProgram` (`es_idProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_setup_detail`;
CREATE TABLE `exam_setup_detail` (
  `esd_id` int(11) NOT NULL AUTO_INCREMENT,
  `es_id` int(11) NOT NULL COMMENT 'fk exam_setup',
  `esd_mds_id` int(11) NOT NULL DEFAULT '0' COMMENT 'tbl_markdistribution_setup',
  `esd_pe_id` int(11) NOT NULL COMMENT 'programme_exam',
  `esd_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `esd_type` tinyint(4) NOT NULL,
  `esd_idSubject` int(11) DEFAULT NULL,
  `esd_compulsory` tinyint(4) DEFAULT '1',
  `esd_createddt` datetime NOT NULL,
  `esd_createdby` int(11) NOT NULL,
  `esd_modifydt` datetime DEFAULT NULL,
  `esd_modifyby` int(11) DEFAULT NULL,
  PRIMARY KEY (`esd_id`),
  KEY `es_id` (`es_id`),
  KEY `esd_id` (`esd_id`),
  KEY `esd_pe_id` (`esd_pe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_sitting`;
CREATE TABLE `exam_sitting` (
  `est_id` int(11) NOT NULL AUTO_INCREMENT,
  `est_name` varchar(200) NOT NULL,
  `est_start_date` date NOT NULL,
  `est_end_date` date NOT NULL,
  `est_month` int(11) NOT NULL,
  `est_year` year(4) NOT NULL,
  `est_createddt` datetime NOT NULL,
  `est_createdby` int(11) NOT NULL,
  `est_open` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`est_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `exam_slip_release`;
CREATE TABLE `exam_slip_release` (
  `esr_id` int(11) NOT NULL AUTO_INCREMENT,
  `esr_semester_id` int(11) NOT NULL COMMENT 'fk tbl_semestermaster',
  `esr_assessment_type_id` int(11) NOT NULL COMMENT 'fk tbl_examination_assessment_type',
  `esr_date` date NOT NULL,
  `esr_time` time NOT NULL,
  `esr_status` int(11) NOT NULL DEFAULT '1' COMMENT '0: close, 1: open',
  `esr_last_edit_by` int(11) NOT NULL,
  `esr_last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`esr_id`),
  UNIQUE KEY `esr_semester_id_2` (`esr_semester_id`,`esr_assessment_type_id`),
  KEY `esr_semester_id` (`esr_semester_id`,`esr_assessment_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exam_slot`;
CREATE TABLE `exam_slot` (
  `sl_id` int(11) NOT NULL AUTO_INCREMENT,
  `sl_name` varchar(250) NOT NULL,
  `sl_starttime` time NOT NULL,
  `sl_endtime` time NOT NULL,
  `sl_allow_buffering` int(11) NOT NULL COMMENT '0=no, 1=yes',
  `sl_bufferingvalue` int(11) NOT NULL DEFAULT '0' COMMENT 'value in minutes',
  `sl_status` int(11) NOT NULL COMMENT '0=inactive, 1=active',
  `sl_inhouse` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Not Inhouse, 1=Inhouse',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`sl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `exam_taggingslotcenterroom`;
CREATE TABLE `exam_taggingslotcenterroom` (
  `tsc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsc_slotid` int(11) NOT NULL COMMENT 'exam_slot',
  `tsc_examcenterid` int(11) NOT NULL COMMENT 'tbl_exam_center',
  `tsc_room_id` int(11) DEFAULT NULL COMMENT 'tbl_exam_room',
  `createdby` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`tsc_id`),
  KEY `tsc_slotid` (`tsc_slotid`),
  KEY `tsc_examcenterid` (`tsc_examcenterid`),
  KEY `tsc_room_id` (`tsc_room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exemption_item`;
CREATE TABLE `exemption_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exemption_main_id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1=Award, 2=Enroll',
  `registration_type` int(11) NOT NULL DEFAULT '1' COMMENT '1=Module, 2=Exam',
  `IdProgram` int(11) NOT NULL DEFAULT '0',
  `IdSubject` int(11) NOT NULL DEFAULT '0',
  `pe_id` int(11) NOT NULL DEFAULT '0',
  `IdStudentRegistration` int(11) NOT NULL DEFAULT '0',
  `IdStudentRegSubjects` int(11) NOT NULL DEFAULT '0',
  `examregistration_id` int(11) NOT NULL DEFAULT '0',
  `lms_updated` int(11) NOT NULL DEFAULT '0' COMMENT '0=pending, 1=updated',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `exemption_main`;
CREATE TABLE `exemption_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `std_id` int(11) NOT NULL,
  `IdProgram` int(11) NOT NULL,
  `IdSubject` int(11) DEFAULT NULL,
  `IdStudentRegSubjects` int(11) DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Entry 1=Approved',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `approved_at` datetime DEFAULT NULL,
  `approved_by` int(11) NOT NULL DEFAULT '0',
  `mode` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'N=Normal, CS=Challenge Status, FT=Fast Track',
  `invoice_main_id` int(11) NOT NULL DEFAULT '0',
  `paid` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=false, 1=true',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_discount_rank`;
CREATE TABLE `fee_discount_rank` (
  `fdr_id` int(11) NOT NULL AUTO_INCREMENT,
  `fdr_rank` int(11) NOT NULL,
  `fdr_item_id` int(11) NOT NULL COMMENT 'fk_fee_structure_item',
  `fdr_percentage` decimal(10,2) DEFAULT NULL,
  `fdr_amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`fdr_id`),
  KEY `fdr_rank` (`fdr_rank`),
  KEY `fdr_item_id` (`fdr_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_item`;
CREATE TABLE `fee_item` (
  `fi_id` int(11) NOT NULL AUTO_INCREMENT,
  `fi_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fi_name_bahasa` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fi_name_short` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fi_code` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fi_amount_calculation_type` int(11) NOT NULL COMMENT 'fk_tbl_definationms (Fee Item Calculation Type)',
  `fi_frequency_mode` int(11) NOT NULL COMMENT 'fk_tbl_definationms (Fee Item Frequency Mode)',
  `fi_fc_id` int(11) NOT NULL COMMENT 'FK tbl_fee_category id',
  `fi_ac_id` int(11) NOT NULL COMMENT 'fk tbl_account_code',
  `fi_refundable` tinyint(4) NOT NULL DEFAULT '0',
  `fi_non_invoice` tinyint(4) NOT NULL DEFAULT '0',
  `fi_active` tinyint(4) NOT NULL DEFAULT '1',
  `fi_exclude` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `fi_gst` int(11) DEFAULT NULL,
  `fi_gst_tax` decimal(20,2) DEFAULT NULL,
  `fi_effective_date` date DEFAULT NULL,
  PRIMARY KEY (`fi_id`),
  KEY `fi_amount_calculation_type` (`fi_amount_calculation_type`),
  KEY `fi_frequency_mode` (`fi_frequency_mode`),
  KEY `fi_fc_id` (`fi_fc_id`),
  KEY `fi_ac_id` (`fi_ac_id`),
  KEY `fi_non_invoice` (`fi_non_invoice`),
  KEY `fi_active` (`fi_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_item_account`;
CREATE TABLE `fee_item_account` (
  `fiacc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fiacc_fee_item` int(11) NOT NULL COMMENT 'fk_fee_item',
  `fiacc_program_id` int(11) NOT NULL COMMENT 'fk tbl_program',
  `fiacc_faculty_id` int(11) NOT NULL COMMENT 'fk_collegemaster',
  `fiacc_account` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fiacc_bank` bigint(20) NOT NULL COMMENT 'fk_tbl_definationms',
  `fiacc_creator` bigint(20) NOT NULL COMMENT 'fk_tbl_user',
  `fiacc_create_date` datetime NOT NULL,
  `fiacc_update_by` bigint(20) DEFAULT NULL COMMENT 'fk_tbl_user',
  `fiacc_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`fiacc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_item_convo`;
CREATE TABLE `fee_item_convo` (
  `fv_id` int(20) NOT NULL AUTO_INCREMENT,
  `fv_convo` int(11) NOT NULL COMMENT 'convocation',
  `fv_fi_id` int(11) NOT NULL COMMENT 'fee_item',
  `fv_amount` decimal(20,2) NOT NULL,
  `fv_currency_id` int(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `updUser` int(10) NOT NULL,
  PRIMARY KEY (`fv_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `fee_item_convo_history`;
CREATE TABLE `fee_item_convo_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fv_id` int(20) NOT NULL,
  `fv_convo` int(11) NOT NULL,
  `fv_fi_id` int(11) NOT NULL,
  `fv_amount` decimal(20,2) NOT NULL,
  `fv_currency_id` int(11) NOT NULL,
  `fv_action` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `UpdDate` datetime NOT NULL,
  `updUser` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_item_country`;
CREATE TABLE `fee_item_country` (
  `fic_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fic_fi_id` int(10) unsigned NOT NULL,
  `fic_idCountry` int(10) unsigned NOT NULL,
  `fic_cur_id` smallint(5) unsigned NOT NULL,
  `fic_amount` decimal(11,2) NOT NULL,
  `fic_added_by` int(11) unsigned DEFAULT NULL,
  `fic_added_date` datetime DEFAULT NULL,
  `fic_updated_by` int(11) unsigned DEFAULT NULL,
  `fic_updated_date` datetime DEFAULT NULL,
  `fic_year` int(11) DEFAULT '1',
  PRIMARY KEY (`fic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `fee_item_history`;
CREATE TABLE `fee_item_history` (
  `fi_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fi_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fi_name_bahasa` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fi_name_short` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fi_code` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fi_amount_calculation_type` int(11) NOT NULL COMMENT 'fk_tbl_definationms (Fee Item Calculation Type)',
  `fi_frequency_mode` int(11) NOT NULL COMMENT 'fk_tbl_definationms (Fee Item Frequency Mode)',
  `fi_fc_id` int(11) NOT NULL COMMENT 'FK tbl_fee_category id',
  `fi_ac_id` int(11) NOT NULL COMMENT 'fk tbl_account_code',
  `fi_refundable` tinyint(4) NOT NULL DEFAULT '0',
  `fi_non_invoice` tinyint(4) NOT NULL DEFAULT '0',
  `fi_active` tinyint(4) NOT NULL DEFAULT '1',
  `fi_exclude` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `fi_gst` int(11) DEFAULT NULL,
  `fi_gst_tax` int(11) DEFAULT NULL,
  `fi_effective_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fi_amount_calculation_type` (`fi_amount_calculation_type`),
  KEY `fi_frequency_mode` (`fi_frequency_mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_item_state`;
CREATE TABLE `fee_item_state` (
  `fis_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fic_fi_id` int(10) unsigned NOT NULL,
  `fic_idCountry` int(10) unsigned NOT NULL,
  `fic_idState` int(11) NOT NULL,
  `fic_cur_id` smallint(5) unsigned NOT NULL,
  `fic_amount` decimal(11,2) NOT NULL,
  `fic_added_by` int(11) unsigned DEFAULT NULL,
  `fic_added_date` datetime DEFAULT NULL,
  `fic_updated_by` int(11) unsigned DEFAULT NULL,
  `fic_updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`fis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `fee_quit`;
CREATE TABLE `fee_quit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `intake_id` bigint(20) NOT NULL COMMENT 'fk_tbl_intake',
  `last_effective_date` date NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0: inactive, 1: active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_quit_faculty`;
CREATE TABLE `fee_quit_faculty` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fq_id` bigint(20) NOT NULL COMMENT 'fk_fee_quit',
  `college_id` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fq_id` (`fq_id`,`college_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure`;
CREATE TABLE `fee_structure` (
  `fs_id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fs_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `fs_effective_date` date NOT NULL,
  `fs_intake_start` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `fs_intake_end` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `fs_semester_start` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `fs_student_category` int(11) NOT NULL COMMENT 'fl_tbl_definationms (Fee Structure Student Category)',
  `fs_cur_id` int(11) NOT NULL COMMENT 'fk tbl_currency',
  `fs_ac_id` bigint(20) NOT NULL,
  `fs_estimated_total_fee` decimal(20,2) DEFAULT NULL,
  `fs_cp` int(10) unsigned DEFAULT NULL,
  `fs_prepayment_ac` int(11) DEFAULT NULL,
  `fs_status` int(11) NOT NULL COMMENT '1=active, 0=draft, 2=inactive',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `upd_date` datetime NOT NULL,
  `upd_by` int(11) NOT NULL,
  `fs_IdProgram` int(11) DEFAULT NULL,
  PRIMARY KEY (`fs_id`),
  KEY `fs_intake_start` (`fs_intake_start`),
  KEY `fs_intake_end` (`fs_intake_end`),
  KEY `fs_student_category` (`fs_student_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_item`;
CREATE TABLE `fee_structure_item` (
  `fsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsi_structure_id` int(11) NOT NULL COMMENT 'fk_fee_structure',
  `fsi_item_id` int(11) NOT NULL COMMENT 'fk_fee_item',
  `fsi_amount` decimal(11,2) NOT NULL,
  `fsi_cur_id` bigint(20) NOT NULL COMMENT 'FK tbl_currency',
  `fsi_registration_item_id` int(11) NOT NULL COMMENT 'Registration item',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`fsi_id`),
  KEY `fsi_cur_id` (`fsi_cur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_item_level`;
CREATE TABLE `fee_structure_item_level` (
  `fsis_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsis_item_id` int(11) NOT NULL COMMENT 'fk_fee_structure_item',
  `fsis_level` int(11) NOT NULL,
  PRIMARY KEY (`fsis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_item_semester`;
CREATE TABLE `fee_structure_item_semester` (
  `fsis_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsis_item_id` int(11) NOT NULL COMMENT 'fk_fee_structure_item',
  `fsis_semester` int(11) NOT NULL,
  PRIMARY KEY (`fsis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_item_subject`;
CREATE TABLE `fee_structure_item_subject` (
  `fsisub_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsisub_fsi_id` int(11) NOT NULL COMMENT 'fk fee_structure_item',
  `fsisub_subject_id` bigint(20) NOT NULL COMMENT 'fk tbl_subjectmaster',
  PRIMARY KEY (`fsisub_id`),
  KEY `fsisub_fsi_id` (`fsisub_fsi_id`),
  KEY `fsisub_subject_id` (`fsisub_subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_member`;
CREATE TABLE `fee_structure_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_id` int(11) NOT NULL COMMENT 'fee_structure',
  `member_id` int(11) NOT NULL COMMENT 'tbl_member',
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_optional_item`;
CREATE TABLE `fee_structure_optional_item` (
  `fsoi_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fsoi_structure_id` int(11) unsigned NOT NULL,
  `fsoi_detail_id` int(11) unsigned NOT NULL,
  `fsoi_fee_id` int(11) unsigned NOT NULL,
  `fsoi_item_id` int(11) unsigned NOT NULL,
  `fsoi_currency_id` smallint(3) unsigned NOT NULL,
  `fsoi_amount` decimal(11,2) unsigned NOT NULL,
  `added_by` int(11) unsigned NOT NULL,
  `added_date` datetime NOT NULL,
  PRIMARY KEY (`fsoi_id`),
  KEY `fsoi_structure_id` (`fsoi_structure_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_other`;
CREATE TABLE `fee_structure_other` (
  `fso_id` int(11) NOT NULL AUTO_INCREMENT,
  `fso_program_id` int(11) NOT NULL,
  `fso_activity_id` int(11) NOT NULL,
  `fso_trigger` int(11) NOT NULL,
  `fso_proforma` int(11) NOT NULL,
  `fso_fee_id` int(11) NOT NULL,
  `fso_status` int(11) NOT NULL,
  `fso_created_by` int(11) NOT NULL,
  `fso_created_date` datetime NOT NULL,
  PRIMARY KEY (`fso_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_plan`;
CREATE TABLE `fee_structure_plan` (
  `fsp_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsp_structure_id` int(11) NOT NULL COMMENT 'fk_fee_structure',
  `fsp_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fsp_billing_no` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fsp_bil_installment` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`fsp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='cicilan table';


DROP TABLE IF EXISTS `fee_structure_plan_detl`;
CREATE TABLE `fee_structure_plan_detl` (
  `fspd_id` int(11) NOT NULL AUTO_INCREMENT,
  `fspd_plan_id` int(11) NOT NULL COMMENT 'fk_fee_structure_plan',
  `fspd_installment_no` int(11) NOT NULL,
  `fspd_item_id` int(11) NOT NULL COMMENT 'fk_fee_structure_item',
  `fspd_percentage` decimal(10,2) DEFAULT NULL,
  `fspd_amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`fspd_id`),
  UNIQUE KEY `fspd_plan_id` (`fspd_plan_id`,`fspd_installment_no`,`fspd_item_id`),
  KEY `fspd_plan_id_2` (`fspd_plan_id`),
  KEY `fspd_item_id` (`fspd_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_program`;
CREATE TABLE `fee_structure_program` (
  `fsp_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsp_fs_id` int(11) NOT NULL COMMENT 'fk_fee_structure',
  `fsp_program_id` int(11) NOT NULL,
  `fsp_first_sem_sks` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `fsp_idProgramScheme` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `upd_date` datetime NOT NULL,
  `upd_by` int(11) NOT NULL,
  PRIMARY KEY (`fsp_id`),
  KEY `fsp_program_id` (`fsp_program_id`),
  KEY `fsp_fs_id` (`fsp_fs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_program_course`;
CREATE TABLE `fee_structure_program_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_id` int(11) NOT NULL COMMENT 'fk fee_structure',
  `fsp_id` int(11) NOT NULL COMMENT 'fk fee_structure_program',
  `course_id` int(11) NOT NULL DEFAULT '0',
  `exam_id` int(11) NOT NULL DEFAULT '0' COMMENT 'programme_exam',
  `member_id` int(11) DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1=course, 2=exam',
  `cur_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fee_structure_program_item`;
CREATE TABLE `fee_structure_program_item` (
  `fspi_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fspi_type` enum('registration','application') COLLATE utf8mb4_unicode_ci NOT NULL,
  `fspi_fs_id` int(11) unsigned NOT NULL,
  `fspi_program_id` int(11) unsigned NOT NULL,
  `fspi_fee_id` int(11) unsigned NOT NULL,
  `fspi_currency_id` smallint(3) unsigned NOT NULL,
  `fspi_amount` decimal(11,2) NOT NULL,
  `fspi_document_type` int(11) unsigned NOT NULL,
  `added_by` int(11) unsigned NOT NULL,
  `added_date` datetime NOT NULL,
  PRIMARY KEY (`fspi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `finance_configuration`;
CREATE TABLE `finance_configuration` (
  `fin_id` int(11) NOT NULL AUTO_INCREMENT,
  `os_balance` decimal(20,2) NOT NULL,
  `os_effective_date` date DEFAULT NULL,
  `gst` tinyint(4) NOT NULL DEFAULT '0',
  `gst_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `gst_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fin_updatedt` datetime NOT NULL,
  `fin_updateby` int(11) NOT NULL,
  PRIMARY KEY (`fin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `finance_configuration_history`;
CREATE TABLE `finance_configuration_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fin_id` int(11) NOT NULL,
  `os_balance` decimal(20,2) NOT NULL,
  `os_effective_date` date DEFAULT NULL,
  `gst` tinyint(4) NOT NULL DEFAULT '0',
  `gst_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `gst_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fin_updatedt` datetime NOT NULL,
  `fin_updateby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `finance_erp`;
CREATE TABLE `finance_erp` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `filepath` mediumtext COLLATE utf8mb4_unicode_ci,
  `filename` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `finance_service`;
CREATE TABLE `finance_service` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_cron` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '1=applicant, 2=studnet',
  `semester_id` int(11) NOT NULL DEFAULT '0',
  `program_id` int(11) NOT NULL DEFAULT '0',
  `program_scheme_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `intake_id` int(11) NOT NULL DEFAULT '0',
  `profile_status_id` int(11) NOT NULL DEFAULT '0',
  `student_id` mediumtext COLLATE utf8mb4_unicode_ci COMMENT 'student id in array',
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `action` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filepath` mediumtext COLLATE utf8mb4_unicode_ci,
  `filename` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `executed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `executed_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fpx_order`;
CREATE TABLE `fpx_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ol_id` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ENTRY','PENDING','SUCCESS','FAIL') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ENTRY',
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `registration_item_id` int(11) NOT NULL DEFAULT '889',
  `program_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `fpx_buyerAccNo` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerBankBranch` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerBankId` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerEmail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerIban` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerId` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerName` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_checkSum` text COLLATE utf8mb4_unicode_ci,
  `fpx_makerName` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_msgToken` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_msgType` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_productDesc` text COLLATE utf8mb4_unicode_ci,
  `fpx_sellerBankCode` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_sellerExId` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_sellerExOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_sellerOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_sellerTxnTime` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_txnAmount` decimal(20,2) DEFAULT NULL,
  `fpx_txnCurrency` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_version` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_sellerId` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_pay` int(2) NOT NULL DEFAULT '0',
  `fpx_inprogresspay` int(11) NOT NULL DEFAULT '0',
  `fpx_inprogresspay_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fpx_order_detail`;
CREATE TABLE `fpx_order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `proforma_invoice_id` int(11) NOT NULL DEFAULT '0',
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fpx_order_history`;
CREATE TABLE `fpx_order_history` (
  `his_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(11) unsigned NOT NULL,
  `ol_id` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_no` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ENTRY','PENDING','SUCCESS','FAIL') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ENTRY',
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `registration_item_id` int(11) NOT NULL DEFAULT '889',
  `program_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `fpx_transaction`;
CREATE TABLE `fpx_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `fpx_creditAuthNo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mandatory if authcode 00',
  `fpx_msgToken` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_sellerOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_creditAuthCode` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_fpxTxnTime` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_makerName` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'optional',
  `fpx_debitAuthNo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mandatory if authcode 00',
  `fpx_txnAmount` decimal(16,2) NOT NULL,
  `fpx_sellerExId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerBankId` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_msgType` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_checkSum` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_sellerExOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerName` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fpx_sellerTxnTime` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_sellerId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerIban` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_debitAuthCode` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerId` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerBankBranch` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_fpxTxnId` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL COMMENT 'server timestamp',
  `updated_date` datetime DEFAULT NULL COMMENT 'requery',
  `transaction_date` datetime NOT NULL COMMENT 'from fpxTxnTime',
  `manual` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `rcp_id` int(11) DEFAULT NULL COMMENT 'FK receipt',
  `rcp_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='schema according to merchant spec 2.1';


DROP TABLE IF EXISTS `fpx_transaction_history`;
CREATE TABLE `fpx_transaction_history` (
  `ids` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `fpx_sellerOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_debitAuthCode` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_creditAuthNo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mandatory if authcode 00',
  `fpx_msgToken` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_creditAuthCode` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_fpxTxnTime` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_makerName` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'optional',
  `fpx_debitAuthNo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mandatory if authcode 00',
  `fpx_txnAmount` decimal(16,2) NOT NULL,
  `fpx_sellerExId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerBankId` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_msgType` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_checkSum` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_sellerExOrderNo` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerName` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fpx_sellerTxnTime` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_sellerId` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerIban` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_buyerId` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_buyerBankBranch` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fpx_fpxTxnId` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL COMMENT 'server timestamp',
  `updated_date` datetime DEFAULT NULL COMMENT 'requery',
  `transaction_date` datetime NOT NULL COMMENT 'from fpxTxnTime',
  `manual` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `rcp_id` int(11) DEFAULT NULL COMMENT 'FK receipt',
  `rcp_date` datetime DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `as_fid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx_txnCurrency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='schema according to merchant spec 2.1';


DROP TABLE IF EXISTS `general_documents`;
CREATE TABLE `general_documents` (
  `gd_id` int(11) NOT NULL AUTO_INCREMENT,
  `gd_table_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gd_table_id` int(11) NOT NULL,
  `gd_type` int(11) NOT NULL,
  `gd_filepath` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gd_filename` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gd_ori_filename` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gd_filetype` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gd_createddt` datetime NOT NULL,
  `gd_createdby` int(11) NOT NULL,
  PRIMARY KEY (`gd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `graduate`;
CREATE TABLE `graduate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idStudentRegistration` bigint(20) NOT NULL,
  `CGPA` decimal(5,2) NOT NULL,
  `NIRL` varchar(17) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NoSeriIjazah` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NoSeri` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TotalCreditHours` decimal(6,2) NOT NULL,
  `LengthOfStudy` smallint(6) NOT NULL,
  `Predicate` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add_date` datetime NOT NULL,
  `add_by` bigint(20) NOT NULL,
  `dean_approval_date` datetime DEFAULT NULL,
  `dean_approval_skr` int(11) DEFAULT NULL,
  `rector_approval_date` datetime DEFAULT NULL,
  `rector_approval_skr` int(11) DEFAULT NULL,
  `ijazah` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transcript` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idStudentRegistration` (`idStudentRegistration`,`add_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='list of graduated student';


DROP TABLE IF EXISTS `graduated_student`;
CREATE TABLE `graduated_student` (
  `idGraduate` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(57) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `StudentId` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `PlaceOfBirth` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CGPA` decimal(4,2) DEFAULT NULL,
  `FinalAsigment` varchar(652) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Rector_decree` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `COL 8` varchar(28) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `YudisiumDate` date DEFAULT NULL,
  `TotalCreditHour` decimal(6,2) DEFAULT NULL,
  `NIRL` varchar(17) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Decree_Yudisium` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NoSeri` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Study_semester` decimal(5,2) DEFAULT NULL,
  `FinalAssignSemester` decimal(5,2) DEFAULT NULL,
  `RectorDecreeDate` date DEFAULT NULL,
  `GraduateYear` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GraduateSem` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateOdConvo` date DEFAULT NULL,
  `ProgramCode` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MajoringCode` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Strata` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Intake` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Transfer` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dean_approval_skr` int(11) NOT NULL,
  `rector_approval_skr` int(11) NOT NULL,
  `IdSemesterMain` int(11) NOT NULL,
  `idstudentregistration` int(11) NOT NULL,
  PRIMARY KEY (`idGraduate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `graduation`;
CREATE TABLE `graduation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idStudentRegistration` bigint(20) NOT NULL,
  `add_date` datetime NOT NULL,
  `add_by` bigint(20) NOT NULL,
  `dean_approval_date` datetime DEFAULT NULL,
  `dean_approval_skr` int(11) DEFAULT NULL,
  `rector_approval_date` datetime DEFAULT NULL,
  `rector_approval_skr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idStudentRegistration` (`idStudentRegistration`,`add_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='list of graduated student';


DROP TABLE IF EXISTS `graduation_skr`;
CREATE TABLE `graduation_skr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `skr` varchar(200) NOT NULL,
  `period` tinyint(4) NOT NULL,
  `date_of_skr` date NOT NULL,
  `type` enum('pregraduation','graduation') NOT NULL,
  `IdSemesterMain` int(11) NOT NULL,
  `add_by` bigint(20) NOT NULL,
  `add_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `add_by` (`add_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `hostel_fee_invoices`;
CREATE TABLE `hostel_fee_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `IdStudentRegistration` int(11) NOT NULL,
  `IdHostelregistration` int(11) NOT NULL,
  `monthyear` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `icourse`;
CREATE TABLE `icourse` (
  `coursecode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arabic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eng` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credithour` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `idepartment`;
CREATE TABLE `idepartment` (
  `id` int(11) NOT NULL,
  `arabic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eng` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idfaculty` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `ifaculty`;
CREATE TABLE `ifaculty` (
  `id` int(11) NOT NULL,
  `arabic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eng` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `ilandscape`;
CREATE TABLE `ilandscape` (
  `progcode` int(11) NOT NULL,
  `progname_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `progname_e` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `sem` int(11) NOT NULL,
  `semlandscape` int(11) NOT NULL,
  `coursecode` int(11) NOT NULL,
  `coursename_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coursename_e` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credithour` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `ilms_user`;
CREATE TABLE `ilms_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_record_registration_date` datetime DEFAULT NULL,
  `training_record_completion_date` datetime DEFAULT NULL,
  `correction_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_record` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correction` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci,
  `remark_takafuloperator` text COLLATE utf8mb4_unicode_ci,
  `remark_studentregistration` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `ilms_user_bak`;
CREATE TABLE `ilms_user_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_record_registration_date` datetime DEFAULT NULL,
  `training_record_completion_date` datetime DEFAULT NULL,
  `correction_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_record` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correction` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci,
  `remark_takafuloperator` text COLLATE utf8mb4_unicode_ci,
  `remark_studentregistration` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `intake_registration_location`;
CREATE TABLE `intake_registration_location` (
  `rl_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdIntake` int(11) NOT NULL,
  `rl_branch` int(11) NOT NULL COMMENT 'fk tbl_branchofficevenue',
  `rl_date` date NOT NULL,
  `rl_time` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rl_address` mediumtext COLLATE utf8mb4_unicode_ci,
  `rl_createddt` datetime NOT NULL,
  `rl_createdby` int(11) NOT NULL,
  PRIMARY KEY (`rl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `invoice_aging`;
CREATE TABLE `invoice_aging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_no` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `invoice_delete_history`;
CREATE TABLE `invoice_delete_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_main_id` int(11) NOT NULL,
  `bill_number` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appl_id` bigint(20) DEFAULT NULL,
  `no_fomulir` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `academic_year` int(11) NOT NULL COMMENT 'fk  academic_year',
  `semester` bigint(20) DEFAULT NULL COMMENT 'fk tbl_semester',
  `bill_amount` decimal(20,2) NOT NULL,
  `bill_paid` decimal(20,2) DEFAULT NULL,
  `bill_balance` decimal(20,2) DEFAULT NULL,
  `bill_description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `college_id` bigint(20) NOT NULL,
  `program_code` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `creator` bigint(20) NOT NULL COMMENT 'fk tbl_user',
  `fs_id` bigint(20) NOT NULL COMMENT 'fk fee_structure',
  `fsp_id` bigint(20) NOT NULL COMMENT 'fk fee_structure_plan',
  `cn_amount` decimal(20,2) DEFAULT '0.00',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A:active X: cancel',
  `cancel_by` bigint(20) DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `delete_by` bigint(20) NOT NULL,
  `delete_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bill_number` (`bill_number`),
  KEY `no_fomulir` (`no_fomulir`),
  KEY `appl_id` (`appl_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `invoice_detail`;
CREATE TABLE `invoice_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_main_id` bigint(20) NOT NULL,
  `invoice_student_id` int(11) NOT NULL DEFAULT '0',
  `fi_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk fee_item',
  `fee_item_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cur_id` bigint(20) NOT NULL COMMENT 'FK tbl_currency',
  `amount` decimal(20,2) NOT NULL,
  `amount_gst` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'amount before gst',
  `amount_default_currency` decimal(20,2) NOT NULL DEFAULT '0.00',
  `paid` decimal(20,2) NOT NULL DEFAULT '0.00',
  `balance` decimal(20,2) NOT NULL,
  `exchange_rate` int(11) NOT NULL DEFAULT '0' COMMENT 'PK currency_rate',
  `cn_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `dn_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `sp_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `sp_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pk sponsor_invoice_Detail',
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cur_id` (`cur_id`),
  KEY `invoice_main_id` (`invoice_main_id`),
  KEY `exchange_rate` (`exchange_rate`),
  KEY `fi_id` (`fi_id`),
  KEY `invoice_student_id` (`invoice_student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `invoice_main`;
CREATE TABLE `invoice_main` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bill_number` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_ref_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_id` int(20) NOT NULL DEFAULT '0',
  `corporate_id` int(20) DEFAULT '0' COMMENT 'fk tbl_takafuloperator',
  `IdStudentRegistration` bigint(20) DEFAULT '0' COMMENT 'fk tbl_studentregistration',
  `batchId` bigint(20) DEFAULT NULL,
  `ber_id` int(11) DEFAULT '0' COMMENT 'fk batch_exam_registration',
  `er_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk exam_registration',
  `mr_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk membership_registration',
  `order_id` int(11) DEFAULT NULL COMMENT 'ibfim-lms.order',
  `category_id` int(11) DEFAULT '1' COMMENT 'tbl_category',
  `totalStudent` int(11) DEFAULT '1',
  `bill_amount` decimal(20,2) NOT NULL COMMENT 'nett = bill_amount_gst + gst',
  `bill_amount_gst` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'amount before gst',
  `gst_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'gst amount',
  `service_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'amount charged for service fees. eg: migs, fpx, sst, etc',
  `bill_paid` decimal(20,2) NOT NULL DEFAULT '0.00',
  `bill_balance` decimal(20,2) NOT NULL DEFAULT '0.00',
  `bill_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program_id` bigint(20) NOT NULL DEFAULT '0',
  `date_create` datetime NOT NULL,
  `invoice_date` date NOT NULL,
  `creator` bigint(20) NOT NULL DEFAULT '665' COMMENT 'fk tbl_user',
  `from` int(11) NOT NULL DEFAULT '0' COMMENT '0=sms, 1=lms, 2=corporate',
  `fs_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'fk fee_structure',
  `fsp_id` bigint(20) DEFAULT NULL COMMENT 'fk fee_structure_plan',
  `cn_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `dn_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `sp_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `sp_id` int(11) NOT NULL DEFAULT '0' COMMENT 'pk sponsor_invoice_main',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'A:active X: cancel',
  `approve_date` datetime DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL,
  `cancel_by` bigint(20) DEFAULT NULL,
  `cancel_from` tinyint(4) DEFAULT NULL COMMENT '0=sms,1=lms,2=corp',
  `cancel_date` datetime DEFAULT NULL,
  `currency_id` bigint(20) NOT NULL DEFAULT '1',
  `proforma_invoice_id` bigint(20) DEFAULT NULL COMMENT 'FK proforma_invoice_main',
  `upd_by` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `exchange_rate` int(11) DEFAULT '0' COMMENT 'PK currencry_rate',
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `invoice_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_item` int(11) DEFAULT NULL COMMENT 'tbl_definationms 160',
  `not_include` int(11) NOT NULL DEFAULT '0' COMMENT '1=no,0=yes',
  `adv_transfer_id` int(11) NOT NULL DEFAULT '0' COMMENT 'PK advance_payment',
  `payment_mode` int(11) NOT NULL DEFAULT '0' COMMENT 'payment_mode',
  `payment_mode_date` datetime DEFAULT NULL,
  `manual` tinyint(4) DEFAULT '0' COMMENT '0=auto, 1=manual',
  `coupon_id` int(11) DEFAULT '0' COMMENT 'tbl_coupon',
  PRIMARY KEY (`id`),
  UNIQUE KEY `bill_number` (`bill_number`),
  KEY `status` (`status`),
  KEY `program_id` (`program_id`),
  KEY `fs_id` (`fs_id`),
  KEY `proforma_invoice_id` (`proforma_invoice_id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`),
  KEY `currency_id` (`currency_id`),
  KEY `trans_id` (`corporate_id`),
  KEY `exchange_rate` (`exchange_rate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `invoice_student`;
CREATE TABLE `invoice_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL COMMENT 'fk invoice_main',
  `IdStudentRegistration` int(11) NOT NULL,
  `examregistration_main_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_registration_main',
  `cur_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `paid` decimal(20,2) NOT NULL,
  `balance` decimal(20,2) NOT NULL,
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `invoice_subject`;
CREATE TABLE `invoice_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_main_id` bigint(20) NOT NULL COMMENT 'FK invoice_main',
  `invoice_detail_id` bigint(20) NOT NULL COMMENT 'FK invoice_detail',
  `invoice_student_id` bigint(20) NOT NULL,
  `subject_id` bigint(20) NOT NULL COMMENT 'FK tbl_subjectmaster',
  `cur_id` bigint(20) NOT NULL COMMENT 'FK tbl_currency',
  `amount` decimal(20,2) NOT NULL,
  `amount_default_currency` decimal(20,2) NOT NULL DEFAULT '0.00',
  `paid` decimal(20,2) NOT NULL,
  `balance` decimal(20,2) NOT NULL,
  `rcp_id` bigint(20) DEFAULT NULL,
  `rcp_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_rate` int(11) NOT NULL DEFAULT '0' COMMENT 'PK currency_rate',
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proforma_invoice_main_id` (`invoice_main_id`,`invoice_detail_id`,`subject_id`),
  KEY `cur_id` (`cur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci,
  `createdby` int(11) NOT NULL,
  `createddt` datetime NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `locale` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `language_old`;
CREATE TABLE `language_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci,
  `createdby` int(11) NOT NULL,
  `createddt` datetime NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `locale` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `log_api`;
CREATE TABLE `log_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `function` varchar(250) DEFAULT NULL,
  `message` text,
  `line` text,
  `file` text,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership_application`;
CREATE TABLE `membership_application` (
  `ma_id` int(11) NOT NULL AUTO_INCREMENT,
  `ma_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:Upgrade 2:Reinstate',
  `sp_id` bigint(20) NOT NULL,
  `ma_createddt` datetime DEFAULT NULL,
  `ma_createdby` int(11) DEFAULT NULL,
  `ma_modifieddt` datetime DEFAULT NULL,
  `ma_modifiedby` int(11) DEFAULT NULL,
  `ma_submit_date` datetime DEFAULT NULL,
  `ma_approve_date` datetime DEFAULT NULL,
  `ma_approve_by` int(11) NOT NULL,
  `ma_reject_date` datetime DEFAULT NULL,
  `ma_reject_by` int(11) DEFAULT NULL,
  `ma_activation_date` datetime DEFAULT NULL,
  `ma_expiry_date` datetime DEFAULT NULL,
  `ma_status` int(11) NOT NULL DEFAULT '0' COMMENT 'tbl_definations',
  `ma_payment_status` int(11) NOT NULL DEFAULT '0',
  `mf_id` int(11) NOT NULL DEFAULT '0' COMMENT 'membership_fee',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `membershipId` int(11) DEFAULT NULL COMMENT 'fk tbl_membership',
  `ma_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: Complete 2:Incomplete',
  `ma_remarks` text COLLATE utf8mb4_unicode_ci,
  `ma_prev_mrid` int(11) NOT NULL DEFAULT '0' COMMENT 'previous mr id',
  `ma_application_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:in progress 1:completed',
  PRIMARY KEY (`ma_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `membership_fee`;
CREATE TABLE `membership_fee` (
  `mf_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL COMMENT 'fee_item',
  `member_id` int(11) NOT NULL COMMENT 'tbl_membership',
  `mf_effective_date` date NOT NULL,
  `mf_start_date` date DEFAULT NULL COMMENT 'year start',
  `mf_end_date` date DEFAULT NULL COMMENT 'year end',
  `mf_amount` double NOT NULL,
  `mf_createddt` datetime NOT NULL,
  `mf_createdby` int(11) NOT NULL,
  PRIMARY KEY (`mf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership_registration`;
CREATE TABLE `membership_registration` (
  `mr_id` int(11) NOT NULL AUTO_INCREMENT,
  `mr_transaction_id` bigint(20) NOT NULL,
  `mr_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: New Application 1: Renewal',
  `mr_sp_id` bigint(20) NOT NULL,
  `mr_registrationId` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `mr_bmr_id` int(11) NOT NULL DEFAULT '0' COMMENT 'batch membership registration id',
  `mr_student_id` int(11) NOT NULL COMMENT 'tbl_studentregistration',
  `mr_activation_date` datetime DEFAULT NULL,
  `mr_expiry_date` datetime DEFAULT NULL,
  `mr_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-entry',
  `mr_approve_date` datetime DEFAULT NULL,
  `mr_approve_by` int(11) NOT NULL DEFAULT '0',
  `mr_reject_date` datetime DEFAULT NULL,
  `mr_reject_by` int(11) DEFAULT NULL,
  `mr_payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:pending 1:paid:3:payment cancelled (tbe)',
  `mr_exemption_status` int(11) DEFAULT NULL,
  `mr_exemption_type` int(11) DEFAULT NULL,
  `mr_createddt` datetime DEFAULT NULL,
  `mr_createdby` int(11) DEFAULT NULL,
  `mr_modifieddt` datetime DEFAULT NULL,
  `mr_modifiedby` int(11) DEFAULT NULL,
  `mr_cancel_apply_date` datetime DEFAULT NULL,
  `mr_cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `mr_cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `mr_cancel_approve_date` datetime DEFAULT NULL,
  `mr_cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `mr_fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee structure',
  `mf_id` int(11) NOT NULL DEFAULT '0' COMMENT 'membership_fee',
  `mr_order_id` int(11) NOT NULL DEFAULT '0',
  `mr_membershipId` int(11) DEFAULT NULL COMMENT 'fk tbl_membership',
  `mr_submit_date` datetime DEFAULT NULL,
  `mr_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: Complete 2:Incomplete',
  `mr_remarks` text COLLATE utf8mb4_unicode_ci,
  `mr_prev_mrid` int(11) NOT NULL DEFAULT '0' COMMENT 'previous mr id',
  PRIMARY KEY (`mr_id`),
  KEY `student_id` (`mr_student_id`),
  KEY `IdProgram` (`mr_membershipId`),
  KEY `bmr_id` (`mr_bmr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `membership_registration_checklist`;
CREATE TABLE `membership_registration_checklist` (
  `mrc_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_id` int(11) NOT NULL COMMENT 'fk tbl_membership',
  `mrc_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Membership new application 2: Membership Upgrade',
  `mrc_name` tinytext NOT NULL,
  `mrc_status` int(11) NOT NULL,
  `mrc_remarks` text NOT NULL,
  `mrc_downloadable` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  `mrc_createddt` datetime NOT NULL,
  `mrc_createdby` int(11) NOT NULL,
  PRIMARY KEY (`mrc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership_registration_documents`;
CREATE TABLE `membership_registration_documents` (
  `mrd_id` int(11) NOT NULL AUTO_INCREMENT,
  `mr_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk membership_registration',
  `ma_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk membership_application',
  `mrc_id` int(11) NOT NULL COMMENT 'fk membership_registration_checklist',
  `mrd_name` varchar(250) NOT NULL,
  `mrd_application_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: new application 2: upgrade 3 :reinstate',
  `mrd_createddt` datetime NOT NULL,
  `mrd_createdby` int(11) NOT NULL,
  PRIMARY KEY (`mrd_id`),
  KEY `mr_id` (`mr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership_registration_history`;
CREATE TABLE `membership_registration_history` (
  `mrh_id` int(11) NOT NULL AUTO_INCREMENT,
  `mr_id` int(11) NOT NULL,
  `mr_transaction_id` bigint(20) NOT NULL,
  `mr_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: New Application 1: Renewal',
  `mr_sp_id` bigint(20) NOT NULL,
  `mr_registrationId` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `mr_bmr_id` int(11) NOT NULL DEFAULT '0' COMMENT 'batch membership registration id',
  `mr_student_id` int(11) NOT NULL COMMENT 'tbl_studentregistration',
  `mr_activation_date` datetime DEFAULT NULL,
  `mr_expiry_date` datetime DEFAULT NULL,
  `mr_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-entry',
  `mr_approve_date` datetime NOT NULL,
  `mr_approve_by` int(11) NOT NULL,
  `mr_reject_date` datetime DEFAULT NULL,
  `mr_reject_by` int(11) DEFAULT NULL,
  `mr_payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:pending 1:paid:3:payment cancelled (tbe)',
  `mr_exemption_status` int(11) DEFAULT NULL,
  `mr_exemption_type` int(11) DEFAULT NULL,
  `mr_createddt` datetime DEFAULT NULL,
  `mr_createdby` int(11) DEFAULT NULL,
  `mr_modifieddt` datetime DEFAULT NULL,
  `mr_modifiedby` int(11) DEFAULT NULL,
  `mr_cancel_apply_date` datetime DEFAULT NULL,
  `mr_cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `mr_cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `mr_cancel_approve_date` datetime DEFAULT NULL,
  `mr_cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `mr_fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee structure',
  `mf_id` int(11) NOT NULL DEFAULT '0' COMMENT 'membership_fee',
  `mr_order_id` int(11) NOT NULL DEFAULT '0',
  `mr_membershipId` int(11) DEFAULT NULL COMMENT 'fk tbl_membership',
  `mr_submit_date` datetime DEFAULT NULL,
  `mr_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: Complete 2:Incomplete',
  `mr_remarks` text COLLATE utf8mb4_unicode_ci,
  `mr_prev_mrid` int(11) NOT NULL DEFAULT '0' COMMENT 'previous mr id',
  `mrh_amount_fee` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'total fee',
  `mrh_createddt` datetime DEFAULT NULL,
  `mrh_createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`mrh_id`),
  KEY `student_id` (`mr_student_id`),
  KEY `IdProgram` (`mr_membershipId`),
  KEY `bmr_id` (`mr_bmr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `from_id` int(11) unsigned NOT NULL,
  `to_id` int(11) unsigned NOT NULL,
  `folder_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `haveunread` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `read_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `from_id` (`from_id`),
  KEY `to_id` (`to_id`),
  KEY `haveread` (`haveunread`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `mg_log`;
CREATE TABLE `mg_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) DEFAULT NULL COMMENT 'exam_schedule',
  `ExamDate` date DEFAULT NULL,
  `pe_id` int(11) NOT NULL COMMENT 'programme_exam',
  `ExamName` varchar(200) NOT NULL COMMENT 'tbe.programname',
  `examschedule_id` int(11) DEFAULT '0',
  `examtaggingslot_id` int(11) DEFAULT '0',
  `examsetup_id` int(11) DEFAULT '0',
  `examsetupdetail_id` int(11) DEFAULT '0',
  `total_migrate` int(11) DEFAULT '0',
  `total_all` int(11) DEFAULT '0',
  `total_failed` int(11) DEFAULT '0',
  `total_incomplete` int(11) DEFAULT '0' COMMENT 'incomplete data mapping',
  `total_unmapped` int(11) DEFAULT '0',
  `total_exam` int(11) DEFAULT '0' COMMENT 'exam_registration + result',
  `remarks` text,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scheduleId` (`scheduleId`),
  KEY `ExamDate` (`ExamDate`),
  KEY `pe_id` (`pe_id`),
  KEY `examschedule_id` (`examschedule_id`),
  KEY `examtaggingslot_id` (`examtaggingslot_id`),
  KEY `examsetup_id` (`examsetup_id`),
  KEY `examsetupdetail_id` (`examsetupdetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='log tbe migration log';


DROP TABLE IF EXISTS `mg_tbe`;
CREATE TABLE `mg_tbe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `function` varchar(150) NOT NULL,
  `total_row` int(11) DEFAULT NULL,
  `total_update` int(11) DEFAULT NULL,
  `executed_date` datetime DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mifp`;
CREATE TABLE `mifp` (
  `semester` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `studentId` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark` decimal(4,2) DEFAULT NULL,
  `approval_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attendance_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdProgram` int(11) DEFAULT NULL,
  `migrate_status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_message` longtext COLLATE utf8mb4_unicode_ci,
  `migrate_date` datetime DEFAULT NULL,
  `migrate_by` int(11) DEFAULT NULL,
  `IdSemester` int(11) DEFAULT NULL,
  `IdSubject` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrate_applicant_files`;
CREATE TABLE `migrate_applicant_files` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filesize` bigint(11) unsigned NOT NULL,
  `applicant_id` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assigned_to` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrate_applicant_mapping`;
CREATE TABLE `migrate_applicant_mapping` (
  `id` int(11) NOT NULL,
  `metric_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicant_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program_id` int(11) NOT NULL,
  `program_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrate_invoice_student`;
CREATE TABLE `migrate_invoice_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registrationId` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullname` varchar(39) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester` varchar(23) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_date` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_no` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `COL 7` varchar(42) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fi_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_myr` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_usd` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrate_payment`;
CREATE TABLE `migrate_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `a` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `b` varchar(19) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c` varchar(47) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `d` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rcp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrate_program`;
CREATE TABLE `migrate_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program_name_my` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specialization` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scheme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module_code_old` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module_code_new` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date1` date DEFAULT NULL,
  `end_date1` date DEFAULT NULL,
  `duration1` int(11) DEFAULT NULL,
  `start_date2` date DEFAULT NULL,
  `end_date2` date DEFAULT NULL,
  `duration2` int(11) DEFAULT NULL,
  `start_date3` date DEFAULT NULL,
  `end_date3` date DEFAULT NULL,
  `duration3` int(11) DEFAULT NULL,
  `local_currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `local_normal_fee` int(11) DEFAULT NULL,
  `local_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `local_stfi_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `local_package_deal` int(11) DEFAULT NULL,
  `local_discount_group` int(11) DEFAULT NULL,
  `local_discount_stfi_member` int(11) DEFAULT NULL,
  `local_discount_package_deal` int(11) DEFAULT NULL,
  `foreign_currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foreign_normal_fee` int(11) DEFAULT NULL,
  `foreign_group` int(255) DEFAULT NULL,
  `foreign_stfi_member` int(255) DEFAULT NULL,
  `foreign_package_deal` int(11) DEFAULT NULL,
  `foreign_discount_group` int(11) DEFAULT NULL,
  `foreign_discount_stfi_member` int(11) DEFAULT NULL,
  `foreign_discount_package_deal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrate_staff`;
CREATE TABLE `migrate_staff` (
  `personel_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salutation` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`personel_number`),
  UNIQUE KEY `personel_number` (`personel_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `module_change_status`;
CREATE TABLE `module_change_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'module_change_status.id',
  `IdStudentRegistration` int(11) NOT NULL COMMENT 'FK tbl_studentregistration',
  `IdStudentRegSubjects` int(11) NOT NULL COMMENT 'FK tbl_studentregsubjects',
  `type` int(11) NOT NULL COMMENT '1=Deferment, 2=Withdrawal/Cancel',
  `reasons` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_schedule_id` int(11) NOT NULL DEFAULT '0' COMMENT 'IdCourseTaggingGroup',
  `new_schedule_id` int(11) NOT NULL DEFAULT '0' COMMENT 'IdCourseTaggingGroup',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `nidon`;
CREATE TABLE `nidon` (
  `nik` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(29) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nidon` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `non_invoice`;
CREATE TABLE `non_invoice` (
  `ni_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ni_fi_id` int(11) NOT NULL COMMENT 'FK fee',
  `ni_cur_id` bigint(11) NOT NULL COMMENT 'FK tbl_currency',
  `ni_amount` decimal(20,2) NOT NULL,
  `ni_amount_default_currency` decimal(20,2) NOT NULL,
  `ni_rcp_id` bigint(20) NOT NULL COMMENT 'FK receipt',
  `ni_create_by` bigint(20) NOT NULL,
  PRIMARY KEY (`ni_id`),
  KEY `ni_fi_id` (`ni_fi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_type_id` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `read_status` tinyint(255) DEFAULT '0',
  `read_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `notification_type`;
CREATE TABLE `notification_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `olc_center_start_exam`;
CREATE TABLE `olc_center_start_exam` (
  `cse_id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_id` int(11) NOT NULL COMMENT 'fk tbl exam center',
  `cse_start_time` time NOT NULL,
  `cse_start_by` int(11) NOT NULL,
  `cse_late_remarks` text,
  `cse_createddate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cse_id`),
  KEY `ec_id` (`ec_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `olc_central_exam`;
CREATE TABLE `olc_central_exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `es_id` int(11) NOT NULL,
  `ec_id` int(11) NOT NULL,
  `exam_date` date NOT NULL,
  `exam_type` int(11) DEFAULT NULL COMMENT '1:Central 2:Manual Default is local',
  `slot_id` int(11) NOT NULL,
  `tagging_slot_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1:requested;2:approved;3:rejected',
  `reason` text,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `olc_exam`;
CREATE TABLE `olc_exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_id` int(11) NOT NULL,
  `exam_date` date NOT NULL,
  `exam_type` int(11) NOT NULL DEFAULT '0' COMMENT '1:Central 2:Manual Default is local',
  `slot_id` int(11) NOT NULL,
  `reason` text,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `olc_exam_feedback`;
CREATE TABLE `olc_exam_feedback` (
  `ef_id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_date` date NOT NULL,
  `ec_id` int(11) NOT NULL,
  `slot_id` int(11) NOT NULL,
  `zero_accident` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:no 1yes',
  `electricity_issue` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:no 1yes',
  `manual_exam` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:no 1yes',
  `incomplete_documents` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:no 1yes',
  `infringed_regulations` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:no 1yes',
  `absenteeism` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:no 1yes',
  `narration_infringement` text COLLATE utf8_unicode_ci NOT NULL,
  `invigilator_onduty` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:no 1yes',
  `remarks` text COLLATE utf8_unicode_ci,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdrole` enum('Invigilator','Admin') COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmdt` datetime DEFAULT NULL,
  `confirmby` int(11) DEFAULT NULL,
  PRIMARY KEY (`ef_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `olc_exam_request`;
CREATE TABLE `olc_exam_request` (
  `req_id` int(11) NOT NULL AUTO_INCREMENT,
  `es_id` int(11) NOT NULL,
  `ec_id` int(11) NOT NULL,
  `slot_id` int(11) NOT NULL,
  `req_exam_request` int(11) NOT NULL DEFAULT '1' COMMENT '1:Central 2:Manual Default is local 3: exam session not conducted',
  `req_status` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1:requested;2:approved;3:rejected',
  `req_reason` text,
  `req_createddt` datetime NOT NULL,
  `req_createdby` int(11) NOT NULL,
  PRIMARY KEY (`req_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `olc_extra_time`;
CREATE TABLE `olc_extra_time` (
  `et_id` int(11) NOT NULL AUTO_INCREMENT,
  `cse_id` int(11) NOT NULL,
  `ec_id` int(11) NOT NULL,
  `et_extra_time` int(11) NOT NULL COMMENT 'in minutes',
  `et_remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `et_createdby` int(11) NOT NULL,
  `et_createddt` datetime NOT NULL,
  PRIMARY KEY (`et_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `olc_invigilator_onduty`;
CREATE TABLE `olc_invigilator_onduty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_date` date NOT NULL,
  `ec_id` int(11) NOT NULL,
  `invigilator_id` int(11) NOT NULL,
  `on_duty` tinyint(4) NOT NULL COMMENT '0:not update  1:Yes  2:No',
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `lastupddt` datetime NOT NULL,
  `lastupdby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `olc_pull_log`;
CREATE TABLE `olc_pull_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_id` int(11) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `pl_task_name` varchar(100) DEFAULT NULL,
  `pl_status` varchar(50) DEFAULT NULL,
  `pl_message` text,
  `pl_remarks` text,
  `pl_createddt` datetime NOT NULL,
  `pl_createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `olc_push_log`;
CREATE TABLE `olc_push_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_id` int(11) NOT NULL,
  `exam_date` date DEFAULT NULL,
  `pl_task_name` varchar(100) DEFAULT NULL,
  `pl_status` varchar(50) DEFAULT NULL,
  `pl_message` text,
  `pl_remarks` text,
  `pl_createddt` datetime NOT NULL,
  `pl_createdby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `olc_student_document_checklist`;
CREATE TABLE `olc_student_document_checklist` (
  `sdc_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_id` int(11) NOT NULL,
  `er_id` int(11) NOT NULL,
  `ec_id` int(11) NOT NULL,
  PRIMARY KEY (`sdc_id`),
  KEY `er_id` (`er_id`),
  KEY `ec_id` (`ec_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `olc_student_exam_details`;
CREATE TABLE `olc_student_exam_details` (
  `sed_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ec_id` bigint(20) unsigned NOT NULL,
  `er_id` bigint(20) unsigned NOT NULL COMMENT 'fk exam_registration',
  `sed_session` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sed_start_time` datetime NOT NULL,
  `sed_end_time` datetime DEFAULT NULL,
  `sed_mode` int(11) DEFAULT '1' COMMENT '0:manual, 1:online',
  `sed_createddt` datetime NOT NULL,
  `sed_createdby` int(11) NOT NULL,
  `sed_submittedby` int(11) DEFAULT NULL COMMENT '0:Student,1:Exam Center,2:auto submit',
  `sed_updatedt` datetime DEFAULT NULL,
  `sed_ipaddress` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`sed_id`),
  KEY `sed_id` (`sed_id`),
  KEY `er_id` (`er_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `olc_student_exam_question`;
CREATE TABLE `olc_student_exam_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oseq_id` int(11) NOT NULL,
  `ec_id` int(11) NOT NULL,
  `er_id` int(11) NOT NULL COMMENT 'fk exam_registration',
  `qs_id` int(11) DEFAULT NULL,
  `qsq_id` int(11) NOT NULL COMMENT 'qbs_studenttagging_question',
  `qsq_epq_id` int(11) NOT NULL COMMENT 'qbs_exam_paper_question',
  `esd_id` int(11) NOT NULL COMMENT 'exam setup detail id',
  `section_id` int(11) NOT NULL COMMENT 'qbs_exam_setup id',
  `qid` int(11) NOT NULL COMMENT 'fk qbs_question_main',
  `oseq_question_level` int(11) NOT NULL,
  `oseq_student_answer` int(11) DEFAULT NULL,
  `oseq_question_result` tinyint(4) DEFAULT NULL COMMENT '0: No answer 1: true 2:False',
  `oseq_student_answer_onsubmit` tinyint(4) DEFAULT NULL,
  `oseq_question_result_onsubmit` tinyint(4) DEFAULT NULL,
  `oseq_createddt` datetime DEFAULT NULL,
  `oseq_lastupdate` datetime DEFAULT NULL,
  `oseq_onsubmitdt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `er_id` (`er_id`),
  KEY `esd_id` (`esd_id`),
  KEY `section_id` (`section_id`),
  KEY `qid` (`qid`),
  KEY `qs_id` (`qs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `olc_student_exam_result`;
CREATE TABLE `olc_student_exam_result` (
  `ser_id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_id` int(11) NOT NULL,
  `er_id` int(11) NOT NULL,
  `pe_id` int(11) DEFAULT NULL,
  `ser_raw_mark` int(11) DEFAULT NULL COMMENT 'raw mark',
  `ser_mark` decimal(10,2) DEFAULT NULL COMMENT 'mark after calculation',
  `ser_grade` int(11) NOT NULL COMMENT 'id grade',
  `ser_grade_name` char(1) DEFAULT NULL COMMENT 'A,B,C',
  `ser_pass` varchar(4) DEFAULT NULL COMMENT 'Pass/fail',
  `ser_createddt` datetime NOT NULL,
  `ser_createdby` int(11) NOT NULL,
  PRIMARY KEY (`ser_id`),
  UNIQUE KEY `er_id` (`er_id`),
  KEY `pe_id` (`pe_id`),
  CONSTRAINT `olc_student_exam_result_ibfk_1` FOREIGN KEY (`pe_id`) REFERENCES `programme_exam` (`pe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `olc_student_exam_result_detail`;
CREATE TABLE `olc_student_exam_result_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_id` int(11) NOT NULL,
  `ser_id` int(11) NOT NULL COMMENT 'ol_student_exam_result',
  `er_id` int(11) NOT NULL,
  `es_id` int(11) NOT NULL,
  `pe_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `MarksObtained` decimal(10,2) NOT NULL COMMENT '(Raw)Jumlah markah yang student perolehi ',
  `FinalMarksObtained` decimal(10,2) NOT NULL COMMENT 'Jumlah markah yang student perolehi by Percentage ',
  `component_id` int(11) NOT NULL,
  `component_item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pe_id` (`pe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `olc_student_infringed`;
CREATE TABLE `olc_student_infringed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ef_id` int(11) NOT NULL,
  `er_id` int(11) NOT NULL,
  `regulation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ef_id` (`ef_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `olc_tagging_slot`;
CREATE TABLE `olc_tagging_slot` (
  `ol_set_id` int(11) NOT NULL AUTO_INCREMENT,
  `et_id` int(11) NOT NULL COMMENT 'ol_extra_time',
  `tagging_slot_id` int(11) NOT NULL,
  `slot_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `ec_id` int(11) NOT NULL,
  `cse_id` int(11) NOT NULL,
  `cse_expected_end_time` datetime NOT NULL,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  PRIMARY KEY (`ol_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `olc_tagging_slot_history`;
CREATE TABLE `olc_tagging_slot_history` (
  `ol_set_id` int(11) NOT NULL AUTO_INCREMENT,
  `et_id` int(11) NOT NULL COMMENT 'ol_extra_time',
  `tagging_slot_id` int(11) NOT NULL,
  `slot_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `ec_id` int(11) NOT NULL,
  `cse_id` int(11) NOT NULL,
  `cse_expected_end_time` datetime NOT NULL,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`ol_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `ol_cronjob_schedule`;
CREATE TABLE `ol_cronjob_schedule` (
  `ocs_id` int(11) NOT NULL AUTO_INCREMENT,
  `ocs_es_id` int(11) NOT NULL COMMENT 'fk exam_schedule',
  `ocs_ec_id` int(11) DEFAULT NULL COMMENT 'fk exam_center',
  `ocs_exam_date` date DEFAULT NULL COMMENT 'exam date',
  `ocs_push_date` date NOT NULL,
  `ocs_pull_date` date NOT NULL,
  `ocs_push_time` time NOT NULL,
  `ocs_act_push_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'actual date',
  `ocs_act_pull_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'actual date',
  `ocs_pushed` int(1) NOT NULL DEFAULT '0' COMMENT '0=yet-to-push,1=pushed,2=central,3=error,4=no-exam-schedule,9=queued-to-push',
  `ocs_pulled` int(1) NOT NULL DEFAULT '0' COMMENT '0=yet-to-pull,1=pulled,3=error',
  `ocs_gluster_push` int(1) NOT NULL DEFAULT '0' COMMENT '0=yet-to-pull,1=pulled,3=error',
  `ocs_gluster_pull` int(1) NOT NULL DEFAULT '0' COMMENT '0=yet-to-pull,1=pulled,3=error',
  `ocs_gluster_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ocs_gluster_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ocs_error_remark` text COLLATE utf8_unicode_ci NOT NULL,
  `ocs_error_remark_pull` text COLLATE utf8_unicode_ci NOT NULL,
  `ocs_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ocs_created_date` datetime NOT NULL,
  `ocs_created_by` int(11) NOT NULL,
  PRIMARY KEY (`ocs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `ol_examcenter_server`;
CREATE TABLE `ol_examcenter_server` (
  `svr_id` int(11) NOT NULL AUTO_INCREMENT,
  `svr_ec_id` int(11) NOT NULL,
  `svr_hostname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `svr_foldername` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `svr_ipaddress` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `svr_active` int(1) NOT NULL COMMENT '1=active, 0=disabled',
  `ngrok` int(1) NOT NULL,
  PRIMARY KEY (`svr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `ol_question`;
CREATE TABLE `ol_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_main_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `language` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `ol_question_answer`;
CREATE TABLE `ol_question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_main_id` int(11) NOT NULL,
  `answer_main_id` int(11) NOT NULL COMMENT 'ol_question',
  `answer` text COLLATE utf8_unicode_ci,
  `correct_answer` int(11) DEFAULT NULL,
  `order` int(10) unsigned DEFAULT NULL,
  `lang_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_id` (`answer_main_id`),
  KEY `question_main_id` (`question_main_id`),
  KEY `order` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_rcp_id` bigint(20) NOT NULL COMMENT 'FK receipt',
  `p_payment_mode_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK payment_mode',
  `p_cheque_no` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_doc_bank` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_doc_branch` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_terminal_id` int(11) DEFAULT NULL COMMENT 'FK creditcard_terminal',
  `p_card_no` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_cur_id` int(11) NOT NULL COMMENT 'FK tbl_currency',
  `p_amount` decimal(20,2) NOT NULL,
  `p_amount_default_currency` decimal(20,2) NOT NULL DEFAULT '0.00',
  `p_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ENTRY' COMMENT 'APPROVE, ENTRY, CANCEL',
  `p_adjustment_id` int(11) NOT NULL DEFAULT '0',
  `p_migs` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `p_fpx` int(11) NOT NULL DEFAULT '0' COMMENT 'FK fpx_transaction',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`p_id`),
  KEY `p_payment_mode_id` (`p_payment_mode_id`,`p_terminal_id`,`p_rcp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Store payment made from receipt';


DROP TABLE IF EXISTS `payment_bank_record`;
CREATE TABLE `payment_bank_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_path` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `upload_by` int(11) NOT NULL,
  `upload_date` datetime NOT NULL,
  `file_created_date` datetime NOT NULL,
  `period_start` date NOT NULL,
  `period_end` date NOT NULL,
  `total_record` int(11) NOT NULL,
  `total_amount` decimal(30,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `payment_bank_record_detail`;
CREATE TABLE `payment_bank_record_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pbr_id` bigint(20) NOT NULL,
  `billing_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payee_id` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bill_ref_1` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `bill_ref_2` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `bill_ref_3` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `bill_ref_4` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `bill_ref_5` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_total` decimal(20,0) NOT NULL,
  `amount_1` decimal(20,0) NOT NULL,
  `amount_2` decimal(20,0) NOT NULL,
  `amount_3` decimal(20,0) NOT NULL,
  `amount_4` decimal(20,0) NOT NULL,
  `amount_5` decimal(20,0) NOT NULL,
  `amount_6` decimal(20,0) NOT NULL,
  `amount_7` decimal(20,0) NOT NULL,
  `amount_8` decimal(20,0) NOT NULL,
  `amount_9` decimal(20,0) NOT NULL,
  `amount_10` decimal(20,0) NOT NULL,
  `status_payment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `paid_date` date NOT NULL,
  `bancs_journal_number` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_channel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `debet_account` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_record_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `finance_processing` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_record_type` (`account_record_type`),
  KEY `payee_id` (`payee_id`),
  KEY `billing_no` (`billing_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `payment_detail`;
CREATE TABLE `payment_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pm_id` bigint(20) NOT NULL,
  `item_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_amount` decimal(20,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `payment_gateway`;
CREATE TABLE `payment_gateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_mode` int(11) NOT NULL COMMENT 'payment_mode',
  `desc` varchar(500) NOT NULL,
  `vpc_merchant` varchar(250) DEFAULT NULL,
  `access_code` varchar(500) DEFAULT NULL,
  `secure_secret` varchar(500) DEFAULT NULL,
  `vpc_url` text,
  `seller_id` varchar(500) DEFAULT NULL,
  `exchange_id` varchar(500) DEFAULT NULL,
  `testing` int(11) NOT NULL COMMENT '1=yes, 0=no',
  `default` int(11) NOT NULL COMMENT '1=main',
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `payment_gateway_program`;
CREATE TABLE `payment_gateway_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_gateway` int(11) NOT NULL COMMENT 'payment_gateway',
  `program_id` int(11) NOT NULL COMMENT 'tbl_program',
  `createdDate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `payment_import`;
CREATE TABLE `payment_import` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payer` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appl_id` bigint(20) NOT NULL,
  `payment_date` datetime NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_mode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_reference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `importer` bigint(20) NOT NULL,
  `import_date` datetime NOT NULL,
  `payment_main_id` bigint(20) NOT NULL COMMENT 'fk payment_main',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `payment_main`;
CREATE TABLE `payment_main` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `billing_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payer` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appl_id` int(11) DEFAULT NULL,
  `payment_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `payment_mode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_reference` bigint(20) DEFAULT NULL COMMENT 'pk payment_bank_record_detail',
  `slip_transaction_reference` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `billing_no` (`billing_no`),
  KEY `payer` (`payer`),
  KEY `appl_id` (`appl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `payment_migs`;
CREATE TABLE `payment_migs` (
  `pm_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pm_txn_id` bigint(20) NOT NULL COMMENT 'FK transaction_migs',
  `pm_amount` decimal(20,2) NOT NULL,
  `pm_currency_id` bigint(20) NOT NULL COMMENT 'FK tbl_currency',
  `pm_date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`pm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `payment_mode`;
CREATE TABLE `payment_mode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_mode` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_second_language` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_group` bigint(20) NOT NULL COMMENT 'fk tbl_definationms',
  `foc` tinyint(4) NOT NULL COMMENT '1=yes;0=no',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: enable; 0:disable',
  `upload_payment` tinyint(1) NOT NULL COMMENT '1=yes;0=no',
  `display_portal` tinyint(1) NOT NULL COMMENT '1=yes;0=no',
  `online` tinyint(1) NOT NULL COMMENT '1=yes;0=no',
  `fees_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=None, 1=Amount, 2=Percentage',
  `fees_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'fees amount',
  `fees_percentage` int(11) NOT NULL DEFAULT '0' COMMENT 'fees percenrage',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updDate` datetime NOT NULL,
  `updBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_group` (`payment_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `payment_plan`;
CREATE TABLE `payment_plan` (
  `pp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pp_appl_id` int(11) NOT NULL,
  `pp_installment_no` int(11) NOT NULL,
  `pp_amount` decimal(10,2) NOT NULL,
  `pp_creator` bigint(20) NOT NULL,
  `pp_create_date` datetime NOT NULL,
  PRIMARY KEY (`pp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `payment_plan_detail`;
CREATE TABLE `payment_plan_detail` (
  `ppd_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ppd_payment_plan_id` bigint(20) NOT NULL,
  `ppd_invoice_id` bigint(20) NOT NULL,
  `ppd_installment_no` int(11) NOT NULL,
  PRIMARY KEY (`ppd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `payment_plan_invoice`;
CREATE TABLE `payment_plan_invoice` (
  `ppi_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `ppi_plan_id` bigint(11) NOT NULL COMMENT 'fk_payment_plan',
  `ppi_invoice_id` bigint(11) NOT NULL COMMENT 'fk_invoice_main',
  `ppi_invoice_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ppi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='table to store invoice that will be repayment plan';


DROP TABLE IF EXISTS `pregraduate_back`;
CREATE TABLE `pregraduate_back` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idStudentRegistration` bigint(20) NOT NULL,
  `add_date` datetime NOT NULL,
  `add_by` bigint(20) NOT NULL,
  `dean_approval_date` datetime DEFAULT NULL,
  `dean_approval_skr` int(150) DEFAULT NULL,
  `rector_approval_date` datetime DEFAULT NULL,
  `rector_approval_skr` int(150) DEFAULT NULL,
  `migrated` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'status for migrated to graduation list',
  PRIMARY KEY (`id`),
  KEY `idStudentRegistration` (`idStudentRegistration`,`add_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='list of pregraduate student';


DROP TABLE IF EXISTS `pregraduate_list`;
CREATE TABLE `pregraduate_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idStudentRegistration` bigint(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Shorlisted 1:Approve 2:Graduated',
  `c_id` int(11) DEFAULT NULL COMMENT 'fk convocation',
  `shortlisted_date` datetime NOT NULL,
  `shortlisted_by` bigint(20) NOT NULL,
  `approve_date` datetime DEFAULT NULL COMMENT 'change status to approve',
  `approve_by` int(11) DEFAULT NULL COMMENT 'change status to approve',
  `graduate_date` datetime DEFAULT NULL COMMENT 'change status to graduate',
  `graduate_by` int(11) DEFAULT NULL COMMENT 'change status to graduate',
  `invitation_date` datetime DEFAULT NULL COMMENT 'change status to invite',
  `invitation_by` int(11) DEFAULT NULL COMMENT 'change status to invite',
  `publish` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  `publish_createdt` datetime DEFAULT NULL,
  `publish_createby` int(11) DEFAULT NULL,
  `confirm_attend` tinyint(1) DEFAULT NULL,
  `confirm_by` int(11) unsigned DEFAULT NULL,
  `confirm_date` datetime DEFAULT NULL,
  `confirm_role` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Not Paid 1:Paid',
  `receipt_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idStudentRegistration_2` (`idStudentRegistration`),
  KEY `idStudentRegistration` (`idStudentRegistration`,`shortlisted_by`),
  KEY `idStudentRegistration_3` (`idStudentRegistration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='list of pregraduate student';


DROP TABLE IF EXISTS `pregraduate_student_checklist`;
CREATE TABLE `pregraduate_student_checklist` (
  `psc_id` int(11) NOT NULL AUTO_INCREMENT,
  `pregraduate_list_id` int(11) NOT NULL COMMENT 'fk pregraduate_list',
  `IdStudentRegistration` int(11) NOT NULL,
  `idChecklist` int(11) NOT NULL COMMENT 'fk tbl_definationms (type 176)',
  `psc_status` int(11) NOT NULL DEFAULT '0',
  `psc_view` int(11) NOT NULL DEFAULT '0',
  `remarks_message` longtext COLLATE utf8mb4_unicode_ci,
  `remarks_by` int(11) DEFAULT NULL,
  `remarks_date` datetime DEFAULT NULL,
  `remarks_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `psc_createddt` datetime NOT NULL,
  `psc_createdby` int(11) NOT NULL,
  PRIMARY KEY (`psc_id`),
  UNIQUE KEY `pregraduate_list_id` (`pregraduate_list_id`,`IdStudentRegistration`,`idChecklist`),
  KEY `psc_id` (`psc_id`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `proforma_invoice_detail`;
CREATE TABLE `proforma_invoice_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `proforma_invoice_main_id` bigint(20) NOT NULL,
  `proforma_invoice_student_id` int(11) NOT NULL,
  `fi_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk fee_item',
  `fee_item_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cur_id` bigint(20) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `amount_gst` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'amount before gst',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proforma_invoice_student_id` (`proforma_invoice_student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `proforma_invoice_main`;
CREATE TABLE `proforma_invoice_main` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bill_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `corporate_id` bigint(20) DEFAULT NULL COMMENT 'fk tbl_takafuloperator',
  `IdStudentRegistration` bigint(20) DEFAULT NULL COMMENT 'fk tbl_studentregistration',
  `batchId` int(11) DEFAULT NULL,
  `ber_id` int(11) DEFAULT '0' COMMENT 'fk batch_exam_registration',
  `category_id` int(11) DEFAULT '1' COMMENT 'tbl_category',
  `totalStudent` int(11) DEFAULT NULL,
  `bill_amount` decimal(20,2) NOT NULL,
  `bill_amount_gst` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'amount before gst',
  `amount_lumpsum` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount_lumpsum_gst` decimal(20,2) NOT NULL DEFAULT '0.00',
  `bill_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_mode` int(11) NOT NULL DEFAULT '0' COMMENT 'FK payment_mode',
  `date_create` datetime NOT NULL,
  `creator` bigint(20) NOT NULL COMMENT 'fk tbl_user',
  `fs_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'fk fee_structure',
  `fsp_id` bigint(20) DEFAULT NULL COMMENT 'fk fee_structure_plan',
  `program_id` bigint(11) DEFAULT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'A:active X: cancel, W=waived, E=exempted',
  `cancel_by` bigint(20) DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `currency_id` bigint(20) DEFAULT NULL,
  `invoice_type` enum('PROCESSING','TUTION_FEE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee_category` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_fee_category id',
  `branch_id` int(11) unsigned DEFAULT NULL,
  `invoice_id` int(11) DEFAULT '0',
  `registration_item` int(11) DEFAULT NULL COMMENT 'tbl_definationms 160',
  `from` int(11) DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  `remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bill_number` (`bill_number`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `proforma_invoice_student`;
CREATE TABLE `proforma_invoice_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL COMMENT 'fk proforma_invoice_main',
  `IdStudentRegistration` int(11) NOT NULL,
  `examregistration_main_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK exam_registration_main',
  `cur_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `amount_gst` decimal(20,2) NOT NULL DEFAULT '0.00',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `proforma_invoice_subject`;
CREATE TABLE `proforma_invoice_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proforma_invoice_main_id` bigint(20) NOT NULL COMMENT 'FL proforma_invoice_main',
  `proforma_invoice_detail_id` bigint(20) NOT NULL COMMENT 'FL proforma_invoice_detail',
  `proforma_invoice_student_id` int(11) NOT NULL,
  `subject_id` bigint(20) NOT NULL COMMENT 'FK tbl_subjectmaster',
  `amount` decimal(20,2) NOT NULL,
  `amount_gst` decimal(20,2) NOT NULL DEFAULT '0.00',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proforma_invoice_main_id` (`proforma_invoice_main_id`,`proforma_invoice_detail_id`,`subject_id`),
  KEY `proforma_invoice_student_id` (`proforma_invoice_student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `programme_exam`;
CREATE TABLE `programme_exam` (
  `pe_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProgram` int(11) NOT NULL,
  `IdLandscape` int(11) NOT NULL DEFAULT '0',
  `pe_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pe_active` tinyint(4) NOT NULL DEFAULT '1',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifieddt` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`pe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `programme_exam_prerequisite`;
CREATE TABLE `programme_exam_prerequisite` (
  `pep_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProgram` int(11) NOT NULL,
  `pe_id` int(11) NOT NULL,
  `pep_based_on` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: Programme, 2:Exam',
  `pep_idprogram` int(11) NOT NULL DEFAULT '0',
  `pep_idlandscape` int(11) NOT NULL DEFAULT '0',
  `pep_idsubject` int(11) NOT NULL DEFAULT '0',
  `pep_programmeexam_id` int(11) NOT NULL DEFAULT '0',
  `pep_required_id` int(11) DEFAULT NULL,
  `pep_type` int(11) DEFAULT NULL COMMENT 'FK Defination',
  `pep_grade_status` tinyint(4) DEFAULT '0' COMMENT '0: Fail, 1:Pass',
  `pep_attendance` tinyint(4) DEFAULT '0' COMMENT 'percentage',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifieddt` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`pep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `propose_program`;
CREATE TABLE `propose_program` (
  `pp_id` int(11) NOT NULL AUTO_INCREMENT,
  `pp_IdProgramEntry` int(11) NOT NULL COMMENT 'fk tbl_programentry',
  `pp_IdProgram` int(11) NOT NULL COMMENT 'propose program (fk tbl_program))',
  `pp_min_rank_qualification` int(11) NOT NULL,
  `pp_createddt` datetime NOT NULL,
  `pp_createdby` int(11) NOT NULL,
  PRIMARY KEY (`pp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q001_pool`;
CREATE TABLE `q001_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `dateUpd` datetime NOT NULL,
  `updUser` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `target_user` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q001_pool_semester`;
CREATE TABLE `q001_pool_semester` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semester_id` int(11) NOT NULL,
  `pool_id` int(11) NOT NULL,
  `dateUpd` datetime NOT NULL,
  `updUser` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q002_chapter`;
CREATE TABLE `q002_chapter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_set_id` int(11) NOT NULL COMMENT 'PK q020_sectiontagging',
  `pool_id` int(11) NOT NULL COMMENT 'PK q001_pool',
  `registration_item_id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subsection_instruction` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `order` smallint(6) NOT NULL,
  `dateUpd` datetime NOT NULL,
  `updUser` int(11) NOT NULL,
  `question_type` int(11) NOT NULL COMMENT 'PK q005_question_type',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q003_question`;
CREATE TABLE `q003_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pool_id` int(11) DEFAULT NULL,
  `chapter_id` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rubric_id` int(11) DEFAULT '0' COMMENT 'PK  q006_rubric',
  `question` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1: Active 2: Not Active',
  `point` int(11) DEFAULT NULL,
  `updUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateUpd` datetime NOT NULL,
  `review` int(11) NOT NULL COMMENT '0 - Not review, 1 - Reviewed',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q003_questiontagging`;
CREATE TABLE `q003_questiontagging` (
  `id_questiontagging` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `main_set_id` int(11) NOT NULL COMMENT 'PK q020_sectiontagging',
  `section_id` int(11) NOT NULL COMMENT 'PK q002_chapter',
  `order` int(11) NOT NULL,
  `IdQuestion` int(100) NOT NULL COMMENT 'PK q003_question',
  `updUser` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateUpd` datetime NOT NULL,
  PRIMARY KEY (`id_questiontagging`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q003_question_set`;
CREATE TABLE `q003_question_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pool_id` int(11) DEFAULT NULL,
  `question_type` int(11) DEFAULT NULL,
  `topic_id` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `difficulty_level` int(11) DEFAULT '1' COMMENT '1=easy, 2=medium, 3=difficult',
  `english` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `malay` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `chinese` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tamil` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1: Active 2: Not Active',
  `point` int(11) DEFAULT NULL,
  `createdby` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createddt` datetime NOT NULL,
  `review` int(11) NOT NULL COMMENT '0 - Not review, 1 - Reviewed',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q004_rubricdetails`;
CREATE TABLE `q004_rubricdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Section` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'PK q002_chapter',
  `ValueNumber` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `weightage` int(11) NOT NULL,
  `dateUpd` datetime NOT NULL,
  `updUser` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q005_question_type`;
CREATE TABLE `q005_question_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` int(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q006_answer`;
CREATE TABLE `q006_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `malay` longtext COLLATE utf8mb4_unicode_ci,
  `english` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tamil` mediumtext COLLATE utf8mb4_unicode_ci,
  `chinese` mediumtext COLLATE utf8mb4_unicode_ci,
  `correct_answer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS `q010_setcandidate`;
CREATE TABLE `q010_setcandidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idApplication` int(11) NOT NULL COMMENT 'PK r016_registrationdetails',
  `idSchedule` int(11) NOT NULL,
  `idCourse` int(11) NOT NULL,
  `idTos` int(11) NOT NULL,
  `dateUpd` datetime NOT NULL,
  `usrUpd` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q010_student_answer`;
CREATE TABLE `q010_student_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdMain` int(11) NOT NULL,
  `main_set_id` int(11) DEFAULT NULL,
  `IdSubject` int(11) DEFAULT NULL,
  `IdLecturer` int(11) DEFAULT NULL,
  `pool_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `chapter_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer` int(11) DEFAULT NULL,
  `answer_text` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q010_student_answer_main`;
CREATE TABLE `q010_student_answer_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_set_id` int(11) DEFAULT NULL,
  `IdSubject` int(11) DEFAULT NULL,
  `IdLecturer` int(11) DEFAULT NULL,
  `pool_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `semester_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '1=submit, 0=incomplete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `q020_sectiontagging`;
CREATE TABLE `q020_sectiontagging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `branch_id` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `IdProgramScheme` int(11) DEFAULT NULL,
  `mode_of_program_id` int(11) NOT NULL,
  `mode_of_studies_id` int(11) NOT NULL,
  `mode_of_programtypes_id` int(11) NOT NULL,
  `question_pool` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `section_instructions` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL COMMENT '1:Active 2:Not Active',
  `dateUpd` datetime NOT NULL,
  `updUser` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbslog`;
CREATE TABLE `qbslog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `process` text NOT NULL,
  `param` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_difficulty_level`;
CREATE TABLE `qbs_difficulty_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `level` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS `qbs_exam_paper`;
CREATE TABLE `qbs_exam_paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdExamMain` int(11) DEFAULT NULL COMMENT 'qbs_exam_setup_main',
  `IdScheduleTaggingSlot` int(11) DEFAULT NULL,
  `IdSchedule` int(11) DEFAULT NULL COMMENT 'exam_schedule',
  `publish_immediately` int(11) DEFAULT NULL,
  `total_set` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1:Draft 2:Final 3:Approved 4:Waiting For Approval 5:Reject 6:Change',
  `mode` int(11) DEFAULT '0' COMMENT '0:manual, 1:online - online exam portal',
  `createdby` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `lastchangeby` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastchangedt` datetime DEFAULT NULL,
  `downloadby` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `downloaddt` datetime DEFAULT NULL,
  `approvedby` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approveddt` datetime DEFAULT NULL,
  `coverpage` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_student` int(11) DEFAULT NULL,
  `tbe_idbatch` int(11) DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_exam_paper_detail`;
CREATE TABLE `qbs_exam_paper_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ep_id` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper',
  `status` int(11) DEFAULT NULL COMMENT '1=active,2=inactive/delete',
  `mode` int(11) DEFAULT NULL COMMENT '0:manual, 1:online',
  `IdSet` int(11) DEFAULT NULL COMMENT 'ol_questionset',
  `createdby` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `filename` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filelocation` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `mimetype` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_exam_paper_document`;
CREATE TABLE `qbs_exam_paper_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ep_id` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper',
  `epd_id` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper_detail',
  `fileformat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filename` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filerename` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rename` text COLLATE utf8mb4_unicode_ci,
  `filelocation` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `mimetype` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `uploadby` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uploaddt` datetime DEFAULT NULL,
  `upload` int(11) DEFAULT NULL COMMENT '1:upload, 0:download',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_exam_paper_generate`;
CREATE TABLE `qbs_exam_paper_generate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actionby` int(11) NOT NULL,
  `actiondate` datetime NOT NULL,
  `ipaddress` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `savepath` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `IdPaper` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper',
  `IdPaperDetail` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper_detail',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS `qbs_exam_paper_history`;
CREATE TABLE `qbs_exam_paper_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdExamPaper` int(11) DEFAULT NULL COMMENT 'qbs_exam_exam_paper',
  `IdExamMain` int(11) DEFAULT NULL COMMENT 'qbs_exam_setup_main',
  `IdSchedule` int(11) DEFAULT NULL COMMENT 'exam_schedule',
  `IdScheduleTaggingSlot` int(11) DEFAULT NULL,
  `publish_immediately` int(11) DEFAULT NULL,
  `total_set` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1:Draft 2:Final 3:Approved 4:Waiting For Approval 5:Reject 6:Change',
  `mode` int(11) DEFAULT '0' COMMENT '0:manual, 1:online - online exam portal',
  `createdby` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `lastchangeby` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastchangedt` datetime DEFAULT NULL,
  `downloadby` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `downloaddt` datetime DEFAULT NULL,
  `approvedby` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approveddt` datetime DEFAULT NULL,
  `coverpage` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_exam_paper_questions`;
CREATE TABLE `qbs_exam_paper_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ep_id` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper',
  `epd_id` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper_detail',
  `IdExam` int(11) DEFAULT NULL COMMENT 'qbs_exam_setup',
  `qid` int(11) DEFAULT NULL COMMENT 'qbs_question_main',
  `setno` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `qidb` int(11) DEFAULT NULL COMMENT 'qid before',
  `updby` int(11) DEFAULT NULL,
  `upddt` datetime DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `qid` (`qid`),
  KEY `epid` (`ep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_exam_setup`;
CREATE TABLE `qbs_exam_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdExamMain` int(11) DEFAULT NULL COMMENT 'qbs_exam_setup_main',
  `component_id` int(11) DEFAULT NULL COMMENT 'tbl_marksdistributionmaster',
  `component_item_id` int(11) DEFAULT NULL COMMENT 'tbl_marksdistributiondetails',
  `question_type` int(11) DEFAULT NULL COMMENT 'qbs_question_type',
  `total_question` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_answer` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Number of question to answer',
  `question_mark` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mark per question to answer',
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `cpart_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modifyby` int(11) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `tbe_idbatch` int(11) DEFAULT NULL,
  `tbe_part` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='configuration for each sub component->mark distribution setup';


DROP TABLE IF EXISTS `qbs_exam_setup_language`;
CREATE TABLE `qbs_exam_setup_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdExamMain` int(11) NOT NULL COMMENT 'qbs_exam_setup_main',
  `IdExamDetail` int(11) DEFAULT NULL COMMENT 'qbs_exam_setup',
  `description` text COLLATE utf8mb4_unicode_ci COMMENT 'instruction',
  `name` text COLLATE utf8mb4_unicode_ci,
  `language` int(11) DEFAULT NULL COMMENT 'language',
  `modifyby` int(11) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='exam setup multi language';


DROP TABLE IF EXISTS `qbs_exam_setup_main`;
CREATE TABLE `qbs_exam_setup_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `esd_id` int(11) DEFAULT NULL,
  `IdMarkDistribution` int(11) DEFAULT NULL,
  `IdProgramExam` int(11) DEFAULT NULL,
  `taxanomy_type` int(11) DEFAULT NULL COMMENT '1=bloom, 2=difficulty',
  `random_formula` int(11) DEFAULT NULL COMMENT '1=by candidate, 2=set paper',
  `duration` int(11) DEFAULT NULL,
  `description` text,
  `name` varchar(250) DEFAULT NULL,
  `code` varchar(250) DEFAULT NULL,
  `total_question` int(11) DEFAULT NULL,
  `total_set` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1=active, 0=inactive',
  `status_date` datetime DEFAULT NULL,
  `status_by` int(11) DEFAULT NULL,
  `publish` int(11) DEFAULT NULL,
  `createdby` varchar(50) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `modifyby` varchar(50) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  `tbe_idbatch` int(11) DEFAULT NULL COMMENT 'tbl_batchmaster (TBE) ',
  `migrate_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='configuration by mark distribution';


DROP TABLE IF EXISTS `qbs_exam_setup_pool`;
CREATE TABLE `qbs_exam_setup_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdExamMain` int(11) DEFAULT NULL COMMENT 'qbs_exam_setup_main',
  `IdExam` text COLLATE utf8mb4_unicode_ci COMMENT 'qbs_exam_setup',
  `course_id` int(11) DEFAULT NULL,
  `modifyby` int(11) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='to know which question pool to be used for tos setup';


DROP TABLE IF EXISTS `qbs_language`;
CREATE TABLE `qbs_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `system` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_log`;
CREATE TABLE `qbs_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `log_function` text,
  `log_description` text,
  `log_status` varchar(10) DEFAULT NULL,
  `log_date` datetime NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_question`;
CREATE TABLE `qbs_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_main_id` int(11) DEFAULT NULL COMMENT 'qbs_question_main',
  `question_parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'qbs_question_parent',
  `question_parent_detail_id` int(11) NOT NULL DEFAULT '0' COMMENT 'qbs_question_parent_detail',
  `question` text COLLATE utf8mb4_unicode_ci,
  `language` int(11) DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  `manual` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1-yes, 0-no',
  `migratedt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_main_id` (`question_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_question_answer`;
CREATE TABLE `qbs_question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer_main_id` int(11) NOT NULL COMMENT 'qbs_question_answer_main',
  `question_id` int(11) NOT NULL COMMENT 'qbs_question',
  `lang_id` int(11) NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci,
  `updBy` int(11) DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  `migratedt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`),
  KEY `question_main_id` (`answer_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_question_answer_main`;
CREATE TABLE `qbs_question_answer_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_main_id` int(11) NOT NULL COMMENT 'qbs_question_main',
  `question_parent_id` int(11) DEFAULT NULL,
  `correct_answer` int(11) DEFAULT NULL COMMENT 'NULL : essay answer',
  `order` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updBy` int(11) NOT NULL,
  `updDate` datetime NOT NULL,
  `answer_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'from tbe Answersid',
  `tbe_aid` int(11) DEFAULT NULL,
  `migratedt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_question_main`;
CREATE TABLE `qbs_question_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) DEFAULT NULL COMMENT 'tbl_program',
  `course_id` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `assessment_type` int(11) DEFAULT NULL,
  `question_type` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `taxanomy_level` int(11) DEFAULT '1',
  `question_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question_name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `difficulty_level` int(11) DEFAULT '1',
  `tk` int(11) DEFAULT NULL COMMENT 'type of knowledge',
  `tb` int(11) DEFAULT NULL COMMENT 'Thinking Behaviour',
  `to` int(11) DEFAULT NULL COMMENT 'Thinking Order',
  `status` int(11) DEFAULT NULL COMMENT '0:draft 1: Active/Approved 2:Ready to Moderate 3:Inactive/Rejected',
  `point` decimal(20,1) DEFAULT NULL,
  `template` int(11) NOT NULL COMMENT '1: Essay 2:branching 3:MCQ',
  `limit_min` int(11) DEFAULT NULL COMMENT 'essay',
  `limit_max` int(11) DEFAULT NULL COMMENT 'essay',
  `createdby` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `createddt` datetime NOT NULL,
  `modifyby` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `modifydt` datetime NOT NULL,
  `moderateby` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `moderatedt` datetime DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1' COMMENT '1=active, 0=not active',
  `approvedby` varchar(50) CHARACTER SET latin1 NOT NULL,
  `approveddt` datetime NOT NULL,
  `used` int(11) DEFAULT NULL,
  `used_upddt` datetime NOT NULL,
  `manual` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1-yes, 0-no',
  `tbe_qid` int(11) DEFAULT NULL,
  `tbe_part` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migratedt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_question_mcq_answer`;
CREATE TABLE `qbs_question_mcq_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_main_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer` text CHARACTER SET utf8,
  `correct_answer` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`),
  KEY `question_main_id` (`question_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_question_parent`;
CREATE TABLE `qbs_question_parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_main_id` int(11) DEFAULT NULL,
  `parent_main_id` int(11) DEFAULT NULL COMMENT 'qbs_question_parent_main',
  `question` text COLLATE utf8mb4_unicode_ci,
  `language` int(11) DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_main_id` (`question_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_question_parent_detail`;
CREATE TABLE `qbs_question_parent_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_main_id` int(11) DEFAULT NULL COMMENT 'qbs_question_parent_main',
  `question` text COLLATE utf8mb4_unicode_ci COMMENT 'tak tahu nak guna ke tak',
  `point` decimal(20,1) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_question_parent_main`;
CREATE TABLE `qbs_question_parent_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_main_id` int(11) DEFAULT NULL,
  `question_type` int(11) DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_main_id` (`question_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_question_type`;
CREATE TABLE `qbs_question_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branching` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:no;1:yes - must have qbs_question_parent',
  `enable` int(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_question_upload`;
CREATE TABLE `qbs_question_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_type` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `upl_filename` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upl_filerename` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upl_filelocation` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upl_filesize` int(11) NOT NULL,
  `upl_mimetype` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upl_upddate` datetime NOT NULL,
  `upl_upduser` int(11) NOT NULL,
  `total_question` int(11) DEFAULT NULL,
  `total_uploaded` int(11) DEFAULT NULL,
  `error` int(11) DEFAULT NULL,
  `error_msg` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_question_upload_data`;
CREATE TABLE `qbs_question_upload_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upl_id` int(11) NOT NULL COMMENT 'qbs_question_upload',
  `parent_id` int(11) NOT NULL,
  `question_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topic_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topic_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taxanomy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question` text COLLATE utf8mb4_unicode_ci,
  `A` text COLLATE utf8mb4_unicode_ci,
  `B` text COLLATE utf8mb4_unicode_ci,
  `C` text COLLATE utf8mb4_unicode_ci,
  `D` text COLLATE utf8mb4_unicode_ci,
  `correct_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=success,2-failed',
  `IdQuestionMain` int(11) DEFAULT NULL COMMENT 'qbs_question_main',
  `IdQuestion` int(11) DEFAULT NULL COMMENT 'qbs_question',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_standard`;
CREATE TABLE `qbs_standard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `code` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_standard_detail`;
CREATE TABLE `qbs_standard_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `st_id` int(11) NOT NULL COMMENT 'qbs_standard',
  `name` text NOT NULL,
  `code` varchar(200) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_status`;
CREATE TABLE `qbs_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_studenttagging`;
CREATE TABLE `qbs_studenttagging` (
  `qs_id` int(11) NOT NULL AUTO_INCREMENT,
  `qs_ep_id` int(11) NOT NULL COMMENT 'qbs_exam_paper',
  `qs_epd_id` int(11) NOT NULL COMMENT 'qbs_exam_paper_detail',
  `qs_er_id` int(11) NOT NULL COMMENT 'exam_registration',
  `qs_esd_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `qs_pull` int(11) DEFAULT NULL COMMENT '1=success, 0=fail',
  `qs_pull_date` datetime DEFAULT NULL,
  `qs_push` int(11) DEFAULT NULL COMMENT '1=success, 0=fail',
  `qs_push_date` datetime DEFAULT NULL,
  PRIMARY KEY (`qs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_studenttagging_history`;
CREATE TABLE `qbs_studenttagging_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qs_id` int(11) DEFAULT NULL COMMENT 'qbs_studenttagging',
  `qs_ep_id` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper',
  `qs_epd_id` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper_detail',
  `qs_er_id` int(11) DEFAULT NULL COMMENT 'exam_registration',
  `qs_esd_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `qs_pull` int(11) DEFAULT NULL COMMENT '1=success, 0=fail',
  `qs_pull_date` datetime DEFAULT NULL,
  `qs_push` int(11) DEFAULT NULL COMMENT '1=success, 0=fail',
  `qs_push_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_studenttagging_question`;
CREATE TABLE `qbs_studenttagging_question` (
  `qsq_id` int(11) NOT NULL AUTO_INCREMENT,
  `qsq_qsid` int(11) NOT NULL COMMENT 'qbs_studenttagging',
  `qsq_qid` int(11) DEFAULT NULL COMMENT 'qbs_question_main ',
  `qsq_epq_id` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper_question',
  `qsq_es_id` int(11) DEFAULT NULL COMMENT 'qbs_exam_setup',
  `qsq_order` int(11) NOT NULL,
  `qsq_answer` varchar(20) DEFAULT NULL,
  `qsq_result` varchar(20) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `qsq_push` int(11) DEFAULT NULL COMMENT '1=success, 0=fail',
  `qsq_push_date` datetime DEFAULT NULL,
  `pull_status` int(11) DEFAULT NULL COMMENT '1=success, 0=fail',
  `pull_date` datetime DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL COMMENT 'tbl_examcenter',
  `mapping_question_date` datetime DEFAULT NULL,
  PRIMARY KEY (`qsq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_studenttagging_question_history`;
CREATE TABLE `qbs_studenttagging_question_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qsq_id` int(11) NOT NULL COMMENT 'qbs_studenttagging_question',
  `qsq_qsid` int(11) NOT NULL COMMENT 'qbs_studenttagging',
  `qsq_qid` int(11) NOT NULL COMMENT 'qbs_question_main ',
  `qsq_epq_id` int(11) NOT NULL COMMENT 'qbs_exam_paper_question',
  `qsq_es_id` int(11) NOT NULL COMMENT 'qbs_exam_setup',
  `qsq_order` int(11) NOT NULL,
  `qsq_answer` varchar(20) DEFAULT NULL,
  `qsq_result` varchar(20) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `qsq_push` int(11) DEFAULT NULL COMMENT '1=success, 0=fail',
  `qsq_push_date` datetime DEFAULT NULL,
  `pull_status` int(11) DEFAULT NULL COMMENT '1=success, 0=fail',
  `pull_date` datetime DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL COMMENT 'tbl_examcenter',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_syllabus`;
CREATE TABLE `qbs_syllabus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `courseid` varchar(50) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `level` int(11) DEFAULT NULL,
  `seq` int(11) NOT NULL DEFAULT '0',
  `createdby` varchar(250) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `migratedt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_system_error`;
CREATE TABLE `qbs_system_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdMarkDistribution` int(11) DEFAULT NULL,
  `er_id` int(11) DEFAULT NULL COMMENT 'exam_registration',
  `es_date` date DEFAULT NULL,
  `esd_id` int(11) DEFAULT NULL COMMENT 'exam_setup_detail',
  `ep_id` int(11) DEFAULT NULL COMMENT 'qbs_exam_paper',
  `qesm_id` int(11) DEFAULT NULL,
  `qes_id` int(11) DEFAULT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `url` mediumtext COLLATE utf8_unicode_ci,
  `ipaddress` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `qbs_taxanomy`;
CREATE TABLE `qbs_taxanomy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxanomy_id` int(11) NOT NULL COMMENT '1=bloom,2=difficulty',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_taxanomy_level`;
CREATE TABLE `qbs_taxanomy_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxanomy_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `group` int(11) DEFAULT NULL,
  `level_id` int(5) NOT NULL,
  `status` int(11) DEFAULT NULL COMMENT 'null: Active 1:inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qbs_tos`;
CREATE TABLE `qbs_tos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdExamMain` int(11) DEFAULT NULL COMMENT 'qbs_exam_setup_main',
  `IdExam` int(11) DEFAULT NULL COMMENT 'qbs_exam_setup',
  `component_parent_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `course_id` varchar(50) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `taxanomy_level_id` int(11) DEFAULT NULL COMMENT 'qbs_taxanomy',
  `value` int(10) DEFAULT '0',
  `createddt` datetime DEFAULT NULL,
  `createdby` varchar(50) DEFAULT NULL,
  `modifyby` varchar(50) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`),
  KEY `component_id` (`component_id`),
  KEY `topic_id` (`topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qbs_users_track`;
CREATE TABLE `qbs_users_track` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actionby` varchar(50) NOT NULL,
  `actiondate` datetime NOT NULL,
  `description` varchar(200) NOT NULL,
  `ipaddress` varchar(30) DEFAULT NULL,
  `savepath` text,
  `epid` varchar(20) DEFAULT NULL COMMENT 'qbs_exam_paper',
  `IdSchedule` int(11) DEFAULT NULL COMMENT 'exam_schedule',
  `IdScheduleTaggingSlot` int(11) DEFAULT NULL,
  `courseid` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS `qp_event`;
CREATE TABLE `qp_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCourseTaggingGroup` int(11) NOT NULL DEFAULT '0',
  `sc_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `venue` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `all_day` smallint(6) DEFAULT NULL,
  `start_day` int(11) DEFAULT NULL,
  `end_day` int(11) DEFAULT NULL,
  `start_time` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_time` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_color` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `border_color` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_color` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` int(11) DEFAULT NULL,
  `last_updated` int(11) DEFAULT NULL,
  `access` int(11) DEFAULT NULL,
  `category` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `repeat` int(11) DEFAULT NULL,
  `program_mode` int(11) DEFAULT NULL,
  `course_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `merge_group` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_size` int(11) DEFAULT NULL,
  `class_duration` int(11) DEFAULT NULL,
  `lecturer_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lecturer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lecturer_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_number` int(11) DEFAULT NULL,
  `class_date` int(11) DEFAULT NULL,
  `class_day` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_start_time` int(11) DEFAULT NULL,
  `class_end_time` int(11) DEFAULT NULL,
  `in_workload` int(11) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `tag1_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag1_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag2_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag2_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag3_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag3_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag4_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag4_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag5_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag5_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag6_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag6_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag7_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag7_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `export_date` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `term` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_change1` int(11) DEFAULT NULL,
  `field_change2` int(11) DEFAULT NULL,
  `field_change3` int(11) DEFAULT NULL,
  `field_change4` int(11) DEFAULT NULL,
  `field_change5` int(11) DEFAULT NULL,
  `field_change6` int(11) DEFAULT NULL,
  `field_change7` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `term` (`term`),
  KEY `class_type` (`class_type`(191)),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qp_event_history`;
CREATE TABLE `qp_event_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `venue` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_day` int(11) DEFAULT NULL,
  `end_day` int(11) DEFAULT NULL,
  `start_time` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_time` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` int(11) DEFAULT NULL,
  `last_updated` int(11) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_duration` int(11) DEFAULT NULL,
  `lecturer_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lecturer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lecturer_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_date` int(11) DEFAULT NULL,
  `class_day` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_start_time` int(11) DEFAULT NULL,
  `class_end_time` int(11) DEFAULT NULL,
  `sequence_no` int(11) DEFAULT NULL,
  `field_change1` int(11) DEFAULT NULL,
  `field_change2` int(11) DEFAULT NULL,
  `field_change3` int(11) DEFAULT NULL,
  `field_change4` int(11) DEFAULT NULL,
  `field_change5` int(11) DEFAULT NULL,
  `field_change6` int(11) DEFAULT NULL,
  `field_change7` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qualificationmaster`;
CREATE TABLE `qualificationmaster` (
  `IdQualification` int(11) NOT NULL AUTO_INCREMENT,
  `QualificationLevel` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `QualificationRank` int(10) NOT NULL,
  `Active` int(10) NOT NULL DEFAULT '0',
  `SpecialTreatment` int(10) DEFAULT '0',
  `UpdUser` int(11) NOT NULL,
  `UpdDate` int(11) NOT NULL,
  PRIMARY KEY (`IdQualification`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `qualification_subject_mapping`;
CREATE TABLE `qualification_subject_mapping` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdQualification` bigint(20) NOT NULL,
  `IdSubject` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `radio_option_list`;
CREATE TABLE `radio_option_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question_tagging_id` int(11) unsigned NOT NULL COMMENT 'PK q003_questiontagging',
  `main_set_id` int(11) unsigned NOT NULL,
  `section_id` int(11) unsigned NOT NULL,
  `answer` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(3) unsigned NOT NULL,
  `DatetimeInsert` datetime NOT NULL,
  `InsertBy` int(11) unsigned NOT NULL,
  `DatetimeUpdate` datetime NOT NULL,
  `UpdateBy` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_radio_option_list_q003_questiontagging` (`question_tagging_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `raw_email`;
CREATE TABLE `raw_email` (
  `appid` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`appid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `raw_staff`;
CREATE TABLE `raw_staff` (
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nostaff` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add1` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add2` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel1` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel2` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `race` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `religion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mstatus` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icno` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dept` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reportto` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `regdate` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expdate` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faculty` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cstatus` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `receipt`;
CREATE TABLE `receipt` (
  `rcp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rcp_no` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rcp_account_code` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_account_code',
  `rcp_date` datetime NOT NULL,
  `rcp_receive_date` date NOT NULL,
  `rcp_payee_type` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_definationms',
  `rcp_corporate_id` bigint(20) DEFAULT NULL COMMENT 'FK tbl_takafuloperator',
  `rcp_idStudentRegistration` bigint(20) DEFAULT NULL COMMENT 'FK tbl_studentregistration',
  `rcp_description` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rcp_amount` decimal(20,2) NOT NULL,
  `rcp_amount_default_currency` decimal(20,2) NOT NULL DEFAULT '0.00',
  `rcp_adv_payment_amt` decimal(20,2) DEFAULT NULL,
  `rcp_adv_payment_amt_default_currency` decimal(20,2) DEFAULT NULL,
  `rcp_gainloss` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rcp_gainloss_amt` decimal(20,2) DEFAULT NULL,
  `rcp_status` enum('ENTRY','APPROVE','CANCEL') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ENTRY',
  `rcp_cur_id` int(11) NOT NULL COMMENT 'fk tbl_currency',
  `rcp_create_by` int(11) NOT NULL DEFAULT '1' COMMENT 'FK tbl_user',
  `migs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'PK transaction_migs',
  `fpx_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK fpx_transaction',
  `cancel_by` int(11) DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `CountRecivableadjustment` int(11) DEFAULT '0',
  `adjustment_id` int(11) NOT NULL DEFAULT '0',
  `UpdUser` int(11) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `rcp_from` int(11) NOT NULL DEFAULT '0' COMMENT '0=sms, 1=corporate, 2=lms',
  `exchange_rate` int(11) NOT NULL DEFAULT '0' COMMENT 'tbl_currency_rate',
  `MigrateCode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upl_id` int(11) DEFAULT NULL COMMENT 'tbl_corporate_upload',
  `manual` int(11) DEFAULT NULL COMMENT '1=yes, 0=no',
  PRIMARY KEY (`rcp_id`),
  KEY `rcp_no` (`rcp_no`,`rcp_payee_type`),
  KEY `rcp_trans_id` (`rcp_corporate_id`),
  KEY `rcp_idStudentRegistration` (`rcp_idStudentRegistration`),
  KEY `rcp_status` (`rcp_status`),
  KEY `rcp_cur_id` (`rcp_cur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `receipt_invoice`;
CREATE TABLE `receipt_invoice` (
  `rcp_inv_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rcp_inv_rcp_id` bigint(20) NOT NULL COMMENT 'FK receipt',
  `rcp_inv_rcp_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'FK receipt',
  `rcp_inv_invoice_id` bigint(20) NOT NULL COMMENT 'FK invoice_main',
  `rcp_inv_invoice_dtl_id` bigint(20) NOT NULL COMMENT 'FK invoice_detail',
  `rcp_inv_amount` decimal(20,2) NOT NULL COMMENT 'FK invoice_detail',
  `rcp_inv_amount_default_currency` decimal(20,2) NOT NULL DEFAULT '0.00',
  `rcp_inv_cur_id` bigint(11) NOT NULL COMMENT 'FK tbl_currency',
  `rcp_inv_create_date` datetime NOT NULL,
  `rcp_inv_create_by` bigint(20) NOT NULL COMMENT 'FK tbl_users',
  `rcp_inv_invoice_std_id` int(11) DEFAULT '0',
  `rcp_inv_payment_id` int(11) DEFAULT '0',
  PRIMARY KEY (`rcp_inv_id`),
  KEY `rcp_inv_rcp_id` (`rcp_inv_rcp_id`,`rcp_inv_rcp_no`,`rcp_inv_invoice_id`,`rcp_inv_invoice_dtl_id`),
  KEY `rcp_inv_cur_id` (`rcp_inv_cur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `refund`;
CREATE TABLE `refund` (
  `rfd_id` int(11) NOT NULL AUTO_INCREMENT,
  `rfd_appl_id` int(11) DEFAULT NULL,
  `rfd_trans_id` int(11) DEFAULT NULL,
  `rfd_corporate_id` int(11) DEFAULT '0',
  `rfd_IdStudentRegistration` int(11) NOT NULL,
  `rfd_acad_year_id` bigint(11) NOT NULL,
  `rfd_sem_id` bigint(11) DEFAULT NULL,
  `rfd_fomulir` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rfd_desc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `rfd_process_type` int(11) NOT NULL,
  `rfd_type` int(11) NOT NULL,
  `rfd_amount` decimal(20,2) NOT NULL,
  `rfd_currency_id` int(11) NOT NULL,
  `rfd_exchange_rate` int(11) NOT NULL,
  `rdf_refund_cheque_id` bigint(20) DEFAULT NULL,
  `rfd_creator` bigint(11) NOT NULL,
  `rfd_date` date NOT NULL,
  `rfd_create_date` datetime NOT NULL,
  `rfd_avdpy_id` bigint(11) DEFAULT NULL,
  `rfd_bank_acc` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rfd_approver_id` bigint(20) DEFAULT NULL,
  `rfd_approve_date` datetime DEFAULT NULL,
  `rfd_cancel_by` int(11) NOT NULL,
  `rfd_cancel_date` datetime NOT NULL,
  `rfd_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'A:active X: cancel',
  `rfd_pay_to` int(11) NOT NULL COMMENT '2=student, 1=applicant, 3 = sponsor ',
  `rfd_pay_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`rfd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `refund_cheque`;
CREATE TABLE `refund_cheque` (
  `rchq_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rchq_rfd_id` int(11) NOT NULL,
  `rchq_cheque_no` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rchq_bank_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rchq_amount` decimal(10,2) NOT NULL,
  `rchq_issue_date` date NOT NULL,
  `rchq_insert_date` datetime NOT NULL,
  `rchq_insert_by` bigint(20) NOT NULL,
  `rchq_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `rchq_cancel_by` bigint(20) DEFAULT NULL,
  `rchq_cancel_date` datetime DEFAULT NULL,
  `rchq_collector_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rchq_collector_date` datetime DEFAULT NULL,
  `rchq_collector_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rchq_collector_id_type` int(11) DEFAULT NULL,
  `rchq_collector_update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`rchq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `refund_detail`;
CREATE TABLE `refund_detail` (
  `rfdd_id` int(11) NOT NULL AUTO_INCREMENT,
  `rfdd_refund_id` bigint(20) NOT NULL,
  `rfdd_adv_id` int(11) NOT NULL,
  `rfdd_advpydet_id` bigint(20) NOT NULL,
  `rfdd_amount` decimal(20,2) NOT NULL,
  PRIMARY KEY (`rfdd_id`),
  KEY `rfdd_refund_id` (`rfdd_refund_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `school_decipline_subject`;
CREATE TABLE `school_decipline_subject` (
  `sds_id` int(11) NOT NULL AUTO_INCREMENT,
  `sds_discipline_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk school_discipline(smj_code)',
  `sds_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`sds_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `school_discipline`;
CREATE TABLE `school_discipline` (
  `smd_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smd_desc` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smd_school_type` int(11) NOT NULL COMMENT 'school_type(st_id)',
  PRIMARY KEY (`smd_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `school_master`;
CREATE TABLE `school_master` (
  `sm_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sm_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sm_name_bahasa` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sm_address1` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sm_address2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_city` int(20) NOT NULL,
  `sm_state` int(20) unsigned NOT NULL,
  `sm_country` int(20) unsigned NOT NULL,
  `sm_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_url` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_status` tinyint(1) NOT NULL DEFAULT '1',
  `sm_create_date` datetime NOT NULL,
  `sm_create_by` bigint(20) unsigned NOT NULL,
  `sm_phone_o` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_school_status` bigint(20) unsigned DEFAULT NULL,
  `sm_school_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sm_id`),
  KEY `sm_state` (`sm_state`),
  KEY `sm_name` (`sm_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `school_subject`;
CREATE TABLE `school_subject` (
  `ss_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ss_subject_bahasa` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ss_core_subject` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 core subj ',
  PRIMARY KEY (`ss_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `school_type`;
CREATE TABLE `school_type` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `st_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `st_name_bahasa` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`st_code`),
  UNIQUE KEY `sl_id` (`st_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `semesterstatuscorrection`;
CREATE TABLE `semesterstatuscorrection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semesterstatus` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `studentid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registrationid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `semester_activity_schedule`;
CREATE TABLE `semester_activity_schedule` (
  `sas_id` int(11) NOT NULL AUTO_INCREMENT,
  `sas_sem_id` int(11) NOT NULL,
  `sas_activity_id` int(11) NOT NULL,
  `sas_start_date` date DEFAULT NULL,
  `sas_end_date` date DEFAULT NULL,
  `sas_activity_date` date DEFAULT NULL,
  PRIMARY KEY (`sas_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `seq`;
CREATE TABLE `seq` (
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `val` int(10) unsigned NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sequence`;
CREATE TABLE `sequence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '0' COMMENT '1:PIV; 2:IV; 3:DN; 4:CN; 5:RCP',
  `year` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `intake` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `program_code` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `student_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `field_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seq` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `year_field_name` (`year`,`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sis_language`;
CREATE TABLE `sis_language` (
  `sl_id` int(11) NOT NULL AUTO_INCREMENT,
  `sl_var_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sl_english` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sl_bahasa` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `Language` int(11) DEFAULT NULL,
  PRIMARY KEY (`sl_id`),
  UNIQUE KEY `sl_var_name` (`sl_var_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sis_setup_detl`;
CREATE TABLE `sis_setup_detl` (
  `ssd_id` int(11) NOT NULL AUTO_INCREMENT,
  `ssd_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ssd_seq` int(11) NOT NULL,
  `ssd_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ssd_name_bahasa` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ssd_id`),
  KEY `sis_setup_detl_fk1` (`ssd_code`),
  CONSTRAINT `sis_setup_detl_ibfk_1` FOREIGN KEY (`ssd_code`) REFERENCES `sis_setup_head` (`ssh_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sis_setup_head`;
CREATE TABLE `sis_setup_head` (
  `ssh_id` int(11) NOT NULL AUTO_INCREMENT,
  `ssh_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ssh_desc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ssh_code`),
  UNIQUE KEY `ssh_id` (`ssh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `spmbraw`;
CREATE TABLE `spmbraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nopes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mutu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prog` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appl_id` int(11) NOT NULL,
  `transid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `spmbraw1`;
CREATE TABLE `spmbraw1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nopes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mutu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prog` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appl_id` int(11) NOT NULL,
  `transid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sponsor_application`;
CREATE TABLE `sponsor_application` (
  `sa_id` int(11) NOT NULL AUTO_INCREMENT,
  `sa_af_id` int(11) NOT NULL COMMENT 'fk applicant_financial',
  `sa_scholarship_type` int(11) DEFAULT NULL,
  `sa_cust_id` int(11) NOT NULL COMMENT 'Reflect to cust type (1:fk tbl_studentregistration 2:applicant_transcation)',
  `sa_cust_type` int(11) NOT NULL COMMENT '1:Student 2:applicant 3:admin',
  `sa_status` int(11) NOT NULL COMMENT '1: new /applied 2:approve 3: reject ',
  `sa_semester_id` int(11) DEFAULT NULL,
  `sa_score` float DEFAULT NULL,
  `sa_validated_doc` tinyint(4) DEFAULT NULL,
  `sa_createddt` datetime NOT NULL,
  `sa_createdby` int(11) NOT NULL,
  PRIMARY KEY (`sa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sponsor_application_details`;
CREATE TABLE `sponsor_application_details` (
  `id_detail` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL,
  `student_id` int(10) unsigned NOT NULL,
  `education_id` int(10) unsigned DEFAULT NULL,
  `education_result` decimal(10,2) unsigned DEFAULT NULL,
  `totaldependent` smallint(10) unsigned DEFAULT NULL,
  `empstatus` smallint(10) unsigned DEFAULT NULL,
  `industry` smallint(10) unsigned DEFAULT NULL,
  `income` decimal(12,2) DEFAULT NULL,
  `compaddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `compemail` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_year` year(4) DEFAULT NULL,
  `semester` smallint(5) unsigned DEFAULT NULL,
  `scholarship_score` decimal(10,2) unsigned DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sponsor_application_upload`;
CREATE TABLE `sponsor_application_upload` (
  `sau_id` int(11) NOT NULL AUTO_INCREMENT,
  `sau_sa_id` int(11) NOT NULL,
  `sau_filename` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sau_filerename` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sau_filelocation` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sau_filesize` int(11) NOT NULL,
  `sau_mimetype` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sau_upddate` date NOT NULL,
  `sau_upduser` int(11) NOT NULL,
  PRIMARY KEY (`sau_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sponsor_invoice_batch`;
CREATE TABLE `sponsor_invoice_batch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `batch_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `semester_id` int(11) NOT NULL,
  `sp_type` int(11) NOT NULL COMMENT '1=sponsorship, 2=scholarship',
  `sp_type_id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `total_student` int(10) unsigned NOT NULL,
  `approval` int(11) NOT NULL COMMENT '1=approved, 0=entry',
  `currency_id` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `paid` decimal(20,2) NOT NULL,
  `balance` decimal(20,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `semester_id` (`semester_id`),
  KEY `created_by` (`created_by`),
  KEY `dcnt_id` (`sp_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sponsor_invoice_detail`;
CREATE TABLE `sponsor_invoice_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sp_id` bigint(20) NOT NULL COMMENT 'sponsor_invoice_main',
  `sp_amount` decimal(20,2) NOT NULL,
  `sp_paid` decimal(20,2) NOT NULL,
  `sp_balance` decimal(20,2) NOT NULL,
  `sp_receipt` decimal(20,2) NOT NULL,
  `sp_invoice_id` bigint(20) DEFAULT NULL,
  `sp_invoice_det_id` int(11) NOT NULL,
  `sp_description` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `dcnt_id` (`sp_id`),
  KEY `dcnt_invoice_id` (`sp_invoice_id`),
  KEY `dcnt_invoice_det_id` (`sp_invoice_det_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sponsor_invoice_main`;
CREATE TABLE `sponsor_invoice_main` (
  `sp_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_txn_id` bigint(20) NOT NULL COMMENT 'fk_applicant_transaction',
  `sp_IdStudentRegistration` int(11) NOT NULL,
  `sp_fomulir_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sp_type` int(11) NOT NULL COMMENT '1=sponsorship, 2=scholarship',
  `sp_type_id` int(11) NOT NULL COMMENT 'PK scholar/sponsor',
  `sp_amount` decimal(20,2) DEFAULT NULL,
  `sp_paid` decimal(20,2) DEFAULT '0.00',
  `sp_balance` decimal(20,2) DEFAULT NULL,
  `sp_currency_id` int(11) NOT NULL,
  `sp_exchange_rate` int(11) NOT NULL,
  `sp_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sp_invoice_date` date NOT NULL,
  `sp_creator` int(11) NOT NULL,
  `sp_create_date` datetime NOT NULL,
  `sp_upd_date` datetime NOT NULL,
  `sp_upd_id` int(11) NOT NULL,
  `sp_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sp_batch_id` int(11) NOT NULL,
  `sp_approve_by` int(11) NOT NULL,
  `sp_approve_date` datetime NOT NULL,
  `sp_cancel_by` int(11) NOT NULL,
  `sp_cancel_date` datetime NOT NULL,
  PRIMARY KEY (`sp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sponsor_invoice_receipt`;
CREATE TABLE `sponsor_invoice_receipt` (
  `rcp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rcp_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rcp_account_code` int(11) NOT NULL COMMENT 'FK tbl_account_code',
  `rcp_date` datetime NOT NULL,
  `rcp_receive_date` date NOT NULL,
  `rcp_batch_id` bigint(20) NOT NULL COMMENT 'sponsor_invoice_batch',
  `rcp_type` bigint(20) DEFAULT NULL,
  `rcp_type_id` bigint(20) DEFAULT NULL,
  `rcp_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rcp_amount` decimal(20,2) NOT NULL,
  `rcp_gainloss` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rcp_gainloss_amt` decimal(20,2) DEFAULT NULL,
  `rcp_status` enum('ENTRY','APPROVE','CANCEL') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ENTRY',
  `rcp_cur_id` int(11) NOT NULL COMMENT 'fk tbl_currency',
  `rcp_exchange_rate` int(11) NOT NULL,
  `p_payment_mode_id` int(11) NOT NULL,
  `p_cheque_no` int(150) DEFAULT NULL,
  `p_doc_bank` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_doc_branch` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_terminal_id` int(11) DEFAULT NULL,
  `p_card_no` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rcp_create_by` int(11) NOT NULL DEFAULT '1' COMMENT 'FK tbl_user',
  `cancel_by` int(11) NOT NULL,
  `cancel_date` datetime NOT NULL,
  `UpdUser` int(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`rcp_id`),
  KEY `rcp_no` (`rcp_no`),
  KEY `rcp_trans_id` (`rcp_type`),
  KEY `rcp_idStudentRegistration` (`rcp_type_id`),
  KEY `rcp_status` (`rcp_status`),
  KEY `rcp_payee_id` (`rcp_batch_id`),
  KEY `rcp_cur_id` (`rcp_cur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sponsor_invoice_receipt_invoice`;
CREATE TABLE `sponsor_invoice_receipt_invoice` (
  `rcp_inv_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rcp_inv_rcp_id` bigint(20) NOT NULL COMMENT 'sponsor_invoice_receipt',
  `rcp_inv_student` int(11) NOT NULL COMMENT 'sponsor_invoice_receipt_student',
  `rcp_inv_sp_id` bigint(20) NOT NULL COMMENT 'sponsor_invoice_main',
  `rcp_inv_sponsor_dtl_id` bigint(20) NOT NULL COMMENT 'sponsor_invoice_detail',
  `rcp_inv_amount` decimal(20,2) NOT NULL COMMENT 'FK invoice_detail',
  `rcp_inv_cur_id` bigint(11) NOT NULL COMMENT 'FK tbl_currency',
  `rcp_inv_create_date` datetime NOT NULL,
  `rcp_inv_create_by` bigint(20) NOT NULL COMMENT 'FK tbl_users',
  PRIMARY KEY (`rcp_inv_id`),
  KEY `rcp_inv_rcp_id` (`rcp_inv_rcp_id`,`rcp_inv_sp_id`,`rcp_inv_sponsor_dtl_id`),
  KEY `rcp_inv_cur_id` (`rcp_inv_cur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sponsor_invoice_receipt_student`;
CREATE TABLE `sponsor_invoice_receipt_student` (
  `rcp_inv_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rcp_inv_rcp_id` bigint(20) NOT NULL COMMENT 'sponsor_invoice_receipt',
  `IdStudentRegistration` int(11) NOT NULL,
  `rcp_inv_amount` decimal(20,2) NOT NULL COMMENT 'FK invoice_detail',
  `rcp_adv_amount` decimal(20,2) NOT NULL,
  `rcp_inv_cur_id` bigint(11) NOT NULL COMMENT 'FK tbl_currency',
  `rcp_inv_create_date` datetime NOT NULL,
  `rcp_inv_create_by` bigint(20) NOT NULL COMMENT 'FK tbl_users',
  PRIMARY KEY (`rcp_inv_id`),
  KEY `rcp_inv_rcp_id` (`rcp_inv_rcp_id`,`IdStudentRegistration`),
  KEY `rcp_inv_cur_id` (`rcp_inv_cur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `studentregsubjects_history`;
CREATE TABLE `studentregsubjects_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegSubjects` bigint(20) unsigned NOT NULL,
  `IdStudentRegistration` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentregistration',
  `initial` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subject',
  `SubjectsApproved` binary(1) NOT NULL DEFAULT '0',
  `SubjectsApprovedComments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdSemesterMain` bigint(20) DEFAULT NULL,
  `IdSemesterDetails` bigint(20) DEFAULT NULL,
  `SemesterLevel` tinyint(4) DEFAULT NULL COMMENT 'level/sort semester',
  `IdLandscapeSub` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_landscapesubject',
  `IdBlock` int(11) DEFAULT NULL COMMENT 'fk tbl_blocklandscape',
  `BlockLevel` tinyint(4) DEFAULT NULL COMMENT 'level/sort block',
  `IdGrade` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime NOT NULL,
  `UpdRole` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` tinyint(4) DEFAULT NULL COMMENT '0:pre-register 1:Register  2: Add&Drop 3:Withdraw 4: Repeat 5:Refer',
  `exam_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'C: Completed   IN:Incomplete  MG:Missing Grade F:Fraud NR:No Record',
  `exam_status_updateby` int(11) DEFAULT NULL,
  `exam_status_updatedt` datetime DEFAULT NULL,
  `final_course_mark` decimal(10,2) DEFAULT NULL COMMENT '80',
  `grade_point` decimal(10,2) DEFAULT NULL COMMENT '3.00',
  `grade_name` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'A',
  `grade_desc` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Baik,Sangat Baik etc',
  `grade_status` enum('Pass','Fail') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Pass/Fail',
  `mark_approveby` int(11) DEFAULT NULL COMMENT 'tbl_staffmaster',
  `mark_approvedt` datetime DEFAULT NULL,
  `IdCourseTaggingGroup` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_course_tagging_group',
  `subjectlandscapetype` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Value ambil dari tbl_landscapeblocksubject field type',
  `attendance` decimal(10,2) DEFAULT NULL,
  `hdate` datetime NOT NULL,
  `huser` int(11) NOT NULL,
  `hactivity` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IdSubject` (`IdSubject`),
  KEY `UpdUser` (`UpdUser`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`),
  KEY `IdSemesterMain` (`IdSemesterMain`),
  KEY `IdCourseTaggingGroup` (`IdCourseTaggingGroup`),
  KEY `Active` (`Active`),
  KEY `exam_status` (`exam_status`),
  KEY `grade_name` (`grade_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_accomodation`;
CREATE TABLE `student_accomodation` (
  `acd_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `acd_trans_id` bigint(20) NOT NULL,
  `acd_assistance` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: no 1:yes',
  `acd_type` int(11) NOT NULL DEFAULT '0',
  `acd_updateby` int(11) NOT NULL,
  `acd_updatedt` datetime NOT NULL,
  PRIMARY KEY (`acd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_ansscheme`;
CREATE TABLE `student_ansscheme` (
  `sas_id` int(11) NOT NULL AUTO_INCREMENT,
  `sas_IdMarksDistributionMaster` bigint(20) NOT NULL,
  `sas_IdMarksDistributionDetails` bigint(20) DEFAULT NULL,
  `sas_set_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sas_total_quest` int(11) NOT NULL,
  `sas_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active 0:inactive',
  PRIMARY KEY (`sas_id`),
  KEY `sas_IdMarksDistributionMaster` (`sas_IdMarksDistributionMaster`),
  KEY `sas_IdMarksDistributionDetails` (`sas_IdMarksDistributionDetails`),
  KEY `sas_id` (`sas_id`),
  KEY `sas_IdMarksDistributionMaster_2` (`sas_IdMarksDistributionMaster`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_ansscheme_detl`;
CREATE TABLE `student_ansscheme_detl` (
  `sad_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `sad_anscheme_id` int(11) NOT NULL,
  `sad_ques_no` int(5) NOT NULL,
  `sad_ques_ans` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`sad_anscheme_id`,`sad_ques_no`),
  UNIQUE KEY `sad_id` (`sad_id`),
  KEY `sad_anscheme_id` (`sad_anscheme_id`),
  KEY `sad_id_2` (`sad_id`),
  KEY `sad_anscheme_id_2` (`sad_anscheme_id`),
  KEY `sad_ques_no` (`sad_ques_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_attribute`;
CREATE TABLE `student_attribute` (
  `sa_id` int(11) NOT NULL AUTO_INCREMENT,
  `sa_sp_id` int(11) NOT NULL,
  `sa_appl_id` int(11) NOT NULL,
  `sa_trans_id` int(11) NOT NULL,
  `sa_type` int(11) NOT NULL,
  `sa_category` int(11) NOT NULL,
  `sa_rating` int(11) NOT NULL,
  `sa_updDate` datetime NOT NULL,
  `sa_updUser` int(11) NOT NULL,
  PRIMARY KEY (`sa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_comment`;
CREATE TABLE `student_comment` (
  `scm_id` int(11) NOT NULL AUTO_INCREMENT,
  `scm_sp_id` int(11) NOT NULL,
  `scm_appl_id` int(11) NOT NULL,
  `scm_trans_id` smallint(11) NOT NULL,
  `scm_type` int(11) NOT NULL,
  `scm_desc` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scm_date` datetime NOT NULL,
  `scm_user` int(11) NOT NULL,
  PRIMARY KEY (`scm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_curricular_activities`;
CREATE TABLE `student_curricular_activities` (
  `ca_id` int(11) NOT NULL AUTO_INCREMENT,
  `ca_sp_id` int(11) NOT NULL,
  `ca_appl_id` int(11) NOT NULL,
  `ca_trans_id` int(11) NOT NULL,
  `ca_seq` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ca_desc` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ca_date` date NOT NULL,
  PRIMARY KEY (`ca_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_dependent_pass`;
CREATE TABLE `student_dependent_pass` (
  `sdp_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL COMMENT 'fk student_profile',
  `p_id` int(11) NOT NULL COMMENT 'fk student_passport',
  `av_id` int(11) NOT NULL COMMENT 'fk student_visa',
  `sdp_relationship` int(11) NOT NULL,
  `sdp_passport_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdp_country_issue` int(11) NOT NULL,
  `sdp_date_issue` date NOT NULL,
  `sdp_dob` date NOT NULL,
  `sdp_nationality` int(11) NOT NULL,
  `sdp_expiry_date` date NOT NULL,
  `sdp_active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active 1: Active',
  `sdp_name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdp_createddt` datetime NOT NULL,
  `sdp_createdby` int(11) NOT NULL,
  PRIMARY KEY (`sdp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_documents`;
CREATE TABLE `student_documents` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `ad_dcl_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_documentchecklist_dcl',
  `ad_ads_id` int(11) NOT NULL,
  `ad_section_id` int(11) DEFAULT '0',
  `ad_type` int(11) NOT NULL DEFAULT '0',
  `ad_table_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'name of table that reflect ad_table_id',
  `ad_table_id` int(11) DEFAULT '0',
  `ad_filepath` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_filename` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_ori_filename` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ad_createddt` date NOT NULL,
  `ad_createdby` int(11) NOT NULL,
  `ad_role` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ad_filetype` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ad_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_emergency_contact`;
CREATE TABLE `student_emergency_contact` (
  `ec_id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_sp_id` int(11) NOT NULL,
  `ec_appl_id` int(11) NOT NULL,
  `ec_trans_id` int(11) NOT NULL,
  `ec_type` int(11) NOT NULL COMMENT 'fk to tbl_definationms',
  `ec_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_add1` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_add2` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_add3` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_country` int(11) NOT NULL,
  `ec_state` int(11) NOT NULL,
  `ec_city` int(11) NOT NULL,
  `ec_state_others` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_city_others` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_postcode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_phone_mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_phone_home` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_phone_office` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ec_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_employment`;
CREATE TABLE `student_employment` (
  `ae_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `ae_appl_id` int(11) NOT NULL,
  `ae_trans_id` int(11) NOT NULL,
  `ae_status` int(11) NOT NULL COMMENT 'Employment Status',
  `ae_comp_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_comp_address` longtext COLLATE utf8mb4_unicode_ci,
  `ae_comp_phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_comp_fax` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_position` int(11) DEFAULT NULL COMMENT 'position level',
  `ae_from` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_to` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emply_year_service` int(11) DEFAULT NULL,
  `ae_industry` int(11) DEFAULT NULL COMMENT 'Industry Working Experience',
  `ae_job_desc` longtext COLLATE utf8mb4_unicode_ci,
  `ae_income` float DEFAULT NULL,
  `ae_industry_type` int(11) DEFAULT NULL,
  `ae_is_current` int(11) DEFAULT NULL,
  `ae_order` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `upd_by` int(11) DEFAULT NULL,
  `ae_role` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ae_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_english_proficiency`;
CREATE TABLE `student_english_proficiency` (
  `ep_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `ep_transaction_id` int(11) NOT NULL,
  `ep_test` int(11) NOT NULL,
  `ep_test_detail` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ep_date_taken` date NOT NULL,
  `ep_score` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ep_updatedt` datetime NOT NULL,
  `ep_updateby` int(11) NOT NULL,
  PRIMARY KEY (`ep_id`),
  UNIQUE KEY `ep_id` (`ep_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_essay`;
CREATE TABLE `student_essay` (
  `e_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `e_trans_id` int(10) unsigned NOT NULL,
  `e_file` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_updatedt` datetime NOT NULL,
  `e_updateby` int(11) NOT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_financial`;
CREATE TABLE `student_financial` (
  `af_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `af_appl_id` int(11) NOT NULL,
  `af_trans_id` int(11) NOT NULL,
  `af_method` int(11) NOT NULL,
  `af_sponsor_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_type_scholarship` int(11) DEFAULT NULL,
  `af_scholarship_secured` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_scholarship_apply` int(11) DEFAULT NULL,
  `af_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upd_date` int(11) NOT NULL,
  PRIMARY KEY (`af_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_health_condition`;
CREATE TABLE `student_health_condition` (
  `ah_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `ah_appl_id` int(11) NOT NULL,
  `ah_trans_id` int(11) NOT NULL,
  `ah_status` int(11) NOT NULL,
  `upd_date` datetime NOT NULL,
  `ah_others` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ah_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_mark_raw`;
CREATE TABLE `student_mark_raw` (
  `tmp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tmp_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_set_code` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_answers_raw` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`tmp_id`),
  KEY `tmp_set_code` (`tmp_set_code`),
  KEY `tmp_file` (`tmp_file`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_omr_answers`;
CREATE TABLE `student_omr_answers` (
  `soa_id` int(11) NOT NULL AUTO_INCREMENT,
  `soa_IdStudentRegistration` int(11) NOT NULL,
  `soa_exam_groupid` int(11) NOT NULL COMMENT 'fk exam_group',
  `soa_IdMarksDistributionMaster` int(11) NOT NULL,
  `soa_IdMarksDistributionDetails` int(11) NOT NULL,
  `soa_set_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `soa_createddt` datetime NOT NULL,
  `soa_createdby` bigint(20) NOT NULL,
  `soa_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`soa_id`),
  KEY `soa_id` (`soa_id`),
  KEY `soa_IdStudentRegistration` (`soa_IdStudentRegistration`),
  KEY `soa_exam_groupid` (`soa_exam_groupid`),
  KEY `soa_IdStudentRegistration_2` (`soa_IdStudentRegistration`),
  KEY `soa_id_2` (`soa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_omr_ans_detl`;
CREATE TABLE `student_omr_ans_detl` (
  `soad_id` int(11) NOT NULL AUTO_INCREMENT,
  `soad_soa_id` int(11) NOT NULL COMMENT 'fk student_omr_answer)',
  `soad_ques_no` int(5) NOT NULL,
  `soad_student_ans` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `soad_status_ans` tinyint(4) NOT NULL COMMENT '1 betul 0 salah',
  PRIMARY KEY (`soad_id`),
  KEY `apad_apa_id` (`soad_soa_id`),
  KEY `apad_status_ans` (`soad_status_ans`),
  KEY `soad_soa_id` (`soad_soa_id`),
  KEY `soad_id` (`soad_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_otherdocument`;
CREATE TABLE `student_otherdocument` (
  `sod_id` int(11) NOT NULL AUTO_INCREMENT,
  `sod_spid` int(11) NOT NULL DEFAULT '0',
  `sod_type` int(11) NOT NULL DEFAULT '0',
  `sod_remarks` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sod_createby` int(11) NOT NULL,
  `sod_createdate` date NOT NULL,
  `sod_updby` int(11) NOT NULL,
  `sod_upddate` date NOT NULL,
  PRIMARY KEY (`sod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_otherdocument_upload`;
CREATE TABLE `student_otherdocument_upload` (
  `sou_id` int(11) NOT NULL AUTO_INCREMENT,
  `sou_sodid` int(11) NOT NULL DEFAULT '0',
  `sou_filename` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sou_filerename` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sou_filelocation` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sou_filesize` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sou_mimetype` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sou_updby` int(11) NOT NULL DEFAULT '0',
  `sou_upddate` date DEFAULT NULL,
  PRIMARY KEY (`sou_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_passport`;
CREATE TABLE `student_passport` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL COMMENT 'fk student_profile',
  `p_visa_type` int(11) DEFAULT NULL,
  `p_type` int(11) DEFAULT NULL,
  `p_passport_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_country_issue` int(11) DEFAULT NULL COMMENT 'fk tbl countries',
  `p_date_issue` date DEFAULT NULL,
  `p_prev_passport` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'prev passport no',
  `p_expiry_date` date DEFAULT NULL,
  `p_active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active 1: Active',
  `ad_id` int(11) NOT NULL DEFAULT '0',
  `p_createddt` datetime NOT NULL,
  `p_createdby` int(11) NOT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_profile`;
CREATE TABLE `student_profile` (
  `std_id` int(11) NOT NULL AUTO_INCREMENT,
  `std_corporate_id` int(11) DEFAULT NULL,
  `spt_id` int(11) DEFAULT NULL,
  `std_username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_fullname` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_idnumber` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_dob` date DEFAULT NULL,
  `std_nationality` int(11) NOT NULL,
  `std_type_nationality` int(11) DEFAULT NULL,
  `std_gender` int(11) DEFAULT NULL COMMENT '0=Female 1=Male',
  `std_race` int(11) DEFAULT NULL,
  `std_religion` int(11) DEFAULT NULL,
  `std_estatus` int(11) DEFAULT NULL COMMENT '1-Self employed ; 2-Goverment Sector;3-Private Sector; 4-Retiree; 5-Housewife;6-Other;',
  `std_contact_number` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_email` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_qualification` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `std_department` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_designation` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_membership` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_staffno` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_location` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_region` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_company` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_address` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_address_correspondence` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_poscode` int(11) DEFAULT NULL,
  `std_city` int(11) DEFAULT NULL,
  `std_state` int(11) DEFAULT NULL,
  `std_country` int(11) DEFAULT NULL,
  `std_remark` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `btch_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `migrate_date` datetime DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_repository` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lms_migrate` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  `lms_migratedate` datetime NOT NULL,
  `tbe_migration` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1=yes,0=no ',
  `tbe_migratedate` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tbe_id` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'tbe - tbl_studentapplication ',
  `tbe_password` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'tbe - tbl_studentapplication .password',
  `edit` int(2) DEFAULT '0',
  `employer_id` int(11) DEFAULT NULL COMMENT 'pk : tbl_takafuloperator',
  PRIMARY KEY (`std_id`),
  KEY `std_corporate_id` (`std_corporate_id`),
  KEY `std_company` (`std_company`(191)),
  KEY `std_nationality` (`std_nationality`),
  KEY `btch_id` (`btch_id`),
  KEY `std_idnumber` (`std_idnumber`(191)),
  KEY `std_fullname` (`std_fullname`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_profile_history`;
CREATE TABLE `student_profile_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `std_id` int(11) DEFAULT NULL,
  `std_corporate_id` int(11) DEFAULT NULL,
  `spt_id` int(11) DEFAULT NULL,
  `std_username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_fullname` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_idnumber` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_dob` date DEFAULT NULL,
  `std_nationality` int(11) NOT NULL,
  `std_type_nationality` int(11) DEFAULT NULL,
  `std_gender` int(11) DEFAULT NULL,
  `std_race` int(11) DEFAULT NULL,
  `std_religion` int(11) DEFAULT NULL,
  `std_contact_number` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_estatus` int(11) DEFAULT NULL COMMENT '1-Self employed ; 2-Goverment Sector;3-Private Sector; 4-Retiree; 5-Housewife;6-Other;',
  `std_email` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_qualification` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `std_department` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_designation` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_membership` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_staffno` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_location` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_region` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_company` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_address` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_address_correspondence` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_poscode` int(11) DEFAULT NULL,
  `std_city` int(11) DEFAULT NULL,
  `std_state` int(11) DEFAULT NULL,
  `std_country` int(11) DEFAULT NULL,
  `std_remark` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `btch_id` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `migrate_date` datetime DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_repository` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lms_migrate` int(11) DEFAULT NULL COMMENT '	1=yes,0=no',
  `lms_migratedate` datetime NOT NULL,
  `tbe_migration` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1=yes,0=no ',
  `tbe_migratedate` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tbe_id` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'tbe - tbl_studentapplication ',
  `tbe_password` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'tbe - tbl_studentapplication .password',
  `edit` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_profile_old`;
CREATE TABLE `student_profile_old` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `appl_id` bigint(20) unsigned NOT NULL,
  `appl_salutation` int(11) unsigned DEFAULT NULL,
  `appl_fname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_mname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_lname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_idnumber` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_idnumber_type` int(11) unsigned DEFAULT NULL,
  `appl_passport_expiry` date DEFAULT NULL,
  `appl_address1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_address2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_address3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_postcode` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_city` int(10) DEFAULT NULL,
  `appl_state` int(11) DEFAULT NULL,
  `appl_city_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_state_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_country` int(11) DEFAULT NULL COMMENT 'Country',
  `appl_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_email_personal` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_username` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'later nanti nak pakai just create aje',
  `appl_password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_dob` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'dd-mm-yyyy',
  `appl_gender` tinyint(1) DEFAULT NULL COMMENT '1:  male 2: female',
  `appl_prefer_lang` int(11) NOT NULL DEFAULT '2' COMMENT 'fk applicant_languange(al_id)',
  `appl_nationality` int(11) DEFAULT NULL,
  `appl_type_nationality` int(11) DEFAULT NULL COMMENT 'tbl maintenance',
  `appl_category` int(11) NOT NULL COMMENT 'fk tbl_definations (type:)',
  `appl_admission_type` int(11) DEFAULT NULL COMMENT 'jgn pakai ni lagi guna at_appl_type dkt transaction',
  `appl_religion` int(11) DEFAULT NULL,
  `appl_religion_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_race` int(11) DEFAULT NULL,
  `appl_race_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_marital_status` int(11) DEFAULT NULL,
  `appl_bumiputera` int(11) DEFAULT NULL,
  `appl_no_of_child` int(11) DEFAULT NULL,
  `appl_phone_home` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_phone_mobile` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_phone_office` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_caddress1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'correspondence add',
  `appl_caddress2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_caddress3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_ccity` int(10) DEFAULT NULL,
  `appl_cstate` int(11) DEFAULT NULL,
  `appl_ccountry` int(11) DEFAULT NULL,
  `appl_ccity_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cstate_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cpostcode` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cphone_home` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cphone_mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cphone_office` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cfax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_contact_home` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_contact_mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_contact_office` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `change_passwordby` int(11) DEFAULT NULL,
  `change_passworddt` datetime DEFAULT NULL,
  `appl_role` smallint(5) unsigned DEFAULT NULL COMMENT '0:Applicant  1:Student',
  `fromOldSys` text COLLATE utf8mb4_unicode_ci,
  `verifyKey` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'untuk tujuan verify email',
  `create_date` datetime DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `prev_student` tinyint(4) unsigned DEFAULT '0' COMMENT '0: No 1:yes',
  `prev_txn_id` bigint(20) unsigned DEFAULT NULL COMMENT 'student_profile txn id',
  `prev_studentID` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'refer to registrationid at tbl_studentregistration',
  `branch_id` int(11) unsigned DEFAULT NULL,
  `appl_company` int(11) DEFAULT NULL,
  `appl_designation` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_department` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_default_qlevel` int(11) DEFAULT NULL COMMENT 'highest qualification',
  `appl_category_type` int(11) DEFAULT NULL COMMENT 'tbl_category',
  `appl_company_id` int(11) DEFAULT NULL COMMENT 'tbl_takafuloperator',
  `appl_membership` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userKey` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `migrate_by` int(11) unsigned DEFAULT NULL,
  `sp_repository` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_changed_by` int(11) unsigned DEFAULT NULL,
  `password_changed_date` datetime DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_email` tinyint(1) unsigned DEFAULT NULL,
  `clear_pass` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_blast` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_profile_temp`;
CREATE TABLE `student_profile_temp` (
  `std_id` int(11) NOT NULL AUTO_INCREMENT,
  `std_corporate_id` int(11) NOT NULL,
  `std_username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_fullname` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_idnumber` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_dob` date DEFAULT NULL,
  `std_nationality` int(11) DEFAULT NULL,
  `std_type_nationality` int(11) DEFAULT NULL,
  `std_gender` int(11) DEFAULT NULL,
  `std_race` int(11) DEFAULT NULL,
  `std_contact_number` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_email` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_qualification` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_department` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_designation` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_membership` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_staffno` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_location` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_region` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_company` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_remark` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `req_check` int(11) DEFAULT NULL,
  `req_error` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `upl_id` int(11) DEFAULT NULL COMMENT 'fk tbl_corporate_upload',
  `upld_id` int(11) DEFAULT NULL COMMENT 'tbl_corporate_upload_data',
  `btch_id` int(11) DEFAULT NULL,
  `fs_id` int(11) DEFAULT NULL,
  `excel` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`std_id`),
  KEY `btch_id` (`btch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_profile_temp_history`;
CREATE TABLE `student_profile_temp_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `std_id` int(11) NOT NULL,
  `std_corporate_id` int(11) DEFAULT NULL,
  `std_username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_fullname` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_idnumber` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_dob` date DEFAULT NULL,
  `std_nationality` int(11) DEFAULT NULL,
  `std_type_nationality` int(11) DEFAULT NULL,
  `std_gender` int(11) DEFAULT NULL,
  `std_race` int(11) DEFAULT NULL,
  `std_contact_number` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_email` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_qualification` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_department` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_designation` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_membership` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_staffno` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_location` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_region` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_company` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `std_remark` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `req_check` int(11) DEFAULT NULL,
  `req_error` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `upl_id` int(11) DEFAULT NULL COMMENT 'fk tbl_corporate_upload',
  `fs_id` int(11) DEFAULT NULL,
  `btch_id` int(11) DEFAULT NULL,
  `excel` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `updDate` datetime DEFAULT NULL,
  `updBy` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `btch_id` (`btch_id`),
  KEY `fs_id` (`fs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_qualification`;
CREATE TABLE `student_qualification` (
  `ae_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `ae_appl_id` bigint(20) NOT NULL,
  `ae_transaction_id` int(11) NOT NULL,
  `ae_qualification` int(11) DEFAULT NULL COMMENT 'PK tbl_qualificationmaster',
  `ae_majoring` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_degree_awarded` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_class_degree` int(11) DEFAULT NULL,
  `ae_result` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_year_graduate` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_institution_country` int(11) DEFAULT NULL COMMENT 'fk tbl_countries',
  `ae_institution` int(11) DEFAULT NULL COMMENT 'fk.school_master',
  `ae_medium_instruction` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_order` int(11) NOT NULL,
  `others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `createBy` int(11) DEFAULT NULL,
  `ae_role` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'admin/student',
  PRIMARY KEY (`ae_id`),
  KEY `ae_transaction_id` (`ae_transaction_id`),
  KEY `ae_appl_id` (`ae_appl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_quantitative_proficiency`;
CREATE TABLE `student_quantitative_proficiency` (
  `qp_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `qp_trans_id` bigint(20) NOT NULL,
  `qp_level` int(11) DEFAULT NULL,
  `qp_grade_mark` decimal(10,2) DEFAULT NULL,
  `qp_institution` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qp_createdby` int(11) NOT NULL,
  `qp_createddt` datetime NOT NULL,
  PRIMARY KEY (`qp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_referees`;
CREATE TABLE `student_referees` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `r_txn_id` bigint(20) NOT NULL,
  `r_ref1_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_ref1_position` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_ref1_add1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_add2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_add3` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_postcode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_city` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_ref1_country` int(11) NOT NULL,
  `r_ref1_state` int(11) DEFAULT NULL,
  `r_ref1_phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_ref1_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_ref2_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_ref2_position` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_ref2_add1` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_ref2_add2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_add3` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_postcode` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_city` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_country` int(11) NOT NULL,
  `r_ref2_state` int(11) DEFAULT NULL,
  `r_ref2_phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_ref2_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_updateby` int(11) NOT NULL,
  `r_updatedt` datetime NOT NULL,
  PRIMARY KEY (`r_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_registrationid_seqno`;
CREATE TABLE `student_registrationid_seqno` (
  `srs_id` int(11) NOT NULL AUTO_INCREMENT,
  `srs_program_id` int(11) DEFAULT NULL,
  `srs_year` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `srs_award` int(11) DEFAULT NULL,
  `srs_intake` int(11) DEFAULT NULL,
  `srs_semester` int(11) DEFAULT NULL,
  `srs_branch` int(11) DEFAULT NULL,
  `srs_gender` char(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `srs_type` char(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `srs_seqno` int(11) NOT NULL,
  `srs_vseqno` int(11) NOT NULL DEFAULT '1',
  `srs_updateby` int(11) DEFAULT NULL,
  `srs_updatedt` datetime DEFAULT NULL,
  PRIMARY KEY (`srs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_relationship_information`;
CREATE TABLE `student_relationship_information` (
  `ri_id` int(11) NOT NULL AUTO_INCREMENT,
  `ri_sp_id` int(11) NOT NULL,
  `ri_appl_id` int(11) NOT NULL,
  `ri_trans_id` int(11) NOT NULL,
  `ri_staff_id` int(11) NOT NULL COMMENT 'fk to tbl_staffmaster',
  `ri_updDate` datetime NOT NULL,
  `ri_updUser` int(11) NOT NULL,
  PRIMARY KEY (`ri_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_research_publication`;
CREATE TABLE `student_research_publication` (
  `rpd_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `rpd_trans_id` bigint(20) NOT NULL,
  `rpd_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rpd_date` date DEFAULT NULL,
  `rpd_publisher` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rpd_createddt` datetime NOT NULL,
  `rpd_createdby` int(11) NOT NULL,
  PRIMARY KEY (`rpd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_visa`;
CREATE TABLE `student_visa` (
  `av_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `av_appl_id` int(11) NOT NULL,
  `av_trans_id` int(11) NOT NULL,
  `av_malaysian_visa` int(11) NOT NULL COMMENT '1=yes, 0=no',
  `av_status` int(11) DEFAULT NULL,
  `av_expiry` date DEFAULT NULL,
  `upd_date` datetime NOT NULL,
  `upd_by` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `av_reference_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `av_issue_date` date DEFAULT NULL,
  `av_active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active 1: Active',
  `ad_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`av_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `student_working_experience`;
CREATE TABLE `student_working_experience` (
  `aw_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `aw_appl_id` int(11) NOT NULL,
  `aw_trans_id` int(11) NOT NULL,
  `aw_industry_id` int(11) NOT NULL COMMENT 'Industry Working Experience',
  `aw_years` int(11) NOT NULL COMMENT 'Years of industry',
  `upd_date` datetime NOT NULL,
  `aw_others` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`aw_id`),
  UNIQUE KEY `aw_id` (`aw_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `subjectre`;
CREATE TABLE `subjectre` (
  `a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `subjectxre`;
CREATE TABLE `subjectxre` (
  `a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `suspend_detail`;
CREATE TABLE `suspend_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sm_id` bigint(20) NOT NULL,
  `item_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_amount` decimal(20,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `suspend_main`;
CREATE TABLE `suspend_main` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `billing_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appl_id` bigint(20) DEFAULT NULL,
  `payment _description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `payment_mode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_reference` bigint(20) DEFAULT NULL COMMENT 'pk payment_bank_record_detail',
  `payment_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `system_error`;
CREATE TABLE `system_error` (
  `se_id` int(11) NOT NULL AUTO_INCREMENT,
  `se_txn_id` int(11) DEFAULT '0',
  `se_IdStudentRegistration` int(11) DEFAULT '0',
  `se_IdStudentRegSubjects` int(11) DEFAULT '0',
  `se_title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `se_message` mediumtext COLLATE utf8_unicode_ci,
  `se_createddt` datetime NOT NULL,
  `se_createdby` int(11) NOT NULL,
  `se_url` mediumtext COLLATE utf8_unicode_ci,
  `se_from` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`se_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `takafuloperatortbe`;
CREATE TABLE `takafuloperatortbe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addr1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addr2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_cell` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_type_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_academicprogress_subjects`;
CREATE TABLE `tbl_academicprogress_subjects` (
  `IdAcademicProgressSub` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudent` bigint(20) DEFAULT NULL COMMENT 'FK to tbl_studentregistration',
  `IdAcademicProgress` bigint(20) NOT NULL COMMENT 'FK to tbl_academeic_progress',
  `Year_Level_Block` int(11) DEFAULT NULL COMMENT 'field to hold the year/level/block ',
  `Semester` int(11) DEFAULT NULL COMMENT 'field to hold semester',
  `IdCourseType` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CourseID` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CourseDescription` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreditHours` double DEFAULT NULL,
  `Grade` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsRegistered` binary(1) DEFAULT NULL COMMENT '0: Not registered, 1:registered',
  `UpdUser` bigint(20) DEFAULT NULL COMMENT 'FK to user table',
  `updDate` datetime DEFAULT NULL,
  PRIMARY KEY (`IdAcademicProgressSub`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_academicstatus`;
CREATE TABLE `tbl_academicstatus` (
  `IdAcademicStatus` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdProgram` bigint(20) unsigned DEFAULT NULL,
  `IdSemester` bigint(20) unsigned NOT NULL,
  `SemesterCode` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdAward` bigint(20) DEFAULT NULL,
  `IdScheme` bigint(20) DEFAULT NULL,
  `AcademicStatus` bigint(20) unsigned NOT NULL COMMENT '0:GPA 1:CGPA',
  `BasedOn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Minimum` decimal(10,2) DEFAULT NULL,
  `Maximum` decimal(10,2) DEFAULT NULL,
  `MinimumTotalMarks` decimal(10,2) DEFAULT NULL,
  `MaximumTotalMarks` decimal(10,2) DEFAULT NULL,
  `StatusEnglishName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `StatusArabicName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ClassHonorship` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `Active` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`IdAcademicStatus`),
  KEY `IdProgram` (`IdProgram`),
  KEY `IdSemester` (`IdSemester`),
  KEY `UpdUser` (`UpdUser`),
  KEY `IdSemester_2` (`IdSemester`),
  KEY `IdAcademicStatus` (`IdAcademicStatus`),
  KEY `IdProgram_2` (`IdProgram`),
  KEY `IdSemester_3` (`IdSemester`),
  KEY `IdAcademicStatus_2` (`IdAcademicStatus`),
  KEY `IdProgram_3` (`IdProgram`),
  KEY `IdSemester_4` (`IdSemester`),
  KEY `IdScheme` (`IdScheme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_academicstatus_details`;
CREATE TABLE `tbl_academicstatus_details` (
  `IdAcademicStatusDetail` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdAcademicStatus` bigint(20) NOT NULL,
  `Minimum` decimal(10,2) NOT NULL,
  `Maximum` decimal(10,2) NOT NULL,
  `Gradepoint` decimal(10,2) DEFAULT NULL COMMENT 'x pakai',
  `Gradevalue` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'x pakai',
  `StatusEnglishName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `StatusArabicName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Probation` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: No 1:Yes',
  PRIMARY KEY (`IdAcademicStatusDetail`),
  KEY `IdAcademicStatusDetail` (`IdAcademicStatusDetail`),
  KEY `IdAcademicStatus` (`IdAcademicStatus`),
  KEY `IdAcademicStatusDetail_2` (`IdAcademicStatusDetail`),
  KEY `IdAcademicStatus_2` (`IdAcademicStatus`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_academic_period`;
CREATE TABLE `tbl_academic_period` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `ap_intake_id` int(11) DEFAULT NULL COMMENT 'fk_tbl_academic_period (IdIntake)',
  `ap_month` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ap_year` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ap_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ap_desc` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ap_number` int(11) DEFAULT NULL COMMENT '1-10 sahaja, refer jadual penerimaan calon',
  PRIMARY KEY (`ap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='period untuk intake (application)';


DROP TABLE IF EXISTS `tbl_academic_progress`;
CREATE TABLE `tbl_academic_progress` (
  `IdAcademicProgress` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudent` bigint(20) DEFAULT NULL COMMENT 'FK to tbl_studentregistration',
  `IdScheme` bigint(20) DEFAULT NULL COMMENT 'FK to scheme table',
  `IdFaculty` bigint(20) DEFAULT NULL COMMENT 'FK to faculty table',
  `IdProgram` bigint(20) DEFAULT NULL COMMENT 'FK to tbl_program',
  `IdIntake` bigint(20) DEFAULT NULL COMMENT 'FK to tbl_intake',
  `IdBranch` bigint(20) DEFAULT NULL COMMENT 'FK to branch table',
  `ProfileStatus` bigint(20) DEFAULT NULL COMMENT 'FK to maintenance table',
  `LandscapeType` int(11) DEFAULT NULL COMMENT '1: block, 2:level, 3:semester',
  `UpdUser` bigint(20) DEFAULT NULL COMMENT 'FK to user table',
  `UpdDate` datetime DEFAULT NULL,
  PRIMARY KEY (`IdAcademicProgress`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_academic_year`;
CREATE TABLE `tbl_academic_year` (
  `ay_id` int(11) NOT NULL AUTO_INCREMENT,
  `ay_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ay_desc` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ay_start_date` date NOT NULL COMMENT 'tarikh besar utk semester',
  `ay_end_date` date NOT NULL COMMENT 'tarikh besar utk semester',
  `ay_year_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'PAST, CURRENT, NEXT, FUTURE',
  PRIMARY KEY (`ay_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='tahun yang student belajar';


DROP TABLE IF EXISTS `tbl_accountgroup`;
CREATE TABLE `tbl_accountgroup` (
  `IdAccountGroup` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `GroupName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GroupCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdParent` bigint(20) unsigned DEFAULT NULL,
  `Active` binary(1) NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdAccountGroup`),
  KEY `UpdUser` (`UpdUser`),
  KEY `IdParent` (`IdParent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_accountmaster`;
CREATE TABLE `tbl_accountmaster` (
  `idAccount` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `AccountName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AccShortName` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PrefixCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AccountType` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0- Debit , 1- Credit',
  `Description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Period` bigint(20) NOT NULL,
  `Revenue` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - yes , 0 - no',
  `AccountCreatedDt` datetime NOT NULL,
  `AccountCreatedBy` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_user ',
  `IdGroup` bigint(20) DEFAULT '0' COMMENT 'Not Physical FK to tbl_Accountmaster ',
  `BillingModule` bigint(20) DEFAULT NULL COMMENT 'FK to tbl_definisionms',
  `duringRegistration` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-if during registration',
  `AdvancePayment` tinyint(1) NOT NULL DEFAULT '0',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_user',
  `Active` binary(1) DEFAULT NULL COMMENT '0:No, 1:Yes',
  `AccountCode` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Deposit` binary(1) NOT NULL DEFAULT '0',
  `Refund` binary(1) NOT NULL DEFAULT '0',
  `duringProcessing` tinyint(1) DEFAULT '0' COMMENT '0-No,1-Yes',
  PRIMARY KEY (`idAccount`),
  KEY `FK_tbl_AccountMaster_AccountCreatedBy` (`AccountCreatedBy`),
  KEY `FK_tbl_AccountMaster_UpdUser` (`UpdUser`),
  CONSTRAINT `FK_tbl_AccountMaster_AccountCreatedBy` FOREIGN KEY (`AccountCreatedBy`) REFERENCES `tbl_user` (`iduser`),
  CONSTRAINT `FK_tbl_AccountMaster_UpdUser` FOREIGN KEY (`UpdUser`) REFERENCES `tbl_user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_account_code`;
CREATE TABLE `tbl_account_code` (
  `ac_id` int(11) NOT NULL AUTO_INCREMENT,
  `ac_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ac_desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ac_acc_type` bigint(20) NOT NULL COMMENT 'FK tbl_definationms',
  `ac_status` tinyint(4) NOT NULL DEFAULT '1',
  `ac_last_edit_by` bigint(20) NOT NULL,
  `ac_last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`ac_id`),
  KEY `ac_acc_type` (`ac_acc_type`),
  KEY `last_edit_by` (`ac_last_edit_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Table to store account code';


DROP TABLE IF EXISTS `tbl_activity`;
CREATE TABLE `tbl_activity` (
  `idActivity` int(11) NOT NULL AUTO_INCREMENT,
  `ActivityType` int(11) NOT NULL DEFAULT '0',
  `ActivityName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ActivityColorCode` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'HTML Color Code',
  `UpdDate` datetime NOT NULL,
  `UpdUser` int(1) NOT NULL,
  PRIMARY KEY (`idActivity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_activity_calender`;
CREATE TABLE `tbl_activity_calender` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL COMMENT '1:semester 2:period . No longer in use',
  `IdSemester` int(10) DEFAULT NULL COMMENT 'No longer in use',
  `IdSemesterMain` int(10) DEFAULT NULL COMMENT 'No longer in use',
  `IdPeriod` int(11) DEFAULT NULL COMMENT 'No longer in use',
  `IdActivity` int(10) NOT NULL,
  `IdIntake` int(11) NOT NULL,
  `IdProgram` int(11) NOT NULL,
  `IdProgramScheme` int(11) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `createdby` int(11) NOT NULL,
  `createddt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_address`;
CREATE TABLE `tbl_address` (
  `add_id` int(11) NOT NULL AUTO_INCREMENT,
  `add_org_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'eg. branch,faculty etc utk mudah programmer simpan nama table',
  `add_org_id` int(11) NOT NULL COMMENT 'organization fk id ',
  `add_address_type` int(4) NOT NULL COMMENT 'fk tbl_defination (98)',
  `add_country` int(11) NOT NULL COMMENT 'fk tbl_country',
  `add_state` int(11) NOT NULL COMMENT 'fk_tbl_state',
  `add_city` int(11) DEFAULT NULL,
  `add_city_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_state_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_address1` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `add_address2` mediumtext COLLATE utf8mb4_unicode_ci,
  `add_zipcode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add_phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add_phone2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_affiliate` int(11) NOT NULL DEFAULT '0',
  `add_createddt` datetime NOT NULL,
  `add_createdby` int(11) NOT NULL,
  `add_modifyby` int(11) DEFAULT NULL,
  `add_modifydt` datetime DEFAULT NULL,
  PRIMARY KEY (`add_id`),
  KEY `add_org_name` (`add_org_name`),
  KEY `add_org_id` (`add_org_id`),
  KEY `add_org_name_2` (`add_org_name`),
  KEY `add_org_id_2` (`add_org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_address_history`;
CREATE TABLE `tbl_address_history` (
  `addh_id` int(11) NOT NULL AUTO_INCREMENT,
  `add_id` int(11) NOT NULL COMMENT 'fk tbl_address',
  `addh_org_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'eg. branch,faculty etc utk mudah programmer simpan nama table',
  `addh_org_id` int(11) NOT NULL COMMENT 'organization fk id ',
  `addh_address_type` int(4) NOT NULL COMMENT 'fk tbl_defination (98)',
  `addh_country` int(11) NOT NULL COMMENT 'fk tbl_country',
  `addh_state` int(11) NOT NULL COMMENT 'fk_tbl_state',
  `addh_city` int(11) DEFAULT '0',
  `addh_city_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addh_state_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addh_address1` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `addh_address2` mediumtext COLLATE utf8mb4_unicode_ci,
  `addh_zipcode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addh_phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addh_phone2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addh_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addh_email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addh_affiliate` int(11) NOT NULL DEFAULT '0',
  `addh_createddt` datetime NOT NULL,
  `addh_createdby` int(11) NOT NULL,
  `addh_changedt` datetime NOT NULL,
  `addh_changeby` int(11) NOT NULL,
  PRIMARY KEY (`addh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_advisement`;
CREATE TABLE `tbl_advisement` (
  `advisement_id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_id` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `IdSubject` int(11) NOT NULL,
  `IdSubjectRepeat` int(11) NOT NULL DEFAULT '0',
  `grade` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_hours` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '1: normal 2: Audit 3: Exemption 4: Credit transfer 5: repeat retake',
  PRIMARY KEY (`advisement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='tmp for what if tools';


DROP TABLE IF EXISTS `tbl_agentapplicanttagging`;
CREATE TABLE `tbl_agentapplicanttagging` (
  `IdAgentApplicantTag` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdAgent` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `InchargeImperson` bigint(20) NOT NULL,
  `IdStudent` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AssignedOn` date NOT NULL,
  `AssignedBy` bigint(20) NOT NULL,
  PRIMARY KEY (`IdAgentApplicantTag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_agentmaster`;
CREATE TABLE `tbl_agentmaster` (
  `IdAgentMaster` bigint(20) NOT NULL AUTO_INCREMENT,
  `AgentType` bigint(20) NOT NULL,
  `AgentCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AgentName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AgentName1` longtext COLLATE utf8mb4_unicode_ci,
  `AgentAdd1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AgentAdd2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` bigint(20) unsigned NOT NULL,
  `State` bigint(20) unsigned NOT NULL,
  `Country` bigint(20) unsigned NOT NULL,
  `Phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `URL` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ContactPerson` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Desgination` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ContactPhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ContactCell` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` binary(1) NOT NULL,
  `EffectiveDate` datetime NOT NULL,
  `AgentField1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AgentField2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AgentField3` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AgentField4` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AgentField5` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `passwd` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`IdAgentMaster`),
  KEY `UpdUser` (`UpdUser`),
  CONSTRAINT `tbl_agentmaster_ibfk_1` FOREIGN KEY (`UpdUser`) REFERENCES `tbl_user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_agentpaymentdetails`;
CREATE TABLE `tbl_agentpaymentdetails` (
  `IdPayment` bigint(20) NOT NULL AUTO_INCREMENT,
  `AgentType` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Intake` bigint(20) NOT NULL,
  `Amount` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`IdPayment`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_agentprogram`;
CREATE TABLE `tbl_agentprogram` (
  `IdAgentProgram` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdAgentMaster` bigint(20) unsigned NOT NULL,
  `IdProgram` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`IdAgentProgram`),
  KEY `IdAgentMaster` (`IdAgentMaster`),
  KEY `IdProgram` (`IdProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_announcement`;
CREATE TABLE `tbl_announcement` (
  `sl_id` int(11) NOT NULL AUTO_INCREMENT,
  `sl_var_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sl_english` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sl_bahasa` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`sl_id`),
  UNIQUE KEY `sl_var_name` (`sl_var_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_answerdetails`;
CREATE TABLE `tbl_answerdetails` (
  `idanswerdetail` bigint(20) NOT NULL AUTO_INCREMENT,
  `Regid` varchar(255) NOT NULL,
  `QuestionNo` bigint(20) NOT NULL,
  `Answer` bigint(20) NOT NULL,
  PRIMARY KEY (`idanswerdetail`),
  KEY `Regid` (`Regid`),
  KEY `QuestionNo` (`QuestionNo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_answers`;
CREATE TABLE `tbl_answers` (
  `idanswers` bigint(20) NOT NULL AUTO_INCREMENT,
  `idquestion` bigint(20) DEFAULT NULL,
  `answers` varchar(500) DEFAULT NULL,
  `CorrectAnswer` binary(1) NOT NULL DEFAULT '0',
  `Tamil` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Arabic` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Malay` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Answersid` varchar(50) DEFAULT NULL,
  `migrate_status` varchar(10) DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `migrate_remarks` text,
  PRIMARY KEY (`idanswers`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS `tbl_appeal`;
CREATE TABLE `tbl_appeal` (
  `IdAppeal` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdRegistration` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentregistration',
  `IdApplication` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentappication',
  `IdMarksDistributionMaster` bigint(20) unsigned NOT NULL,
  `IdMarksDistributionDetails` bigint(20) unsigned NOT NULL,
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'tbl_subject',
  `Idverifiermarks` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_verifiermarks',
  `AppealCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MarksObtained` int(10) unsigned NOT NULL,
  `MarksExpected` int(10) unsigned NOT NULL,
  `Comments` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` date NOT NULL,
  `AppealStatus` binary(1) NOT NULL,
  PRIMARY KEY (`IdAppeal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_appealmarksentry`;
CREATE TABLE `tbl_appealmarksentry` (
  `IdAppealEntry` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdAppeal` bigint(20) NOT NULL,
  `NewMarks` decimal(12,2) NOT NULL,
  `DateOfRemarks` date NOT NULL,
  `RemarkedBy` bigint(20) NOT NULL,
  PRIMARY KEY (`IdAppealEntry`),
  KEY `IdAppeal` (`IdAppeal`),
  CONSTRAINT `tbl_appealmarksentry_ibfk_1` FOREIGN KEY (`IdAppeal`) REFERENCES `tbl_appeal` (`IdAppeal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_appealmarks_config`;
CREATE TABLE `tbl_appealmarks_config` (
  `IdAppealMarksConfig` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUniversity` bigint(20) DEFAULT NULL,
  `MaxAppeal` tinyint(5) DEFAULT NULL COMMENT 'Min is 1',
  `ChooseMarks` bigint(20) DEFAULT NULL COMMENT 'FK to tbl_definationms : 85',
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `SemesterCode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Semestercode',
  PRIMARY KEY (`IdAppealMarksConfig`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_applicant`;
CREATE TABLE `tbl_applicant` (
  `IdApplicant` bigint(20) NOT NULL AUTO_INCREMENT,
  `Emaillogin` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ApplicantCode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RegisteredDate` datetime NOT NULL,
  `tab_value` tinyint(4) DEFAULT '1' COMMENT '1:Ppart, 2:Pprog, 3:Qua, 4:Decl, 5:Admsn',
  PRIMARY KEY (`IdApplicant`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_applicant_personal_detail`;
CREATE TABLE `tbl_applicant_personal_detail` (
  `IdApplicantPersonalDetail` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplicant` bigint(20) NOT NULL,
  `ApplicantCode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `defaultlangname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameField2` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameField3` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameField4` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameField5` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PPNos` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PPIssueDt` date DEFAULT NULL,
  `PPExpDt` date DEFAULT NULL,
  `PPField1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PPField2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PPField3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PPField4` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PPField5` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VisaDetailField1` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `VisaDetailField2` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `VisaDetailField3` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `VisaDetailField4` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `VisaDetailField5` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField2` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField3` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField4` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField5` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField6` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField7` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField8` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField9` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField10` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField11` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField12` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField13` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField14` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField15` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField16` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField17` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField18` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField19` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraIdField20` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Nationality` bigint(20) NOT NULL,
  `Gender` binary(1) NOT NULL,
  `HomeNumber` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `OfficeNumber` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MobileNumber` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MaritalStatus` int(10) DEFAULT NULL,
  `CorrespondenceAdd` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CorrespondenceAdd2` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CorrespondenceCountry` bigint(11) DEFAULT NULL,
  `CorrespondenceProvince` bigint(11) DEFAULT NULL,
  `CorrespondenceCity` bigint(11) DEFAULT NULL,
  `CorrespondenceZip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdProgram` bigint(20) NOT NULL,
  `status` bigint(20) NOT NULL,
  `DOB` date DEFAULT NULL,
  `ApplicantCodeFormat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdFormat` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IdApplicantPersonalDetail`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_applicant_preffered`;
CREATE TABLE `tbl_applicant_preffered` (
  `IdApplicationPreferred` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplicant` bigint(20) NOT NULL,
  `IdIntake` bigint(20) NOT NULL,
  `IdProgramLevel` bigint(20) NOT NULL,
  `IdPriorityNo` bigint(20) NOT NULL,
  `IdProgram` bigint(20) NOT NULL,
  `IdBranch` bigint(20) NOT NULL,
  `IdScheme` bigint(20) NOT NULL,
  PRIMARY KEY (`IdApplicationPreferred`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_applicant_qualification`;
CREATE TABLE `tbl_applicant_qualification` (
  `IdApplicationQualification` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplicant` bigint(20) NOT NULL,
  `IdPlaceObtained` bigint(20) NOT NULL,
  `yearobtained` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdEducationalLevel` bigint(20) NOT NULL,
  `certificatename` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdInstitute` bigint(20) DEFAULT NULL,
  `otherinstitute` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instituteaddress1` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instituteaddress2` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` bigint(20) NOT NULL,
  `state` bigint(20) NOT NULL,
  `city` bigint(20) NOT NULL,
  `zipcode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonecountrycode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonestatecode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faxcountrycode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faxstatecode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdResultItem` bigint(20) NOT NULL,
  `resulttotal` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`IdApplicationQualification`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_applicant_subject_detail`;
CREATE TABLE `tbl_applicant_subject_detail` (
  `IdApplicationSubjectDetail` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplicationQualification` bigint(20) NOT NULL,
  `IdSubject` bigint(20) NOT NULL,
  `IdSubjectGrade` bigint(20) NOT NULL,
  PRIMARY KEY (`IdApplicationSubjectDetail`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_application_config`;
CREATE TABLE `tbl_application_config` (
  `IdAppConfig` bigint(20) NOT NULL AUTO_INCREMENT,
  `RevertProcessMigration` binary(1) NOT NULL,
  `RevertProcessApproval` binary(1) NOT NULL,
  `AgentAppointment` binary(1) NOT NULL,
  `ApplicationValidity` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ApplicationVal` int(11) NOT NULL,
  `IdUniversity` bigint(20) NOT NULL,
  `NoofPreferred` int(20) NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdAppConfig`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_application_history`;
CREATE TABLE `tbl_application_history` (
  `IdApplicationHistory` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) DEFAULT NULL,
  `status` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdApplicationHistory`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_application_initialconfig`;
CREATE TABLE `tbl_application_initialconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `programID` int(11) NOT NULL COMMENT 'PK tbl_program_scheme',
  `sectionID` int(11) NOT NULL COMMENT 'fk tbl_application_section',
  `sdtCtgy` int(11) NOT NULL,
  `section_maxEntry` int(11) NOT NULL DEFAULT '1',
  `section_mandatory` tinyint(4) NOT NULL,
  `seq_order` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `initialconfigdesc` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Description',
  `initModDate` datetime NOT NULL,
  `initModBy` int(11) NOT NULL,
  `sectionTagInstruction` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_application_item`;
CREATE TABLE `tbl_application_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_section_id` int(11) NOT NULL,
  `variable` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formType` int(11) NOT NULL COMMENT 'id 100',
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `itemMalayName` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'item malay name',
  `modDate` datetime NOT NULL COMMENT 'Modified date',
  `modBy` int(11) NOT NULL COMMENT 'Modified by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_application_item_tagging`;
CREATE TABLE `tbl_application_item_tagging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTagSection` int(11) NOT NULL COMMENT 'PK tbl_application_initialconfig',
  `idItem` int(250) NOT NULL COMMENT 'PK tbl_application_item',
  `item_dclId` int(11) NOT NULL COMMENT 'fk tbl_documentchecklist_dcl',
  `view` int(11) NOT NULL COMMENT '1=yes, 0=no',
  `compulsory` int(11) NOT NULL COMMENT '1=yes, 0=no',
  `enable` int(11) NOT NULL COMMENT '1=yes, 0=disabled',
  `hide` int(11) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  `item_docDownload` int(11) NOT NULL COMMENT 'fk comm_template',
  `seq_order` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  `createBy` int(11) NOT NULL,
  `tagDesc` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Description',
  `itemTagInstruction` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `itemInstructionPosition` int(11) NOT NULL DEFAULT '1' COMMENT 'instruction position 1=top 2=bottom',
  `tagModDate` datetime NOT NULL COMMENT 'Modified date',
  `tagModBy` int(11) NOT NULL COMMENT 'Modified By',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_application_section`;
CREATE TABLE `tbl_application_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instruction` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `modDate` datetime NOT NULL COMMENT 'Modified date',
  `modBy` int(11) NOT NULL COMMENT 'Modified by',
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_primary_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_id_column` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_column` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_join` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `join_column1` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `join_column2` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `student_table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_application_section_backup`;
CREATE TABLE `tbl_application_section_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instruction` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `tbl_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modDate` datetime NOT NULL COMMENT 'Modified date',
  `modBy` int(11) NOT NULL COMMENT 'Modified by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_application_status_message`;
CREATE TABLE `tbl_application_status_message` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdAppConfig` bigint(20) NOT NULL,
  `ApplicationStatus` bigint(20) NOT NULL,
  `Message` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_auditpaperapplication`;
CREATE TABLE `tbl_auditpaperapplication` (
  `apa_id` int(11) NOT NULL AUTO_INCREMENT,
  `apa_studregid` int(11) NOT NULL,
  `apa_appid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apa_programId` int(11) NOT NULL,
  `apa_semesterid` int(11) NOT NULL,
  `apa_status` int(11) NOT NULL,
  `apa_approveddate` date DEFAULT NULL,
  `apa_applieddate` date DEFAULT NULL,
  `apa_approvedby` int(11) NOT NULL,
  `apa_appliedby` int(11) NOT NULL,
  `apa_remarks` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apa_source` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apa_type` int(11) NOT NULL,
  `apa_subjectreg_id` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apa_ec_id` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apa_invid_0` int(11) NOT NULL DEFAULT '0',
  `apa_invid_1` int(11) NOT NULL DEFAULT '0',
  `apa_paid_0` int(11) NOT NULL DEFAULT '0',
  `apa_paid_1` int(11) NOT NULL DEFAULT '0',
  `apa_upddate` date DEFAULT NULL,
  `apa_upduser` int(11) NOT NULL,
  PRIMARY KEY (`apa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_auditpaperapplicationcourse`;
CREATE TABLE `tbl_auditpaperapplicationcourse` (
  `apc_id` int(11) NOT NULL AUTO_INCREMENT,
  `apc_apaid` int(11) NOT NULL,
  `apc_courseid` int(11) NOT NULL,
  `apc_credithours` int(11) NOT NULL,
  `apc_gred` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apc_remarks` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apc_status` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apc_program` int(11) NOT NULL DEFAULT '0',
  `apc_programscheme` int(11) NOT NULL DEFAULT '0',
  `apc_coursesection` int(11) NOT NULL DEFAULT '0',
  `apc_country` int(11) NOT NULL DEFAULT '0',
  `apc_city` int(11) NOT NULL DEFAULT '0',
  `apc_cityothers` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apc_itemreg` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apc_upddate` date NOT NULL,
  `apc_upduser` int(11) NOT NULL,
  PRIMARY KEY (`apc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_auditpaperapplicationitem`;
CREATE TABLE `tbl_auditpaperapplicationitem` (
  `api_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_apa_id` int(11) NOT NULL DEFAULT '0',
  `api_apc_id` int(11) NOT NULL DEFAULT '0',
  `api_item` int(11) NOT NULL DEFAULT '0',
  `api_invid_0` int(11) NOT NULL DEFAULT '0',
  `api_invid_1` int(11) NOT NULL DEFAULT '0',
  `api_paid_0` int(11) NOT NULL DEFAULT '0',
  `api_paid_1` int(11) NOT NULL DEFAULT '0',
  `api_upddate` datetime DEFAULT NULL,
  `api_upduser` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`api_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_auditpaperhistory`;
CREATE TABLE `tbl_auditpaperhistory` (
  `aph_id` int(11) NOT NULL AUTO_INCREMENT,
  `aph_oldstatus` int(11) NOT NULL,
  `aph_newstatus` int(11) NOT NULL,
  `aph_apaid` int(11) NOT NULL,
  `aph_upddate` date NOT NULL,
  `aph_upduser` int(11) NOT NULL,
  PRIMARY KEY (`aph_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_autocredittransfer`;
CREATE TABLE `tbl_autocredittransfer` (
  `IdAutoCredit` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdProgram` bigint(20) NOT NULL,
  `IdInstitution` bigint(20) NOT NULL,
  `IdQualification` bigint(20) NOT NULL,
  `IdSpecialization` bigint(20) NOT NULL,
  `MaxValueNextProm` int(11) NOT NULL,
  `YearLimit` int(11) NOT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`IdAutoCredit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_autocredittransferdetails`;
CREATE TABLE `tbl_autocredittransferdetails` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdAutoCredit` bigint(20) NOT NULL,
  `IdSubject` bigint(20) NOT NULL,
  `MinGradPoints` int(11) NOT NULL,
  `Remarks` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdCourse` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_awardlevel`;
CREATE TABLE `tbl_awardlevel` (
  `IdAllowance` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdLevel` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `IdAllowanceLevel` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdAllowance`),
  KEY `IdLevel` (`IdLevel`),
  KEY `IdAllowanceLevel` (`IdAllowanceLevel`),
  KEY `UpdUser` (`UpdUser`),
  CONSTRAINT `tbl_awardlevel_ibfk_1` FOREIGN KEY (`IdLevel`) REFERENCES `tbl_definationms` (`idDefinition`),
  CONSTRAINT `tbl_awardlevel_ibfk_2` FOREIGN KEY (`IdAllowanceLevel`) REFERENCES `tbl_definationms` (`idDefinition`),
  CONSTRAINT `tbl_awardlevel_ibfk_3` FOREIGN KEY (`UpdUser`) REFERENCES `tbl_user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_bank`;
CREATE TABLE `tbl_bank` (
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `IdBank` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `BankName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `BankAdd1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `BankAdd2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Country` bigint(20) DEFAULT NULL,
  `Phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `URL` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ContactPerson` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Desgination` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ContactPhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ContactCell` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` tinyint(4) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `MICR` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BKCode1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BKCode2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Branch` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AccountNo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IdBank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_bankdetails`;
CREATE TABLE `tbl_bankdetails` (
  `IdAccount` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdBank` bigint(20) unsigned NOT NULL,
  `AccountType` bigint(20) unsigned NOT NULL,
  `AccountNumber` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AccountingHeadName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` binary(1) NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdAccount`),
  KEY `IdBank` (`IdBank`),
  KEY `UpdUser` (`UpdUser`),
  KEY `AccountType` (`AccountType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_banktype`;
CREATE TABLE `tbl_banktype` (
  `AUTOINC` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Unique Key',
  `BANKCODE` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Bank Code. Must be unique.',
  `BANK_DESC` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Bank Description',
  `BANK_DESC2` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Bank Description default language',
  PRIMARY KEY (`AUTOINC`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_bank_account`;
CREATE TABLE `tbl_bank_account` (
  `IdBankAccount` int(11) NOT NULL AUTO_INCREMENT,
  `IdBank` int(11) DEFAULT NULL,
  `BankAccountName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BankAccountNumber` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`IdBankAccount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `tbl_barringrelease`;
CREATE TABLE `tbl_barringrelease` (
  `tbr_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tbr_appstud_id` int(11) unsigned NOT NULL,
  `tbr_type` int(11) unsigned NOT NULL,
  `tbr_reason` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tbr_status` tinyint(3) NOT NULL COMMENT '0: barring 1: release',
  `tbr_createby` int(11) unsigned NOT NULL,
  `tbr_createdate` datetime NOT NULL,
  `tbr_updby` int(11) unsigned NOT NULL,
  `tbr_upddate` datetime NOT NULL,
  PRIMARY KEY (`tbr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_barringreleasehistory`;
CREATE TABLE `tbl_barringreleasehistory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tbr_id` int(11) unsigned NOT NULL,
  `tbr_category` int(11) unsigned NOT NULL,
  `tbr_appstud_id` int(11) unsigned NOT NULL,
  `tbr_type` int(11) unsigned NOT NULL,
  `tbr_intake` int(11) unsigned NOT NULL,
  `tbr_reason` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tbr_status` tinyint(3) NOT NULL COMMENT '0: barring 1: release',
  `tbr_changeType` tinyint(3) NOT NULL COMMENT '0: delete 1: add 2: edit',
  `tbr_createby` int(11) unsigned NOT NULL,
  `tbr_createdate` date NOT NULL,
  `tbr_updby` int(11) unsigned NOT NULL,
  `tbr_upddate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_barringrole`;
CREATE TABLE `tbl_barringrole` (
  `brole_id` int(11) NOT NULL AUTO_INCREMENT,
  `brole_type` int(11) NOT NULL,
  `brole_role` int(11) NOT NULL,
  `brole_updby` int(11) NOT NULL,
  `brole_upddate` date NOT NULL,
  PRIMARY KEY (`brole_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_barringsetup`;
CREATE TABLE `tbl_barringsetup` (
  `tbs_id` int(11) NOT NULL AUTO_INCREMENT,
  `tbs_type` int(11) NOT NULL DEFAULT '0',
  `tbs_role` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tbs_upddate` date NOT NULL,
  `tbs_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tbs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_batchdetail`;
CREATE TABLE `tbl_batchdetail` (
  `IdBatchDetail` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdBatch` bigint(20) unsigned DEFAULT NULL COMMENT 'Foregin Key from tbl_BatchMaster',
  `IdPart` varchar(50) NOT NULL,
  `NoOfQuestions` int(11) NOT NULL DEFAULT '0',
  `BatchDtlStatus` binary(1) NOT NULL DEFAULT '0' COMMENT '0: Active , 1 : InActive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdBatchDetail`),
  KEY `FK_tbl_BatchDetail_IdBatch` (`IdBatch`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_batchmaster`;
CREATE TABLE `tbl_batchmaster` (
  `IdBatch` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `BatchName` varchar(1000) NOT NULL,
  `BatchFrom` date NOT NULL,
  `BatchTo` date NOT NULL,
  `BatchStatus` binary(1) NOT NULL DEFAULT '0' COMMENT '0: Active , 1 : InActive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `IdProgrammaster` bigint(20) unsigned NOT NULL COMMENT 'Fk to programmaster',
  `migrate_remarks` text,
  `migrate_date` datetime DEFAULT NULL,
  `migrate_status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`IdBatch`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_billprogram`;
CREATE TABLE `tbl_billprogram` (
  `idBillProgram` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idBillStructureMaster` bigint(20) unsigned NOT NULL,
  `idProgram` bigint(20) unsigned NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idBillProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_billstructuredetails`;
CREATE TABLE `tbl_billstructuredetails` (
  `idBillStructureDetails` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idBillStructureMaster` bigint(20) unsigned NOT NULL,
  `idVoucherType` bigint(20) unsigned NOT NULL,
  `itemDescription` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idCalculationMethod` bigint(20) unsigned NOT NULL COMMENT 'tbl_definitionms(monthly,yearly...)',
  `Amount` float(20,3) NOT NULL,
  `idDebitAccount` bigint(20) unsigned NOT NULL,
  `idCreditAccount` bigint(20) unsigned NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idBillStructureDetails`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_billstructuremaster`;
CREATE TABLE `tbl_billstructuremaster` (
  `idBillStructureMaster` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `BillMasterCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdIntakeFrom` bigint(20) unsigned NOT NULL COMMENT 'tbl_semestermaster',
  `IdIntakeTo` bigint(20) unsigned NOT NULL COMMENT 'tbl_semestermaster',
  `IdCategory` bigint(20) unsigned NOT NULL COMMENT 'tbl_definitionms',
  `effectiveDate` date NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idBillStructureMaster`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_blocksemesteryear`;
CREATE TABLE `tbl_blocksemesteryear` (
  `IdLandscapetempblockyearsemester` bigint(11) NOT NULL AUTO_INCREMENT,
  `IdLandscape` bigint(11) NOT NULL,
  `Year` bigint(11) NOT NULL,
  `YearSemester` bigint(11) NOT NULL,
  PRIMARY KEY (`IdLandscapetempblockyearsemester`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_branchofficevenue`;
CREATE TABLE `tbl_branchofficevenue` (
  `IdBranch` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `BranchName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ShortName` longtext COLLATE utf8mb4_unicode_ci,
  `BranchCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdType` int(11) NOT NULL DEFAULT '0',
  `Arabic` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` binary(1) NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `InstitutionEmail` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone2` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Currency` int(11) NOT NULL,
  `ExemptionCriteria` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Remark` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpemail` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cph` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cphemail` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registration_url` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` date NOT NULL,
  `invoice` int(11) NOT NULL COMMENT '1=yes, 0=no',
  PRIMARY KEY (`IdBranch`),
  KEY `UpdUser` (`UpdUser`),
  CONSTRAINT `tbl_branchofficevenue_ibfk_3` FOREIGN KEY (`UpdUser`) REFERENCES `tbl_user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_branchofficevenue_mou`;
CREATE TABLE `tbl_branchofficevenue_mou` (
  `mou_id` int(11) NOT NULL AUTO_INCREMENT,
  `mou_IdBranch` int(11) NOT NULL DEFAULT '0',
  `mou_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mou_desc` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mou_startDate` date NOT NULL,
  `mou_endDate` date NOT NULL,
  `mou_filepath` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mou_updUser` int(11) NOT NULL DEFAULT '0',
  `mou_updDate` datetime NOT NULL,
  PRIMARY KEY (`mou_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_branchofficevenue_program`;
CREATE TABLE `tbl_branchofficevenue_program` (
  `bp_id` int(11) NOT NULL AUTO_INCREMENT,
  `bp_IdBranch` int(11) NOT NULL,
  `bp_IdProgram` int(11) NOT NULL,
  `bp_updUser` int(11) NOT NULL,
  `bp_updDate` datetime NOT NULL,
  PRIMARY KEY (`bp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_branchregistrationmap`;
CREATE TABLE `tbl_branchregistrationmap` (
  `IdRegistration` int(4) NOT NULL AUTO_INCREMENT,
  `IdBranch` bigint(20) unsigned NOT NULL,
  `RegistrationLoc` int(50) NOT NULL,
  `Programme` int(11) NOT NULL,
  PRIMARY KEY (`IdRegistration`),
  KEY `br_fk_rl` (`IdBranch`),
  CONSTRAINT `br_fk_rl` FOREIGN KEY (`IdBranch`) REFERENCES `tbl_branchofficevenue` (`IdBranch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE `tbl_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_type` int(11) DEFAULT NULL,
  `category_range_min` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_range_max` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updateddt` datetime DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_cgpacalculation`;
CREATE TABLE `tbl_cgpacalculation` (
  `IdCGPAcalculation` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdRegistration` bigint(20) unsigned NOT NULL,
  `Cgpa` decimal(10,2) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `IdApplication` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`IdCGPAcalculation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_changemodeapplication`;
CREATE TABLE `tbl_changemodeapplication` (
  `cma_id` int(11) NOT NULL AUTO_INCREMENT,
  `cma_appid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cma_studregid` int(11) NOT NULL,
  `cma_applieddate` date NOT NULL,
  `cma_approveddate` date DEFAULT NULL,
  `cma_appliedby` int(11) NOT NULL,
  `cma_approvedby` int(11) NOT NULL,
  `cma_reason` int(11) NOT NULL,
  `cma_remarks` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cma_status` int(11) NOT NULL,
  `cma_semesterid` int(11) NOT NULL,
  `cma_mop` int(11) NOT NULL,
  `cma_mos` int(11) NOT NULL,
  `cma_currentmop` int(11) NOT NULL,
  `cma_currentmos` int(11) NOT NULL,
  `cma_programid` int(11) NOT NULL,
  `cma_pt` int(11) NOT NULL,
  `cma_upddate` date NOT NULL,
  `cma_upduser` int(11) NOT NULL,
  `cma_source` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cma_comment` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cma_reasonothers` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`cma_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_changemodeupload`;
CREATE TABLE `tbl_changemodeupload` (
  `cmu_id` int(11) NOT NULL AUTO_INCREMENT,
  `cmu_cmaid` int(11) NOT NULL,
  `cmu_filename` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmu_filerename` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmu_filelocation` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmu_filesize` int(11) NOT NULL,
  `cmu_mimetype` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmu_upddate` date NOT NULL,
  `cmu_upduser` int(11) NOT NULL,
  PRIMARY KEY (`cmu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_change_program_application`;
CREATE TABLE `tbl_change_program_application` (
  `IdChangeProgramApplication` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdCPApplication` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdStudent` bigint(20) NOT NULL,
  `Reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ApplicationDate` date NOT NULL,
  `AppliedBy` bigint(20) NOT NULL,
  `ApplicationStatus` bigint(20) NOT NULL,
  `CurrentProgram` bigint(20) NOT NULL,
  `NewProgram` bigint(20) NOT NULL,
  `EffectiveSemester` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Remarks` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ApprovedBy` bigint(20) NOT NULL,
  `ApprovedDate` date NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdChangeProgramApplication`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_change_program_course_transferred`;
CREATE TABLE `tbl_change_program_course_transferred` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdChangeProgram` bigint(20) NOT NULL,
  `IdSemesterMain` bigint(20) NOT NULL,
  `IdSemesterDetail` bigint(20) NOT NULL,
  `IdCourse` bigint(20) NOT NULL,
  `Grade` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Type` bigint(20) NOT NULL,
  `Upduser` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_change_status_application`;
CREATE TABLE `tbl_change_status_application` (
  `IdChangeStatusApplication` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) NOT NULL,
  `CSApplicationCode` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Reason` bigint(20) NOT NULL,
  `EffectiveSemesterFrom` int(11) NOT NULL DEFAULT '0',
  `EffectiveSemesterTo` int(11) DEFAULT NULL,
  `IdApplyingFor` bigint(20) NOT NULL,
  `Others` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ValidReason` tinyint(4) NOT NULL DEFAULT '0',
  `ApplicationStatus` bigint(20) NOT NULL,
  `ApplicationDate` date NOT NULL,
  `AppliedBy` bigint(20) NOT NULL,
  `AppliedByRole` int(11) NOT NULL,
  `Remarks` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ApprovedBy` bigint(20) NOT NULL,
  `DateApproved` date NOT NULL,
  `idregsub` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `idregsubdtl` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `cancelBy` int(11) DEFAULT NULL,
  `cancelDate` datetime DEFAULT NULL,
  `cancelByRole` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdChangeStatusApplication`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_change_status_audit_trail`;
CREATE TABLE `tbl_change_status_audit_trail` (
  `IdChangeStatusApplication` bigint(20) NOT NULL,
  `IdStudentRegistration` bigint(20) NOT NULL,
  `CSApplicationCode` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Reason` bigint(20) NOT NULL,
  `EffectiveSemesterFrom` int(11) NOT NULL DEFAULT '0',
  `EffectiveSemesterTo` int(11) DEFAULT NULL,
  `IdApplyingFor` bigint(20) NOT NULL,
  `Others` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ValidReason` tinyint(4) NOT NULL DEFAULT '0',
  `ApplicationStatus` bigint(20) NOT NULL,
  `ApplicationDate` date NOT NULL,
  `AppliedBy` bigint(20) NOT NULL,
  `AppliedByRole` int(11) NOT NULL,
  `Remarks` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ApprovedBy` bigint(20) NOT NULL,
  `DateApproved` date NOT NULL,
  `idregsub` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `idregsubdtl` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `cancelBy` int(11) DEFAULT NULL,
  `cancelDate` datetime DEFAULT NULL,
  `cancelByRole` int(11) DEFAULT NULL,
  `approval_status` int(11) NOT NULL COMMENT '1:approved 2:reject 3:revert',
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_charges`;
CREATE TABLE `tbl_charges` (
  `IdCharges` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdAccountMaster` bigint(20) unsigned NOT NULL,
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_program',
  `effectiveDate` date NOT NULL,
  `Rate` decimal(12,2) NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime NOT NULL,
  `Active` binary(1) NOT NULL,
  PRIMARY KEY (`IdCharges`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_chiefofprogramlist`;
CREATE TABLE `tbl_chiefofprogramlist` (
  `IdChiefOfProgramList` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  `IdStaff` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  `FromDate` date NOT NULL,
  `IdCollege` int(11) NOT NULL,
  `IdDepartment` int(11) DEFAULT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdChiefOfProgramList`),
  KEY `tbl_registrarlist_ibfk_1` (`UpdUser`),
  KEY `tbl_registrarlist_ibfk_2` (`IdProgram`),
  KEY `tbl_registrarlist_ibfk_3` (`IdStaff`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_city`;
CREATE TABLE `tbl_city` (
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `idCity` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CityCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idState` bigint(20) unsigned NOT NULL,
  `CityName` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` binary(1) NOT NULL,
  PRIMARY KEY (`idCity`),
  KEY `idState` (`idState`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_city_dummy`;
CREATE TABLE `tbl_city_dummy` (
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `idCity` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CityCode` varchar(201) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idState` bigint(20) unsigned NOT NULL,
  `CityName` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` binary(1) NOT NULL,
  PRIMARY KEY (`idCity`),
  KEY `idState` (`idState`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_classattendance`;
CREATE TABLE `tbl_classattendance` (
  `ctd_id` int(11) NOT NULL AUTO_INCREMENT,
  `ctd_groupid` int(11) NOT NULL DEFAULT '0',
  `ctd_studentid` int(11) NOT NULL DEFAULT '0',
  `ctd_item_type` int(11) NOT NULL,
  `ctd_scdate` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ctd_status` int(11) NOT NULL DEFAULT '0',
  `ctd_class_status` int(11) NOT NULL,
  `ctd_reason` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ctd_upddate` datetime NOT NULL,
  `ctd_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ctd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_collegemaster`;
CREATE TABLE `tbl_collegemaster` (
  `IdCollege` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CollegeName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ShortName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CollegeCode` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ArabicName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsDepartment` binary(1) NOT NULL DEFAULT '0' COMMENT '0: not department  1: Department',
  `CollegeType` binary(1) NOT NULL COMMENT '(0:Branch of university, 1:Independent college)',
  `Idhead` int(11) DEFAULT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  PRIMARY KEY (`IdCollege`),
  KEY `FK_tbl_collegemaster_UpdUser` (`UpdUser`),
  CONSTRAINT `tbl_collegemaster_ibfk_12` FOREIGN KEY (`UpdUser`) REFERENCES `tbl_user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_collegemasterbak`;
CREATE TABLE `tbl_collegemasterbak` (
  `IdCollege` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CollegeName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ShortName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CollegeCode` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ArabicName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsDepartment` binary(1) NOT NULL DEFAULT '0' COMMENT '0: not department  1: Department',
  `Add1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Add2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `State` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_state',
  `Country` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_countries',
  `Zip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AffiliatedTo` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_universitymaster',
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fax` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CollegeType` binary(1) NOT NULL COMMENT '(0:Branch of university, 1:Independent college)',
  `Idhead` int(11) NOT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdCollege`),
  KEY `FK_tbl_collegemaster_State` (`State`),
  KEY `FK_tbl_collegemaster_Country` (`Country`),
  KEY `FK_tbl_collegemaster_AffiliatedTo` (`AffiliatedTo`),
  KEY `FK_tbl_collegemaster_UpdUser` (`UpdUser`),
  CONSTRAINT `FK_tbl_collegemaster_ibfk_1` FOREIGN KEY (`AffiliatedTo`) REFERENCES `tbl_universitymaster` (`IdUniversity`),
  CONSTRAINT `FK_tbl_collegemaster_ibfk_2` FOREIGN KEY (`Country`) REFERENCES `tbl_countries` (`idCountry`),
  CONSTRAINT `FK_tbl_collegemaster_ibfk_3` FOREIGN KEY (`State`) REFERENCES `tbl_state_2012120xx` (`idState`),
  CONSTRAINT `FK_tbl_collegemaster_ibfk_4` FOREIGN KEY (`UpdUser`) REFERENCES `tbl_user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_company`;
CREATE TABLE `tbl_company` (
  `IdCompany` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `industryId` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `upd_date` datetime NOT NULL,
  `upd_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_concurrent_program`;
CREATE TABLE `tbl_concurrent_program` (
  `cp_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProgram` int(11) NOT NULL COMMENT 'fk tbl_program',
  `cp_program_id` int(11) NOT NULL,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`cp_id`),
  KEY `IdProgram` (`IdProgram`),
  KEY `cp_program_id` (`cp_program_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_config`;
CREATE TABLE `tbl_config` (
  `UpdDate` datetime NOT NULL,
  `Landscape` binary(1) NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `idUniversity` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_universutymaster',
  `idConfig` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `noofrowsingrid` int(11) NOT NULL,
  `DefaultCountry` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_country',
  `DefaultDropDownLanguage` bigint(20) NOT NULL COMMENT '0:English,1:Bahana Indonesia',
  `PPField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `PPField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `PPField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `PPField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `PPField5` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VisaDetailField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VisaDetailField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VisaDetailField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VisaDetailField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VisaDetailField5` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SMTPServer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SMTPUsername` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SMTPPassword` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SMTPPort` tinyint(5) DEFAULT NULL,
  `SSL` binary(1) DEFAULT NULL,
  `DefaultEmail` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ApplicantCodeType` tinyint(1) NOT NULL DEFAULT '0',
  `ApplicantIdFormat` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ApplicantPrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ResetApplicantSeq` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegistrationCodeType` tinyint(4) NOT NULL DEFAULT '0',
  `BillNoType` tinyint(4) NOT NULL,
  `BillNoTypeSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BillNoTypeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BillNoTypeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BillNoTypeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BillNoTypeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `StudentIdType` tinyint(4) NOT NULL,
  `StudentIdPrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `StudentIdFormat` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ResetStudentIdSeq` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CourseIdType` tinyint(4) NOT NULL,
  `CoursePrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CourseIdFormat` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ResetCourseSeq` tinyint(4) NOT NULL,
  `AgentIdType` tinyint(4) NOT NULL,
  `AgentIdFormat` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AgentPrefix` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ResetAgentSeq` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ReceiptType` tinyint(4) NOT NULL,
  `ReceiptTypeSeparator` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceiptTypeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceiptTypeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceiptTypeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceiptTypeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SponsorPrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SponsorSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SponsorCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SponsorCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SponsorCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SponsorCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegistrationIdFormat` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegistrationPrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ResetRegistrationSeq` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegistrationCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegistrationCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegistrationCodeText1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegistrationCodeText2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegistrationCodeText3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegistrationCodeText4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccountCode` binary(1) NOT NULL DEFAULT '0',
  `base` binary(1) NOT NULL DEFAULT '0',
  `levelbase` binary(1) NOT NULL DEFAULT '0',
  `FirstletterSeparator` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `FixedSeparator` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `FixedText` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `GeneralbaseSeparator` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseField1` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseField2` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseField3` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseField4` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseText1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseText2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseText3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseText4` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ItemCode` binary(1) NOT NULL DEFAULT '0',
  `Itembase` binary(1) NOT NULL DEFAULT '0',
  `itemlevelbase` binary(1) NOT NULL DEFAULT '0',
  `FirstletteritemSeparator` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `FixeditemSeparator` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `FixeditemText` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `GeneralbaseitemSeparator` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseitemField1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseitemField2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseitemField3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseitemField4` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseitemText1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseitemText2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseitemText3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `generalbaseitemText4` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SemesterCodeType` tinyint(1) NOT NULL DEFAULT '0',
  `SemesterSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SemesterCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SemesterCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SemesterCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SemesterCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SemesterCodeText1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SemesterCodeText2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SemesterCodeText3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SemesterCodeText4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SubjectCodeType` tinyint(4) NOT NULL DEFAULT '0',
  `SubjectIdFormat` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SubjectPrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ResetSubjectSeq` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SubjectCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SubjectCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartmentCodeType` tinyint(1) NOT NULL DEFAULT '0',
  `DepartmentSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartmentCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartmentCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartmentCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartmentCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CollegeCodeType` tinyint(1) NOT NULL DEFAULT '0',
  `CollegeSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CollegeCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CollegeCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CollegeCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccountCodeType` tinyint(1) NOT NULL DEFAULT '0',
  `AccountSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccountCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccountCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccountCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccountCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccountCodeText1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccountCodeText2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccountCodeText3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccountCodeText4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProformaInvoicePrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProformaInvoiceSeparator` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProformaInvoiceCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProformaInvoiceCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProformaInvoiceCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProformaInvoiceCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoicePrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoiceSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoiceCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoiceCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoiceCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoiceCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreditNotePrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreditNoteSeparator` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreditNoteCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreditNoteCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreditNoteCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreditNoteCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceiptPrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceiptSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceiptCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceiptCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceiptCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceiptCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceivableAdjustmentPrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceivableAdjustmentSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceivableAdjustmentField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceivableAdjustmentField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceivableAdjustmentField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ReceivableAdjustmentField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AdvancePrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AdvanceSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AdvanceCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AdvanceCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AdvanceCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AdvanceCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DiscountPrefix` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `RefundPrefix` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `InvoiceSponsorPrefix` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ReceiptSponsorPrefix` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AppealCodeType` tinyint(1) NOT NULL DEFAULT '0',
  `AppealCodeSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AppealCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AppealCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AppealCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AppealCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `StaffIdType` tinyint(4) NOT NULL DEFAULT '0',
  `StaffIdFormat` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `StaffPrefix` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ResetStaffSeq` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CollegeAliasName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CourseAliasName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SubjectAliasName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartmentAliasName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DeanAliasName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegisterAliasName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProgramAliasName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BranchAliasName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `LandscapeAliasName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Language` int(5) DEFAULT NULL,
  `AccDtlCount` int(11) DEFAULT '0',
  `AccDtl1` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl3` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl4` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl5` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl6` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl7` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl8` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl9` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl10` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl11` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl12` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl13` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl14` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl15` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl16` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl17` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl18` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl19` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccDtl20` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtlCount` int(11) DEFAULT '0',
  `MoheDtl1` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl3` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl4` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl5` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl6` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl7` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl8` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl9` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl10` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl11` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl12` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl13` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl14` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl15` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl16` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl17` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl18` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl19` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MoheDtl20` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `LocalStudent` binary(1) NOT NULL DEFAULT '0',
  `BlockType` binary(1) NOT NULL DEFAULT '0' COMMENT '0-Manual, 1 - Auto',
  `BlockName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BlockSeparator` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NoRefCode` tinyint(5) unsigned NOT NULL,
  `RefCodeField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RefCodeField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RefCodeField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RefCodeField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BKCode1` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BKCode2` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AgentField1` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AgentField2` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AgentField3` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AgentField4` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AgentField5` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `InternationalPlacementTest` tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  `InternationalCertification` tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  `InternationalAndOr` tinyint(1) DEFAULT '0' COMMENT '0-Or, 1-And',
  `LocalPlacementTest` tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  `LocalCertification` tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  `LocalAndOr` tinyint(1) DEFAULT '0' COMMENT '0-Or, 1-And',
  `InternationalPlcamenetTestProcessingFee` binary(1) NOT NULL DEFAULT '0' COMMENT '1: yes 0: no',
  `InternationalCertificationProcessingFee` binary(1) NOT NULL DEFAULT '0' COMMENT '1: yes 0: no',
  `LocalPlcamenetTestProcessingFee` binary(1) NOT NULL DEFAULT '0' COMMENT '1: yes 0: no',
  `LocalCertificationProcessingFee` binary(1) NOT NULL DEFAULT '0' COMMENT '1: yes 0: no',
  `Completionofyears` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `HijriDate` int(5) DEFAULT NULL,
  `HijriDateOptions` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `NoofWarnings` tinyint(3) DEFAULT '0' COMMENT 'No of Maximum warnings (Disciplinary Action)',
  `AddDrop` binary(1) NOT NULL,
  `MarksAppeal` smallint(5) NOT NULL DEFAULT '0' COMMENT '0:Take Latest,1:Take Highest',
  `TakeMarks` smallint(5) NOT NULL DEFAULT '0' COMMENT '0:Marks by average;1:Marks by rank',
  `marksdistributiontype` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0: course(subject) based 1: lecture based',
  `Failedsubjects` binary(1) NOT NULL DEFAULT '0' COMMENT '0:push failed subjects 1: ignore failed subjects',
  `Branch` binary(1) NOT NULL,
  `Office` binary(1) NOT NULL,
  `Venue` binary(1) NOT NULL,
  `Scheme` binary(1) NOT NULL,
  `NameDtlCount` int(11) NOT NULL,
  `NameDtl1` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `NameDtl2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `NameDtl3` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `NameDtl4` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `NameDtl5` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtlCount` int(10) NOT NULL,
  `ExtarIdDtl1` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl3` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl4` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl5` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl6` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl7` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl8` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl9` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl10` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl11` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl12` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl13` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl14` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl15` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl16` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl17` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl18` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl19` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ExtarIdDtl20` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `inventoryIdType` tinyint(4) NOT NULL,
  `inventoryPrefix` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `inventoryIdFormat` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `creditTransferIdType` int(11) NOT NULL,
  `creditTransferPrefix` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `creditTransferIdFormat` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ResetCreditTransferSeq` int(11) NOT NULL,
  `exemptionIdType` int(11) NOT NULL,
  `exemptionPrefix` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `exemptionIdFormat` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ResetExemptionSeq` int(11) NOT NULL,
  `auditPaperIdType` int(11) NOT NULL,
  `auditPaperPrefix` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `auditPaperIdFormat` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ResetAuditPaperSeq` int(11) NOT NULL,
  `changeModeIdType` int(11) NOT NULL,
  `changeModePrefix` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `changeModeIdFormat` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ResetChangeModeSeq` int(11) NOT NULL,
  `changeStatusIdType` int(11) DEFAULT NULL,
  `changeStatusPrefix` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `changeStatusIdFormat` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ResetChangeStatusSeq` int(11) DEFAULT NULL,
  PRIMARY KEY (`idConfig`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_corporate_upload`;
CREATE TABLE `tbl_corporate_upload` (
  `upl_id` int(11) NOT NULL AUTO_INCREMENT,
  `upl_corporate_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL DEFAULT '0' COMMENT 'batch_registration',
  `batch_exam_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK batch_exam_registration',
  `invoice_id` int(11) DEFAULT '0',
  `type` int(11) DEFAULT NULL COMMENT '0=batch registration, 1=proof of payment, 2=inhouse document, 3=supporting docoument',
  `upl_filename` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `upl_filerename` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `upl_filelocation` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `upl_filesize` int(11) NOT NULL,
  `upl_mimetype` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `upl_upddate` datetime NOT NULL,
  `upl_upduser` int(11) NOT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '	type=1 -> (0=entry,1=approved,2=rejected); type=2 -> (0=disabled,1=active) ',
  `status_date` datetime DEFAULT NULL,
  `document_type` int(11) DEFAULT NULL COMMENT 'ref table: tbl_definationms ',
  PRIMARY KEY (`upl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `tbl_corporate_upload_data`;
CREATE TABLE `tbl_corporate_upload_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upl_id` int(11) NOT NULL COMMENT 'tbl_corporate_upload',
  `idnumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `race` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `membership` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `created_role` datetime NOT NULL,
  `created_iduser` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_countries`;
CREATE TABLE `tbl_countries` (
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `idCountry` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CountryName` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DefaultLanguage` int(11) DEFAULT NULL,
  `CountryCode` int(10) NOT NULL DEFAULT '123',
  `Alias` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CountryIso` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CountryISO3` char(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` binary(1) DEFAULT NULL COMMENT '1:Active, 0:Inactive',
  `CurrencyShortName` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CurrencySymbol` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CurrencyName` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Nationality` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imf_ranking` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCountry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Country and their ISO Formats';


DROP TABLE IF EXISTS `tbl_coupon`;
CREATE TABLE `tbl_coupon` (
  `cpn_id` int(11) NOT NULL AUTO_INCREMENT,
  `cpn_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpn_description` text COLLATE utf8mb4_unicode_ci,
  `cpn_code_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=System Generated, 2=User Defined',
  `cpn_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpn_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Unlimited, 2=Limited',
  `cpn_limit` int(11) NOT NULL DEFAULT '1' COMMENT 'redemption limit',
  `cpn_category_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Individual, 3=Corporate, 4=Non-Corporate',
  `cpn_member_id` int(11) NOT NULL DEFAULT '0',
  `cpn_program_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_program',
  `cpn_subject_id` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_subjectmaster',
  `cpn_calculation_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Percentage, 2=Amount',
  `cpn_calculation_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `cpn_minimum_purchase` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'minimum amount to illegible for coupon ',
  `cpn_currency_id` int(11) NOT NULL DEFAULT '1',
  `cpn_start_date` datetime DEFAULT NULL COMMENT 'start date',
  `cpn_end_date` datetime DEFAULT NULL COMMENT 'expiry date',
  `cpn_serialize` int(11) NOT NULL DEFAULT '0' COMMENT '0=Nope 1=Serialized',
  `cpn_count` int(11) NOT NULL DEFAULT '1' COMMENT 'number of generated',
  `cpn_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `cpn_created_by` int(11) NOT NULL DEFAULT '0',
  `cpn_created_at` datetime DEFAULT NULL,
  `cpn_updated_by` int(11) NOT NULL DEFAULT '0',
  `cpn_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`cpn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_coupon_application`;
CREATE TABLE `tbl_coupon_application` (
  `cpna_id` int(11) NOT NULL AUTO_INCREMENT,
  `cpna_cpn_id` int(11) NOT NULL DEFAULT '0',
  `cpna_cpnd_id` int(11) NOT NULL DEFAULT '0',
  `cpna_student_id` int(11) NOT NULL DEFAULT '0',
  `cpna_corporate_id` int(11) NOT NULL DEFAULT '0',
  `cpna_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `cpna_created_at` datetime DEFAULT NULL,
  `cpna_updated_at` datetime DEFAULT NULL,
  `cpna_invoice_id` int(11) DEFAULT '0',
  PRIMARY KEY (`cpna_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_coupon_detail`;
CREATE TABLE `tbl_coupon_detail` (
  `cpnd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cpnd_cpn_id` int(11) NOT NULL DEFAULT '0',
  `cpnd_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpnd_usage_count` int(11) NOT NULL DEFAULT '0' COMMENT 'increment every time coupon is used, mmkay?',
  `cpnd_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `cpnd_created_at` datetime DEFAULT NULL,
  `cpnd_created_by` int(11) NOT NULL DEFAULT '0',
  `cpnd_updated_at` datetime DEFAULT NULL,
  `cpnd_updated_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cpnd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_coursemaster`;
CREATE TABLE `tbl_coursemaster` (
  `IdCoursemaster` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `CourseName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ArabicName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdCoursemaster`),
  KEY `tbl_coursemaster_ibfk_1` (`UpdUser`),
  CONSTRAINT `tbl_coursemaster_ibfk_1` FOREIGN KEY (`UpdUser`) REFERENCES `tbl_user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_coursetype`;
CREATE TABLE `tbl_coursetype` (
  `IdCourseType` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CourseType` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Bahasaindonesia` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MandatoryCreditHrs` binary(1) NOT NULL DEFAULT '0' COMMENT '1:Yes,0:No',
  `MandatoryAmount` binary(1) NOT NULL DEFAULT '0' COMMENT '1:Yes,0:No',
  `CourseExam` binary(1) NOT NULL DEFAULT '0' COMMENT '1:Yes,0:No',
  `ExamTimeTable` bigint(20) NOT NULL DEFAULT '0',
  `Practical` bigint(20) DEFAULT NULL,
  `PassFailGrade` bigint(20) DEFAULT NULL,
  `Active` binary(1) DEFAULT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime DEFAULT NULL,
  `UpdUser` bigint(20) unsigned DEFAULT NULL,
  `Audit` binary(1) DEFAULT NULL COMMENT '1:Yes;0:No',
  `Project` binary(1) DEFAULT '\0' COMMENT '1:Yes;0:No',
  `ClassTimetable` binary(1) DEFAULT '\0' COMMENT '1:Yes,0:No',
  PRIMARY KEY (`IdCourseType`),
  KEY `UpdUser` (`UpdUser`),
  CONSTRAINT `tbl_coursetype_ibfk_1` FOREIGN KEY (`UpdUser`) REFERENCES `tbl_user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_coursetypedetails`;
CREATE TABLE `tbl_coursetypedetails` (
  `IdCourseTypeDetails` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdCourseType` bigint(20) unsigned NOT NULL,
  `ComponentName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleteFlag` binary(1) NOT NULL DEFAULT '0',
  `Active` binary(1) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  KEY `IdCourseTypeDetails` (`IdCourseTypeDetails`),
  KEY `IdCourseType` (`IdCourseType`),
  CONSTRAINT `tbl_coursetypedetails_ibfk_1` FOREIGN KEY (`IdCourseType`) REFERENCES `tbl_coursetype` (`IdCourseType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_course_group_student_mapping`;
CREATE TABLE `tbl_course_group_student_mapping` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdCourseTaggingGroup` bigint(20) NOT NULL,
  `IdStudent` bigint(20) NOT NULL,
  `IdSubject` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_course_tagging_group`;
CREATE TABLE `tbl_course_tagging_group` (
  `IdCourseTaggingGroup` int(11) NOT NULL AUTO_INCREMENT,
  `IdLecturer` bigint(20) NOT NULL,
  `IdSemester` int(11) NOT NULL COMMENT 'fk tbl_intake',
  `IdProgram` bigint(20) unsigned NOT NULL,
  `IdProgramScheme` int(11) DEFAULT NULL,
  `IdLandscape` int(11) DEFAULT NULL,
  `SemCode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdSubject` bigint(20) NOT NULL,
  `GroupName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GroupCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdUniversity` bigint(20) NOT NULL,
  `maxstud` int(11) NOT NULL,
  `remark` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sc_date` date NOT NULL,
  `reservation` int(11) NOT NULL DEFAULT '0',
  `IdBranch` int(11) NOT NULL,
  `VerifyBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_start_date` date DEFAULT NULL,
  `class_end_date` date DEFAULT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `migrate_by` int(11) DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  PRIMARY KEY (`IdCourseTaggingGroup`),
  KEY `IdProgram` (`IdProgram`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_cpd_compliance`;
CREATE TABLE `tbl_cpd_compliance` (
  `cpdc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cpdc_hours` int(11) NOT NULL,
  `cpdc_type` int(11) DEFAULT NULL,
  `cpdc_description` text NOT NULL,
  `cpdc_year` int(11) NOT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`cpdc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_cpd_default_activities`;
CREATE TABLE `tbl_cpd_default_activities` (
  `def_id` int(11) NOT NULL AUTO_INCREMENT,
  `def_cpd_learning_id` int(11) NOT NULL COMMENT 'pk: tbl_definationms = ''CPD Learning''',
  `def_cpd_category_id` int(11) NOT NULL COMMENT 'pk: tbl_definationms = ''CPD Category''',
  `def_cpd_activity_id` int(11) NOT NULL COMMENT 'pk: tbl_definationms = ''CPD Activity''',
  `def_cpd_descriptions` varchar(100) DEFAULT NULL COMMENT 'CPD Hours Desc',
  `def_cpd_duration` decimal(11,0) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `def_cpd_status` int(11) NOT NULL COMMENT '0= Not Active, 1=Active',
  PRIMARY KEY (`def_id`),
  KEY `def_cpd_learning_id` (`def_cpd_learning_id`),
  KEY `def_cpd_category_id` (`def_cpd_category_id`),
  KEY `def_cpd_activity_id` (`def_cpd_activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_cpd_details`;
CREATE TABLE `tbl_cpd_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spid` int(11) NOT NULL COMMENT 'pk : student_profile',
  `cpd_cpdm_id` int(11) DEFAULT NULL COMMENT 'pk : tbl_cpd_material',
  `cpd_learning_id` int(11) NOT NULL COMMENT 'pk: tbl_definationms = ''CPD Learning''',
  `cpd_category_id` int(11) NOT NULL COMMENT 'pk: tbl_definationms = ''CPD Category''',
  `cpd_activity_id` int(11) NOT NULL COMMENT 'pk: tbl_definationms = ''CPD Activity''',
  `cpd_content_id` int(11) DEFAULT NULL,
  `cpd_title` varchar(55) DEFAULT NULL,
  `cpd_venue` varchar(55) DEFAULT NULL,
  `cpd_description` varchar(250) DEFAULT NULL,
  `cpd_provider` varchar(55) DEFAULT NULL,
  `date_started` date DEFAULT NULL,
  `date_completion` date DEFAULT NULL,
  `cpd_duration` decimal(11,0) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `cpd_corp_id` int(11) DEFAULT NULL COMMENT 'pk : takaful_operator',
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `cpd_status` int(11) NOT NULL COMMENT '0= Submitted, 1=Approved',
  PRIMARY KEY (`id`),
  KEY `spid` (`spid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_cpd_doc`;
CREATE TABLE `tbl_cpd_doc` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `cpd_id` int(11) DEFAULT NULL,
  `cpde_id` int(11) DEFAULT NULL,
  `doc_content_id` int(11) DEFAULT NULL,
  `doc_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_hashname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_extension` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_filesize` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`doc_id`),
  KEY `sp_id` (`sp_id`),
  KEY `cpd_id` (`cpd_id`),
  KEY `cpde_id` (`cpde_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_cpd_exemptions`;
CREATE TABLE `tbl_cpd_exemptions` (
  `cpde_id` int(11) NOT NULL AUTO_INCREMENT,
  `cpde_mr_id` int(11) NOT NULL COMMENT 'pk : membership_registration',
  `cpde_cpdc_id` int(11) NOT NULL COMMENT 'pk : tbl_cpd_compliace',
  `cpde_type` int(11) NOT NULL COMMENT '1 = Exemption, 2 = Declaration',
  `cpde_description` text,
  `cpde_status` int(11) DEFAULT NULL COMMENT '1 = Applied, 2 = Approved',
  `created_by` tinyint(4) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`cpde_id`),
  KEY `cpde_cpdc_id` (`cpde_cpdc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_cpd_material`;
CREATE TABLE `tbl_cpd_material` (
  `cpdm_id` int(11) NOT NULL AUTO_INCREMENT,
  `cpdm_type` int(11) NOT NULL COMMENT 'pk: tbl_definationms = ''CPD Learning''',
  `cpdm_hours` int(11) NOT NULL,
  `cpdm_category` int(11) NOT NULL COMMENT 'pk: tbl_definationms = ''CPD Category''',
  `cpdm_activity` int(11) NOT NULL COMMENT 'pk: tbl_definationms = ''CPD Activity''',
  `cpdm_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpdm_content` int(11) DEFAULT NULL COMMENT 'pk: tbl_definationms = ''CPD Content''',
  `cpdm_venue` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpdm_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpdm_desc` text COLLATE utf8mb4_unicode_ci,
  `cpdm_filename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpdm_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpdm_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpdm_extension` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpdm_filesize` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `cpdm_corp_id` int(11) DEFAULT NULL COMMENT 'pk : takaful_operator',
  `date_started` datetime DEFAULT NULL,
  `date_completion` datetime DEFAULT NULL,
  PRIMARY KEY (`cpdm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_cpd_material_tagging`;
CREATE TABLE `tbl_cpd_material_tagging` (
  `cpdmt_id` int(11) NOT NULL AUTO_INCREMENT,
  `cpdmt_std_id` int(11) NOT NULL COMMENT 'pk : student_profile',
  `cpdmt_cpdm_id` int(11) NOT NULL COMMENT 'pk : tbl_cpd_material',
  `created_by` tinyint(4) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`cpdmt_id`),
  KEY `cpdmt_mr_id` (`cpdmt_std_id`),
  KEY `cpdmt_cpdm_id` (`cpdmt_cpdm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_credittransfer`;
CREATE TABLE `tbl_credittransfer` (
  `IdCreditTransfer` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdStudentRegistration` bigint(20) DEFAULT NULL,
  `IdCTApplication` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdApplication` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentapplication',
  `IdProgram` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_program',
  `Status` binary(1) DEFAULT NULL COMMENT '1:Approved,0:Rejected',
  `ApplicationStatus` bigint(20) DEFAULT NULL,
  `DateApplied` datetime DEFAULT NULL,
  `AppliedBy` int(11) NOT NULL,
  `DateApproved` datetime DEFAULT NULL,
  `ApprovedBy` int(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `Comments` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `InstitutionType` int(11) NOT NULL,
  `semesterId` int(11) NOT NULL,
  `applicationType` int(11) NOT NULL,
  `subjectreg_id` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoiceid_0` int(11) NOT NULL DEFAULT '0',
  `invoiceid_1` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdCreditTransfer`),
  KEY `UpdUser` (`UpdUser`),
  KEY `IdProgram` (`IdProgram`),
  KEY `IdApplication` (`IdApplication`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_credittransferhistory`;
CREATE TABLE `tbl_credittransferhistory` (
  `cth_id` int(11) NOT NULL AUTO_INCREMENT,
  `cth_oldstatus` int(11) NOT NULL,
  `cth_ctid` int(11) NOT NULL,
  `cth_newstatus` int(11) NOT NULL,
  `cth_upddate` date NOT NULL,
  `cth_upduser` int(11) NOT NULL,
  PRIMARY KEY (`cth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_credittransfersubjects`;
CREATE TABLE `tbl_credittransfersubjects` (
  `IdCreditTransferSubjects` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdCreditTransfer` bigint(20) unsigned DEFAULT NULL,
  `IdSubject` bigint(20) unsigned DEFAULT NULL,
  `IdSemester` bigint(20) DEFAULT NULL,
  `IdSemesterMain` bigint(20) DEFAULT NULL,
  `IdCourse` bigint(20) DEFAULT NULL,
  `CreditHours` int(3) DEFAULT NULL,
  `creditStatus` tinyint(5) DEFAULT NULL COMMENT '1:approve, 0: disapprove',
  `ApplicationDate` date DEFAULT NULL,
  `AppliedBy` bigint(1) DEFAULT NULL,
  `ApprovedDate` date DEFAULT NULL,
  `Approvedby` bigint(20) DEFAULT NULL,
  `ApplicationStatus` bigint(20) DEFAULT NULL,
  `IdInstitution` bigint(20) DEFAULT NULL,
  `IdQualification` bigint(20) DEFAULT NULL,
  `IdSpecialization` bigint(20) DEFAULT NULL,
  `EquivalentCourse` bigint(20) DEFAULT NULL,
  `EquivalentGrade` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EquivalentCreditHours` int(3) DEFAULT NULL,
  `Marks` bigint(20) DEFAULT NULL,
  `Comments` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disapprovalComment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `institutionName` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge` int(11) NOT NULL,
  `charge2` int(11) NOT NULL DEFAULT '0',
  `invoiceid_0` int(11) NOT NULL DEFAULT '0',
  `invoiceid_1` int(11) NOT NULL DEFAULT '0',
  `paid_0` int(11) NOT NULL DEFAULT '0',
  `paid_1` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdCreditTransferSubjects`),
  KEY `IdCreditTransfer` (`IdCreditTransfer`),
  KEY `IdSubject` (`IdSubject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_credittransfersubjectsmore`;
CREATE TABLE `tbl_credittransfersubjectsmore` (
  `csm_id` int(11) NOT NULL AUTO_INCREMENT,
  `csm_creditTransferId` int(11) NOT NULL,
  `csm_creditTransferSubId` int(11) NOT NULL,
  `csm_type` int(11) NOT NULL,
  `csm_subjectinternal` int(11) NOT NULL,
  `csm_subjectexternal` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `csm_upduser` int(11) NOT NULL,
  `csm_upddate` date NOT NULL,
  PRIMARY KEY (`csm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_credittransferuploads`;
CREATE TABLE `tbl_credittransferuploads` (
  `IdCreditTransferSubjectsDocs` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdCreditTransferSubjects` bigint(20) unsigned NOT NULL,
  `FileLocation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FileName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UploadedFilename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FileSize` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MIMEType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `Comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DocumentsVerified` binary(1) NOT NULL DEFAULT '0',
  `DocumentsApproved` binary(1) NOT NULL DEFAULT '0',
  `CreditTransferComments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documentcategory` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`IdCreditTransferSubjectsDocs`),
  KEY `IdCreditTransferSubjects` (`IdCreditTransferSubjects`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_currency`;
CREATE TABLE `tbl_currency` (
  `cur_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cur_code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Currency Code',
  `cur_desc` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Currency Description',
  `cur_desc_default_language` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Currency Description (Default Language). Malay/ Hindi',
  `cur_symbol_prefix` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cur_symbol_suffix` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cur_default` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Default status',
  `cur_decimal` int(11) NOT NULL DEFAULT '2',
  `cur_status` tinyint(1) NOT NULL COMMENT 'Active/ Inactive  (1/0)',
  PRIMARY KEY (`cur_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_currency_rate`;
CREATE TABLE `tbl_currency_rate` (
  `cr_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cr_cur_id` bigint(20) NOT NULL COMMENT 'FK tbl_currency',
  `cr_exchange_rate` decimal(10,4) NOT NULL,
  `cr_min_rate` decimal(10,4) NOT NULL,
  `cr_max_rate` decimal(10,4) NOT NULL,
  `cr_effective_date` date NOT NULL,
  `cr_update_by` int(11) NOT NULL COMMENT 'FK tbl_user',
  `cr_update_date` datetime NOT NULL,
  PRIMARY KEY (`cr_id`),
  KEY `cr_effective_date` (`cr_effective_date`),
  KEY `cr_cur_id` (`cr_cur_id`,`cr_update_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Storing currency exchange rate';


DROP TABLE IF EXISTS `tbl_currency_rate_history`;
CREATE TABLE `tbl_currency_rate_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cr_id` bigint(20) NOT NULL COMMENT 'FK tbl_currency_rate',
  `cr_cur_id` bigint(20) NOT NULL COMMENT 'FK tbl_currency',
  `cr_exchange_rate` decimal(10,4) NOT NULL,
  `cr_min_rate` decimal(10,4) NOT NULL,
  `cr_max_rate` decimal(10,4) NOT NULL,
  `cr_effective_date` date NOT NULL,
  `action` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_update_by` int(11) NOT NULL COMMENT 'FK tbl_user',
  `cr_update_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cr_effective_date` (`cr_effective_date`),
  KEY `cr_cur_id` (`cr_cur_id`,`cr_update_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Storing currency exchange rate history';


DROP TABLE IF EXISTS `tbl_deanlist`;
CREATE TABLE `tbl_deanlist` (
  `IdDeanList` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdCollege` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_collegemaster',
  `IdStaff` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  `FromDate` date NOT NULL,
  `ToDate` date DEFAULT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdDeanList`),
  KEY `tbl_deanlist_ibfk_1` (`UpdUser`),
  KEY `tbl_deanlist_ibfk_2` (`IdCollege`),
  KEY `tbl_deanlist_ibfk_3` (`IdStaff`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_deanlist_history`;
CREATE TABLE `tbl_deanlist_history` (
  `tdh_id` int(11) NOT NULL DEFAULT '0',
  `tdh_IdCollege` int(11) NOT NULL,
  `tdh_IdStaff` int(11) NOT NULL,
  `tdh_fromDate` date NOT NULL,
  `tdh_toDate` date NOT NULL,
  `tdh_updUser` int(11) NOT NULL,
  `tdh_updDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_definationms`;
CREATE TABLE `tbl_definationms` (
  `idDefinition` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idDefType` int(10) unsigned NOT NULL,
  `DefinitionCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DefinitionDesc` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: Active , 1 : InActive',
  `BahasaIndonesia` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BhasaIndonesia` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `defOrder` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`idDefinition`),
  KEY `FK_tbl_DefinationMS_1` (`idDefType`),
  KEY `FK_idDefType` (`idDefType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Detail Data Defination';


DROP TABLE IF EXISTS `tbl_definationtypems`;
CREATE TABLE `tbl_definationtypems` (
  `idDefType` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `defTypeDesc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  `BahasaIndonesia` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DefinitionTypeDesc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idDefType`),
  KEY `defTypeDesc` (`defTypeDesc`(191)),
  KEY `Active` (`Active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Keep Master Data Defination';


DROP TABLE IF EXISTS `tbl_departmentmaster`;
CREATE TABLE `tbl_departmentmaster` (
  `IdDepartment` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DepartmentType` tinyint(1) NOT NULL COMMENT '0:Dept under College, 1:Dept under branch',
  `IdCollege` bigint(20) unsigned NOT NULL,
  `DepartmentName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ArabicName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ShortName` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DeptCode` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` tinyint(1) NOT NULL COMMENT '0:Inactive,1:Active',
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`IdDepartment`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_departmentmaster_bak`;
CREATE TABLE `tbl_departmentmaster_bak` (
  `IdDepartment` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DepartmentType` binary(1) NOT NULL COMMENT '0:Dept under College, 1:Dept under branch',
  `IdCollege` bigint(20) unsigned NOT NULL,
  `DepartmentName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ArabicName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ShortName` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DeptCode` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` binary(1) NOT NULL COMMENT '0:Inactive,1:Active',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`IdDepartment`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_disciplinaryaction`;
CREATE TABLE `tbl_disciplinaryaction` (
  `IdDisciplinaryAction` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) unsigned NOT NULL COMMENT 'FK To Student Application',
  `IdDispciplinaryActionType` bigint(20) unsigned NOT NULL COMMENT 'FK To Definition MS( Disciplinary Actions)',
  `DateOfMistake` date NOT NULL,
  `ReportingDate` date NOT NULL,
  `DetailedNarration` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PenaltyDescription` varchar(205) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PenaltyAmount` decimal(12,2) NOT NULL,
  `WarningIssued` binary(1) NOT NULL,
  `SemesterId` bigint(20) unsigned NOT NULL,
  `UploadNotice` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `Approve` binary(1) NOT NULL,
  PRIMARY KEY (`IdDisciplinaryAction`),
  KEY `IdApplication` (`IdApplication`),
  KEY `IdDispciplinaryActionType` (`IdDispciplinaryActionType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_disciplinaryactiondetails`;
CREATE TABLE `tbl_disciplinaryactiondetails` (
  `iddisciplinayactiondetails` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdDisciplinaryAction` bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_disciplinaryaction',
  `idSubject` bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_subject',
  `idSemester` bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_semester',
  `DisplinaryAction` bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_definitionms',
  `deleteFlag` binary(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`iddisciplinayactiondetails`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_disciplinaryactionmailsentto`;
CREATE TABLE `tbl_disciplinaryactionmailsentto` (
  `IdDisciplinaryActionMailSentTo` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdDisciplinaryAction` bigint(20) NOT NULL COMMENT 'FK to tbl_disciplinaryaction',
  `DiscMsgCollegeHead` binary(1) NOT NULL COMMENT '0:Mail-Not-Sent 1:Mail-Sent',
  `DiscMsgParent` binary(1) NOT NULL COMMENT '0:Mail-Not-Sent 1:Mail-Sent',
  `DiscMsgSponor` binary(1) NOT NULL COMMENT '0:Mail-Not-Sent 1:Mail-Sent',
  `DiscMsgRegistrar` binary(1) NOT NULL COMMENT '0:Mail-Not-Sent 1:Mail-Sent',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDisciplinaryActionMailSentTo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_disciplinaryactionmaster`;
CREATE TABLE `tbl_disciplinaryactionmaster` (
  `idDisciplinaryActionMaster` bigint(20) NOT NULL AUTO_INCREMENT,
  `idDisciplinaryActionType` bigint(20) NOT NULL,
  `DiscMsgCollegeHead` tinyint(1) DEFAULT '0',
  `DiscMsgParent` tinyint(1) DEFAULT '0',
  `DiscMsgSponor` tinyint(1) DEFAULT '0',
  `DiscMsgRegistar` tinyint(1) DEFAULT '0',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`idDisciplinaryActionMaster`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_discount`;
CREATE TABLE `tbl_discount` (
  `discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `discount_type` int(11) DEFAULT NULL,
  `mode_of_program` int(11) DEFAULT NULL COMMENT 'type 96',
  `registration_item_id` int(11) DEFAULT '889' COMMENT '889=course, 879=exam',
  `discount_calculation_type` int(11) DEFAULT NULL,
  `discount_calculation_amount` decimal(20,2) DEFAULT NULL,
  `discount_start_date` date DEFAULT NULL,
  `discount_end_date` date DEFAULT NULL,
  `effective_date` date DEFAULT NULL,
  `effective_date_end` date DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL COMMENT '1: Active, 0: Inactive',
  `createdby` int(11) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updateddt` datetime DEFAULT NULL,
  PRIMARY KEY (`discount_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `tbl_discount_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_discount_category`;
CREATE TABLE `tbl_discount_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_id` int(11) DEFAULT NULL COMMENT 'FK tbl_discount.discount_id',
  `category_id` int(11) DEFAULT NULL COMMENT 'FK tbl_category.category_id',
  `createdby` int(11) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_discount_company`;
CREATE TABLE `tbl_discount_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_id` int(11) DEFAULT NULL COMMENT 'FK tbl_discount.discount_id',
  `company_id` int(11) DEFAULT NULL COMMENT 'FK tbl_takafuloperator',
  `createdby` int(11) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_discount_eligibility`;
CREATE TABLE `tbl_discount_eligibility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_id` int(11) DEFAULT NULL COMMENT 'FK tbl_discount.discount_id',
  `individual_type` int(11) DEFAULT NULL COMMENT '1: Academician, 2: Student',
  `createdby` int(11) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_discount_program`;
CREATE TABLE `tbl_discount_program` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'FK tbl_discount',
  `IdProgram` int(11) DEFAULT NULL COMMENT 'FK tbl_program.IdProgram',
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedAt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_discount_range_user`;
CREATE TABLE `tbl_discount_range_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_id` int(11) NOT NULL COMMENT 'fk tbl_discount',
  `range_id` int(11) NOT NULL COMMENT 'fk tbl_range_user',
  `discount_calculation_amount` decimal(20,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_documentchecklist_dcl`;
CREATE TABLE `tbl_documentchecklist_dcl` (
  `dcl_Id` int(11) NOT NULL AUTO_INCREMENT,
  `dcl_stdCtgy` int(11) NOT NULL,
  `dcl_programScheme` int(11) NOT NULL,
  `dcl_sectionid` int(11) NOT NULL DEFAULT '0',
  `dcl_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dcl_malayName` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dcl_desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dcl_type` int(11) NOT NULL,
  `dcl_activeStatus` int(11) NOT NULL,
  `dcl_priority` int(11) NOT NULL,
  `dcl_uplStatus` int(11) NOT NULL DEFAULT '0' COMMENT '0: Download 1:Upload',
  `dcl_uplType` int(11) NOT NULL COMMENT 'fk tbl_definationms (type:10)',
  `dcl_finance` tinyint(2) NOT NULL,
  `dcl_mandatory` int(11) NOT NULL,
  `dcl_specific` int(11) NOT NULL DEFAULT '0',
  `dcl_addDate` datetime NOT NULL,
  `dcl_addBy` int(11) NOT NULL,
  `dcl_modDate` datetime NOT NULL,
  `dcl_modBy` int(11) NOT NULL,
  PRIMARY KEY (`dcl_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_documentdetails`;
CREATE TABLE `tbl_documentdetails` (
  `idDocumentDetails` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentapplication',
  `FileLocation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FileName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UploadedFilename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Uploaded file name with the uploaded datetime attached to the filename',
  `FileSize` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MIMEType` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `UpdUser` bigint(20) unsigned DEFAULT NULL,
  `Comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documentcategory` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`idDocumentDetails`),
  KEY `UpdUser` (`UpdUser`),
  KEY `IdApplication` (`IdApplication`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_education_subject`;
CREATE TABLE `tbl_education_subject` (
  `IdEducationSubject` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudEduDtl` bigint(20) NOT NULL,
  `IdSubject` bigint(20) NOT NULL,
  `IdSubjectGrade` bigint(20) NOT NULL,
  PRIMARY KEY (`IdEducationSubject`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_emailtemplate`;
CREATE TABLE `tbl_emailtemplate` (
  `idTemplate` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TemplateName` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TemplateFrom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TemplateFromDesc` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TemplateSubject` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TemplateHeader` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TemplateBody` varchar(7277) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TemplateFooter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Deleteflag` binary(1) NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` datetime NOT NULL,
  `idDefinition` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`idTemplate`),
  KEY `FK_tbl_emailtemplate_1` (`idDefinition`),
  KEY `FK_tbl_emailtemplate_2` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_examcenter_invigilator`;
CREATE TABLE `tbl_examcenter_invigilator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examcenter_id` int(11) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ic_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ismain` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ec_invigilatorstatus` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `upd_by` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_examcenter_server`;
CREATE TABLE `tbl_examcenter_server` (
  `svr_id` int(11) NOT NULL AUTO_INCREMENT,
  `svr_ec_id` int(11) NOT NULL,
  `svr_hostname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `svr_ipaddress` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `svr_active` int(1) NOT NULL COMMENT '1=active, 0=disabled',
  PRIMARY KEY (`svr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_examcompany`;
CREATE TABLE `tbl_examcompany` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examcenter_id` int(11) DEFAULT NULL COMMENT 'FK tbl_exam_center',
  `takafuloperator_id` int(11) DEFAULT NULL COMMENT 'FK tbl_takafuloperator',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `upd_by` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_examination_assessment_item`;
CREATE TABLE `tbl_examination_assessment_item` (
  `IdExaminationAssessmentType` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdDescription` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DescriptionDefaultlang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` int(11) NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`IdExaminationAssessmentType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_examination_assessment_type`;
CREATE TABLE `tbl_examination_assessment_type` (
  `IdExaminationAssessmentType` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSemester` int(11) NOT NULL,
  `IdDescription` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DescriptionDefaultlang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL,
  `Type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: standard 1:CE',
  `FinalExam` tinyint(4) NOT NULL COMMENT '0:No 1:Yes',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`IdExaminationAssessmentType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_examination_resit_configuration`;
CREATE TABLE `tbl_examination_resit_configuration` (
  `IdExaminationResitConfiguration` bigint(20) NOT NULL AUTO_INCREMENT,
  `ApplicationCount` int(10) NOT NULL,
  `MinimumGrade` bigint(20) NOT NULL COMMENT 'IdGradeSetUp from table tbl_gradesetup',
  `FinalMarks` bigint(20) NOT NULL COMMENT 'tbl_defination - ID 284 and 285',
  `IdUniversity` bigint(20) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  `UpdatedBy` bigint(20) NOT NULL,
  PRIMARY KEY (`IdExaminationResitConfiguration`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_examroom`;
CREATE TABLE `tbl_examroom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examcenter_id` int(11) DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `floor_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quota` int(100) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `order` smallint(3) unsigned NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `upd_by` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_exam_center`;
CREATE TABLE `tbl_exam_center` (
  `ec_id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_default_language` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ec_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_short_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ec_start_date` date DEFAULT NULL,
  `ec_end_date` date DEFAULT NULL,
  `ec_active` int(11) NOT NULL,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `Default` int(11) NOT NULL DEFAULT '0',
  `is_local` tinyint(1) NOT NULL,
  `inhouse` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 : Public 1 : In-house',
  `is_learning_center` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:No 1:yes',
  PRIMARY KEY (`ec_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_feesetupdetail`;
CREATE TABLE `tbl_feesetupdetail` (
  `IdFeesSetupDetail` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdFeessetupMaster` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_feessetupmaster',
  `Description` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DebitAccount` int(100) NOT NULL,
  `CreditAccount` int(100) NOT NULL,
  `Amount` float(10,2) NOT NULL,
  `Mode` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  PRIMARY KEY (`IdFeesSetupDetail`),
  KEY `IdFeessetupMaster` (`IdFeessetupMaster`),
  KEY `Mode` (`Mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_feesetupmaster`;
CREATE TABLE `tbl_feesetupmaster` (
  `IdFeesSetupMaster` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `VoucherNo` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `StartingIntake` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_semester',
  `EndSemester` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_semester',
  `IdCategory` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `IdPlantype` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_plantype',
  `Description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'FK  to tbl_user',
  `Active` int(11) NOT NULL,
  PRIMARY KEY (`IdFeesSetupMaster`),
  KEY `StartingIntake` (`StartingIntake`),
  KEY `EndSemester` (`EndSemester`),
  KEY `IdCategory` (`IdCategory`),
  KEY `IdPlantype` (`IdPlantype`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_fee_category`;
CREATE TABLE `tbl_fee_category` (
  `fc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fc_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fc_desc` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fc_desc_second_language` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fc_seq` int(10) unsigned NOT NULL DEFAULT '0',
  `fc_group` int(11) NOT NULL,
  `fc_generateyear` int(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` int(11) NOT NULL,
  PRIMARY KEY (`fc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_fee_setup`;
CREATE TABLE `tbl_fee_setup` (
  `IdFeeSetup` bigint(20) NOT NULL AUTO_INCREMENT,
  `FeeCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ChargeCode` bigint(20) DEFAULT NULL,
  `Description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DefaultLanguage` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ShortDescription` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FeeCategory` bigint(20) DEFAULT NULL,
  `ChargingPeriod` bigint(20) DEFAULT NULL,
  `Value` int(11) DEFAULT NULL,
  `Refundable` int(1) DEFAULT NULL,
  `Active` binary(1) DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `IdUniversity` bigint(20) DEFAULT NULL,
  `CompanyCode` bigint(20) DEFAULT NULL,
  `AccountCode` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdFeeSetup`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_financial_aid`;
CREATE TABLE `tbl_financial_aid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_financial_aid_course`;
CREATE TABLE `tbl_financial_aid_course` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `financial_aid_id` bigint(20) NOT NULL,
  `semester_id` bigint(20) NOT NULL,
  `total_course` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_financial_aid_coverage`;
CREATE TABLE `tbl_financial_aid_coverage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `financial_aid_id` bigint(20) NOT NULL,
  `semester_id` bigint(20) NOT NULL,
  `fee_code` bigint(20) NOT NULL,
  `calculation_mode` tinyint(1) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_gpacalculation`;
CREATE TABLE `tbl_gpacalculation` (
  `Idgpacalculation` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) unsigned NOT NULL,
  `Gpa` decimal(10,2) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `IdApplication` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`Idgpacalculation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DELIMITER ;;

CREATE TRIGGER `gpa_after_insert` AFTER INSERT ON `tbl_gpacalculation` FOR EACH ROW
BEGIN   
   
    DECLARE cgpaamt float(10,2); 
    DECLARE iDSTUDREGISTRATION int; 

  	

 	SET cgpaamt = (SELECT AVG(Gpa) FROM `tbl_gpacalculation` WHERE tbl_gpacalculation.IdStudentRegistration=NEW.IdStudentRegistration GROUP BY NEW.IdStudentRegistration) ;   
     
        INSERT INTO tbl_cgpacalculation (IdRegistration,IdApplication,Cgpa,UpdDate,UpdUser) VALUES (NEW.IdStudentRegistration,NEW.	IdApplication,cgpaamt,NEW.UpdDate,NEW.UpdUser);  

    END;;

DELIMITER ;

DROP TABLE IF EXISTS `tbl_gpastudents`;
CREATE TABLE `tbl_gpastudents` (
  `IdGpaStudents` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) unsigned NOT NULL,
  `IdSemester` bigint(20) unsigned NOT NULL,
  `AcademicStatus` bigint(20) unsigned NOT NULL,
  `Marks` int(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`IdGpaStudents`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_gpa_calculation_mode`;
CREATE TABLE `tbl_gpa_calculation_mode` (
  `IdGpaCalculationmode` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdAcademicStatus` bigint(20) NOT NULL,
  `LandscapeType` bigint(20) NOT NULL,
  `Level` int(10) NOT NULL,
  `Semester` int(10) NOT NULL,
  PRIMARY KEY (`IdGpaCalculationmode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_grade`;
CREATE TABLE `tbl_grade` (
  `IdStudentGrade` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudent` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentregistration',
  `SemesterNumber` bigint(20) unsigned NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdStudentGrade`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_gradesetup`;
CREATE TABLE `tbl_gradesetup` (
  `IdGradeSetUp` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdProgram` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_program',
  `IdGradeSetUpMain` bigint(20) NOT NULL COMMENT 'fk to tbl_gradesetup_main',
  `IdSubject` bigint(20) unsigned DEFAULT NULL,
  `IdSemester` bigint(20) unsigned DEFAULT NULL,
  `IdScheme` bigint(20) DEFAULT NULL,
  `IdAward` bigint(20) DEFAULT NULL,
  `BasedOn` tinyint(4) DEFAULT NULL,
  `EffectiveDate` date DEFAULT NULL,
  `GradeName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GradeDesc` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GradePoint` decimal(10,2) NOT NULL,
  `MinPoint` decimal(10,2) NOT NULL,
  `MaxPoint` decimal(10,2) NOT NULL,
  `Rank` bigint(20) unsigned NOT NULL,
  `Pass` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DescEnglishName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DescArabicName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DefaultLanguage` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Grade` bigint(20) DEFAULT NULL,
  `deleteFlag` binary(1) DEFAULT '0',
  `Active` tinyint(4) NOT NULL,
  `Countable` tinyint(4) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`IdGradeSetUp`),
  KEY `IdProgram` (`IdProgram`),
  KEY `IdGradeSetUp` (`IdGradeSetUp`),
  KEY `IdGradeSetUpMain` (`IdGradeSetUpMain`),
  KEY `IdSubject` (`IdSubject`),
  KEY `IdSemester` (`IdSemester`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_gradesetup_main`;
CREATE TABLE `tbl_gradesetup_main` (
  `IdGradeSetUpMain` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdProgram` bigint(20) NOT NULL,
  `IdSubject` bigint(20) DEFAULT NULL,
  `IdIntake` int(11) DEFAULT NULL COMMENT 'fk tbl_intake',
  `IdSemester` bigint(11) DEFAULT NULL,
  `IdScheme` bigint(20) DEFAULT NULL,
  `IdAward` bigint(20) DEFAULT NULL,
  `BasedOn` tinyint(4) NOT NULL DEFAULT '2',
  `Active` tinyint(4) DEFAULT NULL,
  `EffectiveDate` date NOT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdGradeSetUpMain`),
  KEY `IdGradeSetUpMain` (`IdGradeSetUpMain`),
  KEY `IdProgram` (`IdProgram`),
  KEY `IdSubject` (`IdSubject`),
  KEY `IdSemester` (`IdSemester`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_graduation_attestation`;
CREATE TABLE `tbl_graduation_attestation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) unsigned NOT NULL,
  `opt_mohe` tinyint(1) unsigned NOT NULL,
  `opt_mofa` tinyint(1) unsigned NOT NULL,
  `opt_embassy` tinyint(1) unsigned NOT NULL,
  `opt_embassy_location` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `applied_date` datetime NOT NULL,
  `applied_by` int(11) unsigned NOT NULL,
  `applied_role` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` set('APPLIED','APPROVED','REJECTED') COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `approval_by` int(11) DEFAULT NULL,
  `approval_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_graduation_award`;
CREATE TABLE `tbl_graduation_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:Not Active 1: Active',
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifydt` datetime DEFAULT NULL,
  `modifyby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_graduation_award_program`;
CREATE TABLE `tbl_graduation_award_program` (
  `gap_id` int(11) NOT NULL AUTO_INCREMENT,
  `gap_award_id` int(11) NOT NULL COMMENT 'fk tbl_graduation_award',
  `gap_program_id` int(11) NOT NULL COMMENT 'fk tbl_program',
  `gap_createddt` datetime NOT NULL,
  `gap_createdby` int(11) NOT NULL,
  PRIMARY KEY (`gap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_graduation_award_student`;
CREATE TABLE `tbl_graduation_award_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL,
  `tbl_graduation_award_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_graduation_checklist`;
CREATE TABLE `tbl_graduation_checklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active 1: Active',
  `document` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:No 1: Yes (ada tagging doucment tak?)',
  `upload` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  `type` tinyint(11) NOT NULL COMMENT '1: Name 2: ID No 3:Address 4: ContactNo 5:Email',
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_graduation_checklist_student`;
CREATE TABLE `tbl_graduation_checklist_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_graduation_checklist_id` int(11) NOT NULL,
  `IdStudentRegistration` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_graduation_collection`;
CREATE TABLE `tbl_graduation_collection` (
  `gc_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL,
  `scroll_collection` tinyint(4) DEFAULT NULL COMMENT '1:Self Collect',
  `photo_collection` tinyint(4) DEFAULT NULL COMMENT '2:Courier',
  `scroll_address1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scroll_address2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scroll_postcode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scroll_country` int(11) DEFAULT NULL,
  `scroll_state` int(11) DEFAULT NULL,
  `scroll_state_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scroll_city` int(11) DEFAULT NULL,
  `scroll_city_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_address1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_address2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_postcode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_country` int(11) DEFAULT NULL,
  `photo_state` int(11) DEFAULT NULL,
  `photo_state_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_city` int(11) DEFAULT NULL,
  `photo_city_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gc_createddt` datetime NOT NULL,
  `gc_createdby` int(11) NOT NULL,
  `gc_modifyby` int(11) DEFAULT NULL,
  `gc_modifydt` datetime DEFAULT NULL,
  `gc_modify_role` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gc_invoice_id` int(11) DEFAULT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Not Paid 1:Paid',
  `receipt_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`gc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_graduation_collectioncertificate`;
CREATE TABLE `tbl_graduation_collectioncertificate` (
  `gc_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL,
  `scroll_collection` tinyint(4) DEFAULT NULL COMMENT '1:Self Collect',
  `photo_collection` tinyint(4) DEFAULT NULL COMMENT '2:Courier',
  `scroll_address1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scroll_address2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scroll_postcode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scroll_country` int(11) DEFAULT NULL,
  `scroll_state` int(11) DEFAULT NULL,
  `scroll_state_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scroll_city` int(11) DEFAULT NULL,
  `scroll_city_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_address1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_address2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_postcode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_country` int(11) DEFAULT NULL,
  `photo_state` int(11) DEFAULT NULL,
  `photo_state_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_city` int(11) DEFAULT NULL,
  `photo_city_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gc_createddt` datetime NOT NULL,
  `gc_createdby` int(11) NOT NULL,
  `gc_modifyby` int(11) DEFAULT NULL,
  `gc_modifydt` datetime DEFAULT NULL,
  `gc_modify_role` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gc_invoice_id` int(11) DEFAULT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Not Paid 1:Paid',
  `receipt_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`gc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_graduation_documents`;
CREATE TABLE `tbl_graduation_documents` (
  `gd_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gd_type` int(11) NOT NULL,
  `gd_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gd_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `created_role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_role` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`gd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `tbl_graduation_guests`;
CREATE TABLE `tbl_graduation_guests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL,
  `guests` int(11) DEFAULT NULL,
  `guest_approve` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `applied_date` datetime DEFAULT NULL,
  `applied_by` int(11) DEFAULT NULL,
  `applied_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approval_date` datetime DEFAULT NULL,
  `approval_by` int(11) DEFAULT NULL,
  `status` set('APPLIED','APPROVED','REJECTED') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1: Guest 2: Refreshment',
  `invoice_id` int(11) DEFAULT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Not Paid 1:Paid',
  `receipt_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `tbl_group_subject`;
CREATE TABLE `tbl_group_subject` (
  `IdGroupSubject` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSubject` bigint(20) NOT NULL,
  `GroupId` bigint(20) unsigned NOT NULL,
  `ProgramEntry` bigint(20) NOT NULL,
  `IdQualification` int(20) unsigned NOT NULL,
  `fieldofstudy` int(20) unsigned NOT NULL,
  `MinGradePoint` int(20) NOT NULL,
  `NoofSubject` int(20) NOT NULL,
  `IdProgramentryreq` int(11) NOT NULL,
  PRIMARY KEY (`IdGroupSubject`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_headofdepartmentlist`;
CREATE TABLE `tbl_headofdepartmentlist` (
  `IdHeadOfDepartmentList` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdDepartment` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  `IdStaff` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  `FromDate` date NOT NULL,
  `ToDate` date DEFAULT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdHeadOfDepartmentList`),
  KEY `tbl_headofdepartmentlist_ibfk_1` (`UpdUser`),
  KEY `tbl_headofdepartmentlist_ibfk_2` (`IdDepartment`),
  KEY `tbl_headofdepartmentlist_ibfk_3` (`IdStaff`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_block`;
CREATE TABLE `tbl_hostel_block` (
  `IdHostelBlock` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ShortName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DefaultLang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Address1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` int(11) DEFAULT NULL,
  `State` int(11) DEFAULT NULL,
  `Zipcode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Country` int(11) DEFAULT NULL,
  `Phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` int(11) DEFAULT '1',
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  PRIMARY KEY (`IdHostelBlock`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_config`;
CREATE TABLE `tbl_hostel_config` (
  `Idhostelconfiguration` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUniversity` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Applicableforapproval` binary(1) DEFAULT '0',
  `Applicableforrevert` binary(1) DEFAULT '0',
  `Applicableforeservice` binary(1) DEFAULT '0',
  `Applicableforextend` binary(1) DEFAULT '0',
  `Applicableforgenbill` binary(1) DEFAULT '0',
  `chargesRate` int(11) DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `Block_Label` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Level_Label` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Room_Label` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `termAndConditions` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`Idhostelconfiguration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_history`;
CREATE TABLE `tbl_hostel_history` (
  `IdHistory` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) DEFAULT NULL,
  `IdHostelRoom` bigint(20) DEFAULT NULL,
  `ActivityDate` datetime DEFAULT NULL,
  `Activity` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `checkinDate` datetime DEFAULT NULL,
  `OldValue` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NewValue` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  PRIMARY KEY (`IdHistory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_inventory`;
CREATE TABLE `tbl_hostel_inventory` (
  `IdHostelInventory` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ShortName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DefaultLang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  PRIMARY KEY (`IdHostelInventory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_item`;
CREATE TABLE `tbl_hostel_item` (
  `IdHostelItem` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ShortName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DefaultLang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` binary(1) DEFAULT '1' COMMENT '1: Active, 0:Inactive',
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  PRIMARY KEY (`IdHostelItem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_item_register`;
CREATE TABLE `tbl_hostel_item_register` (
  `IdHostelItemRegister` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) NOT NULL,
  `IdHostelItem` bigint(20) NOT NULL,
  `Quantity` int(20) NOT NULL,
  `UpdUser` bigint(11) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdHostelItemRegister`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_level`;
CREATE TABLE `tbl_hostel_level` (
  `IdHostelLevel` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ShortName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DefaultLang` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdHostelBlock` bigint(20) NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `Active` tinyint(4) NOT NULL,
  PRIMARY KEY (`IdHostelLevel`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_level_inventory`;
CREATE TABLE `tbl_hostel_level_inventory` (
  `IdHostelLevelInventory` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdHostelLevel` bigint(20) NOT NULL,
  `InventoryName` bigint(20) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `UpdUser` bigint(11) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdHostelLevelInventory`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_online_reservation_config`;
CREATE TABLE `tbl_hostel_online_reservation_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section_name_malay` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instruction` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_by` int(10) unsigned NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_registration`;
CREATE TABLE `tbl_hostel_registration` (
  `IdHostelregistration` bigint(20) NOT NULL AUTO_INCREMENT,
  `StudentStatus` tinyint(4) DEFAULT NULL COMMENT '1:checkin,2:checkout,3:changeroom,4:outcampus,5:campuscancel',
  `IdStudentRegistration` bigint(20) DEFAULT NULL,
  `SemesterCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdHostelRoom` bigint(20) DEFAULT NULL,
  `CheckInDate` datetime DEFAULT NULL,
  `CheckOutDate` datetime DEFAULT NULL,
  `SeatNo` int(10) DEFAULT NULL,
  `RoomKeyTakenDate` datetime DEFAULT NULL,
  `RoomKeyReturnDate` datetime DEFAULT NULL,
  `ChargingPerRoom` int(11) DEFAULT NULL,
  `RemarkChangeRoom` mediumtext COLLATE utf8mb4_unicode_ci,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  PRIMARY KEY (`IdHostelregistration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_reservation`;
CREATE TABLE `tbl_hostel_reservation` (
  `IdHostelReservation` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(20) NOT NULL COMMENT 'student (fk tbl_studentregistration)  applicant (applicant_transaction)',
  `agent_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_type` int(11) NOT NULL COMMENT '1: student  2:applicant 3:admin',
  `tenant_type` int(20) DEFAULT '263',
  `IdHostelRoomType` int(11) DEFAULT NULL,
  `room_type_id` int(11) NOT NULL COMMENT 'fk hostel_room_type',
  `IdHostelRoom` bigint(20) DEFAULT NULL,
  `SeatNo` int(10) DEFAULT NULL,
  `IdSemester` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SemesterStartDate` date DEFAULT NULL,
  `SemesterEndDate` date DEFAULT NULL,
  `duration` char(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `Active` int(1) NOT NULL,
  `Status` bigint(20) NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdHostelReservation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_reservation_history`;
CREATE TABLE `tbl_hostel_reservation_history` (
  `IdHostelReservationHistory` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) NOT NULL COMMENT 'foreign key of student registration',
  `IdHostelRoom` bigint(20) NOT NULL COMMENT 'foreign key of hostel room table',
  `Activity` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'name of activity as reserve room ,cancel rervation etc',
  `IpAddress` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'IP address of machine by which any activity has done',
  `Date` datetime NOT NULL COMMENT 'On which date this activity has done',
  `OldValue` bigint(20) NOT NULL COMMENT 'old status of reservation application as approve, entry,reject',
  `NewValue` bigint(20) NOT NULL COMMENT 'new status of reservation application',
  `Remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Remarks, when canceling the reservation',
  `Upduser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdHostelReservationHistory`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_reservation_setup`;
CREATE TABLE `tbl_hostel_reservation_setup` (
  `IdReservation` bigint(20) NOT NULL AUTO_INCREMENT,
  `SemesterCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Priority` bigint(11) NOT NULL,
  `ValueType` bigint(11) NOT NULL,
  `Condition` bigint(11) NOT NULL,
  `ValueId` bigint(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(11) NOT NULL,
  PRIMARY KEY (`IdReservation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_room`;
CREATE TABLE `tbl_hostel_room` (
  `IdHostelRoom` bigint(20) NOT NULL AUTO_INCREMENT,
  `RoomName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RoomCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DefaultLang` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Level` bigint(20) NOT NULL,
  `Block` bigint(20) NOT NULL,
  `RoomType` bigint(20) NOT NULL,
  `Capacity` int(11) NOT NULL,
  `OccupiedCapacity` int(10) NOT NULL DEFAULT '0',
  `Active` binary(1) NOT NULL,
  `Gender` int(1) NOT NULL,
  `PropertyOwner` varchar(220) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdUser` bigint(11) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdHostelRoom`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_roomtype_photos`;
CREATE TABLE `tbl_hostel_roomtype_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_room_inventory`;
CREATE TABLE `tbl_hostel_room_inventory` (
  `IdHostelRoomInventory` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdHostelRoom` bigint(20) NOT NULL,
  `InventoryName` bigint(20) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `UpdUser` bigint(11) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdHostelRoomInventory`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_room_type`;
CREATE TABLE `tbl_hostel_room_type` (
  `IdHostelRoomType` bigint(20) NOT NULL AUTO_INCREMENT,
  `RoomType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `OccupancyType` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `RoomCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ShortName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RoomFeature` int(11) NOT NULL,
  `DefaultLang` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` binary(1) NOT NULL,
  `UpdUser` bigint(11) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdHostelRoomType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_hostel_student_reservation`;
CREATE TABLE `tbl_hostel_student_reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type_id` int(11) NOT NULL,
  `charge_id` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'new' COMMENT 'new,cancel,reject',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'student' COMMENT '1: student  2:applicant 3:admin',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_idstudgrade`;
CREATE TABLE `tbl_idstudgrade` (
  `IdStudGrade` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentGrade` bigint(20) NOT NULL COMMENT 'FK to tbl_grade',
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_subject',
  `IdGrade` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_gradesetup',
  PRIMARY KEY (`IdStudGrade`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_individual_workload`;
CREATE TABLE `tbl_individual_workload` (
  `tiw_id` int(11) NOT NULL AUTO_INCREMENT,
  `tiw_staffid` int(11) NOT NULL DEFAULT '0',
  `tiw_workloadtype` int(11) NOT NULL DEFAULT '0',
  `tiw_min` int(11) NOT NULL DEFAULT '0',
  `tiw_max` int(11) NOT NULL DEFAULT '0',
  `tiw_nostudent` int(11) NOT NULL DEFAULT '0',
  `tiw_upddate` int(11) DEFAULT NULL,
  `tiw_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tiw_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `tbl_institution`;
CREATE TABLE `tbl_institution` (
  `idInstitution` int(11) NOT NULL AUTO_INCREMENT,
  `InstitutionName` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `InstitutionCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `InstitutionAddress1` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `InstitutionAddress2` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Country` bigint(20) NOT NULL,
  `State` bigint(20) DEFAULT NULL,
  `City` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `InstitutionPhoneNumber` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `InstitutionFaxNumber` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` int(10) NOT NULL DEFAULT '0',
  `UpdDate` int(11) NOT NULL,
  `UpdUser` int(11) NOT NULL,
  PRIMARY KEY (`idInstitution`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_institution_subject_mapping`;
CREATE TABLE `tbl_institution_subject_mapping` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdInstitution` bigint(20) NOT NULL,
  `IdSubject` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_intake`;
CREATE TABLE `tbl_intake` (
  `IdIntake` bigint(20) NOT NULL AUTO_INCREMENT,
  `IntakeId` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IntakeDesc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IntakeDefaultLanguage` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ApplicationStartDate` date DEFAULT NULL,
  `ApplicationEndDate` date DEFAULT NULL,
  `sem_seq` enum('JAN','JUN','SEP') COLLATE utf8mb4_unicode_ci NOT NULL,
  `apply_online` int(11) NOT NULL DEFAULT '0',
  `sem_year` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  `MigratedCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IdIntake`),
  UNIQUE KEY `IntakeId` (`IntakeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_intake_branch_mapping`;
CREATE TABLE `tbl_intake_branch_mapping` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdIntake` bigint(20) NOT NULL,
  `IdProgram` bigint(20) NOT NULL,
  `IdBranch` bigint(20) NOT NULL,
  `ip_id` int(10) unsigned NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_intake_program`;
CREATE TABLE `tbl_intake_program` (
  `ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdIntake` int(11) NOT NULL COMMENT 'fk tbl_intake',
  `IdProgram` int(11) NOT NULL COMMENT 'fk tbl_program',
  `IdProgramScheme` int(11) NOT NULL COMMENT 'fk tbl_program_scheme',
  `IdStudentCategory` int(11) NOT NULL COMMENT 'fk tbl_definations',
  `ApplicationStartDate` date NOT NULL,
  `ApplicationEndDate` date NOT NULL,
  `AdmissionStartDate` date NOT NULL COMMENT 'upload doc start date',
  `AdmissionEndDate` date NOT NULL,
  `PeriodStartDate` date NOT NULL,
  `PeriodEndDate` date NOT NULL,
  `Quota` int(11) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createddt` datetime NOT NULL,
  PRIMARY KEY (`ip_id`),
  UNIQUE KEY `IdIntake` (`IdIntake`,`IdProgram`,`IdStudentCategory`,`IdProgramScheme`),
  KEY `IdIntake_2` (`IdIntake`),
  KEY `IdProgram` (`IdProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_invoicedetails`;
CREATE TABLE `tbl_invoicedetails` (
  `IdInvoiceDetails` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdInvoice` bigint(20) unsigned NOT NULL,
  `idAccount` bigint(20) unsigned NOT NULL,
  `Discount` decimal(12,2) DEFAULT '0.00',
  `Description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Amount` decimal(12,2) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_user',
  `Active` binary(1) DEFAULT NULL COMMENT '0:No, 1:Yes',
  PRIMARY KEY (`IdInvoiceDetails`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_invoicemaster`;
CREATE TABLE `tbl_invoicemaster` (
  `IdInvoice` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdStudent` bigint(20) unsigned NOT NULL,
  `IdSemester` bigint(20) unsigned NOT NULL,
  `InvoiceNo` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `InvoiceDt` date DEFAULT NULL,
  `InvoiceAmt` decimal(12,2) NOT NULL,
  `AcdmcPeriod` bigint(20) unsigned DEFAULT '0',
  `Naration` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_user',
  `Active` binary(1) DEFAULT NULL COMMENT '0:No, 1:Yes',
  `Approved` binary(1) NOT NULL DEFAULT '0',
  `idsponsor` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdInvoice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_invoicesubjectdetails`;
CREATE TABLE `tbl_invoicesubjectdetails` (
  `IdInvoicesubdetail` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdInvoice` bigint(20) unsigned NOT NULL,
  `Amount` decimal(10,0) NOT NULL DEFAULT '0',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  `IdSubject` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`IdInvoicesubdetail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_invoicetermsandconditions`;
CREATE TABLE `tbl_invoicetermsandconditions` (
  `IdInvoiceTermAndCondition` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdInvoice` bigint(20) unsigned NOT NULL,
  `idTermsandconditions` bigint(20) NOT NULL,
  PRIMARY KEY (`IdInvoiceTermAndCondition`),
  KEY `idTermsandconditions` (`idTermsandconditions`),
  KEY `IdInvoice` (`IdInvoice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_itemgroup`;
CREATE TABLE `tbl_itemgroup` (
  `IdItem` bigint(20) NOT NULL AUTO_INCREMENT,
  `ItemCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EnglishDescription` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `BahasaDescription` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Alias` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ShortName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdBank` bigint(20) unsigned NOT NULL,
  `IdAccount` bigint(20) unsigned NOT NULL,
  `IdAccountGroup` bigint(20) unsigned NOT NULL,
  `Active` binary(1) NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdItem`),
  KEY `IdBank` (`IdBank`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_landscape`;
CREATE TABLE `tbl_landscape` (
  `IdLandscape` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  `LandscapeType` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `IdStartSemester` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign key to tbl_intake (sebenarnya ini adalah effective intake)',
  `IdProgramScheme` int(11) NOT NULL,
  `SemsterCount` int(5) DEFAULT '0',
  `Blockcount` int(11) NOT NULL DEFAULT '0',
  `Levelcount` smallint(2) unsigned NOT NULL DEFAULT '0',
  `Active` int(11) NOT NULL DEFAULT '0' COMMENT '1: Active 0:Inactive',
  `Default` int(11) NOT NULL DEFAULT '0' COMMENT '1: Yes 0:No',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `TotalCreditHours` decimal(10,2) DEFAULT NULL,
  `ProgramDescription` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landscapeDefaultLanguage` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AddDrop` binary(1) DEFAULT NULL,
  `AssessmentMethod` enum('point','grade') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MaxRepeatCourse` int(11) NOT NULL DEFAULT '0',
  `MaxRepeatExam` int(11) NOT NULL DEFAULT '0',
  `modifyby` int(11) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  `MaxExamCycle` int(11) DEFAULT NULL COMMENT 'for kmc',
  `MaxExamSitting` int(11) DEFAULT NULL COMMENT 'for kmc',
  `StudyPeriod` int(11) DEFAULT NULL COMMENT 'in months',
  PRIMARY KEY (`IdLandscape`),
  KEY `tbl_landscape_ibfk_1` (`UpdUser`),
  KEY `tbl_landscape_ibfk_2` (`IdStartSemester`),
  KEY `tbl_landscape_ibfk_3` (`IdProgram`),
  KEY `tbl_landscape_ibfk_4` (`LandscapeType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_landscapeblock`;
CREATE TABLE `tbl_landscapeblock` (
  `idblock` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idlandscape` bigint(20) NOT NULL,
  `block` int(11) NOT NULL,
  `blockname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` int(11) NOT NULL COMMENT 'block semester level',
  `CreditHours` decimal(10,2) DEFAULT '0.00',
  `prerequisite` int(10) unsigned NOT NULL DEFAULT '0',
  `createdby` int(11) NOT NULL,
  `createddt` datetime NOT NULL,
  `modifyby` int(11) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  UNIQUE KEY `idblock` (`idblock`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_landscapeblocksemester`;
CREATE TABLE `tbl_landscapeblocksemester` (
  `IdLandscapeblocksemester` int(11) NOT NULL AUTO_INCREMENT,
  `IdLandscape` bigint(20) unsigned NOT NULL,
  `blockid` bigint(20) unsigned NOT NULL,
  `semesterid` bigint(20) unsigned NOT NULL,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`IdLandscapeblocksemester`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_landscapeblocksubject`;
CREATE TABLE `tbl_landscapeblocksubject` (
  `IdLandscapeblocksubject` int(11) NOT NULL AUTO_INCREMENT,
  `IdLandscape` bigint(20) unsigned NOT NULL,
  `blockid` bigint(20) unsigned NOT NULL,
  `subjectid` bigint(20) unsigned NOT NULL,
  `coursetypeid` int(11) DEFAULT NULL,
  `IdProgramReq` int(11) NOT NULL COMMENT 'fk tbl_programrequirement',
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`IdLandscapeblocksubject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_landscapesubject`;
CREATE TABLE `tbl_landscapesubject` (
  `IdLandscapeSub` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  `IdLandscape` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_landscape',
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subjectmaster',
  `SubjectType` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `IdSemester` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_semester',
  `IdLevel` smallint(10) unsigned DEFAULT NULL COMMENT 'FK tbl_landscapeblock',
  `CreditHours` float NOT NULL,
  `IDProgramMajoring` bigint(20) unsigned NOT NULL,
  `IdProgramReq` int(11) NOT NULL COMMENT 'fk tbl_programrequirement',
  `Compulsory` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Not Compulsory, 1: Compulsory',
  `Papers` tinyint(1) unsigned DEFAULT '0',
  `Active` int(11) NOT NULL DEFAULT '1' COMMENT '1: Active 0:Inactive',
  `initial_exam_paper` int(11) NOT NULL DEFAULT '0',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdLandscapeSub`),
  KEY `IdProgram` (`IdProgram`),
  KEY `IdLandscape` (`IdLandscape`),
  KEY `IdSubject` (`IdSubject`),
  KEY `SubjectType` (`SubjectType`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_landscapesubject_item`;
CREATE TABLE `tbl_landscapesubject_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdLandscapeSub` int(11) NOT NULL,
  `idDefinition` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddt` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_landscapesubject_mode`;
CREATE TABLE `tbl_landscapesubject_mode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdLandscapeSub` int(11) NOT NULL,
  `idDefinition` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddt` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_landscapesubject_tag`;
CREATE TABLE `tbl_landscapesubject_tag` (
  `tag_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `landscape_id` int(10) unsigned NOT NULL,
  `subjectdata` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`tag_id`),
  KEY `landscape_id` (`landscape_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_landscape_company`;
CREATE TABLE `tbl_landscape_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `landscape_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL COMMENT 'FK tbl_takafuloperator',
  `createdby` int(11) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_landscape_detail`;
CREATE TABLE `tbl_landscape_detail` (
  `ld_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdLandscape` int(11) NOT NULL COMMENT 'fk tbl_landscape',
  `SemesterType` int(11) NOT NULL COMMENT 'fk tbl_definationms (51)',
  `MinRegCourse` int(11) NOT NULL,
  `MaxRegCourse` int(11) NOT NULL,
  `MinReqPassCourse` int(11) NOT NULL,
  `Type` int(11) NOT NULL COMMENT '1: Course 2:Credit Hours',
  `Initial` smallint(5) unsigned NOT NULL,
  `InitialType` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`ld_id`),
  KEY `IdLandscape` (`IdLandscape`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_language`;
CREATE TABLE `tbl_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `english` longtext COLLATE utf8mb4_unicode_ci,
  `arabic` mediumtext COLLATE utf8mb4_unicode_ci,
  `createdby` int(11) NOT NULL,
  `createddt` datetime NOT NULL,
  `Language` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Language` (`Language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_learningmode`;
CREATE TABLE `tbl_learningmode` (
  `IdLearningMode` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `LearningModeName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdLearningMode`),
  KEY `tbl_learningmode_ibfk_1` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_learning_material`;
CREATE TABLE `tbl_learning_material` (
  `tlm_id` int(11) NOT NULL AUTO_INCREMENT,
  `tlm_subjectId` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_subjectmaster',
  `tlm_materialType` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_definationms',
  `tlm_materialStatus` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_definationms',
  `tlm_status` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_definationms',
  `tlm_ISBN` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tlm_author` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tlm_specialInstructor` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tlm_semester` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tlm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_ledgercode`;
CREATE TABLE `tbl_ledgercode` (
  `AUTOINC` bigint(20) NOT NULL AUTO_INCREMENT,
  `CMPYCODE` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ACCOUNTCODE` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ACCOUNTYPE` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MISCCODE` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `STATUS` tinyint(1) NOT NULL,
  PRIMARY KEY (`AUTOINC`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_log`;
CREATE TABLE `tbl_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `hostname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_markdistribution_item`;
CREATE TABLE `tbl_markdistribution_item` (
  `mdi_id` int(11) NOT NULL AUTO_INCREMENT,
  `mdi_program` int(11) DEFAULT NULL,
  `mdi_programme_exam` int(11) DEFAULT NULL,
  `mds_id` int(11) DEFAULT NULL,
  `mdi_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mdi_raw` int(11) DEFAULT NULL,
  `mdi_weightage` int(11) DEFAULT NULL,
  `mdi_createddt` datetime DEFAULT NULL,
  `mdi_createdby` int(11) DEFAULT NULL,
  `mdi_modifydt` datetime DEFAULT NULL,
  `mdi_modifyby` int(11) DEFAULT NULL,
  PRIMARY KEY (`mdi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_markdistribution_setup`;
CREATE TABLE `tbl_markdistribution_setup` (
  `mds_id` int(11) NOT NULL AUTO_INCREMENT,
  `mds_semester` int(11) NOT NULL,
  `mds_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mds_landscape` int(11) NOT NULL DEFAULT '0',
  `mds_program` int(11) DEFAULT NULL,
  `mds_programme_exam` int(11) DEFAULT NULL,
  `mds_course` int(11) NOT NULL,
  `mds_min_weightage` decimal(10,2) NOT NULL,
  `mds_max_weightage` decimal(10,2) NOT NULL,
  `mds_weightage` decimal(10,2) DEFAULT NULL,
  `mds_raw` int(11) DEFAULT NULL,
  `mds_user_type` tinyint(1) NOT NULL,
  `mds_assessmenttypeid` bigint(20) NOT NULL,
  `mds_createdby` int(11) NOT NULL,
  `mds_createddt` datetime NOT NULL,
  `mds_modifyby` int(11) DEFAULT NULL,
  `mds_modifydt` datetime DEFAULT NULL,
  PRIMARY KEY (`mds_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_marksdistributiondetails`;
CREATE TABLE `tbl_marksdistributiondetails` (
  `IdMarksDistributionDetails` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdMarksDistributionMaster` bigint(20) unsigned DEFAULT NULL,
  `idStaff` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_staffmaster',
  `IdLecturer` int(11) DEFAULT NULL,
  `idSemester` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_semester',
  `IdProgram` int(11) DEFAULT NULL,
  `programme_exam` int(11) DEFAULT NULL,
  `IdComponentType` bigint(20) DEFAULT NULL,
  `IdComponentItem` bigint(20) DEFAULT NULL,
  `Status` bigint(20) NOT NULL,
  `ComponentName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Weightage` decimal(10,2) NOT NULL,
  `PassMark` decimal(10,2) DEFAULT NULL,
  `TotalMark` decimal(10,2) NOT NULL,
  `Percentage` decimal(10,2) NOT NULL,
  `PassStatus` bigint(20) unsigned DEFAULT NULL,
  `deleteFlag` binary(1) DEFAULT NULL,
  `ec_compulsory` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Not Compulsory 1: Compulsory',
  `moodle` tinyint(4) NOT NULL COMMENT '0:No 1:Yes',
  `moodle_type` int(11) NOT NULL DEFAULT '0',
  `CreatedDt` datetime DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`IdMarksDistributionDetails`),
  KEY `IdMarksDistributionMaster` (`IdMarksDistributionMaster`),
  KEY `idSemester` (`idSemester`),
  KEY `IdComponentType` (`IdComponentType`),
  KEY `IdMarksDistributionDetails` (`IdMarksDistributionDetails`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_marksdistributionmaster`;
CREATE TABLE `tbl_marksdistributionmaster` (
  `IdMarksDistributionMaster` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mds_id` int(11) NOT NULL DEFAULT '0',
  `MarksApplicationCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdComponentType` bigint(20) NOT NULL,
  `IdComponentItem` bigint(20) NOT NULL DEFAULT '0',
  `IdScheme` bigint(20) DEFAULT NULL,
  `IdFaculty` bigint(20) DEFAULT NULL,
  `Marks` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Percentage` decimal(10,2) NOT NULL DEFAULT '0.00',
  `TotalMark` decimal(10,2) NOT NULL DEFAULT '0.00',
  `IdCourseTaggingGroup` int(11) NOT NULL DEFAULT '0',
  `IdProgram` bigint(20) NOT NULL DEFAULT '0',
  `programme_exam` bigint(20) NOT NULL,
  `IdCourse` bigint(20) NOT NULL,
  `semester` bigint(100) NOT NULL,
  `EffectiveDate` date NOT NULL,
  `Status` bigint(20) NOT NULL COMMENT '0:Entry 1:Approved 2:Rejected',
  `Type` tinyint(4) NOT NULL COMMENT '0: standard 1:CE',
  `ApprovedBy` bigint(20) DEFAULT NULL,
  `ApprovedOn` datetime DEFAULT NULL,
  `RejectedBy` bigint(20) DEFAULT NULL,
  `RejectedOn` datetime DEFAULT NULL,
  `Remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreatedDt` datetime DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `allow_appeal` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Not Allow 1:Allow',
  `allow_resit` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Not Allow 1:Allow',
  `ce_pass_mark` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT 'CE Pass Mark - for CE Only',
  `ce_total_mark` decimal(11,2) DEFAULT NULL,
  `IdLandscape` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdMarksDistributionMaster`),
  KEY `IdMarksDistributionMaster` (`IdMarksDistributionMaster`),
  KEY `IdProgram` (`IdProgram`),
  KEY `IdCourse` (`IdCourse`),
  KEY `IdScheme` (`IdScheme`),
  KEY `semester` (`semester`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_marksentrysetup`;
CREATE TABLE `tbl_marksentrysetup` (
  `IdMarksEntrySetup` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSubject` bigint(20) NOT NULL,
  `IdStaff` bigint(20) unsigned NOT NULL,
  `Verification` binary(1) NOT NULL,
  `EffectiveDate` date NOT NULL,
  `Rank` int(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `Active` binary(1) NOT NULL,
  PRIMARY KEY (`IdMarksEntrySetup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_member`;
CREATE TABLE `tbl_member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `createdby` int(11) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updateddt` datetime DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_membership`;
CREATE TABLE `tbl_membership` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_name` varchar(100) NOT NULL,
  `m_classification` int(11) DEFAULT NULL COMMENT 'fk tbl_defination',
  `m_active` int(11) NOT NULL COMMENT '1:active 0:not active',
  `m_register` int(11) NOT NULL COMMENT '1:show 0:hide',
  PRIMARY KEY (`m_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_module_accessibility`;
CREATE TABLE `tbl_module_accessibility` (
  `ma_id` int(11) NOT NULL AUTO_INCREMENT,
  `ma_role_id` int(11) NOT NULL,
  `ma_start_date` date NOT NULL,
  `ma_end_date` date DEFAULT NULL,
  `ma_faculty_id` int(11) DEFAULT NULL,
  `ma_program_id` int(11) DEFAULT NULL,
  `ma_branch_id` int(11) DEFAULT NULL,
  `ma_course_id` int(11) DEFAULT NULL,
  `ma_createddt` datetime NOT NULL,
  `ma_createdby` int(11) NOT NULL,
  PRIMARY KEY (`ma_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_nav_menu`;
CREATE TABLE `tbl_nav_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `label` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seq_order` int(11) NOT NULL DEFAULT '0',
  `visible` int(11) NOT NULL DEFAULT '1' COMMENT '1:show 0:hide',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_nav_sidebar`;
CREATE TABLE `tbl_nav_sidebar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nav_menu_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_head` tinyint(4) NOT NULL DEFAULT '0',
  `seq_order` int(11) NOT NULL DEFAULT '0',
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_oldlandsacpesemester`;
CREATE TABLE `tbl_oldlandsacpesemester` (
  `IdOldLandscapesemester` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdLandscape` bigint(20) NOT NULL,
  `semesterid` int(11) NOT NULL,
  PRIMARY KEY (`IdOldLandscapesemester`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_paymentvarification`;
CREATE TABLE `tbl_paymentvarification` (
  `idPaymentVarification` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) unsigned NOT NULL,
  `PaymentRecived` tinyint(1) DEFAULT '0' COMMENT '1-yes,0-No',
  `FullPayment` tinyint(1) DEFAULT '0' COMMENT '1-yes,0-No',
  `OtherDocumentVarified` tinyint(1) DEFAULT '0' COMMENT '1-yes,0-No',
  `Comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`idPaymentVarification`),
  KEY `IdApplication` (`IdApplication`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_payment_mode_category`;
CREATE TABLE `tbl_payment_mode_category` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdPaymentMode` int(255) DEFAULT NULL COMMENT 'FK payment_mode.id',
  `IdCategory` int(11) DEFAULT NULL COMMENT 'FK tbl_category.category_id',
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedAt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_payment_mode_company`;
CREATE TABLE `tbl_payment_mode_company` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdPaymentMode` int(255) DEFAULT NULL COMMENT 'FK payment_mode.id',
  `IdCompany` int(11) DEFAULT NULL COMMENT 'FK tbl_company.IdCompany',
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedAt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_payment_mode_program`;
CREATE TABLE `tbl_payment_mode_program` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdPaymentMode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'FK payment_mode.id',
  `IdProgram` int(11) DEFAULT NULL COMMENT 'FK tbl_program.IdProgram',
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedAt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_placementtest`;
CREATE TABLE `tbl_placementtest` (
  `IdPlacementTest` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `PlacementTestName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PlacementTestDate` date NOT NULL,
  `Addr1` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Addr2` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `idCountry` bigint(20) NOT NULL,
  `idState` bigint(20) NOT NULL,
  `City` bigint(20) NOT NULL,
  `Zip` bigint(20) NOT NULL,
  `Phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PlacementTestTime` time NOT NULL,
  `MarksBasedOn` binary(1) DEFAULT NULL COMMENT '0: Total marks 1: Individual marks',
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `MinMark` bigint(20) DEFAULT NULL,
  `TotalMark` bigint(20) DEFAULT NULL,
  `PassingMark` bigint(20) DEFAULT NULL,
  `Intake` bigint(20) NOT NULL,
  `PlacementTestType` int(11) NOT NULL,
  PRIMARY KEY (`IdPlacementTest`),
  KEY `tbl_placementtest_ibfk_1` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_placementtestcomponent`;
CREATE TABLE `tbl_placementtestcomponent` (
  `IdPlacementTestComponent` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdPlacementTest` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_placementtest',
  `ComponentName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MarkingType` int(5) DEFAULT NULL,
  `ComponentWeightage` int(5) NOT NULL,
  `ComponentTotalMarks` int(5) NOT NULL,
  `MinimumMark` int(5) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdPlacementTestComponent`),
  KEY `tbl_placementtestcomponent_ibfk_1` (`UpdUser`),
  KEY `tbl_placementtestcomponent_ibfk_2` (`IdPlacementTest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_placementtestmarks`;
CREATE TABLE `tbl_placementtestmarks` (
  `IdPlacementTestMarks` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdPlacementTest` bigint(20) NOT NULL,
  `IdPlacementTestComponent` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key tbl_placementtestcomponent',
  `IdApplication` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_studentapplication',
  `Marks` decimal(4,2) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdPlacementTestMarks`),
  KEY `tbl_placementtestmarks_ibfk_3` (`IdPlacementTestComponent`),
  KEY `tbl_placementtestmarks_ibfk_1` (`UpdUser`),
  KEY `tbl_placementtestmarks_ibfk_2` (`IdApplication`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_placementtestprogram`;
CREATE TABLE `tbl_placementtestprogram` (
  `IdPlacementTestProgram` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdPlacementTest` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_placementtest',
  `IdProgramScheme` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program_scheme',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `IdScheme` bigint(20) NOT NULL,
  PRIMARY KEY (`IdPlacementTestProgram`),
  KEY `tbl_placementtestbranch_ibfk_1` (`UpdUser`),
  KEY `tbl_placementtestbranch_ibfk_2` (`IdProgramScheme`),
  KEY `tbl_placementtestbranch_ibfk_3` (`IdPlacementTest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_plantype`;
CREATE TABLE `tbl_plantype` (
  `IdPlantype` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Planname` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  `Active` binary(1) NOT NULL,
  PRIMARY KEY (`IdPlantype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_profilesemesterstatus_tagging`;
CREATE TABLE `tbl_profilesemesterstatus_tagging` (
  `pst_id` int(11) NOT NULL AUTO_INCREMENT,
  `pst_profilestatus` int(11) NOT NULL DEFAULT '0',
  `pst_semesterstatus` int(11) NOT NULL DEFAULT '0',
  `pst_updby` int(11) NOT NULL DEFAULT '0',
  `pst_upddate` datetime DEFAULT NULL,
  PRIMARY KEY (`pst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_program`;
CREATE TABLE `tbl_program` (
  `IdProgram` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IsMembership` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `ProgramMembership` tinyint(4) DEFAULT NULL,
  `ProgramName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ProgramCode` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ShortName` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TotalCreditHours` decimal(12,2) NOT NULL,
  `ArabicName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Award` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Active` tinyint(1) unsigned NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `MinimumAge` bigint(20) NOT NULL DEFAULT '0',
  `SelectColgDept1` bigint(20) unsigned DEFAULT NULL,
  `IdCollege1` bigint(20) unsigned NOT NULL DEFAULT '0',
  `IdScheme` bigint(20) NOT NULL,
  `programSalutation` int(11) DEFAULT NULL,
  `SelectColgDept` tinyint(4) NOT NULL DEFAULT '0',
  `IdCollege` int(11) NOT NULL,
  `apply_online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `program_validity` int(11) NOT NULL DEFAULT '0',
  `seq_no` smallint(3) unsigned NOT NULL DEFAULT '0',
  `allow_credittransfer` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `allow_exemption` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `allow_audit` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `inhouse` int(11) NOT NULL,
  `individual` int(11) NOT NULL,
  `corporate` int(11) NOT NULL,
  `epf_withdrawal` int(11) NOT NULL DEFAULT '0',
  `corporate_closingtype` int(100) NOT NULL DEFAULT '0',
  `corporate_closingday` int(100) NOT NULL DEFAULT '0',
  `noncorporate_closingtype` int(100) NOT NULL DEFAULT '0',
  `noncorporate_closingday` int(100) NOT NULL DEFAULT '0',
  `specialization` int(100) NOT NULL COMMENT 'FK to tbl_definitionms',
  `programme_level` int(100) NOT NULL COMMENT 'FK to tbl_definitionms',
  `programme_type` int(100) NOT NULL COMMENT 'FK to tbl_definitionms',
  `group_id` int(10) unsigned DEFAULT NULL,
  `exam_only` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0=Course&Exam, 1=Exam Only',
  `exam_closing_date` int(10) unsigned DEFAULT '30' COMMENT 'in days',
  `course_offering` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '	1=offer,0=no',
  `exam_offering` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '	1=offer,0=no',
  `esmtp_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'fk email_smtp',
  `programme_group` int(10) unsigned DEFAULT NULL,
  `require_membership` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0:no 1:yes',
  PRIMARY KEY (`IdProgram`),
  UNIQUE KEY `ProgramCode` (`ProgramCode`),
  KEY `tbl_program_ibfk_1` (`UpdUser`),
  KEY `tbl_program_ibfk_4` (`Award`),
  KEY `IdCollege` (`IdCollege`),
  KEY `IdScheme` (`IdScheme`),
  KEY `SelectColgDept` (`SelectColgDept`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programaccreditiondetails`;
CREATE TABLE `tbl_programaccreditiondetails` (
  `IdProgramAccreditionDetails` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_program',
  `AccredictionDate` date DEFAULT NULL,
  `AccredictionReferences` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccredictionNumber` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ProgramCode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ValidityFrom` date NOT NULL,
  `ValidityTo` date NOT NULL,
  `AccDtl1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl3` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl4` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl5` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl6` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl7` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl8` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl9` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl10` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl11` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl12` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl13` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl14` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl15` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl16` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl17` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl18` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl19` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl20` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccreditionType` bigint(20) NOT NULL COMMENT 'fk to tbl_definitonms',
  `ApprovalDate` date DEFAULT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdProgramAccreditionDetails`),
  KEY `IdProgram` (`IdProgram`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programbranchlink`;
CREATE TABLE `tbl_programbranchlink` (
  `IdProgramBranchLink` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdCollege` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_collegemaster',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  `IdSemester` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_semester',
  `MaxQuota` int(11) NOT NULL DEFAULT '0',
  `Quota` int(5) NOT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdProgramBranchLink`),
  KEY `tbl_programbranchlink_ibfk_1` (`UpdUser`),
  KEY `tbl_programbranchlink_ibfk_3` (`IdProgram`),
  KEY `IdCollege` (`IdCollege`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programcharges`;
CREATE TABLE `tbl_programcharges` (
  `IdProgramCharges` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key tbl_program',
  `IdCharges` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key tbl_charges',
  `Charges` decimal(12,2) NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdProgramCharges`),
  KEY `IdProgram` (`IdProgram`),
  KEY `IdCharges` (`IdCharges`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programchecklist`;
CREATE TABLE `tbl_programchecklist` (
  `IdCheckList` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CheckListName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Priority` bigint(20) NOT NULL DEFAULT '0',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_program',
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  `UpdDate` datetime NOT NULL,
  `Active` binary(1) NOT NULL,
  `Mandatory` binary(1) NOT NULL DEFAULT '1',
  `AddPlacementTest` binary(1) NOT NULL,
  PRIMARY KEY (`IdCheckList`),
  KEY `UpdUser` (`UpdUser`),
  KEY `IdProgram` (`IdProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programclosingdate`;
CREATE TABLE `tbl_programclosingdate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProgram` int(11) NOT NULL COMMENT 'fk tbl_program',
  `category_id` int(11) NOT NULL COMMENT 'fk tbl_category',
  `registration_type` int(11) NOT NULL COMMENT '0: module registration 1: exam registration',
  `days` int(50) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createddt` datetime NOT NULL,
  `updatedby` int(11) NOT NULL,
  `updateddt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_programentry`;
CREATE TABLE `tbl_programentry` (
  `IdProgramEntry` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `RequirementType` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1: Program 2: membership',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program or tbl_membership based on requirement type',
  `min_rank_qualification` int(11) NOT NULL DEFAULT '0',
  `IdScheme` bigint(20) unsigned NOT NULL,
  `MinAge` tinyint(3) DEFAULT NULL,
  `InternationalPlacementTest` tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  `InternationalCertification` tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  `InternationalAndOr` tinyint(1) DEFAULT '0' COMMENT '0-Or, 1-And',
  `LocalPlacementTest` tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  `LocalCertification` tinyint(1) DEFAULT '0' COMMENT '0- No, 1- Yes',
  `LocalAndOr` tinyint(1) DEFAULT '0' COMMENT '0-Or, 1-And',
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `Autoct` binary(1) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdProgramEntry`),
  KEY `tbl_programentry_ibfk_1` (`UpdUser`),
  KEY `tbl_programentry_ibfk_3` (`IdProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programentrylearningmode`;
CREATE TABLE `tbl_programentrylearningmode` (
  `Idprogramentrylearningmode` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdProgramEntry` bigint(20) unsigned NOT NULL,
  `LearningMode` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`Idprogramentrylearningmode`),
  KEY `IdProgramEntry` (`IdProgramEntry`),
  KEY `LearningMode` (`LearningMode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programentryrequirement`;
CREATE TABLE `tbl_programentryrequirement` (
  `IdProgramEntryReq` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdProgramEntry` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_programentry',
  `Desc` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Item` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Unit` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Condition` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Value` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Validity` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EntryLevel` bigint(20) NOT NULL COMMENT 'fk tbl_qualificationmaster',
  `IdSpecialization` bigint(20) unsigned NOT NULL,
  `GroupId` bigint(10) NOT NULL,
  `Mandatory` binary(1) NOT NULL COMMENT '1: Yes 0:No',
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Description` int(255) DEFAULT NULL,
  `IdProgram` int(20) unsigned NOT NULL,
  `IdSubject` int(20) unsigned NOT NULL,
  `RequirementType` int(20) unsigned NOT NULL,
  `RegistrationItem` int(20) unsigned NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdProgramEntryReq`),
  KEY `tbl_programentryrequirement_ibfk_1` (`UpdUser`),
  KEY `tbl_programentryrequirement_ibfk_2` (`IdProgramEntry`),
  KEY `tbl_programentryrequirement_ibfk_3` (`Item`),
  KEY `tbl_programentryrequirement_ibfk_4` (`Unit`),
  KEY `tbl_programentryrequirement_ibfk_5` (`Condition`),
  KEY `IdSubject` (`IdSubject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programentryrequirementotherdetails`;
CREATE TABLE `tbl_programentryrequirementotherdetails` (
  `IdProgramEntryReqOtherDetail` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdProgramEntry` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_programentry',
  `Item` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Condition` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Value` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GroupId` bigint(10) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdProgramEntryReqOtherDetail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programentrysubject`;
CREATE TABLE `tbl_programentrysubject` (
  `idProgramentrySubject` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdProgramEntry` bigint(20) unsigned NOT NULL,
  `IdSubject` bigint(20) DEFAULT '0',
  `SubjectName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Marks` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`idProgramentrySubject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programfees`;
CREATE TABLE `tbl_programfees` (
  `IdProgramFees` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdFeesSetupMaster` bigint(20) unsigned NOT NULL COMMENT 'FK To tbl_feessetupmaster',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'FK To tbl_program',
  PRIMARY KEY (`IdProgramFees`),
  KEY `IdFeesSetupMaster` (`IdFeesSetupMaster`),
  KEY `IdProgram` (`IdProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programhistory`;
CREATE TABLE `tbl_programhistory` (
  `IdHistory` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdProgram` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `Active` bigint(20) NOT NULL COMMENT '0:Inactive,1:Active',
  `Remarks` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`IdHistory`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programlearningmodedetails`;
CREATE TABLE `tbl_programlearningmodedetails` (
  `IdProgramLearningModeDetails` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_program',
  `IdLearningMode` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_definationms',
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdProgramLearningModeDetails`),
  KEY `IdProgram` (`IdProgram`),
  KEY `IdLearningMode` (`IdLearningMode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programmajoring`;
CREATE TABLE `tbl_programmajoring` (
  `IDProgramMajoring` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key ',
  `IdMajor` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idProgram` bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_program',
  `EnglishDescription` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `BahasaDescription` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifyby` int(11) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  PRIMARY KEY (`IDProgramMajoring`),
  KEY `idProgram` (`idProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programmohedetails`;
CREATE TABLE `tbl_programmohedetails` (
  `IdProgramMOHEDetails` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_program',
  `MoheDate` date DEFAULT NULL,
  `MoheReferences` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl3` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl4` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl5` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl6` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl7` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl8` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl9` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl10` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl11` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl12` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl13` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl14` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl15` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl16` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl17` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl18` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl19` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MoheDtl20` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdProgramMOHEDetails`),
  KEY `IdProgram` (`IdProgram`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_programrequirement`;
CREATE TABLE `tbl_programrequirement` (
  `IdProgramReq` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdLandscape` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_landscape',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  `IDProgramMajoring` int(10) unsigned DEFAULT NULL COMMENT 'FK tbl_programmajoring',
  `SubjectType` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `CreditHours` int(3) NOT NULL,
  `Compulsory` int(11) DEFAULT NULL COMMENT '1:Yes 2:Not',
  `RequirementType` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `modifyby` int(11) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  PRIMARY KEY (`IdProgramReq`),
  KEY `tbl_programrequirement_ibfk_1` (`UpdUser`),
  KEY `tbl_programrequirement_ibfk_2` (`IdLandscape`),
  KEY `tbl_programrequirement_ibfk_3` (`IdProgram`),
  KEY `tbl_programrequirement_ibfk_4` (`SubjectType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_program_bank_account`;
CREATE TABLE `tbl_program_bank_account` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProgram` int(11) DEFAULT NULL,
  `IdBankAccount` int(11) DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_program_details_section`;
CREATE TABLE `tbl_program_details_section` (
  `pds_id` int(11) NOT NULL AUTO_INCREMENT,
  `pds_program_id` int(11) NOT NULL COMMENT 'fk to tbl_program',
  `pds_item_id` int(11) NOT NULL COMMENT 'fk to tbl_application_item',
  `pds_instruction_position` tinyint(2) NOT NULL COMMENT '1: top 2: Bottom',
  `pds_instruction` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `pds_tooltip` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `pds_view` tinyint(2) NOT NULL,
  `pds_compulsory` tinyint(2) NOT NULL,
  `pds_enable` tinyint(2) NOT NULL,
  `pds_hide` tinyint(2) NOT NULL,
  `pds_updUser` int(11) NOT NULL,
  `pds_updDate` datetime NOT NULL,
  PRIMARY KEY (`pds_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_program_group`;
CREATE TABLE `tbl_program_group` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_program_scheme`;
CREATE TABLE `tbl_program_scheme` (
  `IdProgramScheme` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdProgram` bigint(20) NOT NULL COMMENT 'pk tbl_program',
  `mode_of_program` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_definations',
  `mode_of_study` int(11) NOT NULL,
  `program_type` int(11) NOT NULL,
  `Duration` decimal(4,1) NOT NULL COMMENT 'Min Duration',
  `DurationType` int(11) NOT NULL DEFAULT '0' COMMENT '0: Month 1: semester 2: Year',
  `MinYear` int(11) NOT NULL DEFAULT '0',
  `MinMonth` int(11) NOT NULL DEFAULT '0',
  `OptimalDuration` decimal(4,1) NOT NULL COMMENT 'Max Duration',
  `OptDurationType` int(11) NOT NULL DEFAULT '0' COMMENT '0: Month 1: semester 2: Year',
  `MaxYear` int(11) NOT NULL DEFAULT '0',
  `MaxMonth` int(11) NOT NULL DEFAULT '0',
  `createdby` int(11) NOT NULL,
  `createddt` datetime NOT NULL,
  `modifyby` int(11) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  `visibility` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '1:Public 0:Private ',
  PRIMARY KEY (`IdProgramScheme`),
  UNIQUE KEY `IdProgram_3` (`IdProgram`,`mode_of_program`,`mode_of_study`,`program_type`),
  KEY `IdProgram_2` (`IdProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_publish`;
CREATE TABLE `tbl_publish` (
  `id_Publish` int(11) NOT NULL AUTO_INCREMENT,
  `main_set_id` int(11) NOT NULL COMMENT 'PK q020_sectiontagging',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `condition` int(11) NOT NULL COMMENT '1: Exam 2:Result',
  `enable` int(1) unsigned NOT NULL,
  `dateUpd` datetime NOT NULL,
  `updUser` int(11) NOT NULL,
  PRIMARY KEY (`id_Publish`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_publishcondition`;
CREATE TABLE `tbl_publishcondition` (
  `id` int(11) NOT NULL,
  `condition` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_publishdetails`;
CREATE TABLE `tbl_publishdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Publish` int(11) NOT NULL COMMENT 'PK tbl_publish',
  `main_set_id` int(11) NOT NULL COMMENT 'PK q020_sectiontagging',
  `id_PoolPublish` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tbl_publishdetails_tbl_publish` (`id_Publish`),
  CONSTRAINT `FK_tbl_publishdetails_tbl_publish` FOREIGN KEY (`id_Publish`) REFERENCES `tbl_publish` (`id_Publish`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_publish_history`;
CREATE TABLE `tbl_publish_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Publish` int(11) NOT NULL,
  `main_set_id` int(11) NOT NULL COMMENT 'PK q020_sectiontagging',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `condition` int(11) NOT NULL COMMENT '1: Exam 2:Result',
  `enable` int(1) unsigned NOT NULL,
  `dateUpd` datetime NOT NULL,
  `updUser` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_Publish` (`id_Publish`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_publish_mark`;
CREATE TABLE `tbl_publish_mark` (
  `pm_id` int(11) NOT NULL AUTO_INCREMENT,
  `pm_idProgram` int(11) DEFAULT NULL,
  `pm_idSemester` int(11) DEFAULT NULL,
  `pm_idSubject` int(11) DEFAULT NULL,
  `pm_idGroup` int(11) DEFAULT NULL,
  `IdMarksDistributionMaster` int(11) DEFAULT NULL COMMENT 'fk mark distributionmaster',
  `IdMarksDistributionDetails` int(11) NOT NULL,
  `pm_type` tinyint(4) DEFAULT NULL COMMENT '1: Component 2: Course',
  `pm_category` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Standard 1:CE',
  `pm_date` date DEFAULT NULL,
  `pm_time` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`pm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_qualificationmaster`;
CREATE TABLE `tbl_qualificationmaster` (
  `IdQualification` int(11) NOT NULL AUTO_INCREMENT,
  `QualificationLevel` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `QualificationLevelMalay` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `QualificationRank` int(10) NOT NULL,
  `Active` int(10) NOT NULL DEFAULT '0',
  `qualification_type_id` int(11) NOT NULL,
  `SpecialTreatment` int(10) DEFAULT '0',
  `UpdUser` int(11) NOT NULL,
  `UpdDate` int(11) NOT NULL,
  PRIMARY KEY (`IdQualification`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_qualificationsub`;
CREATE TABLE `tbl_qualificationsub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qualification_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_qualification_subject_mapping`;
CREATE TABLE `tbl_qualification_subject_mapping` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdQualification` bigint(20) NOT NULL,
  `IdSubject` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_questions`;
CREATE TABLE `tbl_questions` (
  `idquestions` bigint(20) NOT NULL AUTO_INCREMENT,
  `Question` text NOT NULL,
  `QuestionAlias` text NOT NULL,
  `QuestionType` varchar(100) NOT NULL,
  `AnswerStyle` int(11) NOT NULL COMMENT '0:Multiple Choice,1:Essay',
  `Field2` varchar(255) DEFAULT NULL,
  `Field3` varchar(255) DEFAULT NULL,
  `Field4` varchar(255) DEFAULT NULL,
  `Field5` varchar(255) DEFAULT NULL,
  `Field1` varchar(255) DEFAULT NULL,
  `QuestionLevel` int(11) NOT NULL DEFAULT '0',
  `QuestionGroup` varchar(255) DEFAULT NULL,
  `QuestionNumber` varchar(255) DEFAULT NULL,
  `Active` binary(1) NOT NULL DEFAULT '0' COMMENT '1:Active;0:Inactive;2:Review',
  `Tamil` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Arabic` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Malay` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `QuestionId` varchar(50) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `syllabus_id` int(11) DEFAULT NULL,
  `migrate_status` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `migrate_remarks` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `taxanomy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`idquestions`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_range_user`;
CREATE TABLE `tbl_range_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT 'fk tbl_category',
  `number` int(11) NOT NULL COMMENT 'number start',
  `order` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_receivablesetup_bankcard`;
CREATE TABLE `tbl_receivablesetup_bankcard` (
  `IdBankCard` bigint(20) NOT NULL AUTO_INCREMENT,
  `BankCardType` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Language` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `IdUniversity` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdBankCard`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_receivable_adjustment`;
CREATE TABLE `tbl_receivable_adjustment` (
  `IdReceivableAdjustment` int(11) NOT NULL AUTO_INCREMENT,
  `AdjustmentCode` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AdjustmentType` bigint(20) NOT NULL,
  `DocumentDate` date NOT NULL,
  `IdReceipt` bigint(20) NOT NULL,
  `Remarks` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'APPROVE, ENTRY, CANCEL',
  `ProcessedBy` bigint(20) DEFAULT NULL,
  `ProcessedOn` date DEFAULT NULL,
  `IdUniversity` bigint(20) DEFAULT NULL,
  `EnterBy` bigint(20) DEFAULT NULL,
  `EnterDate` date DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `PaymentGroup` bigint(20) DEFAULT NULL,
  `PaymentMode` bigint(20) DEFAULT NULL,
  `ChequeNo` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PaymentDocBank` bigint(20) DEFAULT NULL,
  `PayDocBankBranch` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TerminalId` bigint(20) DEFAULT NULL,
  `BankCardNo` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BankCardType` bigint(20) DEFAULT NULL,
  `cancel_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `cancel_date` datetime NOT NULL,
  `cancel_by` int(11) NOT NULL,
  PRIMARY KEY (`IdReceivableAdjustment`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_recieptmaster`;
CREATE TABLE `tbl_recieptmaster` (
  `idReciept` bigint(20) NOT NULL AUTO_INCREMENT,
  `RecieptNumber` int(20) NOT NULL,
  `RecieptDate` date NOT NULL,
  `InvoiceId` bigint(20) unsigned DEFAULT NULL,
  `StudentId` bigint(20) unsigned NOT NULL,
  `SemesterId` bigint(20) unsigned NOT NULL,
  `IdProgram` bigint(20) unsigned NOT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ApprovedBy` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ChangedBy` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdCollege` bigint(20) NOT NULL,
  `ModeOfPayment` bigint(20) unsigned NOT NULL,
  `ChequeDate` date DEFAULT NULL,
  `ChequeNo` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idReciept`),
  KEY `StudentId` (`StudentId`),
  KEY `SemesterId` (`SemesterId`),
  KEY `InvoiceId` (`InvoiceId`),
  KEY `IdProgram` (`IdProgram`),
  KEY `ModeOfPayment` (`ModeOfPayment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_recieptmasterdetails`;
CREATE TABLE `tbl_recieptmasterdetails` (
  `idRecieptDetails` bigint(20) NOT NULL AUTO_INCREMENT,
  `idReciept` bigint(20) unsigned NOT NULL,
  `idItem` bigint(20) unsigned NOT NULL,
  `AmountPaid` decimal(10,2) NOT NULL,
  `UpdUser` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`idRecieptDetails`),
  KEY `idReciept` (`idReciept`),
  KEY `idItem` (`idItem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_record_config`;
CREATE TABLE `tbl_record_config` (
  `IdRecordConfig` bigint(20) NOT NULL AUTO_INCREMENT,
  `ApprovalCreditTransfer` int(10) NOT NULL,
  `RevertCreditTransfer` int(10) NOT NULL,
  `EserviceCreditTransfer` int(10) NOT NULL,
  `ApprovalChangeProgram` int(10) NOT NULL,
  `RevertChangeProgram` int(10) NOT NULL,
  `EserviceChangeProgram` int(10) NOT NULL,
  `ApprovalChangeStatus` int(10) NOT NULL,
  `RevertChangeStatus` int(10) NOT NULL,
  `EserviceChangeStatus` int(10) NOT NULL,
  `IdUniversity` bigint(20) NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdRecordConfig`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_record_config_messages`;
CREATE TABLE `tbl_record_config_messages` (
  `IdRecordMessage` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdRecordConfig` int(20) NOT NULL,
  `RecordModule` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RecordModuleStatus` bigint(20) NOT NULL,
  `Email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdRecordMessage`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_record_reason_defer`;
CREATE TABLE `tbl_record_reason_defer` (
  `IdRecordResonDefer` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUniversity` bigint(20) NOT NULL,
  `ReasonDefer` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DeferDescription` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdRecordResonDefer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_record_reason_quit`;
CREATE TABLE `tbl_record_reason_quit` (
  `IdRecordResonQuit` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUniversity` bigint(20) NOT NULL,
  `Reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `QuitDescription` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdRecordResonQuit`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_registrarlist`;
CREATE TABLE `tbl_registrarlist` (
  `IdRegistrarList` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdUniversity` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_universitymaster',
  `IdStaff` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  `FromDate` date NOT NULL,
  `ToDate` date DEFAULT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdRegistrarList`),
  KEY `tbl_registrarlist_ibfk_1` (`UpdUser`),
  KEY `tbl_registrarlist_ibfk_2` (`IdUniversity`),
  KEY `tbl_registrarlist_ibfk_3` (`IdStaff`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_registrationlocation`;
CREATE TABLE `tbl_registrationlocation` (
  `IdRegistrationLocation` int(11) NOT NULL AUTO_INCREMENT,
  `RegistrationLocationCode` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RegistrationLocationName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RegistrationLocationShortName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RegistrationLocationDescription` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RegistrationLocationIntake` bigint(20) NOT NULL,
  `RegistrationLocationStatus` binary(1) NOT NULL,
  `RegistrationLocationAddress1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RegistrationLocationAddress2` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` bigint(20) NOT NULL DEFAULT '1',
  `country` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RegPhone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`IdRegistrationLocation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_registration_configuration`;
CREATE TABLE `tbl_registration_configuration` (
  `regConfiguration_id` int(11) NOT NULL AUTO_INCREMENT,
  `registrationType` tinyint(4) DEFAULT NULL,
  `minFinanceBalance` binary(1) DEFAULT NULL,
  `academicLandscapeReqt` binary(1) DEFAULT NULL,
  `minCgpa` binary(1) DEFAULT NULL,
  `studentDisciplinary` binary(1) DEFAULT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`regConfiguration_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_registration_info`;
CREATE TABLE `tbl_registration_info` (
  `IdRegistrationInfo` bigint(10) NOT NULL AUTO_INCREMENT,
  `IdRegistrationLocation` bigint(10) NOT NULL,
  `RegistrationLocationScheme` bigint(20) NOT NULL,
  `RegistrationLocationDate` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RegistrationLocationTime` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RegistrationLocationRemarks` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RegistrationLocationBranch` int(11) NOT NULL,
  `RegistrationLocationProgram` int(11) NOT NULL,
  PRIMARY KEY (`IdRegistrationInfo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_registration_item`;
CREATE TABLE `tbl_registration_item` (
  `ri_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `semester_id` int(11) unsigned NOT NULL,
  `program_id` int(11) unsigned NOT NULL,
  `program_scheme_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `chargable` tinyint(1) unsigned NOT NULL,
  `mandatory` tinyint(1) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ri_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_registration_item_activity`;
CREATE TABLE `tbl_registration_item_activity` (
  `ria_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ria_ri_id` int(11) unsigned NOT NULL,
  `ria_activity_id` int(11) unsigned NOT NULL,
  `ria_calculation_type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '0=percentage, 1=amount',
  `ria_amount` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_registration_type`;
CREATE TABLE `tbl_registration_type` (
  `rt_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rt_activity` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rt_display_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`rt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_resources`;
CREATE TABLE `tbl_resources` (
  `idResources` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `Module` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Controller` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Action` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `RouteName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Modified` datetime NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`idResources`),
  KEY `Controller` (`Controller`(191)),
  KEY `Module` (`Module`(191)),
  KEY `Action` (`Action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_resources_new`;
CREATE TABLE `tbl_resources_new` (
  `r_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `r_menu_id` int(11) NOT NULL COMMENT 'fk tbl_top_menu',
  `r_title_id` int(11) NOT NULL COMMENT 'fk tbl_sidebar_menu',
  `r_screen_id` int(11) DEFAULT NULL COMMENT 'fk tbl_sidebar_menu',
  `r_module` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_controller` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_group` tinyint(4) NOT NULL COMMENT '1: View 2:Add 3:Edit 4:Delete 5:Approve',
  `r_action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_createddt` datetime NOT NULL,
  `r_createdby` int(11) NOT NULL,
  PRIMARY KEY (`r_id`),
  KEY `r_menu_id` (`r_menu_id`),
  KEY `r_title_id` (`r_title_id`),
  KEY `r_screen_id` (`r_screen_id`),
  KEY `r_controller` (`r_controller`(191)),
  KEY `r_group` (`r_group`),
  KEY `r_action` (`r_action`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_roleresources`;
CREATE TABLE `tbl_roleresources` (
  `idRoleResources` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `idRoles` bigint(11) unsigned NOT NULL,
  `idResources` bigint(11) unsigned NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`idRoleResources`),
  KEY `idRoles` (`idRoles`),
  KEY `idResources` (`idResources`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_role_resources_nav`;
CREATE TABLE `tbl_role_resources_nav` (
  `rrn_id` int(11) NOT NULL AUTO_INCREMENT,
  `rrn_role_id` int(11) NOT NULL COMMENT 'fk tbl_defination  :idDefType=1',
  `rrn_menu_id` int(11) DEFAULT NULL COMMENT 'fk tbl_top_menu',
  `rrn_title_id` int(11) DEFAULT NULL COMMENT 'fk tbl_sidebar_menu',
  `rrn_screen_id` int(11) DEFAULT NULL COMMENT 'fk tbl_sidebar_menu',
  `rrn_res_group` tinyint(4) NOT NULL DEFAULT '0',
  `rrn_createddt` datetime NOT NULL,
  `rrn_createdby` int(11) NOT NULL,
  PRIMARY KEY (`rrn_id`),
  UNIQUE KEY `rrn_role_id` (`rrn_role_id`,`rrn_menu_id`,`rrn_title_id`,`rrn_screen_id`,`rrn_res_group`),
  KEY `rrn_id` (`rrn_id`),
  KEY `rrn_menu_id_2` (`rrn_menu_id`),
  KEY `rrn_title_id_2` (`rrn_title_id`),
  KEY `rrn_screen_id_2` (`rrn_screen_id`),
  KEY `rrn_res_group_2` (`rrn_res_group`),
  KEY `rrn_role_id_2` (`rrn_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_room_charges`;
CREATE TABLE `tbl_room_charges` (
  `IdRoomCharges` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdHostelRoom` bigint(20) NOT NULL,
  `SemesterCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdIntake` int(11) DEFAULT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `Currency` int(11) DEFAULT NULL,
  `Rate` double NOT NULL,
  `Deposit` double NOT NULL,
  `Admin` double NOT NULL,
  `tenancy_period` int(11) DEFAULT NULL,
  `CustomerType` bigint(11) NOT NULL,
  `UpdUser` bigint(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdRoomCharges`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scheme`;
CREATE TABLE `tbl_scheme` (
  `IdScheme` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SchemeCode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EnglishDescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ArabicDescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `Mode` bigint(20) unsigned DEFAULT NULL COMMENT 'made it to null on 190712-sp4-10998',
  `Active` binary(1) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `UpdUser` bigint(20) unsigned DEFAULT NULL,
  `english_temp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arabic_temp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consecutive_sem` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdScheme`),
  KEY `Mode` (`Mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarshiptagging_sct`;
CREATE TABLE `tbl_scholarshiptagging_sct` (
  `sct_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sct_schId` int(11) NOT NULL,
  `sct_intake` int(11) NOT NULL,
  `sct_program` int(11) NOT NULL,
  `sct_studentcategory` int(11) NOT NULL,
  `sct_programscheme` int(11) NOT NULL,
  `sct_desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sct_status` int(11) NOT NULL,
  `sct_startDate` date NOT NULL,
  `sct_endDate` date NOT NULL,
  `sct_addDate` datetime NOT NULL,
  `sct_addBy` int(11) NOT NULL,
  `sct_modDate` datetime NOT NULL,
  `sct_modBy` int(11) NOT NULL,
  PRIMARY KEY (`sct_Id`),
  UNIQUE KEY `sct_Id` (`sct_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_academic_weight`;
CREATE TABLE `tbl_scholarship_academic_weight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_age_weight`;
CREATE TABLE `tbl_scholarship_age_weight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_country_weight`;
CREATE TABLE `tbl_scholarship_country_weight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `score` decimal(2,1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_dependent_weight`;
CREATE TABLE `tbl_scholarship_dependent_weight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_fee_info`;
CREATE TABLE `tbl_scholarship_fee_info` (
  `IdScholarshipFeeInfo` bigint(20) NOT NULL AUTO_INCREMENT,
  `sct_Id` bigint(20) NOT NULL,
  `FeeCode` bigint(20) NOT NULL,
  `CalculationMode` tinyint(1) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  `Repeat` int(11) DEFAULT NULL,
  `MaxRepeat` int(11) DEFAULT NULL,
  `FreqMode` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdScholarshipFeeInfo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_income_weight`;
CREATE TABLE `tbl_scholarship_income_weight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` float NOT NULL,
  `to` float NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_industry`;
CREATE TABLE `tbl_scholarship_industry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `score` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_sch`;
CREATE TABLE `tbl_scholarship_sch` (
  `sch_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sch_code` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sch_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sch_malayName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sch_desc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sch_frequency` smallint(5) DEFAULT '0',
  `sch_amount` float NOT NULL,
  `sch_addDate` datetime NOT NULL,
  `sch_addBy` int(11) NOT NULL,
  `sch_modDate` datetime NOT NULL,
  `sch_modBy` int(11) NOT NULL,
  UNIQUE KEY `sch_Id` (`sch_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_student`;
CREATE TABLE `tbl_scholarship_student` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `semester_id` int(10) unsigned NOT NULL,
  `scholarship_type` smallint(5) unsigned NOT NULL,
  `application_type` tinyint(1) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_studenttag`;
CREATE TABLE `tbl_scholarship_studenttag` (
  `sa_id` int(11) NOT NULL AUTO_INCREMENT,
  `sa_af_id` int(11) NOT NULL COMMENT 'fk applicant_financial',
  `sa_scholarship_type` int(11) DEFAULT NULL,
  `sa_trans_id` int(11) DEFAULT NULL COMMENT 'applicant_transaction',
  `sa_cust_id` int(11) NOT NULL COMMENT 'studentregistration',
  `sa_cust_type` int(11) NOT NULL COMMENT '1:Student 2:applicant 3:admin',
  `sa_status` int(11) NOT NULL COMMENT '1: new /applied 2:approve 3: reject ',
  `sa_semester_id` int(11) DEFAULT NULL,
  `sa_score` float DEFAULT NULL,
  `sa_validated_doc` tinyint(4) DEFAULT NULL,
  `sa_start_date` date DEFAULT NULL,
  `sa_end_date` date DEFAULT NULL,
  `sa_createddt` datetime NOT NULL,
  `sa_createdby` int(11) NOT NULL,
  PRIMARY KEY (`sa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_scholarship_student_program`;
CREATE TABLE `tbl_scholarship_student_program` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `setup_id` int(10) unsigned NOT NULL,
  `program_id` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_schoolmaster`;
CREATE TABLE `tbl_schoolmaster` (
  `idSchool` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idCity` bigint(20) unsigned NOT NULL,
  `idState` bigint(20) unsigned NOT NULL,
  `SchoolType` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SchoolName` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SchoolCode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `School_ArabicName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Address1` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Address2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Url` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` binary(1) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `workPhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`idSchool`),
  KEY `idState` (`idState`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_school_decipline_subject`;
CREATE TABLE `tbl_school_decipline_subject` (
  `sds_id` int(11) NOT NULL AUTO_INCREMENT,
  `sds_discipline_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk school_discipline(smj_code)',
  `sds_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`sds_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_school_discipline`;
CREATE TABLE `tbl_school_discipline` (
  `smd_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smd_desc` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smd_school_type` int(11) NOT NULL COMMENT 'school_type(st_id)',
  PRIMARY KEY (`smd_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_school_subject`;
CREATE TABLE `tbl_school_subject` (
  `ss_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ss_subject_bahasa` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ss_core_subject` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 core subj ',
  PRIMARY KEY (`ss_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_school_type`;
CREATE TABLE `tbl_school_type` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `st_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`st_code`),
  UNIQUE KEY `sl_id` (`st_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_semester`;
CREATE TABLE `tbl_semester` (
  `IdSemester` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `Semester` bigint(20) NOT NULL,
  `SemesterCode` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SemesterStartDate` date NOT NULL,
  `SemesterEndDate` date NOT NULL,
  `StudentIntake` bigint(20) NOT NULL COMMENT '0:No, There will be no intake for this semester, 1:Yes, There will be student intake for this semester',
  `SemesterStatus` bigint(20) NOT NULL COMMENT 'Status of the semester',
  `Active` binary(1) NOT NULL DEFAULT '1' COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `Program` bigint(20) DEFAULT NULL COMMENT 'FK to tbl_scheme',
  PRIMARY KEY (`IdSemester`),
  KEY `tbl_semester_ibfk_1` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_semestermaster`;
CREATE TABLE `tbl_semestermaster` (
  `IdSemesterMaster` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `SemesterMainName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SemesterMainDefaultLanguage` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SemesterMainCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AcademicYear` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SemesterType` int(11) NOT NULL COMMENT 'fk tbl_defination (51)',
  `IsCountable` tinyint(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `Branch` bigint(20) NOT NULL,
  `SemesterMainStartDate` date NOT NULL,
  `SemesterMainEndDate` date NOT NULL,
  `sem_seq` enum('JAN','JUN','SEP') COLLATE utf8mb4_unicode_ci NOT NULL,
  `SemesterCountType` int(11) NOT NULL,
  `SemesterFunctionType` tinyint(4) DEFAULT NULL,
  `IdScheme` int(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `idacadyear` int(11) NOT NULL,
  `ismigrate` tinyint(1) DEFAULT NULL,
  `display` int(11) NOT NULL,
  PRIMARY KEY (`IdSemesterMaster`),
  KEY `tbl_semestermaster_ibfk_1` (`UpdUser`),
  KEY `Branch` (`Branch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_semesterprogramlink`;
CREATE TABLE `tbl_semesterprogramlink` (
  `IdSemesterProgramLink` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdSemester` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_semester',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_program',
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdSemesterProgramLink`),
  KEY `tbl_semesterprogramlink_ibfk_1` (`UpdUser`),
  KEY `tbl_semesterprogramlink_ibfk_2` (`IdSemester`),
  KEY `tbl_semesterprogramlink_ibfk_3` (`IdProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_semesterstatus`;
CREATE TABLE `tbl_semesterstatus` (
  `IdSemesterStatus` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSemester` bigint(20) NOT NULL COMMENT 'FK  to tbl_semester',
  `SemesterStatus` int(11) NOT NULL COMMENT '0:Previous;1:Present;2:Past',
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) NOT NULL COMMENT 'FK to tbl_user',
  PRIMARY KEY (`IdSemesterStatus`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_sendoffer`;
CREATE TABLE `tbl_sendoffer` (
  `IdApprove` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplication` int(11) NOT NULL,
  `IdProgram` int(11) NOT NULL,
  `Approved` int(11) NOT NULL,
  `UpdUser` int(11) NOT NULL,
  `Update` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`IdApprove`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_sequence_id`;
CREATE TABLE `tbl_sequence_id` (
  `tsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsi_type` int(11) NOT NULL COMMENT '1: Credit Transfer 2: Exemption 3:Audit Paper 4: Change Mode 5:Change Status(boleh tambah lagi type)',
  `tsi_seq_no` int(11) NOT NULL,
  `tsi_seq_year` year(4) NOT NULL,
  `tsi_upd_date` date NOT NULL,
  `tsi_upd_user` int(11) NOT NULL,
  PRIMARY KEY (`tsi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_sidebar_menu`;
CREATE TABLE `tbl_sidebar_menu` (
  `sm_id` int(11) NOT NULL AUTO_INCREMENT,
  `sm_tm_id` int(11) NOT NULL,
  `sm_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_desc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sm_parentid` int(11) NOT NULL DEFAULT '0',
  `sm_module` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_controller` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_action` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_seq_order` int(11) NOT NULL DEFAULT '0',
  `sm_visibility` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:Hide 1:Show',
  PRIMARY KEY (`sm_id`),
  KEY `sm_tm_id` (`sm_tm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_specialization`;
CREATE TABLE `tbl_specialization` (
  `IdSpecialization` bigint(20) NOT NULL AUTO_INCREMENT,
  `Specialization` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdSpecialization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_sponsor`;
CREATE TABLE `tbl_sponsor` (
  `idsponsor` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SponsorCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DOB` date NOT NULL DEFAULT '0000-00-00',
  `gender` binary(1) DEFAULT '0',
  `Add1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Add2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `State` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Country` bigint(20) DEFAULT NULL,
  `HomePhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `WorkPhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CellPhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `Organisation` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typeSponsor` binary(1) NOT NULL DEFAULT '0' COMMENT '0-Individual, 1-Organisation',
  `url` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idsponsor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_sponsortemplate`;
CREATE TABLE `tbl_sponsortemplate` (
  `idSponsortemplate` bigint(20) NOT NULL AUTO_INCREMENT,
  `templateName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`idSponsortemplate`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_sponsortemplatedetails`;
CREATE TABLE `tbl_sponsortemplatedetails` (
  `idSponsortemplatedetails` bigint(20) NOT NULL AUTO_INCREMENT,
  `idSponsortemplate` bigint(20) NOT NULL,
  `idAccount` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`idSponsortemplatedetails`),
  KEY `idSponsortemplate` (`idSponsortemplate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_sponsor_appform`;
CREATE TABLE `tbl_sponsor_appform` (
  `id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('applicant','student') COLLATE utf8mb4_unicode_ci NOT NULL,
  `stype` int(11) NOT NULL DEFAULT '0' COMMENT '0: undefined 1:scholarship 2:sponsorship 3:financial aid',
  `application_type` tinyint(1) unsigned NOT NULL,
  `program_id` int(10) unsigned DEFAULT NULL,
  `sch_type` int(10) unsigned DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_sponsor_coordinator`;
CREATE TABLE `tbl_sponsor_coordinator` (
  `c_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_sponsor_id` int(10) unsigned NOT NULL,
  `c_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_active` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_sponsor_fee_info`;
CREATE TABLE `tbl_sponsor_fee_info` (
  `IdSponsorFeeInfo` bigint(20) NOT NULL AUTO_INCREMENT,
  `idsponsor` bigint(20) NOT NULL,
  `FeeCode` bigint(20) NOT NULL,
  `CalculationMode` tinyint(1) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  `Repeat` int(11) DEFAULT NULL,
  `MaxRepeat` int(11) DEFAULT NULL,
  `FreqMode` int(11) DEFAULT NULL COMMENT '1=one time,2=semester',
  PRIMARY KEY (`IdSponsorFeeInfo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_sponsor_registration`;
CREATE TABLE `tbl_sponsor_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_id` int(11) NOT NULL COMMENT 'pk : tbl_takafuloperator',
  `external_id` int(11) NOT NULL COMMENT 'pk : student_profile',
  `sponsor_type` int(11) NOT NULL COMMENT '1: Membership 2:Qualification',
  `sponsor_program_id` int(11) DEFAULT NULL,
  `sponsor_member_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `sponsor_amount` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sponsor_discount` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sponsor_balance` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sponsor_fs_id` int(11) DEFAULT NULL,
  `fs_id_upddate` datetime DEFAULT NULL,
  `at_processing_fee` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_mode` int(11) DEFAULT NULL COMMENT 'fk payment_mode',
  `payment_mode_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1=Applied 2=Approved 3=Rejected',
  `processing_date` datetime DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `sendmail_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `corporate_id` (`corporate_id`),
  KEY `external_id` (`external_id`),
  KEY `sponsor_program_id` (`sponsor_program_id`),
  KEY `sponsor_fs_id` (`sponsor_fs_id`),
  KEY `payment_mode` (`payment_mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `tbl_sponsor_tag`;
CREATE TABLE `tbl_sponsor_tag` (
  `IdSponsorTag` bigint(20) NOT NULL AUTO_INCREMENT,
  `Sponsor` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appl_id` int(10) unsigned DEFAULT NULL,
  `StudentId` int(11) unsigned DEFAULT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `AggrementNo` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  `AggrementStatus` tinyint(1) DEFAULT NULL,
  `FeeItem` smallint(10) unsigned DEFAULT NULL,
  `CalcMode` smallint(5) unsigned DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `IdUniversity` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdSponsorTag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_staffmaster`;
CREATE TABLE `tbl_staffmaster` (
  `IdStaff` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SecondName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ThirdName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FourthName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FullName` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField4` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField5` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ArabicName` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField4` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField5` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField6` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField7` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField8` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField9` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField10` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField11` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField12` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField13` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField14` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField15` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField16` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField17` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField18` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField19` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField20` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '1',
  `Add1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Add2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_state',
  `Country` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_countries',
  `Zip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` tinyint(1) NOT NULL COMMENT '(0:Inactive,1:Active)',
  `StaffType` tinyint(1) NOT NULL COMMENT '0:Staff under College, 1:Staff under Branch',
  `IdCollege` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_collegemaster of IdCollegeMaster (IdCollege/IdBranch. Depends on field StaffType)',
  `IdLevel` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_definationms',
  `IdDepartment` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `StaffAcademic` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:Academic Staff,1:Non-Academic Staff',
  `StaffId` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FrontSalutation` bigint(20) NOT NULL DEFAULT '0',
  `BackSalutation` bigint(20) DEFAULT NULL COMMENT 'FK  to tbl_definitionms',
  `NationalIdendificationNumber1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NationalIdendificationNumber2` int(11) DEFAULT NULL,
  `NationalIdendificationNumber3` int(11) DEFAULT NULL,
  `NationalIdendificationNumber4` int(11) DEFAULT NULL,
  `Religion` bigint(20) DEFAULT '0' COMMENT 'FK to tbl_definationms',
  `PlaceOfBirth` bigint(20) DEFAULT '0' COMMENT 'FK to tbl_city',
  `BankId` bigint(20) DEFAULT '0' COMMENT 'FK to tbl_bank',
  `BankAccountNo` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HighestQualification` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `QualificationNo` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HomePage` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdendityNoOfStateLecturer` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AuthorizationLetterNumber` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PtiAuthorizationLetterNo` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PtiName` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PreviousPtiNo` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PermitNo` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `StaffJobType` bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definationms',
  `JoiningDate` date DEFAULT NULL,
  `group` bigint(20) DEFAULT NULL,
  `Dosen_Code_EPSBED` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IdStaff`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_staffsubject`;
CREATE TABLE `tbl_staffsubject` (
  `idStaffSubject` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdStaff` bigint(20) unsigned NOT NULL COMMENT 'foriegn key to tbl_staffmaster',
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'foriegn key to tbl_subjectmaster',
  PRIMARY KEY (`idStaffSubject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_staffsubjects`;
CREATE TABLE `tbl_staffsubjects` (
  `IdStaffSubjects` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStaff` bigint(20) NOT NULL COMMENT 'FK to tbl_staff',
  `IdSemester` bigint(20) NOT NULL COMMENT 'FK to tbl_semester',
  `IdSubject` bigint(20) NOT NULL COMMENT 'FK to tbl_subject',
  `EffectiveDate` date NOT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` int(11) NOT NULL COMMENT 'FK to tbl_user',
  `Active` binary(1) NOT NULL,
  PRIMARY KEY (`IdStaffSubjects`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_state`;
CREATE TABLE `tbl_state` (
  `idState` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `StateCode` varchar(20) NOT NULL,
  `state_Code_EPSBED` char(2) NOT NULL,
  `StateName` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `idCountry` bigint(20) unsigned NOT NULL,
  `Active` binary(1) NOT NULL COMMENT '1:Active, 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idState`),
  KEY `FK_tbl_State_1` (`idCountry`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_statusattachment_sth`;
CREATE TABLE `tbl_statusattachment_sth` (
  `sth_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sth_stpId` int(11) NOT NULL COMMENT 'fk `tbl_statustemplate_stp',
  `sth_tplId` int(11) NOT NULL COMMENT 'fk comm_template',
  `sth_type` int(11) NOT NULL COMMENT 'fk defination',
  `sth_addBy` int(11) NOT NULL,
  `sth_addDate` datetime NOT NULL,
  PRIMARY KEY (`sth_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_statustemplate_stp`;
CREATE TABLE `tbl_statustemplate_stp` (
  `stp_Id` int(11) NOT NULL AUTO_INCREMENT,
  `stp_tplId` int(11) DEFAULT NULL COMMENT 'fk comm_template tpl_id',
  `stp_tplType` int(11) DEFAULT NULL COMMENT 'fk tbl_definationms (type:122)',
  `stp_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stp_language` int(11) DEFAULT NULL,
  `contentMy` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'content in malay',
  `contentEng` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'content in english',
  `stp_addBy` int(11) NOT NULL DEFAULT '0',
  `stp_addDate` datetime NOT NULL,
  `stp_modBy` int(11) NOT NULL DEFAULT '0',
  `stp_modDate` datetime NOT NULL,
  PRIMARY KEY (`stp_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_status_sts`;
CREATE TABLE `tbl_status_sts` (
  `sts_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sts_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk tbl_definationms (txn status)',
  `sts_malayName` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sts_desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sts_message` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_statustemplate_stp',
  `sts_emailNotification` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_statustemplate_stp ',
  `sts_addBy` int(11) NOT NULL DEFAULT '0',
  `sts_addDate` datetime NOT NULL,
  `sts_modBy` int(11) NOT NULL DEFAULT '0',
  `sts_modDate` datetime NOT NULL,
  `sts_programScheme` int(11) NOT NULL COMMENT 'fk tbl_program_scheme',
  `sts_stdCtgy` int(11) NOT NULL COMMENT 'tbl_defination',
  PRIMARY KEY (`sts_Id`),
  KEY `sts_Id` (`sts_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentactivity_setup`;
CREATE TABLE `tbl_studentactivity_setup` (
  `IdStudentActivitySetup` bigint(20) NOT NULL AUTO_INCREMENT,
  `ChangeProgramFeeSetup` bigint(20) DEFAULT NULL,
  `ChangeProgramChargeable` int(1) DEFAULT NULL,
  `ChangeProgramTrigger` bigint(20) DEFAULT NULL,
  `ChangeStatusFeeSetup` bigint(20) DEFAULT NULL,
  `ChangeStatusChargeable` int(1) DEFAULT NULL,
  `ChangeStatusTrigger` bigint(20) DEFAULT NULL,
  `ChangeStatusQuitFeeSetup` bigint(20) DEFAULT NULL,
  `ChangeStatusQuitChargeable` int(1) DEFAULT NULL,
  `ChangeStatusQuitTrigger` bigint(20) DEFAULT NULL,
  `CreditTransferFeeSetup` bigint(20) DEFAULT NULL,
  `CreditTransferChargeable` int(1) DEFAULT NULL,
  `CreditTransferTrigger` bigint(20) DEFAULT NULL,
  `ResitExamFeeSetup` bigint(20) DEFAULT NULL,
  `ResitExamChargeable` int(1) DEFAULT NULL,
  `ResitExamTrigger` bigint(20) DEFAULT NULL,
  `PaperReviewFeeSetup` bigint(20) DEFAULT NULL,
  `PaperReviewChargeable` int(1) DEFAULT NULL,
  `PaperReviewTrigger` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `IdUniversity` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdStudentActivitySetup`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentappfilesupload`;
CREATE TABLE `tbl_studentappfilesupload` (
  `IdStudentAppFilesUpload` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdApplication` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_application',
  `FileName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UploadedFileName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UploadPath` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DocumentType` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_definationms',
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdStudentAppFilesUpload`),
  KEY `IdApplication` (`IdApplication`),
  KEY `DocumentType` (`DocumentType`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentapplication`;
CREATE TABLE `tbl_studentapplication` (
  `IdApplication` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) DEFAULT NULL COMMENT 'from tbl_studentregistration. gets inserted only when status changed to ACTIVATED',
  `registrationId` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'this inserts when student gets registered',
  `EmergencyCountry` bigint(20) NOT NULL,
  `EmergencyState` bigint(20) NOT NULL,
  `EmergencyCity` bigint(20) NOT NULL,
  `EmergencyZip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdApplicant` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` bigint(20) unsigned DEFAULT NULL,
  `StudentId` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `StudentCollegeId` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameField1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField4` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField5` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TypeofId` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField4` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField5` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField6` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField7` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField8` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField9` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField10` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField11` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField12` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField13` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField14` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField15` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField16` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField17` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField18` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField19` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtraIdField20` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateOfBirth` date NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Gender` binary(1) NOT NULL COMMENT '0: Male 1:Female',
  `NoOfChildren` smallint(10) DEFAULT '0',
  `Nationality` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PermAddressDetails` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PermCity` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PermState` bigint(20) unsigned NOT NULL,
  `PermCountry` bigint(20) unsigned NOT NULL,
  `PermZip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CorrsAddressDetails` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CorrsCity` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CorrsState` bigint(20) unsigned DEFAULT NULL,
  `CorrsCountry` bigint(20) unsigned DEFAULT NULL,
  `CorrsZip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OutcampusAddressDetails` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OutcampusCountry` bigint(20) DEFAULT NULL,
  `OutcampusState` bigint(20) DEFAULT NULL,
  `OutcampusCity` bigint(20) DEFAULT NULL,
  `OutcampusZip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HomePhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '000-000-000',
  `CellPhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EmailAddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idCollege` bigint(20) NOT NULL,
  `idsponsor` bigint(20) unsigned DEFAULT NULL,
  `InternationalStd` bigint(20) DEFAULT NULL,
  `PPNos` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PPIssueDt` date DEFAULT NULL,
  `PPExpDt` date DEFAULT NULL,
  `PPField1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PPField2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PPField3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PPField4` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PPField5` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VisaDetailField1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VisaDetailField2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VisaDetailField3` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VisaDetailField4` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VisaDetailField5` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `Active` binary(1) NOT NULL COMMENT '''1:Active, 0:Inactive''',
  `hobbies` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `othercurricularactivity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Relative` binary(1) NOT NULL DEFAULT '0',
  `RelativeDiscountType` bigint(20) DEFAULT '0',
  `Local` binary(1) NOT NULL DEFAULT '0' COMMENT '"0=Local Student 1=Non Local Student"',
  `Sponsorpercent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `idSponsortemplate` bigint(20) DEFAULT NULL,
  `idcoursedetails` bigint(20) NOT NULL DEFAULT '1',
  `Studapp` bigint(20) NOT NULL DEFAULT '0',
  `PreffredHostel` bigint(20) DEFAULT NULL,
  `PreferredBlock` bigint(20) DEFAULT NULL,
  `PreffredFloor` bigint(20) DEFAULT NULL,
  `PreffredroomType` bigint(20) DEFAULT NULL,
  `PrefferedApartment` bigint(20) DEFAULT NULL,
  `PreffredBed` bigint(20) DEFAULT NULL,
  `bedAllotedto` bigint(20) DEFAULT '0',
  `RoomPrefredFrom` date DEFAULT '0000-00-00',
  `RoomPrefredto` date DEFAULT '0000-00-00',
  `CountryFirstVisitDate` date DEFAULT NULL,
  `currentjobfromdate` date DEFAULT '0000-00-00',
  `currentjobtodate` date DEFAULT '0000-00-00',
  `currentjobtitle` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currentjoborganizationtype` bigint(20) DEFAULT NULL,
  `currentjobemployer` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currentjobemployeraddress` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currentjobsalary` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Termination` binary(1) DEFAULT '0' COMMENT '1:Terminated 0: Un-terminated',
  `IDCourse` bigint(20) NOT NULL,
  `ICNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Registered` binary(1) NOT NULL COMMENT '1:Yes, 0:No',
  `Offered` binary(1) NOT NULL COMMENT '1:Yes, 0:No',
  `Accepted` binary(1) NOT NULL COMMENT '1:Yes, 0:No',
  `ApplicationType` binary(1) NOT NULL COMMENT '1:Online, 0:Manual',
  `IdPlacementtest` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_placementtest',
  `ApplicationDate` datetime NOT NULL,
  `PlaceOfBirth` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TypeOfResidence` int(11) NOT NULL COMMENT '1=Rent a hoouse;0=Family House',
  `Religion` int(11) DEFAULT NULL COMMENT 'FK  from tbl_definitionms',
  `BloodGroup` int(11) DEFAULT NULL COMMENT 'FK  from tbl_definitionms',
  `NameOfFather` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameOfMother` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CircOfFather` binary(1) NOT NULL DEFAULT '1' COMMENT '0;deceased 1:alive',
  `CircOfMother` binary(1) NOT NULL DEFAULT '1' COMMENT '0;deceased 1:alive',
  `ParentAddressDetails` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PostCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `City` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_city',
  `Telephone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Handphone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PEmail` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MaritalStatus` int(10) DEFAULT NULL,
  `ParentJob` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to definitionMs',
  `ParentEducation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Referrel` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to definitionMs',
  `JacketSize` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_definationms',
  `LocalPlacementTest` binary(1) NOT NULL DEFAULT '0',
  `LocalCertification` binary(1) NOT NULL DEFAULT '0',
  `InternationalPlacementTest` binary(1) NOT NULL DEFAULT '0',
  `InternationalCertification` binary(1) NOT NULL DEFAULT '0',
  `Rejected` binary(1) NOT NULL DEFAULT '0' COMMENT '0-No,1-Yes',
  `RejectedComments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateRejected` datetime DEFAULT NULL,
  `RejectedBy` bigint(10) DEFAULT NULL,
  `RejectedReason` bigint(20) NOT NULL,
  `intake` bigint(20) unsigned DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `Source` bigint(10) DEFAULT NULL,
  `Scheme` bigint(20) unsigned DEFAULT NULL,
  `DateIncompleted` datetime DEFAULT NULL,
  `IncompleteBy` bigint(10) DEFAULT NULL,
  `IncompleteComments` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ReasonIncomplete` bigint(20) NOT NULL,
  `Dateofferred` date DEFAULT NULL,
  `OfferredBy` bigint(10) DEFAULT NULL,
  `ProgramOfferred` bigint(10) DEFAULT NULL,
  `BranchOfferred` bigint(20) unsigned DEFAULT NULL,
  `DateRegistered` datetime DEFAULT NULL,
  `RegisteredBy` bigint(10) DEFAULT NULL,
  `ProgramRegistered` bigint(10) DEFAULT NULL,
  `FullArabicName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Race` bigint(10) DEFAULT NULL,
  `SpecialTreatment` bigint(10) DEFAULT NULL,
  `SpecialTreatmentType` int(10) DEFAULT NULL,
  `DateProvisionalOffered` date DEFAULT NULL,
  `DateProvisionalOfferedBy` bigint(20) DEFAULT NULL,
  `ProvisionalProgramOffered` bigint(20) DEFAULT NULL,
  `ProvisionalBranchOffered` bigint(20) unsigned DEFAULT NULL,
  `RelationshipType` bigint(20) unsigned NOT NULL,
  `RelativeName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EmergencyAddressDetails` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EmergencyHomePhone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EmergencyCellPhone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EmergencyOffPhone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_migrated` binary(1) DEFAULT NULL,
  `IdApplicantFormat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdFormat` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IdApplication`),
  KEY `FK_tbl_StudentApplication_PermState` (`PermState`),
  KEY `FK_tbl_StudentApplication_PermCountry` (`PermCountry`),
  KEY `FK_tbl_StudentApplication_CorrsState` (`CorrsState`),
  KEY `FK_tbl_StudentApplication_CorrsCountry` (`CorrsCountry`),
  KEY `JacketSize` (`JacketSize`),
  KEY `Referrel` (`Referrel`),
  KEY `ParentJob` (`ParentJob`),
  KEY `City` (`City`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentattandance`;
CREATE TABLE `tbl_studentattandance` (
  `IdStudentAttandance` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentregistration',
  `IdSemester` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_semester',
  `AttandanceStatus` binary(1) NOT NULL DEFAULT '0' COMMENT '1=Present;0=Absent',
  `UpdDate` date NOT NULL,
  `UpdUser` bigint(20) NOT NULL COMMENT 'FK to tbl_user',
  PRIMARY KEY (`IdStudentAttandance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentcurricularactivity`;
CREATE TABLE `tbl_studentcurricularactivity` (
  `IdExtraCurActivity` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key TO Student Application',
  `idActivity` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key To Definition MS',
  `GamingLevel` bigint(20) NOT NULL COMMENT 'FK to definitionms',
  `DateOfActivity` date NOT NULL,
  `PositionHeld` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DetailedDescription` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SpecialHonor` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`IdExtraCurActivity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentdetain`;
CREATE TABLE `tbl_studentdetain` (
  `idStudentDetain` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) unsigned NOT NULL,
  `IdSemester` bigint(20) unsigned NOT NULL,
  `IdAction` bigint(20) unsigned NOT NULL,
  `ToEffectiveDate` date NOT NULL,
  `Comments` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idStudentDetain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studenteducationdetails`;
CREATE TABLE `tbl_studenteducationdetails` (
  `IdStudEduDtl` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ProgCheckListName` bigint(20) NOT NULL COMMENT 'FK to tbl_programchecklist',
  `IdApplication` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_studentapplication',
  `InstitutionName` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UniversityName` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `StudyPlace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MajorFiledOfStudy` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `YearOfStudyFrom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `YearOfStudyTo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DegreeType` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definationms ( Degree Type )',
  `GradeOrCGPA` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `TypeOfSchool` int(11) DEFAULT NULL,
  `StatusOfSchool` int(11) DEFAULT NULL,
  `TypeOfStudent` bigint(20) DEFAULT NULL COMMENT '1: international 0 : Local',
  `HomeTownSchool` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreditTransferFrom` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SubjectId` bigint(20) DEFAULT NULL COMMENT 'FK to tbl_subjectmaster',
  `SubjectMark` bigint(20) DEFAULT '0',
  `IdQualification` bigint(20) DEFAULT NULL,
  `IdSpecialization` bigint(20) DEFAULT NULL,
  `IdInstitute` bigint(20) DEFAULT NULL,
  `YearGraduated` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdResultItem` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Resulttotal` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IdStudEduDtl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentemploymentdetails`;
CREATE TABLE `tbl_studentemploymentdetails` (
  `IdStudentEmployment` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) NOT NULL COMMENT 'FK to tbl_studentapplication',
  `From` date NOT NULL,
  `To` date NOT NULL,
  `Posttitle` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `OrganisationType` bigint(20) NOT NULL,
  `EmployerName` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EmployerAddress` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`IdStudentEmployment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentexemption`;
CREATE TABLE `tbl_studentexemption` (
  `IdExemption` tinyint(4) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL COMMENT 'FK tbl_studentregistration ',
  `IdProgram` int(11) NOT NULL COMMENT 'FK tbl_program',
  `IdLandscape` int(11) DEFAULT NULL COMMENT 'FK tbl_landscape ',
  `IdMajoringPathway` int(11) NOT NULL COMMENT 'FK tbl_programmajoring',
  `IdSubject` int(11) NOT NULL COMMENT 'FK tbl_subjectmaster',
  `ExemptionStatus` int(11) NOT NULL COMMENT '1=exempted',
  `ExemptedDate` datetime NOT NULL,
  `ExemptedBy` int(11) NOT NULL,
  PRIMARY KEY (`IdExemption`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_studentextendtime`;
CREATE TABLE `tbl_studentextendtime` (
  `IdExtend` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL COMMENT 'FK tbl_studentregistration',
  `IdProgramExtend` int(11) NOT NULL COMMENT 'FK tbl_program',
  `ReasonExtend` text NOT NULL,
  `DurationExtend` varchar(50) DEFAULT NULL,
  `DateApplyExtend` datetime DEFAULT NULL,
  `DateApproveExtend` datetime DEFAULT NULL,
  `ApproveByExtend` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdExtend`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_studentparentdetails`;
CREATE TABLE `tbl_studentparentdetails` (
  `IdStudparentDtl` int(11) NOT NULL AUTO_INCREMENT,
  `IdApplication` int(11) NOT NULL COMMENT 'FK To tbl_studentapplication',
  `NameOfFather` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameOfMother` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CircOfFather` int(11) NOT NULL COMMENT '1=life;0=deceased',
  `CircOfMother` int(11) NOT NULL COMMENT '1=life;0=deceased',
  `ParentAddressDetails` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PostCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `City` int(11) NOT NULL COMMENT 'FK tbl_definitionms',
  `Telephone` int(11) NOT NULL,
  `Handphone` int(11) NOT NULL,
  `EmailAddress` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ParentJob` int(11) NOT NULL COMMENT 'FK To tbl_definitionms',
  `ParentEducation` int(11) NOT NULL COMMENT 'FK To tbl_definitionms',
  `STPTrisakti` int(11) NOT NULL,
  PRIMARY KEY (`IdStudparentDtl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentplacementtest`;
CREATE TABLE `tbl_studentplacementtest` (
  `IdStudentPlacementTest` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) unsigned NOT NULL,
  `IdPlacementTest` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`IdStudentPlacementTest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentregcharges`;
CREATE TABLE `tbl_studentregcharges` (
  `IdStudentRegCharges` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdStudentRegistration` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentregistration',
  `IdCharge` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_charges',
  `Charge` decimal(12,2) NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdStudentRegCharges`),
  KEY `IdCharge` (`IdCharge`),
  KEY `UpdUser` (`UpdUser`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentregistration`;
CREATE TABLE `tbl_studentregistration` (
  `IdStudentRegistration` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdTransaction` int(11) NOT NULL COMMENT 'fk tbl applicant transaction',
  `TransactionStatus` int(11) NOT NULL COMMENT 'fk definationms:DRAFT,ENTRY,OFFERED,REJECTED,COMPLETE,INCOMPLETE,SHORTLISTED,KIV,ACCEPTED,DECLINE,ENROLLED.ARCHIEVE ',
  `ExemptionStatus` int(11) NOT NULL DEFAULT '0' COMMENT '3=disapprove; 2=approve; 1=apply; 0=no; ',
  `TimeExtension` int(11) NOT NULL DEFAULT '0' COMMENT '3-reject; 2-approve; 1-apply; 0-no',
  `IdMajoringPathway` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_programmajoring',
  `sp_id` int(11) DEFAULT NULL COMMENT 'fk student_profile',
  `registrationId` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'matric no',
  `IdRegistrationType` tinyint(4) DEFAULT NULL COMMENT 'fk tbl_registration_type',
  `IdLandscape` bigint(20) NOT NULL COMMENT 'fk tbl_landscape',
  `IdProgram` bigint(20) NOT NULL COMMENT 'fk tbl_program',
  `IdProgramScheme` int(11) NOT NULL COMMENT 'fk programscheme',
  `IdProgramMajoring` int(11) DEFAULT '0' COMMENT 'fk tbl_programmajoring',
  `IdFeePlan` int(10) unsigned DEFAULT NULL,
  `IdDiscountPlan` int(10) unsigned DEFAULT NULL,
  `IdDiscountPlanDate` datetime DEFAULT NULL,
  `IdDiscountPlanBy` int(10) unsigned DEFAULT NULL,
  `IdFeePlanDate` datetime DEFAULT NULL,
  `IdFeePlanBy` int(10) unsigned DEFAULT NULL,
  `IdBranch` bigint(20) DEFAULT NULL COMMENT 'fk tbl_branchofficeavenue',
  `IdIntake` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `profileStatus` bigint(20) NOT NULL COMMENT 'fk tbl_definationms (Student Status = 20) ',
  `UpdUser` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime DEFAULT NULL,
  `senior_student` tinyint(4) DEFAULT NULL COMMENT '0: No 1:Yes',
  `sr_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `ref_id` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `fs_id` int(11) DEFAULT NULL COMMENT 'PK fee_structure',
  `fs_id_upddate` datetime DEFAULT NULL,
  `employment_status` int(11) NOT NULL DEFAULT '0' COMMENT 'pk tbl_definations (def type 105)',
  `company_id` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL COMMENT 'tbl_category',
  `groupId` int(11) DEFAULT NULL,
  `createdDate` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AcademicAdvisor` int(11) DEFAULT '0',
  `grad_status` int(11) DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  `erc_id` int(11) NOT NULL DEFAULT '0' COMMENT '//curent cycle',
  `grad_temp_status` int(11) DEFAULT '0' COMMENT '1-shortlisted, 2-approved',
  PRIMARY KEY (`IdStudentRegistration`),
  KEY `UpdUser` (`UpdUser`),
  KEY `IdProgram` (`IdProgram`),
  KEY `IdProgramScheme` (`IdProgramScheme`),
  KEY `sp_id` (`sp_id`),
  KEY `IdLandscape` (`IdLandscape`),
  KEY `batch_id` (`batch_id`),
  KEY `company_id` (`company_id`),
  KEY `createdDate` (`createdDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentregistration_history`;
CREATE TABLE `tbl_studentregistration_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdStudentRegistration` int(11) DEFAULT NULL,
  `IdTransaction` int(11) DEFAULT NULL COMMENT 'fk tbl applicant transaction ',
  `TransactionStatus` int(11) DEFAULT NULL COMMENT 'fk definationms:DRAFT,ENTRY,OFFERED,REJECTED,COMPLETE,INCOMPLETE,SHORTLISTED,KIV,ACCEPTED,DECLINE,ENROLLED.ARCHIEVE',
  `ExemptionStatus` int(11) DEFAULT '0' COMMENT '3=disapprove; 2=approve; 1=apply; 0=no; ',
  `TimeExtension` int(11) DEFAULT '0' COMMENT '3-reject; 2-approve; 1-apply; 0-no',
  `IdMajoringPathway` int(11) DEFAULT '0' COMMENT 'FK tbl_programmajoring',
  `IdRegistrationType` tinyint(4) DEFAULT NULL COMMENT 'fk tbl_registration_type',
  `sp_id` int(11) DEFAULT NULL COMMENT 'fk student_profile',
  `registrationId` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'matric no',
  `IdLandscape` bigint(20) NOT NULL COMMENT 'fk tbl_landscape',
  `IdProgram` bigint(20) NOT NULL COMMENT 'fk tbl_program',
  `IdProgramScheme` int(11) NOT NULL COMMENT 'fk programscheme',
  `IdProgramMajoring` int(11) DEFAULT '0' COMMENT 'fk tbl_programmajoring',
  `IdFeePlan` int(10) unsigned DEFAULT NULL,
  `IdDiscountPlan` int(10) unsigned DEFAULT NULL,
  `IdDiscountPlanDate` datetime DEFAULT NULL,
  `IdDiscountPlanBy` int(10) unsigned DEFAULT NULL,
  `IdFeePlanDate` datetime DEFAULT NULL,
  `IdFeePlanBy` int(10) unsigned DEFAULT NULL,
  `IdBranch` bigint(20) DEFAULT NULL COMMENT 'fk tbl_branchofficeavenue',
  `IdIntake` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `profileStatus` bigint(20) NOT NULL COMMENT 'fk tbl_definationms (Student Status = 20) ',
  `UpdUser` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime DEFAULT NULL,
  `senior_student` tinyint(4) DEFAULT NULL COMMENT '0: No 1:Yes',
  `sr_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `ref_id` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `fs_id` int(11) DEFAULT NULL COMMENT 'PK fee_structure',
  `fs_id_upddate` datetime DEFAULT NULL,
  `employment_status` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `groupId` int(11) DEFAULT NULL,
  `createdDate` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AcademicAdvisor` int(11) NOT NULL DEFAULT '0',
  `grad_status` int(11) DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentregsubjects`;
CREATE TABLE `tbl_studentregsubjects` (
  `IdStudentRegSubjects` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdStudentRegistration` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentregistration',
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subject',
  `ExemptionStatus` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '0-no;1-yes;',
  `SubCode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_hour_registered` int(11) DEFAULT NULL,
  `SubjectsApproved` binary(1) NOT NULL DEFAULT '0',
  `SubjectsApprovedComments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdLandscapeSub` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_landscapesubject',
  `IdBlock` int(11) DEFAULT NULL COMMENT 'fk tbl_blocklandscape',
  `BlockLevel` tinyint(4) DEFAULT NULL COMMENT 'level/sort block',
  `subjectlandscapetype` tinyint(4) NOT NULL DEFAULT '1',
  `IdGrade` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime DEFAULT NULL,
  `UpdRole` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT 'admin',
  `batch_id` int(11) DEFAULT NULL COMMENT 'batch_registration',
  `Active` tinyint(4) DEFAULT NULL COMMENT '0:pre-register 1:Register  2: Add&Drop 3:Withdraw 4: Repeat 5:Refer 6: CT 9 : Pre-Repeat',
  `module_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'C: Completed   IN:Incomplete  MG:Missing Grade F:Fraud NR:No Record',
  `exam_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'C: Completed   IN:Incomplete  MG:Missing Grade F:Fraud NR:No Record',
  `exam_status_updateby` int(11) DEFAULT NULL,
  `exam_status_updatedt` datetime DEFAULT NULL,
  `final_course_mark` decimal(10,2) DEFAULT NULL COMMENT '80',
  `grade_point` decimal(10,2) DEFAULT NULL COMMENT '3.00',
  `grade_id` int(11) DEFAULT NULL COMMENT 'fk tbl_definations',
  `grade_name` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'A',
  `grade_desc` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Baik,Sangat Baik etc',
  `grade_status` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Pass/Fail',
  `mark_approval_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Entry 1:submit for approval 2:approve',
  `mark_approveby` int(11) DEFAULT NULL COMMENT 'tbl_staffmaster',
  `mark_approvedt` datetime DEFAULT NULL,
  `approval_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `IdCourseTaggingGroup` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT 'fk course_group_schedule',
  `IdCourseTaggingGroup_date` datetime DEFAULT NULL,
  `cgpa_calculation` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: calculate 1:exclude calculation',
  `replace_subject` int(11) DEFAULT NULL COMMENT 'fk tbl_studentregsubjects',
  `appeal_mark` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:No 1:yes',
  `exam_scaling_mark` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  `migrate_date` datetime DEFAULT NULL,
  `migrate_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `result` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lms` tinyint(4) DEFAULT NULL COMMENT '1=yes,0=no',
  `lms_date` datetime DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `lms_migrate` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  PRIMARY KEY (`IdStudentRegSubjects`),
  KEY `IdSubject` (`IdSubject`),
  KEY `UpdUser` (`UpdUser`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`),
  KEY `IdStudentRegSubjects` (`IdStudentRegSubjects`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentregsubjects_detail`;
CREATE TABLE `tbl_studentregsubjects_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `regsub_id` int(11) unsigned NOT NULL,
  `student_id` int(11) unsigned NOT NULL,
  `semester_id` int(10) unsigned NOT NULL,
  `subject_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned NOT NULL,
  `ec_country` int(11) unsigned NOT NULL,
  `ec_city` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_id` int(10) unsigned DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_role` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT 'student',
  `ses_id` int(10) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '2:drop 3:withdraw',
  `refund` int(11) DEFAULT NULL,
  `modifyby` int(11) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentregsubjects_detail_history`;
CREATE TABLE `tbl_studentregsubjects_detail_history` (
  `srsdh_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL COMMENT 'fk tbl_studentregsubjects_detail',
  `regsub_id` int(11) unsigned NOT NULL,
  `student_id` int(11) unsigned NOT NULL,
  `semester_id` int(10) unsigned NOT NULL,
  `subject_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned NOT NULL,
  `ec_country` int(11) unsigned NOT NULL,
  `ec_city` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_id` int(10) unsigned DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_role` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT 'student',
  `ses_id` int(10) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:add 1:drop 2:withdraw',
  `refund` int(11) DEFAULT NULL,
  `modifyby` int(11) DEFAULT NULL,
  `modifydt` datetime DEFAULT NULL,
  `message` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`srsdh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentregsubjects_history`;
CREATE TABLE `tbl_studentregsubjects_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegSubjects` bigint(20) unsigned NOT NULL,
  `IdStudentRegistration` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentregistration',
  `initial` tinyint(4) DEFAULT '0',
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subject',
  `audit_program` int(11) DEFAULT NULL,
  `credit_hour_registered` int(11) DEFAULT NULL,
  `SubjectsApproved` binary(1) NOT NULL DEFAULT '0',
  `SubjectsApprovedComments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdSemesterMain` bigint(20) DEFAULT NULL,
  `IdSemesterDetails` bigint(20) DEFAULT NULL,
  `SemesterLevel` tinyint(4) DEFAULT NULL COMMENT 'level/sort semester',
  `IdLandscapeSub` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_landscapesubject',
  `IdBlock` int(11) DEFAULT NULL COMMENT 'fk tbl_blocklandscape',
  `BlockLevel` tinyint(4) DEFAULT NULL COMMENT 'level/sort block',
  `subjectlandscapetype` tinyint(4) NOT NULL DEFAULT '1',
  `IdGrade` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime DEFAULT NULL,
  `UpdRole` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL COMMENT 'batch_registration',
  `Active` tinyint(4) DEFAULT NULL COMMENT '0:pre-register 1:Register  2: Add&Drop 3:Withdraw 4: Repeat 5:Refer',
  `module_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'C: Completed   IN:Incomplete  MG:Missing Grade F:Fraud NR:No Record',
  `exam_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'C: Completed   IN:Incomplete  MG:Missing Grade F:Fraud NR:No Record',
  `exam_status_updateby` int(11) DEFAULT NULL,
  `exam_status_updatedt` datetime DEFAULT NULL,
  `final_course_mark` decimal(10,2) DEFAULT NULL COMMENT '80',
  `grade_id` int(11) DEFAULT NULL,
  `grade_point` decimal(10,2) DEFAULT NULL COMMENT '3.00',
  `grade_name` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'A',
  `grade_desc` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Baik,Sangat Baik etc',
  `grade_status` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Pass/Fail',
  `mark_approval_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Entry 1:submit for approval 2:approve',
  `mark_approveby` int(11) DEFAULT NULL COMMENT 'tbl_staffmaster',
  `mark_approvedt` datetime DEFAULT NULL,
  `approval_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `IdCourseTaggingGroup` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'fk tbl_course_tagging_group',
  `IdCourseTaggingGroup_date` datetime DEFAULT NULL,
  `cgpa_calculation` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: calculate 1:exclude calculation',
  `replace_subject` int(11) DEFAULT NULL COMMENT 'fk tbl_studentregsubjects',
  `appeal_mark` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  `exam_scaling_mark` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  `migrate_date` datetime DEFAULT NULL,
  `migrate_by` int(11) DEFAULT NULL,
  `message` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `pre_active` int(11) NOT NULL DEFAULT '0',
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `lms_migrate` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentregsubjects_session`;
CREATE TABLE `tbl_studentregsubjects_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL,
  `reg_id` int(10) unsigned NOT NULL,
  `semester_id` int(10) unsigned NOT NULL,
  `session_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'subject' COMMENT 'subject, item',
  `created_date` datetime NOT NULL,
  `logdata` longtext COLLATE utf8mb4_unicode_ci,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='student portal registration session';


DROP TABLE IF EXISTS `tbl_studentscredittransfer`;
CREATE TABLE `tbl_studentscredittransfer` (
  `IdStudentsCredittransfer` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `DocumentsVerified` binary(1) NOT NULL COMMENT '1:Verified,0:Not Verified',
  `DocumentsApproved` binary(1) NOT NULL COMMENT '1:approved,0:Not Approved',
  `Comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdApplication` bigint(20) unsigned NOT NULL,
  `IdCreditTransferSubjectsDocs` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`IdStudentsCredittransfer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentsemesterstatus`;
CREATE TABLE `tbl_studentsemesterstatus` (
  `idstudentsemsterstatus` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) DEFAULT NULL,
  `idSemester` bigint(20) unsigned DEFAULT NULL COMMENT 'IdSemesterDetails',
  `IdSemesterMain` bigint(20) DEFAULT NULL COMMENT 'fk tbl_semester',
  `IdBlock` int(11) DEFAULT NULL COMMENT 'fk  tbl_landscapeblocks',
  `studentsemesterstatus` smallint(6) DEFAULT NULL COMMENT 'fk tbl_definationms (Student Semester Status)',
  `Level` int(11) NOT NULL,
  `Part` tinyint(4) DEFAULT '0' COMMENT 'indicates part for CIFP only',
  `Reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_user',
  `UpdRole` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT 'admin',
  `validReason` int(11) NOT NULL DEFAULT '0',
  `appreson` int(11) NOT NULL DEFAULT '99',
  `appreasonother` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idstudentsemsterstatus`),
  UNIQUE KEY `IdStudentRegistration` (`IdStudentRegistration`,`IdSemesterMain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentsemesterstatus_history`;
CREATE TABLE `tbl_studentsemesterstatus_history` (
  `idstudentsemsterstatus` bigint(20) unsigned NOT NULL,
  `IdStudentRegistration` bigint(20) DEFAULT NULL,
  `idSemester` bigint(20) unsigned DEFAULT NULL COMMENT 'IdSemesterDetails',
  `IdSemesterMain` bigint(20) DEFAULT NULL COMMENT 'fk tbl_semester',
  `IdBlock` int(11) DEFAULT NULL COMMENT 'fk  tbl_landscapeblocks',
  `studentsemesterstatus` smallint(6) DEFAULT NULL COMMENT 'fk tbl_definationms (Student Semester Status)',
  `Level` int(11) NOT NULL,
  `Part` tinyint(4) DEFAULT '0' COMMENT 'indicates part for CIFP only',
  `Reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'fk to tbl_user',
  `UpdRole` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'admin',
  `Type` tinyint(4) DEFAULT NULL COMMENT '1:Change Status',
  `message` int(11) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `validReason` int(11) NOT NULL DEFAULT '0',
  `appreson` int(11) NOT NULL DEFAULT '99',
  `appreasonother` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idstudentsemsterstatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_studentstatus`;
CREATE TABLE `tbl_studentstatus` (
  `IdStudentStatus` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdApplication` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_studentapplication',
  `Status` int(11) NOT NULL COMMENT '1:Active, 2:Terminated, 3:Deceased, 4:Graduated',
  `Reason` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `SemesterStatus` int(11) DEFAULT NULL COMMENT '1:Defer, 2:Suspended',
  `EffSemester` bigint(20) unsigned DEFAULT NULL,
  `NoofSemester` int(11) DEFAULT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`IdStudentStatus`),
  KEY `IdApplication` (`IdApplication`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_academic_advisor_history`;
CREATE TABLE `tbl_student_academic_advisor_history` (
  `saa_id` int(11) NOT NULL AUTO_INCREMENT,
  `saa_IdStudentRegistration` int(11) NOT NULL COMMENT 'fk to tbl_studentregistration',
  `saa_staff_id` int(11) DEFAULT NULL COMMENT 'fk to tbl_staffmaster',
  `saa_status` set('Assign','Unassigned') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Assign' COMMENT '''Assign'',''Unassigned''',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`saa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_appeal`;
CREATE TABLE `tbl_student_appeal` (
  `sa_id` int(11) NOT NULL AUTO_INCREMENT,
  `sa_idSemester` int(11) NOT NULL,
  `sa_idSubject` int(11) NOT NULL,
  `sa_idStudentRegSubject` int(11) NOT NULL,
  `sa_applyDate` datetime NOT NULL,
  `sa_applyBy` int(11) NOT NULL,
  `sa_applyRole` int(11) NOT NULL,
  `sa_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: Apply 2:Approve application 3:Reject Application 4:Submit for verification',
  `sa_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `sa_approvedDate` datetime DEFAULT NULL,
  `sa_approvedBy` int(11) DEFAULT NULL,
  `sa_rejectDate` datetime DEFAULT NULL,
  `sa_rejectBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`sa_id`),
  KEY `sa_idSemester` (`sa_idSemester`),
  KEY `sa_idSubject` (`sa_idSubject`),
  KEY `sa_idStudentRegSubject` (`sa_idStudentRegSubject`),
  KEY `sa_id` (`sa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_appeal_mark`;
CREATE TABLE `tbl_student_appeal_mark` (
  `sam_id` int(11) NOT NULL AUTO_INCREMENT,
  `sam_sa_id` int(11) NOT NULL COMMENT 'fk tbl_student_appeal',
  `sam_examiner1` int(11) DEFAULT NULL,
  `sam_mark1` decimal(10,2) DEFAULT NULL,
  `sam_examiner2` int(11) DEFAULT NULL,
  `sam_mark2` decimal(10,2) DEFAULT NULL,
  `sam_examiner3` int(11) NOT NULL,
  `sam_mark3` decimal(10,2) DEFAULT NULL,
  `sam_actual_mark` decimal(10,2) DEFAULT NULL,
  `final_course_mark` decimal(10,2) DEFAULT NULL,
  `grade_point` decimal(10,2) DEFAULT NULL,
  `grade_name` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_status` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sam_createdby` int(11) NOT NULL,
  `sam_createddt` datetime NOT NULL,
  PRIMARY KEY (`sam_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_detail_marks_entry`;
CREATE TABLE `tbl_student_detail_marks_entry` (
  `IdStudentMarksEntryDetail` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentMarksEntry` bigint(20) DEFAULT NULL,
  `IdIntake` int(11) DEFAULT NULL COMMENT 'tbl_intake.IdIntake',
  `IdProgram` int(11) DEFAULT NULL COMMENT 'tbl_program.IdProgram',
  `pe_id` int(11) DEFAULT NULL COMMENT 'programme_exam.pe_id',
  `es_id` int(11) DEFAULT NULL COMMENT 'exam_schedule.es_id',
  `er_id` int(11) DEFAULT NULL,
  `Component` bigint(20) DEFAULT NULL COMMENT 'IdMarksDistributionMaster',
  `ComponentItem` bigint(20) DEFAULT NULL,
  `ComponentDetail` bigint(20) DEFAULT NULL COMMENT 'IdMarksDistributionDetails',
  `MarksObtained` decimal(10,2) DEFAULT NULL COMMENT '(Raw Mark) Jumlah markah yang student perolehi',
  `FinalMarksObtained` decimal(10,2) NOT NULL COMMENT 'Jumlah markah yang student perolehi by Percentage',
  `TotalMarks` decimal(10,2) DEFAULT NULL COMMENT 'Jumlah markah keseluruhan',
  `CreatedDt` datetime DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `pull_status` tinyint(4) DEFAULT NULL,
  `pull_date` datetime DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `tbe_id` int(11) DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_examresult_migratedt` datetime DEFAULT NULL,
  PRIMARY KEY (`IdStudentMarksEntryDetail`),
  KEY `IdStudentMarksEntryDetail` (`IdStudentMarksEntryDetail`),
  KEY `IdStudentMarksEntry` (`IdStudentMarksEntry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_detail_marks_entry_history`;
CREATE TABLE `tbl_student_detail_marks_entry_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentMarksEntryDetail` bigint(20) NOT NULL,
  `IdStudentMarksEntry` bigint(20) DEFAULT NULL,
  `IdIntake` int(11) DEFAULT NULL COMMENT 'tbl_intake.IdIntake',
  `IdProgram` int(11) DEFAULT NULL COMMENT 'tbl_program.IdProgram',
  `pe_id` int(11) DEFAULT NULL COMMENT 'programme_exam.pe_id',
  `es_id` int(11) DEFAULT NULL COMMENT 'exam_schedule.es_id',
  `er_id` int(11) DEFAULT NULL,
  `Component` bigint(20) DEFAULT NULL COMMENT 'IdMarksDistributionMaster',
  `ComponentItem` bigint(20) DEFAULT NULL,
  `ComponentDetail` bigint(20) DEFAULT NULL COMMENT 'IdMarksDistributionDetails',
  `MarksObtained` decimal(10,2) DEFAULT NULL COMMENT '(Raw Mark) Jumlah markah yang student perolehi',
  `FinalMarksObtained` decimal(10,2) NOT NULL COMMENT 'Jumlah markah yang student perolehi by Percentage',
  `TotalMarks` decimal(10,2) DEFAULT NULL COMMENT 'Jumlah markah keseluruhan',
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `pull_status` tinyint(4) DEFAULT NULL,
  `pull_date` datetime DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `tbe_id` int(11) DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IdStudentMarksEntryDetail` (`IdStudentMarksEntryDetail`),
  KEY `IdStudentMarksEntry` (`IdStudentMarksEntry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_generated_id`;
CREATE TABLE `tbl_student_generated_id` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) NOT NULL,
  `YYYY` int(4) DEFAULT '0',
  `YY` tinyint(4) DEFAULT '0',
  `seqno` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `px` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT ' ',
  `iid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `b` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `a` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `g` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `UpdDate` datetime NOT NULL,
  `p` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `UpdUser` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Table to store student generated id';


DROP TABLE IF EXISTS `tbl_student_grade`;
CREATE TABLE `tbl_student_grade` (
  `sg_id` int(11) NOT NULL AUTO_INCREMENT,
  `sg_IdStudentRegistration` bigint(20) NOT NULL COMMENT 'fk tbl_studentregistration',
  `sg_semesterId` int(11) NOT NULL COMMENT 'fk tbl_semestermain',
  `sg_idstudentsemsterstatus` bigint(20) NOT NULL COMMENT 'fk tbl_studentsemesterstatus',
  `sg_sem_credithour` int(11) NOT NULL,
  `sg_sem_totalpoint` decimal(10,2) NOT NULL,
  `sg_gpa` decimal(10,2) NOT NULL,
  `sg_gpa_status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sg_cum_credithour` int(11) NOT NULL,
  `sg_cum_totalpoint` decimal(10,2) NOT NULL,
  `sg_cgpa` decimal(10,2) NOT NULL,
  `sg_cgpa_status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sg_id`),
  KEY `sg_registrationId` (`sg_IdStudentRegistration`),
  KEY `sg_semesterId` (`sg_semesterId`),
  KEY `sg_idstudentsemsterstatus` (`sg_idstudentsemsterstatus`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_marks_entry`;
CREATE TABLE `tbl_student_marks_entry` (
  `IdStudentMarksEntry` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSemester` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdIntake` int(11) DEFAULT NULL,
  `IdProgram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pe_id` int(11) DEFAULT NULL,
  `es_id` int(11) DEFAULT NULL,
  `er_id` int(11) DEFAULT NULL,
  `Course` bigint(20) DEFAULT NULL,
  `mds_id` int(11) DEFAULT NULL COMMENT 'tbl_markdistribution_setup',
  `IdMarksDistributionMaster` int(11) NOT NULL COMMENT 'fk tbl_marksdistributionmaster',
  `Component` bigint(20) DEFAULT NULL COMMENT 'IdMarksDistributionMaster',
  `ComponentItem` bigint(20) DEFAULT NULL COMMENT 'tidak digunakan',
  `AttendanceStatus` bigint(20) DEFAULT NULL,
  `Instructor` bigint(20) DEFAULT NULL COMMENT 'tidak digunakan',
  `MarksEntryStatus` bigint(20) DEFAULT NULL COMMENT 'tidak digunakan',
  `ApprovedBy` bigint(20) DEFAULT NULL COMMENT 'tidak digunakan',
  `ApprovedOn` datetime DEFAULT NULL COMMENT 'tidak digunakan',
  `Remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'tidak digunakan',
  `IdStudentRegistration` bigint(20) DEFAULT NULL COMMENT 'fk tbl_studentregistration',
  `IdStudentRegSubjects` bigint(20) DEFAULT NULL COMMENT 'fk tbl_studentregsubjects',
  `MarksTotal` decimal(10,2) DEFAULT NULL COMMENT 'tidak digunakan',
  `TotalMarkObtained` decimal(10,2) DEFAULT NULL COMMENT '(Raw Mark) Jumlah markah yang student perolehi',
  `FinalTotalMarkObtained` decimal(10,2) NOT NULL COMMENT 'ini markah setelah pengiraan percentage component',
  `TotalMarkObtainedScaling` decimal(10,2) DEFAULT NULL COMMENT 'tidak digunakan',
  `TotalMarkObtainedResit` decimal(10,2) DEFAULT NULL COMMENT 'tidak digunakan',
  `CreatedDt` datetime DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `pull_status` tinyint(4) DEFAULT NULL,
  `pull_date` datetime DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `tbe_id` int(11) DEFAULT NULL,
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_examresult_migratedt` datetime DEFAULT NULL,
  PRIMARY KEY (`IdStudentMarksEntry`),
  KEY `IdStudentMarksEntry` (`IdStudentMarksEntry`),
  KEY `IdSemester` (`IdSemester`),
  KEY `Course` (`Course`),
  KEY ` IdMarksDistributionMaster` (`IdMarksDistributionMaster`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`),
  KEY `IdStudentRegSubjects` (`IdStudentRegSubjects`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_marks_entry_history`;
CREATE TABLE `tbl_student_marks_entry_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentMarksEntry` bigint(20) NOT NULL,
  `IdSemester` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdIntake` int(11) DEFAULT NULL,
  `IdProgram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pe_id` int(11) DEFAULT NULL,
  `es_id` int(11) DEFAULT NULL,
  `er_id` int(11) DEFAULT NULL,
  `Course` bigint(20) DEFAULT NULL,
  `mds_id` int(11) DEFAULT NULL COMMENT 'tbl_markdistribution_setup',
  `IdMarksDistributionMaster` int(11) NOT NULL COMMENT 'fk tbl_marksdistributionmaster',
  `Component` bigint(20) DEFAULT NULL,
  `ComponentItem` bigint(20) DEFAULT NULL COMMENT 'tidak digunakan',
  `AttendanceStatus` bigint(20) DEFAULT NULL,
  `Instructor` bigint(20) DEFAULT NULL COMMENT 'tidak digunakan',
  `MarksEntryStatus` bigint(20) DEFAULT NULL COMMENT 'tidak digunakan',
  `ApprovedBy` bigint(20) DEFAULT NULL COMMENT 'tidak digunakan',
  `ApprovedOn` datetime DEFAULT NULL COMMENT 'tidak digunakan',
  `Remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'tidak digunakan',
  `IdStudentRegistration` bigint(20) DEFAULT NULL COMMENT 'fk tbl_studentregistration',
  `IdStudentRegSubjects` bigint(20) DEFAULT NULL COMMENT 'fk tbl_studentregsubjects',
  `MarksTotal` decimal(10,2) DEFAULT NULL COMMENT 'tidak digunakan',
  `TotalMarkObtained` decimal(10,2) DEFAULT NULL COMMENT '(Raw Mark) Jumlah markah yang student perolehi',
  `FinalTotalMarkObtained` decimal(10,2) NOT NULL COMMENT 'ini markah setelah pengiraan percentage component',
  `TotalMarkObtainedScaling` decimal(10,2) DEFAULT NULL COMMENT 'tidak digunakan',
  `TotalMarkObtainedResit` decimal(10,2) DEFAULT NULL COMMENT 'tidak digunakan',
  `UpdUser` bigint(20) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `message` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_date` datetime DEFAULT NULL,
  `pull_status` tinyint(4) DEFAULT NULL,
  `pull_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IdStudentMarksEntry` (`IdStudentMarksEntry`),
  KEY `IdSemester` (`IdSemester`(191)),
  KEY `Course` (`Course`),
  KEY ` IdMarksDistributionMaster` (`IdMarksDistributionMaster`),
  KEY `IdStudentRegistration` (`IdStudentRegistration`),
  KEY `IdStudentRegSubjects` (`IdStudentRegSubjects`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_mark_change_status`;
CREATE TABLE `tbl_student_mark_change_status` (
  `cs_id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegSubjects` int(11) NOT NULL,
  `cs_status` tinyint(4) NOT NULL,
  `cs_remarks` longtext COLLATE utf8mb4_unicode_ci,
  `cs_createdby` int(11) NOT NULL,
  `cs_createddt` datetime NOT NULL,
  PRIMARY KEY (`cs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_preffered`;
CREATE TABLE `tbl_student_preffered` (
  `IdStudentPreferred` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) NOT NULL,
  `IdIntake` bigint(20) NOT NULL,
  `IdProgramLevel` bigint(20) NOT NULL,
  `IdPriorityNo` bigint(20) NOT NULL,
  `IdProgram` bigint(20) NOT NULL,
  `IdBranch` bigint(20) NOT NULL,
  `IdScheme` bigint(20) NOT NULL,
  `CreatedBy` bigint(20) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  PRIMARY KEY (`IdStudentPreferred`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_release_status`;
CREATE TABLE `tbl_student_release_status` (
  `IdStudentRelease` bigint(10) NOT NULL AUTO_INCREMENT,
  `IdStudent` bigint(10) NOT NULL,
  `IdCategory` bigint(10) NOT NULL,
  `IdReleaseType` bigint(10) NOT NULL,
  `IdIntake` bigint(10) NOT NULL,
  `Reason` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdUser` bigint(10) NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdStudentRelease`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Table to save student release data';


DROP TABLE IF EXISTS `tbl_student_resit`;
CREATE TABLE `tbl_student_resit` (
  `sr_id` int(11) NOT NULL AUTO_INCREMENT,
  `sr_idStudentRegistration` int(11) NOT NULL,
  `sr_idSemester` int(11) NOT NULL,
  `sr_idSubject` int(11) NOT NULL,
  `sr_idComponent` int(11) NOT NULL COMMENT 'fk markdistributionmaster',
  `sr_charge` decimal(10,2) NOT NULL,
  `sr_applyDate` datetime NOT NULL,
  `sr_applyBy` int(11) NOT NULL,
  `sr_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: Apply 2:Approve 3:Reject',
  `sr_approvedDate` datetime NOT NULL,
  `sr_approvedBy` int(11) NOT NULL,
  PRIMARY KEY (`sr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_status_history`;
CREATE TABLE `tbl_student_status_history` (
  `IdStudentHistory` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` bigint(20) NOT NULL,
  `profileStatus` bigint(20) NOT NULL,
  `IpAddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` tinyint(4) DEFAULT NULL COMMENT '1: Change Status',
  `UpdUser` bigint(20) NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdStudentHistory`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_student_upload`;
CREATE TABLE `tbl_student_upload` (
  `upl_id` int(11) NOT NULL AUTO_INCREMENT,
  `std_id` int(11) NOT NULL,
  `IdStudentRegistration` int(11) DEFAULT NULL,
  `upl_corporate_id` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT '0' COMMENT 'batch_registration',
  `invoice_id` int(11) DEFAULT '0',
  `module_change_status_id` int(11) DEFAULT '0' COMMENT 'FK module_change_status',
  `examregistration_main_id` int(11) DEFAULT '0' COMMENT 'FK exam_registration_main',
  `exemption_main_id` int(11) DEFAULT '0' COMMENT 'FK exemption_main',
  `type` int(11) DEFAULT NULL COMMENT '0=batch registration, 1=proof of payment, 2=inhouse document, 3=supporting document',
  `upl_filename` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `upl_filerename` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `upl_filelocation` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `upl_filesize` int(11) NOT NULL,
  `upl_mimetype` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `upl_upddate` datetime DEFAULT NULL,
  `upl_upduser` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT 'type=1 -> (0=entry,1=approved,2=rejected); type=2 -> (0=disabled,1=active)',
  `status_date` datetime DEFAULT NULL,
  `document_type` int(11) DEFAULT NULL COMMENT '	ref table: tbl_definationms ',
  PRIMARY KEY (`upl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `tbl_subcredithoursdistrbtn`;
CREATE TABLE `tbl_subcredithoursdistrbtn` (
  `IdSubjcredithoursdistrbtn` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_subjectmaster',
  `CreditHour` decimal(12,2) NOT NULL DEFAULT '0.00',
  `Idcomponents` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'FK TO tbl_definitionms',
  `IdcomponentItem` bigint(20) NOT NULL DEFAULT '0',
  `componentType` int(11) NOT NULL COMMENT 'fk tbl_definationms',
  `instructorContactHours` decimal(12,2) NOT NULL,
  `workloadHours` decimal(12,2) NOT NULL,
  `OEEWorkloadHours` decimal(12,2) NOT NULL,
  `methodOfTeaching` int(11) NOT NULL COMMENT 'fk tbl_definationms',
  `attendanceType` int(11) NOT NULL COMMENT 'fk tbl_definationms',
  PRIMARY KEY (`IdSubjcredithoursdistrbtn`),
  KEY `IdSubject` (`IdSubject`,`Idcomponents`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subject`;
CREATE TABLE `tbl_subject` (
  `IdSubject` bigint(11) NOT NULL AUTO_INCREMENT,
  `SubjectName` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `SubjectCode` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `SubjectCatagory` bigint(11) DEFAULT NULL,
  `Institution` bigint(11) NOT NULL DEFAULT '0',
  `UpdUser` bigint(11) NOT NULL,
  `UpdDate` date NOT NULL,
  `SubjectCodeFormat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdFormat` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IdSubject`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectcoordinatorlist`;
CREATE TABLE `tbl_subjectcoordinatorlist` (
  `IdSubjectCoordinatorList` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdSemester` int(11) NOT NULL DEFAULT '0',
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_subjectmaster',
  `IdStaff` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_staffmaster',
  `FromDate` date DEFAULT NULL,
  `ToDate` date DEFAULT NULL,
  `Active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1: Active 0:Inactive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  PRIMARY KEY (`IdSubjectCoordinatorList`),
  KEY `tbl_subjectcoordinatorlist_ibfk_1` (`UpdUser`),
  KEY `tbl_subjectcoordinatorlist_ibfk_2` (`IdSubject`),
  KEY `tbl_subjectcoordinatorlist_ibfk_3` (`IdStaff`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectdetails`;
CREATE TABLE `tbl_subjectdetails` (
  `IdSubjectDetails` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSubject` bigint(20) NOT NULL,
  `SubjectDetailId` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SubjectDetailDesc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` date NOT NULL,
  `UpdUser` date NOT NULL,
  PRIMARY KEY (`IdSubjectDetails`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectequivalent`;
CREATE TABLE `tbl_subjectequivalent` (
  `idequivalent` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idsubject` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_subjectmaster',
  `idsubjectequivalent` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_subjectmaster',
  `Active` binary(1) DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `UpdUser` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_user',
  PRIMARY KEY (`idequivalent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectgradepoint`;
CREATE TABLE `tbl_subjectgradepoint` (
  `Idsubjectgradepoint` bigint(20) NOT NULL AUTO_INCREMENT,
  `Idsubjectgradetype` bigint(20) NOT NULL,
  `subjectgrade` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subjectgradedescription` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subjectgradepoint` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`Idsubjectgradepoint`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectgradetype`;
CREATE TABLE `tbl_subjectgradetype` (
  `Idsubjectgradetype` bigint(20) NOT NULL AUTO_INCREMENT,
  `Iduniversity` bigint(20) NOT NULL,
  `subjectcodetype` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subjectcodedescription` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Idqualification` bigint(20) NOT NULL,
  PRIMARY KEY (`Idsubjectgradetype`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectmarksentry`;
CREATE TABLE `tbl_subjectmarksentry` (
  `idSubjectMarksEntry` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdMarksDistributionDetails` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_marksdistributiondetails',
  `Year` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdSemester` int(20) NOT NULL,
  `idStaff` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdStudentRegistration` bigint(20) unsigned NOT NULL,
  `idSubject` bigint(20) unsigned NOT NULL,
  `StudentId` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Grade` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subjectmarks` decimal(12,2) NOT NULL DEFAULT '0.00',
  `SubjectCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` binary(1) NOT NULL DEFAULT '1' COMMENT '1:Active 0:inactive',
  `UpdUser` bigint(20) unsigned NOT NULL DEFAULT '1' COMMENT 'FK to tbl_user',
  `UpdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idSubjectMarksEntry`),
  KEY `idSubject` (`StudentId`),
  KEY `UpdUser` (`UpdUser`),
  KEY `idStaff` (`IdSemester`),
  KEY `IdStudentRegistration` (`Year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectmaster`;
CREATE TABLE `tbl_subjectmaster` (
  `IdSubject` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdFaculty` bigint(20) unsigned NOT NULL,
  `IdDepartment` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  `SubjectName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ShortName` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subjectMainDefaultLanguage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `courseDescription` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BahasaIndonesia` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ArabicName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SubCode` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` tinyint(1) unsigned NOT NULL COMMENT '1: Active 0:Inactive',
  `CreditHours` float NOT NULL,
  `AmtPerHour` decimal(12,2) NOT NULL DEFAULT '0.00',
  `CourseType` bigint(20) unsigned NOT NULL DEFAULT '1',
  `MinCreditHours` int(11) NOT NULL DEFAULT '0',
  `IdDefaultSyllabus` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_Syllabus',
  `ClassTimeTable` binary(1) NOT NULL DEFAULT '0' COMMENT '1:With class timetable;0:Without class timetable',
  `ExamTimeTable` binary(1) NOT NULL DEFAULT '0' COMMENT '1:With exam timetable;0:Without exam timetable',
  `ReligiousSubject` binary(1) NOT NULL DEFAULT '0' COMMENT '1:Religious;0:Non religious',
  `IdReligion` bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definitionms',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `SubCodeFormat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdFormat` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Group` int(11) NOT NULL DEFAULT '0',
  `audit` tinyint(4) NOT NULL DEFAULT '0',
  `singleModule` tinyint(4) NOT NULL DEFAULT '0',
  `specialization` int(11) DEFAULT NULL,
  `SubCodeNew` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CurrentSubCode` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_language` tinyint(4) DEFAULT '1' COMMENT 'used for qbank',
  `tag_taxanomy` tinyint(4) DEFAULT NULL COMMENT 'used for qbank (1=bloom, 2=difficulty, 0=n/a)',
  PRIMARY KEY (`IdSubject`),
  UNIQUE KEY `SubCode` (`SubCode`),
  KEY `UpdUser` (`UpdUser`),
  KEY `IdDepartment` (`IdDepartment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectmaster_bkp`;
CREATE TABLE `tbl_subjectmaster_bkp` (
  `IdSubject` bigint(20) unsigned NOT NULL DEFAULT '0',
  `IdFaculty` bigint(20) unsigned NOT NULL,
  `IdDepartment` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_departmentmaster',
  `SubjectName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ShortName` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `courseDescription` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BahasaIndonesia` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ArabicName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SubCode` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Active` binary(1) NOT NULL COMMENT '1: Active 0:Inactive',
  `CreditHours` float NOT NULL,
  `AmtPerHour` decimal(12,2) NOT NULL DEFAULT '0.00',
  `CourseType` bigint(20) unsigned NOT NULL DEFAULT '1',
  `MinCreditHours` int(11) NOT NULL,
  `IdDefaultSyllabus` bigint(20) unsigned DEFAULT NULL COMMENT 'Foreign key to tbl_Syllabus',
  `ClassTimeTable` binary(1) NOT NULL DEFAULT '0' COMMENT '1:With class timetable;0:Without class timetable',
  `ExamTimeTable` binary(1) NOT NULL DEFAULT '0' COMMENT '1:With exam timetable;0:Without exam timetable',
  `ReligiousSubject` binary(1) NOT NULL DEFAULT '0' COMMENT '1:Religious;0:Non religious',
  `IdReligion` bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK to tbl_definitionms',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `SubCodeFormat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdFormat` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectprerequisites`;
CREATE TABLE `tbl_subjectprerequisites` (
  `IdSubjectPrerequisites` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdLandscape` int(11) NOT NULL DEFAULT '0' COMMENT ' fk tbl_landscape',
  `IdLandscapeSub` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_landscapesubject',
  `IdLandscapeblocksubject` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_landscapeblocksubject',
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subjectmaster',
  `IdRequiredSubject` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subjectmaster',
  `PrerequisiteType` int(4) NOT NULL COMMENT '0:Pass With Grade 1:Total Credit Hours 2: Co-requisite',
  `PrerequisiteGrade` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TotalCreditHours` smallint(10) unsigned DEFAULT NULL,
  `MinCreditHours` decimal(12,2) NOT NULL,
  `createddt` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`IdSubjectPrerequisites`),
  KEY `IdSubject` (`IdSubject`),
  KEY `IdRequiredSubject` (`IdRequiredSubject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectprogram`;
CREATE TABLE `tbl_subjectprogram` (
  `IdSubjectProgram` int(11) NOT NULL AUTO_INCREMENT,
  `IdProgram` bigint(20) NOT NULL COMMENT 'FK to tbl_program',
  `IdSubject` bigint(20) DEFAULT '0' COMMENT 'FK to tbl_subject',
  `Mark` bigint(20) NOT NULL,
  `UpdUser` bigint(20) NOT NULL COMMENT 'FK to tbl_user',
  `UpdDate` date NOT NULL,
  `Active` binary(1) NOT NULL COMMENT '1=Active;0=Inactive',
  `SubjectNamenew` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CheckSubject` binary(1) NOT NULL COMMENT '1:from Idsubject;0: from SubjectName',
  PRIMARY KEY (`IdSubjectProgram`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectregistration`;
CREATE TABLE `tbl_subjectregistration` (
  `IdSubjectRegister` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_program',
  `EffectiveDate` date NOT NULL,
  `AcademicStatus` int(11) NOT NULL COMMENT '0=GPA;1=CGPA',
  `TerminateStatus` bigint(20) NOT NULL COMMENT 'FK to tbl_definitionms',
  `Semester` int(11) NOT NULL,
  `VirtualSemester` int(11) NOT NULL,
  `Min` float NOT NULL,
  `Max` float NOT NULL,
  `Subjects` int(11) NOT NULL,
  `UpdUser` bigint(20) NOT NULL COMMENT 'FK to tbl_users',
  `UpdDate` date NOT NULL,
  `Active` binary(1) NOT NULL COMMENT '0=Inactive;1=Active',
  PRIMARY KEY (`IdSubjectRegister`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectsoffered`;
CREATE TABLE `tbl_subjectsoffered` (
  `IdSubjectsOffered` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `IdSemester` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_semester',
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_subject',
  `Branch` int(11) NOT NULL,
  `MinQuota` int(11) NOT NULL,
  `MaxQuota` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `reservation` int(11) NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  PRIMARY KEY (`IdSubjectsOffered`),
  KEY `IdSemester` (`IdSemester`),
  KEY `IdSubject` (`IdSubject`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectstaffverification`;
CREATE TABLE `tbl_subjectstaffverification` (
  `IdSubjectStaffVerification` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdStaff` bigint(20) unsigned NOT NULL DEFAULT '0',
  `IdSemester` bigint(20) unsigned NOT NULL,
  `IdSubject` bigint(20) NOT NULL COMMENT 'FK to tbl_subject',
  `SameLecturer` binary(1) NOT NULL,
  `Active` binary(1) NOT NULL COMMENT '1:Active;0:Inacive',
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` date NOT NULL,
  PRIMARY KEY (`IdSubjectStaffVerification`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_subjectwithdrawalpolicy`;
CREATE TABLE `tbl_subjectwithdrawalpolicy` (
  `IdSubjectWithdrawalPolicy` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSemester` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_semester',
  `IdSubject` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_subject',
  `Days` bigint(20) unsigned NOT NULL,
  `Percentage` float NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  `UpdDate` datetime NOT NULL,
  `Active` binary(1) NOT NULL,
  PRIMARY KEY (`IdSubjectWithdrawalPolicy`),
  KEY `IdSemester` (`IdSemester`),
  KEY `IdSubject` (`IdSubject`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_takafuloperator`;
CREATE TABLE `tbl_takafuloperator` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_type` int(11) DEFAULT '1026',
  `registrationId` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_number` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `countryId` int(11) DEFAULT NULL,
  `stateId` int(11) DEFAULT NULL,
  `stateOther` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'if country != malaysia',
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` int(11) DEFAULT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_type` int(11) DEFAULT NULL,
  `business_type_other` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'if business type == others',
  `person_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_contact_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `individual` int(11) DEFAULT NULL,
  `inhouse` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `upd_by` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0=entry, 1=approved, 2=cancel, 3=rejected',
  `location` int(11) DEFAULT NULL COMMENT '0=local, 1=international',
  `at_repository` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migration_date` datetime DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `tbe` int(11) DEFAULT '0',
  `contact_main_id` int(11) unsigned DEFAULT NULL,
  `send_email_set_password` tinyint(4) DEFAULT '0',
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exam_closing_date` int(10) unsigned DEFAULT '0' COMMENT 'in days. if 0, follow default by program',
  `number_of_staff` int(10) unsigned DEFAULT NULL COMMENT 'dashboard',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_takaful_contactperson`;
CREATE TABLE `tbl_takaful_contactperson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `takafuloperator_id` int(11) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `designation_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `ismain` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `upd_by` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `userKey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_tempprogramaccreditiondetails`;
CREATE TABLE `tbl_tempprogramaccreditiondetails` (
  `IdTempProgramAccreditionDetails` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `AccredictionDate` date DEFAULT NULL,
  `AccredictionReferences` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl3` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl4` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl5` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl6` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl7` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl8` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl9` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl10` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl11` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl12` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl13` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl14` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl15` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl16` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl17` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl18` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl19` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccDtl20` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccreditionType` bigint(20) NOT NULL COMMENT 'fk to tbl_definitonms',
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime NOT NULL,
  `unicode` bigint(20) NOT NULL,
  `Date` date NOT NULL,
  `sessionId` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idExists` bigint(20) NOT NULL,
  `deleteFlag` binary(1) NOT NULL,
  PRIMARY KEY (`IdTempProgramAccreditionDetails`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_tempprogramentryrequirement`;
CREATE TABLE `tbl_tempprogramentryrequirement` (
  `IdTemp` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `Desc` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Item` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Unit` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Condition` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Value` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Validity` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EntryLevel` bigint(20) NOT NULL,
  `IdSpecialization` bigint(20) unsigned NOT NULL,
  `Mandatory` binary(1) NOT NULL COMMENT '1: Yes 0:No',
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Description` int(255) DEFAULT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `unicode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GroupId` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  `sessionId` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idExists` bigint(20) NOT NULL,
  `deleteFlag` binary(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IdTemp`),
  KEY `tbl_programentryrequirement_ibfk_1` (`UpdUser`),
  KEY `tbl_programentryrequirement_ibfk_3` (`Item`),
  KEY `tbl_programentryrequirement_ibfk_4` (`Unit`),
  KEY `tbl_programentryrequirement_ibfk_5` (`Condition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_tempprogramentryrequirementotherdetail`;
CREATE TABLE `tbl_tempprogramentryrequirementotherdetail` (
  `IdTemp` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `GroupId` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Item` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Condition` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_definitionms',
  `Value` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign key to tbl_user',
  `Date` date NOT NULL,
  `sessionId` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idExists` bigint(20) NOT NULL,
  `deleteFlag` binary(1) NOT NULL DEFAULT '1',
  `unicode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`IdTemp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_tempprogramquota`;
CREATE TABLE `tbl_tempprogramquota` (
  `IdTemp` int(11) NOT NULL AUTO_INCREMENT,
  `IdProgramQuota` bigint(20) unsigned NOT NULL COMMENT 'Primary Key',
  `IdProgram` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_program',
  `IdQuota` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_definationms',
  `Quota` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'Foreign Key to tbl_user',
  `UpdDate` datetime NOT NULL,
  `Date` date NOT NULL,
  `sessionId` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idExists` bigint(20) NOT NULL,
  `deleteFlag` binary(1) NOT NULL,
  PRIMARY KEY (`IdTemp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_tempstaffsubject`;
CREATE TABLE `tbl_tempstaffsubject` (
  `idTempStaffSubject` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSubject` bigint(20) NOT NULL,
  `unicode` bigint(20) NOT NULL,
  `Date` date NOT NULL,
  `sessionId` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idExists` bigint(20) NOT NULL,
  `deleteFlag` binary(1) NOT NULL,
  PRIMARY KEY (`idTempStaffSubject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_tempsubjectprerequisites`;
CREATE TABLE `tbl_tempsubjectprerequisites` (
  `IdTempSubjectPrerequisites` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdRequiredSubject` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_subject master',
  `PrerequisiteType` int(4) NOT NULL,
  `PrerequisiteGrade` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `unicode` bigint(20) NOT NULL,
  `Date` date NOT NULL,
  `sessionId` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `idExists` bigint(20) NOT NULL,
  `deleteFlag` binary(1) NOT NULL,
  PRIMARY KEY (`IdTempSubjectPrerequisites`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `tbl_tooltip_setup`;
CREATE TABLE `tbl_tooltip_setup` (
  `tts_id` int(11) NOT NULL AUTO_INCREMENT,
  `tts_program_id` int(11) NOT NULL COMMENT 'fk to tbl_program',
  `tts_item_id` int(11) NOT NULL COMMENT 'fk to tbl_application_item',
  `tts_value_id` int(11) NOT NULL,
  `tts_desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tts_updUser` int(11) NOT NULL,
  `tts_updDate` datetime NOT NULL,
  PRIMARY KEY (`tts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_top_menu`;
CREATE TABLE `tbl_top_menu` (
  `tm_id` int(11) NOT NULL AUTO_INCREMENT,
  `tm_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tm_desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `tm_module` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tm_controller` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tm_action` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tm_seq_order` int(11) NOT NULL DEFAULT '0',
  `tm_visibility` tinyint(11) NOT NULL DEFAULT '1' COMMENT '1:show 0:hide',
  PRIMARY KEY (`tm_id`),
  KEY `tm_id` (`tm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_tosdetail`;
CREATE TABLE `tbl_tosdetail` (
  `IdTOSDetail` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdTOS` bigint(20) unsigned DEFAULT NULL COMMENT 'Foregin Key from TOS Master',
  `IdSection` varchar(20) DEFAULT NULL,
  `NosOfQuestion` bigint(20) NOT NULL,
  `TOSDetailStatus` binary(1) NOT NULL DEFAULT '0' COMMENT '0: Active , 1 : InActive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `sectionpercentage` varchar(20) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `syllabus_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdTOSDetail`),
  KEY `FK_tbl_TOSDetail_IdTOS` (`IdTOS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_tosmaster`;
CREATE TABLE `tbl_tosmaster` (
  `IdTOS` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdBatch` bigint(20) unsigned DEFAULT NULL COMMENT 'Foregin Key from tbl_BatchMaster',
  `NosOfQues` bigint(20) NOT NULL,
  `TimeLimit` int(11) DEFAULT NULL,
  `AlertTime` int(11) DEFAULT NULL,
  `TOSStatus` binary(1) NOT NULL DEFAULT '0' COMMENT '0: Active , 1 : InActive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `Pass` varchar(45) DEFAULT NULL,
  `Active` binary(1) NOT NULL DEFAULT '0' COMMENT '0:Inactive,1:Active',
  `Setcode` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IdTOS`),
  KEY `FK_tbl_TOSMaster_IdBatch` (`IdBatch`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS `tbl_tossubdetail`;
CREATE TABLE `tbl_tossubdetail` (
  `IdTOSSubDetail` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdTOSDetail` bigint(20) unsigned DEFAULT NULL COMMENT 'Foregin Key from TOS Master',
  `IdDiffcultLevel` bigint(20) unsigned DEFAULT NULL COMMENT 'Foregin Key from Difficult Level Master',
  `TOSDetailStatus` binary(1) NOT NULL DEFAULT '0' COMMENT '0: Active , 1 : InActive',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `NoofQuestions` decimal(10,0) NOT NULL DEFAULT '0',
  `IdSection` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `syllabus_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdTOSSubDetail`),
  KEY `FK_tbl_TOSSubDetail_IdTOSDetail` (`IdTOSDetail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_universitymaster`;
CREATE TABLE `tbl_universitymaster` (
  `IdUniversity` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `Univ_Name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Univ_ArabicName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ShortName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Country` bigint(20) unsigned DEFAULT NULL COMMENT 'FK to tbl_countries',
  `Url` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Active` binary(1) NOT NULL COMMENT '0:Inactive, 1:Active',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_user',
  PRIMARY KEY (`IdUniversity`),
  KEY `Country` (`Country`),
  KEY `UpdUser` (`UpdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `iduser` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdStaff` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_staff',
  `loginName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passwd` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userArabicName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameField1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField4` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField5` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DOB` date NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '1',
  `addr1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addr2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipCode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `homePhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `workPhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cellPhone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` mediumtext COLLATE utf8mb4_unicode_ci,
  `IdRole` bigint(20) unsigned DEFAULT NULL,
  `NoOfLoginAttempt` tinyint(3) unsigned DEFAULT '0',
  `LastLogAttemptOn` datetime DEFAULT NULL,
  `LockStatus` binary(1) NOT NULL DEFAULT '0' COMMENT '0:Unlock, 1 :Lock',
  `UserStatus` tinyint(4) NOT NULL COMMENT '1:Active, 0:Inactive',
  `userkey` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `role_in_moodle` int(11) DEFAULT NULL COMMENT 'Manager - 1, CourseCreator - 2, EditingTeacher - 3, Teacher - 4, Student - 5, User - 7',
  PRIMARY KEY (`iduser`),
  KEY `mName` (`mName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_user_ori`;
CREATE TABLE `tbl_user_ori` (
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) DEFAULT NULL,
  `iduser` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdStaff` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_staff',
  `loginName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passwd` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userArabicName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameField1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField4` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameField5` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DOB` date NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '1',
  `addr1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addr2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipCode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `homePhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `workPhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cellPhone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdRole` bigint(20) unsigned DEFAULT NULL,
  `NoOfLoginAttempt` tinyint(3) unsigned DEFAULT '0',
  `LastLogAttemptOn` datetime DEFAULT NULL,
  `LockStatus` binary(1) NOT NULL DEFAULT '0' COMMENT '0:Unlock, 1 :Lock',
  `UserStatus` binary(1) NOT NULL COMMENT '1:Active, 0:Inactive',
  PRIMARY KEY (`iduser`),
  KEY `mName` (`mName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_varifiedprogramchecklist`;
CREATE TABLE `tbl_varifiedprogramchecklist` (
  `IdVarifiedProgramChecklist` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) unsigned NOT NULL,
  `IdCheckList` bigint(20) unsigned NOT NULL,
  `IdProgram` bigint(20) unsigned NOT NULL,
  `Varified` tinyint(1) DEFAULT '0',
  `Status` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Comments` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdVarifiedProgramChecklist`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_verifiermarks`;
CREATE TABLE `tbl_verifiermarks` (
  `idVerifierMarks` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idSubjectMarksEntry` bigint(20) unsigned NOT NULL COMMENT 'FK to tbl_subjectsmarksentry',
  `idverifier` bigint(20) DEFAULT NULL,
  `Rank` int(20) DEFAULT NULL,
  `verifiresubjectmarks` decimal(12,2) NOT NULL DEFAULT '0.00',
  `UpdDate` datetime NOT NULL,
  `UpdUser` bigint(20) NOT NULL COMMENT 'FK to tbl_user',
  `Active` binary(1) NOT NULL,
  PRIMARY KEY (`idVerifierMarks`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_verifypayments`;
CREATE TABLE `tbl_verifypayments` (
  `IdVerifyPayments` bigint(10) NOT NULL AUTO_INCREMENT,
  `IdApplication` bigint(20) unsigned NOT NULL,
  `IdSemester` bigint(20) unsigned NOT NULL,
  `VerifyPayment` binary(1) NOT NULL COMMENT '0: Not-Verified,1:Verified',
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` datetime NOT NULL,
  PRIMARY KEY (`IdVerifyPayments`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_voucherdetails`;
CREATE TABLE `tbl_voucherdetails` (
  `idvoucherdetails` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idvoucher` bigint(20) unsigned NOT NULL COMMENT 'References tbl_vouchermaster',
  `amount` float(15,2) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idvoucherdetails`),
  KEY `idvoucher` (`idvoucher`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_vouchermaster`;
CREATE TABLE `tbl_vouchermaster` (
  `idvoucher` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Voucherdate` date NOT NULL DEFAULT '0000-00-00',
  `Vouchertype` int(11) NOT NULL,
  `Vouchernumber` bigint(50) NOT NULL,
  `idstudent` bigint(20) NOT NULL,
  `approved` binary(1) NOT NULL DEFAULT '0',
  `Fromdate` date NOT NULL DEFAULT '0000-00-00',
  `Todate` date NOT NULL DEFAULT '0000-00-00',
  `Upddate` datetime NOT NULL,
  `Upduser` bigint(20) NOT NULL,
  PRIMARY KEY (`idvoucher`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tbl_vouchertype`;
CREATE TABLE `tbl_vouchertype` (
  `idVoucherType` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `VoucherType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `VoucherCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdUser` bigint(20) unsigned NOT NULL,
  `UpdDate` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Active,0-Inactive',
  PRIMARY KEY (`idVoucherType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `topkek`;
CREATE TABLE `topkek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombo_awek` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `transaction_migs`;
CREATE TABLE `transaction_migs` (
  `mt_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mt_ol_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'transaction_online',
  `mt_currency_id` int(10) unsigned NOT NULL DEFAULT '1',
  `mt_amount` decimal(20,2) NOT NULL,
  `mt_txn_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_txn_date` datetime DEFAULT NULL,
  `mt_status` enum('ENTRY','SUCCESS','FAIL','CANCEL') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ENTRY',
  `mt_url` text COLLATE utf8mb4_unicode_ci COMMENT 'migs request url',
  `mt_rep_raw` text COLLATE utf8mb4_unicode_ci,
  `mt_rep_error_status` tinyint(4) DEFAULT NULL COMMENT '1: have error; 0: no error',
  `mt_rep_hash_validation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_amount` decimal(20,2) DEFAULT NULL,
  `mt_rep_locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_batch_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_command` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_message` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_card_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_order_info` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_receipt_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_merchant_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_authorize_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_merch_txn_ref` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_transaction_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_acq_response_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_txn_response_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_txn_response_code_desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_3d_rep_ver_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_ver_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_ver_status_desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_3d_rep_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_ver_secur_level` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_enrolled` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_xid` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_acq_eci` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_3d_rep_auth_status` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_registration_item_id` int(11) DEFAULT '889' COMMENT '889=Course, 879=Examination',
  `mt_from` tinyint(1) DEFAULT NULL COMMENT '0 - corporate, 1 = lms, 2=manually process',
  `mt_referer` text COLLATE utf8mb4_unicode_ci,
  `mt_notes` text COLLATE utf8mb4_unicode_ci,
  `mt_external_id` int(11) unsigned DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `program_id` int(11) NOT NULL DEFAULT '0',
  `manual` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `rcp_id` int(11) DEFAULT NULL,
  `rcp_date` datetime DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`mt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `transaction_migs_detail`;
CREATE TABLE `transaction_migs_detail` (
  `tmd_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tmd_tm_id` bigint(20) NOT NULL COMMENT 'FK transaction_migs',
  `tmd_proforma_invoice_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'FL proforma_invoice_detail',
  `tmd_invoice_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'FK invoice_detail',
  `tmd_amount` decimal(20,2) DEFAULT NULL,
  `tmd_date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`tmd_id`),
  KEY `tmd_tm_id` (`tmd_tm_id`,`tmd_proforma_invoice_id`,`tmd_invoice_id`),
  KEY `tmd_proforma_invoice_id` (`tmd_proforma_invoice_id`),
  KEY `tmd_invoice_id` (`tmd_invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `transaction_migs_history`;
CREATE TABLE `transaction_migs_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mt_id` bigint(20) unsigned NOT NULL,
  `mt_ol_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'transaction_online',
  `mt_currency_id` int(10) unsigned NOT NULL DEFAULT '1',
  `mt_amount` decimal(20,2) NOT NULL,
  `mt_txn_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_txn_date` datetime DEFAULT NULL,
  `mt_status` enum('ENTRY','SUCCESS','FAIL','CANCEL') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ENTRY',
  `mt_url` text COLLATE utf8mb4_unicode_ci COMMENT 'migs request url',
  `mt_rep_raw` text COLLATE utf8mb4_unicode_ci,
  `mt_rep_error_status` tinyint(4) DEFAULT NULL COMMENT '1: have error; 0: no error',
  `mt_rep_hash_validation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_amount` decimal(20,2) DEFAULT NULL,
  `mt_rep_locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_batch_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_command` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_message` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_card_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_order_info` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_receipt_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_merchant_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_authorize_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_merch_txn_ref` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_transaction_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_acq_response_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_txn_response_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_txn_response_code_desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_3d_rep_ver_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_ver_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_ver_status_desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_3d_rep_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_ver_secur_level` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_enrolled` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_xid` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_acq_eci` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_3d_rep_auth_status` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_registration_item_id` int(11) DEFAULT '889' COMMENT '889=Course, 879=Examination',
  `mt_from` tinyint(1) DEFAULT NULL COMMENT '0 - corporate, 1 = lms, 2=manually process',
  `mt_referer` text COLLATE utf8mb4_unicode_ci,
  `mt_notes` text COLLATE utf8mb4_unicode_ci,
  `mt_external_id` int(11) unsigned DEFAULT NULL,
  `UpdDate` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `program_id` int(11) NOT NULL DEFAULT '0',
  `manual` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `rcp_id` int(11) DEFAULT NULL,
  `rcp_date` datetime DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `transaction_migs_track`;
CREATE TABLE `transaction_migs_track` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mt_txn_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_id` int(11) DEFAULT NULL,
  `mt_txn_date` datetime DEFAULT NULL,
  `mt_status` enum('ENTRY','SUCCESS','FAIL') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ENTRY',
  `mt_url` text COLLATE utf8mb4_unicode_ci COMMENT 'migs request url',
  `mt_rep_raw` text COLLATE utf8mb4_unicode_ci,
  `mt_rep_error_status` tinyint(4) DEFAULT NULL COMMENT '1: have error; 0: no error',
  `mt_rep_hash_validation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_amount` decimal(20,2) DEFAULT NULL,
  `mt_rep_locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_batch_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_command` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_message` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_card_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_order_info` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_receipt_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_merchant_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_authorize_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_merch_txn_ref` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_transaction_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_acq_response_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_txn_response_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_rep_txn_response_code_desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_3d_rep_ver_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_ver_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_ver_status_desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_3d_rep_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_ver_secur_level` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_enrolled` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_xid` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_3d_rep_acq_eci` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_3d_rep_auth_status` mediumtext COLLATE utf8mb4_unicode_ci,
  `mt_get` text COLLATE utf8mb4_unicode_ci,
  `createddt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `transaction_online`;
CREATE TABLE `transaction_online` (
  `ol_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ol_currency_id` int(10) unsigned NOT NULL DEFAULT '1',
  `ol_amount` decimal(20,2) unsigned NOT NULL DEFAULT '0.00',
  `ol_service_charge` decimal(20,2) unsigned NOT NULL DEFAULT '0.00',
  `ol_type` int(10) DEFAULT NULL COMMENT '1=migs, 2=fpx',
  `ol_txn_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ol_txn_date` datetime DEFAULT NULL,
  `ol_status` enum('ENTRY','SUCCESS','FAIL') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ENTRY',
  `ol_url` text COLLATE utf8_unicode_ci COMMENT 'request url',
  `ol_return_url` text COLLATE utf8_unicode_ci COMMENT 'return url',
  `ol_from` int(11) DEFAULT NULL COMMENT '0 - corporate, 1 = lms, 2=manually process',
  `ol_notes` text COLLATE utf8_unicode_ci,
  `ol_external_id` int(11) unsigned DEFAULT NULL,
  `rcp_id` int(11) DEFAULT NULL,
  `rcp_date` datetime DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ol_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `zeta_email_data`;
CREATE TABLE `zeta_email_data` (
  `IDNumber` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `EmailID` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2018-03-05 10:00:36
