-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE TABLE `batch_exam_registration` (
  `ber_id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'batch_no',
  `corporate_id` int(11) NOT NULL COMMENT 'tbl_takafuloperator',
  `inhouse` int(11) NOT NULL DEFAULT '0' COMMENT '0:Non-Inhouse 1:Inhouse',
  `IdProgram` int(11) NOT NULL,
  `IdLandscape` int(11) NOT NULL,
  `examsetup_id` int(11) NOT NULL COMMENT 'exam_setup.es_id',
  `type_nationality` int(11) NOT NULL DEFAULT '579',
  `total_student` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee_structure.fs_id',
  `submitted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes',
  `paid_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Unpaid 1:Paid',
  `paymentmode` int(11) NOT NULL DEFAULT '0',
  `at_processing_fee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_role` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `tbe_id` int(11) DEFAULT NULL COMMENT 'tbe.idBatchRegistration',
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_updatedate` datetime DEFAULT NULL COMMENT 'to update wrong id',
  PRIMARY KEY (`ber_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `batch_exam_registration_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ber_id` int(11) NOT NULL,
  `batch_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'batch_no',
  `corporate_id` int(11) NOT NULL COMMENT 'tbl_takafuloperator',
  `inhouse` int(11) NOT NULL DEFAULT '0' COMMENT '0:Non-Inhouse 1:Inhouse',
  `IdProgram` int(11) NOT NULL,
  `IdLandscape` int(11) NOT NULL,
  `examsetup_id` int(11) NOT NULL COMMENT 'exam_setup.es_id',
  `type_nationality` int(11) NOT NULL DEFAULT '579',
  `total_student` int(11) NOT NULL DEFAULT '0',
  `fs_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fee_structure.fs_id',
  `submitted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes',
  `paid_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:Unpaid 1:Paid',
  `paymentmode` int(11) NOT NULL DEFAULT '0',
  `at_processing_fee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_role` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `cancel_apply_date` datetime DEFAULT NULL,
  `cancel_apply_by` int(11) NOT NULL DEFAULT '0',
  `cancel_approve_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Entry1=Approved 2=Rejected',
  `cancel_approve_date` datetime DEFAULT NULL,
  `cancel_approve_by` int(11) NOT NULL DEFAULT '0',
  `tbe_id` int(11) DEFAULT NULL COMMENT 'tbe.idBatchRegistration',
  `tbe_migratedate` datetime DEFAULT NULL,
  `tbe_updatedate` datetime DEFAULT NULL COMMENT 'to update wrong id',
  `remarks` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdStudentRegSubjects` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `batch_exam_schedule` (
  `bes_id` int(11) NOT NULL AUTO_INCREMENT,
  `ber_id` int(11) NOT NULL COMMENT 'batch_exam_registration.ber_id',
  `es_id` int(11) NOT NULL COMMENT 'exam_schedule.es_id',
  `examtaggingslot_id` int(11) NOT NULL,
  `examcenter_id` int(11) NOT NULL,
  `individual` int(11) NOT NULL DEFAULT '0' COMMENT '0:Main Schedule, 1:Individual',
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `tbe_migratedate` datetime NOT NULL,
  PRIMARY KEY (`bes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `batch_registration` (
  `btch_id` int(11) NOT NULL AUTO_INCREMENT,
  `btch_corporate_id` int(11) NOT NULL,
  `btch_upl_id` int(11) NOT NULL,
  `btch_no` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `btch_program_id` int(11) NOT NULL,
  `btch_scheme_id` int(11) NOT NULL,
  `btch_intake_id` int(11) DEFAULT NULL COMMENT 'tak pakai',
  `btch_landscape_id` int(11) NOT NULL,
  `btch_contact_person` int(11) NOT NULL COMMENT 'fk takaful_operator',
  `btch_type_nationality` int(11) DEFAULT NULL,
  `number_candidate` int(11) NOT NULL,
  `number_candidate_maximum` int(11) NOT NULL DEFAULT '0',
  `number_candidate_accepted` int(11) NOT NULL DEFAULT '0',
  `upload` int(11) NOT NULL COMMENT '1=excel,0=no',
  `closing_date` date DEFAULT '0000-00-00' COMMENT 'last date to add/edit participant',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `btch_fs_id` int(11) NOT NULL,
  `fs_id_upddate` datetime NOT NULL,
  `at_processing_fee` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_mode` int(11) DEFAULT NULL COMMENT 'fk payment_mode',
  `payment_mode_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1=done',
  `last_step` int(11) DEFAULT NULL COMMENT 'tbl_definationtypems 196',
  `step_date` datetime DEFAULT NULL,
  `btch_country_id` int(11) DEFAULT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `manual` int(11) DEFAULT '0' COMMENT '1=manual entry sms',
  `candidate_changes` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No Changes 1=Has Changes',
  `lms_access` int(11) DEFAULT '0' COMMENT '1=yes, 0=no ',
  `inhouse` int(11) DEFAULT '0' COMMENT '1=inhouse,0=normal ',
  `inhouse_status` int(11) DEFAULT '1' COMMENT '1=active, 0=inactive',
  `sendmail_date` datetime DEFAULT NULL,
  PRIMARY KEY (`btch_id`),
  KEY `btch_corporate_id` (`btch_corporate_id`),
  KEY `btch_upl_id` (`btch_upl_id`),
  KEY `btch_program_id` (`btch_program_id`),
  KEY `btch_scheme_id` (`btch_scheme_id`),
  KEY `btch_intake_id` (`btch_intake_id`),
  KEY `btch_landscape_id` (`btch_landscape_id`),
  KEY `btch_fs_id` (`btch_fs_id`),
  KEY `payment_mode` (`payment_mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `batch_registration_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `btch_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `IdCourseTaggingGroup` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_iduser` int(11) DEFAULT NULL,
  `created_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_iduser` int(11) DEFAULT NULL,
  `updated_role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `batch_registration_schedule` (
  `brs_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment',
  `br_id` int(11) NOT NULL COMMENT 'batch_registration.btch_id',
  `cgs_id` int(11) NOT NULL COMMENT 'course_group_schedule.sc_id',
  `course_id` int(11) NOT NULL DEFAULT '0' COMMENT 'perlu ke ada field ni??',
  `individual` int(11) NOT NULL COMMENT '0:Main Schedule, 1:Individual',
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '0:Inactive, 1:Active',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`brs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `certificate_template` (
  `crt_id` int(11) NOT NULL AUTO_INCREMENT,
  `crt_content` text NOT NULL,
  `crt_updby` int(11) NOT NULL DEFAULT '0',
  `crt_upddate` datetime DEFAULT NULL,
  PRIMARY KEY (`crt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2018-03-05 10:22:51
