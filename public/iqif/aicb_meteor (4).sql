-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `advance_payment`;
CREATE TABLE `advance_payment` (
  `advpy_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `advpy_appl_id` bigint(20) DEFAULT NULL,
  `advpy_trans_id` int(11) DEFAULT NULL,
  `advpy_payee_type` int(11) DEFAULT '0' COMMENT 'fk tbl_definationms',
  `advpy_corporate_id` int(11) DEFAULT '0',
  `advpy_idStudentRegistration` int(11) DEFAULT NULL,
  `advpy_acad_year_id` bigint(20) DEFAULT NULL,
  `advpy_sem_id` bigint(20) DEFAULT NULL,
  `advpy_prog_code` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advpy_fomulir` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_invoice_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advpy_invoice_id` int(11) DEFAULT NULL,
  `advpy_invoice_det_id` int(11) NOT NULL DEFAULT '0' COMMENT 'invoice_detail',
  `advpy_payment_id` bigint(20) DEFAULT NULL,
  `advpy_rcp_id` int(11) NOT NULL,
  `advpy_refund_id` bigint(20) DEFAULT NULL,
  `advpy_cur_id` int(11) NOT NULL,
  `advpy_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_amount` decimal(20,2) NOT NULL,
  `advpy_total_paid` decimal(20,2) NOT NULL,
  `advpy_total_balance` decimal(20,2) NOT NULL,
  `advpy_status` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A: active; X: inactive',
  `advpy_creator` bigint(20) NOT NULL,
  `advpy_date` date NOT NULL,
  `advpy_create_date` datetime NOT NULL,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `advpy_exchange_rate` int(11) NOT NULL,
  `advpy_approve_by` int(11) NOT NULL DEFAULT '0',
  `advpy_approve_date` datetime DEFAULT NULL,
  `advpy_cancel_by` int(11) NOT NULL,
  `advpy_cancel_date` datetime NOT NULL,
  `IdReceivableAdjustment` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_receivable_adjustment',
  PRIMARY KEY (`advpy_id`),
  KEY `advpy_appl_id` (`advpy_appl_id`),
  KEY `advpy_trans_id` (`advpy_trans_id`),
  KEY `advpy_idStudentRegistration` (`advpy_idStudentRegistration`),
  KEY `advpy_invoice_id` (`advpy_invoice_id`),
  KEY `advpy_cur_id` (`advpy_cur_id`),
  KEY `advpy_rcp_id` (`advpy_rcp_id`),
  KEY `advpy_fomulir` (`advpy_fomulir`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `advance_payment_detail`;
CREATE TABLE `advance_payment_detail` (
  `advpydet_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `advpydet_advpy_id` bigint(20) NOT NULL COMMENT 'fk advance_payment',
  `advpydet_inv_id` int(11) DEFAULT NULL,
  `advpydet_inv_det_id` int(11) NOT NULL DEFAULT '0',
  `advpydet_refund_cheque_id` bigint(20) DEFAULT NULL,
  `advpydet_refund_id` bigint(20) DEFAULT NULL,
  `advpydet_total_paid` decimal(20,2) NOT NULL,
  `advpydet_updby` int(11) NOT NULL DEFAULT '0',
  `advpydet_upddate` datetime DEFAULT NULL,
  `advpydet_exchange_rate` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`advpydet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='amout used from adv payment table';


DROP TABLE IF EXISTS `agent_form_number`;
CREATE TABLE `agent_form_number` (
  `afn_id` int(11) NOT NULL AUTO_INCREMENT,
  `afn_form_no` int(8) NOT NULL,
  `afn_taken_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: available; 1:taken',
  `afn_type` int(11) NOT NULL,
  `afn_academic_year` int(11) DEFAULT NULL,
  `afn_intake` int(11) NOT NULL DEFAULT '78',
  PRIMARY KEY (`afn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='tbl to hold manual form pes id';


DROP TABLE IF EXISTS `announcement`;
CREATE TABLE `announcement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci,
  `program_id` int(11) unsigned DEFAULT NULL,
  `intake_id` int(11) unsigned DEFAULT NULL,
  `semester_id` int(11) unsigned DEFAULT NULL,
  `views` bigint(11) unsigned NOT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `sticky` tinyint(1) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `html` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `program_id_intake_id_semester_id` (`program_id`,`intake_id`,`semester_id`),
  KEY `type` (`type`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `announcement_files`;
CREATE TABLE `announcement_files` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` int(11) unsigned NOT NULL,
  `filename` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileurl` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filesize` bigint(11) unsigned NOT NULL,
  `total_download` bigint(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `announcement_id` (`announcement_id`),
  CONSTRAINT `announcement_files_ibfk_1` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `announcement_read`;
CREATE TABLE `announcement_read` (
  `read_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` int(11) unsigned NOT NULL,
  `user_id` bigint(11) unsigned NOT NULL,
  `announcement_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`read_id`),
  KEY `user_id_announcement_type` (`user_id`,`announcement_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_accomodation`;
CREATE TABLE `applicant_accomodation` (
  `acd_id` int(11) NOT NULL AUTO_INCREMENT,
  `acd_trans_id` bigint(20) NOT NULL,
  `acd_assistance` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: no 1:yes',
  `acd_type` int(11) NOT NULL DEFAULT '0',
  `acd_updateby` int(11) NOT NULL,
  `acd_updatedt` datetime NOT NULL,
  PRIMARY KEY (`acd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_assessment`;
CREATE TABLE `applicant_assessment` (
  `aar_id` int(11) NOT NULL AUTO_INCREMENT,
  `aar_trans_id` int(11) NOT NULL,
  `aar_rating_dean` tinyint(1) DEFAULT NULL,
  `aar_dean_status` int(11) DEFAULT NULL,
  `aar_rating_rector` tinyint(11) DEFAULT NULL,
  `aar_rector_status` int(11) DEFAULT NULL,
  `aar_remarks` longtext COLLATE utf8mb4_unicode_ci,
  `aar_dean_selectionid` int(11) NOT NULL,
  `aar_rector_selectionid` int(11) NOT NULL,
  `aar_final_selectionid` int(11) NOT NULL,
  `aar_dean_rateby` int(11) DEFAULT NULL,
  `aar_dean_ratedt` datetime DEFAULT NULL,
  `aar_rector_rateby` int(11) DEFAULT NULL,
  `aar_rector_ratedt` datetime DEFAULT NULL,
  `aar_approvalby` int(11) DEFAULT NULL,
  `aar_approvaldt` datetime NOT NULL,
  `aar_academic_year` int(11) DEFAULT NULL,
  `aar_reg_start_date` date DEFAULT NULL,
  `aar_reg_end_date` date DEFAULT NULL,
  `aar_payment_start_date` date DEFAULT NULL,
  `aar_payment_end_date` date DEFAULT NULL,
  PRIMARY KEY (`aar_id`),
  UNIQUE KEY `aar_id` (`aar_id`),
  KEY `aar_trans_id` (`aar_trans_id`),
  KEY `aar_dean_selectionid` (`aar_dean_selectionid`),
  KEY `aar_rector_selectionid` (`aar_rector_selectionid`),
  KEY `aar_rector_selectionid_2` (`aar_rector_selectionid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_assessment_date`;
CREATE TABLE `applicant_assessment_date` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `lock_status` tinyint(4) NOT NULL COMMENT '0: not lock; 1: lock',
  `lock_by` int(11) NOT NULL COMMENT 'fk_tbl_user',
  `lock_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_assessment_usm`;
CREATE TABLE `applicant_assessment_usm` (
  `aau_id` int(11) NOT NULL AUTO_INCREMENT,
  `aau_trans_id` int(11) NOT NULL,
  `aau_ap_id` int(11) NOT NULL COMMENT 'fk applicant_program',
  `aau_createddt` datetime NOT NULL,
  `aau_createdby` int(11) NOT NULL,
  `aau_rector_ranking` int(11) DEFAULT NULL,
  `aau_rector_status` int(11) DEFAULT NULL,
  `aau_rector_createby` int(11) DEFAULT NULL,
  `aau_rector_createdt` datetime DEFAULT NULL,
  `aau_rector_selectionid` int(11) DEFAULT NULL COMMENT 'applicant_assessment_usm_detl',
  `aau_reversal_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:yes',
  PRIMARY KEY (`aau_id`),
  KEY `aau_trans_id` (`aau_trans_id`),
  KEY `aau_ap_id` (`aau_ap_id`),
  KEY `aau_rector_selectionid` (`aau_rector_selectionid`),
  KEY `aau_rector_status` (`aau_rector_status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_assessment_usm_detl`;
CREATE TABLE `applicant_assessment_usm_detl` (
  `aaud_id` int(11) NOT NULL AUTO_INCREMENT,
  `aaud_type` int(11) NOT NULL COMMENT '1: Dean 2:Rektor',
  `aaud_nomor` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aaud_decree_date` date DEFAULT NULL,
  `aaud_academic_year` int(11) DEFAULT NULL,
  `aaud_selection_period` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aaud_reg_start_date` date DEFAULT NULL,
  `aaud_reg_end_date` date DEFAULT NULL,
  `aaud_payment_start_date` date DEFAULT NULL,
  `aaud_payment_end_date` date DEFAULT NULL,
  `aaud_lock_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:locked  0:unlock',
  `aaud_lock_by` int(11) DEFAULT NULL,
  `aaud_lock_date` datetime DEFAULT NULL,
  `aaud_createddt` datetime DEFAULT NULL,
  `aaud_createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`aaud_id`),
  KEY `aaud_nomor` (`aaud_nomor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_bulk_history`;
CREATE TABLE `applicant_bulk_history` (
  `b_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `b_status` enum('NEW','IMPORTED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NEW',
  `b_filename` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b_fileurl` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b_totalapp` smallint(10) unsigned NOT NULL,
  `b_totaladded` smallint(10) unsigned NOT NULL DEFAULT '0',
  `b_created_by` int(10) unsigned NOT NULL,
  `b_created_date` datetime NOT NULL,
  PRIMARY KEY (`b_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_bulk_history_item`;
CREATE TABLE `applicant_bulk_history_item` (
  `bi_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `b_id` int(10) unsigned NOT NULL,
  `bi_trans_id` int(10) unsigned NOT NULL,
  `bi_appl_id` int(10) unsigned NOT NULL,
  `bi_bid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bi_id`),
  KEY `b_id` (`b_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_changescheme_attachment`;
CREATE TABLE `applicant_changescheme_attachment` (
  `aca_id` int(11) NOT NULL AUTO_INCREMENT,
  `aca_ach_id` int(11) NOT NULL DEFAULT '0',
  `aca_fileName` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aca_fileUrl` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aca_updDate` datetime NOT NULL,
  `aca_updUser` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aca_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_changescheme_history`;
CREATE TABLE `applicant_changescheme_history` (
  `ach_id` int(11) NOT NULL AUTO_INCREMENT,
  `ach_trans_id` int(11) NOT NULL DEFAULT '0',
  `ach_new_scheme_id` int(11) NOT NULL DEFAULT '0',
  `ach_old_scheme_id` int(11) NOT NULL DEFAULT '0',
  `ach_send_email` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: not send email  1:email send',
  `ach_updUser` int(11) NOT NULL DEFAULT '0',
  `ach_updDate` datetime NOT NULL,
  PRIMARY KEY (`ach_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_change_program`;
CREATE TABLE `applicant_change_program` (
  `acp_id` int(11) NOT NULL AUTO_INCREMENT,
  `acp_appl_id` int(11) NOT NULL,
  `acp_trans_id_from` int(11) NOT NULL,
  `acp_trans_id_to` int(11) NOT NULL,
  `acp_createddt` datetime NOT NULL,
  `acp_createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`acp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_declaration`;
CREATE TABLE `applicant_declaration` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `txn_id` int(11) NOT NULL,
  `declaration_status` tinyint(4) NOT NULL COMMENT '0: No 1:Yes',
  `declaration_date` date NOT NULL,
  PRIMARY KEY (`ad_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_documents`;
CREATE TABLE `applicant_documents` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_txn_id` int(11) NOT NULL COMMENT 'sebenarnye txn ID bukan appl_id',
  `ad_dcl_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_documentchecklist_dcl',
  `ad_ads_id` int(11) NOT NULL,
  `ad_section_id` int(11) DEFAULT '0',
  `ad_table_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'name of table that reflect ad_table_id',
  `ad_table_id` int(11) DEFAULT '0',
  `ad_filepath` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_filename` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_ori_filename` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ad_createddt` date NOT NULL,
  `ad_createdby` int(11) NOT NULL,
  `ad_role` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ad_id`),
  KEY `ad_txn_id` (`ad_txn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_document_checklisthistory`;
CREATE TABLE `applicant_document_checklisthistory` (
  `adc_id` int(11) NOT NULL AUTO_INCREMENT,
  `adc_adh_id` int(11) NOT NULL DEFAULT '0',
  `adc_dcl_id` int(11) NOT NULL DEFAULT '0',
  `adc_stdCtgy` int(11) NOT NULL DEFAULT '0',
  `adc_programScheme` int(11) NOT NULL DEFAULT '0',
  `adc_sectionid` int(11) NOT NULL DEFAULT '0',
  `adc_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adc_malayName` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adc_desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `adc_type` int(11) NOT NULL DEFAULT '0',
  `adc_activeStatus` int(11) NOT NULL DEFAULT '0',
  `adc_priority` int(11) NOT NULL DEFAULT '0',
  `adc_uplStatus` int(11) NOT NULL DEFAULT '0',
  `adc_uplType` int(11) NOT NULL DEFAULT '0',
  `adc_finance` tinyint(2) NOT NULL DEFAULT '0',
  `adc_mandatory` int(11) NOT NULL DEFAULT '0',
  `adc_specific` int(11) NOT NULL DEFAULT '0',
  `adc_upddate` date NOT NULL,
  `adc_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_document_history`;
CREATE TABLE `applicant_document_history` (
  `adh_id` int(11) NOT NULL AUTO_INCREMENT,
  `adh_txn_id` int(11) NOT NULL DEFAULT '0',
  `adh_type` int(11) NOT NULL DEFAULT '0',
  `adh_oldprogramscheme` int(11) NOT NULL DEFAULT '0',
  `adh_newprogramscheme` int(11) NOT NULL DEFAULT '0',
  `adh_oldstudentcat` int(11) NOT NULL DEFAULT '0',
  `adh_newstudentcat` int(11) NOT NULL DEFAULT '0',
  `adh_upddate` date DEFAULT NULL,
  `adh_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_document_status`;
CREATE TABLE `applicant_document_status` (
  `ads_id` int(11) NOT NULL AUTO_INCREMENT,
  `ads_txn_id` int(11) NOT NULL DEFAULT '0',
  `ads_dcl_id` int(11) NOT NULL DEFAULT '0',
  `ads_ad_id` int(11) DEFAULT NULL COMMENT 'fk applicant_documents',
  `ads_appl_id` int(11) NOT NULL DEFAULT '0',
  `ads_section_id` int(11) NOT NULL DEFAULT '0',
  `ads_table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ads_table_id` int(11) NOT NULL DEFAULT '0',
  `ads_confirm` tinyint(11) NOT NULL DEFAULT '0' COMMENT '1: Confirm (can proceed processing data)',
  `ads_status` tinyint(11) NOT NULL DEFAULT '0' COMMENT '1: New   2: Viewed   3: Incomplete   4: Complete',
  `ads_comment` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ads_createBy` int(11) NOT NULL DEFAULT '0',
  `ads_createDate` datetime NOT NULL,
  `ads_modBy` int(11) NOT NULL DEFAULT '0',
  `ads_modDate` datetime NOT NULL,
  PRIMARY KEY (`ads_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_document_statushistory`;
CREATE TABLE `applicant_document_statushistory` (
  `adt_id` int(11) NOT NULL AUTO_INCREMENT,
  `adt_adh_id` int(11) NOT NULL DEFAULT '0',
  `adt_adc_id` int(11) NOT NULL DEFAULT '0',
  `adt_txn_id` int(11) NOT NULL DEFAULT '0',
  `adt_dcl_id` int(11) NOT NULL DEFAULT '0',
  `adt_ad_id` int(11) NOT NULL DEFAULT '0',
  `adt_appl_id` int(11) NOT NULL DEFAULT '0',
  `adt_section_id` int(11) NOT NULL DEFAULT '0',
  `adt_table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adt_table_id` int(11) NOT NULL DEFAULT '0',
  `adt_confirm` tinyint(2) NOT NULL DEFAULT '0',
  `adt_status` int(2) NOT NULL DEFAULT '0',
  `adt_comment` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adt_upddate` date DEFAULT NULL,
  `adt_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_document_uploadhistory`;
CREATE TABLE `applicant_document_uploadhistory` (
  `adu_id` int(11) NOT NULL AUTO_INCREMENT,
  `adu_adh_id` int(11) NOT NULL DEFAULT '0',
  `adu_adc_id` int(11) NOT NULL DEFAULT '0',
  `adu_txn_id` int(11) NOT NULL DEFAULT '0',
  `adu_dcl_id` int(11) NOT NULL DEFAULT '0',
  `adu_ads_id` int(11) NOT NULL DEFAULT '0',
  `adu_appl_id` int(11) NOT NULL DEFAULT '0',
  `adu_section_id` int(11) NOT NULL DEFAULT '0',
  `adu_table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adu_table_id` int(11) NOT NULL DEFAULT '0',
  `adu_filepath` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adu_filename` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adu_ori_filename` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adu_upddate` date DEFAULT NULL,
  `adu_updby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_education`;
CREATE TABLE `applicant_education` (
  `ae_id` int(11) NOT NULL AUTO_INCREMENT,
  `ae_appl_id` bigint(20) NOT NULL,
  `ae_transaction_id` int(11) NOT NULL,
  `ae_institution` int(11) NOT NULL COMMENT 'fk.school_master',
  `ae_discipline_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk school_majoring(smj_code)',
  `ae_year_from` date NOT NULL,
  `ae_year_end` date NOT NULL,
  `ae_year` tinyint(4) DEFAULT NULL,
  `ae_award` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ae_id`),
  KEY `ae_transaction_id` (`ae_transaction_id`),
  KEY `ae_appl_id` (`ae_appl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_education_detl`;
CREATE TABLE `applicant_education_detl` (
  `aed_id` int(11) NOT NULL AUTO_INCREMENT,
  `aed_ae_id` int(11) NOT NULL COMMENT 'fk applicant_education(ae_id)',
  `aed_subject_id` int(11) NOT NULL COMMENT 'fk chool_majoring_subject(sms_id)',
  `aed_sem1` int(3) DEFAULT NULL,
  `aed_sem2` int(3) DEFAULT NULL,
  `aed_sem3` int(3) DEFAULT NULL,
  `aed_sem4` int(3) DEFAULT NULL,
  `aed_sem5` int(3) DEFAULT NULL,
  `aed_sem6` int(3) DEFAULT NULL,
  `aed_grade` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aed_average` decimal(10,1) DEFAULT NULL,
  PRIMARY KEY (`aed_id`),
  KEY `applicant_education_detl_fk1` (`aed_ae_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_eligible`;
CREATE TABLE `applicant_eligible` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program` int(11) NOT NULL COMMENT 'fk tbl_program',
  `qualification` int(11) NOT NULL COMMENT 'fk tbl_qualificationmaster',
  `appl_id` int(11) DEFAULT NULL,
  `appl_trans_id` int(11) DEFAULT NULL COMMENT 'xperlu',
  `status` int(11) NOT NULL COMMENT '1=eligible 0 = no',
  `age` int(11) DEFAULT NULL,
  `work_exp` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_employment`;
CREATE TABLE `applicant_employment` (
  `ae_id` int(11) NOT NULL AUTO_INCREMENT,
  `ae_appl_id` int(11) NOT NULL,
  `ae_trans_id` int(11) NOT NULL,
  `ae_status` int(11) NOT NULL COMMENT 'Employment Status',
  `ae_comp_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_comp_address` longtext COLLATE utf8mb4_unicode_ci,
  `ae_comp_phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_comp_fax` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_position` int(11) DEFAULT NULL COMMENT 'position level',
  `ae_from` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_to` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emply_year_service` int(11) DEFAULT NULL,
  `ae_industry` int(11) DEFAULT NULL COMMENT 'Industry Working Experience',
  `ae_job_desc` longtext COLLATE utf8mb4_unicode_ci,
  `ae_income` float DEFAULT NULL,
  `ae_industry_type` int(11) DEFAULT NULL,
  `ae_is_current` int(11) DEFAULT NULL,
  `ae_order` int(11) DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ae_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_english_proficiency`;
CREATE TABLE `applicant_english_proficiency` (
  `ep_id` int(11) NOT NULL AUTO_INCREMENT,
  `ep_transaction_id` int(11) NOT NULL,
  `ep_test` int(11) DEFAULT NULL,
  `ep_test_detail` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ep_date_taken` date DEFAULT NULL,
  `ep_score` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ep_updatedt` datetime NOT NULL,
  `ep_updateby` int(11) NOT NULL,
  PRIMARY KEY (`ep_id`),
  UNIQUE KEY `ep_id` (`ep_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_essay`;
CREATE TABLE `applicant_essay` (
  `e_id` int(11) NOT NULL AUTO_INCREMENT,
  `e_trans_id` int(10) unsigned NOT NULL,
  `e_file` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_updatedt` datetime NOT NULL,
  `e_updateby` int(11) NOT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_family`;
CREATE TABLE `applicant_family` (
  `af_id` int(11) NOT NULL AUTO_INCREMENT,
  `af_appl_id` int(11) NOT NULL,
  `af_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `af_relation_type` int(11) NOT NULL COMMENT 'fk sis_setup_detl(ssd_id)',
  `af_family_condition` int(11) DEFAULT NULL COMMENT 'fk sis_setup_detl',
  `af_address_rt` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_address_rw` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_address1` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `af_address2` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_kelurahan` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_kecamatan` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_city` int(11) NOT NULL,
  `af_state` bigint(20) unsigned NOT NULL,
  `af_province` int(11) DEFAULT NULL,
  `af_postcode` bigint(5) NOT NULL,
  `af_phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `af_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `af_job` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `af_education_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`af_id`),
  KEY `applicant_family_fk_1` (`af_state`),
  KEY `applicant_family_fk_2` (`af_city`),
  KEY `applicant_family_fk_3` (`af_relation_type`),
  KEY `af_appl_id` (`af_appl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_financial`;
CREATE TABLE `applicant_financial` (
  `af_id` int(11) NOT NULL AUTO_INCREMENT,
  `af_appl_id` int(11) NOT NULL,
  `af_trans_id` int(11) NOT NULL,
  `af_method` int(11) NOT NULL,
  `af_sponsor_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_type_scholarship` int(11) DEFAULT NULL,
  `af_scholarship_secured` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `af_scholarship_apply` int(11) DEFAULT NULL,
  `af_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upd_date` int(11) NOT NULL,
  PRIMARY KEY (`af_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_generated_document`;
CREATE TABLE `applicant_generated_document` (
  `tgd_id` int(11) NOT NULL AUTO_INCREMENT,
  `tgd_txn_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk applicant_transaction',
  `tgd_appl_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk to applicant_profile',
  `sth_id` int(11) NOT NULL COMMENT 'fk tbl_statusattachment_sth',
  `tgd_filename` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgd_fileurl` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgd_userType` int(11) NOT NULL DEFAULT '0' COMMENT '0: applicant 1:admin',
  `tgd_updUser` int(11) NOT NULL DEFAULT '0',
  `tgd_updDate` datetime NOT NULL,
  PRIMARY KEY (`tgd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_health_condition`;
CREATE TABLE `applicant_health_condition` (
  `ah_id` int(11) NOT NULL AUTO_INCREMENT,
  `ah_appl_id` int(11) NOT NULL,
  `ah_trans_id` int(11) NOT NULL,
  `ah_status` int(11) NOT NULL,
  `upd_date` datetime NOT NULL,
  `ah_others` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ah_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_id_seqno`;
CREATE TABLE `applicant_id_seqno` (
  `seq_id` int(11) NOT NULL AUTO_INCREMENT,
  `seq_no` int(11) NOT NULL,
  `seq_year` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seq_createdby` int(11) NOT NULL,
  `seq_createddt` datetime NOT NULL,
  PRIMARY KEY (`seq_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_language`;
CREATE TABLE `applicant_language` (
  `al_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `al_language` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`al_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_mark_raw`;
CREATE TABLE `applicant_mark_raw` (
  `tmp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tmp_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_ptest_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_set_code` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_answerraw` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_sched_id` int(11) NOT NULL,
  `tmp_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`tmp_id`),
  KEY `tmp_sched_id` (`tmp_sched_id`),
  KEY `tmp_set_code` (`tmp_set_code`),
  KEY `tmp_file` (`tmp_file`(191)),
  KEY `tmp_id` (`tmp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_profile`;
CREATE TABLE `applicant_profile` (
  `appl_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl_salutation` int(11) DEFAULT NULL,
  `appl_fname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_mname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_lname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_idnumber` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_idnumber_type` int(11) DEFAULT NULL,
  `appl_passport_expiry` date DEFAULT NULL,
  `appl_address1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_address2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_address3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_postcode` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_city` int(10) DEFAULT NULL,
  `appl_state` int(11) DEFAULT NULL,
  `appl_city_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_state_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_country` int(11) DEFAULT NULL COMMENT 'Country',
  `appl_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_username` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'later nanti nak pakai just create aje',
  `appl_password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_dob` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'dd-mm-yyyy',
  `appl_gender` tinyint(1) DEFAULT NULL COMMENT '1:  male 2: female',
  `appl_prefer_lang` int(11) NOT NULL DEFAULT '2' COMMENT 'fk applicant_languange(al_id)',
  `appl_nationality` int(11) DEFAULT NULL,
  `appl_type_nationality` int(11) DEFAULT NULL COMMENT 'tbl maintenance',
  `appl_category` int(11) NOT NULL COMMENT 'fk tbl_definations (type:)',
  `appl_admission_type` int(11) DEFAULT NULL COMMENT 'jgn pakai ni lagi guna at_appl_type dkt transaction',
  `appl_religion` int(11) DEFAULT NULL,
  `appl_religion_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_race` int(11) DEFAULT NULL,
  `appl_race_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_marital_status` int(11) DEFAULT NULL,
  `appl_bumiputera` int(11) DEFAULT NULL,
  `appl_takafuloperator` int(11) DEFAULT NULL,
  `appl_no_of_child` int(11) DEFAULT NULL,
  `appl_phone_home` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_phone_mobile` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_phone_office` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_caddress1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'correspondence add',
  `appl_caddress2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_caddress3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_ccity` int(10) DEFAULT NULL,
  `appl_cstate` int(11) DEFAULT NULL,
  `appl_ccountry` int(11) DEFAULT NULL,
  `appl_ccity_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cstate_others` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cpostcode` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cphone_home` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cphone_mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cphone_office` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_cfax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_contact_home` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_contact_mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_contact_office` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `change_passwordby` int(11) DEFAULT NULL,
  `change_passworddt` datetime DEFAULT NULL,
  `appl_role` int(11) DEFAULT NULL COMMENT '0:Applicant  1:Student',
  `fromOldSys` text COLLATE utf8mb4_unicode_ci,
  `verifyKey` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'untuk tujuan verify email',
  `create_date` datetime DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `prev_student` tinyint(4) DEFAULT '0' COMMENT '0: No 1:yes',
  `prev_txn_id` bigint(20) DEFAULT NULL COMMENT 'student_profile txn id',
  `prev_studentID` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'refer to registrationid at tbl_studentregistration',
  `branch_id` int(11) DEFAULT NULL,
  `appl_company` int(11) DEFAULT NULL,
  `appl_designation` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appl_department` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_default_qlevel` int(11) DEFAULT NULL COMMENT 'highest qualification',
  `appl_category_type` int(11) DEFAULT NULL COMMENT 'tbl_category',
  `appl_company_id` int(11) DEFAULT NULL COMMENT 'tbl_takafuloperator',
  `appl_membership` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userKey` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MigrateCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migrate_email` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`appl_id`),
  KEY `applicant_profile_fk1` (`appl_prefer_lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_proforma_invoice`;
CREATE TABLE `applicant_proforma_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `billing_no` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payee_id` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appl_id` int(11) NOT NULL COMMENT 'fk_applicant_profile',
  `txn_id` int(11) NOT NULL COMMENT 'fk_applicant_transaction',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'faculty_code-shortname',
  `ref2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'blank',
  `ref3` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'program_code-shortname',
  `ref4` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'intake year',
  `ref5` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_total` decimal(20,2) NOT NULL,
  `amount1` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'SP',
  `amount2` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'BPP-POKOK',
  `amount3` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'BPP-SKS',
  `amount4` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'PRAKTIKUM',
  `amount5` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount6` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount7` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount8` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount9` decimal(20,2) NOT NULL DEFAULT '0.00',
  `amount10` decimal(20,2) NOT NULL DEFAULT '0.00',
  `register_no` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'random generated no.',
  `due_date` date DEFAULT NULL,
  `offer_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `billing_no` (`billing_no`),
  KEY `payee_id` (`payee_id`),
  KEY `txn_id` (`txn_id`),
  KEY `appl_id` (`appl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_program`;
CREATE TABLE `applicant_program` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `ap_at_trans_id` bigint(20) NOT NULL COMMENT 'fk application_transaction(at_trans_id)',
  `ap_prog_id` bigint(20) NOT NULL,
  `ap_prog_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk tbl_program',
  `ap_ptest_prog_id` int(11) DEFAULT NULL COMMENT 'aps_app_id fk to appl_placement_program(app_id)',
  `ap_preference` tinyint(2) NOT NULL DEFAULT '1',
  `mode_study` int(11) NOT NULL,
  `program_mode` int(11) NOT NULL,
  `program_type` int(11) NOT NULL,
  `ap_prog_scheme` int(11) NOT NULL DEFAULT '0',
  `upd_date` datetime NOT NULL,
  UNIQUE KEY `app_id` (`ap_id`),
  KEY `ap_at_trans_id` (`ap_at_trans_id`),
  KEY `ap_prog_code` (`ap_prog_code`),
  KEY `ap_ptest_prog_id` (`ap_ptest_prog_id`),
  KEY `ap_preference` (`ap_preference`),
  KEY `ap_id` (`ap_id`),
  KEY `ap_at_trans_id_2` (`ap_at_trans_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_qualification`;
CREATE TABLE `applicant_qualification` (
  `ae_id` int(11) NOT NULL AUTO_INCREMENT,
  `ae_appl_id` bigint(20) NOT NULL,
  `ae_transaction_id` int(11) NOT NULL,
  `ae_qualification` int(11) DEFAULT NULL COMMENT 'PK tbl_qualificationmaster',
  `ae_degree_awarded` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_majoring` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_class_degree` int(11) DEFAULT NULL,
  `ae_result` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_year_graduate` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_institution_country` int(11) DEFAULT NULL COMMENT 'fk tbl_countries',
  `ae_institution` int(11) DEFAULT NULL COMMENT 'fk.school_master',
  `ae_medium_instruction` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ae_order` int(11) NOT NULL,
  `others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`ae_id`),
  KEY `ae_transaction_id` (`ae_transaction_id`),
  KEY `ae_appl_id` (`ae_appl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_quantitative_proficiency`;
CREATE TABLE `applicant_quantitative_proficiency` (
  `qp_id` int(11) NOT NULL AUTO_INCREMENT,
  `qp_trans_id` bigint(20) NOT NULL,
  `qp_level` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qp_grade_mark` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qp_institution` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qp_createdby` int(11) NOT NULL,
  `qp_createddt` datetime NOT NULL,
  PRIMARY KEY (`qp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_quit`;
CREATE TABLE `applicant_quit` (
  `aq_id` int(11) NOT NULL AUTO_INCREMENT,
  `aq_trans_id` int(11) DEFAULT NULL,
  `aq_reason` int(11) DEFAULT NULL,
  `aq_authorised_personnel` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aq_relationship` int(11) DEFAULT NULL,
  `aq_address` mediumtext COLLATE utf8mb4_unicode_ci,
  `aq_identity_type` int(11) DEFAULT NULL,
  `aq_identity_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aq_createddt` datetime DEFAULT NULL,
  `aq_approvaldt` date DEFAULT NULL COMMENT 'for status approved/reject',
  `aq_approvalby` int(11) DEFAULT NULL COMMENT 'for status approved/reject',
  `aq_processdt` datetime DEFAULT NULL COMMENT 'ini system date bila admin process appliation ',
  `aq_processby` int(11) DEFAULT NULL,
  `aq_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `aq_cheque_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`aq_id`),
  KEY `aq_trans_id` (`aq_trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_quit_revert`;
CREATE TABLE `applicant_quit_revert` (
  `aqr_id` int(11) NOT NULL AUTO_INCREMENT,
  `aq_id` int(11) NOT NULL,
  `aq_trans_id` int(11) DEFAULT NULL,
  `aq_reason` int(11) DEFAULT NULL,
  `aq_authorised_personnel` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aq_relationship` int(11) DEFAULT NULL,
  `aq_address` mediumtext COLLATE utf8mb4_unicode_ci,
  `aq_identity_type` int(11) DEFAULT NULL,
  `aq_identity_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aq_createddt` datetime DEFAULT NULL,
  `aq_approvaldt` date DEFAULT NULL COMMENT 'for status approved/reject',
  `aq_approvalby` int(11) DEFAULT NULL COMMENT 'for status approved/reject',
  `aq_processdt` datetime DEFAULT NULL COMMENT 'ini system date bila admin process appliation ',
  `aq_processby` int(11) DEFAULT NULL,
  `aq_remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `aq_cheque_id` int(11) DEFAULT NULL,
  `aqr_remark` longtext COLLATE utf8mb4_unicode_ci,
  `aqr_by` bigint(20) NOT NULL,
  `aqr_date` datetime NOT NULL,
  PRIMARY KEY (`aqr_id`),
  KEY `aq_trans_id` (`aq_trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_referees`;
CREATE TABLE `applicant_referees` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `r_txn_id` bigint(20) NOT NULL,
  `r_ref1_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_position` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_add1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_add2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_add3` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_postcode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_city` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_city_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_country` int(11) DEFAULT NULL,
  `r_ref1_state` int(11) DEFAULT NULL,
  `r_ref1_state_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref1_email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_position` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_add1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_add2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_add3` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_postcode` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_city` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_city_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_country` int(11) DEFAULT NULL,
  `r_ref2_state` int(11) DEFAULT NULL,
  `r_ref2_state_others` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_ref2_email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_updateby` int(11) NOT NULL,
  `r_updatedt` datetime NOT NULL,
  PRIMARY KEY (`r_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_research_publication`;
CREATE TABLE `applicant_research_publication` (
  `rpd_id` int(11) NOT NULL AUTO_INCREMENT,
  `rpd_trans_id` bigint(20) NOT NULL,
  `rpd_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rpd_date` date DEFAULT NULL,
  `rpd_publisher` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rpd_createddt` datetime NOT NULL,
  `rpd_createdby` int(11) NOT NULL,
  PRIMARY KEY (`rpd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_reversal_track`;
CREATE TABLE `applicant_reversal_track` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_appl_type` int(11) NOT NULL,
  `art_trans_id` int(11) NOT NULL,
  `art_createdby` int(11) NOT NULL,
  `art_createddt` datetime NOT NULL,
  PRIMARY KEY (`art_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_section_status`;
CREATE TABLE `applicant_section_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appl_id` int(11) NOT NULL,
  `appl_trans_id` int(11) NOT NULL,
  `idSection` int(11) NOT NULL COMMENT 'PK tbl_application_section',
  `status` int(11) NOT NULL,
  `retrieve_status` tinyint(4) DEFAULT '0' COMMENT '0: Default  1:Not Retrieve 2:Retrieve',
  `upd_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_selection_detl`;
CREATE TABLE `applicant_selection_detl` (
  `asd_id` int(11) NOT NULL AUTO_INCREMENT,
  `asd_type` int(11) NOT NULL COMMENT '1: Dean 2: Rector 3:Final Approval',
  `asd_nomor` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asd_decree_date` date NOT NULL,
  `asd_faculty_id` int(11) NOT NULL,
  `asd_selection_period` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asd_intake_id` int(11) NOT NULL,
  `asd_academic_year` int(11) NOT NULL COMMENT 'fk_tbl_academic_year (ay_id)',
  `asd_period_id` int(11) NOT NULL,
  `asd_createddt` datetime NOT NULL,
  `asd_createdby` int(11) NOT NULL,
  PRIMARY KEY (`asd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_status_history`;
CREATE TABLE `applicant_status_history` (
  `ash_id` int(11) NOT NULL AUTO_INCREMENT,
  `ash_trans_id` int(11) NOT NULL DEFAULT '0',
  `ash_status` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ash_oldStatus` int(11) NOT NULL,
  `ash_changeType` int(11) NOT NULL COMMENT '1 : change status 2:change scheme',
  `ash_userType` int(11) NOT NULL DEFAULT '0' COMMENT '0:applicant 1:student',
  `ash_updDate` datetime NOT NULL,
  `ash_updUser` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ash_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_temp_usm_selection`;
CREATE TABLE `applicant_temp_usm_selection` (
  `ats_id` int(11) NOT NULL AUTO_INCREMENT,
  `ats_transaction_id` int(11) NOT NULL,
  `ats_ap_id` int(11) NOT NULL COMMENT 'fk applicant_program ',
  `ats_program_code` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ats_preference` tinyint(11) NOT NULL,
  PRIMARY KEY (`ats_id`),
  KEY `ats_transaction_id` (`ats_transaction_id`),
  KEY `ats_ap_id` (`ats_ap_id`),
  KEY `ats_program_code` (`ats_program_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_transaction`;
CREATE TABLE `applicant_transaction` (
  `at_trans_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `at_appl_id` int(11) NOT NULL COMMENT 'fk applicant_profile(ap_id)',
  `at_pes_id` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_appl_type` int(11) NOT NULL COMMENT '1 - placementtest 2 -HS',
  `at_academic_year` int(11) DEFAULT '3' COMMENT 'fk academic_year(ay_id)',
  `at_intake` int(11) DEFAULT NULL,
  `at_intake_prev` int(11) DEFAULT NULL,
  `at_period` int(11) DEFAULT NULL,
  `at_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'DRAFT,ENTRY,OFFERED,REJECTED,COMPLETE,INCOMPLETE,SHORTLISTED,KIV,ACCEPTED,DECLINE,ENROLLED.ARCHIEVE',
  `at_selection_status` int(11) DEFAULT '0' COMMENT '0:Waiting for dean rating 1: Waiting for rector verification 2: Waiting for approval 3:cpmleted 4: In Pool (USM)',
  `at_create_by` int(11) NOT NULL,
  `at_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `at_submit_date` datetime DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `entry_type` int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  `at_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:not paid 1:paid tobe removed x pakai dah; 2: biasiswa',
  `at_quit_status` int(4) NOT NULL DEFAULT '0' COMMENT '1: Apply Quit 2:Approved Quit 3:Reject 4:Incomplete ',
  `at_move_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk applicant_change_program',
  `at_document_verified` int(11) NOT NULL DEFAULT '0' COMMENT '0:No 1:yes',
  `at_document_verifiedby` int(11) DEFAULT NULL,
  `at_document_verifieddt` datetime DEFAULT NULL,
  `at_registration_status` int(11) DEFAULT NULL COMMENT '1: Active',
  `at_IdStudentRegistration` int(11) NOT NULL DEFAULT '0' COMMENT 'fk tbl_studentregistration',
  `at_registration_date` datetime DEFAULT NULL,
  `upd_date` datetime DEFAULT NULL,
  `at_copy_status` tinyint(4) DEFAULT '0' COMMENT '0: Default 1:Copy2:Not Copy',
  `at_bookmark` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_repository` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_fs_id` int(11) DEFAULT NULL,
  `at_dp_id` int(11) unsigned DEFAULT NULL,
  `at_dp_by` int(11) unsigned DEFAULT NULL,
  `at_dp_date` datetime DEFAULT NULL,
  `at_fs_date` datetime DEFAULT NULL,
  `at_fs_by` int(11) DEFAULT NULL,
  `at_processing_fee` enum('ISSUED','PAID') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_tution_fee` enum('ISSUED','PAID') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_offer_acceptance_date` datetime DEFAULT NULL,
  `at_default_qlevel` int(11) DEFAULT NULL,
  `at_reason` int(11) DEFAULT NULL,
  `IdLandscape` bigint(20) DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '1',
  `at_remarks` longtext COLLATE utf8mb4_unicode_ci,
  `at_ref_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`at_trans_id`),
  UNIQUE KEY `at_pes_id` (`at_pes_id`),
  KEY `at_appl_id` (`at_appl_id`),
  KEY `at_appl_type` (`at_appl_type`),
  KEY `at_period` (`at_period`),
  KEY `at_status` (`at_status`),
  KEY `at_intake` (`at_intake`),
  KEY `entry_type` (`entry_type`),
  KEY `at_selection_status` (`at_selection_status`),
  KEY `at_document_verified` (`at_document_verified`),
  KEY `at_appl_id_2` (`at_appl_id`),
  KEY `at_appl_id_3` (`at_appl_id`),
  KEY `at_status_2` (`at_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_transaction_rosak`;
CREATE TABLE `applicant_transaction_rosak` (
  `at_trans_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `at_appl_id` int(11) NOT NULL,
  `at_appl_type` int(11) NOT NULL,
  `at_status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'DRAFT,ENTRY,OFFERED,REJECTED,COMPLETE,INCOMPLETE,SHORTLISTED,KIV,ACCEPTED,DECLINE,ENROLLED.ARCHIEVE',
  `at_create_by` int(11) NOT NULL,
  `at_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `at_submit_date` datetime NOT NULL,
  `entry_type` int(11) NOT NULL COMMENT '1:online 2: manual (for agent only)',
  PRIMARY KEY (`at_trans_id`),
  KEY `at_appl_id` (`at_appl_id`),
  KEY `at_appl_type` (`at_appl_type`),
  KEY `at_status` (`at_status`),
  KEY `entry_type` (`entry_type`),
  KEY `at_appl_id_2` (`at_appl_id`),
  KEY `at_appl_id_3` (`at_appl_id`),
  KEY `at_status_2` (`at_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_visa`;
CREATE TABLE `applicant_visa` (
  `av_id` int(11) NOT NULL AUTO_INCREMENT,
  `av_appl_id` int(11) NOT NULL,
  `av_trans_id` int(11) NOT NULL,
  `av_malaysian_visa` int(11) NOT NULL COMMENT '1=yes, 0=no',
  `av_status` int(11) DEFAULT NULL,
  `av_expiry` date DEFAULT NULL,
  `upd_date` datetime NOT NULL,
  PRIMARY KEY (`av_id`),
  UNIQUE KEY `av_appl_id` (`av_appl_id`,`av_trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `applicant_working_experience`;
CREATE TABLE `applicant_working_experience` (
  `aw_id` int(11) NOT NULL AUTO_INCREMENT,
  `aw_appl_id` int(11) NOT NULL,
  `aw_trans_id` int(11) NOT NULL,
  `aw_industry_id` int(11) NOT NULL COMMENT 'Industry Working Experience',
  `aw_years` int(11) NOT NULL COMMENT 'Years of industry',
  `upd_date` datetime NOT NULL,
  `aw_others` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`aw_id`),
  UNIQUE KEY `aw_id` (`aw_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_ansscheme`;
CREATE TABLE `appl_ansscheme` (
  `aas_id` int(11) NOT NULL AUTO_INCREMENT,
  `aas_ptest_code` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aas_set_code` int(11) NOT NULL,
  `aas_total_quest` int(11) NOT NULL,
  `aas_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active 0:inactive',
  PRIMARY KEY (`aas_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_ansscheme_detl`;
CREATE TABLE `appl_ansscheme_detl` (
  `aad_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `aad_anscheme_id` int(11) NOT NULL,
  `aad_ques_no` int(5) NOT NULL,
  `aad_ques_ans` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`aad_anscheme_id`,`aad_ques_no`),
  UNIQUE KEY `aad_id` (`aad_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_family_job`;
CREATE TABLE `appl_family_job` (
  `afj_id` int(11) NOT NULL AUTO_INCREMENT,
  `afj_title` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `afj_description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`afj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_location`;
CREATE TABLE `appl_location` (
  `al_id` int(11) NOT NULL AUTO_INCREMENT,
  `al_location_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `al_location_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `al_address1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `al_address2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `al_city` bigint(20) DEFAULT NULL,
  `al_state` bigint(20) DEFAULT NULL,
  `al_country` bigint(20) DEFAULT NULL,
  `al_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `al_contact_person` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `al_status` tinyint(1) NOT NULL COMMENT '1=Active 0= Inactive',
  `al_update_by` bigint(20) NOT NULL,
  `al_update_date` datetime NOT NULL,
  PRIMARY KEY (`al_location_code`),
  UNIQUE KEY `al_id` (`al_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_pin_to_bank`;
CREATE TABLE `appl_pin_to_bank` (
  `billing_no` int(8) DEFAULT NULL,
  `PAYEE_ID` int(8) NOT NULL DEFAULT '0',
  `ADDRESS_1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'Universitas Trisakti',
  `BILL_REF_1` varchar(17) COLLATE utf8mb4_unicode_ci DEFAULT 'Biaya Pendaftaran',
  `BILL_REF_2` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'USM online',
  `BILL_REF_3` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT '2013/2014',
  `AMOUNT_TOTAL` int(6) DEFAULT '250000',
  `REGISTER_NO` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bank` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'BNI',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'E' COMMENT 'E -Entry - P -PROCESS',
  `intakeId` int(11) NOT NULL DEFAULT '78',
  PRIMARY KEY (`PAYEE_ID`,`REGISTER_NO`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_fee_setup`;
CREATE TABLE `appl_placement_fee_setup` (
  `apfs_id` int(11) NOT NULL AUTO_INCREMENT,
  `apfs_fee_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk appl_placement_fee_type(apft_fee_code)',
  `apfs_value` int(11) NOT NULL COMMENT 'Fee Type Prog - number of prog, fee location value location id',
  `apfs_amt` decimal(10,2) NOT NULL,
  `apfs_currency` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apfs_start_date` date NOT NULL,
  `apfs_end_date` date DEFAULT NULL,
  `apfs_create_by` int(11) NOT NULL,
  `apfs_create_date` datetime NOT NULL,
  PRIMARY KEY (`apfs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_fee_type`;
CREATE TABLE `appl_placement_fee_type` (
  `apft_fee_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apft_fee_desc` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`apft_fee_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_head`;
CREATE TABLE `appl_placement_head` (
  `aph_id` int(11) NOT NULL AUTO_INCREMENT,
  `aph_placement_code` varchar(10) CHARACTER SET utf8 NOT NULL,
  `aph_placement_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `aph_academic_year` int(11) NOT NULL COMMENT 'PK tbl_intake',
  `aph_batch` int(11) DEFAULT NULL,
  `aph_fees_program` tinyint(1) NOT NULL COMMENT '1=''Yes'', 0 =No',
  `aph_fees_location` tinyint(1) NOT NULL COMMENT '1=''Yes'', 0 =No',
  `aph_start_date` date NOT NULL,
  `aph_end_date` date NOT NULL,
  `aph_effective_date` date NOT NULL,
  `aph_create_by` int(10) NOT NULL,
  `aph_create_date` datetime NOT NULL,
  `aph_wizard` tinyint(4) NOT NULL DEFAULT '1',
  `aph_appl_running_no` int(11) NOT NULL DEFAULT '0',
  `aph_testtype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aph_placement_code`),
  UNIQUE KEY `aph_id` (`aph_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_program`;
CREATE TABLE `appl_placement_program` (
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_placement_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk appl_placement_head(apd_placement_code)',
  `app_program_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_pass_mark` int(3) NOT NULL,
  `app_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`app_placement_code`,`app_program_code`),
  UNIQUE KEY `app_id` (`app_id`),
  KEY ` appl_placement_program_fk_2` (`app_program_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_program_setup`;
CREATE TABLE `appl_placement_program_setup` (
  `apps_id` int(11) NOT NULL AUTO_INCREMENT,
  `apps_program_id` bigint(20) unsigned NOT NULL COMMENT 'fk tbl_program(idProgram)',
  `apps_comp_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apps_create_by` int(11) NOT NULL COMMENT 'fk tbl_user',
  `apps_create_date` datetime NOT NULL,
  `aph_type` smallint(6) NOT NULL DEFAULT '0' COMMENT '0: USM 1:Psychology',
  PRIMARY KEY (`apps_program_id`,`apps_comp_code`),
  UNIQUE KEY `apps_id` (`apps_id`),
  KEY `appl_placement_program_setup_ibfk_2` (`apps_comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_schedule`;
CREATE TABLE `appl_placement_schedule` (
  `aps_id` int(11) NOT NULL AUTO_INCREMENT,
  `aps_placement_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk to appl_placement_head(aph_placement_code)',
  `aps_location_id` int(11) NOT NULL COMMENT 'fk appl_location',
  `aps_test_date` date NOT NULL,
  `aps_start_time` time NOT NULL,
  PRIMARY KEY (`aps_id`),
  KEY `appl_placement_schedule_ibfk_1` (`aps_placement_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_schedule_seatno`;
CREATE TABLE `appl_placement_schedule_seatno` (
  `apss_id` int(11) NOT NULL AUTO_INCREMENT,
  `apss_aps_id` int(11) NOT NULL,
  `apss_room_id` int(11) NOT NULL,
  `apss_exam_capasity` int(11) NOT NULL,
  `apss_exam_apply` int(11) NOT NULL DEFAULT '0',
  `apss_examno_flag` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'F',
  `apss_examno_f` int(3) NOT NULL DEFAULT '0',
  `apss_examno_l` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`apss_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_schedule_time`;
CREATE TABLE `appl_placement_schedule_time` (
  `apst_id` int(11) NOT NULL AUTO_INCREMENT,
  `apst_aps_id` int(11) NOT NULL COMMENT 'fk appl_placement_schedule(aps_id)',
  `apst_test_type` int(11) NOT NULL COMMENT 'fk appl_test_type(act_id)',
  `apst_time_start` time NOT NULL,
  `apst_time_end` time DEFAULT NULL,
  PRIMARY KEY (`apst_id`),
  KEY `appl_placement_schedule_time_fk1` (`apst_aps_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_placement_weightage`;
CREATE TABLE `appl_placement_weightage` (
  `apw_id` int(11) NOT NULL AUTO_INCREMENT,
  `apw_app_id` int(11) NOT NULL,
  `apw_apd_id` int(11) NOT NULL COMMENT 'fk appl_placement_detl(apd_id)',
  `apw_weightage` int(3) NOT NULL,
  PRIMARY KEY (`apw_app_id`,`apw_apd_id`),
  UNIQUE KEY `apw_id` (`apw_id`),
  KEY `appl_placement_weightage_fk_2` (`apw_apd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_program_req`;
CREATE TABLE `appl_program_req` (
  `apr_id` int(11) NOT NULL AUTO_INCREMENT,
  `apr_academic_year` int(11) NOT NULL COMMENT 'fk tbl_program(program_code)',
  `apr_program_code` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apr_decipline_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`apr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_program_req_mig`;
CREATE TABLE `appl_program_req_mig` (
  `AcademicYear` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Periode` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ProgramCode` int(3) DEFAULT NULL,
  `HighSchoolDeciplineCode` int(3) DEFAULT NULL,
  `semester` int(1) DEFAULT NULL,
  `EntranceCode` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_room`;
CREATE TABLE `appl_room` (
  `av_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `av_location_code` int(11) unsigned NOT NULL COMMENT 'FK to tbl_appl_location',
  `av_room_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `av_room_name_short` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `av_room_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `av_building` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `av_tutorial_capacity` int(11) NOT NULL DEFAULT '0',
  `av_exam_capacity` int(11) NOT NULL DEFAULT '0',
  `av_update_by` bigint(20) NOT NULL,
  `av_update_date` datetime NOT NULL,
  `av_status` tinyint(1) NOT NULL DEFAULT '1',
  `av_seq` int(11) DEFAULT NULL,
  PRIMARY KEY (`av_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_room_assign`;
CREATE TABLE `appl_room_assign` (
  `ara_id` int(11) NOT NULL AUTO_INCREMENT,
  `ara_program1` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'fk_app_id',
  `ara_program2` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ara_room` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ara_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_room_test_type`;
CREATE TABLE `appl_room_test_type` (
  `artt_id` int(11) NOT NULL AUTO_INCREMENT,
  `artt_code` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `artt_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`artt_code`),
  UNIQUE KEY `artt_id` (`artt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_room_type`;
CREATE TABLE `appl_room_type` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_room_id` int(11) NOT NULL,
  `art_test_type` int(3) NOT NULL,
  PRIMARY KEY (`art_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_test_type`;
CREATE TABLE `appl_test_type` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT,
  `act_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `act_start_time` time DEFAULT NULL,
  `act_end_time` time DEFAULT NULL,
  PRIMARY KEY (`act_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `appl_upload_file`;
CREATE TABLE `appl_upload_file` (
  `auf_id` int(11) NOT NULL AUTO_INCREMENT,
  `auf_appl_id` int(11) NOT NULL COMMENT 'changed to transaction_id',
  `auf_file_name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auf_file_type` int(11) DEFAULT NULL,
  `auf_upload_date` datetime NOT NULL,
  `auf_upload_by` int(11) NOT NULL,
  `pathupload` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`auf_id`),
  KEY `auf_appl_id` (`auf_appl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `assessment_type`;
CREATE TABLE `assessment_type` (
  `at_id` int(11) NOT NULL AUTO_INCREMENT,
  `at_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_description` text COLLATE utf8mb4_unicode_ci,
  `at_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `at_markentry_readonly` tinyint(4) DEFAULT '0',
  `createddt` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddt` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `attendance_mark`;
CREATE TABLE `attendance_mark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdStudentRegistration` int(11) NOT NULL COMMENT 'FK tbl_studentregistration',
  `IdProgram` int(11) NOT NULL COMMENT 'FK tbl_program',
  `IdSubject` int(11) NOT NULL DEFAULT '0' COMMENT 'FK tbl_subjectmaster',
  `coursegroup_id` int(11) NOT NULL COMMENT 'FK course_group',
  `coursegroupschedule_id` int(11) NOT NULL COMMENT 'FK course_group_schedule',
  `marks` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'in percentage',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `auditlog`;
CREATE TABLE `auditlog` (
  `audit_id` int(11) NOT NULL AUTO_INCREMENT,
  `tableName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rowPK` int(11) DEFAULT NULL,
  `fieldName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` blob,
  `new_value` blob,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`audit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `audit_log`;
CREATE TABLE `audit_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log_module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_identity` bigint(10) unsigned NOT NULL,
  `log_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_type_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `log_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_user` int(10) unsigned NOT NULL,
  `log_date` datetime NOT NULL,
  `log_ip` int(10) unsigned NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `log_identity` (`log_identity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2018-03-05 10:21:07
