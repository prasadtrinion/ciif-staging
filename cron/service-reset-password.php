<?php

set_time_limit(0);
ini_set('memory_limit', '-1');

class resetPasswordService
{
	public $conn;
    
    public function __construct(){    	
    
        $paths = array(
            realpath(dirname(__FILE__) . '/../library'),
            '.',
        );

        set_include_path(implode(PATH_SEPARATOR, $paths));

        $path = realpath(dirname(__FILE__).'/../');

        require_once($path.'/library/Zend/Loader/Autoloader.php');
        $loader = Zend_Loader_Autoloader::getInstance();

        $this->config = $config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');

        $this->conn = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);

		$dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__.'/service'));
		$classes = array();
		foreach ( $dir as $file )
		{
			if ($file->isDir()) continue;

			include_once $file;
		}
		
		 require_once($path.'/library/Cms/Common.php');
    }
    
}


	global $argv;	
	if($argv[1]){
		$limit=$argv[1];
	}
	
    $service = new resetPasswordService();    
    
    $select = $service->conn->select() 
        			->from(array('u' => 'user'))
        			->where('u.lastlogin IS NULL')
        			->where('u.resetpwd IS NULL');
        			
        			if(isset($limit)){
        				$select->limit($limit);
        			}
	echo $select;
    $rows = $service->conn->fetchAll($select);

    //reset password
    if(count($rows)>0){
    	foreach($rows as $key=>$row){
    		$salt =  Cms_Common::generateRandomString(22);
	        $options  = array('salt' => $salt);
	        $hash = password_hash(123456, PASSWORD_BCRYPT, $options);
	        
	        $service->conn->update('user',array('password'=>$hash,'salt' => $salt,'resetpwd'=>new Zend_Db_Expr('UTC_TIMESTAMP()')), array('id = ?' => $row['id']));
    	}
    }
    
    echo 'end';
?>