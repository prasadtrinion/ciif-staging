<?php
//set unlimited
ini_set('display_errors', 'On');
ini_set('html_errors', 0);
error_reporting(-1);
//set_time_limit(0);
//ini_set('memory_limit', '-1');
date_default_timezone_set('Asia/Kuala_Lumpur');

define('_DEBUG', 0);

class cronService
{
	public $conn;
    
    public function __construct(){    	
    
        $paths = array(
            realpath(dirname(__FILE__) . '/../library'),
            '.',
        );

        set_include_path(implode(PATH_SEPARATOR, $paths));

        $path = realpath(dirname(__FILE__).'/../');

        require_once($path.'/library/Zend/Loader/Autoloader.php');
        $loader = Zend_Loader_Autoloader::getInstance();

        $this->config = $config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');

        $this->conn = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);

		$dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__.'/service'));
		$classes = array();
		foreach ( $dir as $file )
		{
			if ($file->isDir()) continue;

			include_once $file;
		}

    }
	
	public function close($id, $results=null,$recurring=0) 
	{
		$dataUpdate = array(
								'inprogress' => 0,
								'executed_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'), 
								'executed' => 1, 
								'execution_time' => $this->time_end(),
								'results' => $results
							);
		
		if ( $recurring == 1)
		{
			$dataUpdate['recurring_lastexecuted'] = new Zend_Db_Expr('UTC_TIMESTAMP()');
			$dataUpdate['executed'] = 0;
		}
		
		$this->conn->update('cron_service', $dataUpdate, array('id = ?' =>  $id) );
	}
	
	function time_start($id)
	{
		$starttime = microtime();	
		$starttime = explode(" ",$starttime);
		$starttime = $starttime[1] + $starttime[0];

		$this->_TIMESTART = $starttime;
	
		//update into in progress
		$dataUpdate = array(
								'inprogress' => 1, 
							);
		
		$this->conn->update('cron_service', $dataUpdate, array('id = ?' =>  $id) );
	}

	function time_end()
	{
		$starttime =  $this->_TIMESTART;

		$endtime = microtime();
		$endtime = explode(" ",$endtime);
		$endtime = $endtime[1] + $endtime[0];
		$stime = $endtime - $starttime;
		return round($stime,4);
	}
}


//Process
$service = new cronService();

$select = $service->conn->select()->from(array('a'=>'cron_service'))->limit(5);
if ( !defined('_DEBUG') )
{
	$select->where('a.executed = 0 AND inprogress = 0');
}
$results = $service->conn->fetchAll($select);

$db = $service->conn;

foreach ( $results as $row )
{
	$class = $row['class'] == 'self' ? __CLASS__ : $row['class'];

	if ( is_callable( array( $class, $row['action']) ) )
	{
		$exec = 1;
		$recurring = 0;

		if ( $row['recurring'] == 1)
		{
			$exec = 0;

			$checkdate = strtotime($row['recurring_day'].'-'.date('m-Y'));

			if ( $row['recurring_type'] == 'month' )
			{

				if ( date('d') == date('d', $checkdate) && strtotime(date('d-m-Y')) > strtotime($row['recurring_lastexecuted']) )
				{
					$exec = 1;
					$recurring = 1;
				}
			}
			else if ( $row['recurring_type'] == 'day' )
			{
				$howmany = howDays( strtotime($row['recurring_lastexecuted']), time() );

				if ( $howmany >= $row['recurring_day'] )
				{
					$exec = 1;
					$recurring = 1;

				}
			}
		}


		if ( $exec )
		{
			$service->time_start($row['id']);
			
			$func_results = call_user_func( array( $class , $row['action']) );

			fputs( fopen( "../log/cron.service.txt", 'a'), $func_results."\n\n".date("d/m/Y, g:i a")."\r\n------------------------\r\n\r\n");

			//done
			$service->close($row['id'], $func_results, $recurring );
		}
	}
}

echo 'Done. '.time();
exit;

function howDays($from, $to) {
    $first_date = $from;
    $second_date = $to;
    $offset = $second_date-$first_date; 
    return floor($offset/60/60/24);
}
?>