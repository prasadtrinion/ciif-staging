<?php
class Service_External
{
    static $db2;
    static $db;

    public static function externalDb()
    {
        $path = realpath(dirname(__FILE__).'/../../');

        $config = $config = new Zend_Config_Ini($path.'/application/configs/application.ini','development');
        try
        {
            self::$db2 = Zend_Db::factory('Pdo_Mysql', $config->resources->multidb->db2);
            self::$db2->getConnection();
        }
        catch (Zend_Db_Adapter_Exception $e)
        {
            echo $e->getMessage();
            die('Could not connect to External database.');
        }
    }

    public static function initMaintenance($type='')
    {
        global $db;

        self::$db = $db;

        //init external db
        self::externalDb();

        $out = array();

        //stuff
        $select = $db->select()->from(array('a' => 'external_datatype'));
        if ( $type != '' )
        {
            $select->where('a.name = ?', $type);
        }

        $results = $db->fetchAll($select);

        foreach ( $results as $row )
        {
            if ( is_callable(array(__CLASS__, $row['name'])) )
            {
                $result = call_user_func( array(__CLASS__, $row['name']));

                $out[] = self::updateDataset($row['name'],$result);
            }
            else
            {
                die('Invalid function: '.$row['name']);
            }
        }

        return implode("\n\n", $out);
    }

    /*
    *  ------------ DATA SET ----------------
    */

    public static function LearningMode()
    {
        $db = self::$db2;

        $selectData = $db ->select()
            ->from(array('a'=>'tbl_definationms'),array('*'))
            ->where('a.idDefType = 96')
            ->where('a.Status=1');


        $results = $db->fetchAll($selectData);

        $mapped = array_map(function($row) {

            //if ( $row['IdQualification'] != 22 ) {
            return array(
                'id' => $row['DefinitionCode'],
                'name' => $row['DefinitionDesc'],
                'value' => $row['idDefinition']
            );
            //}
        }, $results);

        return $mapped;
    }

    public static function Race()
    {
        $db = self::$db2;

        $selectData = $db ->select()
            ->from(array('a'=>'tbl_definationms'),array('*'))
            ->where('a.idDefType = 60')
            ->where('a.Status=1');


        $results = $db->fetchAll($selectData);

        $mapped = array_map(function($row) {

            //if ( $row['IdQualification'] != 22 ) {
            return array(
                'id' => $row['DefinitionCode'],
                'name' => $row['DefinitionDesc'],
                'value' => $row['idDefinition'],
                'deforder' => $row['defOrder']
            );
            //}
        }, $results);

        return $mapped;
    }

    public static function Religion()
    {
        $db = self::$db2;

        $selectData = $db ->select()
            ->from(array('a'=>'tbl_definationms'),array('*'))
            ->where('a.idDefType = 23')
            ->where('a.Status=1');


        $results = $db->fetchAll($selectData);

        $mapped = array_map(function($row) {

            //if ( $row['IdQualification'] != 22 ) {
            return array(
                'id' => $row['DefinitionCode'],
                'name' => $row['DefinitionDesc'],
                'value' => $row['idDefinition'],
                 'deforder' => $row['defOrder']
            );
            //}
        }, $results);

        return $mapped;
    }


    public static function Qualification()
    {
        $db = self::$db2;

        $selectData = $db ->select()
            ->from(array('a'=>'tbl_qualificationmaster'),array('*'))
            ->where('a.qualification_type_id = 607')
            ->where('a.Active=1')
            ->order('a.QualificationRank desc');


        $results = $db->fetchAll($selectData);

        $mapped = array_map(function($row) {

            //if ( $row['IdQualification'] != 22 ) {
            return array(
                'id' => $row['IdQualification'],
                'name' => $row['QualificationLevel']
            );
            //}
        }, $results);


        return $mapped;
    }

    public static function Courses()
    {
        $db = self::$db2;

        $selectData = $db ->select()
            ->from(array('a'=>'tbl_subjectmaster'),array('*'))
            ->where('a.Active=1');


        $results = $db->fetchAll($selectData);

        $mapped = array_map(function($row) {

            return array(
                'id' => $row['SubCode'],
                'name' => $row['SubjectName'],
                'value' => $row['IdSubject']
            );
        }, $results);


        return $mapped;
    }

    public static function Curriculum()
    {
        $db = self::$db2;

        $select = $db->select()
            ->from(array("a" => "tbl_program"))
            ->where('Active = 1')
            ->order('ProgramName ASC');

        $results = $db->fetchAll($select);

        $mapped = array_map(function($row) {

            return array(
                'id' => $row['ProgramCode'],
                'name' => $row['ProgramName'],
                'value' => $row['IdProgram'],
                'data'  => json_encode($row)
            );
        }, $results);


        return $mapped;
    }

    public static function CurriculumGroup()
    {
        $db = self::$db2;

        $select = $db->select()
            ->from(array("a" => "tbl_program_group"))
            ->where('a.active = 1');

        $results = $db->fetchAll($select);

        $mapped = array_map(function($row) {

            return array(
                'id' => $row['group_id'],
                'name' => $row['name']
            );
        }, $results);


        return $mapped;
    }

    public static function Company()
    {
        $db = self::$db2;

        $select = $db->select()
            ->from(array("a" => "tbl_takafuloperator"));
            //->where('registration_type = ?', 1026);

        $results = $db->fetchAll($select);

        $mapped = array_map(function($row) {

            return array(
                'id' => $row['Id'],
                'name' => $row['name']
            );
        }, $results);


        return $mapped;
    }

    public static function CurriculumCourses()
    {
        global $db;

       //init external db
        self::externalDb();


        $db2 = self::$db2;

        $select = $db->select()->from(array('a' => 'courses'));
        $_courses = $db->fetchAll($select);

        $courses = array();
        foreach ($_courses as $row)
        {
            $courses[ $row['code'] ] = $row;
        }


        //start
        $db->query("TRUNCATE TABLE curriculum_courses");

        //get all programs
        $select = $db2->select()
            ->from(array("a" => "tbl_program"))
            ->where('Active = 1')
            ->order('ProgramName ASC');

        $programs = $db2->fetchAll($select);

        foreach ( $programs as $program )
        {
            //get their landscape ( 1 is enough, desc )
            $select = $db2->select()->from(array('lan' => 'tbl_landscape'))
                ->where('lan.IdProgram = ?',$program['IdProgram'])
                ->where('lan.Active = 1')
                ->order("lan.UpdDate DESC")
                ->limit(1);

            $landscape = $db2->fetchRow($select);


            //curriculum
            $select = $db->select()->from(array('a' => 'curriculum'))
                ->where('a.code = ?',$program['ProgramCode'])
                ->where('a.display_only = 0')
                ->limit(1);

            $curriculum = $db->fetchRow($select);

            if ( empty($curriculum) )
            {
                echo 'Failed to sync. Looking for Program "'.$program['ProgramCode'].'" (id: '.$program['IdProgram'].') in SMS and cannot find the matching curriculum in LMS.';
                exit;
            }


            if ( !empty($landscape) )
            {
                $select = $db2->select()
                    ->from(array("ls"=>"tbl_landscapesubject"))
                    ->joinLeft(array("s"=>"tbl_subjectmaster"),'s.IdSubject=ls.IdSubject',array('BahasaIndonesia','SubCode','SubjectName'=>'SubjectName'))
                    ->joinLeft(array('l'=>'tbl_landscapeblock'), 'l.idblock=ls.IdLevel',array('l.block as Level'))
                    ->where("ls.IdProgram = ?",$program['IdProgram'])
                    ->where("ls.IdLandscape = ?",$landscape['IdLandscape'])
                    ->where("IDProgramMajoring = 0")
                    ->order("ls.IdSemester")
                    ->order("ls.IdLandscapeSub")
                    ->group('ls.IdSubject');

                $lsubs = $db2->fetchAll($select);

                foreach ( $lsubs as $lsub )
                {
                   $data = array(
                                    'curriculum_id' => $curriculum['id'],
                                    'course_id'     => $courses[ $lsub['SubCode'] ]['id'],
                                    'created_by'    => Zend_Auth::getInstance()->getIdentity()->id,
                                    'created_date'  =>  new Zend_Db_Expr('UTC_TIMESTAMP()')
                    );

                    $db->insert('curriculum_courses', $data);
                }
            }
        }
    }

    /*
     *  ---------------- FUNCTIONS -------------
     */
    //update data
    protected static function updateDataset($name='', array $remote )
    {
        $db = self::$db;

        $select = $db->select()->from(array('a' => 'external_datatype'))->where('name = ?', $name );
        $datatype = $db->fetchRow($select);

        if ( empty($datatype) )
        {
            return 'Invalid Datatype :'.$name;
        }

        $select2 = $db->select()->from(array('a' => 'external_data'))->join(array('b'=>'external_datatype'),'b.id=a.type_id',array())
                                ->where('b.name = ?', $name);

        $local  = $db->fetchAll($select2);

        $a = array_column($remote,'id');
        $b = array_column($local, 'code');

        $add = array_diff($a, $b);
        $remove = array_diff($b, $a);

        $added = $removed = $updated = 0;

        if ( !empty($add) )
        {
            foreach ($remote as $row)
            {
                if ( in_array($row['id'], $add) ) {

                    $data = array(
                                    'type_id'   => $datatype['id'],
                                    'code'      => $row['id'],
                                    'name'      => $row['name'],
                                    'value'     => isset($row['value']) ? $row['value'] : '',
                                    'fulldata'      => isset($row['data']) ? $row['data'] : ''
                                );

                    $db->insert('external_data', $data);
                    $added++;
                }
            }
        }

        if ( !empty($remove) )
        {
            foreach ($remove as $del)
            {
                $db->delete('external_data', array('code = ?' => $del));
                $removed++;
            }
        }


        //$localupdate = array_column($local, );
        $localbycode = array();
        foreach ( $local as $lo )
        {
            $localbycode[$lo['code']] = $lo;
        }


        //update
        if ( $datatype['updatedata'] == 1 )
        {
            foreach ($remote as $row)
            {

                if ( isset($localbycode[$row['id']]['name']) && $localbycode[$row['id']]['name'] != $row['name']  )
                {
                    $data = array(
                        'code' => $row['id'],
                        'name' => $row['name'],
                        'value' => isset($row['value']) ? $row['value'] : '',
                        'fulldata' => isset($row['data']) ? $row['data'] : '',
                        'modified_date' => date('Y-m-d H:i:s')
                    );

                    $db->update('external_data', $data, array('type_id = ?' => $datatype['id'], 'code' => $row['id']));
                    $updated++;
                }
            }
        }

        //update datatype
        $db->update('external_datatype',array('lastsync'=>new Zend_Db_Expr('UTC_TIMESTAMP()')), array('id = ?' => $datatype['id']));
        return $name.' { Added New: '.$added.', Removed: '.$removed. ', Updated: '.$updated.'}';

    }


}