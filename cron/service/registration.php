<?php
class Service_Registration
{
    static $db;


    public static function init($type='')
    {
        global $db;

        self::$db = $db;
        
        $db->update("enrol", array('active' => 0), array(
            'expiry_date IS NOT NULL',
            'CURDATE() > expiry_date',
            'active = ?' => 1
        ));
		
		return '';
	}

}