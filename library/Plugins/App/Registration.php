<?php

class Plugins_App_Registration extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        Cms_Events::attach('auth.register.success', array($this,'registrationEmail'));
        Cms_Events::attach('auth.register.form', array($this,'registrationForm'));
        Cms_Events::attach('auth.register.course', array($this,'courseRegistrationEmail'));
        Cms_Events::attach('auth.register.exam', array($this,'examRegistrationEmail'));
    }

    public static function registrationForm()
    {

    }

    public static function getLearningMode(array $schedule)
    {
        $mode = 'OL';

        $online = 0;
        $ftf = 0;

        if ( !empty($schedule) )
        {
            foreach ( $schedule as $course_id => $schedule_id )
            {
                if ( $schedule_id == 0 )
                {
                    $online++;
                }
                else
                {
                    $ftf++;
                }
            }
        }

        if ( $online == 0 && $ftf > 0 )
        {
            $mode = 'FTF';
        }
        else if ( $online > 0 && $ftf > 0 )
        {
            $mode = 'FTF-OL'; //blended
        }

        return $mode;
    }

    public static function registrationEmail($e)
    {
        $data = $e->getParams();

        $userDb = new App_Model_User();

        $user = $userDb->fetchRow(array('id = ?' => $data['id']));

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }
    
        $dataProfile = $user->toArray();
    
        $db      = getDB();
        $db2     = getDB2();
        $edDb    = new Admin_Model_DbTable_ExternalData();
        $session = new Zend_Session_Namespace('Registration');
        
        $select = $db2->select()
            ->from(array('a' => 'student_profile'))
            ->where('a.std_idnumber = ?', $dataProfile['username']);
        $student_profile = $db2->fetchRow($select);
    
        $std_id = null;
    
        if ($student_profile)
        {
            $std_id = $student_profile['std_id'];
        }
        else
        {
            $formData = $session->formData;
        
            foreach ($dataProfile as $field => $value)
            {
                if (!$value && isset($formData[$field]) && $formData[$field])
                {
                    $dataProfile[$field] = $formData[$field];
                }
            }
        
            $dataProfile['birthdate'] = date('Y-m-d', strtotime($formData['dob_year'] .'-'. $formData['dob_month'] .'-'. $formData['dob_day']));
            $dataProfile['data_from'] = 'LMS';
            
            $params = $dataProfile;
    
            if (isset($dataProfile['qualification']) && $dataProfile['qualification'])
            {
                $qualification = $edDb::getValue($dataProfile['qualification'], 0);
                if ($qualification)
                {
                    $params['qualification'] = $qualification['value'];
                }
                else
                {
                    $params['qualification'] = 0;
                }
            }
            
            if (isset($dataProfile['race']) && $dataProfile['race'])
            {
                $race = $edDb::getValue($dataProfile['race'], 0);
                if ($race)
                {
                    $params['race'] = $race['value'];
                }
                else
                {
                    $params['race'] = 0;
                }
            }
    
            if (isset($dataProfile['religion']) && $dataProfile['religion'])
            {
                $religion = $edDb::getValue($dataProfile['religion'], 0);
                if ($religion)
                {
                    $params['religion'] = $religion['value'];
                }
                else
                {
                    $params['religion'] = 0;
                }
            }
            
            $sp  = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewProfile', $params);
            $std_id = $sp['sp_id'];
        }
    
        if ($std_id && !$dataProfile['external_id'])
        {
            $db->update('user', array('external_id' => $std_id), array('id = ?' => $dataProfile['id']));
        }
    
        $_SESSION['Zend_Auth']['storage']->external_id = $std_id;
        $_SESSION['Zend_Auth']['storage']->sp_id       = $std_id;
        $session->setExpirationSeconds(1);

        $msg = "Hi $user[firstname] $user[lastname], <br />

Thank-you for becoming a part of ".APP_NAME."! <br /><br />

You can start finding courses you might like here: <br />
".APP_URL."/courses <br /><br />

And/or you can personalize your account here:
".APP_URL."/user/editprofile <br /><br />

If you have any questions or issues, please don't hesitate to reach out. <br /><br />

- ".APP_NAME." Team";

        $response = Cms_Curl::__(SMS_API . '/get/Registration/do/getCommunicationTemplate', array('type' => '1-lms'));
        if(isset($response['tpl_id']) && $response['tpl_id']) {
            $msg = $response['tpl_content'];
            $msg = str_replace("[name]", "$user[firstname] $user[lastname]", $msg);
            $msg = str_replace("[username]", $user["username"], $msg);
            $msg = str_replace("[login-url]", APP_URL."/login", $msg);
            $msg = str_replace("[forgetpassword-url]", APP_URL."/index/forgotpassword", $msg);
            $msg = str_replace("[course-url]", APP_URL."/courses", $msg);
            $msg = str_replace("[account-url]", APP_URL."/user/profile", $msg);
        }

        //send email
        //$mail = new Cms_SendMail();
        //$mail->fnSendMail($user['email'], APP_NAME. ' - Welcome!', $msg);
        
        $db = getDB2();
        $email_que = array(
            'recepient_email'     => $user['email'],
            'subject'             => APP_NAME. ' - Welcome!',
            'content'             => $msg,
            'date_que'            => date("Y-m-d H:i:s"),
            'smtp'                => 1
        );
        $db->insert('email_que', $email_que);
    }

    public static function courseRegistrationEmail($e) {
        $data = $e->getParams();
        $db   = getDB();
        $db2  = getDB2();

        $orderDb = new App_Model_Order();
        $enrolDb = new App_Model_Enrol();
        $userDb  = new App_Model_User();

        $order   = $orderDb->getOrder(array('a.id = ?' => $data['order_id']), true);
        $user    = $userDb->getUser($order['user_id']);
        unset($user['password'], $user['salt']);

        $learningmode = $order['learningmode'];

        $type = 2;
        if($learningmode == 'FTF') {
            $type = '2-f2f';
        }

        $response = Cms_Curl::__(SMS_API . '/get/Registration/do/getCommunicationTemplate', array('type' => $type));

        if(!isset($response['tpl_id'])) {
            return false;
        }

        $msg = $response['tpl_content'];

        $program_name = ($order['curriculum_name'] ? $order['curriculum_name'] : $order['course_name']);

        $msg = str_replace("[name]", "$user[firstname] $user[lastname]", $msg);
        $msg = str_replace("[Program Name]", $program_name, $msg);
        $msg = str_replace("[URL]", APP_URL."/login", $msg);
        $msg = str_replace("[username]", "$user[username]", $msg);
        $msg = str_replace("[password]", "Your current password", $msg);
        $msg = str_replace("[invoice-url]", APP_URL."/user/invoice/id/". $data['order_id'], $msg);
        $msg = str_replace("[invoice-no]", $order['invoice_no'], $msg);
        $msg = str_replace("[invoice-amount]", $order['amount'], $msg);

        $select = $db->select()
            ->from(array("a" => "enrol"), array("id", "course_id", "batch_id", "curriculum_id", "regtype", "learningmode", "schedule"))
            ->join(array("b" => "courses"), "b.id = a.course_id", array('course_title' => 'title', 'course_code' => 'code', 'course_code_safe' => 'code_safe'))
            ->where("a.order_id = ?", $data['order_id']);
        $enrols = $db->fetchAll($select);

        $learning_material = '';

        foreach($enrols as $row) {
            $learning_material .= "<li><p><a href='".APP_URL."/courses/$row[course_code_safe]/learn' target='_blank'>$row[course_code] - $row[course_title]</a></p></li>";
        }

        $learning_material = '<ol>'. $learning_material .'</ol>';
        $msg = str_replace('[learning-material]', $learning_material, $msg);

        $schedules = '';

        foreach($enrols as $row) {
            if($row['learningmode'] != 'FTF') {
                continue;
            }

            $select = $db2->select()
                ->from(array('a' => 'course_group_schedule'), array('sc_date_start', 'sc_date_end', 'sc_duration', 'sc_start_time', 'sc_end_time', 'sc_venue'))
                ->join(array('b' => 'course_group'), 'b.id = a.IdSchedule', array())
                ->joinLeft(array('c' => 'tbl_staffmaster'), 'b.IdLecturer = c.IdStaff', array('FullName'))
                ->join(array('d' => 'tbl_subjectmaster'), 'd.IdSubject = b.IdSubject', array('SubjectName', 'SubCode'))
                ->where('a.sc_id = ?', $row['schedule']);
            $schedule = $db2->fetchRow($select);

            $sc_date   = date('d-m-Y', strtotime($schedule['sc_date_start']));
            $sc_date   .= ' ~ ' . date('d-m-Y', strtotime($schedule['sc_date_end']));
            $schedules .= "<li>
                            <p>
                                <strong>$schedule[SubCode] - $schedule[SubjectName]</strong><br />
                                Date: $sc_date <br />
                                Time: ". date('h.i a', strtotime($schedule['sc_start_time'])) . ' to ' . date('h.i a', strtotime($schedule['sc_end_time'])) ."<br />
                                Venue: $schedule[sc_venue] <br />
                                Lecturer: $schedule[FullName]
                            </p>
                        </li>";
        }

        $schedules = '<ol>'. $schedules .'</ol>';
        $msg = str_replace('[course-schedule]', $schedules, $msg);

        $emailSubject = APP_NAME. ' - Confirmation of Registration';

        //$mail = new Cms_SendMail();
        //$mail->fnSendMail($user['email'], $emailSubject, $msg);
        
        $db2       = getDB2();
        $IdProgram = $order['IdProgram'];
        $select    = $db2->select()->from(array('a' => 'tbl_program'))->where('a.IdProgram = ?', $IdProgram);
        $program   = $db2->fetchRow($select);
        
        $emailDb = new App_Model_Email();
        $data = array(
            'recepient_email' => $user['email'],
            'subject'         => $emailSubject,
            'content'         => $msg,
            'smtp'            => $program['esmtp_id']
        );

        //to send email with attachment
        $emailDb->addData($data);
    }
    
    public static function examRegistrationEmail($e)
    {
        $auth = Zend_Auth::getInstance();
        
        $data     = $e->getParams();
        $orderDb  = new App_Model_Order();
        $order    = $orderDb->getOrder(array('a.id = ?' => $data['order_id']), true);
        $params   = array(
            'er_main_id' => $order['item_id'],
            'invoice_id' => $order['invoice_id'],
            'email'      => $auth->getIdentity()->email
        );
        $response = Cms_Curl::__(SMS_API . '/get/Examination/do/sendRegistrationEmail', $params);
    }
}