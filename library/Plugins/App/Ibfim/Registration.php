<?php

class Plugins_App_Ibfim_Registration extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        Cms_Events::attach('auth.register.form', array($this, 'registrationForm'));
        Cms_Events::attach('auth.register.post', array($this, 'registrationPost'));
        Cms_Events::attach('payment.migs.success', array($this, 'paymentMigsProcess'));

        //Cms_Hooks::push('registration.message', '<div class="uk-alert">Malaysian: Please enter your <strong>NRIC</strong> number as username</div>');
        $message = '<div class="uk-alert uk-text-left">'.Cms_Options::__('registration_text').'</div>';

        Cms_Hooks::push('registration.message', $message);
    }

    public static function paymentMigsProcess($e)
    {
        $data = $e->getParams();
    }

    public static function registrationPost($e)
    {
        $data = $e->getParams();

        $form = new App_Form_User();

        $form = self::registrationFormFields($form);

        if ( !$form->isValid($data) )
        {
            return array('fail' => 1, 'msg' => $form->getMessages());
        }
        else
        {
            $uniquedata = array();
            $fields = array('company', 'nationality', 'qualification','gender','race','religion','estatus');
            foreach ( $fields as $field )
            {
                $uniquedata[$field] = $data[$field];
            }

            //dob

            $uniquedata['birthdate'] = date('Y-m-d', strtotime($data['dob_year'].'-'.$data['dob_month'].'-'.$data['dob_day']));

            return array('fail' => 0, 'data' => $uniquedata);

        }

        //$check = Cms_Curl::__()
    }


    public static function registrationForm($e)
    {
        $data = $e->getParams();

        $form  = $data['form'];

        $form = self::registrationFormFields($form);

        //company
        $dataDb = new Admin_Model_DbTable_ExternalData();
        $company = $dataDb->getData('Company');
        $company_list = [];

        foreach( $company as $row ) {
            $company_list[] = addslashes($row['name']);
        }

        //view
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');

        $view->form = $form;

        $view->company = json_encode($company_list);


        $view->setBasePath(APPLICATION_PATH . '/views/');
        return $view->render('plugins/registration.phtml');
    }

    public static function registrationFormFields($form)
    {
        //nationality
        $nationality_list = array('' => '');

        foreach( Cms_Common::getCountryList() as $code => $name ) {
            $nationality_list[ $code ] = $name;
        }

        $form->addElement('select', 'nationality', array(
            'required'    => true,
            'multiOptions'=> $nationality_list
        ));


        //qualification
        $dataDb = new Admin_Model_DbTable_ExternalData();
        $qualification = $dataDb->getData('Qualification');

        $qualification_list = array('' => '');
        foreach( $qualification as $row ) {
            $qualification_list[ $row['id'] ] = $row['name'];
        }

        $form->addElement('select', 'qualification', array(
            'required'    => true,
            'multiOptions'=> $qualification_list
        ));

        //gender
        $gender_list = array(
                        "" => "",
                        "1" => "Male",
                        "2" => "Female",
                    );

        $form->addElement('select', 'gender', array(
            'required'    => true,
            'multiOptions'=> $gender_list
        ));

        //Race
        $dataDb = new Admin_Model_DbTable_ExternalData();
        $race = $dataDb->getData('Race');

        $race_list = array('' => '');
        foreach( $race as $row ) {
            $race_list[ $row['id'] ] = $row['name'];
        }

        $form->addElement('select', 'race', array(
            'required'    => true,
            'multiOptions'=> $race_list
        ));

        //Religion
        $dataDb = new Admin_Model_DbTable_ExternalData();
        $religion = $dataDb->getData('Religion');

        $religion_list = array('' => '');
        foreach( $religion as $row ) {
            $religion_list[ $row['id'] ] = $row['name'];
        }

        $form->addElement('select', 'religion', array(
            'required'    => true,
            'multiOptions'=> $religion_list
        ));

        //Current Employment Status
        $estatus_list = array(
                        "" => "",
                        "1" => "Self employed",
                        "2" => "Goverment Sector",
                        "3" => "Private Sector",
                        "4" => "Retiree",
                        "5" => "Housewife",
                        "6" => "Others",
                    );

        $form->addElement('select', 'estatus', array(
            'required'    => true,
            'multiOptions'=> $estatus_list
        ));

        //company
        $dataDb = new Admin_Model_DbTable_ExternalData();
        $company = $dataDb->getData('Company');

        $company_list = array(0 => '');
        foreach( $company as $row ) {
           $company_list[ $row['id'] ] = $row['name'];
        }

        $form->addElement('select', 'company', array(
            'required'    => true,
            'class'      => 'uk-form-medium uk-width-1-1',
            'multiOptions'=> $company_list
        ));

        $form->nationality->setAttrib('required','required');
        $form->company->setAttrib('required','required');
        $form->qualification->setAttrib('required','required');
        $form->gender->setAttrib('required','required');
        $form->race->setAttrib('required','required');
        $form->religion->setAttrib('required','required');
        $form->estatus->setAttrib('required','required');

        return $form;
    }
}