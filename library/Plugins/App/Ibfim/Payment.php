<?php
class Plugins_App_Ibfim_Payment extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        Cms_Events::attach('payment.success', array($this, 'emailEnrol'));
    }

    public static function emailEnrol($e)
    {
        $data = $e->getParams();
        $payment_status = $e->getParams();


        $orderDb = new App_Model_Order();
        $orderGroupDb = new App_Model_OrderGroup();
        $orderGroupUserDb = new App_Model_OrderGroupUser();

        $courseDb = new App_Model_Courses();
        $currDb = new App_Model_Curriculum();
        
        $order_id = $payment_status['external_id'];
        $order    = $orderDb->getOrder(array('a.id = ?' => $order_id), 1);

        // $data = $data['data'];

        //get migs  mt_external_id to get order info
        // $order = $orderDb->getOrder(array('id = ?' => $data['mt_external_id']));

        if (empty($order)) throw new Exception('Invalid Order ID');

        if ( $order['status'] != 'PENDING' )
        {
            return;
        }

        $userDb = new App_Model_User();

        $user = $userDb->fetchRow(array('id = ?' => $order['user_id']));

        if ( empty($user) )
        {
            throw new Exception('Invalid User ID');
        }

        if ( $order['regtype'] == 'course' )
        {
            $course = $courseDb->getCourse(array('a.id = ?' => $order['item_id']));
            $label = array(
                            'item_name' => $course['title'],
                            'item_type' => 'Course',
                            'item_url'  => APP_URL . '/courses/'. $course['code_safe']
            );
        }
        else
        {
            $curriculum = $currDb->getCurriculum(array('id = ?' => $order['item_id']));
            $label = array(
                'item_name' => $curriculum['name'],
                'item_type' => Cms_Options::__('label_curriculum'),
                'item_url' => APP_URL .'/courses/curriculum/cid/'.$order['item_id']
            );
        }

        $msg = "Hi $user[firstname] $user[lastname], <br />

You have successfully enrolled in ".$label['item_name']."! <br /><br />

Start learning:<br />
".APP_URL."/dashboard <br /><br />

View purchase information here: <br />
".APP_URL."/user/invoice/id/".$order['id']." <br /><br />


If you have any questions or issues, please don't hesitate to reach out. <br /><br />

- ".APP_NAME." Team";

        //send email
        $mail = new Cms_SendMail();
        // $mail->fnSendMail($user['email'], APP_NAME.' - '. $label['item_type'].' Registration - '.$label['item_name'], $msg);


        //group
        $group = $orderGroupDb->getGroup(array('order_id = ?'=>$order['id']));
        if ( !empty($group) )
        {
            $groupUsers = $orderGroupUserDb->getUsers(array('group_id = ?' => $group['id']));
            foreach ( $groupUsers as $guest )
            {
                $msg = "Hi $guest[firstname] $guest[lastname], <br /><br />

Your friend <strong>$user[firstname] $user[lastname]</strong> enrolled you in <strong>".$label['item_name']."</strong>. You can complete your registration by following the link below: <br /><br />

".APP_URL."/index/activate/key/".$guest['token']."<br /><br />

If you have any questions or issues, please don't hesitate to reach out. <br /><br />

- ".APP_NAME." Team";

                //send email
                $mail = new Cms_SendMail();
                $mail->fnSendMail($guest['email'], APP_NAME. ' - Complete your registration', $msg);
                //$mail->fnSendMail($user['email'], APP_NAME. ' - Complete your registration', $msg);
            }
        }
    }

    public static function orderStatus($status)
    {
        $color = '';

        switch ( $status )
        {
            case "PENDING":
                $color = 'status-pending';
            break;

            case "ACTIVE":
                $color = 'status-active';
            break;

            case "CANCELLED":
                $color = 'status-cancelled';
            break;
        }

        return $color;
    }

}