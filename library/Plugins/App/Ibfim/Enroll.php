<?php
class Plugins_App_Ibfim_Enroll extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        Cms_Events::attach('enroll.curriculum.check', array($this, 'checkRequirements'));
    }

    public static function checkRequirements($e)
    {
        $data = $e->getParams();

        $user = Zend_Auth::getInstance()->getIdentity();
        
        $birth_date = $user->birthdate;
        $valid_date = Cms_Common::validateDate($birth_date);
        
        $age = ($valid_date ? Cms_Common::calculateAge(date('d-m-Y', strtotime($birth_date))) : null);
        $qualification = Admin_Model_DbTable_ExternalData::getValue($user->qualification);
        $program = Admin_Model_DbTable_ExternalData::getValue($data['cid']);

        $fail = 0;

        if ( empty($age) || empty($qualification) || empty($program) )
        {
            $fail = 1;
        }

        if ( $fail == 1 )
        {
            return array('fail' => 'Failed Entry Requirement Check.');
        }

        //entry requirement
        //program
        //age
        //qualification
        $param = array(
                        'appl_age'                  => $age,
                        'appl_highqualification'    => $qualification['code'],
                        'appl_program'              => $program['code']
        );

        //$check = Cms_Curl::__('EntryRequirement','check',$param);
        $check = Cms_Curl::__(SMS_API.'/services/api/get/EntryRequirement/do/check', $param);

        if ( $check[0] == 1 )
        {
            return array('fail' => 0);
        }
        else
        {
            return array('fail' => 1, 'error' => ($check[1] == '' ? 'Failed Entry Requirement Check' : $check[1]));
        }
    }


}