<?php
class Plugins_App_Course extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {

    }

    public static function accessPeriod($val=null)
    {

        $int = filter_var($val, FILTER_SANITIZE_NUMBER_INT);
        $type_val = str_replace($int, '', $val);

        $text = array( $int );

        switch ( $type_val )
        {
            case "D": $text[] = 'Day'.($int>1?'s':''); break;
            case "M": $text[] = 'Month'.($int>1?'s':''); break;
            case "W": $text[] = 'Week'.($int>1?'s':''); break;
            case "Y": $text[] = 'Year'.($int>1?'s':''); break;
        }

        return $val != null ? implode(' ', $text) : 'Unlimited';
    }

    public static function accessPeriodDate($val=null)
    {

        $int = filter_var($val, FILTER_SANITIZE_NUMBER_INT);
        $type_val = str_replace($int, '', $val);

        $s = $int>1?'s':'';
        
        $format = '';

        switch ( $type_val )
        {
            case "D": $format = '+'.$int.' day'.$s; break;
            case "M": $format = '+'.$int.' month'.$s; break;
            case "W": $format = '+'.$int.' week'.$s; break;
            case "Y": $format = '+'.$int.' year'.$s; break;
        }

        $date = gmdate('Y-m-d', strtotime($format));

        return $val != null ? $date : null;
    }
}