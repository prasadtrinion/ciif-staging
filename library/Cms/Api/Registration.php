<?php

class Cms_Api_Registration extends Cms_Api
{
    /**
     * what
     */
    public static function addUser()
    {
        parent::getPost();
        $post = self::$post;

        //check required parameters
        self::checkParams(array('IdUser'), $post);


    }

    public static function addEnrolCorporate()
    {
        parent::getPost();
        $post = self::$post;

        //check required parameters
        self::checkParams(array('batchId'), $post);

        $batchId = $post['batchId'];
        $from = isset($post['from'])?$post['from']:1; //2=sms,1=corporate
        $inhouse = isset($post['inhouse'])?1:0;

        $paramsRegistration = array(
            'batchId' => $batchId
        );

        $regInfo = Cms_Curl::__(SMS_API . '/get/Registration/do/getRegistrationInfo', $paramsRegistration);


        if ($regInfo) {
            self::processEnrollment($regInfo, $batchId, $from);

        } else {
            die();
        }

    }
    
    public static function addEnrolIndividual($params = array())
    {
        try {
            parent::getPost();
            $post = self::$post;
    
            if (!empty($params)) {
                $post = $params;
            }
    
            self::checkParams(array('IdStudentRegistration'), $post);
            $IdStudentRegistration = $post['IdStudentRegistration'];
            $invoice_id            = (isset($post['invoice_id']) && $post['invoice_id'] ? $post['invoice_id'] : 0);
            $from                  = (isset($post['from']) && $post['from'] ? $post['from'] : 2);
            $from_desc             = ($from == 2 ? 'SMS' : 'CORP');
    
            $sms = getDB2();
            
            $curriculumDb = new App_Model_Curriculum();
            $courseDB     = new App_Model_Courses();
            $dbUser       = new App_Model_User();
            $orderGroupDb = new App_Model_OrderGroup();
            $enrolDb      = new App_Model_Enrol();
            $orderDb      = new App_Model_Order();
    
            $select         = $sms->select()
                ->from(array('a' => 'tbl_studentregistration'))
                ->join(array('b' => 'tbl_program'), 'b.IdProgram = a.IdProgram', array('ProgramName', 'ProgramCode', 'programme_type'))
                ->join(array('c' => 'tbl_program_scheme'), 'c.IdProgramScheme = a.IdProgramScheme', array('mode_of_program'))
                ->where('a.IdStudentRegistration = ?', $IdStudentRegistration);
            $sms_curriculum = $sms->fetchRow($select);
    
            if (!$sms_curriculum) {
                $response = array(
                    'error'   => 1,
                    'message' => 'record not found in tbl_studentregistration'
                );
        
                if (!empty($params)) {
                    return $response;
                }
        
                die(json_encode($response));
            }
            
            $learning_mode     = ($sms_curriculum['mode_of_program'] == 577 ? 'OL' : 'FTF');
            $lms_curriculum    = $curriculumDb->getDataByExternal($sms_curriculum['IdProgram']);
            $lms_curriculum_id = 0;
            
            if($lms_curriculum)
            {
                $lms_curriculum_id = $lms_curriculum['id'];
            }
    
            $select      = $sms->select()
                ->from(array('a' => 'tbl_studentregsubjects'))
                ->join(array('b' => 'tbl_subjectmaster'), 'b.IdSubject = a.IdSubject', array('SubCode', 'SubjectName'))
                ->where('a.IdStudentRegistration = ?', $IdStudentRegistration);
            $sms_courses = $sms->fetchAll($select);
    
            if (!$sms_courses) {
                $response = array(
                    'error'   => 1,
                    'message' => 'record not found in tbl_studentregsubjects'
                );
        
                if (!empty($params)) {
                    return $response;
                }
        
                die(json_encode($response));
            }
    
            $select   = $sms->select()
                ->from(array('c' => 'student_profile'))
                ->where('c.std_id = ?', $sms_curriculum['sp_id']);
            $sms_user = $sms->fetchRow($select);
    
            if (!$sms_user) {
                $response = array(
                    'error'   => 1,
                    'message' => 'record not found in student_profile'
                );
        
                if (!empty($params)) {
                    return $response;
                }
        
                die(json_encode($response));
            }
            
            $lms_user = $dbUser->getUserByUsername($sms_user['std_idnumber']);
    
            if (!$lms_user) {
                $salt         = Cms_Common::generateRandomString(22);
                $options      = array('salt' => $salt);
                $passwordTemp = 123456;
                $hash         = @password_hash($passwordTemp, PASSWORD_BCRYPT, $options);
                $randToken    = Cms_Common::generateRandomString(22);
        
                $lms_user       = array(
                    'username'      => $sms_user['std_idnumber'],
                    'password'      => $hash,
                    'salt'          => $salt,
                    'firstname'     => $sms_user['std_fullname'],
                    'lastname'      => '',
                    'email'         => $sms_user['std_email'],
                    'contactno'     => $sms_user['std_contact_number'],
                    'company'       => (isset($sms_user['std_corporate_id']) ? $sms_user['std_corporate_id'] : 0),
                    'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'role'          => 'student',
                    'token'         => $randToken,
                    'active'        => 1,
                    'from'          => 1,
                    'external_id'   => $sms_user['std_id'],
                );
                $lms_user['id'] = $dbUser->insert($lms_user);
                $api_params     = array('std_id' => $sms_user['std_id']);
                $api_response   = Cms_Curl::__(SMS_API . '/get/Registration/do/syncProfileToLms', $api_params);
            }
    
            $sms_user_id = $sms_user['std_id'];
            $lms_user_id = $lms_user['id'];
    
            $select = $sms->select()
                ->join(array('a' => 'invoice_student'), array())
                ->join(array('b' => 'invoice_main'), 'b.id = a.invoice_id', array('invoice_id' => 'id', 'bill_number', 'bill_amount', 'bill_paid', 'bill_balance'))
                ->joinLeft(array('c' => 'payment_mode'), 'c.id = b.payment_mode', array('payment_mode_code' => 'payment_mode'))
                ->where('a.IdStudentRegistration = ?', $IdStudentRegistration)
                ->where('a.examregistration_main_id = ?', 0)
                ->where('b.status = ?', 'A');
            $invoice = $sms->fetchRow($select);
    
            if (!$invoice)
            {
                $response = array(
                    'error'   => 1,
                    'message' => 'invoice not found'
                );
    
                if (!empty($params)) {
                    return $response;
                }
    
                die(json_encode($response));
            }
    
            $lms_order = array(
                'user_id'          => $lms_user_id,
                'created_date'     => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                'created_by'       => 1, //admin
                'amount'           => $invoice['bill_amount'],
                'paymentmethod'    => $invoice['payment_mode_code'],
                'status'           => 'ACTIVE',
                'invoice_id'       => $invoice['invoice_id'],
                'invoice_no'       => $invoice['bill_number'],
                'invoice_external' => 1,
                'regtype'          => ($sms_curriculum['programme_type'] == 1037 ? 'curriculum' : 'course'),
                'item_id'          => 0, //tbc munzir
                'learningmode'     => ($sms_curriculum['mode_of_program'] == 577 ? 'OL' : 'FTF'),
                'schedule'         => 0,
                'external_id'      => $IdStudentRegistration,
                'visibility'       => 0
            );
            $order_id = $orderDb->insert($lms_order);
    
            $lms_order_group = array(
                'order_id'     => $order_id,
                'totaluser'    => 1,
                'description'  => $from_desc,
                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                'created_by'   => 1,
            );
            $lms_order_group_id = $orderGroupDb->insert($lms_order_group);
    
            
            $curEnrol_id = 0;
            $curEnrol    = $enrolDb->getEnrol(array(
                'user_id = ?'       => $lms_user_id,
                'regtype = ?'       => 'curriculum',
                'curriculum_id = ?' => $lms_curriculum_id
            ));
    
            if (empty($curEnrol)) {
                $data = array(
                    'curriculum_id' => $lms_curriculum_id,
                    'course_id'     => NULL,
                    'schedule'      => NULL,
                    'learningmode'  => $learning_mode,
                    'user_id'       => $lms_user_id,
                    'regtype'       => 'curriculum',
                    'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'created_by'    => 1,
                    'order_id'      => $order_id,
                    'external_id'   => $IdStudentRegistration,
                    'expiry_date'   => NULL,
                    'invoice_id'    => $invoice['invoice_id']
                );
                $curEnrol_id = $curEnrol['id'] = $enrolDb->insert($data);
            }
            
            
            $lms_enrolled = 0;
            
            foreach ($sms_courses as $course) {
                $courseLms      = $courseDB->getCoursesByExternalId($course['IdSubject']);
                $expiry_date    = $courseLms['accessperiod'] == null ? null : Plugins_App_Course::accessPeriodDate($courseLms['accessperiod']);
                $curEnrolCourse = $enrolDb->getEnrol(array(
                    'user_id = ?'       => $lms_user_id,
                    'regtype = ?'       => 'course',
                    'curriculum_id = ?' => $lms_curriculum_id,
                    'course_id = ?'     => $courseLms['id']
                ));
        
                if (empty($curEnrolCourse)) {
                    $IdCourseTaggingGroup = 0;
                    $learning_mode        = ($sms_curriculum['mode_of_program'] == 577 ? 'OL' : 'FTF');
                    
                    if($course['IdCourseTaggingGroup'] != 'OL')
                    {
                        $IdCourseTaggingGroup = $course['IdCourseTaggingGroup'];
                        $learning_mode        = 'FTF';
                    }
                    
                    $data = array(
                        'curriculum_id' => $lms_curriculum_id,
                        'course_id'     => $courseLms['id'],
                        'schedule'      => $IdCourseTaggingGroup,
                        'learningmode'  => $learning_mode,
                        'user_id'       => $lms_user_id,
                        'regtype'       => 'course',
                        'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'    => 1,
                        'order_id'      => $order_id,
                        'external_id'   => $course['IdStudentRegistration'],
                        'expiry_date'   => $expiry_date,
                        'invoice_id'    => $invoice['invoice_id']
                    );
                    $enrolDb->insert($data);
                    $label = $course['SubjectName'];
    
                    $lms_enrolled++;
                    
                    //update sms
                    $dataUpdateLms = array(
                        'IdStudentRegSubjects' => $course['IdStudentRegSubjects'],
                        'lms'                  => 1,
                        'lms_date'             => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    );
                    Cms_Curl::__(SMS_API . '/get/Registration/do/updateRegisteredLms', $dataUpdateLms);
            
                }
            }
    
            $learning_mode = ($sms_curriculum['mode_of_program'] == 577 ? 'OL' : 'FTF');
            self::courseRegistrationEmail(array(
                'order_id'     => $order_id,
                'learningmode' => $learning_mode
            ));
            
            $response = array(
                'error'    => 0,
                'message'  => 'OK',
                'user_id'  => $lms_user_id,
                'order_id' => $order_id,
                'enrolled' => $lms_enrolled
            );
            
            if (empty($params))
            {
                die(json_encode($response));
            }
            
            return $response;
        }
        catch (Exception $e)
        {
            $error = $e->getMessage();
    
            $response = array(
                'error'    => 0,
                'message'  => $error,
            );
            if (empty($params))
            {
                die(json_encode($response));
            }
    
            return $response;
        }
    }

    protected static function processEnrollment($data, $batchId, $from)
    {

        $orderDb = new App_Model_Order();
        $currCoursesDb = new App_Model_CurriculumCourses();
        $curriculumDb = new App_Model_Curriculum();
        $paymentDb = new App_Model_PaymentHistory();
        $enrolDb = new App_Model_Enrol();
        $orderGroupUserDb = new App_Model_OrderGroupUser();
        $courseDB = new App_Model_Courses();
        $orderGroupDb = new App_Model_OrderGroup();
        $dbUser = new App_Model_User;
        $courseDB = new App_Model_Courses();

        $pass = 0;
        $desc = 'CORP';
        $paymentInfo = array();
        $db  = getDB();
        $db2 = getDB2();

        if($from == 2) {//sms
            $pass = 1;
            $desc = 'SMS';
            $paymentInfo['ol_type'] = '';
            $paymentInfo['mt_status'] = '';
        }else {

            $paramInvoice = array('batchId' => $batchId);

            $paymentInfo = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoiceBatch', $paramInvoice);
        }

        if ($paymentInfo['mt_status'] == 'SUCCESS' || $pass == 1) {

            //register
            $totalStudent = count($data);
            $ordergroup_id = 0;

            foreach ($data as $m => $reg) {

                if ($reg['std_idnumber']) {

                    $learningmode = $reg['learningmode'];
                    $schedule = null;

                    $IdStudentRegistration = $reg['IdStudentRegistration'];
                    $invoiceAmount = isset($paymentInfo['mt_amount'])?$paymentInfo['mt_amount']:0;
                    $invoiceId = isset($paymentInfo['id'])?$paymentInfo['id']:0;
                    $invoiceNo = isset($paymentInfo['bill_number'])?$paymentInfo['bill_number']:NULL;
                    $paymentMethod = ($paymentInfo['ol_type'] == 1) ? 'MIGS' : 'FPX';

                    $regtype = 'course';
                    $label = '';
                    if($from == 1) {//corporate
                        if ($reg['programme_type'] == 1037) {
                            $regtype = 'curriculum';
                        }
                        $label = $reg['ProgramName'];
                    }

                    // 1. add user
                    $salt = Cms_Common::generateRandomString(22);
                    $options = array('salt' => $salt);
                    $passwordTemp = 123456;
                    $hash = @password_hash($passwordTemp, PASSWORD_BCRYPT, $options);
                    $randToken = Cms_Common::generateRandomString(22);

                    //check exist idnumber/username
                    $userExist = $dbUser->getUserByUsername($reg['std_idnumber']);

                    if($userExist){
                        $user_id = $userExist['id'];
                    }else {

                        $dataUser = array(
                            'username' => $reg['std_idnumber'],
                            'password' => $hash,
                            'salt' => $salt,
                            'firstname' => $reg['std_fullname'],
                            'lastname' => '',
                            'email' => $reg['std_email'],
                            'contactno' => $reg['std_contact_number'],
                            'birthdate' => isset($reg['std_dob'])?$reg['std_dob']:date('Y-m-d'),
                            'nationality' => isset( $reg['nationality'])?$reg['nationality']:0,
                            'company' => isset($reg['std_corporate_id']) ? $reg['std_corporate_id'] : 0,
                            'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'role' => 'student',
                            'token' => $randToken,
                            'active' => 1,
                            'from' => 1,
                            'external_id' => @$reg['std_id'],
                        );
    
                        if (isset($reg['std_gender']))
                        {
                            $dataUser['gender'] = $reg['std_gender'];
                        }
    
                        if (isset($reg['std_religion']) && $reg['std_religion'] > 0)
                        {
                            $select = $db->select()
                                ->from(array('a' => 'external_data'))
                                ->join(array('b' => 'external_datatype'), 'b.id = a.type_id', array())
                                ->where('a.value = ?', $reg['std_religion'])
                                ->where('b.name = ?', 'Religion');
                            $result = $db->fetchRow($select);
        
                            if ($result)
                            {
                                $dataUser['religion'] = $result['id'];
                            }
                        }
    
                        if (isset($reg['std_race']) && $reg['std_race'] > 0)
                        {
                            $select = $db->select()
                                ->from(array('a' => 'external_data'))
                                ->join(array('b' => 'external_datatype'), 'b.id = a.type_id', array())
                                ->where('a.value = ?', $reg['std_race'])
                                ->where('b.name = ?', 'Race');
                            $result = $db->fetchRow($select);
        
                            if ($result)
                            {
                                $dataUser['race'] = $result['id'];
                            }
                        }
    
                        if (isset($reg['std_qualification']) && $reg['std_qualification'] > 0)
                        {
                            $select = $db->select()
                                ->from(array('a' => 'external_data'))
                                ->join(array('b' => 'external_datatype'), 'b.id = a.type_id', array())
                                ->where('a.code = ?', $reg['std_qualification'])
                                ->where('b.name = ?', 'Qualification');
                            $result = $db->fetchRow($select);
        
                            if ($result)
                            {
                                $dataUser['qualification'] = $result['id'];
                            }
                        }

                        $user_id = $dbUser->insert($dataUser);

                        $student_profile = array('std_username' => $reg['std_idnumber']);
                        $db2->update('student_profile', $student_profile, 'std_idnumber =' .$reg['std_idnumber']);

                        if($from == 1) {//corporate

                            //send email

                            $urlActivate = APP_URL . "/index/activate/c/1/key/" . $randToken;

                            $msg = "Hi $reg[std_fullname], <br /><br />

                                You have successfully enrolled in " . $label . "! <br /><br />

                                Kindly activate your account by following the link below :<br />" . $urlActivate . "<br /><br />
                                Your temporary password is 123456<br /><br />
                                Please reset your password after you login into the system.<br /><br />
                                If you have any questions or issues, please don't hesitate to reach out. <br /><br />

                                - " . APP_NAME . " Team";

                            $emailSubject = APP_NAME . ' - Registration - ' . $label;

                            //send email
                            //$mail = new Cms_SendMail();
                            //$mail->fnSendMail($reg['std_email'],$emailSubject , $msg);

                            $emailDb = new App_Model_Email();
                            $data = array(
                                'recepient_email' => $reg['std_email'],
                                'subject' => $emailSubject,
                                'content' => $msg,
                                'date_send' =>  date("Y-m-d H:i:s"),
                                'attachment_path' => '',
                                'attachment_filename' => '',
                                'smtp' => 1
                            );

                            //to send email with attachment
                            $emailDb->addData($data);

                            //send email cc to payment@ibfim.com
                            $mail = new Cms_SendMail();
                            $mail->fnSendMail('ibfim.staging@gmail.com',$emailSubject , $msg);
                            //$mail->fnSendMail('payment@ibfim.com',$emailSubject , $msg);
                            //send email cc to training@ibfim.com
                            //$mail = new Cms_SendMail();
                            //$mail->fnSendMail('training@ibfim.com',$emailSubject , $msg);

                        }

                    }

                    //2. add order
                    $orderData = array(
                        'user_id' => $user_id,
                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by' => 1, //admin
                        'amount' => $invoiceAmount,
                        'paymentmethod' => $paymentMethod,
                        'status' => 'ACTIVE',
                        'invoice_id' => $invoiceId,
                        'invoice_no' => $invoiceNo,
                        'invoice_external' => 1,
                        'regtype' => $regtype,
                        'item_id' => 0, //tbc munzir
                        'learningmode' => $learningmode,
                        'schedule' => $schedule,
                        'external_id' => $IdStudentRegistration,
                        'visibility' => 0
                    );

                    $order_id = $orderDb->insert($orderData);

                    /* self::processGuests*/
                    if ($m == 0) {
                        //add entry
                        $data = array(
                            'order_id' => $order_id,
                            'totaluser' => $totalStudent,
                            'description' => $desc,
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => 1,
                        );
                        $ordergroup_id = $orderGroupDb->insert($data);

                    } else {

                        $data = array(
                            'group_id' => $ordergroup_id,
                            'order_id' => $order_id,
                            'email' => $reg['std_email'],
                            'firstname' => $reg['std_fullname'],
                            'lastname' => '',
                            'token' => Cms_Common::generateRandomString(22),
                            'birthdate' => isset($reg['std_dob'])?$reg['std_dob']:date('Y-m-d'),
                            'nationality' => isset( $reg['nationality'])?$reg['nationality']:0,
                            'company' => $reg['std_corporate_id'],
                            'qualification' => $reg['std_qualification'],
                            'activated' => 0,

                        );

                        $orderGroupUserDb->insert($data);

                    }
                    /*end processGuests*/

                    /* 3.add enrol
                     * 3.1 add enrol program
                     * 3.2 add enrol courses
                     */

                    $program = $curriculumDb->getDataByExternal($reg['IdProgram']);

                    $curriculumId = 0;
                    if($program){
                        $curriculumId = $program['id'];
                    }


                    /* $courses = $currCoursesDb->getDataByExternal($reg['IdProgram']);
                     if ($courses) {
                         $curriculumId = isset($courses[0]['curriculum_id'])?$courses[0]['curriculum_id']:0;
                     }*/

                    $msg = '';
                    // email message for program
                    $msg = ($program) ? str_replace('[Program Name]', $program['name'], $msg) : str_replace('[Program Name]', '[course]', $msg);

                    //get registered course at sms
                    $courseRegistered = $reg['course'];

                    $IdCourseTaggingGroup = 0;
                    $learningMode = 'OL';
                    if($courseRegistered[0]['IdCourseTaggingGroup'] != 'OL'){
                        $IdCourseTaggingGroup = $courseRegistered[0]['IdCourseTaggingGroup'];
                        $learningMode = 'FTF';
                    }

                    //3.1
                    //check enrol existing

                    $curEnrol = $enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' =>
                        'curriculum', 'curriculum_id = ?' => $curriculumId));

                    if (empty($curEnrol)) {
                        $data = array(
                            'curriculum_id' => $curriculumId,
                            'course_id' => NULL,
                            'schedule' => NULL,
                            'learningmode' => $learningMode,
                            'user_id' => $user_id,
                            'regtype' => 'curriculum',
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => 1,
                            'order_id' => $order_id,
                            'external_id' => $courseRegistered[0]['IdStudentRegistration'],
                            'expiry_date' => NULL
                        );

                        $enrol_id = $enrolDb->insert($data);

                    }

                    //4.courses
                    foreach ($courseRegistered as $course) {
                        //get lms course id
                        $courseLms = $courseDB->getCoursesByExternalId($course['IdSubject']);

                        //access period
                        $expiry_date = $courseLms['accessperiod'] == null ? null : Plugins_App_Course::accessPeriodDate($courseLms['accessperiod']);

                        $curEnrolCourse = $enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' => 'course', 'curriculum_id = ?' => $curriculumId, 'course_id = ?' => $courseLms['id']));

                        if (empty($curEnrolCourse)) {
                            $data = array(
                                'curriculum_id' => $curriculumId,
                                'course_id' => $courseLms['id'],
                                'schedule' => $IdCourseTaggingGroup,
                                'learningmode' => $learningMode,
                                'user_id' => $user_id,
                                'regtype' => 'course',
                                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by' => 1,
                                'order_id' => $order_id,
                                'external_id' => $course['IdStudentRegistration'],
                                'expiry_date' => $expiry_date
                            );

                            $enrolDb->insert($data);

                            $label = $course['SubjectName'];

                            //update sms
                            $dataUpdateLms = array(
                                'IdStudentRegSubjects' => $course['IdStudentRegSubjects'],
                                'lms' => 1,
                                'lms_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            );
                            Cms_Curl::__(SMS_API . '/get/Registration/do/updateRegisteredLms', $dataUpdateLms);

                        }
                    }

                    //auto email
                    self::courseRegistrationEmail(array('order_id'=>$order_id,'learningmode'=>$learningMode));

                }
            }
        }
    }

    public static function addEnrolCorporateInhouse()
    {
        parent::getPost();
        $post = self::$post;

        //check required parameters
        self::checkParams(array('batchId'), $post);

        $batchId = $post['batchId'];
        $from = isset($post['from'])?$post['from']:1; //2=sms,1=corporate
        $inhouse = isset($post['inhouse'])?1:0;
        $sendemail = isset($post['email'])?1:0; //1= no need to send email, 0=send email
        //pr($from);exit;
        $paramsRegistration = array(
            'batchId' => $batchId
        );

        $regInfo = Cms_Curl::__(SMS_API . '/get/Registration/do/getRegistrationInfo', $paramsRegistration);

        if ($regInfo) {
            self::processEnrollmentInhouse($regInfo, $batchId, $from, 1, $sendemail);
        } else {

            die();
        }
    }

    protected static function processEnrollmentInhouse($data, $batchId, $from)
    {

        $orderDb = new App_Model_Order();
        $currCoursesDb = new App_Model_CurriculumCourses();
        $curriculumDb = new App_Model_Curriculum();
        $paymentDb = new App_Model_PaymentHistory();
        $enrolDb = new App_Model_Enrol();
        $orderGroupUserDb = new App_Model_OrderGroupUser();
        $courseDB = new App_Model_Courses();
        $orderGroupDb = new App_Model_OrderGroup();
        $dbUser = new App_Model_User;
        $courseDB = new App_Model_Courses();

        $pass = 0;
        $desc = 'CORP';
        $paymentInfo = array();
        $db  = getDB();
        $db2 = getDB2();

        if($from == 2) {//sms
            $pass = 1;
            $desc = 'SMS';
            $paymentInfo['ol_type'] = '';
            $paymentInfo['mt_status'] = '';
        }else {

            $paramInvoice = array('batchId' => $batchId);

            $paymentInfo = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoiceBatch', $paramInvoice);
        }

        if ($paymentInfo['mt_status'] == 'SUCCESS' || $pass == 1) {

            //register
            $totalStudent = count($data);
            $ordergroup_id = 0;

            foreach ($data as $m => $reg) {

                if ($reg['std_idnumber']) {

                    $learningmode = $reg['learningmode'];
                    $schedule = null;

                    $IdStudentRegistration = $reg['IdStudentRegistration'];
                    $invoiceAmount = isset($paymentInfo['mt_amount'])?$paymentInfo['mt_amount']:0;
                    $invoiceId = isset($paymentInfo['id'])?$paymentInfo['id']:0;
                    $invoiceNo = isset($paymentInfo['bill_number'])?$paymentInfo['bill_number']:NULL;
                    $paymentMethod = ($paymentInfo['ol_type'] == 1) ? 'MIGS' : 'FPX';

                    $regtype = 'course';
                    $label = '';
                    if($from == 1) {//corporate
                        if ($reg['programme_type'] == 1037) {
                            $regtype = 'curriculum';
                        }
                        $label = $reg['ProgramName'];
                    }

                    // 1. add user
                    $salt = Cms_Common::generateRandomString(22);
                    $options = array('salt' => $salt);
                    $passwordTemp = 123456;
                    $hash = @password_hash($passwordTemp, PASSWORD_BCRYPT, $options);
                    $randToken = Cms_Common::generateRandomString(22);

                    //check exist idnumber/username
                    $userExist = $dbUser->getUserByUsername($reg['std_idnumber']);

                    if($userExist){
                        $user_id = $userExist['id'];
                    }else {

                        $dataUser = array(
                            'username' => $reg['std_idnumber'],
                            'password' => $hash,
                            'salt' => $salt,
                            'firstname' => $reg['std_fullname'],
                            'lastname' => '',
                            'email' => $reg['std_email'],
                            'contactno' => $reg['std_contact_number'],
                            'birthdate' => isset($reg['std_dob'])?$reg['std_dob']:date('Y-m-d'),
                            'nationality' => isset( $reg['nationality'])?$reg['nationality']:0,
                            'company' => isset($reg['std_corporate_id']) ? $reg['std_corporate_id'] : 0,
                            'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'role' => 'student',
                            'token' => $randToken,
                            'active' => 1,
                            'from' => 1,
                            'external_id' => @$reg['std_id'],
                        );
    
                        if (isset($reg['std_gender']))
                        {
                            $dataUser['gender'] = $reg['std_gender'];
                        }
    
                        if (isset($reg['std_religion']) && $reg['std_religion'] > 0)
                        {
                            $select = $db->select()
                                ->from(array('a' => 'external_data'))
                                ->join(array('b' => 'external_datatype'), 'b.id = a.type_id', array())
                                ->where('a.value = ?', $reg['std_religion'])
                                ->where('b.name = ?', 'Religion');
                            $result = $db->fetchRow($select);
        
                            if ($result)
                            {
                                $dataUser['religion'] = $result['id'];
                            }
                        }
    
                        if (isset($reg['std_race']) && $reg['std_race'] > 0)
                        {
                            $select = $db->select()
                                ->from(array('a' => 'external_data'))
                                ->join(array('b' => 'external_datatype'), 'b.id = a.type_id', array())
                                ->where('a.value = ?', $reg['std_race'])
                                ->where('b.name = ?', 'Race');
                            $result = $db->fetchRow($select);
        
                            if ($result)
                            {
                                $dataUser['race'] = $result['id'];
                            }
                        }
    
                        if (isset($reg['std_qualification']) && $reg['std_qualification'] > 0)
                        {
                            $select = $db->select()
                                ->from(array('a' => 'external_data'))
                                ->join(array('b' => 'external_datatype'), 'b.id = a.type_id', array())
                                ->where('a.code = ?', $reg['std_qualification'])
                                ->where('b.name = ?', 'Qualification');
                            $result = $db->fetchRow($select);
        
                            if ($result)
                            {
                                $dataUser['qualification'] = $result['id'];
                            }
                        }

                        $user_id = $dbUser->insert($dataUser);

                        $student_profile = array('std_username' => $reg['std_idnumber']);
                        $db2->update('student_profile', $student_profile, 'std_idnumber =' .$reg['std_idnumber']);

                        //if($from == 1) {//corporate

                            //send email

                            $urlActivate = APP_URL . "/index/activate/c/1/key/" . $randToken;

                            $msg = "Hi $reg[std_fullname], <br /><br />

                                You have successfully enrolled in " . $label . "! <br /><br />

                                Kindly activate your account by following the link below :<br />" . $urlActivate . "<br /><br />
                                Your temporary password is 123456<br /><br />
                                Please reset your password after you login into the system.<br /><br />
                                If you have any questions or issues, please don't hesitate to reach out. <br /><br />

                                - " . APP_NAME . " Team";

                            $emailSubject = APP_NAME . ' - Registration - ' . $label;

                            //send email
                            $mail = new Cms_SendMail();
                            $mail->fnSendMail($reg['std_email'],$emailSubject , $msg);

                            $emailDb = new App_Model_Email();
                            $data = array(
                                'recepient_email' => $reg['std_email'],
                                'subject' => $emailSubject,
                                'content' => $msg,
                                'date_send' =>  date("Y-m-d H:i:s"),
                                'attachment_path' => '',
                                'attachment_filename' => '',
                                'smtp' => 1
                            );

                            //to send email with attachment
                            $emailDb->addData($data);


                        //}

                    }

                    //2. add order
                    $orderData = array(
                        'user_id' => $user_id,
                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by' => 1, //admin
                        'amount' => $invoiceAmount,
                        'paymentmethod' => $paymentMethod,
                        'status' => 'ACTIVE',
                        'invoice_id' => $invoiceId,
                        'invoice_no' => $invoiceNo,
                        'invoice_external' => 1,
                        'regtype' => $regtype,
                        'item_id' => 0, //tbc munzir
                        'learningmode' => $learningmode,
                        'schedule' => $schedule,
                        'external_id' => $IdStudentRegistration,
                        'visibility' => 0
                    );

                    $order_id = $orderDb->insert($orderData);

                    /* self::processGuests*/
                    if ($m == 0) {
                        //add entry
                        $data = array(
                            'order_id' => $order_id,
                            'totaluser' => $totalStudent,
                            'description' => $desc,
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => 1,
                        );
                        $ordergroup_id = $orderGroupDb->insert($data);

                    } else {

                        $data = array(
                            'group_id' => $ordergroup_id,
                            'order_id' => $order_id,
                            'email' => $reg['std_email'],
                            'firstname' => $reg['std_fullname'],
                            'lastname' => '',
                            'token' => Cms_Common::generateRandomString(22),
                            'birthdate' => isset($reg['std_dob'])?$reg['std_dob']:date('Y-m-d'),
                            'nationality' => isset( $reg['nationality'])?$reg['nationality']:0,
                            'company' => $reg['std_corporate_id'],
                            'qualification' => $reg['std_qualification'],
                            'activated' => 0,

                        );

                        $orderGroupUserDb->insert($data);

                    }
                    /*end processGuests*/

                    /* 3.add enrol
                     * 3.1 add enrol program
                     * 3.2 add enrol courses
                     */

                    $program = $curriculumDb->getDataByExternal($reg['IdProgram']);

                    $curriculumId = 0;
                    if($program){
                        $curriculumId = $program['id'];
                    }


                    /* $courses = $currCoursesDb->getDataByExternal($reg['IdProgram']);
                     if ($courses) {
                         $curriculumId = isset($courses[0]['curriculum_id'])?$courses[0]['curriculum_id']:0;
                     }*/

                    $msg = '';
                    // email message for program
                    $msg = ($program) ? str_replace('[Program Name]', $program['name'], $msg) : str_replace('[Program Name]', '[course]', $msg);

                    //get registered course at sms
                    $courseRegistered = $reg['course'];

                    $IdCourseTaggingGroup = 0;
                    $learningMode = 'OL';
                    if($courseRegistered[0]['IdCourseTaggingGroup'] != 'OL'){
                        $IdCourseTaggingGroup = $courseRegistered[0]['IdCourseTaggingGroup'];
                        $learningMode = 'FTF';
                    }

                    //3.1
                    //check enrol existing

                    $curEnrol = $enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' =>
                        'curriculum', 'curriculum_id = ?' => $curriculumId));

                    if (empty($curEnrol)) {
                        $data = array(
                            'curriculum_id' => $curriculumId,
                            'course_id' => NULL,
                            'schedule' => NULL,
                            'learningmode' => $learningMode,
                            'user_id' => $user_id,
                            'regtype' => 'curriculum',
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => 1,
                            'order_id' => $order_id,
                            'external_id' => $courseRegistered[0]['IdStudentRegistration'],
                            'expiry_date' => NULL
                        );

                        $enrol_id = $enrolDb->insert($data);

                    }

                    //4.courses
                    foreach ($courseRegistered as $course) {
                        //get lms course id
                        $courseLms = $courseDB->getCoursesByExternalId($course['IdSubject']);

                        //access period
                        $expiry_date = $courseLms['accessperiod'] == null ? null : Plugins_App_Course::accessPeriodDate($courseLms['accessperiod']);

                        $curEnrolCourse = $enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' => 'course', 'curriculum_id = ?' => $curriculumId, 'course_id = ?' => $courseLms['id']));

                        if (empty($curEnrolCourse)) {
                            $data = array(
                                'curriculum_id' => $curriculumId,
                                'course_id' => $courseLms['id'],
                                'schedule' => $IdCourseTaggingGroup,
                                'learningmode' => $learningMode,
                                'user_id' => $user_id,
                                'regtype' => 'course',
                                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by' => 1,
                                'order_id' => $order_id,
                                'external_id' => $course['IdStudentRegistration'],
                                'expiry_date' => $expiry_date
                            );

                            $enrolDb->insert($data);

                            $label = $course['SubjectName'];

                            //update sms
                            $dataUpdateLms = array(
                                'IdStudentRegSubjects' => $course['IdStudentRegSubjects'],
                                'lms' => 1,
                                'lms_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            );
                            Cms_Curl::__(SMS_API . '/get/Registration/do/updateRegisteredLms', $dataUpdateLms);

                        }
                    }

                    //auto email
                    self::courseRegistrationEmail(array('order_id'=>$order_id,'learningmode'=>$learningMode));

                }
            }
        }
    }

    protected static function processEnrollmentInhouseold($data, $batchId, $from, $inhouse=0, $sendemail)
    {

        $orderDb = new App_Model_Order();
        $currCoursesDb = new App_Model_CurriculumCourses();
        $paymentDb = new App_Model_PaymentHistory();
        $enrolDb = new App_Model_Enrol();
        $orderGroupUserDb = new App_Model_OrderGroupUser();
        $courseDB = new App_Model_Courses();
        $orderGroupDb = new App_Model_OrderGroup();
        $dbUser = new App_Model_User;
        $courseDB = new App_Model_Courses();
        $curriculumDb = new App_Model_Curriculum();
        $smsExternalDb = new App_Model_SmsExternal();



        $pass = 0;
        $desc = 'CORP';
        $paymentInfo = array();

        if($from == 2 || $inhouse == 1) {//sms
            $pass = 1;
            $desc = 'SMS' . (($inhouse == 1) ? " - inhouse" : "");
            $paymentInfo['ol_type'] = '';
            $paymentInfo['mt_status'] = '';
        }else {

            $paramInvoice = array('batchId' => $batchId);

            $paymentInfo = Cms_Curl::__(SMS_API . '/get/Finance/do/getInvoiceBatch', $paramInvoice);
        }

        if ($paymentInfo['mt_status'] == 'SUCCESS' || $pass == 1) {

            //register
            $totalStudent = count($data);
            $ordergroup_id = 0;



            foreach ($data as $m => $reg) {

                if ($reg['std_idnumber']) {

                    $learningmode = $reg['learningmode'];
                    $schedule = null;

                    $IdStudentRegistration = $reg['IdStudentRegistration'];
                    $invoiceAmount = isset($paymentInfo['mt_amount']) ? $paymentInfo['mt_amount'] : 0;
                    $invoiceId = isset($paymentInfo['id']) ? $paymentInfo['id'] : 0;
                    $invoiceNo = isset($paymentInfo['bill_number']) ? $paymentInfo['bill_number'] : NULL;
                    $paymentMethod = ($paymentInfo['ol_type'] == 1) ? 'MIGS' : 'FPX';

                    $regtype = 'course';
                    $courses_list = '';
                    $label = '';
                    if ($from == 1) {//corporate
                        if ($reg['programme_type'] == 1037) {
                            $regtype = 'curriculum';
                            $label = $reg['ProgramName'];
                            $courses = $reg['ProgramName'] . "<br />";
                        }
                    }

                    // 1. add user
                    $salt = Cms_Common::generateRandomString(22);
                    $options = array('salt' => $salt);
                    $passwordTemp = 123456;
                    $hash = @password_hash($passwordTemp, PASSWORD_BCRYPT, $options);
                    $randToken = Cms_Common::generateRandomString(22);


                    //check exist idnumber/username
                    $userExist = $dbUser->getUserByUsername($reg['std_idnumber']);

                    if ($userExist) {

                        // email message to existing user
                        $msg = self::getCommunicationTemplate(1);
                        $msg = str_replace('[name]', $userExist['firstname'] . " " . $userExist['lastname'], $msg);
                        $msg = str_replace('[username]', $userExist['username'], $msg);
                        $msg = str_replace('[password]', '<b>Your current password</br>', $msg);

                        $user_id = $userExist['id'];


                    } else {


                        $dataUser = array(
                            'username' => $reg['std_idnumber'],
                            'password' => $hash,
                            'salt' => $salt,
                            'firstname' => $reg['std_fullname'],
                            'lastname' => '',
                            'email' => $reg['std_email'],
                            'contactno' => $reg['std_contact_number'],
                            'birthdate' => isset($reg['std_dob']) ? $reg['std_dob'] : date('Y-m-d'),
                            'nationality' => isset($reg['nationality']) ? $reg['nationality'] : 0,
                            'qualification' => $reg['std_qualification'],
                            'company' => isset($reg['std_corporate_id']) ? $reg['std_corporate_id'] : 0,
                            'register_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'role' => 'student',
                            'token' => $randToken,
                            'active' => 1,
                            'from' => 1,
                            'external_id' => $IdStudentRegistration,

                        );

                        $user_id = $dbUser->insert($dataUser);

                    }

                    //2. add order
                    $orderData = array(
                        'user_id' => $user_id,
                        'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by' => 1, //admin
                        'amount' => $invoiceAmount,
                        'paymentmethod' => $paymentMethod,
                        'status' => 'ACTIVE',
                        'invoice_id' => $invoiceId,
                        'invoice_no' => $invoiceNo,
                        'invoice_external' => 1,
                        'regtype' => $regtype,
                        'item_id' => 0, //tbc munzir
                        'learningmode' => $learningmode,
                        'schedule' => $schedule,
                        'external_id' => $IdStudentRegistration,
                        'visibility' => 0
                    );

                    $order_id = $orderDb->insert($orderData);

                    /* self::processGuests*/
                    if ($m == 0) {
                        //add entry
                        $data = array(
                            'order_id' => $order_id,
                            'totaluser' => $totalStudent,
                            'description' => $desc,
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => 1,
                        );
                        $ordergroup_id = $orderGroupDb->insert($data);

                    } else {

                        $data = array(
                            'group_id' => $ordergroup_id,
                            'order_id' => $order_id,
                            'email' => $reg['std_email'],
                            'firstname' => $reg['std_fullname'],
                            'lastname' => '',
                            'token' => Cms_Common::generateRandomString(22),
                            'birthdate' => isset($reg['std_dob']) ? $reg['std_dob'] : date('Y-m-d'),
                            'nationality' => isset($reg['nationality']) ? $reg['nationality'] : 0,
                            'company' => $reg['std_corporate_id'],
                            'qualification' => $reg['std_qualification'],
                            'activated' => 0,

                        );

                        $orderGroupUserDb->insert($data);

                    }
                    /*end processGuests*/

                    //3.add enrol
                    $courses = $currCoursesDb->getDataByExternal($reg['IdProgram']);
                    $curriculumId = 0;
                    if ($courses) {
                        $curriculumId = isset($courses[0]['curriculum_id']) ? $courses[0]['curriculum_id'] : 0;
                    } else {

                        //add inhouse curriculum
                        //get programinfo
                        $smsProgram = $smsExternalDb->getProgramInfo($reg['IdProgram']);

                        $accessperiod = '3W';//$formData['accessperiod_opt'] == '1' ? $formData['accessperiod_num'].$formData['accessperiod_type'] : null;

                        $dataAdd = array(
                            'name' => $smsProgram['ProgramName'],
                            'parent_id' => 0,
                            'display_only' => intval(0),
                            'code' => $smsProgram['ProgramCode'],
                            'description' => 'Inhouse',
                            'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by' => 1,
                            'visibility' => 0,
                            'excerpt' => NULL,
                            'accessperiod' => $accessperiod,
                            'allow_course_registration' => 0,
                            'inhouse' => 1,
                            'external_id' => $reg['IdProgram'],
                        );

                        $curriculumId = $curriculumDb->insert($dataAdd);

                        $courses = $dataAdd;

                    }


                    //get registered course at sms
                    $courseRegistered = $reg['course'];

                    //4.courses
                    foreach ($courseRegistered as $course) {
                        //get lms course id
                        $courseLms = $courseDB->getCoursesByExternalId($course['IdSubject']);

                        if (!$courseLms) {
                            //add inhouse course

                            $smsCourse = $smsExternalDb->getCourseInfo($course['IdSubject']);

                            $dataCourseAdd = array(
                                'title' => $smsCourse['SubjectName'],
                                'description' => $smsCourse['courseDescription'],
                                'welcome' => $smsCourse['welcome'],
                                'excerpt' => NULL,
                                'category_id' => NULL,
                                'code' => $smsCourse['SubCode'],
                                'code_safe' => Cms_Common::safename($smsCourse['SubCode']),
                                'visibility' => 0,
                                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by' => 1,
                                'inhouse' => 1,
                                'external_id' => $course['IdSubject'],
                                'use_forum' => 0,
                                'learning_online' => 0,
                                'learning_ftf' => 0
                            );

                            $courseLms['id'] = $courseDB->insert($dataCourseAdd);

                            //add curriculum course

                            $dataCourseCurAdd = array(
                                'curriculum_id' => $curriculumId,
                                'course_id' => $courseLms['id'],
                                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by' => 1,
                            );

                            $currCoursesDb->insert($dataCourseCurAdd);

                        }

                        $IdCourseTaggingGroup = 0;
                        $learningMode = 'OL';
                        if ($course['IdCourseTaggingGroup'] != 'OL') {
                            $IdCourseTaggingGroup = $course['IdCourseTaggingGroup'];
                            $learningMode = 'FTF';
                        }

                        //3.1
                        //check enrol existing

                        $curEnrol = $enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' =>
                            'curriculum', 'curriculum_id = ?' => $curriculumId));
                        //pr($curEnrol);
                        if (empty($curEnrol)) {
                            $data = array(
                                'curriculum_id' => $curriculumId,
                                'course_id' => NULL,
                                'schedule' => NULL,
                                'learningmode' => $learningMode,
                                'user_id' => $user_id,
                                'regtype' => 'curriculum',
                                'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                'created_by' => 1,
                                'order_id' => $order_id,
                                'external_id' => $courseRegistered[0]['IdStudentRegistration'],
                                'expiry_date' => NULL
                            );

                            $enrolDb->insert($data);
                        }

                        //4.courses
                        foreach ($courseRegistered as $courseregistered) {
                            //get lms course id
                            echo('elo');
                            $courseLms = $courseDB->getCoursesByExternalId($courseregistered['IdSubject']);
                            //pr($courseLms);exit;

                            //access period
                            $expiry_date = $courseLms['accessperiod'] == null ? null : Plugins_App_Course::accessPeriodDate($courseLms['accessperiod']);

                            $curEnrolCourse = $enrolDb->getEnrol(array('user_id = ?' => $user_id, 'regtype = ?' => 'course', 'curriculum_id = ?' => $curriculumId, 'course_id = ?' => $courseLms['id']));

                            if (empty($curEnrolCourse)) {
                                $data = array(
                                    'curriculum_id' => $curriculumId,
                                    'course_id' => $courseLms['id'],
                                    'schedule' => $IdCourseTaggingGroup,
                                    'learningmode' => $learningMode,
                                    'user_id' => $user_id,
                                    'regtype' => 'course',
                                    'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                    'created_by' => 1,
                                    'order_id' => $order_id,
                                    'external_id' => $course['IdStudentRegistration'],
                                    'expiry_date' => $expiry_date
                                );

                                $enrolDb->insert($data);

                                $label = $course['SubjectName'];
                                $courses_list = $course['SubjectName'];

                                if ($course['lms'] == 0 && $sendemail == 1) {

                                    // email message to new user
                                    $msg = self::getCommunicationTemplate(2);
                                    $msg = str_replace('[name]', $reg['std_fullname'], $msg);
                                    $msg = str_replace('[username]', $reg['std_idnumber'], $msg);
                                    $msg = str_replace('[password]', '123456', $msg);

                                    //send email
                                    $msg = str_replace('[Program Name]', $courses['name'], $msg);

                                    //$template = self::getCommunicationTemplate(18);       // html content email
                                    //$title = self::getCommunicationTemplate(18, true);    // title

                                    $title = APP_NAME . ' - In-House Registration';
                                    $emailDb = new App_Model_Email();
                                    $data = array(
                                        'recepient_email' => $reg['std_email'],
                                        'subject' => $title,
                                        'content' => $msg,
                                        'date_send' => date("Y-m-d H:i:s"),
                                        'attachment_path' => '',
                                        'attachment_filename' => '',
                                        'smtp' => 1
                                    );

                                    //to send email with attachment
                                    $eqa_emailque_id = $emailDb->addData($data);

                                    $mail = new Cms_SendMail();
                                    $mail->fnSendMail($reg['std_email'], $title, $msg);
                                }


                                //update sms
                                $dataUpdateLms = array(
                                    'IdStudentRegSubjects' => $course['IdStudentRegSubjects'],
                                    'lms' => 1,
                                    'lms_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                                );
                                Cms_Curl::__(SMS_API . '/get/Registration/do/updateRegisteredLms', $dataUpdateLms);

                            }
                        }
                    }
                }
            }
        }
    }

    public static function getCommunicationTemplate($type=1, $title=false)
    {
        $params = array(
                'batchId'   => 1,
                'type'      => $type
            );

        $regInfo = Cms_Curl::__(SMS_API . '/get/Registration/do/getCommunicationTemplate', $params);
        $template = ($regInfo);

        $msg = $template['tpl_content'];
        $msg = str_replace('[URL]', APP_URL, $msg);

        if ($title) {
            return $template['tpl_name'];
        }
        return $msg;
    }

     public static function sendMailerexamresult($er_id) {
        $erDB = new App_Model_Exam_ExamRegistration();
        $examReg = $erDB->getDataEr($er_id);

            foreach($examReg as $a=>$ex){
                    $examReg[$a]['curr'] = $invoice = $erDB->getInvoiceMain($ex['order_id']);
                }

            $auth = Zend_Auth::getInstance();
            $uemail = 'ibfim.staging@gmail.com';
            $params = array(
                'type'      => 20
                );
            // email message to new user
            $msg = Cms_Curl::__(SMS_API . '/get/Registration/do/getCommunicationTemplate', $params);

                $fullname = $examReg[0]['std_fullname'];
                $email = $uemail ;
                $examresultslip = APP_URL.'/exam/result-sliplms/id/'.$examReg[0]['er_id'] ;

                $NotiExam = array(
                        'name'   => $fullname,
                        'email address'   => $email,
                        'examresultslip'   => $examresultslip,
                        'Email Date'      => new Zend_Date()
                        );

            // email message to existing user
                $msg = str_replace('[Name]', $NotiExam['name'] , $msg);
                $msg = str_replace('[examresultslip]', $NotiExam['examresultslip'], $msg);
                $msg = str_replace('[Email Date]', $NotiExam['Email Date'],$msg);
            //send email
            $mail = new Cms_SendMail();
            $mail->fnSendMail($email, $msg['tpl_name'],$msg['tpl_content']);


             $emailDb = new App_Model_Email();
             $data = array(
                 'recepient_email' => $email,
                 'subject' => $msg['tpl_name'],
                 'content' => $msg['tpl_content'],
                 'date_send' =>  date("Y-m-d H:i:s"),
                 'attachment_path' => '',
                 'attachment_filename' => ''
             );

             //to send email with attachment
             $emailDb->addData($data);
        }

    public static function processExemption() {
        parent::getPost();
        $post = self::$post;

        self::checkParams(array('exemption_id'), $post);
        $exemption_id = $post['exemption_id'];

        $auth     = Zend_Auth::getInstance();
        $userDB   = new App_Model_User();
        $courseDB = new App_Model_Courses();
        $enrolDB  = new App_Model_Enrol();
        $orderDB  = new App_Model_Order();
        $erDB     = new App_Model_Exam_ExamRegistration();
        $icampus_Plugin_Finance = new icampus_Plugin_Finance();

        $db        = getDB2();
        $select    = $db->select()->from(array('a' => 'exemption_main'))->where('a.id = ?', $exemption_id);
        $exemption = $db->fetchRow($select);

        $select = $db->select()
            ->from(array('a' => 'exemption_item'))
            ->join(array('b' => 'tbl_studentregistration'), 'b.IdStudentRegistration = a.IdStudentRegistration', array())
            ->join(array('c' => 'tbl_program'), 'c.IdProgram = b.IdProgram', array('ProgramName', 'ProgramCode'))
            ->joinLeft(array('d1' => 'tbl_studentregsubjects'),'d1.IdStudentRegSubjects = a.IdStudentRegSubjects', array('IdCourseTaggingGroup'))
            ->joinLeft(array('d2' => 'tbl_subjectmaster'), 'd2.IdSubject = d1.IdSubject', array('IdSubject', 'SubCode', 'SubjectName'))
            ->joinLeft(array('d3' => 'course_group_schedule'), 'd3.sc_id = d1.IdCourseTaggingGroup', array('sc_date_start', 'sc_date_end', 'sc_duration', 'sc_start_time', 'sc_end_time'))
            ->joinLeft(array('e1' => 'exam_registration'), 'e1.er_id = a.examregistration_id', array('er_id', 'examschedule_id'))
            ->joinLeft(array('e2' => 'exam_schedule'), 'e2.es_id = e1.examschedule_id',  array('es_date'))
            ->joinLeft(array('e4' => 'exam_setup_detail'), 'e4.esd_id = e1.examsetupdetail_id', array())
            ->joinLeft(array('e5' => 'programme_exam'), 'e5.pe_id = e4.esd_pe_id', array('pe_name'))

            ->join(array('e6' => 'exam_scheduletaggingslot'), 'e6.sts_schedule_id = e2.es_id', array('schedule_tagging_slot_id' => 'sts_id'))
            ->join(array('e7' => 'exam_slot'), 'e7.sl_id = e6.sts_slot_id', array('exam_slot_id' => 'sl_id', 'sl_name', 'sl_starttime', 'sl_endtime', 'es_start_time' => 'sl_starttime', 'es_end_time' => 'sl_endtime'))
            ->join(array('e8' => 'exam_taggingslotcenterroom'), 'e8.tsc_slotid = tsc_examcenterid', array('tagging_slot_center_room_id' => 'tsc_id'))
            ->join(array('e9' => 'tbl_exam_center'), 'e9.ec_id =  e8.tsc_examcenterid', array('exam_center_id' => 'ec_id', 'ec_name'))

            ->where('a.exemption_main_id  = ?', $exemption_id)
            ->order('a.type')
            ->order('a.registration_type');
        $items = $db->fetchAll($select);

        $std_id  = $exemption['std_id'];
        $student = $userDB->getUser($std_id, 'external_id');
        $user_id = $student['id'];
        unset($student['password']);

        $module_invoices = array();
        $exam_invoices   = array();

        foreach($items as $row) {
            $is_module = ($row['registration_type'] == 1 ? true : false);
            $is_award  = ($row['type'] == 1 ? true : false);

            if($is_module) {
                $IdSubject    = $row['IdSubject'];
                $course       = $courseDB->getCoursesByExternalId($IdSubject);
                $course_id    = $course['id'];

                $learningmode = ($row['IdCourseTaggingGroup'] == 'OL' ? 'OL' : 'FTF');
                $schedule     = ($row['IdCourseTaggingGroup'] == 'OL' ? 0 : $row['IdCourseTaggingGroup']);

                $invoice_id   = null;
                $order_id     = null;

                if(!$is_award) {

                    $IdStudentRegistration = $row['IdStudentRegistration'];

                    if(!isset($module_invoices[$IdStudentRegistration])) {

                        $proforma_invoice = $icampus_Plugin_Finance->generateProformaIndividual($IdSubject, $IdStudentRegistration);
                        $invoice          = $icampus_Plugin_Finance->generateInvoice($proforma_invoice['id']);
                        $invoice_id       = $invoice['invoiceId'];

                        $order = array(
                            'user_id'               => $user_id,
                            'created_date'          => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                            'created_by'            => 1,
                            'amount'                => $invoice['amount'],
                            'paymentmethod'         => 'MIGS',
                            'status'                => 'PENDING',
                            'invoice_id'            => $invoice_id,
                            'invoice_no'            => $invoice['invoiceNo'],
                            'invoice_external'      => 1,
                            'regtype'               => 'course',
                            'curriculum_id'         => 0,
                            'item_id'               => $course_id,
                            'learningmode'          => $learningmode,
                            'schedule'              => $schedule,
                            'external_id'           => $row['IdStudentRegistration']
                        );
                        $order_id = $orderDB->insert($order);

                        $invoice_data = array(
                            'invoice_id' => $invoice_id,
                            'order_id'   => $order_id,
                            'amount'     => $invoice['amount']
                        );

                        $module_invoices[$IdStudentRegistration] = $invoice_data;
                    }
                    else {
                        $invoice_id = $module_invoices[$IdStudentRegistration]['invoice_id'];
                        $order_id   = $module_invoices[$IdStudentRegistration]['order_id'];
                    }
                }

                $active = ($is_award ? 0 : 1);

                if($is_award) {
                    $learningmode = 'EXEMPTION';
                }

                $enrol = array(
                    'user_id'        => $user_id,
                    'course_id'      => $course_id,
                    'batch_id'       => 0,
                    'curriculum_id'  => 0,
                    'regtype'        => 'course',
                    'created_date'   => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'created_by'     => 1,
                    'haspayment'     => 0,
                    'completed'      => ($is_award ? 1 : 0),
                    'completed_date' => ($is_award ? new Zend_Db_Expr('UTC_TIMESTAMP()') : null),
                    'invoice_id'     => $invoice_id,
                    'order_id'       => $order_id,
                    'learningmode'   => $learningmode,
                    'schedule'       => $schedule,
                    'external_id'    => $row['IdStudentRegSubjects'],
                    'expiry_date'    => null,
                    'active'         => $active
                );
                $enrolDB->insert($enrol);
            }
            elseif(!$is_module && !$is_award) { //exam + enrol
                $IdStudentRegistration = $row['IdStudentRegistration'];

                $invoice_id = null;
                $order_id   = null;

                if(!isset($exam_invoices[$IdStudentRegistration])) {
                    $exam_registration = $erDB->getData($row['examregistration_id']);
                    $er_main_id        = $exam_registration['er_main_id'];
                    $schedule          = $exam_registration['examschedule_id'];
                    $categoryId        = ($student['nationality'] == 'MY' ? 579 : 580);

                    $proforma_invoice = $icampus_Plugin_Finance->generateProformaIndividual(0, $IdStudentRegistration, $categoryId, 1, 879, $er_main_id);
                    $invoice          = $icampus_Plugin_Finance->generateInvoice($proforma_invoice['id']);
                    $invoice_id       = $invoice['invoiceId'];

                    $order = array(
                        'user_id'               => $user_id,
                        'created_date'          => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                        'created_by'            => 1,
                        'amount'                => $invoice['amount'],
                        'paymentmethod'         => 'MIGS',
                        'status'                => 'PENDING',
                        'invoice_id'            => $invoice_id,
                        'invoice_no'            => $invoice['invoiceNo'],
                        'invoice_external'      => 1,
                        'regtype'               => 'course',
                        'curriculum_id'         => 0,
                        'item_id'               => 0,
                        'learningmode'          => 'OL',
                        'schedule'              => $schedule,
                        'external_id'           => $IdStudentRegistration
                    );
                    $order_id = $orderDB->insert($order);

                    $invoice_data = array(
                        'invoice_id' => $invoice_id,
                        'order_id'   => $order_id,
                        'amount'     => $invoice['amount']
                    );

                    $exam_invoices[$IdStudentRegistration] = $invoice_data;
                }
                else {
                    $invoice_id = $exam_invoices[$IdStudentRegistration]['invoice_id'];
                    $order_id   = $exam_invoices[$IdStudentRegistration]['order_id'];
                }

                $db->update('exam_registration', array('order_id' => $order_id), 'er_id = '. $exam_registration['er_id']);
            }
        }

        $response = array(
            'success' => 1,
            'msg'     => null
        );

        echo json_encode($response); exit;
    }

    public static function updateEnrolStatus($params = array()) {
        parent::getPost();
        $post = self::$post;

        self::checkParams(array('external_id', 'regtype'), $post);

        $external_id   = $post['external_id'];
        $regtype       = $post['regtype'];
        $set_completed = (isset($post['set_completed']) ? $post['set_completed'] : 0);

        $enrolDb = new App_Model_Enrol();
        $enrol   = $enrolDb->getEnrol(array(
            'external_id = ?' => $external_id,
            'regtype = ?'     => $regtype
        ));

        if (!$enrol) {
            $response = array(
                'success' => 0,
                'msg'     => 'Enrol not found'
            );

            echo json_encode($response); exit;
        }

        $updated_data = array(
            'completed'      => $set_completed,
            'completed_date' => ($set_completed == 1 ? new Zend_Db_Expr('UTC_TIMESTAMP()') : null)
        );

        $response = array(
            'success'   => 1,
            'msg'       => 'OK',
            'completed' => $set_completed
        );

        $enrolDb->update($updated_data, array('id = ?' => $enrol['id']));

        echo json_encode($response); exit;
    }

    public static function courseRegistrationEmail($data) {
        $db   = getDB();
        $db2  = getDB2();

        $order_id = $data['order_id'];
        $learningmode = isset($data['learningmode'])?$data['learningmode']:'OL';

        $orderDb = new App_Model_Order();
        $enrolDb = new App_Model_Enrol();
        $userDb  = new App_Model_User();

        $order   = $orderDb->getOrder(array('a.id = ?' => $data['order_id']), true);
        $user    = $userDb->getUser($order['user_id']);

        unset($user['password'], $user['salt']);

        $order_id = $data['order_id'];

        if($learningmode == 'OL'){
            $type = 2;
        }else{
            $type = 30;
        }

        $response = Cms_Curl::__(SMS_API . '/get/Registration/do/getCommunicationTemplate', array('type' => $type,'val'=>1));

        if(!isset($response['tpl_id'])) {
            return false;
        }

        $msg = $response['tpl_content'];

        $program_name = ($order['curriculum_name'] ? $order['curriculum_name'] : $order['course_name']);

        $msg = str_replace("[name]", "$user[firstname] $user[lastname]", $msg);
        $msg = str_replace("[Program Name]", $program_name, $msg);
        $msg = str_replace("[URL]", APP_URL."/login", $msg);
        $msg = str_replace("[username]", "$user[username]", $msg);
        $msg = str_replace("[password]", "Your current password", $msg);
        $msg = str_replace("[invoice-url]", APP_URL."/user/invoice/id/". $data['order_id'], $msg);
        $msg = str_replace("[invoice-no]", $order['invoice_no'], $msg);
        $msg = str_replace("[invoice-amount]", $order['amount'], $msg);

        $select = $db->select()
            ->from(array("a" => "enrol"), array("id", "course_id", "batch_id", "curriculum_id", "regtype", "learningmode", "schedule"))
            ->join(array("b" => "courses"), "b.id = a.course_id", array('course_title' => 'title', 'course_code' => 'code', 'course_code_safe' => 'code_safe'))
            ->where("a.order_id = ?", $order_id);
        $enrols = $db->fetchAll($select);

        $learning_material = '';
        $schedules = '';

        if($enrols) {

            foreach ($enrols as $row) {
                $learning_material .= "<li><p><a href='" . APP_URL . "/courses/$row[course_code_safe]/learn' target='_blank'>$row[course_code] - $row[course_title]</a></p></li>";
            }

            $learning_material = '<ol>' . $learning_material . '</ol>';
            $msg = str_replace('[learning-material]', $learning_material, $msg);

            foreach ($enrols as $row) {
                if ($row['learningmode'] != 'FTF') {
                    continue;
                }

                $select = $db2->select()
                    ->from(array('a' => 'course_group_schedule'), array('sc_date_start', 'sc_date_end', 'sc_duration', 'sc_start_time', 'sc_end_time', 'sc_venue'))
                    ->join(array('b' => 'course_group'), 'b.id = a.IdSchedule', array())
                    ->joinLeft(array('c' => 'tbl_staffmaster'), 'b.IdLecturer = c.IdStaff', array('FullName'))
                    ->join(array('d' => 'tbl_subjectmaster'), 'd.IdSubject = b.IdSubject', array('SubjectName', 'SubCode'))
                    ->where('a.sc_id = ?', $row['schedule']);
                $schedule = $db2->fetchRow($select);

                $sc_date = date('d-m-Y', strtotime($schedule['sc_date_start']));
                $sc_date .= ' ~ ' . date('d-m-Y', strtotime($schedule['sc_date_end']));
                $schedules .= "<li>
                            <p>
                                <strong>$schedule[SubCode] - $schedule[SubjectName]</strong><br />
                                Date: $sc_date <br />
                                Time: " . date('h.i a', strtotime($schedule['sc_start_time'])) . ' to ' . date('h.i a', strtotime($schedule['sc_end_time'])) . "<br />
                                Venue: $schedule[sc_venue] <br />
                                Lecturer: $schedule[FullName]
                            </p>
                        </li>";
            }
        }

        $schedules = '<ol>'. $schedules .'</ol>';
        $msg = str_replace('[course-schedule]', $schedules, $msg);

        $emailSubject = APP_NAME. ' - Confirmation of Registration';
        $mail = new Cms_SendMail();
        //$mail->fnSendMail($user['email'], $emailSubject, $msg);

        $emailDb = new App_Model_Email();
        $data = array(
            'recepient_email' => $user['email'],
            'subject' => $emailSubject,
            'content' => $msg,
            'date_send' =>  date("Y-m-d H:i:s"),
            'attachment_path' => '',
            'attachment_filename' => ''
        );

        //to send email with attachment
        $emailDb->addData($data);
    }

    /*TBE User Migration*/

    public static function addProfile($params = array())
    {

        parent::getPost();
        $post = self::$post;

        //check required parameters
        self::checkParams(array('std_id'), $post);
        
        $db2 = getDB2();
        $db2->insert('exam_registration_log', array(
            'function_name' => 'LMS addProfile()',
            'params'        => json_encode($post),
            'created_at'    => date('Y-m-d H:i:s')
        ));

        $std_id = $post['std_id'];
        $from = isset($post['from']) ? $post['from'] : 1; //2=sms,1=corporate,0=lms
        $std_idnumber = $post['std_idnumber'];

        $dbUser = new App_Model_User;
        $externalDataDB = new Admin_Model_DbTable_ExternalData();

        // add user
        // password kalau bukan tbe auto set kan 123456
        $salt = Cms_Common::generateRandomString(22);
        $options = array('salt' => $salt);
        $passwordTemp = isset($post['tbe_password'])?$post['tbe_password']:123456;
        $hash = @password_hash($passwordTemp, PASSWORD_BCRYPT, $options);
        $randToken = Cms_Common::generateRandomString(22);

        //check exist idnumber/username
        $userExist = $dbUser->getUserByUsername($std_idnumber);
        
        $gender = NULL;
        if($post['std_gender'] == 1){
            $gender = 1;
        }elseif ($post['std_gender'] == '0'){
            $gender = 2;
        }

        //get external data
        $qualification = ($post['std_qualification'] ? $externalDataDB->getIdByValue(1, $post['std_qualification']) : null);
        $company       = ($post['std_corporate_id'] ? $externalDataDB->getIdByValue(4, $post['std_corporate_id']) : null);
        $country       = ($post['std_country'] ? $externalDataDB->getExternalCountry('country', $post['std_country']) : null);
        $city          = ($post['std_country'] ? $externalDataDB->getExternalCountry('city', $post['std_city']) : null);
        $state         = ($post['std_state'] ? $externalDataDB->getExternalCountry('state', $post['std_state']) : null);
        $race          = ($post['std_race'] ? $externalDataDB->getIdByValue(8, $post['std_race']) : null);
        $religion      = ($post['std_religion'] ? $externalDataDB->getIdByValue(7, $post['std_religion']) : null);

        if ($userExist) {
            $user_id = $userExist['id'];

            $dataUserUpd = array();

            //update
            if(!$userExist['race'] || $userExist['race'] == 0){
                $dataUserUpd['race'] = isset($race) ? $race['id'] : 0;
            }

            if(!$userExist['religion'] || $userExist['religion'] == 0){
                $dataUserUpd['religion'] = isset($religion) ? $religion['id'] : 0;
            }

            if(!$userExist['gender']){
                $dataUserUpd['gender'] = $gender;
            }

            if(!$userExist['qualification'] || $userExist['qualification'] == 0){
                $dataUserUpd['qualification'] = isset($qualification) ? $qualification['id'] : 0;
            }

            if(!$userExist['address1'] || $userExist['address1'] == '' || $userExist['address1'] == NULL){
                $dataUserUpd['address1'] = $post['std_address'];
            }

            if(!$userExist['postcode'] || $userExist['postcode'] == 0 || $userExist['postcode'] == NULL){
                $dataUserUpd['postcode'] = $post['std_poscode'];
            }

            if(!$userExist['country'] || $userExist['country'] == 0 || $userExist['country'] == NULL){
                $dataUserUpd['country'] = isset($country) ? $country['id'] : 0;
            }

            if(!$userExist['city'] || $userExist['city'] == 0 || $userExist['city'] == NULL){
                $dataUserUpd['city'] = isset($city) ? $city['id'] : 0;
            }

            if(!$userExist['state'] || $userExist['state'] == 0 || $userExist['state'] == NULL){
                $dataUserUpd['state'] = isset($state) ? $state['id'] : 0;
            }

            if($userExist['tbe_id'] != 0 && (!$userExist['company'] || $userExist['company'] == 0 || ($userExist['company'] != $company['id']))){
                $dataUserUpd['company'] = isset($company) ? $company['id'] : 0;
            }

            $dbUser->update($dataUserUpd, array('id = ?' => $user_id));
        }else{

            $dataUser = array(
                'username' => $std_idnumber,
                'password' => $hash,
                'salt' => $salt,
                'firstname' => $post['std_fullname'],
                'lastname' => '',
                'email' => $post['std_email'],
                'contactno' => $post['std_contact_number'],
                'address1' => $post['std_address'],
                'postcode' => $post['std_poscode'],
                'city' => isset($city) ? $city['id'] : 0,
                'state' => isset($state) ? $state['id'] : 0,
                'country' => isset($country) ? $country['id'] : 0,
                'birthdate' => isset($post['std_dob']) ? $post['std_dob'] : date('Y-m-d'),
                'nationality' => isset($country) ? $country['code'] : 'MY',
                'qualification' => isset($qualification)?$qualification['id']:0,
                'company' => isset($post['std_corporate_id']) ? $company['id'] : 0,
                'gender' => $gender, //1 - male ; 2 - female;
                'race' => isset($race) ? $race['id'] : 0,
                'religion' => isset($religion) ? $religion['id'] : 0,
                'register_date' => isset($post['created_date']) ? $post['created_date'] : new Zend_Db_Expr('UTC_TIMESTAMP()'),
                'timezone' => 'Asia/Kuala_Lumpur',
                'role' => 'student',
                'token' => $randToken,
                'active' => 1,
                'from' => $from,
                'external_id' => $std_id,
                'migrate_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                'tbe_id' => isset($post['tbe_id'])?$post['tbe_id']:0,

            );

            $user_id = $dbUser->insert($dataUser);

        }

        $response = array('id'=>$user_id);
        die(json_encode($response));
    }
    
    public static function test()
    {
        parent::getPost();
        $post = self::$post;
        
        self::checkParams(array('test'), $post);
        
        $response = array(
            'error'   => 0,
            'message' => 'OK'
        );
        
        echo json_encode($response); exit;
    }

}
