<?php
class Cms_SystemLog
{
	public static function add($logtype,$message='')
	{
		$logDb = new App_Model_SystemLog();

		$data = array(
						'logtype'		=> $logtype,
						'message'		=> $message,
						'created_by'	=> Zend_Auth::getInstance()->getIdentity()->id,
						'created_date' 	=> new Zend_Db_Expr('UTC_TIMESTAMP()')
		);

		$logDb->insert($data);
	}
}