<?php
class Cms_Events
{
    static function trigger($name, $target = null, $argv = array(), $callback = null )
    {
        $events = Zend_Registry::get('Events');
        return $events->trigger($name, $target, $argv, $callback );
    }

    static function attach($event, $callback = null, $priority = 1 )
    {
        $events = Zend_Registry::get('Events');
        return $events->attach($event, $callback, $priority);
    }
}