<?php
class Cms_Render
{
    public static function error($content = '', $redirect = '')
    {
        $helper = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $helper->setNoRender(true);

        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');

        $view->content = $content;
        $view->redirect = $redirect;

        $view->setBasePath(APPLICATION_PATH . '/views/');
        echo $view->render('partials/error.phtml');
    }

    public static function login($redirect = '')
    {
        //$helper = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        //$helper->setNoRender(true);

        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view->redirect = $redirect;

        $view->setBasePath(APPLICATION_PATH . '/views/');
        return $view->render('partials/login.phtml');
    }
}