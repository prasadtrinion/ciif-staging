<?php
class Cms_Api
{
	protected static $post;

	public static function getPost()
	{
		$front = Zend_Controller_Front::getInstance();
		$post = $front->getRequest()->getPost();

		self::$post = $post;
	}

	public static function checkKey($key)
	{
		$check = true;

		return $check;
	}

	public static function checkParams( array $data, array $post, $terminate=true )
	{
		$error = array();
		foreach ( $data  as $check )
		{
			if ( !isset($post[$check]) )
			{
				$error[] = $check;
			}
		}

		if ( !empty($error) )
		{
			if ( $terminate == true )
			{
				$message = 'Missing Parameter(s): '.implode(', ',$error);
				self::error($message);
			}
			return $message;
		}
	}

	public static function error($msg='Error Occured')
	{
		$data = array('msg' => $msg, 'error' => 1);
		echo json_encode($data);
		exit;
	}
}