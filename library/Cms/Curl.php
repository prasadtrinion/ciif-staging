<?php
class Cms_Curl
{
    public static function __($get, $params)
    {
        return self::request($get, $params);
    }

    public static function request($uri, $params)
    {
        try
        {
            $config = array('adapter'   => 'Zend_Http_Client_Adapter_Curl' );

            $site = new Zend_Http_Client($uri, $config);
            $site->setParameterPost($params);

            $response = $site->request('POST')->getBody();

            if ( Cms_Common::isJson($response) ) {
                return json_decode($response, true);
            }
            else
            {
                return $response;
            }
        }
        catch (Zend_Http_Client_Adapter_Curl_Exception $e)
        {
            die('Error Doing Curl');
        }
    }
}