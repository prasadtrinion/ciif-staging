<?php
class Cms_Permission extends Cms_Base
{
    public static function allowed($action)
    {
        $permDb = new App_Model_PermissionRole();

        $permission = $permDb->getAll();

        $can = false;

        if ( Zend_Auth::getInstance()->hasIdentity() )
        {

            //get role
            $role = Zend_Auth::getInstance()->getIdentity()->role;

            //check
            if ( isset($permission[$role][$action]) && $permission[$role][$action] == 1 )
            {
                $can = true;
            }
        }

        return $can;
    }
}