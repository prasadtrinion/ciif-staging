<?php
class Cms_SendMail 
{
	private $use_smtp;
	private $config;
	private $lobjTransport;
	
	public function __construct() 
	{
		$this->use_smtp = true;

	$this->config = array(
								'auth' => 'login',
								'username' => SMTP_USERNAME,
								'password' => SMTP_PASSWORD,
								'ssl' => 'tls',
								'port' => '587'
	);

		$this->fnsetObj();
	}

	private function fnsetObj() 
	{
		if( $this->use_smtp == true )
		{
			$this->lobjTransport = new Zend_Mail_Transport_Smtp(SMTP_SERVER, $this->config);
			Zend_Mail::setDefaultTransport($this->lobjTransport);
		}
		else
		{
			$this->lobjTransport = new Zend_Mail_Transport_Smtp('localhost');
			Zend_Mail::setDefaultTransport($this->lobjTransport);
		}
	}
	
	/*
	*
	*/
	public function fnSendMail($to,$subject,$message,$toname="",$cc=array(),$bcc=array()) 
	{
		$wraptemplate = 0; //change to 0 if dont want

		if ( $wraptemplate == 1 )
		{
			$message = "<div id=\"mailtpl\" style=\"background:#f0fbfc; margin-bottom:20px; padding:20px;\">
				<div class=\"logo\" style=\"padding-left:10px; margin-bottom:10px;\"><img src=\"http://211.25.49.196/images/logo_new.png\" alt=\"INCEIF\" /></div>
				<div class=\"wrap\" style=\"background:#fff; border:1px solid #5997a9; font-family: 'Lucida Grande', Arial, sans-serif; padding:0; margin:0;\">	
					<!--<div class=\"head\"><h2 style=\"background:#0897c2;  padding:6px; border-bottom:1px solid #59a9a9; margin:0;\"><a href=\"http://www.inceif.org\" style=\"text-decoration:none; text-transform:uppercase; color:#fff; font: bold 16px/16px \"Lucida Grande\"\">INCEIF</a></h2></div>-->
					<div class=\"body\" style=\"padding:10px; font:normal 11px \"Lucida Grande\", Arial; color:#545a5a; line-height:1.4em\">
						".$message."
					</div>
					<div class=\"footer\" style=\"padding:10px; margin-top:10px; border-top:1px solid #eee; font-size:9px; color:#ccc;\">
						&copy; ".date('Y')." INCEIF&reg; Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)
					</div>
				</div>
			</div>";
		} // wrap template

		$mail = new Zend_Mail();
		$mail->addTo($to);
		$mail->setSubject($subject);
		$mail->setBodyHtml($message);
		$mail->setFrom(SMTP_FROM_EMAIL, SMTP_FROM);

		foreach($cc as $email => $name)
		{
			$mail->addCc($email,$name);
		}

		foreach($bcc as $email) 
		{
			$mail->addBcc($email);
		}

		//Send it!
		$sent = true;
		try 
		{
			if ( defined('SMTP_SEND') && SMTP_SEND == 1 )
			{
				$mail->send();
			}
		}
		catch (Exception $e)
		{	
			throw new Exception( $e->getMessage() );
			exit;
		}
	}
}
