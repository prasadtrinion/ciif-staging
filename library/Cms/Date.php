<?php
/*
 * handling dates and timezones
 */
use Carbon\Carbon;
class Cms_Date extends Carbon
{
    public static function date($date, $format='j M Y g:i A')
    {
        $tz =  date_default_timezone_get();
        if ( Zend_Auth::getInstance()->hasIdentity() )
        {
            $tz = Zend_Auth::getInstance()->getIdentity()->timezone != null ? Zend_Auth::getInstance()->getIdentity()->timezone : $tz;
        }

        $format = $format == null || $format == '' ? 'd/m/Y' : $format;

        $carbon = new Carbon($date, 'UTC');
        $carbon->timezone = new DateTimeZone($tz);

        return $carbon->format($format);
    }
}