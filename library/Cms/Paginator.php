<?php
class Cms_Paginator
{
    public static function query($sql, $pagecount=10)
    {
        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($sql));
        $paginator->setItemCountPerPage($pagecount);
        $paginator->setCurrentPageNumber(Zend_Controller_Front::getInstance()->getRequest()->getParam('page',1));

        return $paginator;
    }

    public static function arrays($array, $pagecount=10)
    {
        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($array));
        $paginator->setItemCountPerPage($pagecount);
        $paginator->setCurrentPageNumber(Zend_Controller_Front::getInstance()->getRequest()->getParam('page',1));

        return $paginator;
    }
}