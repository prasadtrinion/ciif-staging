<?php
class Cms_Breadcrumbs extends Cms_Base
{
    public static function __( array $data )
    {
        return self::show($data);
    }

    /*
     * array( 'home', array(), '')
     */
    public static function show( array $data )
    {
        $link = array();
        foreach ( $data as $row )
        {
            if ( $row == 'home')
            {
                $link[] = '<li><a href="/"><i class="uk-icon-home"></i></a></li>';
            }
            else if ( is_array($row) )
            {
                $link[] = '<li><a href="'.key($row).'">'.$row[key($row)].'</a></li>';
            }
            else if ( is_string($row) )
            {
                $link[] = '<li class="uk-active"><span>'.$row.'</span></li>';
            }
        }

        if ( !empty($link) )
        {
            return '<ul class="uk-breadcrumb">'.implode('', $link).'</ul>';
        }
    }
}