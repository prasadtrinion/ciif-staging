<?php
/*
 * view hooks into layout
 */
class Cms_Hooks
{
    /*
     * layout hooks:
     * - head
     * - body.top
     * - body.bottom
     * - content.top
     * - content.bottom
     */
    public static function hook($name)
    {
        $hook_name = 'hook_'.$name;

        $view = Zend_Layout::getMvcInstance()->getView();
        if ( isset($view->layout()->$hook_name) )
        {
            echo $view->layout()->$hook_name;
        }
    }

    public static function start()
    {
        ob_start();
    }

    public static function end($name)
    {
        $content = ob_get_contents();
        ob_end_clean();

        self::push($name, $content);
    }

    public static function push($name, $content)
    {
        $hook_name = 'hook_'.$name;

        $view = Zend_Layout::getMvcInstance()->getView();
        if ( isset($view->layout()->$hook_name) )
        {
            $value = $view->layout()->$hook_name;
            $content = $value . $content;
        }

        Zend_Layout::getMvcInstance()->assign($hook_name, $content);
    }
}