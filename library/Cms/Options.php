<?php
class Cms_Options extends Cms_Base
{
    public static function updateOptions(array $data)
    {
        $optDb = new App_Model_Options();

        if ( empty($data) ) return false;

        foreach ( $data as $name => $value )
        {
            $optDb->updateOption($name, $value);
        }

        Cms_Events::trigger('options.update');
    }

    public static function getOption($name, $getValue = true)
    {
        $options = self::getAll($getValue);

        return isset($options[$name]) ? $options[$name] : null;
    }

    public static function __($name)
    {
        return self::getOption($name);
    }

    public static function getAll($getValue = true)
    {
        $cache = Zend_Registry::get('cache');

        $cache_name = ($getValue ? 'options' : 'options_default');
        $field_name = ($getValue ? 'opt_value' : 'opt_default');

        $result = $cache->load($cache_name);

        if(  !$result )
        {
            $optDb = new App_Model_Options();

            $results = $optDb->fetchAll()->toArray();

            $data = array();

            foreach ( $results as $row )
            {
                $data[$row['opt_name']] = $row[$field_name];
            }

            $cache->save($data, $cache_name);

            return $data;
        }
        else
        {
            return $result;
        }
    }

    public static function updateCache()
    {
        $cache = Zend_Registry::get('cache');

        $optDb = new App_Model_Options();

        $results = $optDb->fetchAll()->toArray();

        $data    = array();
        $default = array();

        foreach ( $results as $row )
        {
            $data[$row['opt_name']]    = $row['opt_value'];
            $default[$row['opt_name']] = $row['opt_default'];
        }

        $cache->save($data, 'options');
        $cache->save($default, 'options_default');
    }

}