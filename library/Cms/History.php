<?php
class Cms_History
{
	public static function addHistory($data)
	{
		$historyDb = new App_Model_History();

		$historyDb->insert($data);
	}

	public static function getHistory($entry_id, $entry_table,$desc=true)
	{
		$historyDb = new App_Model_History();
		$db = getDB();

		return $db->fetchAll( $historyDb->select()->where('entry_id=?', $entry_id)
												  ->where('entry_table=?',$entry_table)
												  ->order('entry_id '.($desc?'DESC':'ASC'))
							);
	}
}