<?php
class Cms_Common
{
	public static function calculateAge($date)
	{
		return date_diff(date_create($date), date_create('today'))->y;
	}

	/*
	 * Convert numerical number to words
	 */
	public static function numToWords($number) {

		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
				0                   => 'zero',
				1                   => 'one',
				2                   => 'two',
				3                   => 'three',
				4                   => 'four',
				5                   => 'five',
				6                   => 'six',
				7                   => 'seven',
				8                   => 'eight',
				9                   => 'nine',
				10                  => 'ten',
				11                  => 'eleven',
				12                  => 'twelve',
				13                  => 'thirteen',
				14                  => 'fourteen',
				15                  => 'fifteen',
				16                  => 'sixteen',
				17                  => 'seventeen',
				18                  => 'eighteen',
				19                  => 'nineteen',
				20                  => 'twenty',
				30                  => 'thirty',
				40                  => 'fourty',
				50                  => 'fifty',
				60                  => 'sixty',
				70                  => 'seventy',
				80                  => 'eighty',
				90                  => 'ninety',
				100                 => 'hundred',
				1000                => 'thousand',
				1000000             => 'million',
				1000000000          => 'billion',
				1000000000000       => 'trillion',
				1000000000000000    => 'quadrillion',
				1000000000000000000 => 'quintillion'
		);

		if (!is_numeric($number)) {
			return false;
		}

		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			// overflow
			trigger_error(
			'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
			E_USER_WARNING
			);
			return false;
		}

		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}

		$string = $fraction = null;

		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}

		switch (true) {
			case $number < 21:
				$string = $dictionary[$number];
				break;
			case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
				break;
			case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . convert_number_to_words($remainder);
				}
				break;
			default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= convert_number_to_words($remainder);
				}
				break;
		}

		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}

		return ucwords($string);
	}

	/*
	 * Generate pdf
	 * @param array $content_option array of option to generate PDF
	 * @return int The function returns the number of bytes that were written to the file, or false on failure.
	 * @throws Exception 
	 *  
	 */
	public static function generatePdf(array $content_option){
		
		$options = array(
			'content' => '',
			'file_name' => 'INCEIF',
			'file_extension' => 'pdf',
			'save_path' => DOCUMENT_PATH,
			'save' => false,
			'css' => '@page { margin: 10px 50px; 0px; 50px}
					  body { font-family: Helvetica, Arial, sans-serif; font-size:14px }',
				
			'header' => '<script type="text/php">
						if ( isset($pdf) ) {
					
						  $header = $pdf->open_object();
						 
						  $w = $pdf->get_width();
						  $h = $pdf->get_height();
							
						  $img_w = 180; 
						  $img_h = 64;
	//                      $pdf->image("images/logo_text.jpg",  $w-($img_w+35), 20, $img_w, $img_h);
						  
						  // Draw a line along the bottom
						  $font = Font_Metrics::get_font("Helvetica");
						  $size = 8;
						  $text_height = Font_Metrics::get_font_height($font, $size)+2;
						  $y = $h - (2.5 * $text_height)-10;
						  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
						  $pdf->close_object();
				
						  $pdf->add_object($header, "all");
						}
						</script>',
				
			'footer' => '<script type="text/php">
						if ( isset($pdf) ) {
					
						  $footer = $pdf->open_object();
				
						  $font = Font_Metrics::get_font("Helvetica");
						  $size = 8;
						  $color = array(0,0,0);
						  $text_height = Font_Metrics::get_font_height($font, $size)+2;
						 
						  $w = $pdf->get_width();
						  $h = $pdf->get_height();
						 
					
						  // Draw a line along the bottom
						  $y = $h - (2.5 * $text_height)-10;
						  $pdf->line(10, $y, $w - 10, $y, $color, 1);
					
						  //1st row footer
						  $text = "Management Office";
						  $width = Font_Metrics::get_text_width($text, $font, $size);	
						  $y = $h - (2 * $text_height)-10;
						  $x = ($w - $width) / 2.0;
						  
						  $pdf->page_text($x, $y, $text, $font, $size, $color);
						 
						  //2nd row footer
						  $text = "19th Floor, Menera Takaful Malaysia, Jalan Sultan Sulaiman, 50000 Kuala Lumpur, Malaysia.";
						  $width = Font_Metrics::get_text_width($text, $font, $size);	
						  $y = $h - (1 * $text_height)-10;
						  $x = ($w - $width) / 2.0;
						  
						  $pdf->page_text($x, $y, $text, $font, $size, $color);
						  //3rd row footer
						  $text = "T:+603 2276 5279/5232 E: info@ciif-global.org W: www.ciif-global.org";
						  $width = Font_Metrics::get_text_width($text, $font, $size);	
						  $y = $h - (1 * $text_height)-2;
						  $x = ($w - $width) / 2.0;
						  
						  $pdf->page_text($x, $y, $text, $font, $size, $color);
					
						  $pdf->close_object();
				
						  $pdf->add_object($footer, "all");
						 
						}
						</script>' 		
		);
		
		foreach ($content_option as $key => $value) {
			if ( !array_key_exists($key, $options) ) {
			  throw new Exception("Option '$key' doesn't exist");
			}
		
			$options[$key] = $value;
		}
		
		
		require_once '../library/dompdf/dompdf_config.inc.php';
		//require_once 'dompdf_config.inc.php';
		$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
		$autoloader->pushAutoloader('DOMPDF_autoload');

		$dompdf = new DOMPDF();
		$dompdf->set_paper("a4","portrait");

		$html = '<html>
						<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><style>'.$options['css'].'body { font-family: dejavu sans; }</style></head>
						<body>'.$options['header'].$options['content'].$options['footer'].'</body>
				 </html>';

		$dompdf->load_html($html,'UTF-8');
		$dompdf->render();

		if($options['save'] == true){ //  save file to location

			$output = $dompdf->output();

			if (!is_dir($options['save_path']))
			{
				if ( mkdir_p($options['save_path']) === false )
				{
					throw new Exception('Cannot create attachment folder ('.$options['save_path'].')');
				}
			}

			$pdf_file = file_put_contents($options['save_path']."/".$options['file_name'], $output);
		  
		}else{// stream file

			$pdf_file = $dompdf->stream($options['file_name']);
		}

		return $pdf_file;
	}



	/*
	 * credits to WP
	 */

	
	public static function mkdir_p( $target )
	{
		// safe mode fails with a trailing slash under certain PHP versions.
		$target = rtrim($target, '/'); // Use rtrim() instead of untrailingslashit to avoid formatting.php dependency.
		if ( empty($target) )
			$target = '/';

		if ( file_exists( $target ) )
			return @is_dir( $target );

		// We need to find the permissions of the parent folder that exists and inherit that.
		$target_parent = dirname( $target );
		while ( '.' != $target_parent && ! is_dir( $target_parent ) ) {
			$target_parent = dirname( $target_parent );
		}

		// Get the permission bits.
		$dir_perms = false;
		if ( $stat = @stat( $target_parent ) ) 
		{
			$dir_perms = $stat['mode'] & 0007777;
		}
		else 
		{
			$dir_perms = 0777;
		}

		if ( @mkdir( $target, $dir_perms, true ) ) 
		{

			// If a umask is set that modifies $dir_perms, we'll have to re-set the $dir_perms correctly with chmod()
			if ( $dir_perms != ( $dir_perms & ~umask() ) ) 
			{
				$folder_parts = explode( '/', substr( $target, strlen( $target_parent ) + 1 ) );
				for ( $i = 1; $i <= count( $folder_parts ); $i++ ) 
				{
					@chmod( $target_parent . '/' . implode( '/', array_slice( $folder_parts, 0, $i ) ), $dir_perms );
				}
			}

			return true;
		}

		return false;
	}

	public static function getExt($filename)
	{
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		return $ext;
	}

	public static function getPublic() {
		chdir(APPLICATION_PATH);
		return realpath("../public");
	}

	public static function expire()
	{
		if ( !headers_sent() )
		{
			header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
			header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
		}
	}

	public static function generateRandomString($length = 6) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}

	public static function getIp()
	{
		/*inception level ternaries*/
		$ip = getenv('HTTP_CLIENT_IP')?:
		getenv('HTTP_X_FORWARDED_FOR')?:
		getenv('HTTP_X_FORWARDED')?:
		getenv('HTTP_FORWARDED_FOR')?:
		getenv('HTTP_FORWARDED')?:
		getenv('REMOTE_ADDR');

		return $ip;
	}


	/*
		generate datetime for html5 time tag
	*/
	public static function getDateTimeValue( $intDate = null ) {

		$strFormat = 'Y-m-dTH:i:sP';
		$strDate = $intDate ? date( $strFormat, $intDate ) : date( $strFormat ) ; 
		
		return $strDate;
	}

	public static function exists(&$what,$return='')
	{
		return isset($what) ? $what : $return;
	}

	public static function clean($value='',$type='normal')
	{
		$value = trim(strip_tags($value));
		
		if ( $type == 'alpha' )
		{
			$value = preg_replace('/[^\da-z]/i', '', $value);
		}

		return $value;
	}

	/*
	 * input My Long Name
	 * output my-long-name
	 */
	public static function safename($name,$replace='-')
	{
		$untouched = $name;
		$name = strip_tags( trim($name) );
		preg_match_all('/[a-z0-9]+/',strtolower($name), $match);

		$value = implode($replace,$match[0]);

		//this means that the value is some unicode crap
		if ( $value == '' )
		{
			$value = substr(md5(strlen($value)),0,8);
			//$value = $untouched;
		}

		return $value;
	}

	public static function cleanArray($data=array(), $keys=array())
	{
		if (is_array($data))
		{
			foreach( $data as $key => $val)
			{
				if ( !empty($keys) )
				{
					if ( is_string($val) && in_array($key, $keys) )
					{
						$data[$key] = self::clean($val);
					}
					elseif ( is_array($val))
					{
						$data[$key] = self::cleanArray($val,$keys);
					}
				}
				else
				{

					if(is_string($val))
					{
						$data[$key] = self::clean($val);
					}
					elseif(is_array($val))
					{
						$data[$key] = selff::cleanArray($val,$keys);
					}
				}
			}

		}
		return $data;
	}

	public static function timeAgo($timestring, $timezone = NULL, $language = 'en') {
		$timeAgo = new Cms_TimeAgo($timezone, $language);

		return $timeAgo->inWords($timestring, "now");
	}

	public static function snippet($text,$length=250,$tail="...")
	{
		$text = trim($text);
		$txtl = strlen($text);
		if($txtl > $length) {
			for($i=1;$text[$length-$i]!=" ";$i++) {
				if($i == $length) {
					return substr($text,0,$length) . $tail;
				}
			}
			$text = substr($text,0,$length-$i+1) . $tail;
		}
		return $text;
	}

	public static function formatFilesize($size)
	{
		$unit=array('b','kb','mb','gb','tb','pb');
		return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}

	public static function encrypt($text,$enc = true)
	{
		$salt = $key = '1nc31f';
		/*if ( $enc == true )
		{
			$what = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));

			return str_replace('/','[]', $what);
		}
		else
		{
			$text =  str_replace('[]','/', $text);
			$what = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
			return $what;
		}*/


		if ( $enc == true )
		{
			$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
			$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
			$crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key.$key), $text, MCRYPT_MODE_ECB, $iv);

			// encode data so that $_GET won't urldecode it and mess up some characters
			$data = base64_encode($crypttext);
			$data = str_replace(array('+','/','='),array('-','_',''),$data);
			return trim($data);
		}
		else
		{
			$base64 = $text;
			$data = str_replace(array('-','_'),array('+','/'),$base64); // manual de-hack url formatting
			$mod4 = strlen($data) % 4; // base64 length must be evenly divisible by 4
			if ($mod4) {
				$data .= substr('====', $mod4);
			}
			$crypttext = base64_decode($data);
			$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
			$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
			$decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key.$key), $crypttext, MCRYPT_MODE_ECB, $iv);
			return trim($decrypttext);
		}
	}

	/*
	 * Add Flash Messsenger
	 */
	public static function notify($type='success', $message='')
	{
		//$flashMessenger = $this->_helper->getHelper('FlashMessenger');
		//$flashMessenger->addMessage(array('error' => 'Student data not found'));

		$messenger = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
		$messenger->addMessage(array($type => $message));
	}

	/*
	 *
	 */
	public static function displayFlash($moreclass='')
	{
		$flashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
		if (count($flashMessenger->getMessages())) :
			foreach ($flashMessenger->getMessages() as $message) :

				$alerttype = '';
				switch( key($message) )
				{
					case 'notice': $alerttype = ''; break;
					case 'success': $alerttype = 'uk-alert-success'; break;
					case 'error':	$alerttype = 'uk-alert-danger';break;
				}
				?>
				<div class="fsm message uk-alert <?php echo $alerttype?> <?php echo $moreclass ?>" data-uk-alert>
					<a href="#" class="uk-alert-close uk-close"></a>
					<?php if ( key($message) == 'success' ) { ?><span class="icon icon-check"></span><?php } ?>
					<?php if ( key($message) == 'error' ) { ?><span class="icon icon-attention"></span><?php } ?>
					<?php if ( key($message) == 'notice' ) { ?><span class="icon icon-info-circled"></span><?php } ?>
					<?php echo current($message); ?>
				</div>
			<?php
			endforeach;
		endif;
	}
	/*
	 * get Ip Address
	 */
	public static function get_ip()
	{
		$ip = getenv('HTTP_CLIENT_IP')?:
		getenv('HTTP_X_FORWARDED_FOR')?:
		getenv('HTTP_X_FORWARDED')?:
		getenv('HTTP_FORWARDED_FOR')?:
		getenv('HTTP_FORWARDED')?:
		getenv('REMOTE_ADDR');

		return $ip;
	}


	public static function gravatar ( $email ='' )
	{
		return md5( strtolower(trim($email) ) ) ;
	}

	public static function navactive($active='')
	{
		$view = Zend_Layout::getMvcInstance()->getView();
		if ( $view->layout()->nav == $active )
		{
			echo 'uk-active';
		}
	}

	public static function getCountryList($code='')
	{
		$country_array = array(
			"AF" => "Afghanistan",
			"AL" => "Albania",
			"DZ" => "Algeria",
			"AS" => "American Samoa",
			"AD" => "Andorra",
			"AO" => "Angola",
			"AI" => "Anguilla",
			"AQ" => "Antarctica",
			"AG" => "Antigua and Barbuda",
			"AR" => "Argentina",
			"AM" => "Armenia",
			"AW" => "Aruba",
			"AU" => "Australia",
			"AT" => "Austria",
			"AZ" => "Azerbaijan",
			"BS" => "Bahamas",
			"BH" => "Bahrain",
			"BD" => "Bangladesh",
			"BB" => "Barbados",
			"BY" => "Belarus",
			"BE" => "Belgium",
			"BZ" => "Belize",
			"BJ" => "Benin",
			"BM" => "Bermuda",
			"BT" => "Bhutan",
			"BO" => "Bolivia",
			"BA" => "Bosnia and Herzegovina",
			"BW" => "Botswana",
			"BV" => "Bouvet Island",
			"BR" => "Brazil",
			"BQ" => "British Antarctic Territory",
			"IO" => "British Indian Ocean Territory",
			"VG" => "British Virgin Islands",
			"BN" => "Brunei",
			"BG" => "Bulgaria",
			"BF" => "Burkina Faso",
			"BI" => "Burundi",
			"KH" => "Cambodia",
			"CM" => "Cameroon",
			"CA" => "Canada",
			"CT" => "Canton and Enderbury Islands",
			"CV" => "Cape Verde",
			"KY" => "Cayman Islands",
			"CF" => "Central African Republic",
			"TD" => "Chad",
			"CL" => "Chile",
			"CN" => "China",
			"CX" => "Christmas Island",
			"CC" => "Cocos [Keeling] Islands",
			"CO" => "Colombia",
			"KM" => "Comoros",
			"CG" => "Congo - Brazzaville",
			"CD" => "Congo - Kinshasa",
			"CK" => "Cook Islands",
			"CR" => "Costa Rica",
			"HR" => "Croatia",
			"CU" => "Cuba",
			"CY" => "Cyprus",
			"CZ" => "Czech Republic",
			"CI" => "Ivory Coast",
			"DK" => "Denmark",
			"DJ" => "Djibouti",
			"DM" => "Dominica",
			"DO" => "Dominican Republic",
			"NQ" => "Dronning Maud Land",
			"DD" => "East Germany",
			"EC" => "Ecuador",
			"EG" => "Egypt",
			"SV" => "El Salvador",
			"GQ" => "Equatorial Guinea",
			"ER" => "Eritrea",
			"EE" => "Estonia",
			"ET" => "Ethiopia",
			"FK" => "Falkland Islands",
			"FO" => "Faroe Islands",
			"FJ" => "Fiji",
			"FI" => "Finland",
			"FR" => "France",
			"GF" => "French Guiana",
			"PF" => "French Polynesia",
			"TF" => "French Southern Territories",
			"FQ" => "French Southern and Antarctic Territories",
			"GA" => "Gabon",
			"GM" => "Gambia",
			"GE" => "Georgia",
			"DE" => "Germany",
			"GH" => "Ghana",
			"GI" => "Gibraltar",
			"GR" => "Greece",
			"GL" => "Greenland",
			"GD" => "Grenada",
			"GP" => "Guadeloupe",
			"GU" => "Guam",
			"GT" => "Guatemala",
			"GG" => "Guernsey",
			"GN" => "Guinea",
			"GW" => "Guinea-Bissau",
			"GY" => "Guyana",
			"HT" => "Haiti",
			"HM" => "Heard Island and McDonald Islands",
			"HN" => "Honduras",
			"HK" => "Hong Kong SAR China",
			"HU" => "Hungary",
			"IS" => "Iceland",
			"IN" => "India",
			"ID" => "Indonesia",
			"IR" => "Iran",
			"IQ" => "Iraq",
			"IE" => "Ireland",
			"IM" => "Isle of Man",
			 "IL" => "Israel", 
			"IT" => "Italy",
			"JM" => "Jamaica",
			"JP" => "Japan",
			"JE" => "Jersey",
			"JT" => "Johnston Island",
			"JO" => "Jordan",
			"KZ" => "Kazakhstan",
			"KE" => "Kenya",
			"KI" => "Kiribati",
			"KW" => "Kuwait",
			"KG" => "Kyrgyzstan",
			"LA" => "Laos",
			"LV" => "Latvia",
			"LB" => "Lebanon",
			"LS" => "Lesotho",
			"LR" => "Liberia",
			"LY" => "Libya",
			"LI" => "Liechtenstein",
			"LT" => "Lithuania",
			"LU" => "Luxembourg",
			"MO" => "Macau SAR China",
			"MK" => "Macedonia",
			"MG" => "Madagascar",
			"MW" => "Malawi",
			"MY" => "Malaysia",
			"MV" => "Maldives",
			"ML" => "Mali",
			"MT" => "Malta",
			"MH" => "Marshall Islands",
			"MQ" => "Martinique",
			"MR" => "Mauritania",
			"MU" => "Mauritius",
			"YT" => "Mayotte",
			"FX" => "Metropolitan France",
			"MX" => "Mexico",
			"FM" => "Micronesia",
			"MI" => "Midway Islands",
			"MD" => "Moldova",
			"MC" => "Monaco",
			"MN" => "Mongolia",
			"ME" => "Montenegro",
			"MS" => "Montserrat",
			"MA" => "Morocco",
			"MZ" => "Mozambique",
			"MM" => "Myanmar [Burma]",
			"NA" => "Namibia",
			"NR" => "Nauru",
			"NP" => "Nepal",
			"NL" => "Netherlands",
			"AN" => "Netherlands Antilles",
			"NT" => "Neutral Zone",
			"NC" => "New Caledonia",
			"NZ" => "New Zealand",
			"NI" => "Nicaragua",
			"NE" => "Niger",
			"NG" => "Nigeria",
			"NU" => "Niue",
			"NF" => "Norfolk Island",
			"KP" => "North Korea",
			"VD" => "North Vietnam",
			"MP" => "Northern Mariana Islands",
			"NO" => "Norway",
			"OM" => "Oman",
			"PC" => "Pacific Islands Trust Territory",
			"PK" => "Pakistan",
			"PW" => "Palau",
			"PS" => "Palestinian Territories",
			"PA" => "Panama",
			"PZ" => "Panama Canal Zone",
			"PG" => "Papua New Guinea",
			"PY" => "Paraguay",
			"YD" => "People's Democratic Republic of Yemen",
			"PE" => "Peru",
			"PH" => "Philippines",
			"PN" => "Pitcairn Islands",
			"PL" => "Poland",
			"PT" => "Portugal",
			"PR" => "Puerto Rico",
			"QA" => "Qatar",
			"RO" => "Romania",
			"RU" => "Russia",
			"RW" => "Rwanda",
			"RE" => "R�union",
			"BL" => "Saint Barth�lemy",
			"SH" => "Saint Helena",
			"KN" => "Saint Kitts and Nevis",
			"LC" => "Saint Lucia",
			"MF" => "Saint Martin",
			"PM" => "Saint Pierre and Miquelon",
			"VC" => "Saint Vincent and the Grenadines",
			"WS" => "Samoa",
			"SM" => "San Marino",
			"SA" => "Saudi Arabia",
			"SN" => "Senegal",
			"RS" => "Serbia",
			"CS" => "Serbia and Montenegro",
			"SC" => "Seychelles",
			"SL" => "Sierra Leone",
			"SG" => "Singapore",
			"SK" => "Slovakia",
			"SI" => "Slovenia",
			"SB" => "Solomon Islands",
			"SO" => "Somalia",
			"ZA" => "South Africa",
			"GS" => "South Georgia and the South Sandwich Islands",
			"KR" => "South Korea",
			"ES" => "Spain",
			"LK" => "Sri Lanka",
			"SD" => "Sudan",
			"SR" => "Suriname",
			"SJ" => "Svalbard and Jan Mayen",
			"SZ" => "Swaziland",
			"SE" => "Sweden",
			"CH" => "Switzerland",
			"SY" => "Syria",
			"ST" => "Sao Tome and Principe",
			"TW" => "Taiwan",
			"TJ" => "Tajikistan",
			"TZ" => "Tanzania",
			"TH" => "Thailand",
			"TL" => "Timor-Leste",
			"TG" => "Togo",
			"TK" => "Tokelau",
			"TO" => "Tonga",
			"TT" => "Trinidad and Tobago",
			"TN" => "Tunisia",
			"TR" => "Turkey",
			"TM" => "Turkmenistan",
			"TC" => "Turks and Caicos Islands",
			"TV" => "Tuvalu",
			"UM" => "U.S. Minor Outlying Islands",
			"PU" => "U.S. Miscellaneous Pacific Islands",
			"VI" => "U.S. Virgin Islands",
			"UG" => "Uganda",
			"UA" => "Ukraine",
			"SU" => "Union of Soviet Socialist Republics",
			"AE" => "United Arab Emirates",
			"GB" => "United Kingdom",
			"US" => "United States",
			"ZZ" => "Unknown or Invalid Region",
			"UY" => "Uruguay",
			"UZ" => "Uzbekistan",
			"VU" => "Vanuatu",
			"VA" => "Vatican City",
			"VE" => "Venezuela",
			"VN" => "Vietnam",
			"WK" => "Wake Island",
			"WF" => "Wallis and Futuna",
			"EH" => "Western Sahara",
			"YE" => "Yemen",
			"ZM" => "Zambia",
			"ZW" => "Zimbabwe",
			"AX" => "�land Islands",
		);

		return $code == '' ? $country_array : @$country_array[ strtoupper($code) ];
	}

	public static function getTimezones($get='',$flip=1)
	{
		$timezones = Array(
			'(GMT-12:00) International Date Line West' => 'Pacific/Kwajalein',
			'(GMT-11:00) Midway Island' => 'Pacific/Midway',
			'(GMT-11:00) Samoa' => 'Pacific/Apia',
			'(GMT-10:00) Hawaii' => 'Pacific/Honolulu',
			'(GMT-09:00) Alaska' => 'America/Anchorage',
			'(GMT-08:00) Pacific Time (US & Canada)' => 'America/Los_Angeles',
			'(GMT-08:00) Tijuana' => 'America/Tijuana',
			'(GMT-07:00) Arizona' => 'America/Phoenix',
			'(GMT-07:00) Mountain Time (US & Canada)' => 'America/Denver',
			'(GMT-07:00) Chihuahua' => 'America/Chihuahua',
			'(GMT-07:00) La Paz' => 'America/Chihuahua',
			'(GMT-07:00) Mazatlan' => 'America/Mazatlan',
			'(GMT-06:00) Central Time (US & Canada)' => 'America/Chicago',
			'(GMT-06:00) Central America' => 'America/Managua',
			'(GMT-06:00) Guadalajara' => 'America/Mexico_City',
			'(GMT-06:00) Mexico City' => 'America/Mexico_City',
			'(GMT-06:00) Monterrey' => 'America/Monterrey',
			'(GMT-06:00) Saskatchewan' => 'America/Regina',
			'(GMT-05:00) Eastern Time (US & Canada)' => 'America/New_York',
			'(GMT-05:00) Indiana (East)' => 'America/Indiana/Indianapolis',
			'(GMT-05:00) Bogota' => 'America/Bogota',
			'(GMT-05:00) Lima' => 'America/Lima',
			'(GMT-05:00) Quito' => 'America/Bogota',
			'(GMT-04:00) Atlantic Time (Canada)' => 'America/Halifax',
			'(GMT-04:00) Caracas' => 'America/Caracas',
			'(GMT-04:00) La Paz' => 'America/La_Paz',
			'(GMT-04:00) Santiago' => 'America/Santiago',
			'(GMT-03:30) Newfoundland' => 'America/St_Johns',
			'(GMT-03:00) Brasilia' => 'America/Sao_Paulo',
			'(GMT-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires',
			'(GMT-03:00) Georgetown' => 'America/Argentina/Buenos_Aires',
			'(GMT-03:00) Greenland' => 'America/Godthab',
			'(GMT-02:00) Mid-Atlantic' => 'America/Noronha',
			'(GMT-01:00) Azores' => 'Atlantic/Azores',
			'(GMT-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde',
			'(GMT) Casablanca' => 'Africa/Casablanca',
			'(GMT) Dublin' => 'Europe/London',
			'(GMT) Edinburgh' => 'Europe/London',
			'(GMT) Lisbon' => 'Europe/Lisbon',
			'(GMT) London' => 'Europe/London',
			'(GMT) Monrovia' => 'Africa/Monrovia',
			'(GMT+01:00) Amsterdam' => 'Europe/Amsterdam',
			'(GMT+01:00) Belgrade' => 'Europe/Belgrade',
			'(GMT+01:00) Berlin' => 'Europe/Berlin',
			'(GMT+01:00) Bern' => 'Europe/Berlin',
			'(GMT+01:00) Bratislava' => 'Europe/Bratislava',
			'(GMT+01:00) Brussels' => 'Europe/Brussels',
			'(GMT+01:00) Budapest' => 'Europe/Budapest',
			'(GMT+01:00) Copenhagen' => 'Europe/Copenhagen',
			'(GMT+01:00) Ljubljana' => 'Europe/Ljubljana',
			'(GMT+01:00) Madrid' => 'Europe/Madrid',
			'(GMT+01:00) Paris' => 'Europe/Paris',
			'(GMT+01:00) Prague' => 'Europe/Prague',
			'(GMT+01:00) Rome' => 'Europe/Rome',
			'(GMT+01:00) Sarajevo' => 'Europe/Sarajevo',
			'(GMT+01:00) Skopje' => 'Europe/Skopje',
			'(GMT+01:00) Stockholm' => 'Europe/Stockholm',
			'(GMT+01:00) Vienna' => 'Europe/Vienna',
			'(GMT+01:00) Warsaw' => 'Europe/Warsaw',
			'(GMT+01:00) West Central Africa' => 'Africa/Lagos',
			'(GMT+01:00) Zagreb' => 'Europe/Zagreb',
			'(GMT+02:00) Athens' => 'Europe/Athens',
			'(GMT+02:00) Bucharest' => 'Europe/Bucharest',
			'(GMT+02:00) Cairo' => 'Africa/Cairo',
			'(GMT+02:00) Harare' => 'Africa/Harare',
			'(GMT+02:00) Helsinki' => 'Europe/Helsinki',
			'(GMT+02:00) Istanbul' => 'Europe/Istanbul',
			'(GMT+02:00) Jerusalem' => 'Asia/Jerusalem',
			'(GMT+02:00) Kyev' => 'Europe/Kiev',
			'(GMT+02:00) Minsk' => 'Europe/Minsk',
			'(GMT+02:00) Pretoria' => 'Africa/Johannesburg',
			'(GMT+02:00) Riga' => 'Europe/Riga',
			'(GMT+02:00) Sofia' => 'Europe/Sofia',
			'(GMT+02:00) Tallinn' => 'Europe/Tallinn',
			'(GMT+02:00) Vilnius' => 'Europe/Vilnius',
			'(GMT+03:00) Baghdad' => 'Asia/Baghdad',
			'(GMT+03:00) Kuwait' => 'Asia/Kuwait',
			'(GMT+03:00) Moscow' => 'Europe/Moscow',
			'(GMT+03:00) Nairobi' => 'Africa/Nairobi',
			'(GMT+03:00) Riyadh' => 'Asia/Riyadh',
			'(GMT+03:00) St. Petersburg' => 'Europe/Moscow',
			'(GMT+03:00) Volgograd' => 'Europe/Volgograd',
			'(GMT+03:30) Tehran' => 'Asia/Tehran',
			'(GMT+04:00) Abu Dhabi' => 'Asia/Muscat',
			'(GMT+04:00) Baku' => 'Asia/Baku',
			'(GMT+04:00) Muscat' => 'Asia/Muscat',
			'(GMT+04:00) Tbilisi' => 'Asia/Tbilisi',
			'(GMT+04:00) Yerevan' => 'Asia/Yerevan',
			'(GMT+04:30) Kabul' => 'Asia/Kabul',
			'(GMT+05:00) Ekaterinburg' => 'Asia/Yekaterinburg',
			'(GMT+05:00) Islamabad' => 'Asia/Karachi',
			'(GMT+05:00) Karachi' => 'Asia/Karachi',
			'(GMT+05:00) Tashkent' => 'Asia/Tashkent',
			'(GMT+05:30) Chennai' => 'Asia/Kolkata',
			'(GMT+05:30) Kolkata' => 'Asia/Kolkata',
			'(GMT+05:30) Mumbai' => 'Asia/Kolkata',
			'(GMT+05:30) New Delhi' => 'Asia/Kolkata',
			'(GMT+05:45) Kathmandu' => 'Asia/Kathmandu',
			'(GMT+06:00) Almaty' => 'Asia/Almaty',
			'(GMT+06:00) Astana' => 'Asia/Dhaka',
			'(GMT+06:00) Dhaka' => 'Asia/Dhaka',
			'(GMT+06:00) Novosibirsk' => 'Asia/Novosibirsk',
			'(GMT+06:00) Sri Jayawardenepura' => 'Asia/Colombo',
			'(GMT+06:30) Rangoon' => 'Asia/Rangoon',
			'(GMT+07:00) Bangkok' => 'Asia/Bangkok',
			'(GMT+07:00) Hanoi' => 'Asia/Bangkok',
			'(GMT+07:00) Jakarta' => 'Asia/Jakarta',
			'(GMT+07:00) Krasnoyarsk' => 'Asia/Krasnoyarsk',
			'(GMT+08:00) Beijing' => 'Asia/Hong_Kong',
			'(GMT+08:00) Chongqing' => 'Asia/Chongqing',
			'(GMT+08:00) Hong Kong' => 'Asia/Hong_Kong',
			'(GMT+08:00) Irkutsk' => 'Asia/Irkutsk',
			'(GMT+08:00) Kuala Lumpur' => 'Asia/Kuala_Lumpur',
			'(GMT+08:00) Perth' => 'Australia/Perth',
			'(GMT+08:00) Singapore' => 'Asia/Singapore',
			'(GMT+08:00) Taipei' => 'Asia/Taipei',
			'(GMT+08:00) Ulaan Bataar' => 'Asia/Irkutsk',
			'(GMT+08:00) Urumqi' => 'Asia/Urumqi',
			'(GMT+09:00) Osaka' => 'Asia/Tokyo',
			'(GMT+09:00) Sapporo' => 'Asia/Tokyo',
			'(GMT+09:00) Seoul' => 'Asia/Seoul',
			'(GMT+09:00) Tokyo' => 'Asia/Tokyo',
			'(GMT+09:00) Yakutsk' => 'Asia/Yakutsk',
			'(GMT+09:30) Adelaide' => 'Australia/Adelaide',
			'(GMT+09:30) Darwin' => 'Australia/Darwin',
			'(GMT+10:00) Brisbane' => 'Australia/Brisbane',
			'(GMT+10:00) Canberra' => 'Australia/Sydney',
			'(GMT+10:00) Guam' => 'Pacific/Guam',
			'(GMT+10:00) Hobart' => 'Australia/Hobart',
			'(GMT+10:00) Melbourne' => 'Australia/Melbourne',
			'(GMT+10:00) Port Moresby' => 'Pacific/Port_Moresby',
			'(GMT+10:00) Sydney' => 'Australia/Sydney',
			'(GMT+10:00) Vladivostok' => 'Asia/Vladivostok',
			'(GMT+11:00) Magadan' => 'Asia/Magadan',
			'(GMT+11:00) New Caledonia' => 'Asia/Magadan',
			'(GMT+11:00) Solomon Is.' => 'Asia/Magadan',
			'(GMT+12:00) Auckland' => 'Pacific/Auckland',
			'(GMT+12:00) Fiji' => 'Pacific/Fiji',
			'(GMT+12:00) Kamchatka' => 'Asia/Kamchatka',
			'(GMT+12:00) Marshall Is.' => 'Pacific/Fiji',
			'(GMT+12:00) Wellington' => 'Pacific/Auckland',
			'(GMT+13:00) Nuku\'alofa' => 'Pacific/Tongatapu'
		);

		if ( $flip ) {
			$timezones = array_flip($timezones);
		}

		if ( $get != '' ) {
			return $timezones[ $get ];
		}

		return $timezones;
	}

	public static function hasUploadFiles($name)
	{
		if ( !isset($_FILES[$name]['error']) ) return false;

		return $_FILES[$name]['error'] == UPLOAD_ERR_NO_FILE ? false : true;
	}

	public static function uploadFiles( $uploadDir, $allowed = 'jpg,jpeg,png,gif', $size=50, $max=1, $name=null )
	{
		$data = array();

		try
		{
			if ( !is_dir( $uploadDir ) )
			{
				if ( self::mkdir_p($uploadDir) === false )
				{
					throw new Exception('Cannot create document folder ('.$uploadDir.')');
				}
			}

			$adapter = new Zend_File_Transfer_Adapter_Http();

			$files = $adapter->getFileInfo($name);
			//$adapter->addValidator('NotExists', false, $uploadDir );
			//$adapter->setDestination( $this->uploadDir );
			$adapter->addValidator('Count', false, array('min' => 1 , 'max' => $max));
			$adapter->addValidator('Size', false, array('min' => 400 , 'max' => ($size * 1024 * 1024) , 'bytestring' => true));

			foreach ($files as $no => $fileinfo)
			{
				$adapter->addValidator('Extension', false, array('extension' => $allowed, 'case' => false));

				if ($adapter->isUploaded($no))
				{
					$ext = self::getExt($fileinfo['name']);

					$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
					$fileUrl = $uploadDir.'/'.$fileName;

					$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));
					$adapter->setOptions(array('useByteString' => false));

					$size = $adapter->getFileSize($no);

					if( !$adapter->receive($no))
					{
						$errmessage = array();
						if ( is_array($adapter->getMessages() ))
						{
							foreach(  $adapter->getMessages() as $errtype => $errmsg )
							{
								$errmessage[] = $errmsg;
							}

							throw new Exception(implode('<br />',$errmessage));
						}
					}

					//save file into db
					$data[] = array(

						'filename'		=> $fileinfo['name'],
						'fileurl'		=> $fileName,
						'filesize'		=> $size
					);

				} //isuploaded

			} //foreach
		}
		catch (Exception $e)
		{
			//throw new Exception( $e->getMessage() );
			return Cms_Render::error($e->getMessage());
		}

		return $data;
	}

	/*
	 * Delete a file or recursively delete a directory
	 *
	 * @param string $str Path to file or directory
	 */
	public static function removeDir($str) {
		if (is_file($str)) {
			return @unlink($str);
		}
		elseif (is_dir($str)) {
			$scan = glob(rtrim($str,'/').'/*');
			foreach($scan as $index=>$path) {
				self::removeDir($path);
			}
			return @rmdir($str);
		}
	}

	public static function getUserPhoto()
	{
		$user = Zend_Auth::getInstance()->getIdentity();

		if ( isset($user->photo) && $user->photo != '' )
		{
			$fc = Zend_Controller_Front::getInstance();

			return $fc->getBaseUrl().'/thumb.php?w=400&amp;s=1&amp;f='.DOCUMENT_URL.$user->photo;
		}
		else
		{
			$fc = Zend_Controller_Front::getInstance();
			return $fc->getBaseUrl().'/images/logout.png';
			//return 'https://www.gravatar.com/avatar/'.self::gravatar($user->email);
		}

	}

	public static function getUserPhotoById($user_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select()
						->from(array('a' => 'user'))
						->where('id = ?', $user_id);
		$result = $db->fetchRow($select);

		$user = (object)$result;
		//$user = Zend_Auth::getInstance()->getIdentity();

		if ( isset($user->photo) && $user->photo != '' )
		{
			$fc = Zend_Controller_Front::getInstance();

			return $fc->getBaseUrl().'/thumb.php?w=400&amp;s=1&amp;f='.DOCUMENT_URL.$user->photo;
		}
		else
		{
			return 'https://www.gravatar.com/avatar/'.self::gravatar($user->email);
		}

	}

	/*
	 * usage;
	 * <input type="hidden" name="token" value="<?php echo token()?>">
	 */
	public static function token($salt='')
	{
		$salt = $salt == '' ? APPLICATION_TITLE : $salt;
		$token = strtotime( date('F j, Y, g') );

		return sha1( $salt . $token );
	}

	/*
	 * usage;
	 * if ( tokenCheck($post['token'])  ) { //  }
	 */
	public static function tokenCheck($match='',$salt='')
	{
		$salt = $salt == '' ? APPLICATION_TITLE : $salt;
		$token = strtotime( date('F j, Y, g') );

		if ( $match === sha1( $salt . $token) )
		{
			return true;
		}

		return false;
	}

	public static function getTag($page, $string_start, $string_end)
	{
		$start = strpos(strtolower($page), strtolower($string_start)) + strlen($string_start);
		if(!$start) return(true); # If we don't have any settings

		$end = strpos(strtolower($page), strtolower($string_end), $start);
		if(!$end) return(false);
		# Else

		$data = substr($page, $start, $end - $start);

		$page = trim(substr_replace($page, "", $start - strlen($string_start),
				($end + strlen($string_end)) - ($start - strlen($string_start))))
			. "\n";

		return($data);
	}

	public static function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	public static function htmlRedirect($too,$howlong="2")
	{
		$howlong =  $howlong * 1000;

		$a = "<script type=\"text/javascript\">
			 <!--
			setTimeout(\"window.location.href = '$too' \",$howlong);
			//-->
			</script>";

		return $a;
	}

	public static function MigsAmount($amount)
	{
		return defined('PAYMENT_TEST') && PAYMENT_TEST == 0 ? round($amount) :$amount;
	}

	public static function getDuration($date_start, $date_end, $format = 'Y-m-d')
    {
        $listData2 = array();
        $end = date($format,strtotime($date_end));
        $start = date($format,strtotime($date_start));

        for(;;) {
            $listData2[]=date($format, strtotime($start));
            $start = date($format, strtotime("+1 day", strtotime($start)));
            if(strtotime($start)>strtotime($end)) break;
        }

        return $listData2;
    }

    public static function limitText($text, $limit) 
    {
		$limit = abs((int)$limit);
		if(strlen($text) > $limit) {
			$text = preg_replace("/^(.{1,$limit})(\s.*|$)/s", '\\1...', $text);
		}
		return($text); 
    }

    public static function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public static function getEstatus($code='')
	{
		$estatus_array = array(
			"1" => "Self employed",
			"2" => "Goverment Sector",
			"3" => "Private Sector",
			"4" => "Retiree",
			"5" => "Housewife",
			"6" => "Others",

		);

		return $code == '' ? $estatus : @$estatus_array[ ($code) ];
	}

	 public static function getGender($code='')
	{
		$gender_array = array(
			""  => "",
			"1" => "Male",
			"2" => "Female",
		);

		return $code == '' ? $gender : @$gender_array[ ($code) ];
	}

	public static function getRace($code='')
	{
		//Race
        $dataDb = new Admin_Model_DbTable_ExternalData();
        $race = $dataDb->getData('Race');

        $race_list = array('' => '');
        foreach( $race as $row ) {
            $race_list[ $row['id'] ] = $row['name'];
        }



        return $code == '' ? $race : @$race_list[ ($code) ];
    }

    public static function getReligion($code='')
	{
		//Religion
        $dataDb = new Admin_Model_DbTable_ExternalData();
        $religion = $dataDb->getData('Religion');

        $religion_list = array('' => '');
        foreach( $religion as $row ) {
            $religion_list[ $row['id'] ] = $row['name'];
        }



        return $code == '' ? $religion : @$religion_list[ ($code) ];
    }

    public static function getCountryDb()
    {
    	$db = getDB();
    	$select = $db->select()->from(array('a' => 'country'))->order('a.name');
    	$result = $db->fetchAll($select);

    	$response = array();

    	foreach ($result as $row)
    	{
    		$response[$row['id']] = $row['name'];
    	}
    	
    	return $response;
    }

     public static function getQualification($code='')
    {
        //Qualification
        $dataDb = new Admin_Model_DbTable_ExternalData();
        $qualification = $dataDb->getData('Qualification');

        $qualification_list = array('' => '');
        foreach( $qualification as $row ) {
            $qualification_list[ $row['id'] ] = $row['name'];
        }

        return $code == '' ? $qualification : @$qualification_list[ ($code) ];
    }

}
