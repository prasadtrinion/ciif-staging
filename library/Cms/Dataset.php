<?php
class Cms_Dataset
{
    static $path;

    function __construct()
    {
        self::config();
    }

    static function config()
    {
        self::$path = APPLICATION_PATH . '/data/';
    }
    static function get($name)
    {
        if ( !isset(self::$path) && self::$path == '' ) {
            self::config();
        }

        $get = file_get_contents(self::$path.$name.'.json');

        $json = json_decode(trim($get), true);

        if ( json_last_error())
        {
            throw new Exception('Parsing Dataset Error "'.$name.'". Error Message: '.json_last_error_msg());
        }

        return $json;
    }
}