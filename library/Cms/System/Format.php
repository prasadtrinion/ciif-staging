<?php

class Cms_System_Format extends Zend_View_Helper_Abstract{
	
	function generateStudentID($student_type){
									
		//generate student ID
		$studentid_format = array();
		
		$seqnoDb = new App_Model_General_StudentRegistrationSeqNo();
		$sequence = $seqnoDb->getData(date('Y'));
		
		$tblconfigDB = new App_Model_General_TblConfig();
		$config = $tblconfigDB->getConfig(1);
                
                if($student_type==741){ 
                    $seqno = $sequence['srs_vseqno'];
                }else{
                    $seqno = $sequence['srs_seqno']; 
                }
		
		
		//format
		 $format_config = explode('|',$config['StudentIdFormat']);
		
		for($i=0; $i<count($format_config); $i++){
			$format = $format_config[$i];
			if($format=='px'){ //prefix
				$result = $config['StudentIdPrefix'];
			}elseif($format=='YYYY'){
				$result = date('Y');
			}elseif($format=='YY'){
				$result = date('y');
			}elseif($format=='seqno'){				
				$result = sprintf("%05d", $seqno);
			}elseif($format=='type'){
				if($student_type==741){ 
					//Visiting					
					$result = 'V';
				}
			}
			
			if(isset($result)) array_push($studentid_format,$result);
		}
	
		//var_dump(implode("-",$studentid_format));
		$studentID = implode("",$studentid_format);
		
		//update sequence
                if($student_type==741){ 
                    $seqnoDb->updateData(array('srs_vseqno'=>$seqno+1),$sequence['srs_id']);
                }else{
                    $seqnoDb->updateData(array('srs_seqno'=>$seqno+1),$sequence['srs_id']);
                }
		
		return	$studentID;
				
	}
	
	

}
 ?>