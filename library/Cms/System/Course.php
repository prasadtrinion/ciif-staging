<?php
class Cms_System_Course extends Cms_Base
{

    public static function canEdit()
    {
        return false;
    }

    public static function canView( $course )
    {
        //normal coruses are public
        /*if ( Zend_Auth::getInstance()->hasIdentity() )
        {
            return false;
        }

        $user = Zend_Auth::getInstance()->getIdentity();

        //check perm
        if ( $user->role == 'administrator' )
        {
            return true;
        }*/

        //visibility
        if ( $course['visibility'] == 1 )
        {
            return true;
        }

        return false;
    }

    public static function link($code,$cid='')
    {
        $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();

        return $baseUrl.'/courses/'.$code.($cid!=''?'/pid/'.$cid:'/learn');
    }

    public static function getContentTypes($type, $return='')
    {
        $t = Zend_Registry::get('Zend_Translate');

        $data = array(
                        'content'           =>  array(
                                                        'icon'  =>  'file-text-o',
                                                        'name'  =>  $t->_('Content Page'),
                        ),
                        'scorm'             =>  array(
                                                        'icon'  =>  'cubes',
                                                        'name'  =>  $t->_('SCORM'),
                        ),
                        'assignment'        =>  array(
                                                        'icon'  =>  'file-archive-o',
                                                        'name'  =>  $t->_('Assignment'),
                        ),
                        'quiz'              =>  array(
                                                        'icon'  =>  'check-circle-o',
                                                        'name'  =>  $t->_('Quiz'),
                        ),
                        'support'           =>  array(
                                                        'icon'  =>  'download',
                                                        'name'  =>  $t->_('Download'),
                        ),
                        'link'              =>  array(
                                                        'icon'  =>  'link',
                                                        'name'  =>  $t->_('Link'),
                        ),
                        'video'              =>  array(
                                                        'icon'  =>  'video-camera',
                                                        'name'  =>  $t->_('Video'),
                        ),
                        'audio'              =>  array(
                                                        'icon'  =>  'volume-up',
                                                        'name'  =>  $t->_('Audio'),
                        ),
                        'glossary'          =>  array(
                                                        'icon'  =>  'book',
                                                        'name'  =>  $t->_('Glossary'),
                        ),
						'wiki'           =>  array(
                                                        'icon'  =>  'wikipedia-w',
                                                        'name'  =>  $t->_('Wiki Page'),
                        ),
        );

        if ( !isset($data[$type]) ) throw new Exception('Invalid Content Type. getContentTypes()');

        if ( $return == '' ) return $data[$type];
        return $data[$type][$return];
    }
    public static function getContentName($type)
    {
        return self::getContentTypes($type, 'name');
    }

    public static function getContentIcon($type)
    {
        return self::getContentTypes($type, 'icon');
    }
}