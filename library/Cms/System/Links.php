<?php
class Cms_System_Links extends Cms_Base
{
    public static function dataFile($filename='', $full=true)
    {
        return ( $full == true ? DOCUMENT_PATH : '' ).'/files'.( $filename == '' ? '' : '/'.$filename);
    }

    public static function dataFileUrl($filename='')
    {
        return DOCUMENT_URL . $filename ;
    }

    public static function dataFileShared($filename='', $full=true)
    {
        return ( $full == true ? SHARED_DOCUMENT_PATH : '' ).'/files'.( $filename == '' ? '' : '/'.$filename);
    }
    public static function dataFileUrlShared($filename='')
    {
        return SHARED_DOCUMENT_URL .'/files'. $filename ;
    }
}