<?php
class Cms_Scorm_Package
{
    /**
     * Check scorm package
     * @param string $file The File
     * @return boolean
     * @throws Exception if invalid file
     */
    public static function check($file)
    {
        $file_info = pathinfo($file);
        $filename = $file_info['basename'];
        $extension = $file_info['extension'];

        if( strtolower($extension) != 'zip' )
        {
            throw new Exception('Invalid file. Expected ZIP only.');
        }

        //$result = file_get_contents('zip://'.$file.'#imsmanifest.xml');
        $za = new ZipArchive();

        $za->open($file);
        $scorm = false;

        for( $i = 0; $i < $za->numFiles; $i++ ){
            $stat = $za->statIndex( $i );

            if (  trim(basename( $stat['name'] ) . PHP_EOL ) == 'imsmanifest.xml' )
            {
                $scorm = true;
                break;
            }
        }

        return $scorm;
    }

    /**
     * Extract scorm package
     * @param string $file The zip file
     * @param string $to Path to extract
     * @return string manifest file
     * @throws Exception if invalid file
     */
    public static function extract($file, $to, $nopath = false)
    {
        if  ( !is_dir($to) )
        {
            Cms_Common::mkdir_p($to);
        }

        if ( $nopath !== false )
        {
            $zip = new ZipArchive;
            if ($zip->open($file) === true) {
                for($i = 0; $i < $zip->numFiles; $i++) {
                    $filename = $zip->getNameIndex($i);
                    $fileinfo = pathinfo($filename);

                    copy("zip://".$file."#".$filename, $to."/".$fileinfo['basename']);
                }
                $zip->close();

                return $to . '/imsmanifest.xml';

            }else {
                throw new Exception('Invalid Zip File');
            }
        }
        else
        {

            //removed
            //$allowedDir = array('common','extend','lms','mobile','story_content','unique','vocab','slides');

            $zip = new ZipArchive;
            $manifest = '';

            if ($zip->open($file) === true)
            {
                for($i = 0; $i < $zip->numFiles; $i++)
                {
                    $filename = $zip->getNameIndex($i);
                    $fileinfo = pathinfo($filename);

                    //$path = in_array( pathinfo($fileinfo['dirname'])['basename'], $allowedDir ) ? pathinfo($fileinfo['dirname'])['basename'] : '';

                    $folder = $fileinfo['dirname'];

                    if ( preg_match('/imsmanifest\.xml/', $filename) )
                    {
                        $manifest = $filename;
                    }

                    $curfolder = '';

                    $folder = preg_replace('/[^\da-z\_\-\/]/i', '', $folder);

                    //if ( in_array($folder , $allowedDir) )
                    //{
                        if ( !is_dir($to.'/'.$folder) )
                        {
                            Cms_Common::mkdir_p($to.'/'.$folder);
                        }

                       $curfolder = $folder.'/';
                    //}

                    //copy
                    if  ( $fileinfo['basename'] != $fileinfo['filename'] )
                    {
                        copy("zip://".$file."#".$filename, $to."/".$curfolder.$fileinfo['basename']);
                    }
                }

                $zip->close();

                $manifest = preg_replace('/[^\da-z\_\-\/]/i', '', $manifest);

                return $manifest;
            }
            else
            {
                throw new Exception('Invalid zip file');
            }

            /*
            $zip = new ZipArchive;
            $res = $zip->open($file);
            if ($res === TRUE)
            {
                $zip->extractTo($to);
                $zip->close();

                return $to . '/imsmanifest.xml';

            } else {
                throw new Exception('Invalid Zip File');
            }
            */
        }

    }



}