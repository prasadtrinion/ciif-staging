<?php
class Cms_Scorm_Api
{
    public static function getDurationSeconds($duration)
    {
        preg_match_all('/[0-9]+[HMS]/',$duration,$matches);
        $duration=0;
        foreach($matches as $match){
             foreach($match as $portion){
                $unite=substr($portion,strlen($portion)-1);
                switch($unite){
                    case 'H':{
                        $duration +=    substr($portion,0,strlen($portion)-1)*60*60;
                    }break;
                    case 'M':{
                        $duration +=substr($portion,0,strlen($portion)-1)*60;
                    }break;
                    case 'S':{
                        $duration +=    substr($portion,0,strlen($portion)-1);
                    }break;
                }
            }
        }
        return $duration;

    }

    public static function getUserData($user_id, $scorm_id)
    {
        $scormApiDb = new App_Model_ScormApi();

        $data = $scormApiDb->getDataAll(array('user_id = ?' => $user_id, 'scorm_id = ?' => $scorm_id ));

        $res = array();

        foreach ( $data as $row )
        {
            $res[$row['datakey']] = $row['value'];
        }

       return $res;
    }

    public static function restful()
    {
        $scormApiDb = new App_Model_ScormApi();
        $scormUserDb = new App_Model_UserScorm();

        $fc = Zend_Controller_Front::getInstance();
        $postdata = $fc->getRequest()->getPost();

        if ( empty($postdata['data']) || !isset($postdata['data']) )
        {
            $data = array('success' => 'false');
            echo json_encode($data);
            exit;
        }
        $res = array();

        $user_id = Zend_Auth::getInstance()->hasIdentity() ? Zend_Auth::getInstance()->getIdentity()->id : (isset($postdata['user_id']) ? $postdata['user_id'] : 0);

        if ( empty($user_id) )
        {
            $data = array('success' => 'false','msg' => 'user_id is required');
            echo json_encode($data);
            exit;
        }


        $userdata = array();

        foreach ( $postdata['data'] as $key => $value )
        {
            $check = $scormApiDb->getData(array('user_id = ?' => $user_id, 'scorm_id = ?' => $postdata['module'], 'datakey = ?' => $key));

            if ( !empty($check) )
            {
                $data = array(
                    'modified_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'datakey' => $key,
                    'value' => $value
                );

                $scormApiDb->update($data, array('id = ?' => $check['id']));
            }
            else
            {
                $data = array(
                    'user_id' => $user_id,
                    'created_date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                    'scorm_id' => $postdata['module'],
                    'datakey' => $key,
                    'value' => $value
                );

                $scormApiDb->insert($data);
            }


            $res[$key] = $value;

        }

        //user data
        $check = $scormUserDb->getData(array('user_id = ?' => $user_id, 'data_id = ?' => $postdata['module']));

        if ( !empty($check) )
        {
            $data = array(
                'status'        => isset($res['cmi.completion_status']) ? $res['cmi.completion_status'] : '',
                'duration'      => isset($res['cmi.session_time']) ? Cms_Scorm_Api::getDurationSeconds($res['cmi.session_time']) : 0,
                'lastaccessed'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
            );

            $scormUserDb->update($data, array('user_id = ?' => $user_id, 'data_id = ?' => $postdata['module']));
        }
        else
        {
            $data = array(
                'data_id'       => $postdata['module'],
                'user_id'       => $user_id,
                'status'        => isset($res['cmi.completion_status']) ? $res['cmi.completion_status'] : '',
                'duration'      => isset($res['cmi.session_time']) ? Cms_Scorm_Api::getDurationSeconds($res['cmi.session_time']) : 0,
                'created_date'  => new Zend_Db_Expr('UTC_TIMESTAMP()'),
                'lastaccessed'  => new Zend_Db_Expr('UTC_TIMESTAMP()')
            );

            $scormUserDb->insert($data);
        }


        //return
        $data = array('success' => 'true', 'data' => $res );
        echo json_encode($data);
        exit;

    }

    public static function path($file='')
    {
        return DOCUMENT_PATH.'/content/scorm/'. ($file=='' ? '' : $file);
    }

    public static function pathData($courseid='',$filename='')
    {
        $filename = preg_replace("/[^a-z0-9\-\.]/i", "_" , $filename);

        return DOCUMENT_PATH.'/content/scorm/data/'. ($courseid=='' ? '' : $courseid.'/') . $filename;
    }

    public static function urlData($courseid='',$filename='',$full=false)
    {
        $filename = preg_replace("/[^a-z0-9\-\.]/i", "_" , $filename);

        return ($full==true ? DOCUMENT_URL : '').'/content/scorm/data/'. ($courseid=='' ? '' : $courseid.'/') . $filename;
    }

    public static function zipfile($file='')
    {
        return '/content/scorm/' . $file;
    }

    public static function urlPackage($file='')
    {
        return '/scorm' . $file;
    }
}