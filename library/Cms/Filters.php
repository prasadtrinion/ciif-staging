<?php
class Cms_Filters
{
    //trigger
    static function apply($name, $content = '')
    {
        $events = Zend_Registry::get('Events');

        $response = $events->trigger('filter-'.$name, null, array('content' => $content) );

       if ( $response->count() == 0 )
        {
            return $content;
        }
        else
        {
            return $response->last()['content'];
        }
    }

    static function add($event, $callback = null, $priority = 1 )
    {
        $events = Zend_Registry::get('Events');
        return $events->attach('filter-'.$event, $callback, $priority);
    }
}