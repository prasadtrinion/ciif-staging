<?php 
class icampus_Plugin_UserOnline extends Zend_Controller_Plugin_Abstract 
{
	//dispatchLoopShutdown
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		 
    	$auth = Zend_Auth::getInstance();

    	if ($auth->hasIdentity()) 
		{	
			$user = $auth->getIdentity();
			// echo "<pre>";
			// print_r($user);
			// die();


			$session_id = Zend_Session::getId();

			$db = getDb();

			$sql = $db->select()->from(array("u"=>"user_online"))
								  ->where('u.session_id = ?', $session_id );
			
			$check = $db->fetchRow($sql);

			if ( !empty($check) )
			{
				$db->update("user_online",array('last_activity' => time()), array('session_id = ?' => $session_id));
			}
			else
			{
				$data = array(
								'session_id'	=> $session_id,
								'user_id'		=> $user->id,
								'user_ip'		=> ip2long( Cms_Common::get_ip() ),
								'user_role'		=> $user->role,
								'last_activity' => time()
							);

				$db->insert("user_online", $data);
			}
    	} 
		
		//kill inactive session
		$db->delete('user_online', array('last_activity < (UNIX_TIMESTAMP() - 300)'));
	}
}
?>