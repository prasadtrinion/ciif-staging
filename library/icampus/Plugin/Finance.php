<?php 
class icampus_Plugin_Finance {

    public function generateProformaIndividual($IdSubject=0, $IdStudentRegistration, $categoryId=1, $totalStudent=1, $item=889, $er_main_id=0) {
        $params = array(
            'IdSubject'             => $IdSubject,
            'IdStudentRegistration' => $IdStudentRegistration,
            'categoryId'            => $categoryId,
            'totalStudent'          => $totalStudent,
            'item'                  => $item,
            'er_main_id'            => $er_main_id
        );
        $proformaInvoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateProformaIndividual', $params);

        if ( empty($proformaInvoice) || $proformaInvoice['error'] == 1 ) {
            return Cms_Render::error('Unable to generate proforma invoice.');
        }

        return $proformaInvoice;
    }

    public function generateInvoice($ProformaId, $creatorId = 655, $from = 1, $returnCurrency = 1, $coupon = array()) {
        $params = array(
            'ProformaId'     => $ProformaId,
            'creatorId'      => $creatorId,
            'from'           => $creatorId,
            'returnCurrency' => $returnCurrency
        );

        if(isset($coupon['coupon_id']) && $coupon['coupon_id']) {
            $params['coupon_id']     = $coupon['coupon_id'];
            $params['coupon_amount'] = $coupon['coupon_amount'];
        }

        $invoice = Cms_Curl::__(SMS_API . '/get/Finance/do/generateInvoice', $params);

        if ( empty($invoice) || $invoice['error'] == 1 ) {
            return Cms_Render::error('Something went wrong #552.');
        }
        
        return $invoice;
    }

}
?>