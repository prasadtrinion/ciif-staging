<?php
class icampus_Plugin_Student extends Zend_Controller_Plugin_Abstract
{
    public function createSmsProfile($params = array())
    {
        $auth = Zend_Auth::getInstance();
        if (empty($params))
        {
            if (!isset($auth->getIdentity()->external_id))
            {
                return false;
            }
            
            if ($auth->getIdentity()->external_id)
            {
                return $auth->getIdentity()->external_id;
            }
        
            $params = (array) $auth->getIdentity();
        }
        
        $response = Cms_Curl::__(SMS_API . '/get/Registration/do/addNewProfile', $params);
        $sp_id = false;
        
        if (isset($response['sp_id']) && $response['sp_id'])
        {
            $sp_id = $response['sp_id'];
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->update('user', array('external_id' => $sp_id), array('id = ?' => $params['id']));
            
            $session  = new Zend_Session_Namespace('Zend_Auth');
            $session->storage->external_id = $sp_id;
            $session->storage->sp_id       = $sp_id;
        }
        
        return $sp_id;
    }
}
?>