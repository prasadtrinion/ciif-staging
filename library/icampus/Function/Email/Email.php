<?php
class icampus_Function_Email_Email {

    private function getTemplate($template_id,$locale='en_US'){
        $db = getDB2();

        $select = $db->select()
            ->from(array('ct' => 'comm_template'), array('tpl_type', 'tpl_name'))
            ->join(array('ctc'=>'comm_template_content'),'ctc.tpl_id=ct.tpl_id')
            ->where('ct.tpl_id = ?',$template_id)
            ->where('ctc.locale = ?',$locale);

        return $rows = $db->fetchRow($select);
    }

    private function addtoemailque($emailcontent){
        $db = getDB2();
        $db->insert('email_que', $emailcontent);
    }

    public function sendEmail($info){
        //contoh array
        /*$info = array(
            'template_id' => 1,
            'name' => 'name',
            'program_name' => 'name',
            'membership_name' => 'name',
            'title' => 'title'
        );*/

        $template = $this->getTemplate($info['template_id']);

        if ($template){
            $emailContent = $template['tpl_content'];
            $emailContent = str_replace('[Name]', $info['name'], $emailContent);
            $emailContent = str_replace('[MembershipName]', $info['membership_name'], $emailContent);
            $emailContent = str_replace('[ProgramName]', $info['program_name'], $emailContent);

            $data_email = array(
                'recepient_email' => 'tammy145@gmail.com',
                'subject' => $template['tpl_name'],
                'content' => $emailContent,
                'attachment_path'=>null,
                'attachment_filename'=>null
            );
            $this->addtoemailque($data_email);
        }
    }
}